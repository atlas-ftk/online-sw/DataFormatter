#ifndef __FTKIMMODULE__
#define __FTKIMMODULE__

#include "DataFormatter/DataFormatterConstants.h"
#include "DataFormatter/FtkIPBusApi.h"

using namespace std;

namespace daq {
  namespace ftk {
    class FtkIMApi {
    public:
      FtkIMApi(std::shared_ptr<FtkIPBusApi> ipbus, const std::string& name);
      ~FtkIMApi(){};
      
      inline std::string name_ftk() { return m_name; }
      inline std::vector<uint32_t> getIMLutIdx() {return m_IMLUTidx;};
      inline void setIMLutIdx(vector<uint32_t>& IMLUTidx) { m_IMLUTidx = IMLUTidx; };
      inline void setShelfAndSlot( uint64_t shelf, uint64_t slot ) { m_shelf = shelf; m_slot = slot; }
      inline void setName( const std::string& name ) { m_name = name; }
      inline uint32_t get_fw_version() { return m_fw_version; }
      inline uint32_t get_fw_type() { return m_fw_type; }
      
      uint32_t get_i2c_address(const uint32_t& fpgaId);
      void single_access_write( const std::string& nodename, const uint32_t& value ) ;
      void set_fw_version( const uint32_t& channel );
      void set_word_counter( const uint32_t& n_word);
      // void sendIMCounterReset(const uint32_t& channel );
      
      // void sendIMCounterReset(const uint32_t& channel );
      void send_freeze( const uint32_t& channel, const bool& b_freeze );
      void set_slink_mode( const uint32_t& slink_mode );
      
      bool control_global_register(  const uint32_t& channel, const std::string& register_name, const uint32_t& value );
      bool control_xoff(  const uint32_t& channel, const uint32_t& xoff_on_off );
      bool control_slink( const uint32_t& channel, const uint32_t& slink_enable );
      bool control_pseudo_data( const uint32_t& channel, const uint32_t& pseudo_enable, const uint32_t& pseudo_mode );
      bool control_pseudo_config( const uint32_t& channel, const int& skew_count_at_40MHz, const bool& is_pseudo_data_xoff  );
      void print_fw_verion( const uint32_t& fpgaId, const uint32_t& do_check, const uint32_t& oks_fw_version, bool is_standalone = false );
      
      
      void downloadIMLUT( const uint32_t& channel, const std::string& LUT_file);
      void downloadIMLUT( std::vector<uint32_t>& vecLinkId, std::vector<uint32_t>& vecHashId, std::vector<uint32_t>& vecRODId, const uint32_t& channel );
      void downloadAllIMLUT( const std::string& LUT_file);
      void downloadAllIMLUT_OTF( const std::string& LUT_prefix, const std::map<uint32_t, uint32_t>& FMCInToRodIdMap);
      
      void download_pseudo_data( const uint32_t& channel, const std::string& pseudoData_file, const int& input_rate = 100 ); // in kHz
      void downloadAllPseudoData( const std::string& pseudoData_file, const int& input_rate = 100 ); // in kHz
      void downloadAllPseudoData_OTF( const std::string& pseudoData_prefix, const std::map<uint32_t, uint32_t>& FMCInToRodIdMap, const int& input_rate = 100 ); // in kHz
      
      
      bool set_IM_OKS( std::vector< uint32_t > channels, const std::map< std::string, uint32_t >& OKS_parameters );
      
      void set_i2c_blockSize( const uint32_t blockSize );
      void read_from_DFBuffer_block(const uint32_t blockSize, std::vector<uint32_t>& value);
      void read_from_IMBuffer_to_DFBuffer( const uint32_t& channel, const uint32_t& register_address, const uint32_t& ireadout, bool is_apply_freeze, const uint32_t& df_buffer_depth, bool is_standalone = false );
      void read_from_IMBuffer_to_DFBuffer( const uint32_t& channel, const std::string& register_name, const uint32_t& ireadout, bool is_apply_freeze, const uint32_t& df_buffer_depth, bool is_standalone = false );
      bool i2c_single_access_core( const bool is_write, const uint32_t& i2c_address, const uint32_t& value1, uint32_t& value2, uint32_t& busy, uint32_t& ackerror );
      bool i2c_single_access_read_or_write(bool is_write, const uint32_t& i2c_address, const uint32_t& value1, uint32_t& value2);
      bool i2c_single_access_read_or_write_block( bool is_write, const uint32_t& i2c_address, const uint32_t& value1, uint32_t& value2);
      bool i2c_32b_read(const uint32_t i2c_address, const uint32_t internal_address, uint32_t& value);
      bool i2c_32b_read_block(const uint32_t i2c_address, const uint32_t internal_address, uint32_t& value);
      bool i2c_32b_write(const uint32_t i2c_address, const uint32_t internal_address, const uint32_t value);
      
      bool read_register(   const uint32_t& channel, std::string register_name,       uint32_t& value_to_read, uint32_t internal_address_offset = 0x0 );
      bool write_register(  const uint32_t& channel, std::string register_name, const uint32_t& value_to_write );
      bool update_register( const uint32_t& channel, std::string register_name, const uint32_t value_to_update );
      bool set_register(    const uint32_t& channel, std::string register_name, const uint32_t value_to_update );
      uint32_t get_global_address(       const uint32_t& channel, std::string register_name ) ;
      uint32_t get_internal_address(     const uint32_t& channel, std::string register_name ) ;
      uint32_t get_register_shift_value( const uint32_t& channel, std::string register_name, std::string bit_name );
      
      
      inline const uint32_t get_map_cont( const std::map< const std::string, const uint32_t >& _map, const std::string _key ) { return _map.at( _key ); }
      
    private:
      std::string m_name;
      std::shared_ptr<FtkIPBusApi> m_ipbus;
      
      std::vector<uint32_t> m_IMLUTidx;
      
      // DF shelf and slot
      uint32_t m_shelf = 0;
      uint32_t m_slot = 0;
      
      
      // new schema for IM refactored FW
      uint32_t m_current_fpga;
      uint32_t m_fw_version;
      uint32_t m_fw_type;
      uint32_t m_fw_hash;
      uint32_t m_fw_date;
      // bool m_is_old_fw_treatment;
      
      const std::map< const std::string, const uint32_t > m_map_im_global_address = {
        { "GA_FW_VER",        0x0 },
        { "GA_FW_TYPE",       0x1 }, 
        { "GA_STATUS_REG",    0x2 },
        { "GA_CONTROL_REG0",  0x3 }, 
        { "GA_CONTROL_REG1",  0x4 }, 
        { "GA_CONFIG_REG0",   0x5 },
        { "GA_CONFIG_REG1",   0x6 },
        { "GA_FW_DATE",       0x7 }, 
        { "GA_FW_HASH",       0x8 }, 
        { "GA_OFFSET0",       0xA },
        { "GA_OFFSET1",       0xB },
        { "GA_INTERNALDATA0", 0xC },
        { "GA_INTERNALDATA1", 0xD }
      };
      
      const std::map< const std::string, const uint32_t > m_map_im_reg_bit = {
        { "PSEUDO_MODE"         ,  0x0 },  // control_reg 
        { "PSEUDO_ENABLE"       ,  0x1 }, 
        { "IGNORE_HOLD"         ,  0x2 }, 
        { "SLINK_ENABLE"        ,  0x3 }, 
        { "SLINK_MODE"          ,  0x4 }, 
        { "XOFF_MODE"           ,  0x5 }, 
        { "GANGED_PIXEL"        ,  0x6 }, 
        { "REDUNDANCY"          ,  0x7 }, 
        { "DUPLICATED_MODULE"   ,  0x8 }, 
        { "IS_APPLY_SELECTPS"   ,  0x9 }, 
        { "IS_APPLY_REJECTPS"   ,  0xA }, 
        { "IS_APPLY_L1TTPS"     ,  0xB }, 
        { "IS_APPLY_TRUNCATION" ,  0xC }, 
        { "IS_APPLY_FREEZE"     ,  0xD }, 
        { "IS_APPLY_L1ID_FREEZE",  0xE }, 
        { "IS_APPLY_DF_FREEZE"  ,  0xF }, 
        { "IS_APPLY_ERR_FREEZE" ,  0x1F },
        { "SPY_RESET"     ,  0x0 }, // config_reg
        { "OKS_RESET"     ,  0x1 }, 
        { "FIFO_RESET"    ,  0x2 }, 
        { "GTP_RESET"     ,  0x3 }, 
        { "DATAFLOW_RESET",  0x4 }, 
        { "MONITOR_RESET" ,  0x5 }, 
        { "I2C_RESET"     ,  0x6 }, 
        { "FREEZE"        ,  0x7 }, 
        { "FORCE_RELEASE" ,  0x8 },
        { "0X0", 0x0 }, // just number
        { "0X1", 0x1 }, // just number
        { "0X2", 0x2 }, // just number
        { "0X3", 0x3 }, // just number
        { "0X4", 0x4 }, // just number
        { "0X5", 0x5 }, // just number
        { "0X6", 0x6 }, // just number
        { "0X7", 0x7 }, // just number
        { "0X8", 0x8 }, // just number
        { "0X9", 0x9 }, // just number
        { "0XA", 0xA }, // just number
        { "0XB", 0xB }, // just number
        { "0XC", 0xC }, // just number
        { "0XD", 0xD }, // just number
        { "0XE", 0xE }, // just number
        { "0XF", 0xF }  // just number
      };
      const std::map< const std::string, const uint32_t > m_map_im_internal_address = {
        { "IA_OKS_L1TT_PS_MASK",           0x00000001 },
        { "IA_OKS_L1ID_PS_VALUE",          0x00000002 },
        { "IA_OKS_L1ID_PS_MASK",           0x00000003 },
        { "IA_OKS_L1ID_FREEZE_VALUE",      0x00000004 },
        { "IA_OKS_L1ID_FREEZE_MASK",       0x00000005 },
        { "IA_OKS_ERR_FREEZE_MASK1",       0x00000006 },
        { "IA_OKS_ERR_FREEZE_MASK2",       0x00000007 },
        { "IA_OKS_START_L1ID_MASK",        0x00000008 },
        { "IA_OKS_START_L1ID_VALUE",       0x00000009 },
        { "IA_OKS_RECOVER_CONFIG",         0x0000000A },
        { "IA_OKS_FILLGAP_CONFIG",         0x0000000B },
        { "IA_OKS_HIT_PER_MODULE_IBL",     0x0000000C },
        { "IA_OKS_HIT_PER_MODULE_PIX",     0x0000000D },
        { "IA_OKS_HIT_PER_MODULE_SCT",     0x0000000E },
        { "IA_OKS_IBL_KENTO_REMOVAL",      0x0000000F },
        { "IA_OKS_SPECIAL5",               0x00000010 },
        { "IA_OKS_FREEZE_WAIT_CLK",        0x00000011 },
        { "IA_OKS_GTP_RESET_WAIT_CLK",     0x00000012 },
        { "IA_OKS_STATUS_MONITOR_OPT",     0x00000013 },
        { "IA_OKS_PSEUDO_DELAY_CH0_VALUE", 0x00000014 },
        { "IA_OKS_PSEUDO_DELAY_CH1_VALUE", 0x00000015 },
        { "IA_OKS_PSEUDO_DATA_CONFIG_CH0", 0x00000016 },
        { "IA_OKS_PSEUDO_DATA_CONFIG_CH1", 0x00000017 },
        
        { "IA_OKS_XOFF_KEYWORD",           0x00000020 },
        { "IA_OKS_XOFF_CONFIG",            0x00000021 },
        { "IA_CH0_ISPY_BEGIN",             0x00010000 }, //  TO 0x00010FFF;
        { "IA_CH0_OSPY_BEGIN",             0x00012000 }, //  TO 0x00012FFF;
        { "IA_CH0_SMON_BEGIN",             0x00080000 }, //  TO 0x0008FFFF;
        { "IA_CH1_ISPY_BEGIN",             0x00011000 }, //  TO 0x00011FFF;
        { "IA_CH1_OSPY_BEGIN",             0x00013000 }, //  TO 0x00013FFF;
        { "IA_CH1_SMON_BEGIN",             0x000C0000 }, //  TO 0x000CFFFF;
        { "IA_CH0_COMMON_SPY_REG_ISPY",    0x10000000 },
        { "IA_CH0_COMMON_SPY_REG_OSPY",    0x10000001 },
        { "IA_CH1_COMMON_SPY_REG_ISPY",    0x30000000 },
        { "IA_CH1_COMMON_SPY_REG_OSPY",    0x30000001 },
        { "IA_CH0_PSEUDO_DATA",            0x00020000 },
        { "IA_CH1_PSEUDO_DATA",            0x00021000 },
        { "IA_CH0_MODULE_LOOKUP",          0x00040000 },
        { "IA_CH1_MODULE_LOOKUP",          0x00040100 }
      };
      const uint32_t nretry       = 10;
    }; // end of class FtkIMApi 
  }
}
#endif
