// written by yasuyuki.okumura@cern.ch 

#ifndef __DF_IPBUS_ACCESS_MULTI_BOARD_OK_HH__
#define __DF_IPBUS_ACCESS_MULTI_BOARD_OK_HH__

#include "uhal/uhal.hpp"
#include "uhal/ClientFactory.hpp"
#include <string>
//#include <stdint.h>
#include <vector>
#include <map>

class df_ipbus_access_multi_board_ok {
public:
  df_ipbus_access_multi_board_ok(const std::string& connection_file, 
				 const std::vector<std::string>& device_ids);
  ~df_ipbus_access_multi_board_ok(){};
  
private:
  uhal::ConnectionManager* m_manager;
  std::map<std::string, uhal::HwInterface*> m_hws;
  const std::string m_connection_file; 
  const std::vector<std::string> m_device_ids;
  void connection_inst();
  void set_lut_constants();
  std::vector<uint32_t>* m_addr_width;
  std::vector<uint32_t>* m_addr_max;
  std::vector<uint32_t>* m_lut_default_value;
  std::vector<uint32_t>* m_mask_all;
  uint32_t m_timeout_period;
  
public:
  std::map<std::string, uhal::HwInterface*> get_hardwares(){return m_hws;};
  std::string get_uri(const std::string& deviceId){return m_hws.at(deviceId)->uri();};
  std::string get_connection_file(){return m_connection_file; };
  std::vector<std::string> get_device_ids(){return m_device_ids;};
  void set_timeout_period(const uint32_t& value);
  uint32_t get_timeout_period(){return m_timeout_period;};
  
  void standard_reset_configuration();
  void clock_phase_configuration(const std::string& deviceId,
				 const uint32_t& im_fpga_id,
				 const uint32_t& inv_configuration,
				 const uint32_t& delay_configuration);
  void expected_nummodules_information_configuration(const std::string& deviceId,
						     const uint32_t& lane_id,
						     const uint32_t& update_mask,
						     const uint32_t& value);
  void gt_configuration(const std::string& deviceId,
			const uint32_t& gt_id,
			const uint32_t& rxpolarity,
			const uint32_t& txpolarity,
			const uint32_t& force_ready_mode,
			const uint32_t& to_altera_fpga);
  void gt_monitor(const std::string& deviceId,
		  const uint32_t& gt_id,
		  uint32_t& gt_rxpolarity,
		  uint32_t& gt_txpolarity,
		  uint32_t& force_ready_mode,
		  uint32_t& to_altera_fpga,
		  uint32_t& gt_rxbyteisaligned,
		  uint32_t& gt_tx_reset_done,
		  uint32_t& gt_rx_reset_done,
		  uint32_t& gt_pll_lock,
		  uint32_t& slink_testled_n,
		  uint32_t& slink_lderrled_n,
		  uint32_t& slink_lupled_n,
		  uint32_t& slink_flowctrlled_n,
		  uint32_t& slink_activityled_n,
		  uint32_t& slink_lrl
		  );
  void phase_scan(const std::string& deviceId, const uint32_t& im_fpga_id);
  void gtrxtx_reset_begin();
  void gtrxtx_reset_end();
  void pll_reset_begin();
  void pll_reset_end();
  void single_access_write(const std::string& deviceId,
			   const std::string& node, 
			   const uint32_t& write_value);
  bool single_access_write_read(const std::string& deviceId,
				const std::string& node, 
				const uint32_t& write_value);
  bool single_access_read(const std::string& deviceId,
			  const std::string& node, 
			  uint32_t& read_value);
  void spy_reset(const std::string& deviceId);
  void spy_restart(const std::string& deviceId);
  void spy_freeze(const std::string& deviceId);
  bool spy_dump_individual_lane(const std::string& deviceId, const uint32_t& lane_id, std::vector<uint32_t>& dout);
  bool read_32bit_counter(const std::string& deviceId, const uint32_t& type_id, const uint32_t& lane_id, uint32_t& dout);
  void write_lut(const std::string& deviceId,
		 const uint32_t& lut_type_id,
		 const uint32_t& lut_addr,
		 const uint32_t& lut_data,
		 const uint32_t& lut_lane_enable_mask);
  bool read_lut(const std::string& deviceId,
		const uint32_t& lut_type_id,
		const uint32_t& lut_addr,
		const uint32_t& lut_lane_id,
		uint32_t& lut_data);
  bool dump_lut(const std::string& deviceId,
		const uint32_t& lut_type_id,
		const uint32_t& lut_lane_id,
		std::vector<uint32_t>& dout);
  void init_lut(const std::string& deviceId, const uint32_t& lut_type_id);
  bool i2c_single_access_write(const std::string& deviceId,
			       const uint32_t& i2c_addr,
			       const uint32_t& value);
  bool i2c_single_access_read(const std::string& deviceId,
			      const uint32_t& i2c_addr,
			      uint32_t& value);
  
  // for configuration
  void configuration_map_initializer();
  void set_imfpga2clkinv(const std::string& device_id,
			 const std::map<uint32_t, uint32_t>& _imfpga2clkinv_) {
    m_imfpga2clkinv[device_id]=_imfpga2clkinv_;}
  void set_imfpga2clkdelay(const std::string& device_id,
			   const std::map<uint32_t, uint32_t>& _imfpga2clkdelay_) {
    m_imfpga2clkdelay[device_id]=_imfpga2clkdelay_;}
  void set_gtch2rxpolarity(const std::string& device_id,
			   const std::map<uint32_t, uint32_t>& _gtch2rxpolarity_) {
    m_gtch2rxpolarity[device_id]=_gtch2rxpolarity_;}
  void set_gtch2txpolarity(const std::string& device_id,
			   const std::map<uint32_t, uint32_t>& _gtch2txpolarity_) {
    m_gtch2txpolarity[device_id]=_gtch2txpolarity_;}
  void set_gtch2force_ready_mode(const std::string& device_id,
				 const std::map<uint32_t, uint32_t>& _gtch2force_ready_mode_) {
    m_gtch2force_ready_mode[device_id]=_gtch2force_ready_mode_;}
  void set_gtch2to_altera_fpga(const std::string& device_id,
			       const std::map<uint32_t, uint32_t>& _gtch2to_altera_fpga_) {
    m_gtch2to_altera_fpga[device_id]=_gtch2to_altera_fpga_;}
  void set_enable_fmc_lanes_mask(const std::string& device_id,
				 const uint32_t& _enable_fmc_lanes_mask_) {
    m_enable_fmc_lanes_mask[device_id]=_enable_fmc_lanes_mask_;}
  void set_this_board_mask_special_for_test(const std::string& device_id,
					    const uint32_t& _this_board_mask_ido_, 
					    const uint32_t& _this_board_mask_ili_) {
    m_this_board_mask_ido[device_id]=_this_board_mask_ido_;
    m_this_board_mask_ili[device_id]=_this_board_mask_ili_;}
  void set_this_board_mask(const std::string& device_id,
			   const uint32_t& _this_board_mask_) {
    m_this_board_mask_ido[device_id]=_this_board_mask_;
    m_this_board_mask_ili[device_id]=_this_board_mask_;}
  void set_fmcin2nummodules(const std::string& device_id,
			    const std::map<uint32_t, uint32_t>& _fmcin2nummodules_) {
    m_fmcin2nummodules[device_id]=_fmcin2nummodules_;}
  void set_slinkout2nummodules(const std::string& device_id,
			       const std::map<uint32_t, uint32_t>& _slinkout2nummodules_) {
    m_slinkout2nummodules[device_id]=_slinkout2nummodules_;}
  void set_centralswlaneid2destinationmask(const std::string& device_id,
					   const std::map<uint32_t, uint32_t>& _centralswlaneid2destinationmask_) {
    m_centralswlaneid2destinationmask[device_id]=_centralswlaneid2destinationmask_;}
  
  void set_lane_mod2idx(const std::string& device_id,
			std::map<std::pair<uint32_t, uint32_t>, uint32_t> _lane_mod2idx_) {
    m_lane_mod2idx[device_id]=_lane_mod2idx_;}
  void set_lane_idx2mod(const std::string& device_id,
			std::map<std::pair<uint32_t, uint32_t>, uint32_t> _lane_idx2mod_) {
    m_lane_idx2mod[device_id]=_lane_idx2mod_;}
  void set_pixmod2dst(const std::string& device_id, std::map<uint32_t, uint32_t> _pixmod2dst_){
    m_pixmod2dst[device_id]=_pixmod2dst_;}
  void set_sctmod2dst(const std::string& device_id, std::map<uint32_t, uint32_t> _sctmod2dst_){
    m_sctmod2dst[device_id]=_sctmod2dst_;}
  void set_pixmod2tower(const std::string& device_id, std::map<uint32_t, uint32_t> _pixmod2tower_){
    m_pixmod2tower[device_id]=_pixmod2tower_;}
  void set_sctmod2tower(const std::string& device_id, std::map<uint32_t, uint32_t> _sctmod2tower_){
    m_sctmod2tower[device_id]=_sctmod2tower_;}
  void set_pixmod2ftkplane(const std::string& device_id, std::map<uint32_t, uint32_t> _pixmod2ftkplane_){
    m_pixmod2ftkplane[device_id]=_pixmod2ftkplane_;}
  void set_sctmod2ftkplane(const std::string& device_id, std::map<uint32_t, uint32_t> _sctmod2ftkplane_){
    m_sctmod2ftkplane[device_id]=_sctmod2ftkplane_;}
  
private:
  void clock_phase_configuration(const std::string& deviceId);
  // configuration paramter device to configuration maps
  std::map<std::string, std::map<uint32_t, uint32_t> > m_imfpga2clkinv; // ok
  std::map<std::string, std::map<uint32_t, uint32_t> > m_imfpga2clkdelay; // ok
  void gt_configuration(const std::string& deviceId);
  std::map<std::string, std::map<uint32_t, uint32_t> > m_gtch2rxpolarity; // ok
  std::map<std::string, std::map<uint32_t, uint32_t> > m_gtch2txpolarity; // ok
  std::map<std::string, std::map<uint32_t, uint32_t> > m_gtch2force_ready_mode; // ok
  std::map<std::string, std::map<uint32_t, uint32_t> > m_gtch2to_altera_fpga; // ok
  std::map<std::string, uint32_t> m_enable_fmc_lanes_mask; // ok
  std::map<std::string, uint32_t> m_this_board_mask_ido; // ok
  std::map<std::string, uint32_t> m_this_board_mask_ili; // ok
  void expected_nummodules_information_configuration(const std::string& deviceId);
  std::map<std::string, std::map<uint32_t, uint32_t> > m_fmcin2nummodules; // ok
  std::map<std::string, std::map<uint32_t, uint32_t> > m_slinkout2nummodules; // ok
  static const uint32_t SW_CONF_UPDATE_MASK_FMC_INPUT_NUMBER_OF_EXPECTED_MODULES;
  static const uint32_t SW_CONF_UPDATE_MASK_SLINK_OUTPUT_NUMBER_OF_EXPECTED_MODULES;
  void internal_link_mask_configuration(const std::string& deviceId,
					const uint32_t& central_switch_output_lane_id,
					const uint32_t& mask_pattern);
  void internal_link_mask_configuration(const std::string& deviceId);
  
  std::map<std::string, std::map<std::pair<uint32_t, uint32_t>, uint32_t> > m_lane_mod2idx; // ok
  std::map<std::string, std::map<std::pair<uint32_t, uint32_t>, uint32_t> > m_lane_idx2mod; // ok
  std::map<std::string, std::map<uint32_t, uint32_t> > m_pixmod2dst; // ok
  std::map<std::string, std::map<uint32_t, uint32_t> > m_sctmod2dst; // ok
  std::map<std::string, std::map<uint32_t, uint32_t> > m_pixmod2tower; // ok
  std::map<std::string, std::map<uint32_t, uint32_t> > m_sctmod2tower; // ok
  std::map<std::string, std::map<uint32_t, uint32_t> > m_pixmod2ftkplane; // ok
  std::map<std::string, std::map<uint32_t, uint32_t> > m_sctmod2ftkplane; // ok
  
  std::map<std::string, std::map<uint32_t, uint32_t> > m_centralswlaneid2destinationmask; // ny
  
  void write_lut(const std::string& deviceId,
		 const uint32_t& lut_addr,
		 const uint32_t& lut_data,
		 const uint32_t& lut_lane_enable_mask);
  void lut_configuration(const std::string& deviceId);  
  static const uint32_t m_lut_type_mod2idx;
  static const uint32_t m_lut_type_idx2mod;
  static const uint32_t m_lut_type_pixmod2dst;
  static const uint32_t m_lut_type_sctmod2dst;
  static const uint32_t m_lut_type_pixmod2tower;
  static const uint32_t m_lut_type_sctmod2tower;
  static const uint32_t m_lut_type_pixmod2ftkplane;
  static const uint32_t m_lut_type_sctmod2ftkplane;
};

#endif
