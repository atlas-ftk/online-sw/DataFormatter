/********************************************************/
/*                                                      */
/*                                                      */
/********************************************************/

#ifndef READOUT_MODULE_DF_H
#define READOUT_MODULE_DF_H 

#include <string>
#include <mutex>
#include <memory>
#include "TH2F.h"
#include <vector>
#include <atomic>
#include <ctime>
#include <thread>

#include "ipc/partition.h"
#include "ipc/core.h"
#include "is/info.h"
#include "oh/OHRootProvider.h"
#include "oh/OHRawProvider.h"

#include "DataFormatter/dal/DataFormatterNamed.h"
#include "DataFormatter/FtkDataFormatterApi.h"
#include "DataFormatter/StatusRegisterATCAFactory.h"
#include "DataFormatter/DataFlowReaderIM.h"
#include "DataFormatter/DataFlowReaderDF.h"
#include "DataFormatter/smon_components.h"
#include "DataFormatter/DataFormatterHistHandler.h"
#include "DataFormatter/DataFormatterConfigHandler.h"
#include "DataFormatter/DataFormatterIPBusRegisterMap.h"

#include "ftkcommon/SourceIDSpyBuffer.h"
#include "ftkcommon/StatusRegister/StatusRegisterCollection.h"
#include "ftkcommon/ReadoutModule_FTK.h"

// EMon
#include "ftkcommon/FtkEMonDataOut.h"

//Spy Buffer
#include "DFSpyBuffer.h"

namespace daq {
  namespace ftk {

    // set default configuration for transition parallelization (see ftkcommon/ReadoutModule_FTK.h)
    const uint32_t READOUTMODULE_DATAFORMATTER_CONFIG_DEFAULT = 
                                                READOUTMODULE_FTK_PARALLEL_CONFIGURE |
                                                READOUTMODULE_FTK_PARALLEL_STOPHLT |
                                                READOUTMODULE_FTK_PARALLEL_MONITORING |
                                                READOUTMODULE_FTK_PROTECT_START |
                                                READOUTMODULE_FTK_PROTECT_STOP;

    class ReadoutModule_DataFormatter : public ReadoutModule_FTK
    {
    public:

      ReadoutModule_DataFormatter();
      virtual ~ReadoutModule_DataFormatter() noexcept;

      // overloaded methods inherited from ReadoutModule_FTK
      virtual void setup(DFCountedPointer<ROS::Config> configuration) override;
      void doSetup(DFCountedPointer<ROS::Config> configuration);
      virtual void doConfigure    ( const daq::rc::TransitionCmd& ) override;
      virtual void doConnect      ( const daq::rc::TransitionCmd& ) override;
      virtual void doPrepareForRun( const daq::rc::TransitionCmd& ) override;
      virtual void doStopROIB     ( const daq::rc::TransitionCmd& ) override;
      virtual void doStopDC       ( const daq::rc::TransitionCmd& ) override;
      virtual void doStopHLT      ( const daq::rc::TransitionCmd& ) override;
      virtual void doStopRecording( const daq::rc::TransitionCmd& ) override;
      virtual void doStopGathering( const daq::rc::TransitionCmd& ) override;
      virtual void doStopArchiving( const daq::rc::TransitionCmd& ) override;
      virtual void doDisconnect   ( const daq::rc::TransitionCmd& ) override;
      virtual void doUnconfigure  ( const daq::rc::TransitionCmd& ) override;

      virtual void checkConnect( const daq::rc::SubTransitionCmd& ) override;

      virtual void clearInfo() override;

      virtual void doPublish( uint32_t flag, bool finalPublish ) override;
      virtual void doPublishFullStats( uint32_t flag  ) override;
      void checkConnect();

      //virtual DFCountedPointer < Config > getInfo();
      virtual const std::vector<ROS::DataChannel *> *channels() override; 

      //void SlinkClean();

      inline const std::shared_ptr<FtkDataFormatterApi> get_dfApi()	{return m_dfApi;};

      // make spybuffers available to emon; currently just first active input spy buffer of DF
      void shipSpyBuffer(std::vector< std::shared_ptr< DFSpyBuffer > > & vecSpyBuffers, daq::ftk::BoardType boardType);

      // return the name of the ReadoutModule for ftkcommon ERS_LOG
      std::string name_ftk() { return m_name; }

    protected:
      // compare module ids in two vectors
      bool compareModuleIDs(std::vector<uint32_t> vec_IDs_sb, std::vector<uint32_t> vec_IDs_config);

      /// RC User command transition
      virtual void doUser(const daq::rc::UserCmd& cmd);
      
    private:


      std::vector<ROS::DataChannel *>     m_dataChannels;   
      DFCountedPointer<ROS::Config>       m_configuration;	 /**< Configuration Object, Map Wrapper */
      std::string                         m_isServerName;	 /**< IS Server	*/
      IPCPartition                        m_ipcpartition; 	 /**< Partition	*/
      uint32_t                            m_runNumber;	 /**< Run Number	*/
      bool                                m_dryRun;         ///< By pass IPBus calls
      std::shared_ptr<DataFormatterNamed> m_dfNamed;        ///< Access IS via schema
      std::shared_ptr<OHRootProvider>     m_ohProvider;     ///< Histogram provider
      std::shared_ptr<OHRawProvider<> >   m_ohRawProviderIM;  ///< Alternative Histogram provider to publish histos out of arrays
      std::shared_ptr<OHRawProvider<> >   m_ohRawProviderDF;  ///< Alternative Histogram provider to publish histos out of arrays

      std::string                           m_IPBusRegisterFile;
      RegisterNodeMap                       m_IPBusRegisterMap;

      std::shared_ptr<FtkDataFormatterApi>  m_dfApi;         /* DF API */
      std::string                           m_name;		/**< Card name	*/
      std::string                           m_appName;

      // Variables from OKS 
      std::string     m_configPath;
      std::string     m_dataPath;
      std::string     m_IPBus_connection_file;
      std::string     m_IPBus_transceiver_connection_file;
      std::string     m_IPBus_device_id;
      std::string     m_DF_configuration_file;
      std::string     m_DF_global_configuration_file;
      std::string     m_DF_system_configuration_file;
      std::string     m_DF_board_delay_file;
      std::string     m_IM_modulelist_file;
      std::string     m_DF_multishelf_file;
      std::string     m_IM_LUTs_file;
      std::string     m_IM_PseudoData_file;
      std::string     m_DatabaseFile;

      bool m_DF_build_configuration_OTF;
      bool m_DF_validate_OTF;
      bool m_DF_Write_OTF_Configuration;
      bool m_configuration_done; 

      uint32_t        m_shelf;
      uint32_t        m_slot;
      uint32_t        m_board_number;

      std::time_t    m_startTime;
      
      bool m_IM_build_configuration_OTF;
      std::string m_IM_LUT_prefix;
      std::string m_IM_PseudoData_prefix;
      std::map< std::string, uint32_t > m_IM_OKS_parameters;
      uint32_t m_slink_mode_mask;
      uint32_t m_IM_pseudo_input_rate;// IM pseudo data input rate
      bool     m_IM_pseudo_data_xoff; // IM pseudo data xoff enable or not 
      bool     m_IM_pseudo_initial_delay; // IM pseudo data initial delay enable
      uint32_t        m_useBlockTransfer;
      
      uint32_t        m_maxCyclesWaitedForData;
      uint32_t        m_useAutoDelaySetting; /**< Use DF auto delay setting loop*/
      
      bool m_doDFblockTransfer;
      bool m_doFWCheck;
      uint32_t m_doRedundancy;

      uint32_t m_df_fwVersion; /**< DF stable FW version*/
      uint32_t m_im_fwVersion; /**< IM stable FW version*/


      uint32_t m_b0fTimeoutThreshold;
      uint32_t m_skewTimeoutThresholdMax;
      uint32_t m_skewTimeoutThresholdMin;
      uint32_t m_packetTimeoutThreshold;
      uint32_t m_DF_unknownModuleHeaderWord;
      uint32_t m_DF_moduleWordLimit;
      uint32_t m_DF_boardId;

      bool m_usePseudoData;

      uint32_t m_IPBusEmulatorMode;

      uint64_t m_failOnBadLink;
      uint32_t m_num_good_delay_values;

      // EMon
      daq::ftk::FtkEMonDataOut                *m_ftkemonDataOut;  ///< Store pointer to FtkEMonDataOut module

      uint32_t m_DF_enableInputResync;
      uint32_t m_DF_IM_freezeMode;

      // active input channels
      std::vector<uint32_t> m_isactive;
      uint32_t m_isactive_mask; // same as above, but a bitwise mask
      uint32_t m_istimedout; // mask of channels that are timed out

      uint32_t m_inputPacketErrorLanes; // mask of channels with input packet errors
      uint32_t m_inputPacket8thWordErrorLanes; // mask of channels with input packet 8th word errors
      uint32_t m_unknownModuleHeader;


      // input IM channel number to ROB ID
      std::map<uint32_t, uint32_t> m_IMChToRobId;

      std::unique_ptr<StatusRegisterATCAFactory>  m_statusRegisterATCAFactory;
      std::unique_ptr<DataFlowReaderIM>           m_dfReaderIM;
      std::unique_ptr<DataFlowReaderDF>           m_dfReaderDF;
      std::unique_ptr<DataFormatterHistHandler>   m_histHandler;
      std::unique_ptr<DataFormatterConfigHandler> m_configHandler;

    };

    inline const std::vector<ROS::DataChannel *> *ReadoutModule_DataFormatter::channels()
    {
      return &m_dataChannels;
    }

    static std::atomic<uint32_t> df_rm_DF_enabled_RC;
    static std::atomic<uint32_t> df_rm_DF_enabled_checked;

  } // namespace ftk
} // namespace daq
#endif // READOUT_MODULE_DF_H
 
