#ifndef IMSPYBUFFER
#define IMSPYBUFFER

#include "DataFormatter/FtkIMApi.h"
#include "DataFormatter/DataFormatterConstants.h"
#include "ftkcommon/SpyBuffer.h"

#include <string>
#include <vector>

namespace daq { namespace ftk {

  class IMSpyBuffer : public daq::ftk::SpyBuffer {
  public:
    IMSpyBuffer(  uint32_t lane_id,
                  bool     isInspy,
                  std::shared_ptr<FtkIMApi> im_api,
                  uint32_t useBlockTransfer );
    ~IMSpyBuffer();

    int readSpyStatusRegister( uint32_t& spyStatus );

  protected:

    int readSpyBuffer();

  private:
    int read_spybuffer_blocktransfer();
    int read_spybuffer();

    uint32_t m_lane_id;
    bool m_isInspy;
    std::shared_ptr<FtkIMApi> m_im_api;
    uint32_t m_useBlockTransfer;
  };

}} // namespace daq::ftk

#endif // IMSPYBUFFER
