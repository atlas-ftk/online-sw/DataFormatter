#ifndef __STATUSREGISTERDFDIRECT_H__
#define __STATUSREGISTERDFDIRECT_H__
#include <string>
#include "ftkcommon/StatusRegister/StatusRegister.h"
#include "DataFormatter/FtkDataFormatterApi.h"


namespace daq {
namespace ftk {

   /*! \brief Implementation of the DF direct register access 
    */

  class StatusRegisterDFDirect : public StatusRegister
  {
  public:
    StatusRegisterDFDirect(std::shared_ptr<FtkDataFormatterApi> dfapi, std::string node, srType type);
    StatusRegisterDFDirect(std::shared_ptr<FtkDataFormatterApi> dfapi, uint32_t addr,    srType type);

    ~StatusRegisterDFDirect() {};

    /*! \brief So far , implemetation of 1-by-1 readout
     */
    void readout();

  private:
    std::shared_ptr<FtkDataFormatterApi> m_dfapi;

  };

} // namespace ftk
} // namespace daq

#endif // __STATUSREGISTERDFDIRECT_H__
