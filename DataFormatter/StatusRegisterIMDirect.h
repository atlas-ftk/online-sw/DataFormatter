#ifndef __STATUSREGISTERIMDIRECT_H__
#define __STATUSREGISTERIMDIRECT_H__
#include <string>
#include "ftkcommon/StatusRegister/StatusRegister.h"
#include "DataFormatter/FtkIMApi.h"
#include <vector>


namespace daq {
namespace ftk {

   /*! \brief Implementation of the IM direct register access 
    */

  class StatusRegisterIMDirect : public StatusRegister
  {
  public:
    StatusRegisterIMDirect(std::shared_ptr<FtkIMApi> im, uint32_t fpga, uint32_t address, srType type);
    ~StatusRegisterIMDirect() {};

    /*! \brief So far , implemetation of 1-by-1 readout
     */
    void readout();
    /* void readout_IM(bool IM_use_block_transfer);//maybe temporary */

    /*! \brief Bock-transfer access procedure for IM.
     */
    std::vector<uint32_t> readoutBlockTransfer(uint32_t blockSize = 1);


  private:
    std::shared_ptr<FtkIMApi> m_im;
    uint32_t m_fpga;

  };

} // namespace ftk
} // namespace daq

#endif // __STATUSREGISTERIMDIRECT_H__
