// written by yasuyuki.okumura@cern.ch 

#ifndef __FTKIPBUSAPI_HH__
#define __FTKIPBUSAPI_HH__

#include "DataFormatter/DataFormatterIPBusRegisterMap.h"
#include "uhal/uhal.hpp"
#include "uhal/ClientFactory.hpp"
#include <uhal/log/log.hpp>
#include <string>
#include <stdint.h>
#include <vector>
#include <map>

namespace daq { namespace ftk {

class FtkIPBusApi {
 public:
  FtkIPBusApi(const std::string& connection_file, 
		const std::string& device_id);
  FtkIPBusApi(const std::string& connection_file, 
		const std::string& device_id,
    const RegisterNodeMap& map);
  ~FtkIPBusApi(){ 
                  if ( m_manager ) delete m_manager;
                  if ( m_hw ) delete m_hw; 
                };
 private:
  uhal::ConnectionManager* m_manager;
  uhal::HwInterface*       m_hw;  
  const std::string m_connection_file; 
  const std::string m_device_id;
  uint32_t m_timeout_period;
  void connection_inst();

  bool m_checkRegisterMap;
  std::map<std::string, uint32_t> m_registerAddressMap;
  
  // map : register name to value
  std::map<std::string, uint32_t> m_write_register_buffer;
  std::map<std::string, uint32_t> m_read_register_buffer;
  
  static const uint32_t m_MaxSize;
  
 public:
  uhal::HwInterface* get_hardware()	{return m_hw;};
  std::string get_uri()			{return m_hw->uri();};
  std::string get_connection_file()	{return m_connection_file; };
  std::string get_device_id()		{return m_device_id;};
  void set_timeout_period(const uint32_t& value);
  uint32_t get_timeout_period()		{return m_timeout_period;};
  
  void clear_write_register_buffer()	{m_write_register_buffer.clear();};
  void clear_read_register_buffer()	{m_read_register_buffer.clear();};
  void send_transaction_to_write();
  void send_transaction_to_read();
  void fill_write_node_for_IM_BT();

  void debug() {uhal::setLogLevelTo (uhal::Debug());};
  
  bool add_register_to_write(std::string nodename, uint32_t value);
  bool add_register_to_read (std::string nodename);
  uint32_t get_readback_register_value(std::string nodename);
  void single_access_write(const std::string& nodename, 
			   const uint32_t& value);
  void single_access_write2(const std::string& nodename, 
			    const uint32_t& value);
  bool single_access_read(const std::string& nodename, 
			  uint32_t& read_value);
  bool single_access_read2(const std::string& nodename, 
			   uint32_t& read_value);
  bool single_access_block_read(const std::string& nodename, 
				std::vector<uint32_t>& read_value, uint32_t size);
  bool read_32bit_counter(const uint32_t& type_id,
			  const uint32_t& lane_id, 
			  uint32_t& dout);

  bool read_using_selector(const std::vector<std::string>& in_node, const std::vector<uint32_t>& din,
			   const std::string& out_node,  uint32_t& dout); 

  bool read_using_selector(const std::string& in_node , const uint32_t& din,
			   const std::string& out_node,  uint32_t& dout);

  std::string getRegisterNameFromAddressAndMask(const uint32_t& addr, const uint32_t& mask = 0xffffffff);
  std::string getRegisterNameFromAddress       (const uint32_t& addr);
};

} }

#endif
