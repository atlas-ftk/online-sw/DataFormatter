#ifndef __FTKDATAFORMATTERMODULE__
#define __FTKDATAFORMATTERMODULE__

#include <functional>

#include "DataFormatter/FtkIPBusApi.h"
#include "DataFormatter/FtkIMApi.h"
#include "DataFormatter/DataFormatterConstants.h"
#include "DataFormatter/DataFormatterIPBusRegisterMap.h"

using namespace std;

namespace daq { namespace ftk {

typedef map<pair<uint32_t, uint32_t>, uint32_t> map3_t;
typedef map<uint32_t, uint32_t>                 map2_t;

class FtkDataFormatterApi {
 public:
  FtkDataFormatterApi(const string& connection_file, 
		      const string& device_id, const std::string& name);
  FtkDataFormatterApi(const string& connection_file, 
		      const string& device_id, const std::string& name,
          const RegisterNodeMap& registerMap);
  ~FtkDataFormatterApi();

  std::shared_ptr<FtkIMApi> getIMApi()	{ return m_im; }

  std::string name_ftk() { return m_name; }

  // public wrappers for IPBusApi functions
  std::string getRegisterNameFromAddress( const uint32_t& addr );
  bool single_access_read( const std::string& nodename, uint32_t& read_value );
  bool single_access_block_read( const std::string& nodename, std::vector<uint32_t>& read_value,
                                 uint32_t size );
  bool read_using_selector( const std::vector<std::string>& in_node,
                            const std::vector<uint32_t>& din,
                            const std::string& out_node,
                            uint32_t& dout );
  bool read_using_selector( const std::string& in_node, const uint32_t& din,
                            const std::string& out_node, uint32_t& dout );
  void single_access_write( const std::string& nodename, const uint32_t& value );

  void setDFFWVersionFromBoard(); // set m_df_fwVersion by reading ipbus register
  void getEnableFMCLanesMask( uint32_t& enable_fmc_lanes_mask ); // by reading the register

  bool setConfigRegisters( const uint32_t& maxCyclesWaitedForData,
                           const uint32_t& b0fTimeoutThreshold,
                           const uint32_t& skewTimeoutThresholdMax,
                           const uint32_t& skewTimeoutThresholdMin,
                           const uint32_t& packetTimeoutThreshold,
                           const uint32_t& DF_IM_freezeMode,
                           const uint32_t& DF_enableInputResync,
                           const uint32_t& DF_unknownModHeaderWord,
                           const uint32_t& DF_modWordLimit,
                           const uint32_t& DF_boardId,
                           const uint32_t& DF_enableFMCLanesMask,
                           const uint32_t& boardMaskIDO,
                           const uint32_t& boardMaskILI,
                           const uint32_t& slinkMode,
                           const uint32_t& useBlockTransfer );

  void getCurrentL1IDInput( std::vector<uint32_t>& vec );
  void getGlobalL1ID( uint32_t& l1id );
  void getInputSkew(  uint32_t& skew );
  void getTimedOutLinks( uint32_t& skewTimedOut, uint32_t& packetTimedOut );
  void getModHeaderErrors( uint32_t& unknownModHeaderSeen, uint32_t& duplicateModHeaderSeen );
  void getPacketL1IDErrors( uint32_t& packetError, uint32_t& synchDiscard );
  void getInputLaneStatus( const uint32_t& lane,
                           uint32_t& l1id,
                           uint32_t& errorword,
                           uint32_t& numPacketsDiscarded,
                           uint32_t& numModsHitWordLimit );
  void inputMonitoring( const uint32_t& activeMask, const std::map<uint32_t, uint32_t>& IMChToRobId );

  static int getBoardNumberFromShelfSlot(int shelf, int slot) {
    return (shelf-1) + 4*(slot - 3);
  }

  bool standardReset( bool fast, uint32_t step,
                      const std::map<uint32_t, uint32_t>& gtch2rxpolarity,
                      const std::map<uint32_t, uint32_t>& gtch2txpolarity,
                      const std::map<uint32_t, uint32_t>& gtch2force_ready_mode,
                      const std::map<uint32_t, uint32_t>& gtch2to_altera_fpga,
                      const std::map<uint32_t, uint32_t>& gtch_ignore_freeze,
                      const std::map<std::pair<uint32_t, uint32_t>, bool>& multishelf_enable,
                      uint32_t rxports );

  bool configureFMC( uint32_t enableFMCLanesMask, 
                     uint32_t boardMaskIDO,
                     uint32_t boardMaskILI );

  bool configureExpectedNumModules( const std::map<uint32_t, uint32_t>& FMCIn2NumModules,
                                    const std::map<uint32_t, uint32_t>& SLINKOut2NumModules );

  bool internalLinkMaskConfig( const std::map<uint32_t, uint32_t>& laneId2DestMaskMap );

  bool setLinkURL( uint32_t shelf, uint32_t slot );

  bool phase_scan_all_ch( bool, std::vector<uint32_t>&, const uint32_t& numGoodDelayValues );

  void setClockPhaseConfiguration( const std::vector<uint32_t>& );

  void stopAcceptingData( bool usePseudoData, uint32_t useBlockTransfer );
  void clearFIFOs( std::function<void()> quickReset );
  void clearMonitoring();
  void checkFIFOsCleared();

  void gt_configuration( bool force_ready,
                         const std::map<uint32_t, uint32_t>& gtch2rxpolarity,
                         const std::map<uint32_t, uint32_t>& gtch2txpolarity,
                         const std::map<uint32_t, uint32_t>& gtch2force_ready_mode,
                         const std::map<uint32_t, uint32_t>& gtch2to_altera_fpga,
                         const std::map<uint32_t, uint32_t>& gtch_ignore_freeze,
                         const std::map<std::pair<uint32_t, uint32_t>, bool>& multishelf_enable,
                         uint32_t rxports );

  void setTxRxLine(const uint32_t& valTx,
                   const uint32_t& valRx);
  void setTxRxLineBert(const uint32_t& valTx,
                       const uint32_t& valRx);
  void gt_configuration(const uint32_t& gt_id,
			const uint32_t& gt_rxpolarity,
			const uint32_t& gt_txpolarity,
			const uint32_t& force_ready_mode,
			const uint32_t& to_altera_fpga,
			const uint32_t& ignore_freeze);
  void gt_monitor(const uint32_t& gt_id,
		  uint32_t& gt_rxpolarity,
		  uint32_t& gt_txpolarity,
		  uint32_t& force_ready_mode,
		  uint32_t& to_altera_fpga,
		  uint32_t& gt_rxbyteisaligned,
		  uint32_t& gt_tx_reset_done,
		  uint32_t& gt_rx_reset_done,
		  uint32_t& gt_pll_lock,
		  uint32_t& slink_testled_n,
		  uint32_t& slink_lderrled_n,
		  uint32_t& slink_lupled_n,
		  uint32_t& slink_flowctrlled_n,
		  uint32_t& slink_activityled_n,
		  uint32_t& slink_lrl
		  );

  void getb0fSkew( uint32_t& b0fSkew );
  void getTimedOutLinks( uint32_t& timedOut );
  void getTotalEventsGivenUp( const uint32_t& gt_id, uint32_t& nEvtsGivenUp );
  void getInputPacketErrorLinks( uint32_t& inputPacketErrorLinks, uint32_t& inputPacket8thWordErrorLinks );
  void getInputLaneMonitoringCounts( const uint32_t& lane,
                                     uint32_t& b0f_timeout_count,
                                     uint32_t& l1id_timeout_count,
                                     uint32_t& word_limit_hit_count,
                                     uint32_t& packet_structure_error_count,
                                     uint32_t& packet_8th_word_error_count );
  void getUnknownModuleHeaderLinks( uint32_t& unknownModHeader );

  void gtrxtx_reset();
  void pll_reset();
  void gt_reset();

  void spy_reset();
  void spy_restart();
  void spy_freeze();
  void spy_read_enable();
  void spy_read_disable();

  bool spy_dump_individual_lane(const uint32_t& lane_id, vector<uint32_t>& dout);
  bool spy_dump_individual_lane_block(const uint32_t& lane_id,
				      std::vector<uint32_t>& dout);
  
  int read_spybuffer_blocktransfer(const uint32_t& spybuffer_length, const uint32_t& lane_id, std::vector<uint32_t>& dout);
  int read_spybuffer_blocktransfer_test(const uint32_t& spybuffer_length, const uint32_t& lane_id, std::vector<uint32_t>& dout);
  
  bool read_32bit_counter(const uint32_t& type_id, const uint32_t& lane_id, uint32_t& dout);

  void write_lut(const uint32_t& lut_type_id,
		 const uint32_t& lut_addr,
		 const uint32_t& lut_data,
		 const uint32_t& lut_lane_enable_mask);
  void write_lut(const uint32_t& lut_addr,
		 const uint32_t& lut_data,
		 const uint32_t& lut_lane_enable_mask);
  bool read_lut(const uint32_t& lut_type_id,
		const uint32_t& lut_addr,
		const uint32_t& lut_lane_id,
		uint32_t& lut_data);

  bool doReadCheck(const std::string& nodename,uint32_t& reg); /** do try/except lazily*/


  bool read_switch_fifo_status(const uint32_t& switch_type, // so far only output switch to AUX/SSB is monitored
			       const uint32_t& element_row,
			       const uint32_t& element_column,
			       uint32_t& input1_input_empty,
			       uint32_t& input1_input_full,
			       uint32_t& input1_output1_rdy,
			       uint32_t& input1_output2_rdy,
			       uint32_t& input1_output1_being_sent,
			       uint32_t& input1_output2_being_sent,
			       uint32_t& input2_input_empty,
			       uint32_t& input2_input_full,
			       uint32_t& input2_output1_rdy,
			       uint32_t& input2_output2_rdy,
			       uint32_t& input2_output1_being_sent,
			       uint32_t& input2_output2_being_sent,
			       uint32_t& output1_input1_empty,
			       uint32_t& output1_input2_empty,
			       uint32_t& output1_input1_full,
			       uint32_t& output1_input2_full,
			       uint32_t& output1_input1_being_read,
			       uint32_t& output1_input2_being_read,
			       uint32_t& output1_output_rdy,
			       uint32_t& output2_input1_empty,
			       uint32_t& output2_input2_empty,
			       uint32_t& output2_input1_full,
			       uint32_t& output2_input2_full,
			       uint32_t& output2_input1_being_read,
			       uint32_t& output2_input2_being_read,
			       uint32_t& output2_output_rdy
			       );

  bool read_switch_fifo_fullness(const uint32_t& switch_type, // so far only output switch to AUX/SSB is monitored
				 const uint32_t& element_row,
				 const uint32_t& element_column,
				 uint32_t& input1_input_full,
				 uint32_t& input2_input_full,
				 uint32_t& output1_input1_full,
				 uint32_t& output1_input2_full,
				 uint32_t& output2_input1_full,
				 uint32_t& output2_input2_full
				 );

  bool read_switch_fifo_emptiness(const uint32_t& switch_type, // so far only output switch to AUX/SSB is monitored
				  const uint32_t& element_row,
				  const uint32_t& element_column,
				  uint32_t& input1_input_empty,
				  uint32_t& input2_input_empty,
				  uint32_t& output1_input1_empty,
				  uint32_t& output1_input2_empty,
				  uint32_t& output2_input1_empty,
				  uint32_t& output2_input2_empty
				  );
  bool read_fifo_full_counter();
  bool get_fw_version();
  void printFwVersion( bool useBlockTransfer = true );


  // check module ids in two vectors
  uint32_t checkModuleIDs( std::vector<uint32_t>& vec_IDs_sb, uint32_t channel, 
                           const std::map<std::pair<uint32_t, uint32_t>, uint32_t>& lane_idx2mod );	
  uint32_t checkModuleIDsSize( std::vector<uint32_t>& vec_IDs_sb, uint32_t channel,
                           const std::map<std::pair<uint32_t, uint32_t>, uint32_t>& lane_idx2mod );
  uint32_t checkModuleIDsDuplicates( std::vector<uint32_t>& vec_IDs_sb, uint32_t channel);
  uint32_t checkModuleIDsConsistency( std::vector<uint32_t>& vec_IDs_sb, uint32_t channel,
                          const std::map<std::pair<uint32_t, uint32_t>, uint32_t>& lane_idx2mod );

  inline void setShelfAndSlot(const uint32_t shelf, const uint32_t slot)
    { m_shelf=shelf; m_slot=slot; m_im->setShelfAndSlot( shelf, slot ); }
  inline void setName( const std::string& name )
    { m_name = name; m_im->setName( name ); }
  inline void setFwVersions( bool doFWCheck, 
                             uint32_t df_fwVersion,
                             uint32_t im_fwVersion){
    m_doFWCheck=doFWCheck;m_df_fwVersion=df_fwVersion; m_im_fwVersion=im_fwVersion; }

//======================================================================
  bool usedOutputLane(const int& iOutLane, 
		      const bool& UsedInTopTower, 
		      const bool& UsedInBotTower, 
		      const int& UsedDFOutputLane);

  void lut_config_helper(const uint32_t& lut_type,
                                const map2_t lane,
                                const uint32_t& lane_mask);
  void lut_config_helper(const uint32_t& lut_type,
                                const map3_t lane);

  void clock_phase_configuration( const uint32_t& im_fpga_id,
                                  const uint32_t& inv_configuration,
                                  const uint32_t& delay_configuration );

///////////////////////////////////////////////////////////////////
////////////// Private ////////////////////////////////////////////
///////////////////////////////////////////////////////////////////

 private:

  std::string m_name; // name of the DF board
  std::shared_ptr<FtkIPBusApi> m_ipbus;
  std::shared_ptr<FtkIMApi>    m_im;

  uint32_t m_shelf;
  uint32_t m_slot;

  std::vector<uint32_t> m_goodvalues;

  // DF Lane names
  const string SpyLaneName[58] = {"LSC Ch00",
			     "LSC Ch01",
			     "LSC Ch02",
			     "LSC Ch03",
			     "LSC Ch04",
			     "LSC Ch05",
			     "LSC Ch06",
			     "LSC Ch07",
			     "LSC Ch08",
			     "LSC Ch09",
			     "LSC Ch10",
			     "LSC Ch11",
			     "LSC Ch12",
			     "LSC Ch13",
			     "LSC Ch14",
			     "LSC Ch15",
			     "LSC Ch16",
			     "LSC Ch17",
			     "LSC Ch18",
			     "LSC Ch19",
			     "LSC Ch20",
			     "LSC Ch21",
			     "LSC Ch22",
			     "LSC Ch23",
			     "LSC Ch24",
			     "LSC Ch25",
			     "LSC Ch26",
			     "LSC Ch27",
			     "LSC Ch28",
			     "LSC Ch29",
			     "LSC Ch30",
			     "LSC Ch31",
			     "LSC Ch32",
			     "LSC Ch33",
			     "INPUT Ch00",
			     "INPUT Ch01",
			     "INPUT Ch02",
			     "INPUT Ch03",
			     "INPUT Ch04",
			     "INPUT Ch05",
			     "INPUT Ch06",
			     "INPUT Ch07",
			     "INPUT Ch08",
			     "INPUT Ch09",
			     "INPUT Ch10",
			     "INPUT Ch11",
			     "INPUT Ch12",
			     "INPUT Ch13",
			     "INPUT Ch14",
			     "INPUT Ch15",
			     "FB0 FPGA0 RAW",
			     "FB0 FPGA1 RAW",
			     "FB1 FPGA0 RAW",
			     "FB1 FPGA1 RAW",
			     "FB2 FPGA0 RAW",
			     "FB2 FPGA1 RAW",
			     "FB3 FPGA0 RAW",
			     "FB3 FPGA1 RAW"};


  bool m_doFWCheck; /**< do FW check and throw error if fails*/
  uint32_t m_df_fwVersion; /**< DF stable FW version*/
  uint32_t m_im_fwVersion; /**< IM stable FW version*/

  bool check(uint32_t first, uint32_t second, std::string name);
  bool check(map2_t first, map2_t second, std::string name);
  bool check(map3_t first, map3_t second, std::string name);

  void expected_nummodules_information_configuration(const uint32_t& lane_id,
                                                     const uint32_t& update_mask,
                                                     const uint32_t& value);
  void internal_link_mask_configuration( const uint32_t& central_switch_output_lane_id,
                                         const uint32_t& mask_pattern );

};

} }

#endif 
