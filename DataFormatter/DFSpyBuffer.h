#ifndef DFSPYBUFER
#define DFSPYBUFER

#include "DataFormatter/FtkDataFormatterApi.h"
#include "ftkcommon/SpyBuffer.h"

/*
 *  Functions used for spy buffer access using DF API
 */

#include <string>
#include <vector>

// not yet possible to get status information of DF spy buffers
/*
#define SPY_POINTER_MASK      0x3fff
#define SPY_DIMENSION_MASK    0xffff
#define SPY_OVERFLOW_OFFSET   14
#define SPY_FREEZE_OFFSET     15
#define SPY_DIMENSION_OFFSET  16
*/


namespace daq { 
  namespace ftk {

    class DFSpyBuffer: public daq::ftk::SpyBuffer{
    public:

      /*
       * DF spy buffer
       * lane_id: defined in DF API
       */      
      DFSpyBuffer(uint32_t lane_id,
		  uint32_t spybuffer_length,
		  std::shared_ptr<FtkDataFormatterApi> df_api);
      

      ~DFSpyBuffer();

      int read_spybuffer_blocktransfer(bool reset, uint32_t read_length=0, bool is_print = false);
      int read_spybuffer(bool reset, uint32_t read_length=0);
      int read_individual_spybuffer();
      int read_individual_spybuffer(const uint32_t lane_id );
      int read_individual_lane();
      int read_from_file(std::string filename);

      // rmina 8 March '18 - adding dummy implementation for pure virtual function
      // TODO: to be updated when the DF FW with spybuffer interface is ready
      int readSpyStatusRegister( uint32_t& spyStatus ) { return 0; }

      uint32_t getSpyFreeze() {return m_spyFreeze;};
      bool getSpyOverflow() {return m_spyOverflow;};
      unsigned int getSpyPointer() {return m_spyPointer;};
      unsigned int getSpyDimension() {return m_spyDimension;};

    protected:

      void parse_spybuffer_status();
 
      // rmina 8 March '18 - adding dummy implementation for pure virtual function
      // TODO: to be updated when the DF FW with spybuffer interface is ready
      int readSpyBuffer() { return 0; }   

    private:
      
      unsigned int m_spy_status_reg_DF_address;
      unsigned int m_spy_RAM_DF_base_address;


      // input
      uint32_t m_lane_id;
      uint32_t m_spybuffer_length; 
      std::shared_ptr<FtkDataFormatterApi> m_df_api;

      // address for fpga_id
      uint32_t m_address;
      // address of spy buffer state
      uint32_t m_spybuffer_state_address;

      //status information
      bool m_spyOverflow = false;
      uint32_t m_spyFreeze = 0x0;
      unsigned int m_spyPointer;
      unsigned int m_spyDimension;
      
      // temporary store of 32bit register contents
      uint32_t m_register;
    };

  } // namespace ftk
} // namespace daq

#endif
