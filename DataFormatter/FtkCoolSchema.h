#ifndef FTKCONFCOOL_COOLSCHEMA
#define FTKCONFCOOL_COOLSCHEMA

#include <map>
#include <memory>
#include <string>
#include <cstdint>

#include "CoolKernel/PayloadMode.h"
#include "CoolKernel/RecordSpecification.h"
#include "CoolKernel/ChannelId.h"
#include "CoolKernel/pointers.h"

#include "boost/function.hpp"
typedef boost::function<int (const cool::IRecordPtr &)> FolderHashFunction;

namespace FtkCoolSchema {
  
  typedef unsigned char uchar_t;

  inline std::string coolFolderName(std::string tableName) {
    std::string coolFolderPath = "/FtkLUT"; 
    return coolFolderPath + "/" + tableName;
  }

  class Folder {
  public:
  Folder(std::string n, cool::ChannelId i = 0, cool::PayloadMode::Mode m = cool::PayloadMode::VECTORPAYLOAD): name(n), id(i), mode(m) {}
    virtual std::shared_ptr<cool::RecordSpecification> spec() = 0;
    std::string name;
    cool::ChannelId id;
    cool::PayloadMode::Mode mode;

  protected:
    std::shared_ptr<cool::RecordSpecification> _spec;
  };

  std::map<std::string, std::shared_ptr<Folder> > tables();

  std::map<std::string, FolderHashFunction> hashes();

  class ROB : public Folder {
  public:
    ROB();
    virtual ~ROB() {}
    std::shared_ptr<cool::RecordSpecification> spec();
  };

  class Module : public Folder {
  public:
    Module();
    virtual ~Module() {}
    std::shared_ptr<cool::RecordSpecification> spec();
  };
  
  class IM : public Folder {
  public:
    IM();
    virtual ~IM() {}
    std::shared_ptr<cool::RecordSpecification> spec();
  };
    
  class DF : public Folder {
  public: 
    DF();
    virtual ~DF() {}
    std::shared_ptr<cool::RecordSpecification> spec();
  };
  
}

#endif
