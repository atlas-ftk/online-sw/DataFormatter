#ifndef __STATUSREGISTERDFCOUNTER_H__
#define __STATUSREGISTERDFCOUNTER_H__
#include <string>
#include "DataFormatter/StatusRegisterDFSelector.h"


namespace daq {
namespace ftk {

   /*! \brief Implementation of the DF counter access. Inherits from DFDelector.
    */

  class StatusRegisterDFCounter : public StatusRegisterDFSelector
  {
  public:
    /*! \brief Constructor
     *  \param valueSelector 32-bit word 0xXXXXYYYY, where XXXX is the type of counter and YYYY is the lane id
     */
    StatusRegisterDFCounter(std::shared_ptr<FtkDataFormatterApi> dfapi, const srType& type, const uint32_t& valueSelector);
    /*! \brief Constructor
     *  \param typeSelector 3-bit  word defining type of counter
     *  \param laneSelector 16-bit word defining lane id
     */
    StatusRegisterDFCounter(std::shared_ptr<FtkDataFormatterApi> dfapi, const srType& type, const uint32_t& typeSelector, const uint32_t& laneSelector);
    ~StatusRegisterDFCounter() {};

  private:
    void initialize_selector(const uint32_t& typeSelector, const uint32_t& laneSelector);

  };

} // namespace ftk
} // namespace daq

#endif // __STATUSREGISTERDFCOUNTER_H__
