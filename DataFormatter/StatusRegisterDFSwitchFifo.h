#ifndef __STATUSREGISTERDFSWITCHFIFO_H__
#define __STATUSREGISTERDFSWITCHFIFO_H__
#include <string>
#include "DataFormatter/StatusRegisterDFSelector.h"
#include "DataFormatter/FtkDataFormatterApi.h"


namespace daq {
namespace ftk {

   /*! \brief Implementation of the DF switch fifo access. Inherits from DFDelector.
    */

  class StatusRegisterDFSwitchFifo : public StatusRegisterDFSelector
  {
  public:
    /*! \brief Constructor
     *  \param valueSelector 12-bit word iiiijjjjkkkk, where iiii is the type of switch,
     *  jjjj is the column in the matrix and kkkk is the row in the matrix
     */
    StatusRegisterDFSwitchFifo(std::shared_ptr<FtkDataFormatterApi> dfIPBus, const srType& type, const uint32_t& valueSelector);
    /*! \brief Constructor
     *  \param typeSelector the type of switch
     *  \param columnSelector the column in the matrix
     *  \param rowSelector the row in the matrix
     */
    StatusRegisterDFSwitchFifo(std::shared_ptr<FtkDataFormatterApi> dfapi, const srType& type, 
				const uint32_t& typeSelector, const uint32_t& columnSelector, const uint32_t& rowSelector);
    ~StatusRegisterDFSwitchFifo() {};

  private:
    void initialize_selector(const uint32_t& typeSelector, const uint32_t& columnSelector, const uint32_t& rowSelector);

  };

} // namespace ftk
} // namespace daq

#endif // __STATUSREGISTERDFSWITCHFIFO_H__
