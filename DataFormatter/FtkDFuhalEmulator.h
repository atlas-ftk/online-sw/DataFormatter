#ifndef __FTKDFUHALEMULATOR__
#define __FTKDFUHALEMULATOR__

#include "uhal/log/log.hpp"
#include "uhal/ProtocolIPbus.hpp"
#include "uhal/tests/DummyHardware.hpp"
#include "uhal/IPbusInspector.hpp"
// see uhal/tests/UDPDummyHardware.hpp
// and uhal/IPbusInspector.hpp

#include <boost/asio.hpp>

#include "DataFormatter/FtkDataFormatterApi.h"

class FtkDFuhalEmulator : public uhal::tests::DummyHardware< 2, 0 > {
 public:
  FtkDFuhalEmulator ( const uint16_t& aPort, 
                      const uint32_t& aReplyDelay, 
                      const bool& aBigEndianHack, 
                      std::string a_IPBus_connection_file, 
                      std::string a_IPBus_device_id, 
                      uint32_t a_shelf, 
                      uint32_t a_slot, 
                      bool a_doFWCheck, 
                      uint32_t a_df_fwVersion, 
                      uint32_t a_imS6_fwVersion, 
                      uint32_t a_imA7_fwVersion ) :
    DummyHardware< 2, 0 > ( aReplyDelay, aBigEndianHack ) ,
    mReplyDelay ( aReplyDelay ),
    mBigEndianHack ( aBigEndianHack ),
    mIOservice(),
    mSocket ( mIOservice, boost::asio::ip::udp::endpoint ( boost::asio::ip::udp::v4(), aPort ) ),
    m_dfApi ( new FtkDataFormatterApi(a_IPBus_connection_file, a_IPBus_device_id, a_IPBus_device_id) ),
    mHeader ( 0 ),
    mWordCounter ( 0 ),
    mTransactionId ( 0 ),
    mResponseGood ( 0 ),
    mPacketHeader ( 0 ),
    mPacketCounter ( 0 ),
    mPacketType ( 0 )
  {
    m_dfApi->setFwVersions( a_doFWCheck, a_df_fwVersion, a_imS6_fwVersion, a_imA7_fwVersion );

    m_dfApi->setShelfAndSlot(a_shelf, a_slot);
  }

  ~FtkDFuhalEmulator() {}

  void run();

  bool analyze ( std::vector<uint32_t>::const_iterator& aIt, const std::vector<uint32_t>::const_iterator& aEnd, const bool& aContinueOnError = true );
  void AnalyzeReceivedAndCreateReply ( const uint32_t& aByteCount );

  static const uint32_t IPbus_major = 2;
  static const uint32_t IPbus_minor = 0;

 private:
  uint32_t mReplyDelay;
  bool mBigEndianHack;

  boost::asio::io_service mIOservice;
  boost::asio::ip::udp::socket mSocket;
  boost::asio::ip::udp::endpoint mSenderEndpoint;

  FtkDataFormatterApi *m_dfApi;

  uint32_t mHeader;
  uhal::eIPbusTransactionType mType;
  uint32_t mWordCounter;
  uint32_t mTransactionId;
  uint8_t mResponseGood;

  uint32_t mPacketHeader;
  uint32_t mPacketCounter;
  uint32_t mPacketType;

  void bot(); // byte-ordertransaction
  void ni_read( const uint32_t& aAddress ); // non-incrementing read
  void read ( const uint32_t& aAddress ); // incrementing read
  void ni_write ( const uint32_t& aAddress, std::vector<uint32_t>::const_iterator& aIt, const std::vector<uint32_t>::const_iterator& aEnd ); // non-incrementing write
  void write ( const uint32_t& aAddress, std::vector<uint32_t>::const_iterator& aIt, const std::vector<uint32_t>::const_iterator& aEnd ); // incrementing write
  void rmw_sum ( const uint32_t& aAddress, const uint32_t& aAddend ); // read-modify-write sum
  void rmw_bits ( const uint32_t& aAddress, const uint32_t& aAndTerm, const uint32_t& aOrTerm ); // read-modify-write bits
  void unknown_type ();
  bool control_packet_header ();
  void status_packet_header ();
  void resend_packet_header ();
  void unkown_packet_header ();
};

#endif // __FTKDFUHALEMULATOR
