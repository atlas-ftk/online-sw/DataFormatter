#ifndef DATAFLOWREADERDF_H
#define DATAFLOWREADERDF_H

#include "ftkcommon/DataFlowReader.h"
#include "DataFormatter/dal/FtkDataFlowSummaryDFNamed.h"
#include <string>
#include <vector>
#include <memory>
#include <cstdint>

// Namespaces
namespace daq {
namespace ftk {

/*! \brief  DataFormatter implementation of DataFlowReader
*/
class DataFlowReaderDF : public DataFlowReader
{
public:
    /*! \brief Constructor: sets the input scheme (IS inputs or vectors)
     *  \param ohRawProvider Pointer to initialized  OHRawProvider object
     *  \param forceIS Force the code to use IS as the source of information (default is 0)
     */
    DataFlowReaderDF(std::shared_ptr<OHRawProvider<> > ohRawProvider, uint32_t m_df_fwVersion, bool forceIS = false);
    
    void init(const string& deviceName,
		const std::string& partitionName = "FTK",
		const std::string& isServerName = "DF");
     
    virtual void getFifoInBusy(std::vector<int64_t>& srv) override;

    virtual void getFifoOutBusy(std::vector<int64_t>& srv) override;

    virtual void getFifoInEmpty(std::vector<int64_t>& srv) override;

    virtual void getFifoOutEmpty(std::vector<int64_t>& srv) override;

    virtual void getFMCInCount(std::vector<int64_t>& srv);

    virtual void getSLINKOutCount(std::vector<int64_t>& srv);

    virtual void getFMCInXOff(std::vector<int64_t>& srv);

    virtual void getSLINKOutXOff(std::vector<int64_t>& srv);

    virtual void getSLINKOutEventsGivenUp(std::vector<int64_t>& srv);
    
    virtual void getSLINKOutModsGivenUp(std::vector<int64_t>& srv);
    
    virtual void getSLINKOutEvtSortBuff(std::vector<int64_t>& srv);
      
    virtual void getLinkOutStatus(std::vector<int64_t>& srv) override;
    
    virtual void getFifoInBusyFraction(std::vector<float>& srv, uint32_t) override;

    virtual void getFifoOutBusyFraction(std::vector<float>& srv, uint32_t) override;

    virtual void getFifoInEmptyFraction(std::vector<float>& srv, uint32_t) override;

    virtual void getFifoOutEmptyFraction(std::vector<float>& srv, uint32_t) override;

    virtual void getL1id(std::vector<int64_t>& srv) override;

    virtual void getNPacketsDiscarded(std::vector<int64_t>& srv) override; 
    
    virtual void getNPacketsHeldSync(std::vector<int64_t>& srv) override;
 
    virtual void getNPacketsDiscardedSync(std::vector<int64_t>& srv) override;

    virtual void getNPacketsTimeoutSync(std::vector<int64_t>& srv) override;

    virtual void getNumModulesHitWordLimit(std::vector<int64_t>& srv);
  
    virtual void getInputSkewSync(std::vector<int64_t>& srv) override;
    /*    
    virtual void getSkewOutHist(std::vector<int64_t>& srv);

    virtual void getPHtimeoutHist(std::vector<int64_t>& srv);
    
    virtual void getGlobalL1SkipHist(std::vector<int64_t>& srv);
    
    virtual void getLaneL1SkipHist(std::vector<int64_t>& srv);
    */

    virtual void getFMCInFifoStatus(std::vector<int64_t>& srv);

    virtual void getFMCInFrontFifoStatus(std::vector<int64_t>& srv);

    virtual void getIntLinkRXFifoStatus(std::vector<int64_t>& srv);

    virtual void getIntLinkTXFifoStatus(std::vector<int64_t>& srv);

    virtual void getSlinkCh0to17FifoStatus(std::vector<int64_t>& srv);

    virtual void getSlinkCh18to35FifoStatus(std::vector<int64_t>& srv);

    virtual void getEvtSortBuffCh0to17FifoStatus(std::vector<int64_t>& srv);

    virtual void getEvtSortBuffCh18to35FifoStatus(std::vector<int64_t>& srv);
    
    virtual void getFMCINBackPressure(std::vector<int64_t>& srv);

    virtual void getILIBackPressure(std::vector<int64_t>& srv);

    virtual void getODOBackPressure(std::vector<int64_t>& srv);

    virtual void getILOBackPressure(std::vector<int64_t>& srv);
    
    virtual void getInputGlobalFreezeCount(std::vector<int64_t>& srv);

    virtual void getSLINKLDCFreezeCount(std::vector<int64_t>& srv);
    
    virtual void getFMCOUTtoIMFreezeCount(std::vector<int64_t>& srv);
    
    virtual void getIDOClustersPerEvt(std::vector<int64_t>& srv);
    
    virtual void getILIClustersPerEvt(std::vector<int64_t>& srv);
    
    virtual void getODOClustersPerEvt(std::vector<int64_t>& srv);
    
    virtual void getILOClustersPerEvt(std::vector<int64_t>& srv);
    
    virtual void getB0F2B0FoverB0F2E0F(std::vector<int64_t>& srv);
    
    virtual void publishExtraHistos(uint32_t option = 0) override;
     
    uint32_t ParseInput(uint8_t temp);
    
  

private:
    std::vector<std::string> getISVectorNames();
    uint32_t m_df_fwVersion;
    const std::string& partitionName = "FTK";
    std::shared_ptr<FtkDataFlowSummaryDFNamed> m_theDFSummary;



private:

};

} // namespace ftk
} // namespace daq

#endif /* DATAFLOWREADERDF_H */
