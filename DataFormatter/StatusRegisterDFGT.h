#ifndef __STATUSREGISTERDFGT_H__
#define __STATUSREGISTERDFGT_H__
#include <string>
#include "DataFormatter/StatusRegisterDFSelector.h"
#include "DataFormatter/FtkDataFormatterApi.h"


namespace daq {
namespace ftk {

   /*! \brief Implementation of the DF counter access. Inherits from DFDelector.
    */

  class StatusRegisterDFGT : public StatusRegisterDFSelector
  {
  public:
    /*! \brief Constructor  Register for GT channel monitoring
     *  \param laneSelector Lane id according to internal DF numbering (SpyLaneName, 0-57)
     *  \param reg          Name of the register to be read out
     */
    StatusRegisterDFGT(std::shared_ptr<FtkDataFormatterApi> dfapi, const srType& type, const uint32_t& laneSelector, const std::string& reg);
    /*! \brief Constructor  Register for GT channel monitoring
     *  \param laneSelector Lane id according to internal DF numbering (SpyLaneName, 0-57)
     *  \param addr         Address of the register to be read out
     */
    StatusRegisterDFGT(std::shared_ptr<FtkDataFormatterApi> dfapi, const srType& type, const uint32_t& laneSelector, const uint32_t& addr);

    ~StatusRegisterDFGT() {};

  private:
    void initialize_selector(const uint32_t& laneSelector, const std::string& reg);

  };

} // namespace ftk
} // namespace daq

#endif // __STATUSREGISTERDFGT_H__
