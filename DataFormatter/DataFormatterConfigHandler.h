#ifndef DATAFORMATTERCONFIGHANDLER_H
#define DATAFORMATTERCONFIGHANDLER_H

#include <vector>
#include <map>
#include <iostream>
#include <sstream>
#include <fstream>
#include <atomic>

#include "ers/ers.h"

#include "ftkcommon/core.h"
#include "ftkcommon/exceptions.h"

#include "uhal/Utilities.hpp"

#include "DataFormatter/FtkDataFormatterApi.h"
#include "DataFormatter/DataFormatterConstants.h"

#include "DataFormatter/FtkCoolSchema.h"
#include "CoolApplication/DatabaseSvcFactory.h"
#include "CoolKernel/IDatabaseSvc.h"
#include "CoolKernel/IDatabase.h"
#include "CoolKernel/IFolder.h"
#include "CoolKernel/IObject.h"
#include "CoolKernel/IObjectIterator.h"
#include "CoolKernel/IRecord.h"
#include "CoolKernel/IRecordIterator.h"
#include "CoolKernel/IRecordSpecification.h"
#include "CoolKernel/Record.h"
#include "CoolKernel/pointers.h"
#include "CoolKernel/Exception.h"
#include "CoolKernel/FolderSpecification.h"
#include "CoolKernel/PayloadMode.h"


namespace daq { namespace ftk {

// trim leading whitespace
static inline std::string& ltrim( std::string &s ) {
  s.erase( s.begin(), std::find_if(s.begin(), s.end(), [](int c) { return !std::isspace(c); }) );
  return s;
}

class DataFormatterConfigHandler {
 public:
  DataFormatterConfigHandler( const std::string& name,
                              uint32_t shelf,
                              uint32_t slot,
                              uint32_t useBlockTransfer,
                              uint32_t useAutoDelaySetting,
                              bool doConfigOTF,
                              bool IMConfigOTF,
                              bool dumpConfig,
                              bool usePseudoData,
                              const std::string& standardConfigFile,
                              const std::string& globalConfigFile,
                              const std::string& systemConfigFile,
                              const std::string& boardDelayFile,
                              const std::string& coolConnString,
                              const std::string& moduleListFile,
                              const std::string& multishelfConfigFile,
                              const std::string& transceiverConfigFile,
                              const std::string& configDumpFile,
                              const std::string& IMLUTPrefix,
                              const std::string& IMLUTsFile,
                              const std::string& IMPseudoDataPrefix,
                              const std::string& IMPseudoDataFile,
                              uint32_t IMPseudoData_InputRate,
                              uint32_t maxCyclesWaitedForData,
                              uint32_t b0fTimeoutThreshold,
                              uint32_t skewTimeoutThresholdMax,
                              uint32_t skewTimeoutThresholdMin,
                              uint32_t packetTimeoutThreshold,
                              uint32_t DF_IM_freezeMode,
                              uint32_t DF_enableInputResync,
                              uint32_t DF_unknownModHeaderWord,
                              uint32_t DF_modWordLimit,
                              uint32_t DF_boardId,
                              uint32_t numGoodDelayValues,
			      uint32_t slinkMode,
                              std::shared_ptr<FtkDataFormatterApi> dfApi );
  ~DataFormatterConfigHandler() noexcept {}

  // check that RC and config file agree
  bool checkRCAgainstConfig( uint32_t DF_enabled_RC, const std::string& globalConfigFile );

  bool configure();
  bool unconfigure();
  bool quickReset();

  bool getRobIdForIMLane( const uint32_t& lane, uint32_t& ROB );

  uint32_t get_rxports() { return m_rxports; }

  std::string name_ftk() { return m_name; }

  const std::map<std::pair<uint32_t, uint32_t>, uint32_t>& getLaneIdx2Mod() 
    { return m_lane_idx2mod; }

  uint32_t getEnableFMCMask() { return m_enableFMCLanesMask; }

 private:
  std::string m_name;
  uint32_t m_shelf;
  uint32_t m_slot;
  uint32_t m_useBlockTransfer;
  uint32_t m_useAutoDelaySetting;
  bool m_doConfigOTF;
  bool m_IMConfigOTF;
  bool m_dumpConfig;
  bool m_usePseudoData;
  std::string m_standardConfigFile;
  std::string m_globalCF;
  std::string m_systemCF;
  std::string m_boardDelayFile;
  std::string m_coolConnString;
  std::string m_moduleListFile;
  std::string m_multishelfCF;
  std::string m_transceiverCF;
  std::string m_configDumpFile;
  std::string m_IMLUTPrefix;
  std::string m_IMLUTsFile;
  std::string m_IMPseudoDataPrefix;
  std::string m_IMPseudoDataFile;
  uint32_t m_IMPseudoData_InputRate;
  uint32_t m_maxCyclesWaitedForData;
  uint32_t m_b0fTimeoutThreshold;
  uint32_t m_skewTimeoutThresholdMax;
  uint32_t m_skewTimeoutThresholdMin;
  uint32_t m_packetTimeoutThreshold;
  uint32_t m_DF_IM_freezeMode;
  uint32_t m_DF_enableInputResync;
  uint32_t m_DF_unknownModHeaderWord;
  uint32_t m_DF_modWordLimit;
  uint32_t m_DF_boardId;
  uint32_t m_slinkMode;
  uint32_t m_numGoodDelayValues;

  std::shared_ptr<FtkDataFormatterApi> m_dfApi;

  uint32_t m_boardNumber;
  uint32_t m_enableFMCLanesMask;

  const uint32_t m_lut_type_mod2idx         = 0;
  const uint32_t m_lut_type_idx2mod         = 1;
  const uint32_t m_lut_type_pixmod2dst      = 2;
  const uint32_t m_lut_type_sctmod2dst      = 3;
  const uint32_t m_lut_type_pixmod2ftkplane = 4;
  const uint32_t m_lut_type_pixmod2tower    = 5;
  const uint32_t m_lut_type_sctmod2ftkplane = 6;
  const uint32_t m_lut_type_sctmod2tower    = 7;

  const bool m_32TowerConfig = false;

  // configured in buildConfiguration
  std::vector<uint32_t> m_module_count_in;
  std::vector<uint32_t> m_module_count_out;
  std::map<uint32_t, uint32_t> m_pixmod2dst_map;
  std::map<uint32_t, uint32_t> m_sctmod2dst_map;
  std::map<uint32_t, uint32_t> m_pixmod2ftkplane_map;
  std::map<uint32_t, uint32_t> m_sctmod2ftkplane_map;
  std::map<uint32_t, uint32_t> m_pixmod2tower_map;
  std::map<uint32_t, uint32_t> m_sctmod2tower_map;
  std::map<uint32_t, uint32_t> m_FMCIn2RODId_map;
  std::map<uint32_t, std::vector<uint32_t> > m_FMCIn2modid_map;
  std::map<uint32_t, uint32_t> m_FMCIn2NumModules;
  std::map<uint32_t, uint32_t> m_SLINKOut2NumModules;
  std::map<std::pair<uint32_t, uint32_t>, uint32_t> m_lane_mod2idx;
  std::map<std::pair<uint32_t, uint32_t>, uint32_t> m_lane_idx2mod;
  std::map<uint32_t, uint32_t> m_centralSwLaneId2DestMask;
  std::vector<uint32_t> m_clockPhaseDelayValues;
  std::map<uint32_t, uint32_t> m_laneToDelayMap;
  std::map<uint32_t, uint32_t> m_laneToInvMap;
  uint32_t m_rxports;
  std::map<uint32_t, uint32_t> m_gtCh2RxPolarity;
  std::map<uint32_t, uint32_t> m_gtCh2TxPolarity;
  std::map<uint32_t, uint32_t> m_gtCh2ForceReadyMode;
  std::map<uint32_t, uint32_t> m_gtCh2ToAlteraFPGA;
  std::map<uint32_t, uint32_t> m_gtCh2IgnoreFreeze;

  bool buildConfiguration();

  bool standardConfiguration();
  bool standardOTFConfiguration();
  bool standardResetConfiguration( bool fast );

  bool clockPhaseConfiguration( bool fast );
  bool phaseScanAllChannels( bool fast );

  bool dumpConfiguration();

  bool downloadAllIMLUT_OTF();
  bool downloadAllIMLUT();

  bool downloadAllPseudoData_OTF();
  bool downloadAllPseudoData();

  bool checkIMDFLanes();
  bool checkIMDFModules();

  bool lutConfiguration();

  // system-wide configuration
  bool parseGlobalConfigFile( std::map<uint32_t, bool>& boardEnableMap,
                      std::multimap<uint32_t, std::pair<uint32_t, uint32_t> >& rodIdToFMCInIdMap,
                      std::map<uint32_t, std::map<uint32_t, uint32_t> >& boardIdToFMCInIdToRODIdMap,
                      std::map<uint32_t, uint32_t>& boardIdToEnableFMCLanesMap,
                      std::map<std::pair<uint32_t, uint32_t>, bool>& mulstishelf_boardEnableMap );

  bool parseSystemConfigFile( uint32_t& numberOfFTKTowers,
                std::map<uint32_t, std::pair<uint32_t, uint32_t> >& DFBoardNumberToShelfAndSlotMap,
                std::map<uint32_t, uint32_t>& FTKPlaneToDFOutputMap,
                std::map<uint32_t, uint32_t>& DFBoardNumberToTopTowerMap,
                std::map<uint32_t, uint32_t>& DFBoardNumberToBotTowerMap,
                std::map<uint32_t, uint32_t>& towerToDFBoardNumberMap,
                std::map<uint32_t, uint32_t>& ATCAFabricChToOutputBitPosMap,
                std::map<uint32_t, uint32_t>& DFFiberConnectionMap );

  bool parseDatabaseFile( std::map<uint32_t, std::vector<uint32_t> >& ROB_to_linkIds,
                          std::map<uint32_t, std::vector<uint32_t> >& ROB_to_hashIds,
                          std::map<uint32_t, std::vector<uint32_t> >& ROB_to_rodIds,
                          std::map<uint32_t, std::vector<uint32_t> >& ROB_to_planeInfos,
                          std::vector<std::vector<uint32_t> >& modulelist_data );

  bool parseModuleListFile( std::vector<std::vector<uint32_t> >& modulelist_data );

  // board-specific configuration
  bool processModuleList( std::vector<uint32_t>& module_count_in,
                          std::vector<uint32_t>& module_count_out,
                          std::map<uint32_t, uint32_t>& pixmod2dst_map,
                          std::map<uint32_t, uint32_t>& sctmod2dst_map,
                          std::map<uint32_t, uint32_t>& pixmod2ftkplane_map,
                          std::map<uint32_t, uint32_t>& sctmod2ftkplane_map,
                          std::map<uint32_t, uint32_t>& pixmod2tower_map,
                          std::map<uint32_t, uint32_t>& sctmod2tower_map,
                          std::map<uint32_t, std::vector<uint32_t> >& FMCIn2modid_map,
                          std::map<uint32_t, uint32_t>& FMCIn2NumModules,
                          std::map<uint32_t, uint32_t>& SLINKOut2NumModules,
                          std::map<std::pair<uint32_t, uint32_t>, uint32_t>& lane_mod2idx,
                          std::map<std::pair<uint32_t, uint32_t>, uint32_t>& lane_idx2mod );

  bool getInternalLinkOutputDestWords( std::map<uint32_t, uint32_t>& centralSwLaneId2DestMask );
  uint32_t getInternalLinkOutputBitMap( uint32_t source, uint32_t dest );
  uint32_t getFabricBitPosFromShelfAndSlot( uint32_t source_shelf, uint32_t source_slot,
                                            uint32_t dest_shelf,   uint32_t dest_slot,
                                            uint32_t source_num,   uint32_t dest_num );

  bool buildRxPorts( uint32_t& ports );

  bool parseTransceiverParameters ( std::map<uint32_t, uint32_t>& gtCh2RxPolarity,
                                    std::map<uint32_t, uint32_t>& gtCh2TxPolarity,
                                    std::map<uint32_t, uint32_t>& gtCh2ForceReadyMode,
                                    std::map<uint32_t, uint32_t>& gtCh2ToAlteraFPGA,
                                    std::map<uint32_t, uint32_t>& gtCh2IgnoreFreeze );

  bool parseBoardDelayFile( std::map<uint32_t, uint32_t>& delayMap,
                            std::map<uint32_t, uint32_t>& invMap );

};

namespace dfconfig {

  // System-wide configuration (shared between all boards)
  // Note these containers are protected from concurrent access only in the buildConfiguration stage.
  // That is, access is strictly read-only after building is complete.

  // board configuration file (globalCF)
  static std::atomic<uint32_t> m_globalConfigParseBegin;
  static std::atomic<uint32_t> m_globalConfigParseComplete;
  static std::map<uint32_t, bool> m_boardEnableMap;
  static std::multimap<uint32_t, std::pair<uint32_t, uint32_t> > m_rodIdToFMCInIdMap;
  static std::map<uint32_t, std::map<uint32_t, uint32_t> > m_boardIdToFMCInIdToRODIdMap;
  static std::map<uint32_t, uint32_t> m_boardIdToEnableFMCLanesMap;
  static std::map<std::pair<uint32_t, uint32_t>, bool> m_multishelf_boardEnableMap;

  // system configuration file (systemCF)
  static std::atomic<uint32_t> m_systemConfigParseBegin;
  static std::atomic<uint32_t> m_systemConfigParseComplete;
  static std::map<uint32_t, std::pair<uint32_t, uint32_t> > m_DFBoardNumberToShelfAndSlotMap;
  static std::map<uint32_t, uint32_t> m_FTKPlaneToDFOutputMap;
  static std::map<uint32_t, uint32_t> m_DFBoardNumberToTopTowerMap;
  static std::map<uint32_t, uint32_t> m_DFBoardNumberToBotTowerMap;
  static std::map<uint32_t, uint32_t> m_towerToDFBoardNumberMap;
  static std::map<uint32_t, uint32_t> m_ATCAFabricChToOutputBitPosMap;
  static std::map<uint32_t, uint32_t> m_DFFiberConnectionMap;

  // module list configuration (database or text file)
  static std::atomic<uint32_t> m_modulelistConfigParseBegin;
  static std::atomic<uint32_t> m_modulelistConfigParseComplete;
  static std::map<uint32_t, std::vector<uint32_t> > m_ROB_to_linkIds;
  static std::map<uint32_t, std::vector<uint32_t> > m_ROB_to_hashIds;
  static std::map<uint32_t, std::vector<uint32_t> > m_ROB_to_rodIds;
  static std::map<uint32_t, std::vector<uint32_t> > m_ROB_to_planeInfos;
  static std::vector<std::vector<uint32_t> > m_modulelist_data;

}}} // namespace daq::ftk::dfconfig

#endif
