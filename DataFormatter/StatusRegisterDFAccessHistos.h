#ifndef __STATUSREGISTERDFACCESSHISTOS_H__
#define __STATUSREGISTERDFACCESSHISTOS_H__
#include <string>
#include <vector>
#include "ftkcommon/StatusRegister/StatusRegister.h"
#include "DataFormatter/FtkDataFormatterApi.h" 

namespace daq {
  namespace ftk {

    class StatusRegisterDFAccessHistos : public StatusRegister
    {
    public:
      StatusRegisterDFAccessHistos(std::shared_ptr<FtkDataFormatterApi> dfapi, uint32_t selectorValue, uint32_t selectorAddress, uint32_t readerAddress, srType type); 
      ~StatusRegisterDFAccessHistos();
      
      
      /*! \brief So far , implemetation of 1-by-1 readout*/
      void readout();
    protected:
      std::vector<std::string> m_write_nodes;
      std::vector<uint32_t>    m_write_values;

    private:
      std::shared_ptr<FtkDataFormatterApi> m_dfapi;
    };
  } // namespace ftk
} // namespace daq

#endif // __STATUSREGISTERDFACCESSHISTOS_H__ 
