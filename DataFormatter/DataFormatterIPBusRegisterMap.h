#ifndef DataFormatterIPBusRegisterMap_H
#define DataFormatterIPBusRegisterMap_H

#include <tuple>
#include <utility>
#include <string>
#include <vector>
#include <map>
#include <fstream>

namespace daq { namespace ftk {

typedef std::pair<  std::string, uint32_t > RegisterNodeMask;
typedef std::tuple< std::string, uint32_t, std::string, uint32_t, std::vector<RegisterNodeMask> > RegisterNode;

static RegisterNode makeRegisterNode( const std::string& name, uint32_t address ) {
  return std::make_tuple( name, address, "", 0, std::vector<std::pair<std::string, uint32_t> >() );
}
static RegisterNode makeRegisterNode( const std::string& name, uint32_t address, const std::vector<RegisterNodeMask >& masks ) {
  return std::make_tuple( name, address, "", 0, masks );
}
static RegisterNode makeRegisterNode( const std::string& name, uint32_t address, const std::string& mode, uint32_t size ) {
  return std::make_tuple( name, address, mode, size, std::vector<std::pair<std::string, uint32_t> >() );
}

static inline void getRegisterNodeName( const RegisterNode& node, std::string& name ) { 
  name = std::get<0>(node); 
}
static inline void getRegisterNodeAddress( const RegisterNode& node, uint32_t& address ) { 
  address = std::get<1>(node); 
}
static inline void getRegisterNodeMode( const RegisterNode& node, std::string& mode ) { 
  mode = std::get<2>(node); 
}
static inline void getRegisterNodeSize( const RegisterNode& node, uint32_t& size ) { 
  size = std::get<3>(node); 
}
static inline void getRegisterNodeMasks( const RegisterNode& node, std::vector<RegisterNodeMask>& masks ) { 
  masks = std::get<4>(node);
}

typedef std::vector< RegisterNode > RegisterNodeVector;

// initialize the default register addresses from FW1803E
const static RegisterNodeVector defaultRegisterNodeVector = 
  {
    makeRegisterNode( "ipaddress",                              0x0 ),

    makeRegisterNode( "reset"    ,                              0x1,
                                { std::make_pair("reset_delay",                       0x001),
                                  std::make_pair("disable_fmc_input",                 0x002),
                                  std::make_pair("reset_parity_checker",              0x004),
                                  std::make_pair("fmcin_logic_reset",                 0x008),
                                  std::make_pair("main_state_machine_reset",          0x010),
                                  std::make_pair("i2c_state_machine_reset",           0x020),
                                  std::make_pair("configurable_parameter_reset",      0x040),
                                  std::make_pair("counter_parameter_reset",           0x080),
                                  std::make_pair("internal_link_bert_counter_reset",  0x100) } ),

    makeRegisterNode( "fmcin_front_fifo_error",                 0x02 ),
    makeRegisterNode( "fmcin_front_fifo_full",                  0x03 ),
    makeRegisterNode( "fmcin_parity_check_ok",                  0x05 ),
    makeRegisterNode( "fmcin_parity_error",                     0x06 ),

    makeRegisterNode( "gt_link_controller",                     0x07,
                                { std::make_pair("pll_reset",                         0x01),
                                  std::make_pair("transceiver_reset",                 0x02),
                                  std::make_pair("slink_reset",                       0x04),
                                  std::make_pair("slink_ureset",                      0x08),
                                  std::make_pair("slink_utest",                       0x10),
                                  std::make_pair("patgen_enable",                     0x20),
                                  std::make_pair("patgen_reset",                      0x40) } ),
    makeRegisterNode( "gt_slink_lrl_monitor",                   0x04 ),
    makeRegisterNode( "gt_link_monitor",                        0x08,
                                { std::make_pair("gt_rxbyteisaligned",                0x001),
                                  std::make_pair("gt_tx_reset_done",                  0x002),
                                  std::make_pair("gt_rx_reset_done",                  0x004),
                                  std::make_pair("gt_pll_lock",                       0x008),
                                  std::make_pair("slink_testled_n",                   0x010),
                                  std::make_pair("slink_lderrled_n",                  0x020),
                                  std::make_pair("slink_lupled_n",                    0x040),
                                  std::make_pair("slink_flowctrlled_n",               0x080),
                                  std::make_pair("slink_activityled_n",               0x100),
                                  std::make_pair("internal_link_tx_lock",             0x200),
                                  std::make_pair("internal_link_bert_comparison_valid",0x400) } ),
    makeRegisterNode( "gt_link_monitor_configure_laneselector", 0x09 ),
    makeRegisterNode( "gt_link_configuration",                  0x0A,
                              { std::make_pair("gt_rxpolarity",                       0x01),
                                std::make_pair("gt_txpolarity",                       0x02),
                                std::make_pair("force_ready_mode",                    0x04),
                                std::make_pair("to_altera_fpga",                      0x08),
                                std::make_pair("ignore_freeze",                       0x10) } ),
    makeRegisterNode( "gt_link_configuration_wen",              0x0B,
                              { std::make_pair("wen",                                 0x01) } ),
    makeRegisterNode( "gt_link_configuration_read",             0x0C,
                              { std::make_pair("gt_rxpolarity",                       0x01),
                                std::make_pair("gt_txpolarity",                       0x02),
                                std::make_pair("force_ready_mode",                    0x04),
                                std::make_pair("to_altera_fpga",                      0x08) } ),

    makeRegisterNode( "fmc_user_signal",                        0x0D,
                              { std::make_pair("mezzanine_reset",                     0x01),
                                std::make_pair("mezzanine_trigger",                   0x02) } ),

    makeRegisterNode( "input_buffer_full",                      0x0E ),
    makeRegisterNode( "input_buffer_xoff",                      0x0F ),

    makeRegisterNode( "spy_readout",                            0x10, "incremental", 0x4 ),
    makeRegisterNode( "spy_readaddr",                           0x14 ),
    makeRegisterNode( "spy_laneselector",                       0x16 ),
    makeRegisterNode( "spy_controller",                         0x17,
                              { std::make_pair("spy_reset",                           0x01),
                                std::make_pair("spy_freeze",                          0x02),
                                std::make_pair("spy_reset_im_spy",                    0x04) } ),

    makeRegisterNode( "this_board_mask_ido",                    0x19 ),
    makeRegisterNode( "this_board_mask_ili",                    0x1A ),
    makeRegisterNode( "internallink_url",                       0x1B ),
    makeRegisterNode( "disable_internallink_lanes_mask",        0x1C ),

    makeRegisterNode( "enable_fmc_lanes_mask",                  0x1D ),

    makeRegisterNode( "expected_number_of_module",              0x20 ),

    makeRegisterNode( "max_cycle_waited_for_data_allowed",      0x21 ),

    makeRegisterNode( "switching_configuration_lane_selector",  0x22 ),

    makeRegisterNode( "internallink_destination_words",         0x24 ),

    makeRegisterNode( "update_switching_configuration_enable",  0x28,
                        { std::make_pair("fmc_input_number_of_expected_modules", 0x01),
                          std::make_pair("slink_output_number_of_expected_modules", 0x02 ),
                          std::make_pair("central_switch_output_to_destination_port", 0x04 ) } ),

    makeRegisterNode( "fb_i2c_address",                         0x30,
                        { std::make_pair("address", 0x7F ),
                          std::make_pair("is_read_access", 0x80 ) } ),
    makeRegisterNode( "fb_i2c_data_from_master",                0x31 ),
    makeRegisterNode( "fb_i2c_data_from_slave",                 0x32 ),
    makeRegisterNode( "fb_i2c_word_counter",                    0x36 ),
    makeRegisterNode( "fb_i2c_enable",                          0x3D,
                        { std::make_pair("enable", 0x01) } ),
    makeRegisterNode( "fb_i2c_status",                          0x3E,
                        { std::make_pair("busy",  0x01),
                          std::make_pair("ack_error", 0x02) } ),

    makeRegisterNode( "spy_readen",                             0x3F ),

    makeRegisterNode( "lut_configuration_selector",             0x40 ),
    makeRegisterNode( "lut_configuration_addr_confin",          0x41 ),
    makeRegisterNode( "lut_configuration_wen_confin",           0x43 ),
    makeRegisterNode( "lut_configuration_read_lane",            0x47 ),
    makeRegisterNode( "lut_configuration_data_confin",          0x48 ),
    makeRegisterNode( "lut_configuration_data_confout",         0x4C ),

    makeRegisterNode( "firmware_version",                       0x50 ),

    makeRegisterNode( "b0f_timeout_threshold",                  0x51 ),

    makeRegisterNode( "slink_b0f_b0f_counter",                  0x53 ),
    makeRegisterNode( "slink_evtsorting_counter",               0x54 ),
    makeRegisterNode( "slink_total_evts_given_up",              0x55 ),
    makeRegisterNode( "slink_total_modules_given_up",           0x56 ),

    makeRegisterNode( "input_b0f_skew",                         0x57 ),
    makeRegisterNode( "input_b0f_timeout_links",                0x58 ),
    makeRegisterNode( "l1id_out_of_sync",                       0x59 ),

    makeRegisterNode( "global_l1id",                            0x5A ),

    makeRegisterNode( "word_stuck_in_b0f",                      0x5B ),
    makeRegisterNode( "input_packet_error",                     0x5C,
                      { std::make_pair("8th_word_error_links", 0xFFFF0000),
                        std::make_pair("other_error_links",    0x0000FFFF) } ),

    makeRegisterNode( "im_freeze_mode",                         0x5D ),

    makeRegisterNode( "slink_counter_selected",                 0x5E ),

    makeRegisterNode( "data_checker_fmc_fpga",                  0x5F ),

    makeRegisterNode( "fmc_config_clk_inv",                     0x60 ),
    makeRegisterNode( "fmc_config_clkdelay_ce",                 0x61 ),
    makeRegisterNode( "fmc_config_channeldelay_channel_selector", 0x62 ),
    makeRegisterNode( "fmc_config_channeldelay_ce",             0x63 ),

    makeRegisterNode( "input_enable_resync",                    0x64 ),

    makeRegisterNode( "input_l1id_ecr_jump",                    0x65 ),

    makeRegisterNode( "input_word_limit_per_module",            0x66 ),

    makeRegisterNode( "input_lane_monitor_selector",            0x67 ),

    makeRegisterNode( "input_lane_monitor_b0f_timeout_count",   0x68 ),
    makeRegisterNode( "input_lane_monitor_l1id_timeout_count",  0x69 ),
    makeRegisterNode( "input_lane_monitor_number_modules_hit_word_limit", 0x6A ),
    makeRegisterNode( "input_lane_monitor_packet_structure_error_count",  0x6B ),
    makeRegisterNode( "input_lane_monitor_packet_eighth_word_error_count", 0x6C ),

    makeRegisterNode( "change_ip_addr", 0x6D ),
    makeRegisterNode( "user_config_ip_addr", 0x6E ),
    makeRegisterNode( "user_config_mac_addr", 0x6F ),

    makeRegisterNode( "readout_32bit_counter", 0x70 ),

    makeRegisterNode( "input_unknown_module_header_seen", 0x71 ),
    makeRegisterNode( "input_unknown_module_header_word", 0x72 ),

    makeRegisterNode( "readout_32bit_counter_lane_selector", 0x74,
                      { std::make_pair("lane", 0x0FFFF),
                        std::make_pair("type", 0xF0000) } ),

    makeRegisterNode( "internal_link_tx_enable", 0x75 ),
    makeRegisterNode( "internal_link_rx_enable_bert", 0x76 ),
    makeRegisterNode( "internal_link_tx_enable_bert", 0x77 ),
    makeRegisterNode( "internal_link_rx_enable",  0x78 ),

    makeRegisterNode( "switch_selector_for_fifo_monitoring", 0x79,
                      { std::make_pair("type",   0x00000F00),
                        std::make_pair("column", 0x000000F0),
                        std::make_pair("row",    0x0000000F) } ),
    makeRegisterNode( "switch_fifo_monitoring",  0x7A,
                      { std::make_pair("input1_input_empty",          0x00000001),
                        std::make_pair("input1_input_full",           0x00000002),
                        std::make_pair("input1_output1_rdy",          0x00000004),
                        std::make_pair("input1_output2_rdy",          0x00000008),
                        std::make_pair("input1_output1_being_sent",   0x00000010),
                        std::make_pair("input1_output2_being_sent",   0x00000020),
                        std::make_pair("input2_input_empty",          0x00000100),
                        std::make_pair("input2_input_full",           0x00000200),
                        std::make_pair("input2_output1_rdy",          0x00000400),
                        std::make_pair("input2_output2_rdy",          0x00000800),
                        std::make_pair("input2_output1_being_sent",   0x00001000),
                        std::make_pair("input2_output2_being_sent",   0x00002000),
                        std::make_pair("output1_input1_empty",        0x00010000),
                        std::make_pair("output1_input2_empty",        0x00020000),
                        std::make_pair("output1_input1_full",         0x00040000),
                        std::make_pair("output1_input2_full",         0x00080000),
                        std::make_pair("output1_input1_being_read",   0x00100000),
                        std::make_pair("output1_input2_being_read",   0x00200000),
                        std::make_pair("output1_output_rdy",          0x00400000),
                        std::make_pair("output2_input1_empty",        0x01000000),
                        std::make_pair("output2_input2_empty",        0x02000000),
                        std::make_pair("output2_input1_full",         0x04000000),
                        std::make_pair("output2_input2_full",         0x08000000),
                        std::make_pair("output2_input1_being_read",   0x10000000),
                        std::make_pair("output2_input2_being_read",   0x20000000),
                        std::make_pair("output2_output_rdy",          0x40000000) } )
  };

const static RegisterNodeVector registerNodeVector19000 = 
  {
    makeRegisterNode( "inout_fifo_selector",                      0x4D ),
    makeRegisterNode( "inout_fifo_empty",                         0x4E ),
    makeRegisterNode( "inout_fifo_busy",                          0x4F ),
    makeRegisterNode( "unused_0",                                 0x51 ), // used to be b0f_timeout_threshold
    makeRegisterNode( "fmc_fifo_wr_data",                         0x52 ),
    makeRegisterNode( "fmc_config_clk_inv",                       0x57 ),
    makeRegisterNode( "fmc_config_clkdelay_ce",                   0x58 ),
    makeRegisterNode( "fmc_config_channeldelay_channel_selector", 0x59 ),
    makeRegisterNode( "fmc_config_channeldelay_ce",               0x5a ),
    makeRegisterNode( "input_lane_counters_2",                    0x5b ),
    makeRegisterNode( "input_config_a",                           0x60 ),
    makeRegisterNode( "input_config_b",                           0x61 ),
    makeRegisterNode( "timeout_threshold_a",                      0x62 ),
    makeRegisterNode( "timeout_threshold_b",                      0x63 ),
    makeRegisterNode( "input_global_l1id",                        0x64 ),
    makeRegisterNode( "input_skew_counter",                       0x65 ),
    makeRegisterNode( "input_timedout_latch",                     0x66 ),
    makeRegisterNode( "input_mod_header_error",                   0x67 ),
    makeRegisterNode( "input_packet_l1id_error",                  0x68 ),
    makeRegisterNode( "input_lane_selector",                      0x69 ),
    makeRegisterNode( "input_lane_l1id",                          0x6a ),
    makeRegisterNode( "input_lane_errorword",                     0x6b ),
    makeRegisterNode( "input_lane_counters",                      0x6c )
  };

const static RegisterNodeVector registerNodeVector19006 =
  {
    makeRegisterNode( "input_lane_num_packets_discarded_synch",   0x42 ),
    makeRegisterNode( "input_lane_num_packets_held_synch",        0x44 ),
    makeRegisterNode( "input_lane_num_packets_timedout_synch",    0x45 ),
    makeRegisterNode( "input_lane_num_packets_discarded",         0x46 ),
    makeRegisterNode( "input_lane_num_mods_hit_word_limit",       0x49 )
  };

const static RegisterNodeVector registerNodeVector19008 =
  {
    makeRegisterNode( "input_monitoring_hist_re",                 0x29 ),
    makeRegisterNode( "input_monitoring_hist_skew_hist_out",      0x2A ),
    makeRegisterNode( "input_monitoring_ph_timeout_hist_out",     0x2B ),
    makeRegisterNode( "input_monitoring_global_l1_skip_hist_out", 0x2C ),
    makeRegisterNode( "input_monitoring_lanes_l1_skew_hist_out",  0x2D )
  };

const static RegisterNodeVector registerNodeVector1900D = 
  {
    makeRegisterNode( "fmcin_overflow",                     0x33 ),
    makeRegisterNode( "ilink_rx_overflow",                  0x34 ),
    makeRegisterNode( "ilink_tx_overflow",                  0x35 ),
    makeRegisterNode( "slink_fifo_overflow_1",              0x37 ),
    makeRegisterNode( "slink_fifo_overflow_2",              0x38 ),
    makeRegisterNode( "slink_packer_overflow_1",            0x39 ), 
    makeRegisterNode( "slink_packer_overflow_2",            0x3A ), 
    makeRegisterNode( "monitoring_signal_selector",         0x3B ),
    makeRegisterNode( "monitoring_signal",                  0x3C ),
  };

typedef std::map< uint32_t, RegisterNode > RegisterNodeMap;

// If setDefault = true, populate map from defaultRegisterNodeVector
// Then (regardless of setDefault), additionally apply updates to the map
static void makeRegisterNodeMap( const RegisterNodeVector& updates, RegisterNodeMap& map, bool setDefault=false ) {

  if ( setDefault ) {
    map.clear();
    for ( const auto& reg : defaultRegisterNodeVector ) {
      uint32_t address;
      getRegisterNodeAddress( reg, address );
      map[ address ] = reg;
    }
  }

  for ( const auto& reg : updates ) {
    uint32_t address;
    getRegisterNodeAddress( reg, address );
    map[ address ] = reg;
  }
}

static inline void makeRegisterNodeMap( RegisterNodeMap& map, uint32_t FWversion ) {
  if ( FWversion < 0x19000 ) { // just use 0x1803e mapping (default)
    makeRegisterNodeMap( RegisterNodeVector(), map, true );
  } else { // mapping is the same for 0x19000 - 0x19005
    makeRegisterNodeMap( registerNodeVector19000, map, true );
  }
  if ( FWversion >= 0x19006 ) { // added monitoring in 0x19006
    makeRegisterNodeMap( registerNodeVector19006, map, false ); // setDefault=false to not override 0x19000 updates
  } 
  if ( FWversion >= 0x19008 ){
    makeRegisterNodeMap( registerNodeVector19008, map, false );
  }
  if ( FWversion >= 0x1900D ){
    makeRegisterNodeMap( registerNodeVector1900D, map, false );
  }
  
  
  
  
/*  switch( FWversion ) {
    case 0x1803E : // the default values are already correct
      makeRegisterNodeMap( RegisterNodeVector(), map, true );
      break;
    case 0x19000 :
      makeRegisterNodeMap( registerNodeVector19000, map, true );
      break;
    default:
      makeRegisterNodeMap( RegisterNodeVector(), map, true );
  }*/
}

 void writeRegisterNodeMap( const RegisterNodeMap& map, const std::string& fname, const std::string& name );

} } // namespace daq::ftk

#endif
