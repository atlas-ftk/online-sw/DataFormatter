#ifndef DataFormatterConstants_H
#define DataFormatterConstants_H

#include <cstdint>

namespace daq { namespace ftk {

const uint32_t NUMBER_OF_FTK_TOWERS = 64;

const uint32_t NUMBER_OF_DF_BOARDS = 32;

const uint32_t NUMBER_OF_DF_INPUT_LANES = 16; // per DF

const uint32_t NUMBER_OF_DF_OUTPUT_LANES = 36; // per DF

const uint32_t NUMBER_OF_DF_LANES = NUMBER_OF_DF_INPUT_LANES + NUMBER_OF_DF_OUTPUT_LANES;

const uint32_t NUMBER_OF_IM_FPGAS_PER_DF = 8;

// registers for configuring the DF FW switches
const uint32_t SW_CONF_UPDATE_MASK_FMC_INPUT_NUMBER_OF_EXPECTED_MODULES = 0x1;
const uint32_t SW_CONF_UPDATE_MASK_SLINK_OUTPUT_NUMBER_OF_EXPECTED_MODULES = 0x2;

const uint32_t NUMBER_OF_WORDS_IN_DF_SPYBUFFERS = 1024;

const uint32_t NUMBER_OF_INTERDF_LINKS = 24;

const uint32_t CLOCK_SPEED = 200000000;

const uint32_t NUMBER_OF_DF_GTX = 60;
} } // namespace

#endif
