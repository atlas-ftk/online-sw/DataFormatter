#ifndef smon_components_h
#define smon_components_h

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <vector>
#include <bitset>
#include <string>
#include <sstream>
#include <stdint.h>
#include <array>

class smon_components{
 private:
  std::vector<uint32_t> vec_words;

  // ================
  // Number of events
  // ================ 
  uint32_t nEvent;
  uint64_t nEvent_nonReset;
  uint64_t nEvent_sent;
  uint32_t ndupli_in;
  uint32_t ndupli_out;
  uint32_t nBP;

  // =================
  // Event Information
  // =================
  // 0: RunNumber 1: L1ID 2: #words 3: word 4: time from BOF 5: #LostWords
  uint32_t EvInfo[2][6];//current
  uint32_t RODID;//RODID
  uint32_t is_IBL;//RODID
  // ===================
  // outL1ID information
  // ===================
  uint32_t outfifo_lastL1ID;//last L1ID written in outfifo
  uint32_t sent_L1ID;//L1ID sent to DF
  // ===================
  // dupli hit Info
  // ===================
  uint32_t dupli_check_word[4];//4 dupli check words
  uint32_t error_words[32];
  uint32_t is_dupli_words;//
  // ===================
  // Recover Information
  // ===================
  uint32_t cnt_long_blocked;//
  uint32_t nblocked_readout;//
  uint32_t cnt_timeout;//
  uint32_t ndupli_words;//
  uint32_t nover_fillgap_limit;//
  uint32_t nfillgap;//
  uint32_t ntimeout;//
  // ===================
  // Error Bit and Eegister Info
  // ===================
  uint32_t Error_Register;//Error Register
  uint32_t clock_count_from_rst;//Clock cycle count from rst
  uint32_t my_Error_Bit;//Error Bit
  uint32_t my_Error_Bit_quick;//Error Bit connected simultaneously
  uint32_t my_Error_Bit_quick2;//Error Bit2 connected simultaneously
  uint32_t bin_my_Error_Bit[32];
  uint32_t bin_my_Error_Bit_quick[32];
  uint32_t bin_my_Error_Bit_quick2[32];
  // ==================
  // Flow of data State
  // ==================
  std::string data_state[2][8];// current 1st - 8th
  // ==========================
  // Average variable of inputs
  // ==========================
  // 0: ave. #words (most sig. 4bit) 1: ave. ClusterSize (least sig. 4bit) 2: ave. time (bof-bof) 3:ave. event rate 4: ave. time (bof-eof) 5: ave. latency
  uint32_t word_states[4][6];
  // ===========
  // FIFO Status
  // ===========
  // 0: infifo status 1: sysfifo status 2: rodfifo status 
  // 3: datafifo status 4: buffifo status 5: outfifo status
  // 6: decoded fifo status 7: lvl1id fifo status
  // 8: cluster out fifo status 9: parallel fifo status
  // 10: grid system out fifo status 11: centroid calculation status
  uint32_t infifo_status[7];
  uint32_t sysfifo_status[7];
  uint32_t rodfifo_status[7];
  uint32_t datafifo_status[7];
  uint32_t buffifo_status[7];
  uint32_t outfifo_status[7];
  uint32_t decodedfifo_status[7];
  uint32_t lvl1idfifo_status[7];
  uint32_t clusteroutfifo_status[7];
  uint32_t parallelfifo_status[7];
  uint32_t gridsysoutfifo_status[7];
  uint32_t centroidcalc_status[7];
  uint32_t clusterout_fifo_status_each[8][7];
  uint32_t parallel_fifo_status_each[8][7];
  
  // ===========
  // FIFO Fraction
  // ===========
  // time fractions of FIFO status
  // 0~3: average over 1sec, 4~7: average over 60sec
  // 0,4: infifo empty  1,5: infifo busy 
  // 2,6: outfifo empty 3,7: outfifo busy 
  double fifo_fraction_information[8];
  // =============
  // XOFF Control
  // =============
  uint32_t xoff_enable;
  uint32_t xoff_auto_disable;
  uint32_t xoff_counter;
  /* uint32_t xoff_was_stopped; */

  // ===================
  // Error words
  // ===================
  uint32_t nrod_status_word0;
  uint32_t nrod_status_word1;
  
  uint32_t l1id_error[4];
  uint32_t rod_status_word0[4];
  uint32_t rod_status_word1[4];
  uint32_t ID_cluster_errors[4];
  uint32_t grid_system_errors[4];
  uint32_t data_merger_monitor[16];
  
  // =============================
  // input rate histograms
  // =============================
  uint32_t hist_input_rate_b0f_b0f[9];
  uint32_t hist_input_rate_max_idle[9];
  // for data_merger monitor
  uint32_t data_merger_FSM;
  uint32_t data_merger_out;
  uint32_t data_merger_L1ID;
  uint32_t data_merger_end_event_tag_reg_out;
  uint32_t data_merger_total_active_module_reg_in;
  uint32_t data_merger_total_active_module_reg_out;
  uint32_t data_merger_total_start_event_reg_in;
  uint32_t data_merger_total_start_event_reg_out;
  uint32_t data_merger_total_start_event_encoded;
  uint32_t data_merger_active_start_event;
  uint32_t data_merger_active_end_event;
  uint32_t data_merger_parallel_clusters_rd_int;
  uint32_t data_merger_FSM_detail_monitor;
  uint32_t data_merger_slv_active_module_number;
  uint32_t data_merger_Parallel_clusters_valid;
  uint32_t data_merger_Parallel_clusters_almost_empty;
  uint32_t data_merger_flags;
  
  
  // just for new IM FW, 
  class event_info {
  public :
    event_info( int _idx, int _clk_freq ) : idx(_idx), clk_freq(_clk_freq) {};
    ~event_info(){};
  public :
   const int idx, clk_freq;
    uint32_t run_number, l1id, bcid, l1tt, evTyp, tim, err_word0, err_word1, n_word, cnt_b0f_b0f, cnt_b0f_e0f, rate_b0f_b0f, rate_b0f_e0f;
  };
  
  class l1id_info {
  public :
    l1id_info( int _idx ) : idx(_idx){};
    ~l1id_info() {};
  public :
    const int idx;
    uint32_t n_l1id_jumps, max_l1id_gap, jumped_01_l1id_ahead, jumped_01_l1id_behind, jumped_02_l1id_ahead, jumped_02_l1id_behind, jumped_03_l1id_ahead, jumped_03_l1id_behind;
  };
  
  class dataflow_info {
  public :
    dataflow_info( int _idx, int _clk_freq ) : idx(_idx), clk_freq( _clk_freq ), 
                                               current_event( _idx,      _clk_freq ),
                                               prev_01_event( _idx +  7, _clk_freq ),
                                               prev_02_event( _idx + 14, _clk_freq ),
                                               jump_info(_idx + 28){};
    ~dataflow_info(){};
  public : 
    const int idx, clk_freq;
    uint64_t n_total_event;
    event_info current_event, prev_01_event, prev_02_event;
    l1id_info jump_info;
  };
  
  class fifo_monitor{
  public :
    fifo_monitor(int _idx, int _clk_freq, int _run_time, int _depth, bool _is_small = false ) : idx(_idx), depth(_depth), clk_freq(_clk_freq), run_time(_run_time), is_small(_is_small) {};
    ~fifo_monitor(){};
  public :
    const int idx, depth, clk_freq, run_time;
    const bool is_small;
    bool     is_full, is_almost_full, is_empty, is_almost_empty, is_lost_word;
    uint32_t n_lost_word, n_full, cnt_full, n_almost_full, cnt_almost_full;
    float     busy_fraction, occupancy;
    std::string status, status_s;
  };
  class simple_counter {
  public :
    simple_counter( int _idx, int _clk_freq, int _run_time) : idx(_idx), clk_freq(_clk_freq), run_time(_run_time) {};
    ~simple_counter(){};
  public : 
    const int idx, clk_freq, run_time;
    uint32_t n_full, cnt_full;
    float busy_fraction;
  };
  
 public :
  smon_components( bool is_SCT = false );
  ~smon_components();

  void PutEndl(std::string str, int num);
  void Put(std::string str, int num);
  // char* printf_with_number_of( uint32_t value );
  // char* printf_with_number_of_64( uint64_t value );
  // char* printf_percent( float value );
  std::string printf_with_number_of( const uint32_t value );
  std::string printf_with_number_of_64( const uint64_t value );
  std::string printf_percent( const float value );
  
  void show_title();
  void show_simple_title();
  void show_smon_raw_title();
  void read_32bitdata(std::vector<uint32_t> input_vec_words);

  // read
  void rd_EvInfo();
  void rd_dupli_words();
  void rd_word_state();
  void rd_fifo(int fifo_seed);
  void rd_fifo_each_parallel(int fifo_seed);
  void rd_fifo_each_clusterout(int fifo_seed);
  void rd_fifo_occupancy();
  void rd_fifo_fraction();
  void rd_errors();
  void rd_input_rate_b0f_b0f();
  void rd_input_rate_max_idle();
  void rd_data_merger_monitor();

  // write
  /* void wr_channel(const uint32_t& fpga_id, const uint32_t& channel_id); */
  void wr_globalInfo(const uint32_t& fpga_id, const uint32_t& channel_id,
                     const uint32_t& slink_enable_value, 
                     const uint32_t& pseudo_value, 
                     const bool slink_is_up,
                     const bool slink_is_slave,
                     const bool is_bp_ignored,
                     const bool is_test_im
                     );
  void wr_GeneralInfo();
  void wr_EvtInfo();
  void wr_xoff_control();
  void wr_RecoverInfo();
  void wr_event_rate();
  void wr_ErrorBit();
  void wr_dupli_words();
  void printf_fifo_status( uint32_t fifo_info[7] );
  void printf_fifo_occupancy( uint32_t fifo_wr_data_count, int data_width );
  void wr_fifo(int channel);
  void wr_rod_status0( int idx, std::string error_word );
  void wr_errors();
  void wr_L1ID_monitor();
  void wr_input_rate_b0f_b0f();
  void wr_input_rate_max_idle();
  void wr_data_merger_monitor();
  
  void print_full_old( int channel_id, int fpga_id );
  
  void print_raw_text();
  void print_table1(int num);
  
  
  
  
  
  // just for new IM FW( from version = 0x0130 )
  uint32_t get_bit( uint32_t word, int n ){ return ( word & ( 0x1 << n ) ) >> n; }
  bool check_bit( uint32_t word, int n ){ return ( ( word & ( 0x1 << n ) ) >> n ) == 0x1; }
  std::string on_off( bool b, std::string on = "on", std::string off = "off" ) { return b ? on : off; }
  void read_vector( );
  void print_full( int channel_id );
  void print_one_liner( bool is_print_first, int channel_id );

  void fill( simple_counter& p );
  void fill( event_info& p);
  void fill( l1id_info& p );
  void fill( dataflow_info& p);
  void fill( fifo_monitor& p );
  void print_short( simple_counter& p );
  void print_short( dataflow_info& p );
  void print_short( fifo_monitor& p );

  void print_long1( simple_counter& p, std::string name );
  void print_long1( event_info& p, std::string name, uint64_t n_total_event );
  void print_long1( l1id_info& p, std::string name );
  void print_long1( dataflow_info& p, std::string name );
  void print_long1( fifo_monitor& p, std::string name );
  
  
  // member 
  dataflow_info input_from_LDC;
  dataflow_info output_from_INFIFO;
  dataflow_info output_from_OUTFIFO;
  
  fifo_monitor in_fifo_monitor;
  fifo_monitor rod_fifo_monitor;
  fifo_monitor data_fifo_monitor;
  fifo_monitor out_fifo_monitor;
  
  fifo_monitor SCT_input_fifo_monitor;
  fifo_monitor SCT_decoded_fifo_monitor;
  fifo_monitor SCT_sorted_fifo_monitor;
  
  std::vector< fifo_monitor >  parallel_input_fifo_monitor { { 200, 80, 10, 1024, true },
      { 203, 80, 10, 1024, true  },
        { 206, 80, 10, 1024, true  },
          { 209, 80, 10, 1024, true  },
            { 212, 80, 10, 1024, true  },
              { 215, 80, 10, 1024, true  },
                { 218, 80, 10, 1024, true  },
                  { 221, 80, 10, 1024, true  }
  };
  std::vector< fifo_monitor> parallel_output_fifo_monitor { { 224, 80, 10, 1024, true  },
      { 227, 80, 10, 1024, true  },
        { 230, 80, 10, 1024, true  },
          { 233, 80, 10, 1024, true  },
            { 236, 80, 10, 1024, true  },
              { 239, 80, 10, 1024, true  },
                { 242, 80, 10, 1024, true  },
                  { 245, 80, 10, 1024, true  }
  };
  
  
  
  simple_counter xoff_monitor;
  simple_counter BP_monitor;
  simple_counter recover_monitor;
  simple_counter truncation_monitor;
  simple_counter fillgap_monitor;


  uint32_t fw_version; // 16bits fw version
  uint32_t fw_type; // 16bits fw type
  uint32_t fw_hash; // git hash value
  uint32_t fw_date; // git hash value
  uint32_t status_bit; // 32 bit status bit
  uint32_t error_bit; // 32 bit error bit
  uint32_t first_L1ID; 
  uint32_t timeout_cnt; 
  uint32_t loophit_cnt; 
  
  // FSM encoding for each logic push/pull
  uint32_t fsm_encoding_push_input_handler; 
  uint32_t fsm_encoding_push_input_splitter; 
  uint32_t fsm_encoding_push_output_merger; 
  uint32_t fsm_encoding_push_fillgap_handler; 
  uint32_t fsm_encoding_push_output_builder; 
  uint32_t fsm_encoding_push_input_splitter_; 

  uint32_t fsm_encoding_pull_input_handler; 
  uint32_t fsm_encoding_pull_input_splitter; 
  uint32_t fsm_encoding_pull_output_merger; 
  uint32_t fsm_encoding_pull_fillgap_handler; 
  uint32_t fsm_encoding_pull_output_builder; 
  
  // rate histogram
  uint32_t m_rate_hist_input[6];
  uint32_t m_rate_hist_output[6];
  float    m_rate_hist_input_frac[6];
  float    m_rate_hist_output_frac[6];
  
  // for description of status bit
  
};
#endif
