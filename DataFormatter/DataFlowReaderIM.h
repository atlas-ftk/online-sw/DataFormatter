#ifndef DATAFLOWREADERIM_H
#define DATAFLOWREADERIM_H

#include "ftkcommon/DataFlowReader.h"
#include "DataFormatter/dal/FtkDataFlowSummaryIMNamed.h"

#include <string>
#include <vector>


// Namespaces
namespace daq {
  namespace ftk {
    
    /*! \brief  IM implementation of DataFlowReader
     */
    class DataFlowReaderIM : public DataFlowReader
    {
    public:
      /*! \brief Constructor: sets the input scheme (IS inputs or vectors)
       *  \param ohRawProvider Pointer to initialized  OHRawProvider object
       *  \param forceIS Force the code to use IS as the source of information (default is 0)
       */
      DataFlowReaderIM(std::shared_ptr<OHRawProvider<> > ohRawProvider, uint32_t m_im_fwVersion, bool forceIS = false);
      
      float inputFunction(int kchannel, uint32_t address_input, bool dealing_fraction=false, uint32_t b_option =0);
      uint32_t inputFunctionInt(int kchannel, uint32_t address_input);
      
      void init(const string& deviceName,
                const std::string& partitionName = "FTK",
                const std::string& isServerName = "DF");
  
      // override 
      virtual void getFifoInBusy(std::vector<int64_t>& srv) override;
      virtual void getFifoOutBusy(std::vector<int64_t>& srv) override;
      virtual void getFifoInEmpty(std::vector<int64_t>& srv) override;
      virtual void getFifoOutEmpty(std::vector<int64_t>& srv) override;
      virtual void getBusyFraction(std::vector<float>& srv, uint32_t) override; // ??? 
      virtual void getFifoInBusyFraction(std::vector<float>& srv, uint32_t) override; 
      virtual void getFifoOutBusyFraction(std::vector<float>& srv, uint32_t) override;
      virtual void getFifoInEmptyFraction(std::vector<float>& srv, uint32_t) override;
      virtual void getFifoOutEmptyFraction(std::vector<float>& srv, uint32_t) override;
      virtual void getL1id(std::vector<int64_t>& srv) override;
      // virtual void getDataErrors(std::vector<int64_t>& srv) override;
      virtual void getEventRate(std::vector<int64_t>& srv) override; // output rate of an averate of 20sec
      virtual void getLinkInStatus(std::vector<int64_t>& srv) override;
      // virtual void getLinkOutStatus(std::vector<int64_t>& srv) override;
      // virtual void getFpgaTemperature(std::vector<int64_t>& srv) override;
      virtual void getNEventsProcessed(std::vector<int64_t>& srv) override;
      // virtual void getNpacketsDiscarded(std::vector<int64_t>& srv);
      // virtual void getNpacketsDiscardedSync(std::vector<int64_t>& srv);
      // virtual void getNpacketsHeldSync(std::vector<int64_t>& srv);
      // virtual void getNpacketsTimedout(std::vector<int64_t>& srv);
      // virtual void getNpacketsTimeoutSync(std::vector<int64_t>& srv);
      // virtual void getInputSkewSync(std::vector<int64_t>& srv);
      // virtual void getPacketCompletionTime(std::vector<int64_t>& srv);
      virtual void getFifoInOverflowFlag(std::vector<int64_t>& srv) override;
      virtual void getFifoOutOverflowFlag(std::vector<int64_t>& srv) override;
      // virtual void getInternalOverflowFlag(std::vector<int64_t>& srv) override;
      // virtual void getMaxEvtProcessingTime(std::vector<int64_t>& srv) override;
      // virtual void getMaxBackPressureTime(std::vector<int64_t>& srv) override;
      // virtual void getFreezeCount(std::vector<int64_t>& srv) override;
      // virtual void getErrorCount(std::vector<int64_t>& srv) override;
      // virtual void getDataTruncationCount(std::vector<int64_t>& srv) override;
      // for IM specific 
      virtual void getNInputTruncation(std::vector<int64_t>& srv);
      virtual void getInputTruncationFraction(std::vector<float>& srv);
      virtual void getNRecover(std::vector<int64_t>& srv);
      virtual void getRecoverFraction(std::vector<float>& srv);
      virtual void getBPFractionFromDF(std::vector<float>& srv);
      virtual void getNFifoOverflow(std::vector<float>& srv);
      virtual void getOverflowFraction(std::vector<float>& srv);
      
      
      
      virtual void publishExtraHistos(uint32_t option = 0);
      
      
    private:
      inline bool is_refactord_fw() const { return (m_im_fwVersion & 0x7FFF) > 0x130 ; }
      std::vector<std::string> getISVectorNames();
      const std::string& partitionName = "FTK";
      std::shared_ptr<FtkDataFlowSummaryIMNamed> m_theIMSummary;
      uint32_t m_im_fwVersion;
      
    private:
      
    };
    
  } // namespace ftk
} // namespace daq

#endif /* DATAFLOWREADERIM_H */
