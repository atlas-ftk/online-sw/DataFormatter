#ifndef STATUSREGISTERDFSELECTORGENERIC
#define STATUSREGISTERDFSELECTORGENERIC

#include <string>
#include "DataFormatter/FtkDataFormatterApi.h"
#include "DataFormatter/StatusRegisterDFSelector.h"

namespace daq { namespace ftk {

  class StatusRegisterDFSelectorGeneric : public StatusRegisterDFSelector
  {
  public:
    StatusRegisterDFSelectorGeneric( std::shared_ptr<FtkDataFormatterApi> dfapi, uint32_t selectorValue, uint32_t selectorAddress, uint32_t readerAddress );
    ~StatusRegisterDFSelectorGeneric() {}
  };

}} // namespace daq::ftk

#endif
