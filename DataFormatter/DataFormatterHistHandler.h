#ifndef DATAFORMATTERHISTHANDLER_H
#define DATAFORMATTERHISTHANDLER_H

#include "DataFormatter/FtkDataFormatterApi.h"
#include "DataFormatter/DataFormatterConstants.h"

#include "ftkcommon/EventFragmentHitClusters.h"

#include "TH1F.h"
#include "TH2F.h"

#include "oh/OHRootProvider.h"

namespace daq {
  namespace ftk {

    class DataFormatterHistHandler
    {
    public:

      DataFormatterHistHandler( const std::string& name, std::shared_ptr<OHRootProvider> ohProvider, std::shared_ptr<FtkDataFormatterApi> dfApi ); // ReadoutModule initialize
      virtual ~DataFormatterHistHandler() noexcept; // ReadoutModule destructor

      void configureHists(); // ReadoutModule configure
      void resetHists(); // ReadoutModule prepareForRun
      void publishHists(); // ReadoutModule publish
      // ReadoutModule publishFullStats
      void publishFullStatsHists( std::vector<std::vector<EventFragmentHitClusters*> >&,
                                  const std::map<std::pair<uint32_t,uint32_t>,uint32_t>& );

      std::string name_ftk() { return m_name; }

    private:

      uint32_t m_input_active = 0x0;

      std::string m_name;
      std::shared_ptr<OHRootProvider> m_ohProvider;
      std::shared_ptr<FtkDataFormatterApi> m_dfApi;

      std::vector<bool> m_isactive;

      // spy buffer parsing
      std::unique_ptr<TH1F>               m_hlast_lvl1id;
      std::unique_ptr<TH2F>               m_hnfragments;
      std::unique_ptr<TH2F>               m_hnmodules;
      std::unique_ptr<TH2F>               m_hclusterpermodule;
      std::unique_ptr<TH2F>               m_hpixel_row_width;
      std::unique_ptr<TH2F>               m_hpixel_column_width;
      std::unique_ptr<TH2F>               m_hsctcluster_hit;
      std::unique_ptr<TH2F>               m_hsct_width;
      std::vector<std::unique_ptr<TH1F> > m_vec_hnfragments;
      std::vector<std::unique_ptr<TH1F> > m_vec_hnmodules;
      std::vector<std::unique_ptr<TH1F> > m_vec_hclusterpermodule;
      std::vector<std::unique_ptr<TH2F> > m_vec_hpixelcluster_all;
      std::vector<std::unique_ptr<TH2F> > m_vec_hpixelcluster_split;
      std::vector<std::unique_ptr<TH1F> > m_vec_hpixel_row_width;
      std::vector<std::unique_ptr<TH1F> > m_vec_hpixel_column_width;
      std::vector<std::unique_ptr<TH1F> > m_vec_hsctcluster_hit;
      std::vector<std::unique_ptr<TH1F> > m_vec_hsct_width;
      // other histograms
      std::unique_ptr<TH2F> m_htimedout_lanes;
      std::unique_ptr<TH1F> m_hslink_total_evts_given_up;
      std::unique_ptr<TH1F> m_htimedout_lanes_flat;
      std::unique_ptr<TH1F> m_hpacketErrorLanes;
      std::unique_ptr<TH1F> m_hpacket8thWordErrorLanes;
//      std::unique_ptr<TH1F> m_hinputLaneResyncCount;
    };

  }
}

#endif
