#ifndef __STATUSREGISTERDFSELECTOR_H__
#define __STATUSREGISTERDFSELECTOR_H__
#include <string>
#include <vector>
#include "ftkcommon/StatusRegister/StatusRegister.h"
#include "DataFormatter/FtkDataFormatterApi.h"


namespace daq {
namespace ftk {

   /*! \brief Implementation of the DF register access via selector.
    */

  class StatusRegisterDFSelector : public StatusRegister
  {
  public:
    StatusRegisterDFSelector(std::shared_ptr<FtkDataFormatterApi> dfapi, const srType& type);
    ~StatusRegisterDFSelector();

    /*! \brief So far , implemetation of 1-by-1 readout
     */
    void readout();

  protected:
    std::vector<std::string> m_write_nodes;
    std::vector<uint32_t>    m_write_values;

  private:
    std::shared_ptr<FtkDataFormatterApi> m_dfapi;

  };

} // namespace ftk
} // namespace daq

#endif // __STATUSREGISTERDFSELECTOR_H__
