tdaq_package()

# TODO: DEPENDENCIES

#### Create temporary file containing a string (named cmtversion) that is build using 
#### svn info command. The string can be printed out including version.ixx in the code 

execute_process(
  WORKING_DIRECTORY ../${TDAQ_PACKAGE_NAME}
  COMMAND svn info
  OUTPUT_VARIABLE SVNINFO
)

execute_process( 
  COMMAND mkdir -p ${TDAQ_PACKAGE_NAME}/cmt
)

execute_process(
  COMMAND echo -n "const char* cmtversion = R\"FtkDelim(" \nTag: ${TDAQ_PACKAGE_VERSION} \n${SVNINFO} ")FtkDelim\"; \n" 
  OUTPUT_FILE ${TDAQ_PACKAGE_NAME}/cmt/version.ixx
)

############################################################
# DAL: generation of C++ OKS interface from xml schema file 
#####

tdaq_generate_dal(schema/Readout_DataFormatter.schema.xml
  NAMESPACE daq::ftk::dal
  INCLUDE DataFormatter/dal 
  INCLUDE_DIRECTORIES dal ftkcommon DFConfiguration
  CPP_OUTPUT dal_cpp_srcs
  )

tdaq_add_library(ReadoutModule_DataFormatter_dal
 DAL
 ${dal_cpp_srcs}
 INCLUDE_DIRECTORIES dal DFConfiguration ftkcommon
 LINK_LIBRARIES tdaq::config tdaq::daq-core-dal tdaq-common::ers tdaq::daq-df-dal ReadoutModule_FTK_dal )

############################################################
# IS generation
#####
tdaq_generate_isinfo(DataFormatter_is_ISINFO 
  schema/DataFormatter_is.schema.xml
  NAMED 
  NAMESPACE daq::ftk
  OUTPUT_DIRECTORY DataFormatter/dal 
  CPP_OUTPUT cpp_srcs
  JAVA_OUTPUT is_java_srcs
  )

tdaq_generate_isinfo(StatusRegisterDFIMStatus_is_ISINFO 
  schema/StatusRegisterDFIMStatus_is.schema.xml
  NAMED 
  NAMESPACE daq::ftk
  OUTPUT_DIRECTORY DataFormatter/dal
  CPP_OUTPUT cpp_srcs
  JAVA_OUTPUT is_java_srcs
  )
tdaq_generate_isinfo(FtkDataFlowSummaryDF_is_ISINFO 
  schema/FtkDataFlowSummaryDF_is.schema.xml
  NAMED 
  NAMESPACE daq::ftk
  PREFIX ftkcommon/dal
  OUTPUT_DIRECTORY DataFormatter/dal
  CPP_OUTPUT cpp_srcs
  JAVA_OUTPUT is_java_srcs
  )
tdaq_generate_isinfo(FtkDataFlowSummaryIM_is_ISINFO 
  schema/FtkDataFlowSummaryIM_is.schema.xml
  NAMED 
  NAMESPACE daq::ftk
  PREFIX ftkcommon/dal
  OUTPUT_DIRECTORY DataFormatter/dal
  CPP_OUTPUT cpp_srcs
  JAVA_OUTPUT is_java_srcs
  )


tdaq_add_header_directory(${CMAKE_CURRENT_BINARY_DIR}/DataFormatter/info DESTINATION DataFormatter)
tdaq_add_header_directory(${CMAKE_CURRENT_BINARY_DIR}/DataFormatter/dal DESTINATION DataFormatter)

##########################################################
# FtkAPIs library
##########################################################
tdaq_add_library( FtkApis
  src/FtkIPBusApi.cxx 
  src/FtkIMApi.cxx 
  src/FtkDataFormatterApi.cxx 
  src/FtkCoolSchema.cxx 
  src/DataFormatterIPBusRegisterMap.cxx
  #src/FtkDFuhalEmulator.cxx
  DEFINITIONS WORD64BIT
  INCLUDE_DIRECTORIES /afs/cern.ch/atlas/project/tdaq/inst/tdaq/tdaq-07-01-00/uhal/tests/include/
  LINK_LIBRARIES ftkcommon tdaq-common::ers tdaq::daq-df-dal 
                 tdaq::omniORB4 tdaq::omnithread tdaq::cactus_uhal_grammars tdaq::cactus_uhal_log tdaq::cactus_uhal_uhal tdaq::cactus_uhal_tests Boost::program_options
                 COOL::CoolApplication COOL::RelationalCool COOL::CoolKernel CORAL::CoralBase
)

##########################################################
# DataFormatter library
##########################################################
# This is supposed to be a library of functions (or methods of a class)
# that can be used transparently by a standalone application or
# the ReadoutModule (ie. called by the tdaq Run Control).
# So it should be linked by all the applications and
# by the ReadoutModule_DataFormatter library
##########################################################
tdaq_add_library(FtkDataFormatter 
  src/StatusRegisterDFDirect.cxx 
  src/StatusRegisterDFSelector.cxx
  src/StatusRegisterDFSelectorGeneric.cxx
  src/StatusRegisterDFCounter.cxx 
  src/StatusRegisterDFSwitchFifo.cxx 
  src/StatusRegisterDFGT.cxx 
  src/StatusRegisterIMDirect.cxx
  src/StatusRegisterATCAFactory.cxx
  src/DataFlowReaderDF.cxx 
  src/DataFlowReaderIM.cxx
  LINK_LIBRARIES ftkcommon FtkApis ROOT::Core tdaq::cactus_uhal_grammars tdaq::cactus_uhal_log tdaq::cactus_uhal_uhal Boost::program_options
                 COOL::CoolApplication COOL::RelationalCool COOL::CoolKernel CORAL::CoralBase 
                 StatusRegister SpyBuffer tdaq-common::ers  
                 DataFormatter_is_ISINFO StatusRegisterDFIMStatus_is_ISINFO FtkDataFlowSummaryDF_is_ISINFO FtkDataFlowSummaryIM_is_ISINFO 
)                 

#####################################################
## ReadoutModuleDF library
#####################################################
tdaq_add_library(ReadoutModule_DataFormatter
  src/ReadoutModuleDataFormatter.cxx
  src/DataFormatterHistHandler.cxx
  src/DataFormatterConfigHandler.cxx
  ${dal_cpp_srcs}
  DEFINITIONS WORD64BIT
  LINK_LIBRARIES FtkDataFormatter smon_components ROOT::Core tdaq::EMonDataOut DFSpyBuffer tdaq::daq-df-dal tdaq::cactus_uhal_grammars tdaq::cactus_uhal_log 
                 tdaq::cactus_uhal_uhal Boost::program_options tdaq::ROSCore StatusRegister SpyBuffer tdaq-common::ers ftkcommon
                 COOL::CoolApplication COOL::RelationalCool COOL::CoolKernel CORAL::CoralBase ReadoutModule_FTK ReadoutModule_FTK_dal
)

##########################################################
# DataFormatter Spy Buffer library
##########################################################
tdaq_add_library(DFSpyBuffer
  src/DFSpyBuffer.cxx
  LINK_LIBRARIES FtkDataFormatter ftkcommon SpyBuffer tdaq::cactus_uhal_grammars tdaq::cactus_uhal_log tdaq::cactus_uhal_uhal Boost::program_options
                 COOL::CoolApplication COOL::RelationalCool COOL::CoolKernel CORAL::CoralBase
)

#####################################################
## IM tests Libraries 
#####################################################
tdaq_add_library(dfipbusaccess
  src/df_ipbus_access_ok.cc
  LINK_LIBRARIES tdaq::cactus_uhal_grammars tdaq::cactus_uhal_log tdaq::cactus_uhal_uhal Boost::program_options
                 COOL::CoolApplication COOL::RelationalCool COOL::CoolKernel CORAL::CoralBase
)

tdaq_add_library(smon_components
  src/smon_components.cxx
  LINK_LIBRARIES tdaq::cactus_uhal_grammars tdaq::cactus_uhal_log tdaq::cactus_uhal_uhal Boost::program_options
                 COOL::CoolApplication COOL::RelationalCool COOL::CoolKernel CORAL::CoralBase
                 FtkDataFormatter
                 )

#the following library is not built
if(0)
tdaq_add_library(dfipbusaccessmultiboard
  src/df_ipbus_access_multi_board_ok.cc
  LINK_LIBRARIES tdaq::cactus_uhal_grammars tdaq::cactus_uhal_log tdaq::cactus_uhal_uhal Boost::program_options
                 COOL::CoolApplication COOL::RelationalCool COOL::CoolKernel CORAL::CoralBase
                 FtkDataFormatter
                 )
endif(0)

##################################################
## Stand-alone applications 
##################################################
# tdaq_add_executable(test_im
#     src/test_im.cc
#     LINK_LIBRARIES tdaq::cactus_uhal_grammars tdaq::cactus_uhal_log tdaq::cactus_uhal_uhal Boost::program_options
#                    COOL::CoolApplication COOL::RelationalCool COOL::CoolKernel CORAL::CoralBase
#                    smon_components dfipbusaccess
#     )

tdaq_add_executable(test_im_bt
    src/test_im_bt.cc
    LINK_LIBRARIES smon_components FtkApis
    )

tdaq_add_executable(test_df
    src/test_df.cc
    LINK_LIBRARIES smon_components FtkApis
    )

tdaq_add_executable(im_monitor
    src/im_monitor.cc
    LINK_LIBRARIES smon_components ftkcommon StatusRegister tdaq-common::ers tdaq::ipc FtkApis tdaq::emon
    )

tdaq_add_executable(df_monitor
   src/df_monitor.cc
   LINK_LIBRARIES ftkcommon tdaq::emon FtkDataFormatter FtkApis  
  )

tdaq_add_executable(test_RegisterNodeMap
    src/test_RegisterNodeMap.cc
    LINK_LIBRARIES ftkcommon tdaq-common::ers FtkApis  
  )


#tdaq_add_executable(uhal_emulate_lab4
#    src/test_uhal_emulate_lab4.cc
#    INCLUDE_DIRECTORIES /afs/cern.ch/atlas/project/tdaq/inst/tdaq/tdaq-07-01-00/uhal/tests/include/
#    LINK_LIBRARIES COOL::CoolApplication tdaq::cactus_uhal_log tdaq::cactus_uhal_uhal tdaq::cactus_uhal_tests ftkcommon FtkApis
#  )

###################################
##  Installing schema 
###################################
tdaq_add_schema(schema/*.xml DESTINATION schema)

###################################
##  Installing IS schema 
###################################
tdaq_add_is_schema(schema/DataFormatter_is.schema.xml DESTINATION schema)

tdaq_add_is_schema(schema/StatusRegisterDFIMStatus_is.schema.xml DESTINATION schema)

tdaq_add_is_schema(schema/FtkDataFlowSummaryDF_is.schema.xml DESTINATION schema)

tdaq_add_is_schema(schema/FtkDataFlowSummaryIM_is.schema.xml DESTINATION schema)


