#include "DataFormatter/StatusRegisterDFSelectorGeneric.h"

using namespace daq::ftk;

StatusRegisterDFSelectorGeneric::StatusRegisterDFSelectorGeneric( std::shared_ptr<FtkDataFormatterApi> dfapi,
  uint32_t selectorValue,
  uint32_t selectorAddress,
  uint32_t readerAddress )
  : StatusRegisterDFSelector::StatusRegisterDFSelector( dfapi, srType::srOther )
{
  m_write_nodes.push_back( dfapi->getRegisterNameFromAddress( selectorAddress ) );
  m_write_values.push_back( selectorValue );
  m_node = dfapi->getRegisterNameFromAddress( readerAddress );
}
