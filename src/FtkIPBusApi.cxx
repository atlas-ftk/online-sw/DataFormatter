#include <DataFormatter/FtkIPBusApi.h>
#include "ftkcommon/exceptions.h"

using namespace daq; using namespace ftk;

const uint32_t FtkIPBusApi::m_MaxSize = 100;

// ====================================================
FtkIPBusApi::FtkIPBusApi(const std::string& connection_file, 
			     const std::string& device_id)
  : m_connection_file(connection_file), m_device_id(device_id), m_timeout_period(1000),
    m_checkRegisterMap( false )
{
  // ERS_LOG("FtkIPBusApi::constructor: Entered");
  ERS_DEBUG(1,"FtkIPBusApi::constructor: Entered");
  uhal::setLogLevelTo(uhal::Warning());
  connection_inst();  
}

FtkIPBusApi::FtkIPBusApi(const std::string& connection_file, 
			      const std::string& device_id,
            const RegisterNodeMap& map)
  : m_connection_file(connection_file), m_device_id(device_id), m_timeout_period(1000),
    m_checkRegisterMap( true )
{
  // ERS_LOG("FtkIPBusApi::constructor: Entered");
  ERS_DEBUG(1,"FtkIPBusApi::constructor: Entered");

  m_registerAddressMap.clear();
  for ( const auto& entry : map ) {
    std::string name;
    getRegisterNodeName( entry.second, name );
    m_registerAddressMap[ "reg."+name ] = entry.first;
    std::vector<RegisterNodeMask> masks;
    getRegisterNodeMasks( entry.second, masks );
    for ( auto& mask : masks ) {
      m_registerAddressMap[ "reg."+name+"."+mask.first ] = entry.first;
    }
  }

  uhal::setLogLevelTo(uhal::Warning());
  connection_inst();  
}


// ====================================================
void
FtkIPBusApi::connection_inst()
{
  // ERS_LOG("FtkIPBusApi::connection_inst: Entered");
  ERS_DEBUG(1,"FtkIPBusApi::connection_inst: Entered");
  m_manager = new uhal::ConnectionManager( m_connection_file );
  m_hw      = new uhal::HwInterface(m_manager->getDevice(m_device_id));
  m_hw->setTimeoutPeriod(m_timeout_period);
}

// ====================================================
void 
FtkIPBusApi::send_transaction_to_write()
{
  std::map<std::string, uint32_t>::const_iterator itc = m_write_register_buffer.begin();
  const std::map<std::string, uint32_t>::const_iterator ite = m_write_register_buffer.end();
  for (; itc!=ite; itc++) {
    const std::string& nodename = itc->first;
    const uint32_t&    value    = itc->second;
    m_hw->getNode(nodename).write(value);
  }
  
  m_hw->dispatch(); // send transaction  
}

// ====================================================
void
FtkIPBusApi::send_transaction_to_read()
{
  uhal::ValWord< uint32_t > mems[FtkIPBusApi::m_MaxSize];
  if (FtkIPBusApi::m_MaxSize<m_read_register_buffer.size()) {
    char error_msg[BUFSIZ];
    snprintf(error_msg, sizeof(error_msg), 
	     "Exception: m_MaxSiz=%3d : #registers=%3d \n",
	     FtkIPBusApi::m_MaxSize,
	     (int)m_read_register_buffer.size());
    throw daq::ftk::IPBusRead(ERS_HERE, error_msg);
  }
  const std::map<std::string, uint32_t>::iterator ite  = m_read_register_buffer.end();
  std::map<std::string, uint32_t>::iterator       itc  = m_read_register_buffer.begin();
  int iregs=0;
  for (; itc!=ite; itc++) {
    const std::string& nodename = itc->first;
    mems[iregs] = m_hw->getNode(nodename).read();
    iregs++;
  }
  m_hw->dispatch(); // send transaction  
  iregs=0;
  itc  = m_read_register_buffer.begin();
  for (; itc!=ite; itc++) {
    itc->second = mems[iregs].value();
    iregs++;
  }
}

// ====================================================
void 
FtkIPBusApi::fill_write_node_for_IM_BT()
{
  std::map<std::string, uint32_t>::const_iterator itc = m_write_register_buffer.begin();
  const std::map<std::string, uint32_t>::const_iterator ite = m_write_register_buffer.end();
  for (; itc!=ite; itc++) {
    const std::string& nodename = itc->first;
    const uint32_t&    value    = itc->second;
    m_hw->getNode(nodename).write(value);
  }
  // m_hw->dispatch(); // send transaction  
}

// ====================================================
bool
FtkIPBusApi::add_register_to_write(std::string nodename, uint32_t value)
{
  if ( m_checkRegisterMap && ( m_registerAddressMap.find( nodename ) == m_registerAddressMap.end() ) ) {
    throw daq::ftk::IPBusRead(ERS_HERE,"no such nodename: " + nodename);
    return 1;
  }

  bool rc=m_write_register_buffer.insert(std::pair<std::string, uint32_t>(nodename, value)).second;
  if (!rc) {
    throw daq::ftk::IPBusRead(ERS_HERE,"duplication of write register");
    return rc;
  }
  return rc;
}

// ====================================================
bool
FtkIPBusApi::add_register_to_read (std::string nodename)
{
  if ( m_checkRegisterMap && ( m_registerAddressMap.find( nodename ) == m_registerAddressMap.end() ) ) {
    throw daq::ftk::IPBusRead(ERS_HERE, "no such nodename: " + nodename);
    return 1;
  }

  bool rc=m_read_register_buffer.insert(std::pair<std::string, uint32_t>(nodename, 0x0)).second;
  if (!rc) {
    throw daq::ftk::IPBusRead(ERS_HERE, "duplication of read register");
    return rc;
  }
  return rc;
}

// ====================================================
uint32_t
FtkIPBusApi::get_readback_register_value(std::string nodename)
{
  if (m_read_register_buffer.find(nodename)==m_read_register_buffer.end()) {
    char error_msg[BUFSIZ];
    snprintf(error_msg, sizeof(error_msg), 
	     "Not registered node %s\n",
	     nodename.c_str());
    throw daq::ftk::IPBusRead(ERS_HERE, error_msg);
    return 0X0;
  } else {
    return m_read_register_buffer[nodename];
  }
}

// ====================================================
void
FtkIPBusApi::single_access_write(const std::string& nodename, 
				 const uint32_t& value) 
{
  if ( m_checkRegisterMap && ( m_registerAddressMap.find( nodename ) == m_registerAddressMap.end() ) ) {
    throw daq::ftk::IPBusRead(ERS_HERE," no such nodename: " + nodename);
    return;
  }

  m_hw->getNode (nodename).write(value);
  m_hw->dispatch(); // send transaction  
}

// ====================================================
void
FtkIPBusApi::single_access_write2(const std::string& nodename, 
				  const uint32_t& value) 
{
  if ( m_checkRegisterMap && ( m_registerAddressMap.find( nodename ) == m_registerAddressMap.end() ) ) {
    throw daq::ftk::IPBusRead(ERS_HERE,"no such nodename: " + nodename);
    return;
  }

  m_hw->getNode (nodename).write(value);
  //  m_hw->dispatch(); // send transaction  
}

// ====================================================
bool
FtkIPBusApi::single_access_read(const std::string& node, 
				  uint32_t& value)
{
  if ( m_checkRegisterMap && ( m_registerAddressMap.find( node ) == m_registerAddressMap.end() ) ) {
    throw daq::ftk::IPBusRead(ERS_HERE, "no such nodename: " + node);
    return false;
  }

  uhal::ValWord< uint32_t > mem = m_hw->getNode ( node ).read();
  m_hw->dispatch(); // send transaction
  value = mem.value();
  if (!mem.valid()) {
    throw daq::ftk::IPBusRead(ERS_HERE, "Invalid read access");
    return false;
  }
  return true;
}

// ====================================================
bool
FtkIPBusApi::single_access_block_read(const std::string& node,
				      std::vector<uint32_t>& value, uint32_t size)
{
  if ( m_checkRegisterMap && ( m_registerAddressMap.find( node ) == m_registerAddressMap.end() ) ) {
    throw daq::ftk::IPBusRead(ERS_HERE, "no such nodename: " + node);
    return false;
  }

  uhal::ValVector< uint32_t > mem = m_hw->getNode ( node ).readBlock(size);
  m_hw->dispatch(); // send transaction
  value = mem.value();
  if (!mem.valid()) {
    throw daq::ftk::IPBusRead(ERS_HERE, "Invalid read access");
    return false;
  }
  return true;
}

//===============================
bool
FtkIPBusApi::read_32bit_counter(const uint32_t& type_id,
				const uint32_t& lane_id, 
				uint32_t& dout)
{
  bool rc = true;
  single_access_write("reg.readout_32bit_counter_lane_selector.type", type_id);
  
  single_access_write("reg.readout_32bit_counter_lane_selector.lane", lane_id);
  
  try {
    single_access_read("reg.readout_32bit_counter", dout);
  }
  
  catch (daq::ftk::IPBusRead ex) {
    throw daq::ftk::IPBusRead(ERS_HERE,"Failed reading 32 bit counter: IPBus error when reading",ex);
    return false;
  }
  
  return rc;
}


// ====================================================
bool
FtkIPBusApi::read_using_selector(const std::vector<std::string>& in_node, const std::vector<uint32_t>& din,
			         const std::string& out_node,  uint32_t& dout)
{
  if(in_node.size() != din.size())
  {
    throw daq::ftk::IPBusRead(ERS_HERE,"Failed reading using selector: mismatch in length");
    return false;
  }

  for(uint32_t i=0; i<in_node.size(); i++)
  {
    single_access_write(in_node[i], din[i]);
  }
 
  try {
    single_access_read(out_node, dout);
  } catch (daq::ftk::IPBusRead ex) {
    throw daq::ftk::IPBusRead(ERS_HERE,"Failed reading using selector: IPBus error when reading",ex);
    return false;
  }


  return true;  
}

// ====================================================
bool
FtkIPBusApi::read_using_selector(const std::string& in_node , const uint32_t& din,
			   const std::string& out_node,  uint32_t& dout)
{
  single_access_write(in_node, din);
 
  try {
    single_access_read(out_node, dout);
  } catch (daq::ftk::IPBusRead ex) {
    throw daq::ftk::IPBusRead(ERS_HERE,"Failed reading using selector: IPBus error when reading",ex);
    return false;
  }
  
  return true;  
}


// ====================================================
std::string 
FtkIPBusApi::getRegisterNameFromAddressAndMask(const uint32_t& addr, const uint32_t& mask)
{
  for(std::string str : m_hw->getNodes())
  {
    if( (m_hw->getNode(str).getAddress() == addr) && (m_hw->getNode(str).getMask() == mask) )
      return str;
  }
  ERS_LOG("Didn't find register with address " << hex << addr << " and mask "<< hex << mask <<". Return empty string...");
  return "";
}

// ====================================================
std::string 
FtkIPBusApi::getRegisterNameFromAddress(const uint32_t& addr)
{
  return getRegisterNameFromAddressAndMask(addr, 0xffffffff);
}


