#include <DataFormatter/StatusRegisterIMDirect.h>
#include "ftkcommon/exceptions.h"

#include <unistd.h>//tomoya for usleep

using namespace daq::ftk;

StatusRegisterIMDirect::StatusRegisterIMDirect(std::shared_ptr<FtkIMApi> im, uint32_t fpga, uint32_t address, srType type ) : m_im( im ), m_fpga( fpga ) {
  // initialize parent class protected members
  m_address = address;
  m_access = srAccess::i2cIPBus;
  m_type = type;
}

// ====================================================
void StatusRegisterIMDirect::readout(){ 
  
}

std::vector<uint32_t>  StatusRegisterIMDirect::readoutBlockTransfer(uint32_t blockSize) {
  
  std::vector<uint32_t> vov;       // raw data read from IM->DF->vov
  //const uint32_t lane_id = 52;     // lane number for IM-DF block transfer, Masahiro commented out 2017/12/05
  //  uint32_t depth   = blockSize;    // #SMON words
  float_t sleepTime = 0;
  
  // to be fixed
  // float_t sleepTime = (float(blockSize) / 4096.) * 5.5 * 1e5;
  
  // To do: difine sleepTime by blockSize / N.
  if(blockSize == 256)       sleepTime = 1e5;
  else if(blockSize == 4096) sleepTime = 5.5 * 1e4;
  
  try {
    if( m_address == 0x0 ){ //  this is a IM FW version
      uint32_t tmp_fw_ver  = 0x0;
      uint32_t tmp_fw_type = 0xDEADBEEF;
      uint32_t tmp_fw_hash = 0xDEADBEEF;
      uint32_t tmp_fw_date = 0xDEADBEEF;
      if( m_im->read_register( m_fpga * 2, "GA_FW_VER", tmp_fw_ver, 0x0  ) ){
        if( (tmp_fw_ver & 0x3FFF) > 0x0130 ) { // this is refactored fw
          m_im->read_register( m_fpga * 2, "GA_FW_TYPE", tmp_fw_type, 0x0  ) ;
          m_im->read_register( m_fpga * 2, "GA_FW_HASH", tmp_fw_hash, 0x0  ) ;
          m_im->read_register( m_fpga * 2, "GA_FW_DATE", tmp_fw_date, 0x0  );
        }
      }
      vov.push_back( tmp_fw_ver );
      vov.push_back( tmp_fw_type );
      vov.push_back( tmp_fw_hash );
      vov.push_back( tmp_fw_date );
      
    }else {
      //"IA_CH0_COMMON_SPY_REG_ISPY"
      m_im->send_freeze( m_fpga, true );
      m_im->read_from_IMBuffer_to_DFBuffer( m_fpga/* acutally this is m_channel */, m_address, 0/* ireadout */, false/* is_apply_freeze */, blockSize/* df_buffer_depth */ );
      m_im->read_from_DFBuffer_block(blockSize, vov);
      m_im->send_freeze( m_fpga, false );
    }
  } catch (daq::ftk::i2cIPBusIssue ex) { 
    m_value = srNoneValue;
    daq::ftk::i2cIPBusIssue ex2(ERS_HERE, m_name, " Failed to read out a IM status register",ex);
    ers::warning(ex2);
  }
  
  return vov;
}
