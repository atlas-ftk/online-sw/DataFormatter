// written by yasuyuki.okumura@cern.ch 
// updated by mitani.takashi@cern.ch
// reharbished by Tomoya.Iizawa@cern.ch
// refactored by Masahiro.Morinaga@cern.ch

// #include <DataFormatter/df_ipbus_access_ok.hh>
#include <DataFormatter/FtkIPBusApi.h>
#include <DataFormatter/FtkIMApi.h>
#include <uhal/log/log.hpp>
#include <DataFormatter/smon_components.h>
#include "ftkcommon/exceptions.h"

#include <string>
#include <fstream>
#include <sstream>

#include <unistd.h>//tomoya

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <time.h>

#include <sys/stat.h>

using namespace uhal;
using namespace daq::ftk;


const int nretry = 10;
bool debug = false;

void print_usage (const std::string& default_connection_file) {
  printf("-h          : produce help message \n");
  printf("-M          : mode (see below) \n");
  printf("-C <string> : connection file URI (default %s) \n", default_connection_file.c_str());
  printf("-D <string> : device ID  \n");  
  printf("-d          : loglevel=debug (default loglevel=warning) \n");
  printf("-A          : not release freeze of the spy(default is false)  \n");
  printf("-f          : not check fw version  \n");
  printf("----------------------------------- \n");
  printf("MODE options \n");
  printf("  1: I2C state machine reset     -F <FPGA ID (0-7)> \n");
  printf("  2: Reaback firmware version   -F <FPGA ID (0-7)> \n");
  printf("  3: Readback IM SPY buffer      -F <FPGA ID (0-7)> -N <channel ID (0-1)> -S <Input(0) or Output(1)> \n");
  printf("  4: Readback IM Status Monitor  -F <FPGA ID (0-7)> -N <channel ID (0-1)> -V <0:default(full table), 1:just one line(w/ label), 2:just one line(w/o label)>\n");  
  printf("  5: Download LUT                -F <FPGA ID (0-7)> -N <channel ID (0-1)> -T <LUT.txt>\n");
  printf("  6: Send pseudo control word    -F <FPGA ID (0-7)> -N <channel ID (0-1)> -P <0: both off, 0x1: pseudo_data_mode, 0x2: pseudo_data_ena, 0x3: both on>\n");
  printf("  7: Download pseudo data        -F <FPGA ID (0-7)> -N <channel ID (0-1)> -I <pseudoData.txt> -V <Input Rate(default is 100kHz)>\n");
  printf("  8: Treatment of BP from DF     -F <FPGA ID (0-7)> -N <channel ID (0-1)> -V <0: default, 1: ignore BP>\n");
  printf("  9: Treatment of LUP of SLINK   -F <FPGA ID (0-7)> -N <channel ID (0-1)> -V <0: default, 1: ignore LUP>\n");
  printf(" 10: Treatment of sending XOFF   -F <FPGA ID (0-7)> -N <channel ID (0-1)> -V <0: default, 1: send XOFF>\n");
  printf(" 11: Send freeze signal          -F <FPGA ID (0-7)> -V <0: freeze off, 1: freeze on>\n");
  printf(" 12: control slnk                -F <FPGA ID (0-7)> -V <0: disanable, 1: enable >\n");
  printf("100: read register               -F <FPGA ID (0-7)> -N <channel ID (0-1)> -R <internal register name (string)>\n");
  printf("101: set register                -F <FPGA ID (0-7)> -N <channel ID (0-1)> -R <inernal register name (string)> -W <value to write (e.g. 0x00000001)>\n");
}

bool read_spy_buffer( uint32_t channel, uint32_t inspy_ospy, std::shared_ptr<FtkIMApi> m_im, bool is_not_rel_freeze ){
  std::string im_spy_register_name, im_common_spy_register_name, in_or_out_spy;
  if( inspy_ospy == 0 ){
    in_or_out_spy = "inspy";
    if( channel%2 == 0) { im_spy_register_name = "IA_CH0_ISPY_BEGIN"; im_common_spy_register_name = "IA_CH0_COMMON_SPY_REG_ISPY";  }
    if( channel%2 == 1) { im_spy_register_name = "IA_CH1_ISPY_BEGIN"; im_common_spy_register_name = "IA_CH1_COMMON_SPY_REG_ISPY"; }
  }else if( inspy_ospy == 1 ){
    in_or_out_spy = "outspy";
    if( channel%2 == 0) { im_spy_register_name = "IA_CH0_OSPY_BEGIN";  im_common_spy_register_name = "IA_CH0_COMMON_SPY_REG_OSPY"; }
    if( channel%2 == 1) { im_spy_register_name = "IA_CH1_OSPY_BEGIN";  im_common_spy_register_name = "IA_CH1_COMMON_SPY_REG_OSPY"; }
  }
  std::vector<uint32_t> dout_4k; dout_4k.reserve( 1024*4 );
  m_im->send_freeze( channel, true );
  uint32_t im_common_spy_reg;
  m_im->read_register( channel, im_common_spy_register_name, im_common_spy_reg );

  printf("IM ch%d : common spy reg is 0x%08x\n", channel, im_common_spy_reg );
  // loop for 1k x 4 
  for( uint32_t iread = 0; iread < 4; ++iread ){
    try {
      m_im->read_from_IMBuffer_to_DFBuffer( channel, im_spy_register_name, iread, false, 1024, true );
    } catch ( daq::ftk::i2cIPBusWrite ex ) { std::cout << ex.what() << std::endl; return EXIT_FAILURE;
    } catch ( daq::ftk::i2cIPBusRead  ex ) { std::cout << ex.what() << std::endl; return EXIT_FAILURE; }
    
    std::vector<uint32_t> dout_1k; dout_1k.reserve( 1024 );
    m_im->read_from_DFBuffer_block( 1024, dout_1k );
    std::move( dout_1k.begin(), dout_1k.end(), std::back_inserter( dout_4k ) );
  }
  uint32_t write_address = (im_common_spy_reg & 0x0000ffff );
  for( uint32_t idx = write_address+1; idx < 4*1024; ++idx ) printf( "[IM %6s ch%d] Address : 0x%03x -- 0x%08x\n", in_or_out_spy.c_str(), channel, idx, dout_4k.at( idx ) );
  for( uint32_t idx =      0; idx < write_address+1; ++idx ) printf( "[IM %6s ch%d] Address : 0x%03x -- 0x%08x\n", in_or_out_spy.c_str(), channel, idx, dout_4k.at( idx ) );
  
  if ( ! is_not_rel_freeze ) m_im->send_freeze( channel, false );
  return true;
}

bool read_status_monitor( uint32_t channel, std::shared_ptr<FtkIMApi> m_im, bool is_not_rel_freeze, uint32_t value ){
  std::string im_smon_register_name = (channel%2 == 0) ? "IA_CH0_SMON_BEGIN" : "IA_CH1_SMON_BEGIN";
  m_im->send_freeze( channel, true );
  try {
    m_im->read_from_IMBuffer_to_DFBuffer( channel, im_smon_register_name, 0, false, 256, true );
  } catch ( daq::ftk::i2cIPBusWrite ex ) { std::cout << ex.what() << std::endl; return EXIT_FAILURE;
  } catch ( daq::ftk::i2cIPBusRead  ex ) { std::cout << ex.what() << std::endl; return EXIT_FAILURE;
  }
  std::vector<uint32_t> smon_vec; smon_vec.reserve( 256 );
  m_im->read_from_DFBuffer_block( 256, smon_vec );
  smon_components smon_interface;
  smon_interface.read_32bitdata( smon_vec );
  smon_interface.read_vector();
  m_im->set_fw_version( channel );
  
  if( value == 0 ){
    smon_interface.print_full( channel );
    smon_interface.print_raw_text();
  }else if( value == 1 ){
    smon_interface.print_one_liner( true, channel );
  }else{
    smon_interface.print_one_liner( false, channel );
  }
  if ( ! is_not_rel_freeze ) m_im->send_freeze( channel, false );
  return true;
}

int main(int argc, char* argv[]) {
  // check whether environment variable "DF_IPUBUS_CONFIG" is properly set or not.
  struct stat st;
  int ret = stat(getenv("DF_IPUBUS_CONFIG"),&st);
  if(ret!=0){
    fprintf(stderr, "define proper environment variable DF_IPUBUS_CONFIG with setup.sh in DataFormatter package. \n");
    return EXIT_FAILURE;
  }

  uhal::setLogLevelTo ( Warning() );
  
  //bool rc = false; //unused
  const std::string default_connection_file = std::string(getenv("DF_IPUBUS_CONFIG")) + "/connections.xml";
  std::string connection_file = std::string("file://") + default_connection_file;
  std::string device_id("");
  std::string register_name("");
  std::string LUT_file("");
  std::string pseudoData_file("");
  
  int mode=0X0;
  int c = 0X0;
  uint32_t fpga_id = 0X8;
  uint32_t channel_id = 0X0;
  uint32_t value_to_write = 0Xdeadbeef;
  uint32_t inspy_ospy = 0X0;
  uint32_t pseudo_control = 0X0;
  uint32_t value = 0X0;
  
  bool is_not_rel_freeze = false;
  while ((c = getopt (argc, argv, "hC:D:dM:F:R:W:S:N:T:P:I:B:U:V:A:f")) != -1){
    switch (c) {
    case 'M' : mode              = strtoul(optarg, NULL, 0); break;
    case 'C' : connection_file   = std::string("file://") + optarg; break;
    case 'D' : device_id         = optarg; break;
    case 'A' : is_not_rel_freeze = true; break;
    case 'F' : fpga_id           = strtoul(optarg, NULL, 0); break;
    case 'R' : register_name     = optarg; break;
    case 'W' : value_to_write    = strtoul(optarg, NULL, 0); break;
    case 'S' : inspy_ospy        = strtoul(optarg, NULL, 0); break;
    case 'N' : channel_id        = strtoul(optarg, NULL, 0); break;
    case 'T' : LUT_file          = optarg; break;
    case 'P' : pseudo_control    = strtoul(optarg, NULL,0); break;
    case 'I' : pseudoData_file   = optarg; break;
    case 'V' : value             = strtoul(optarg, NULL,0); break;
    case 'd' : debug             = true; uhal::setLogLevelTo ( Debug() ); break;
    case 'h' : print_usage(default_connection_file); return EXIT_SUCCESS;
    default  : print_usage(default_connection_file); return EXIT_FAILURE; 
    }
    
  }

  uint32_t channel = fpga_id * 2 + channel_id;
  
  if (device_id == "" || fpga_id > 0x7 || channel_id > 0x1 ) {
    print_usage(default_connection_file);
    return EXIT_FAILURE;    
  }
  
  std::shared_ptr<FtkIPBusApi> m_ipbus( new FtkIPBusApi(connection_file, device_id) );
  std::shared_ptr<FtkIMApi> m_im( new FtkIMApi(m_ipbus, device_id) );
  m_im->set_fw_version( channel );
  
  if      ( mode ==   1 ) { /* m_im->sendIMCounterReset( fpga_id ); */ }
  else if ( mode ==   2 ) { m_im->print_fw_verion( fpga_id, false, 0xFACEDEAD, true ); }
  else if ( mode ==   3 ) { read_spy_buffer( channel, inspy_ospy, m_im, is_not_rel_freeze ); }
  else if ( mode ==   4 ) { read_status_monitor( channel, m_im, is_not_rel_freeze, value ); }
  else if ( mode ==   5 ) { m_im->downloadIMLUT( channel, LUT_file ); }
  else if ( mode ==   6 ) { m_im->control_pseudo_data( channel, ( (pseudo_control & 0x2) >> 0x1), ( pseudo_control & 0x1)  ); }
  else if ( mode ==   7 ) { m_im->download_pseudo_data( channel, pseudoData_file, value != 0x0 ? (int)value : 100); 
  }
  else if ( mode ==   8 ) { m_im->control_global_register( channel, std::string("GA_CONTROL_REG:IGNORE_HOLD"), value ); }
  else if ( mode ==   9 ) { m_im->control_global_register( channel, std::string("GA_CONTROL_REG:SLINK_ENABLE"), value ); }
  else if ( mode ==  10 ) { m_im->control_xoff( channel, value ); printf( "IM xoff signal -> 0x%02x\n", value ); }
  else if ( mode ==  11 ) { m_im->send_freeze( channel, value ); printf( "sent IM freeze signal -> 0x%02x\n", value ); }
  else if ( mode ==  12 ) { m_im->control_slink( channel, value ); printf( "sent slink enable -> 0x%02x\n", value ); }
  else if ( mode == 100 ) { m_im->read_register(  channel, register_name, value ); printf( "read  IM internal register(%20s) : 0x%08x\n", register_name.c_str(), value ); }
  else if ( mode == 101 ) { m_im->set_register( channel, register_name, value_to_write ); printf( "write IM internal register(%20s) : 0x%08x\n", register_name.c_str(), value_to_write ); }
  return EXIT_SUCCESS;
}

