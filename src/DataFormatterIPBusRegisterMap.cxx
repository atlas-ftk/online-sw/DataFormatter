
#include "ftkcommon/exceptions.h"
#include "ers/ers.h"

#include "DataFormatter/DataFormatterIPBusRegisterMap.h"

using namespace daq; using namespace ftk;

void daq::ftk::writeRegisterNodeMap( const RegisterNodeMap& map, const std::string& fname, const std::string& name ) {
  ofstream outfile;
  outfile.open( fname, ios::out | ios::trunc );

  if ( outfile.is_open() ) {
    outfile << "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>" << std::endl
            << std::endl
            << "<node id=\"DF\">" << std::endl
            << "  <node id=\"reg\" address=\"0x00000000\">" << std::endl;

    for ( const auto& entry : map ) {
      uint32_t address = entry.first;
      const RegisterNode& node = entry.second;

      std::string name, mode;
      uint32_t size;
      std::vector<RegisterNodeMask> masks;
      getRegisterNodeName( node, name );
      getRegisterNodeMode( node, mode );
      getRegisterNodeSize( node, size );
      getRegisterNodeMasks( node, masks );

      if ( mode.length() != 0 ) {
        outfile << "      <node id=\"" << name << "\" address=\"0x" << std::hex << address << "\" mode=\"" << mode << "\" size=\"0x" << std::hex << size << "\"/>" << std::endl;
      } else if ( masks.size() > 0 ) { 
        outfile << "      <node id=\"" << name << "\" address=\"0x" << std::hex << address << "\">" << std::endl;
        for ( RegisterNodeMask& mask : masks ) {
          outfile << "          <node id=\"" << mask.first << "\" mask=\"0x" << std::hex << mask.second << "\"/>" << std::endl;
        }
        outfile << "      </node>" << std::endl;
      } else {
        outfile << "      <node id=\"" << name <<"\" address=\"0x" << std::hex << address << "\"/>" << std::endl;
      }
    }

    outfile << "  </node>" << std::endl
            << "</node>" << std::endl;

    outfile.close();
  } else {
    ers::warning( IOError( ERS_HERE, name, " could not open file to write register node map: " + fname ) );
  }
}
