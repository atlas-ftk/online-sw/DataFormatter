
#include "DataFormatter/DataFormatterIPBusRegisterMap.h"

using namespace daq; using namespace ftk;

int main( int argc, char **argv ) {
  RegisterNodeMap map;
  std::string df_name = "DF20";
  makeRegisterNodeMap( map, 0x1803E );
  writeRegisterNodeMap( map, "1803E_test_address_df.xml" , df_name );

  makeRegisterNodeMap( map, 0x19000 );
  writeRegisterNodeMap( map, "19000_test_address_df.xml" , df_name );

  return 0;
}
