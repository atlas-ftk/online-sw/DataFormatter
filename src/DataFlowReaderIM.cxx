#include "DataFormatter/DataFlowReaderIM.h"
#include "ftkcommon/ReadSRFromISVectors.h"

#include "ftkcommon/Utils.h"


//OnlineServices to check if running within a partition
#include <ipc/partition.h>
#include <ipc/core.h>


// ers
#include <ers/ers.h>
#include "ftkcommon/exceptions.h"

#include <iostream>
#include <sstream>


using namespace std;
using namespace daq::ftk;

/************************************************************/
DataFlowReaderIM::DataFlowReaderIM(std::shared_ptr<OHRawProvider<> > ohRawProvider, uint32_t im_fwVersion, bool forceIS) 
  : DataFlowReader(ohRawProvider, forceIS), m_im_fwVersion(im_fwVersion)
/************************************************************/
{
}

/***************************************************************************************************************/
void DataFlowReaderIM::init(const string& deviceName, const string& partitionName, const string& isServerName)
/***************************************************************************************************************/
{
  DataFlowReader::init(deviceName, partitionName, isServerName);
  DataFlowReader::name_dataflow();
  m_theIMSummary = std::make_shared<FtkDataFlowSummaryIMNamed>(m_ipcp,name_dataflow());
  m_theSummary= std::static_pointer_cast<FtkDataFlowSummaryNamed>(m_theIMSummary);
      
  ERS_LOG("Initializing IM dataflowsummary: "<<name_dataflow()<<" :pointer");
  	
}


/************************************************************/
vector<string> DataFlowReaderIM::getISVectorNames()
/************************************************************/
{
  vector<string> srv_names;
  stringstream name;

  for (int i = 0; i < 16; i++)
  {
    stringstream name;
    name << "IM"<< i << "_sr_v";
    srv_names.push_back(name.str());
  }

  return srv_names;
}

/************************************************************/
void DataFlowReaderIM::publishExtraHistos(uint32_t option)
/************************************************************/
{
  std::vector<int64_t> srv;
  std::vector<float> srvf;

  srv.clear(); 
  getNInputTruncation(srv);
  publishSingleTH1F(srv,"NInputTruncation");
  m_theIMSummary->NInputTruncation = srv;
  
  srvf.clear(); 
  getInputTruncationFraction(srvf);
  publishSingleTH1F(srvf,"InputTruncationFraction");
  m_theIMSummary->InputTruncationFraction = srvf;

  srv.clear(); 
  getNRecover(srv);
  publishSingleTH1F(srv,"NRecover");
  m_theIMSummary->NRecover = srv;

  srvf.clear(); 
  getRecoverFraction(srvf);
  publishSingleTH1F(srvf,"RecoverFraction");
  m_theIMSummary->RecoverFraction = srvf;

  srvf.clear(); 
  getBPFractionFromDF(srvf);
  publishSingleTH1F(srvf,"BPFractionFromDF");
  m_theIMSummary->BPFractionFromDF = srvf;

  srvf.clear(); 
  getNFifoOverflow(srvf);
  publishSingleTH1F(srvf,"NFifoOverflow");
  m_theIMSummary->NFifoOverflow = srvf;
  
  srvf.clear(); 
  getOverflowFraction(srvf);
  publishSingleTH1F(srvf,"OverflowFraction");
  m_theIMSummary->OverflowFraction = srvf;

}

/************************************************************/
float DataFlowReaderIM::inputFunction(int kchannel, uint32_t address_input, bool dealing_fraction, uint32_t  b_option)
/************************************************************/
{
  float output = -1;
  int jfpga = kchannel; 
  uint32_t address_channel;
  address_channel = (kchannel%2==0) ? 0x0008 : 0x000C; 
  if (dealing_fraction){
    output = (readRegister(merge2x16bIn32b(address_channel,address_input),jfpga,0,0))/(b_option*1e4);
  }
  else {
    output = readRegister(merge2x16bIn32b(address_channel,address_input),jfpga,0,0);
      }
  return output;
}

/************************************************************/
uint32_t DataFlowReaderIM::inputFunctionInt( int kchannel, uint32_t address_input )
/************************************************************/
{
  int jfpga = kchannel;
  uint32_t address_channel = (kchannel%2==0) ? 0x0008 : 0x000C;
  return readRegister(merge2x16bIn32b(address_channel,address_input),jfpga,0,0);
}


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/************************************************************/
void DataFlowReaderIM::getFifoInBusy(vector<int64_t>& srv)
/************************************************************/
{
  if( DataFlowReaderIM::is_refactord_fw() ){
    for (int ichannel=0; ichannel<16; ichannel++){
      srv.push_back(daq::ftk::getBit(inputFunctionInt(ichannel, 21),30)); // in fifo almost full flag
    }
  }else{
  for (int ichannel=0; ichannel<16; ichannel++){
    srv.push_back(daq::ftk::getBit(inputFunctionInt(ichannel,0x30),28));
  }
  } // is refactored 
}

/************************************************************/
void DataFlowReaderIM::getFifoOutBusy(vector<int64_t>& srv)
/************************************************************/
{
  if( DataFlowReaderIM::is_refactord_fw() ){
    for (int ichannel=0; ichannel<16; ichannel++){
      srv.push_back(daq::ftk::getBit(inputFunctionInt(ichannel, 33),30)); // in fifo almost full flag
    }
  }else{
  for (int ichannel=0; ichannel<16; ichannel++){
    srv.push_back(daq::ftk::getBit(inputFunctionInt(ichannel,0x35),28));
  }
  } // is refactored 
}

/************************************************************/
void DataFlowReaderIM::getFifoInEmpty(vector<int64_t>& srv)
/************************************************************/
{
  if( DataFlowReaderIM::is_refactord_fw() ){
    for (int ichannel=0; ichannel<16; ichannel++){
      srv.push_back(daq::ftk::getBit(inputFunctionInt(ichannel, 21),29)); // in fifo empty flag
    }
  }else{
  for (int ichannel=0; ichannel<16; ichannel++){
    srv.push_back(daq::ftk::getBit(inputFunctionInt(ichannel,0x30),27));
  }
  } // is refactored 
}

/************************************************************/
void DataFlowReaderIM::getFifoOutEmpty(vector<int64_t>& srv)
/************************************************************/
{
  if( DataFlowReaderIM::is_refactord_fw() ){
    for (int ichannel=0; ichannel<16; ichannel++){
      srv.push_back(daq::ftk::getBit(inputFunctionInt(ichannel, 33),29)); // in fifo almost full flag
    }
  }else{
  for (int ichannel=0; ichannel<16; ichannel++){
    srv.push_back(daq::ftk::getBit(inputFunctionInt(ichannel,0x35),27));
  }
  } // is refactored 
}
/************************************************************/
void DataFlowReaderIM::getFifoInBusyFraction(vector<float>& srv, uint32_t)
/************************************************************/
{

  if( DataFlowReaderIM::is_refactord_fw() ){
    for (int ichannel=0; ichannel<16; ichannel++){
      srv.push_back( float(inputFunction(ichannel, 24, false,0))/float( 10 * 40*1000000) );  // 10sec * 40MHz
    }
  }else{
  //  uint32_t b = (option==0) ? 40 : 2400;
  for (int ichannel=0; ichannel<16; ichannel++){
    srv.push_back(inputFunction(ichannel,0x19,true,4000));
  }
  } // is refactored 
}

/************************************************************/
void DataFlowReaderIM::getFifoOutBusyFraction(vector<float>& srv, uint32_t)
/************************************************************/
{
  if( DataFlowReaderIM::is_refactord_fw() ){
    for (int ichannel=0; ichannel<16; ichannel++){
      srv.push_back( float(inputFunction(ichannel, 36, false, 0))/float( 10 * 80*1000000) );  // 10sec * 80MHz
    }
  }else{
  //  uint32_t b = (option==0) ? 40 : 2400;
  for (int ichannel=0; ichannel<16; ichannel++){
    srv.push_back(inputFunction(ichannel,0x1B,true,4000));
  }
  } // is refactored 
}


/************************************************************/
void DataFlowReaderIM::getFifoInEmptyFraction(vector<float>& srv, uint32_t)
/************************************************************/
{
  if( DataFlowReaderIM::is_refactord_fw() ){
    srv.clear();
    srv.push_back(-1);
  }else{
  //  uint32_t b = (option==0) ? 40 : 2400;
  
  for (int ichannel=0; ichannel<16; ichannel++){
      srv.push_back(inputFunction(ichannel,0x18,true,4000)); 
  }
  } // is refactored 
}

/************************************************************/
void DataFlowReaderIM::getFifoOutEmptyFraction(vector<float>& srv, uint32_t)
/************************************************************/
{
  if( DataFlowReaderIM::is_refactord_fw() ){
    srv.clear();
    srv.push_back(-1);
  }else{
  //  uint32_t b = (option==0) ? 40 : 2400;

  for (int ichannel=0; ichannel<16; ichannel++){
    srv.push_back(inputFunction(ichannel,0x1A,true,4000));
  }
  } // is refactored 
}

/************************************************************/
void DataFlowReaderIM::getBusyFraction(vector<float>& srv, uint32_t)
/************************************************************/
{ // XOFF fraction 
  if( DataFlowReaderIM::is_refactord_fw() ){
    for (int ichannel=0; ichannel<16; ichannel++){
      srv.push_back( float(inputFunction(ichannel, 38, false,0))/float( 10 * 40*1000000) );  // 10sec * 40MHz
    }
  }else{
  srv.clear();
  srv.push_back(-1);
  }
}


/************************************************************/
void DataFlowReaderIM::getL1id(vector<int64_t>& srv)
/************************************************************/
{
  if( DataFlowReaderIM::is_refactord_fw() ){
    for (int ichannel=0; ichannel<16; ichannel++){
      srv.push_back( inputFunctionInt(ichannel, 145 ));  // L1ID at output of OUTFIFO 
    }
  }else{
  for (int ichannel=0; ichannel<16; ichannel++){
    srv.push_back(inputFunctionInt(ichannel,0x11)); 
  }
  } // is refactored 
}

/************************************************************/
void DataFlowReaderIM::getEventRate(vector<int64_t>& srv)
/************************************************************/
{
  if( DataFlowReaderIM::is_refactord_fw() ){
    for (int ichannel=0; ichannel<16; ichannel++){
      uint32_t tmp_rate = (inputFunctionInt(ichannel, 45 ) / 10 )/1000; 
      srv.push_back( tmp_rate );  // will implemet at 0x0197a, 10sec average (10 sec * 80 MHz)
    }
  }else{
  for (int ichannel=0; ichannel<16; ichannel++){
    srv.push_back(uint32_t(1e-3/((inputFunctionInt(ichannel,0x21))*25e-9)));
  }
  } // 
}

/************************************************************/
void DataFlowReaderIM::getLinkInStatus(vector<int64_t>& srv)
/************************************************************/
{
  if( DataFlowReaderIM::is_refactord_fw() ){
    for (int ichannel=0; ichannel<16; ichannel++){
      srv.push_back( daq::ftk::getBit(inputFunctionInt(ichannel, 3), 18) ); // 18th bit of status_bit 
    }
  }else{
  srv.clear();
  for (int ichannel=0; ichannel<16; ichannel++){
    srv.push_back( daq::ftk::getBit(inputFunctionInt(ichannel,0x17), 30) );
  }
  } // is refactored 
}

/************************************************************/
void DataFlowReaderIM::getNEventsProcessed(vector<int64_t>& srv)
/************************************************************/
{
  if( DataFlowReaderIM::is_refactord_fw() ){
    for (int ichannel=0; ichannel<16; ichannel++){
      // return total number of event at output of OUTFIFO
      uint64_t tmp_n_total_event = 0x0;
      
      tmp_n_total_event += ( inputFunctionInt(ichannel, 33 +  6) & 0x0000FFFF) << 48 ;
      tmp_n_total_event += ( inputFunctionInt(ichannel, 33 + 13) & 0x0000FFFF) << 32 ;
      tmp_n_total_event += ( inputFunctionInt(ichannel, 33 + 20) & 0x0000FFFF) << 16 ;
      tmp_n_total_event += ( inputFunctionInt(ichannel, 33 + 27) & 0x0000FFFF) <<  0 ;
      srv.push_back( tmp_n_total_event );
    }
  }else{
  for (int ichannel=0; ichannel<16; ichannel++){
    srv.push_back(inputFunctionInt(ichannel,0x98) );
  }
  } // is refactored 
}







/************************************************************/
void DataFlowReaderIM::getFifoInOverflowFlag(vector<int64_t>& srv)
/************************************************************/
{

  if( DataFlowReaderIM::is_refactord_fw() ){
    for (int ichannel=0; ichannel<16; ichannel++){
      srv.push_back(daq::ftk::getBit(inputFunctionInt(ichannel, 21), 27)); // in fifo lost word flag
    }
  }else{
    
    for (int ichannel=0; ichannel<16; ichannel++){
      srv.push_back(daq::ftk::getBit(inputFunctionInt(ichannel, 30), 30)); // in fifo full flag
    }
  } // is refactored 
}

/************************************************************/
void DataFlowReaderIM::getFifoOutOverflowFlag(vector<int64_t>& srv)
/************************************************************/
{

  if( DataFlowReaderIM::is_refactord_fw() ){
    for (int ichannel=0; ichannel<16; ichannel++){
      srv.push_back(daq::ftk::getBit(inputFunctionInt(ichannel, 33), 27)); // out fifo lost word flag
    }
  }else{
    
    for (int ichannel=0; ichannel<16; ichannel++){
      srv.push_back(daq::ftk::getBit(inputFunctionInt(ichannel, 35), 30)); // out fifo full flag
    }
  } // is refactored 
}



////////////////// for IM specific functions ///////////////////////// 
/************************************************************/
void DataFlowReaderIM::getNInputTruncation(vector<int64_t>& srv)
/************************************************************/
{
  if( DataFlowReaderIM::is_refactord_fw() ){
    for (int ichannel=0; ichannel<16; ichannel++){
      srv.push_back( (( inputFunctionInt(ichannel, 45 ) & 0xFFFF0000) >> 16 ) );
    }
  }else{
  for (int ichannel=0; ichannel<16; ichannel++){
  srv.clear();
  srv.push_back(-1);
  }
  } // is refactored 
}

/************************************************************/
void DataFlowReaderIM::getInputTruncationFraction(vector<float>& srv)
/************************************************************/
{
  if( DataFlowReaderIM::is_refactord_fw() ){
    for (int ichannel=0; ichannel<16; ichannel++){
      srv.push_back( inputFunctionInt(ichannel, 46 )/(10 * 80 * 1000000) ); // 10 sec in 80MHz
    }
  }else{
  for (int ichannel=0; ichannel<16; ichannel++){
  srv.clear();
  srv.push_back(-1);
  }
  } // is refactored 
}

/************************************************************/
void DataFlowReaderIM::getNRecover(vector<int64_t>& srv)
/************************************************************/
{
  if( DataFlowReaderIM::is_refactord_fw() ){
    for (int ichannel=0; ichannel<16; ichannel++){
      srv.push_back( (( inputFunctionInt(ichannel, 43 ) & 0xFFFF0000) >> 16 ) );
    }
  }else{
  for (int ichannel=0; ichannel<16; ichannel++){
  srv.clear();
  srv.push_back(-1);
  }
  } // is refactored 
}

/************************************************************/
void DataFlowReaderIM::getRecoverFraction(vector<float>& srv)
/************************************************************/
{
  if( DataFlowReaderIM::is_refactord_fw() ){
    for (int ichannel=0; ichannel<16; ichannel++){
      srv.push_back( inputFunctionInt(ichannel, 44 )/(10 * 80 * 1000000) ); // 10 sec in 80MHz
    }
  }else{
  for (int ichannel=0; ichannel<16; ichannel++){
  srv.clear();
  srv.push_back(-1);
  }
  } // is refactored 
}


/************************************************************/
void DataFlowReaderIM::getBPFractionFromDF(vector<float>& srv)
/************************************************************/
{
  if( DataFlowReaderIM::is_refactord_fw() ){
    for (int ichannel=0; ichannel<16; ichannel++){
      srv.push_back( inputFunctionInt(ichannel, 39 )/(10 * 40 * 1000000) ); // 10 sec in 40MHz
    }
  }else{
    srv.clear();
    srv.push_back(-1);
  } // is refactored 
}
/************************************************************/
void DataFlowReaderIM::getNFifoOverflow(vector<float>& srv)
/************************************************************/
{
  if( DataFlowReaderIM::is_refactord_fw() ){
    for (int ichannel=0; ichannel<16; ichannel++){
      srv.push_back( inputFunctionInt(ichannel, 21) & 0xFFFF ); // in fifo #of lost word
    }
  }else{
  for (int ichannel=0; ichannel<16; ichannel++){
    srv.push_back(inputFunctionInt(ichannel,0x30) & 0x7ffffff );
  }
  } // is refactored 
}
/************************************************************/
void DataFlowReaderIM::getOverflowFraction(vector<float>& srv)
/************************************************************/
{
  if( DataFlowReaderIM::is_refactord_fw() ){
    srv.clear();
    srv.push_back(-1);
  }else{
  for (int ichannel=0; ichannel<16; ichannel++){
    srv.push_back(inputFunction(ichannel,0x19,true,4000));
  }
  } // is refactored 
}







