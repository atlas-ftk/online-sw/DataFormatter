#include <DataFormatter/StatusRegisterDFSwitchFifo.h>
#include "ftkcommon/exceptions.h"
#include "ftkcommon/Utils.h"

using namespace daq::ftk;

StatusRegisterDFSwitchFifo::StatusRegisterDFSwitchFifo(std::shared_ptr<FtkDataFormatterApi> dfapi, 
			const srType& type, 
			const uint32_t& valueSelector)
: StatusRegisterDFSwitchFifo::StatusRegisterDFSelector(dfapi, type)
{
  initialize_selector(getBitRange(valueSelector, 8, 11), getBitRange(valueSelector, 4,  7), getBitRange(valueSelector, 0,  3));
}

// ====================================================
StatusRegisterDFSwitchFifo::StatusRegisterDFSwitchFifo(std::shared_ptr<FtkDataFormatterApi> dfapi, 
			const srType& type, 
			const uint32_t& typeSelector, const uint32_t& columnSelector, const uint32_t& rowSelector)
: StatusRegisterDFSwitchFifo::StatusRegisterDFSelector(dfapi, type)
{
  initialize_selector(typeSelector, columnSelector, rowSelector);
}

// ====================================================
void
StatusRegisterDFSwitchFifo::initialize_selector(const uint32_t& typeSelector, const uint32_t& columnSelector, const uint32_t& rowSelector)
{
  m_write_nodes.push_back("reg.switch_selector_for_fifo_monitoring.type");
  m_write_nodes.push_back("reg.switch_selector_for_fifo_monitoring.column");
  m_write_nodes.push_back("reg.switch_selector_for_fifo_monitoring.row");

  m_write_values.push_back(typeSelector);
  m_write_values.push_back(columnSelector);
  m_write_values.push_back(rowSelector);

  m_node	  = "reg.switch_fifo_monitoring";
}


