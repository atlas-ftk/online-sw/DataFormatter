/*****************************************************************/
/*                                                               */
/*                                                               */
/*****************************************************************/

#include <utility> //pair
#include <bitset>
// OKS and IS interfaces
#include "config/Configuration.h"
#include "DataFormatter/dal/ReadoutModule_DataFormatter.h"
#include "RunControl/Common/OnlineServices.h"
#include "RunControl/Common/CommandSender.h"
#include "DFdal/RCD.h"
#include "DFdal/ReadoutConfiguration.h"

#include "DataFormatter/ReadoutModuleDataFormatter.h"
#include "DataFormatter/DataFormatterConstants.h"

// DataFormatter status registers
#include "DataFormatter/StatusRegisterIMDirect.h"
#include "DataFormatter/StatusRegisterDFDirect.h"
#include "DataFormatter/StatusRegisterDFCounter.h"
#include "DataFormatter/StatusRegisterDFSwitchFifo.h"
#include "DataFormatter/StatusRegisterDFGT.h"

// common FTK tools
#include "ftkcommon/exceptions.h"
#include "ftkcommon/core.h"
#include "ftkcommon/Utils.h"

// ftkcommon SpyBuffer
#include "ftkcommon/EventFragmentCollection.h"
#include "ftkcommon/EventFragment.h"
#include "ftkcommon/EventFragmentFTKPacket.h"
#include "ftkcommon/EventFragmentHitClusters.h"
#include "ftkcommon/ModuleHitClusters.h"
#include "ftkcommon/SourceIDSpyBuffer.h"

// IPBUS
#include "uhal/uhal.hpp"
#include "uhal/Utilities.hpp"
#include "uhal/log/exception.hpp"

// speedtest
//#include "DataFormatter/Timer.h"

//protection for stopHLT crash
#include<chrono>

//RunControl exeptions
#include <RunControl/Common/UserExceptions.h>

using namespace daq::ftk;
using namespace std;


/**********************/
ReadoutModule_DataFormatter::ReadoutModule_DataFormatter()
  /**********************/
  : m_ohProvider(NULL),
    m_configuration_done ATOMIC_VAR_INIT(false)
{ 
  #include "cmt/version.ixx"
  ERS_LOG("ReadoutModule_DataFormatter::constructor: Entering"   << std::endl
          << "Build timestamp: " << __DATE__ << " " << __TIME__ << std::endl
          << cmtversion );

  m_name                  = "NAME_UNDEFINED";
  m_configuration         = 0;
  m_dryRun                = false;
  m_configPath            = "";
  m_dataPath              = "";
  m_IPBus_connection_file             = "";
  m_IPBus_transceiver_connection_file = "";
  m_IPBus_device_id                   = "";
  m_DF_configuration_file             = "";
  m_IM_LUTs_file                      = "";
  m_IM_PseudoData_file                = "";

  m_useBlockTransfer             = 0x1;

  m_doDFblockTransfer = true;
  
  m_doFWCheck = true;

  m_df_fwVersion = 0x18007;
  m_im_fwVersion = 0x0190;

  m_b0fTimeoutThreshold = 0;

  m_usePseudoData = false;

  m_IPBusEmulatorMode = 0;

  m_failOnBadLink = 0x0;

  m_DF_unknownModuleHeaderWord = 0x0;
  m_DF_moduleWordLimit = 0;
  m_DF_enableInputResync = 0;
  m_DF_IM_freezeMode = 0;

}

/***********************/
ReadoutModule_DataFormatter::~ReadoutModule_DataFormatter() noexcept
/***********************/
{
  ftkTimeLog tLog(ERS_HERE, name_ftk(),"Destructor");

  df_rm_DF_enabled_RC = 0x0;
  df_rm_DF_enabled_checked = 0x0;

  tLog.end(ERS_HERE);  // End log counter
}

void ReadoutModule_DataFormatter::setup(DFCountedPointer<ROS::Config> configuration)
{
  doSetup( configuration );
}

/************************************************************/
void ReadoutModule_DataFormatter::doSetup(DFCountedPointer<ROS::Config> configuration)
/************************************************************/
{
  ftkTimeLog tLog(ERS_HERE, name_ftk(),"doSetup");
 
  // Get online objects from daq::rc::OnlineServices
  m_ipcpartition      = daq::rc::OnlineServices::instance().getIPCPartition();
  m_appName = daq::rc::OnlineServices::instance().applicationName();
  auto dal_rcdAppl    = daq::rc::OnlineServices::instance().getApplication().cast<daq::df::RCD>();
  Configuration *conf = configuration->getPointer<Configuration>("configurationDB");
  // OR: Configuration &conf   = daq::rc::OnlineServices::instance().getConfiguration();
  auto readOutConfig  = conf->cast<daq::df::ReadoutConfiguration>( dal_rcdAppl->get_Configuration() );
  

  // Read some info from the DB
  ERS_LOG("Start reading info from the DB");
  const ftk::dal::ReadoutModule_DataFormatter * module_dal = conf->get<ftk::dal::ReadoutModule_DataFormatter>(configuration->getString("UID"));
  module_dal->print(3, true, std::cout);
  m_name                              = module_dal->UID();
  cout << "m_name: " << m_name << endl;
  m_shelf                             = module_dal->get_DF_Shelf();
  m_slot                              = module_dal->get_DF_Slot();
  m_isServerName                      = readOutConfig->get_ISServerName(); 
  m_dryRun                            = module_dal->get_DryRun();
  m_configPath                        = module_dal->get_ConfigPath();
  // rmina 2 March 18 : deprecating the IPBus_DID option. It should always be identical to m_name
  m_IPBus_device_id = m_name;
  //m_IPBus_device_id                   = module_dal->get_IPBus_DID();
  m_IPBus_connection_file             = "file://" + m_configPath + module_dal->get_IPBus_CF();
  m_IPBus_transceiver_connection_file = m_configPath + module_dal->get_IPBus_TCF();
  m_DatabaseFile                      = module_dal->get_DatabaseFile();
  m_DF_configuration_file             = m_configPath + module_dal->get_DF_CF();
  m_DF_global_configuration_file      = m_configPath + module_dal->get_DF_Global_CF();
  m_DF_system_configuration_file      = m_configPath + module_dal->get_DF_System_CF();
  m_DF_board_delay_file               = m_configPath + module_dal->get_DF_DelayFile();
  m_DF_multishelf_file                = m_configPath + module_dal->get_DF_MultiShelf_CF();
  m_DF_build_configuration_OTF        = module_dal->get_DF_OTF_Configuration();
  m_DF_validate_OTF                   = module_dal->get_DF_validate_OTF();
  m_DF_Write_OTF_Configuration        = module_dal->get_DF_Write_OTF_Configuration();
  
  m_IM_modulelist_file                = m_configPath + module_dal->get_IM_ModuleListFile();
  m_IM_LUTs_file                      = m_configPath + module_dal->get_IM_LUTs();
  m_IM_LUT_prefix                     = m_configPath + module_dal->get_IM_LUT_prefix();
  m_IM_PseudoData_prefix              = m_configPath + module_dal->get_IM_PseudoData_prefix();
  m_IM_PseudoData_file                = m_configPath + module_dal->get_IM_PseudoData();
  m_IM_build_configuration_OTF        = module_dal->get_IM_OTF_Configuration();
  m_IM_OKS_parameters["IA_OKS_L1TT_PS_MASK"]                 = module_dal->get_IM_L1TT_PS_MASK();
  m_IM_OKS_parameters["IA_OKS_L1ID_PS_VALUE"]                = module_dal->get_IM_L1ID_PS_VALUE();
  m_IM_OKS_parameters["IA_OKS_L1ID_PS_MASK"]                 = module_dal->get_IM_L1ID_PS_MASK();
  m_IM_OKS_parameters["IA_OKS_L1ID_FREEZE_VALUE"]            = module_dal->get_IM_L1ID_FREEZE_VALUE();
  m_IM_OKS_parameters["IA_OKS_L1ID_FREEZE_MASK"]             = module_dal->get_IM_L1ID_FREEZE_MASK();
  m_IM_OKS_parameters["IA_OKS_ERR_FREEZE_MASK1"]             = module_dal->get_IM_ERR_FREEZE_MASK1();
  m_IM_OKS_parameters["IA_OKS_ERR_FREEZE_MASK2"]             = module_dal->get_IM_ERR_FREEZE_MASK2();
  m_IM_OKS_parameters["IA_OKS_START_L1ID_MASK"]              = module_dal->get_IM_START_L1ID_MASK();
  m_IM_OKS_parameters["IA_OKS_START_L1ID_VALUE"]             = module_dal->get_IM_START_L1ID_VALUE();
  m_IM_OKS_parameters["IA_OKS_RECOVER_CONFIG"]               = module_dal->get_IM_RECOVER_CONFIG();
  m_IM_OKS_parameters["IA_OKS_FILLGAP_CONFIG"]               = module_dal->get_IM_FILLGAP_CONFIG();
  m_IM_OKS_parameters["IA_OKS_HIT_PER_MODULE_IBL"]           = module_dal->get_IM_HIT_PER_MODULE_IBL();
  m_IM_OKS_parameters["IA_OKS_HIT_PER_MODULE_PIX"]           = module_dal->get_IM_HIT_PER_MODULE_PIX();
  m_IM_OKS_parameters["IA_OKS_HIT_PER_MODULE_SCT"]           = module_dal->get_IM_HIT_PER_MODULE_SCT();
  m_IM_OKS_parameters["IA_OKS_IBL_KENTO_REMOVAL"]            = module_dal->get_IM_IBL_KENTO_REMOVAL();
  m_IM_OKS_parameters["IA_OKS_SPECIAL5"]                     = module_dal->get_IM_SPECIAL5();
  
  m_IM_OKS_parameters["IA_OKS_FREEZE_WAIT_CLK"]              = module_dal->get_IM_FREEZE_WAIT_CLK();
  m_IM_OKS_parameters["IA_OKS_GTP_RESET_WAIT_CLK"]           = module_dal->get_IM_GTP_RESET_WAIT_CLK();
  m_IM_OKS_parameters["IA_OKS_XOFF_KEYWORD"]                 = module_dal->get_IM_XOFF_KEYWORD();
  m_IM_OKS_parameters["IA_OKS_XOFF_CONFIG"]                  = module_dal->get_IM_XOFF_CONFIG();
  
  m_IM_OKS_parameters["GA_CONTROL_REG:IGNORE_HOLD"]          = module_dal->get_IM_IGNORE_HOLD();
  m_IM_OKS_parameters["GA_CONTROL_REG:XOFF_MODE"]            = module_dal->get_IM_XOFF_MODE();
  m_IM_OKS_parameters["GA_CONTROL_REG:GANGED_PIXEL"]         = module_dal->get_IM_GANGED_PIXEL();
  m_IM_OKS_parameters["GA_CONTROL_REG:REDUNDANCY"]           = module_dal->get_IM_REDUNDANCY();
  m_IM_OKS_parameters["GA_CONTROL_REG:DUPLICATED_MODULE"]    = module_dal->get_IM_DUPLICATED_MODULE();
  m_IM_OKS_parameters["GA_CONTROL_REG:IS_APPLY_SELECTPS"]    = module_dal->get_IM_IS_APPLY_SELECTPS();
  m_IM_OKS_parameters["GA_CONTROL_REG:IS_APPLY_REJECTPS"]    = module_dal->get_IM_IS_APPLY_REJECTPS();
  m_IM_OKS_parameters["GA_CONTROL_REG:IS_APPLY_L1TTPS"]      = module_dal->get_IM_IS_APPLY_L1TTPS();
  m_IM_OKS_parameters["GA_CONTROL_REG:IS_APPLY_TRUNCATION"]  = module_dal->get_IM_IS_APPLY_TRUNCATION();
  m_IM_OKS_parameters["GA_CONTROL_REG:IS_APPLY_FREEZE"]      = module_dal->get_IM_IS_APPLY_FREEZE();
  m_IM_OKS_parameters["GA_CONTROL_REG:IS_APPLY_L1ID_FREEZE"] = module_dal->get_IM_IS_APPLY_L1ID_FREEZE();
  m_IM_OKS_parameters["GA_CONTROL_REG:IS_APPLY_DF_FREEZE"]   = module_dal->get_IM_IS_APPLY_DF_FREEZE(); 
  m_IM_OKS_parameters["GA_CONTROL_REG:IS_APPLY_ERR_FREEZE"]  = module_dal->get_IM_IS_APPLY_ERR_FREEZE();
  
  m_IM_pseudo_input_rate              = module_dal->get_IM_PSEUDO_INPUT_RATE();
  m_IM_pseudo_data_xoff               = ( module_dal->get_IM_PSEUDO_DATA_XOFF() == 0x1 );
  m_IM_pseudo_initial_delay           = ( module_dal->get_IM_PSEUDO_DATA_INITIAL_DELAY() == 0x1 );
  m_slink_mode_mask                   = module_dal->get_IM_SLINK_MODE_MASK();
  m_num_good_delay_values             = module_dal->get_numGoodDelayValues();


  m_useAutoDelaySetting               = module_dal->get_DF_use_auto_delay_setting();
  m_maxCyclesWaitedForData            = module_dal->get_DF_MaxCyclesWaitedForData();
  m_b0fTimeoutThreshold               = module_dal->get_DF_b0fTimeoutThreshold(); 
  m_skewTimeoutThresholdMax           = module_dal->get_DF_skewTimeoutThresholdMax(); 
  m_skewTimeoutThresholdMin           = module_dal->get_DF_skewTimeoutThresholdMin(); 
  m_packetTimeoutThreshold            = module_dal->get_DF_packetHandlerTimeoutThreshold(); 
  m_usePseudoData                     = module_dal->get_usePseudoData();
  m_IPBusEmulatorMode                 = module_dal->get_IPBusEmulatorMode();
  m_failOnBadLink                     = module_dal->get_failOnBadLink();
  m_DF_unknownModuleHeaderWord        = module_dal->get_DF_unknownModuleHeaderWord();
  m_DF_moduleWordLimit                = module_dal->get_DF_moduleWordLimit();
  m_DF_boardId                        = module_dal->get_DF_boardId();
  
  m_DF_enableInputResync = module_dal->get_DF_enableInputResync();
  m_DF_IM_freezeMode = module_dal->get_DF_IM_freezeMode();

  m_doFWCheck = module_dal->get_doFWCheck();
  m_df_fwVersion = module_dal->get_DF_FwVersion();
  m_im_fwVersion = module_dal->get_IM_FwVersion();
  ERS_LOG("Finish reading info from the DB");


  m_IPBusRegisterFile = m_configPath + "address_df_" + m_IPBus_device_id + ".xml";
  // cout << m_IPBusRegisterFile << endl;

  m_IPBusRegisterMap = RegisterNodeMap();
  makeRegisterNodeMap( m_IPBusRegisterMap, m_df_fwVersion );
  writeRegisterNodeMap( m_IPBusRegisterMap, m_IPBusRegisterFile, m_name ); //ssevova 27.03.19 add m_name to void writeRegisterNodeMap for IOError exception 

  m_dfApi = std::shared_ptr<FtkDataFormatterApi>(new FtkDataFormatterApi(m_IPBus_connection_file, m_IPBus_device_id, m_name, m_IPBusRegisterMap));
  m_dfApi->setShelfAndSlot( m_shelf, m_slot );
  m_dfApi->setFwVersions( m_doFWCheck, m_df_fwVersion, m_im_fwVersion);
  m_dfApi->printFwVersion();

  // initialize the superclass for MT transitions
  initialize( m_name, module_dal, BOARD_IMDF );

  ERS_LOG("IPBus_connection : "                      << m_IPBus_connection_file.c_str());
  ERS_LOG("IPBus_transceiver_connection_file : "     << m_IPBus_transceiver_connection_file.c_str());
  ERS_LOG("IPBus_device_id :"                        << m_IPBus_device_id.c_str());
  ERS_LOG("IPBus_register_file :"                    << m_IPBusRegisterFile.c_str());
  ERS_LOG("DatabaseFile : "                          << m_DatabaseFile.c_str());
  ERS_LOG("DF_configuration_file : "                 << m_DF_configuration_file.c_str());
  ERS_LOG("DF_global_configuration_file : "          << m_DF_global_configuration_file.c_str());
  ERS_LOG("DF_system_configuration_file : "          << m_DF_system_configuration_file.c_str());
  ERS_LOG("DF_board_delay_file : "                   << m_DF_board_delay_file.c_str());
  ERS_LOG("IM_modulelist_file : "                    << m_IM_modulelist_file.c_str());
  ERS_LOG("DF_multishelf_file : "                    << m_DF_multishelf_file.c_str());
  ERS_LOG("IM_build_configuration_OTF : "            << m_IM_build_configuration_OTF);
  ERS_LOG("IM_LUT_prefix : "                         << m_IM_LUT_prefix);
  ERS_LOG("IM_PseudoData_prefix : "                  << m_IM_PseudoData_prefix);
  ERS_LOG("DF_build_configuration_OTF : "            << m_DF_build_configuration_OTF);
  ERS_LOG("DF_validate_OTF : "                       << m_DF_validate_OTF);
  ERS_LOG("DF_Write_OTF_Configuration : "            << m_DF_Write_OTF_Configuration);
  ERS_LOG("IM_LUTs_file : "                          << m_IM_LUTs_file.c_str());
  ERS_LOG("IM_PseudoData_file : "                    << m_IM_PseudoData_file.c_str());
  for( auto pair : m_IM_OKS_parameters ){
    ERS_LOG("IM OKS : " << pair.first.c_str() << " -> 0x" << std::hex << pair.second );
  }

  // Prescale log 
  if ( m_IM_OKS_parameters["GA_CONTROL_REG:IS_APPLY_SELECTPS"] == 0x1 ) {
    ERS_LOG("IM Prescale(L1ID) is enabled(selectPS) with 0x" << hex << m_IM_OKS_parameters["IA_OKS_L1ID_PS_MASK"]  << dec << "(0x" << hex << m_IM_OKS_parameters["IA_OKS_L1ID_PS_VALUE"] << dec << ")" );
  }else if( m_IM_OKS_parameters["GA_CONTROL_REG:IS_APPLY_SELECTPS"] == 0x1 ) {
    ERS_LOG("IM Prescale(L1ID) is enabled(rejectPS) with 0x" << hex << m_IM_OKS_parameters["IA_OKS_L1ID_PS_MASK"]  << dec << "(0x" << hex << m_IM_OKS_parameters["IA_OKS_L1ID_PS_VALUE"] << dec << ")" );
  }else if( m_IM_OKS_parameters["GA_CONTROL_REG:IS_APPLY_L1TTPS"] == 0x1 ) {
    ERS_LOG("IM Prescale(L1TT) is enabled with 0x" << hex << m_IM_OKS_parameters["IA_OKS_L1TT_PS_MASK"] );
  }else{
    ERS_LOG("IM Prescale(L1ID) is disabled.");
  }
  
  // hit per module 
  if( ( ( m_IM_OKS_parameters["IA_OKS_HIT_PER_MODULE_IBL"] & 0x8000000 ) >> 31 ) == 0x1  ){
    ERS_LOG("IM Hit/Module is enabled(IBL) wtih 0x" << hex << (m_IM_OKS_parameters["IA_OKS_HIT_PER_MODULE_IBL"] & 0x0000FFFF) );
  }else{
    ERS_LOG("IM Hit/Module is disabled(IBL) " );
  }
  if( ( ( m_IM_OKS_parameters["IA_OKS_HIT_PER_MODULE_PIX"] & 0x8000000 ) >> 31 ) == 0x1  ){
    ERS_LOG("IM Hit/Module is enabled(Pix) wtih 0x" << hex << (m_IM_OKS_parameters["IA_OKS_HIT_PER_MODULE_PIX"] & 0x0000FFFF) );
  }else{
    ERS_LOG("IM Hit/Module is disabled(Pix) " );
  }
  if( ( ( m_IM_OKS_parameters["IA_OKS_HIT_PER_MODULE_SCT"] & 0x8000000 ) >> 31 ) == 0x1  ){
    ERS_LOG("IM Hit/Module is enabled(SCT) wtih 0x" << hex << (m_IM_OKS_parameters["IA_OKS_HIT_PER_MODULE_SCT"] & 0x0000FFFF) );
  }else{
    ERS_LOG("IM Hit/Module is disabled(SCT) " );
  }
  
  
  
  ERS_LOG("DF Max cycles waited for data : 0x"            << hex<<m_maxCyclesWaitedForData);
  ERS_LOG("DF use auto delay setting : "                << m_useAutoDelaySetting);
  ERS_LOG("DF use firmware check : "                    << m_doFWCheck);
  ERS_LOG("DF expected FW version: 0x"                    << hex<<m_df_fwVersion);
  ERS_LOG("IM expected FW version: 0x"                 << hex<<m_im_fwVersion);
  ERS_LOG("b0f timeout threshold : 0x"                    << hex<<m_b0fTimeoutThreshold);
  ERS_LOG("skew timeout threshold max : 0x"                    << hex<<m_skewTimeoutThresholdMax);
  ERS_LOG("skew timeout threshold min : 0x"                    << hex<<m_skewTimeoutThresholdMin);
  ERS_LOG("packet timeout threshold: 0x"                       << hex<<m_packetTimeoutThreshold);

  if ( m_skewTimeoutThresholdMax > 0xfffff ) {
    ers::warning( daq::ftk::ftkException( ERS_HERE, m_name, "Max skew timeout threshold was set too large! Resetting it to 0xfffff." ) );
    m_skewTimeoutThresholdMax = 0xfffff;
  }
  if ( m_skewTimeoutThresholdMin > 0xfffff ) {
    ers::warning( daq::ftk::ftkException( ERS_HERE, m_name, "Min skew timeout threshold was set too large! Resetting it to 0xfffff." ) );
    m_skewTimeoutThresholdMin = 0xfffff;
  }
  if ( m_packetTimeoutThreshold > 0xfffff ) {
    ers::warning( daq::ftk::ftkException( ERS_HERE, m_name, "PacketHandler timeout threshold was set too large! Resetting it to 0xfffff." ) );
    m_packetTimeoutThreshold = 0xfffff;
  }

  ERS_LOG("uhal IPBus emulator mode : "                 << m_IPBusEmulatorMode);
  ERS_LOG("fail on bad link status mask: 0x"              << hex<<m_failOnBadLink);
  ERS_LOG("'Unknown module header' word: 0x"              << hex<<m_DF_unknownModuleHeaderWord);
  ERS_LOG("DF word limit per module: "                    << dec<<m_DF_moduleWordLimit);
  ERS_LOG("DF IM freeze mode : 0x" <<hex<<m_DF_IM_freezeMode);
  if ( m_DF_enableInputResync ) {
    ERS_LOG("Enabling input lane resync.");
  }

  ERS_LOG("DF board ID: 0x"                               << hex<<m_DF_boardId);

//  if (m_channelMaskSLINK == 0X0 ){
  if ( m_usePseudoData ) {
    
    daq::ftk::ftkException e(ERS_HERE, m_name, " Sending Pseudodata from IM!");  // NB: append the original exception (ex) to the new one 
    ers::info(e);  //downgrading from WARNING to INFO on a request from TDAQ RC after a test run on 23.03.2017;  
  }
  if(m_dryRun){
    daq::ftk::ftkException ex(ERS_HERE, m_name, " DryRun option is set in DB, IPBus calls will be skipped!" ); 
    ers::warning(ex);
  }

  // Creating the configuration handler object
  m_configHandler = std::make_unique<DataFormatterConfigHandler>(
                                                              m_name,
                                                              m_shelf,
                                                              m_slot,
                                                              m_useBlockTransfer,
                                                              m_useAutoDelaySetting,
                                                              m_DF_build_configuration_OTF,
                                                              m_IM_build_configuration_OTF,
                                                              m_DF_Write_OTF_Configuration,
                                                              m_usePseudoData,
                                                              m_DF_configuration_file,
                                                              m_DF_global_configuration_file,
                                                              m_DF_system_configuration_file,
                                                              m_DF_board_delay_file,
                                                              m_DatabaseFile,
                                                              m_IM_modulelist_file,
                                                              m_DF_multishelf_file,
                                                              m_IPBus_transceiver_connection_file,
                                                              m_DF_configuration_file+".dump",
                                                              m_IM_LUT_prefix,
                                                              m_IM_LUTs_file,
                                                              m_IM_PseudoData_prefix,
                                                              m_IM_PseudoData_file,
                                                              m_IM_pseudo_input_rate, 
                                                              m_maxCyclesWaitedForData,
                                                              m_b0fTimeoutThreshold,
                                                              m_skewTimeoutThresholdMax,
                                                              m_skewTimeoutThresholdMin,
                                                              m_packetTimeoutThreshold,
                                                              m_DF_IM_freezeMode,
                                                              m_DF_enableInputResync,
                                                              m_DF_unknownModuleHeaderWord,
                                                              m_DF_moduleWordLimit,
                                                              m_DF_boardId,
                                                              m_slink_mode_mask,
                                                              m_num_good_delay_values,
                                                              m_dfApi );


  //---------------------------------------
  // Creating the ISInfo object
  std::string isProvider = m_isServerName + "." + m_name;
  ERS_LOG("IS: publishing in " << isProvider);
  try { 
    m_dfNamed = std::shared_ptr<DataFormatterNamed>(new DataFormatterNamed( m_ipcpartition, isProvider.c_str() )); 
  } catch( ers::Issue &ex )  {
    daq::ftk::ISException e(ERS_HERE, m_name + " Error while creating DataFormatterNamed object", ex);  // NB: append the original exception (ex) to the new one 
    ers::warning(e);  //or throw e;  
  }
 
  //---------------------------------------
  // Creating the Histogramming provider
  std::string OHServer("Histogramming"); // TODO: make it configurable
  std::string OHName("FTK_" + m_name);   // Name of the publication directory
  try { 
    m_ohProvider      = std::shared_ptr<OHRootProvider>(new OHRootProvider ( m_ipcpartition , OHServer, OHName )); 

    m_histHandler     = std::make_unique<DataFormatterHistHandler>( m_name, m_ohProvider, m_dfApi );

    m_ohRawProviderIM = std::shared_ptr<OHRawProvider<> >(new OHRawProvider<>( m_ipcpartition , OHServer, getDFOHNameString(m_name,"DF",m_shelf,m_slot,"IM") ));
    m_ohRawProviderDF = std::shared_ptr<OHRawProvider<> >(new OHRawProvider<>( m_ipcpartition , OHServer, getDFOHNameString(m_name,"DF",m_shelf,m_slot,"DF") ));
    ERS_LOG("OH: publishing in " << OHServer << "." << m_name << ", from application " << m_appName);
  } catch ( daq::oh::Exception & ex)  {  
    daq::ftk::OHException e(ERS_HERE, "DF-" + to_string(m_shelf) + "-" + to_string(m_slot), ex);  // NB: append the original exception (ex) to the new one 
    ers::warning(e);  //or throw e;  
  }
  m_dfReaderIM = std::unique_ptr<DataFlowReaderIM>(new DataFlowReaderIM(m_ohRawProviderIM, m_im_fwVersion, true));
  m_dfReaderIM->init(m_name);
  m_dfReaderDF = std::unique_ptr<DataFlowReaderDF>(new DataFlowReaderDF(m_ohRawProviderDF, m_df_fwVersion, true));
  m_dfReaderDF->init(m_name);
  // set up to check that configuration agrees between RC and text files
  m_board_number = (uint32_t) FtkDataFormatterApi::getBoardNumberFromShelfSlot(m_shelf, m_slot);
  ERS_LOG( "Start check of configuration agreement between RC & txt files" );
  ERS_LOG( " m_board_number is " << m_board_number );
  if ( m_board_number >= NUMBER_OF_DF_BOARDS ) {
    char buffer[300];
    snprintf( buffer, sizeof(buffer), "%s is configured with unknown shelf and slot combination: %u %u", m_name.c_str(), m_shelf, m_slot );
    ftkException theIssue( ERS_HERE, name_ftk(), buffer );
    throw( theIssue );
  }
  if ( df_rm_DF_enabled_RC & (0x1<<m_board_number) ) {
    std::stringstream message;
    message << "Multiple boards configured with the same board number: " << m_board_number;
    ers::warning( daq::ftk::ftkException( ERS_HERE, name_ftk(), message.str() ) );
  }
  df_rm_DF_enabled_RC |= (0x1<<m_board_number);
  df_rm_DF_enabled_checked = 0;
  ERS_LOG( "Finish check of configuration agreement between RC & txt files" );
  
  tLog.end(ERS_HERE);  // End log counter
}

/********************************/
void ReadoutModule_DataFormatter::doConfigure( const daq::rc::TransitionCmd& /*cmd*/ )
/********************************/
{
  ftkTimeLog tLog(ERS_HERE, name_ftk(),"doConfigure");

  if ( df_rm_DF_enabled_checked++ ) {
    try {
      m_configHandler->checkRCAgainstConfig( df_rm_DF_enabled_RC, m_DF_global_configuration_file );
    } catch ( daq::ftk::IOError ex) {
      IOError ex2(ERS_HERE, m_name, " could not open global board config file " + m_DF_global_configuration_file, ex);
      throw ex2;
    }
  }
  
  m_configHandler->configure();

  // Get active channels or reset channel mask
  // find active channels and look up the ROB ID's
  m_isactive.clear();
  m_isactive_mask = m_configHandler->getEnableFMCMask();
  m_istimedout = 0x0;
  m_IMChToRobId.clear();
  m_inputPacketErrorLanes = 0x0;
  m_inputPacket8thWordErrorLanes = 0x0;
  m_unknownModuleHeader = 0x0;
  for(uint32_t ichannel=0; ichannel < NUMBER_OF_DF_INPUT_LANES; ichannel++) {
    m_isactive.push_back( daq::ftk::getBit(m_isactive_mask, ichannel) );
    if(m_isactive.at(ichannel)) {
      uint32_t robId = 0;
      if ( m_configHandler->getRobIdForIMLane( ichannel, robId ) ) {
        ERS_INFO( m_name << " activated IM channel: " << ichannel << " with ROB ID 0x" <<hex<<robId  <<dec<< ".");
        m_IMChToRobId[ ichannel ] = robId;
      } else {
        ers::warning( daq::ftk::ftkException( ERS_HERE, m_name, " activated IM channel " + std::to_string(ichannel) + ", but no ROB ID is configured for this channel." ) );
      }
    }
  }

  if ( m_histHandler ) m_histHandler->configureHists();

  m_configuration_done = true;
  tLog.end(ERS_HERE);  // End log counter
}

/**************************/
void ReadoutModule_DataFormatter::doConnect( const daq::rc::TransitionCmd& cmd  )
/**************************/
{
  ftkTimeLog tLog(ERS_HERE, name_ftk(), cmd.toString());
  
  m_ftkemonDataOut = reinterpret_cast<daq::ftk::FtkEMonDataOut*>(ROS::DataOut::sampled());
  if(!m_ftkemonDataOut) {
    ers::warning( daq::ftk::ftkException( ERS_HERE, name_ftk(), "EMonDataOut plugin not found! Spybuffer data will not be published in emon" ) );
  } else {
    m_ftkemonDataOut->setScalingFactor(1);
    ERS_LOG("Found EMonDataOut plugin");
  }

  //open RX
  m_dfApi->setTxRxLine(0x00FFFFFF,m_configHandler->get_rxports());


  if(! m_dryRun) {
    ERS_LOG("Setting IM OKS parameters... ");
    bool im_configure_status = m_dfApi->getIMApi()->set_IM_OKS( m_isactive, m_IM_OKS_parameters );
    
    m_statusRegisterATCAFactory = std::unique_ptr<StatusRegisterATCAFactory>(new StatusRegisterATCAFactory(m_name, "PSZYUFG", m_isServerName, m_ipcpartition, m_dfApi, m_df_fwVersion, true ));// m_useBlockTransfer
  }// dryRun
  tLog.end(ERS_HERE);  // End log counter
}

void ReadoutModule_DataFormatter::checkConnect( const daq::rc::SubTransitionCmd& cmd )
{
  ERS_LOG( "checkConnect() -- Checking transceiver status" );
  ftkTimeLog tLog(ERS_HERE, name_ftk(), cmd.toString());

  if ( !m_dryRun ) {
    // robmina 28.Jun.17 -- see https://its.cern.ch/jira/browse/FTKHWD-294
    //    only checking the first 36 transceivers (connected to downstream boards)
    uint32_t gt_rxpolarity;
    uint32_t gt_txpolarity;
    uint32_t force_ready_mode;
    uint32_t to_altera_fpga;
    uint32_t gt_rxbyteisaligned;
    uint32_t gt_tx_reset_done;
    uint32_t gt_rx_reset_done;
    uint32_t gt_pll_lock;
    uint32_t slink_testled_n;
    uint32_t slink_lderrled_n;
    uint32_t slink_lupled_n;
    uint32_t slink_flowctrlled_n;
    uint32_t slink_activityled_n;
    uint32_t slink_lrl;
    for ( uint32_t iGt=0; iGt < 36; ++iGt ) {
      string issue_text = "";
      bool passed = false;
      try {
        // see https://svnweb.cern.ch/trac/atlastdaq/browser/FTK/DataFormatter/tags/DataFormatter-01-01-13/src/test_df.cc#L343
        m_dfApi->gt_monitor( iGt, gt_rxpolarity, gt_txpolarity,
                             force_ready_mode, to_altera_fpga, gt_rxbyteisaligned,
                             gt_tx_reset_done, gt_rx_reset_done, gt_pll_lock,
                             slink_testled_n, slink_lderrled_n,
                             slink_lupled_n, slink_flowctrlled_n, slink_activityled_n,
                             slink_lrl );
        passed = ( force_ready_mode == 0x1 ) || // if force_ready is on, ignore this transciever
                 (  
                    ( gt_rxbyteisaligned == 0x1 ) && // require byte aligned
                    ( slink_lderrled_n == 0x1 ) && // require LDERR="GOOD"
                    ( slink_lupled_n == 0x0 ) && // require LUP="GOOD"
                    ( slink_flowctrlled_n == 0x1 ) // require FLOWCTRL="OFF"
                 );
      } catch ( const std::exception& ex ) { 
        passed = false;
        issue_text += ex.what();
      }

      if (!passed) {
        char buffer [500];
        if ( issue_text.empty() ) {
          snprintf( buffer, 500, "%s Link has bad status: %2d.", m_name.c_str(), iGt );
          ftkException the_issue( ERS_HERE, name_ftk(), buffer );
          // check if link is in the m_failOnBadLink mask
          if ( ((uint64_t)0x1 << iGt) & m_failOnBadLink ) {
            throw(the_issue);
          }
          else { // if not, just issue a warning
            ers::warning(the_issue);
          }
        } else {
          snprintf( buffer, 500, "%s Caught exception when querying link status: %2d. ex.what() = %s", m_name.c_str(), iGt, issue_text.c_str() );
          ftkException the_issue( ERS_HERE, name_ftk(), buffer );
          // check if link is in the m_failOnBadLink mask
          if ( ((uint64_t)0x1 << iGt) & m_failOnBadLink ) {
            throw(the_issue);
          }
          else { // if not, just issue a warning
            ers::warning(the_issue);
          }
        }
      } // if (!passed)
    }

    ERS_LOG( "Done checking transciever status." );
  } // if (!m_dryRun)
  tLog.end(ERS_HERE);  // End log counter
  
}

/**********************************/    
void ReadoutModule_DataFormatter::doPrepareForRun( const daq::rc::TransitionCmd& cmd )
/**********************************/    
{
  ftkTimeLog tLog(ERS_HERE, name_ftk(), cmd.toString());
  //re-configure SLINK to enable HOLD from Aux / other DFs
  //ERS_LOG("Reset DF GT configuration, allowing for HOLD");
  //m_dfApi->gt_configuration(0);

  // reset histograms
  if ( m_histHandler ) m_histHandler->resetHists();

  // Reset the IS counters
  //  m_dfNamed->ExampleString = 0;
  m_dfNamed->ExampleU16    = 0;
  m_dfNamed->ExampleS32    = 0;
  m_dfNamed->SB_size       = 0;
  m_dfNamed->SB_freeze     = 0;
  m_dfNamed->SB_overflow   = 0;
  m_dfNamed->SB_position   = 0;
  m_dfNamed->SB_dimension  = 0;
  m_dfNamed->SB_nfragments = 0;
  m_dfNamed->SB_lvl1id     = 0;
  m_dfNamed->isRunning     = 0;

  m_statusRegisterATCAFactory->clear();

  if(! m_dryRun) {
    ftkTimeLog tLog1(ERS_HERE, name_ftk(), "Stopping pseudo data/enable SLINK");
    // stopping pseudo data and enabling SLINK
    // vector<uint32_t> vec_IMChannelidx = m_dfApi->getIMApi()->getIMLutIdx();
    // for(uint32_t ich = 0; ich < vec_IMChannelidx.size(); ich++) {
    
    for(uint32_t ichannel=0; ichannel < NUMBER_OF_DF_INPUT_LANES; ichannel++) {
      if(m_isactive.at(ichannel)) {
        if ( m_usePseudoData ) {
	  ERS_LOG( "Start stop pseudo data channel " << ichannel );
          m_dfApi->getIMApi()->control_pseudo_data( ichannel, 0x0, 0x0 );
          ERS_LOG( "Finished stop pseudo data channel "  << ichannel );
        }
        if ( !m_usePseudoData ) {
	  ERS_LOG( "Start enabling SLINK " << ichannel);
          m_dfApi->getIMApi()->control_slink( ichannel , 1 ); // enable slink
          ERS_LOG( "Finished enabling SLINK "  << ichannel );
        }
      }
    }
    tLog1.end(ERS_HERE);  // End log counter
    
    
    if ( m_usePseudoData ) {
      std::vector< std::chrono::system_clock::time_point > tmp_sw_skews;
      
      for(uint32_t ichannel=0; ichannel < NUMBER_OF_DF_INPUT_LANES; ichannel++) {
        if(m_isactive.at(ichannel)) {
          m_dfApi->getIMApi()->control_pseudo_data( ichannel, 0x0, 0x0 );
        }
        tmp_sw_skews.emplace_back( std::chrono::system_clock::now() );
      }
      auto last_time_stamp = std::chrono::system_clock::now(); // first one is just baseline
      
      ERS_LOG( "Pseudo data enable skew is ...");
      for(uint32_t ichannel = 0; ichannel < NUMBER_OF_DF_INPUT_LANES; ichannel++) {
        double elapsed = std::chrono::duration_cast<std::chrono::nanoseconds>( last_time_stamp - tmp_sw_skews.at( ichannel ) ).count();
        int skew_count_at_40MHz = int(elapsed / 25); // to translate 40MHz clock cycle 
        if(m_isactive.at(ichannel)) {
          ERS_LOG( "IM ch " << ichannel << " is     active : skew is " << elapsed << " [nsec] -> " << skew_count_at_40MHz );
          if( m_IM_pseudo_initial_delay ) m_dfApi->getIMApi()->control_pseudo_config( ichannel, skew_count_at_40MHz, m_IM_pseudo_data_xoff  );
        }else{
          ERS_LOG( "IM ch " << ichannel << " is non active : skew is " << elapsed << " [nsec] -> " << skew_count_at_40MHz );
        }
      }
      
      ftkTimeLog tLog2(ERS_HERE, name_ftk(), "Enabling pseudo data");
      for(uint32_t ichannel=0; ichannel < NUMBER_OF_DF_INPUT_LANES; ichannel++) {
        if(m_isactive.at(ichannel)) {
          m_dfApi->getIMApi()->control_pseudo_data( ichannel, 0x1, 0x1 );
        }
      }
      tLog2.end(ERS_HERE);  // End log counter
    } // m_usePseudoData
    
    
  }
  ERS_LOG( "Finished enabling pseudo data");
  m_startTime = std::time(nullptr);
  tLog.end(ERS_HERE);  // End log counter
}


/**************************/
void ReadoutModule_DataFormatter::doStopROIB( const daq::rc::TransitionCmd& /*cmd*/ )
/**************************/
{}

/**************************/
void ReadoutModule_DataFormatter::doStopDC( const daq::rc::TransitionCmd& /*cmd*/ )
/**************************/
{}

// In stopHLT, perform the actual stop procedure:
//   ** stop accepting data from upstream
//   ** publish final status to IS
//   ** clear FIFOs and buffers
//   ** check that buffers are succesfully cleared
void ReadoutModule_DataFormatter::doStopHLT( const daq::rc::TransitionCmd& cmd )
{
  ftkTimeLog tLog(ERS_HERE, name_ftk(), cmd.toString());
  m_dfApi->stopAcceptingData( m_usePseudoData, m_useBlockTransfer );
  doPublish( 0x0, true );
  m_dfApi->clearFIFOs( [&]() { m_configHandler->quickReset(); } );
  m_dfApi->clearMonitoring();
  m_dfApi->checkFIFOsCleared();
  tLog.end(ERS_HERE);  // End log counter
}

/**************************/
void ReadoutModule_DataFormatter::doStopRecording( const daq::rc::TransitionCmd& /*cmd*/ )
/**************************/
{}

/**************************/
void ReadoutModule_DataFormatter::doStopGathering( const daq::rc::TransitionCmd& /*cmd*/ )
/**************************/
{}

/**************************/
void ReadoutModule_DataFormatter::doStopArchiving( const daq::rc::TransitionCmd& /*cmd*/ )
/**************************/
{}

/**************************/
void ReadoutModule_DataFormatter::doDisconnect( const daq::rc::TransitionCmd& cmd )
/**************************/
{
  ftkTimeLog tLog(ERS_HERE, name_ftk(), cmd.toString());
  try{
    m_dfApi->setTxRxLine( 0x0, 0x0 );
    m_dfApi->setTxRxLineBert( 0x0, 0x0 );
  } catch ( const std::exception& ex ) {
    ERS_LOG( "Caught exception while resetting GTX. Ignoring. ex.what() = " << ex.what());
  }
  tLog.end(ERS_HERE);  // End log counter
}

/**************************/
void ReadoutModule_DataFormatter::doUnconfigure( const daq::rc::TransitionCmd& cmd )
/**************************/
{
  ftkTimeLog tLog(ERS_HERE, name_ftk(), cmd.toString());
  m_configHandler->unconfigure();

  m_configuration_done = false;
  tLog.end(ERS_HERE);  // End log counter
}

// Periodic monitoring operation
void ReadoutModule_DataFormatter::doPublish( uint32_t flag, bool finalPublish )
{
  ftkTimeLog tLog(ERS_HERE, name_ftk(), "Publish");
  // only issue a single warning for each GTX
  static std::vector<bool> gt_warned_already;

  if ( finalPublish ) {
    ERS_LOG( "Performing final publish after stopping dataflow." );
  } else {
    ERS_LOG( "Performing normal publish (in RUNNING state)." );

    ERS_LOG( "Checking gtx status." );
    // robmina 28.Jun.17 -- see https://its.cern.ch/jira/browse/FTKHWD-294
    //    only checking the first 36 transceivers (connected to downstream boards)
    uint32_t gt_rxpolarity;
    uint32_t gt_txpolarity;
    uint32_t force_ready_mode;
    uint32_t to_altera_fpga;
    uint32_t gt_rxbyteisaligned;
    uint32_t gt_tx_reset_done;
    uint32_t gt_rx_reset_done;
    uint32_t gt_pll_lock;
    uint32_t slink_testled_n;
    uint32_t slink_lderrled_n;
    uint32_t slink_lupled_n;
    uint32_t slink_flowctrlled_n;
    uint32_t slink_activityled_n;
    uint32_t slink_lrl;
    for ( uint32_t iGt=0; iGt < 36; ++iGt ) {
      if ( gt_warned_already.size() <= iGt ) gt_warned_already.push_back( false );

      bool passed = false;
      string issue_text = "";
      try {
        // see https://svnweb.cern.ch/trac/atlastdaq/browser/FTK/DataFormatter/tags/DataFormatter-01-01-13/src/test_df.cc#L343
        m_dfApi->gt_monitor( iGt, gt_rxpolarity, gt_txpolarity,
                             force_ready_mode, to_altera_fpga, gt_rxbyteisaligned,
                             gt_tx_reset_done, gt_rx_reset_done, gt_pll_lock,
                             slink_testled_n, slink_lderrled_n,
                             slink_lupled_n, slink_flowctrlled_n, slink_activityled_n,
                             slink_lrl );
        passed = ( force_ready_mode == 0x1 ) || // if force_ready is on, ignore this transciever
                 (  
                    ( gt_rxbyteisaligned == 0x1 ) && // require byte aligned
                    ( slink_lderrled_n == 0x1 ) && // require LDERR="GOOD"
                    ( slink_lupled_n == 0x0 ) // require LUP="GOOD"
                 ); // rmina 5 June '18 - removing requirement for LFLOWCTRL (was causing warnings in publish)
      } catch ( const std::exception& ex ) { 
        passed = false;
        issue_text = ex.what();
      }

      if (!passed && !gt_warned_already.at(iGt)) {
        gt_warned_already.at(iGt) = true;

        char buffer [500];
        if ( issue_text.empty() ) {
          snprintf( buffer, 500, "%s Link has bad status (possibly has gone down): %2d.", m_name.c_str(), iGt );
          ftkException the_issue( ERS_HERE, name_ftk(), buffer );
          ers::warning(the_issue);
        } else {
          snprintf( buffer, 500, "%s Caught exception when querying link status: %2d. ex.what() = %s", m_name.c_str(), iGt, issue_text.c_str() );
          ftkException the_issue( ERS_HERE, name_ftk(), buffer );
            ers::warning(the_issue);
        }
      }
    } // for loop on iGT

  } // if (finalPublish)
  
  //++++++++++++++++++++++++++++++++++++++
  // 1  Access registers via IPBus / I2C 
  //++++++++++++++++++++++++++++++++++++++
  ftkTimeLog tLog1(ERS_HERE, name_ftk(), "readout and publish SRFactory");
  ERS_LOG( "Readout and publish SRFactory");


  try {
    m_statusRegisterATCAFactory->readout();
  } catch ( const std::exception& ex ) {
    daq::ftk::ftkException theIssue( ERS_HERE, m_name, ex.what() );
    ers::warning( theIssue );
  }
  ERS_LOG( "Done publishing SRFactory");
  ERS_LOG( "Readout and publish DataFlowReaders");
  if(!finalPublish && interruptPublish()){
    ERS_LOG( "Interrupt encountered...exiting");
    return;
  }

  try {
    m_dfReaderDF->readIS();
  } catch ( const std::exception& ex ) {
    daq::ftk::ftkException theIssue( ERS_HERE, m_name, ex.what() );
    ers::warning( theIssue );
  }
  if(!finalPublish && interruptPublish()){
    ERS_LOG( "Interrupt encountered...exiting");
    return;
  }
  
  try {
    m_dfReaderDF->publishAllHistos();
  } catch ( const std::exception& ex ) {
    daq::ftk::ftkException theIssue( ERS_HERE, m_name, ex.what() );
    ers::warning( theIssue );
  }
  if(!finalPublish && interruptPublish()){
    ERS_LOG( "Interrupt encountered...exiting");
    return;
  }

  try {
    m_dfReaderIM->readIS();
  } catch ( const std::exception& ex ) {
    daq::ftk::ftkException theIssue( ERS_HERE, m_name, ex.what() );
    ers::warning( theIssue );
  }
  if(!finalPublish && interruptPublish()){
    ERS_LOG( "Interrupt encountered...exiting");
    return;
  }

  try {
    m_dfReaderIM->publishAllHistos();
  } catch ( const std::exception& ex ) {
    daq::ftk::ftkException theIssue( ERS_HERE, m_name, ex.what() );
    ers::warning( theIssue );
  }
  if(!finalPublish && interruptPublish()){
    ERS_LOG( "Interrupt encountered...exiting");
    return;
  }

  ERS_LOG( "Done publishing DataFlowReaders");

  m_dfApi->inputMonitoring( m_isactive_mask, m_IMChToRobId );

  if(!finalPublish && interruptPublish()){
    ERS_LOG( "Interrupt encountered...exiting");
    return;
  }
  tLog1.end(ERS_HERE);  // End log counter

  //++++++++++++++++++++++
  // 2 Publish in IS  and OH  
  //++++++++++++++++++++++
  ERS_LOG( "Publish in IS and OH ...");

  // TODO: populate the IS object with relevant info (spybuffer info, etc.)
  m_dfNamed->isRunning = ( finalPublish ) ? 0 : 1;
  // IS
  try { m_dfNamed->checkin(); }
  catch ( daq::is::Exception & ex ) { // Raise a warning or throw an exception. 
    daq::ftk::ISException e(ERS_HERE, m_name + ex.what()); 
    ers::warning(e);  //or throw e;  
  }

  if ( m_histHandler ) m_histHandler->publishHists();

  tLog.end(ERS_HERE);  // End log counter
}

/**************************/
void ReadoutModule_DataFormatter::doPublishFullStats( uint32_t flag )
/**************************/
{
  ftkTimeLog tLog(ERS_HERE, name_ftk(), "PublishFullStats");
  ERS_LOG( "PublishFullStats: Entering. Check availability.");

  //++++++++++++++++++++++++++++++++++++++
  // 1  Manage spyBuffer
  //++++++++++++++++++++++++++++++++++++++
 
  //++++++++++++++++++++++
  // 1.1 Read the Spy Buffer in binary format
  //++++++++++++++++++++++

  // Data Formatter
  ftkTimeLog tLog1(ERS_HERE, name_ftk(), "DF: Read spybuffer in binary format");
  ERS_LOG( "Read spy buffer...");
  std::vector< std::shared_ptr< daq::ftk::DFSpyBuffer > > vec_DFSpyBuffer;

  try {
    m_dfApi->spy_freeze();
    m_dfApi->spy_read_enable();

    // read DF spy buffer
    vec_DFSpyBuffer.clear();
    for(uint32_t ichannel=0; ichannel<NUMBER_OF_DF_LANES; ichannel++) {
      if(interruptPublish()){
        ERS_LOG( "Interrupt encountered...exiting");
        return;
      }
      std::shared_ptr< daq::ftk::DFSpyBuffer > sb_tmp = std::make_shared< daq::ftk::DFSpyBuffer >(ichannel, NUMBER_OF_WORDS_IN_DF_SPYBUFFERS, m_dfApi); // lane 34 ~ channel 0
      if(!m_doDFblockTransfer){
        if(sb_tmp->read_spybuffer(true)) {
          std::stringstream message;
          message << "Error reading DF spy buffer lane = " << ichannel;
          ers::warning( daq::ftk::ftkException( ERS_HERE, name_ftk(), message.str() ) );
        }
        //ERS_LOG("SpyBuffer size = " << sb_tmp->getBuffer().size());
        vec_DFSpyBuffer.push_back(sb_tmp);
      }else{
        if(sb_tmp->read_spybuffer_blocktransfer(true)) {
          std::stringstream message;
          message << "Error reading DF spy buffer (blocktransfer) lane = " << ichannel;
          ers::warning( daq::ftk::ftkException( ERS_HERE, name_ftk(), message.str() ) );
        }
        //ERS_LOG("SpyBuffer size = " << sb_tmp->getBuffer().size());
        vec_DFSpyBuffer.push_back(sb_tmp);
      }
    }
  
    m_dfApi->spy_restart();
    m_dfApi->spy_read_disable();
  } catch ( const std::exception& ex ) {
    daq::ftk::ftkException theIssue( ERS_HERE, m_name, ex.what() );
    ers::warning( theIssue );
  }
  tLog1.end(ERS_HERE);  // End log counter
  
  // Input Mezzanine

  // depth of IM spybuffer
  const uint32_t im_spy_depth = 4096;
  
  // 1k words x 4 times readout are performed to readout 4k spybuffer info.
  // Buffer should be reset at 1st time of 4 readouts. 
  bool do_reset = true;
  
  std::vector< std::shared_ptr< daq::ftk::DFSpyBuffer > > vec_IMSpyBuffer;
  vec_IMSpyBuffer.clear();
  
  ftkTimeLog tLog2(ERS_HERE, name_ftk(), "IM: Read spybuffer in binary format");
  uint32_t laneId = 52;// DF laneID used for IM spybuffers
  for(uint32_t inspy_ospy = 0; inspy_ospy < 2; inspy_ospy++){// 0:inspy, 1:outspy
    for(uint32_t ichannel = 0; ichannel < NUMBER_OF_DF_INPUT_LANES; ichannel++) {

      if(inspy_ospy == 0) ERS_LOG( "Start readout spybuffer of IM inspy ch" << ichannel);
      else ERS_LOG( "Start readout spybuffer of IM outspy ch" << ichannel);

      std::string im_spy_register_name;
      if( inspy_ospy == 0 ){
        if( ichannel%2 == 0) im_spy_register_name = "IA_CH0_ISPY_BEGIN";
        if( ichannel%2 == 1) im_spy_register_name = "IA_CH1_ISPY_BEGIN";
      }else if( inspy_ospy == 1 ){
        if( ichannel%2 == 0) im_spy_register_name = "IA_CH0_OSPY_BEGIN";
        if( ichannel%2 == 1) im_spy_register_name = "IA_CH1_OSPY_BEGIN";
      }
      
      std::shared_ptr< daq::ftk::DFSpyBuffer > sb_tmp = std::make_shared< daq::ftk::DFSpyBuffer >(laneId, im_spy_depth, m_dfApi); 
      
      for(int ireadout = 0; ireadout < 4; ireadout++){// read 1k x 4 spy buffer
        
        try {
          m_dfApi->getIMApi()->read_from_IMBuffer_to_DFBuffer( ichannel, im_spy_register_name, ireadout, true, 1024 );
        } catch ( const std::exception& ex ) {
          daq::ftk::ftkException theIssue( ERS_HERE, m_name, ex.what() );
          ers::warning( theIssue );
          continue;
        }
        
        try {
          m_dfApi->spy_freeze();
          m_dfApi->spy_read_enable();
          
          do_reset = ( ireadout == 0 ) ? true : false;
          bool is_print = ( ireadout == 0 && ichannel == 11 && inspy_ospy == 0 ) ? true : false;
          
          if(!m_doDFblockTransfer){
            if(sb_tmp->read_spybuffer(do_reset, NUMBER_OF_WORDS_IN_DF_SPYBUFFERS)) {
              std::stringstream message;
              message << "Error reading IM spy buffer lane = " << laneId;
              ers::warning( daq::ftk::ftkException( ERS_HERE, name_ftk(), message.str() ) );
            }
          }else{
            if(sb_tmp->read_spybuffer_blocktransfer(do_reset, NUMBER_OF_WORDS_IN_DF_SPYBUFFERS, is_print )) {
              std::stringstream message;
              message << "Error reading IM spy buffer (blocktransfer) lane = " << laneId;
              ers::warning( daq::ftk::ftkException( ERS_HERE, name_ftk(), message.str() ) );
            }
          }

          m_dfApi->spy_restart();
          m_dfApi->spy_read_disable();
        } catch ( const std::exception& ex ) {
          daq::ftk::ftkException theIssue( ERS_HERE, m_name, ex.what() );
          ers::warning( theIssue );
        }

      }// 4 x 1024 spy information
      vec_IMSpyBuffer.push_back(sb_tmp);
      
      try {
        m_dfApi->getIMApi()->send_freeze(ichannel, false);
      } catch ( const std::exception& ex ) {
        daq::ftk::ftkException theIssue( ERS_HERE, m_name, ex.what() );
        ers::warning( theIssue );
        break; 
      }

      // readout from DF register to EMon vector
      if(interruptPublish()){
        ERS_LOG( "Interrupt encountered...exiting");
        return;
      }

    }// channels

  }// inspy or outspy
  tLog2.end(ERS_HERE);  // End log counter
  

  ERS_LOG("Done reading spy buffer");
  
  //++++++++++++++++++++++
  // 1.2 Ship the spybuffer to EMonDataOut module
  //++++++++++++++++++++++
  ftkTimeLog tLog3(ERS_HERE, name_ftk(), "shipping DF+IM spybuffer to EMonDataOut module");
  ERS_LOG("Ship the DF spybuffer to EMonDataOut module ...");
  if ( vec_DFSpyBuffer.size() )
    shipSpyBuffer(vec_DFSpyBuffer, daq::ftk::BoardType::DF);

  ERS_LOG("Ship the IM spybuffer to EMonDataOut module ...");
  if ( vec_IMSpyBuffer.size() )
    shipSpyBuffer(vec_IMSpyBuffer, daq::ftk::BoardType::IM);

  ERS_LOG("Done shipping spybuffer to EMon");
  tLog3.end(ERS_HERE);  // End log counter
  
  //++++++++++++++++++++++
  // 1.3 Decode the spybuffer to extract information to be published in IS
  //++++++++++++++++++++++
  ftkTimeLog tLog4(ERS_HERE, name_ftk(), "decode of DF+IM spybuffer to extract info for publish in IS");
  ERS_LOG("Decode spy buffer ...");
  // DF spy buffer
  std::vector<std::vector<daq::ftk::EventFragmentHitClusters*> > vec_DF_fragments;
  for(uint32_t ichannel=0; ichannel<NUMBER_OF_DF_LANES; ichannel++) {
    ERS_LOG("Decode DF lane = " << ichannel);
    std::vector<daq::ftk::EventFragmentHitClusters*> DF_fragment;
    DF_fragment  = FTKPacket_parseFragments<daq::ftk::EventFragmentHitClusters>(vec_DFSpyBuffer.at(ichannel)->getBuffer(),16);
    vec_DF_fragments.push_back( DF_fragment );
  }
  tLog4.end(ERS_HERE);  // End log counter


  ERS_LOG("Done decoding spy buffer");
  
  //++++++++++++++++++++++
  // 1.4 Delete the memory when you have done
  //++++++++++++++++++++++
  //delete[] spyBuff;//?

  //++++++++++++++++++++++++++++++++++++++
  // 2  Fill monitoring distributions
  //++++++++++++++++++++++++++++++++++++++
  // all of this is handled by the histHandler, which also clears the memory
  if ( m_histHandler ) m_histHandler->publishFullStatsHists( vec_DF_fragments, m_configHandler->getLaneIdx2Mod() );

  ERS_LOG("Finished Publishing full stats info");
  tLog.end(ERS_HERE);  // End log counter
}


/**************************/
void ReadoutModule_DataFormatter::clearInfo()
/**************************/
{
  /*  ERS_LOG("Clear histograms and counters");
  // Reset histograms
  m_Histogram->Reset();

  // TODO: Reset the IS counters (currently only isRunning is used)
  //  m_dfNamed->ExampleString = 0;
  */
  m_dfNamed->isRunning = 0;
}


//FOR THE PLUGIN FACTORY
extern "C"
{
  extern ROS::ReadoutModule* createReadoutModule_DataFormatter();
}

ROS::ReadoutModule* createReadoutModule_DataFormatter()
{
  return (new ReadoutModule_DataFormatter());
}




/**********************************/   
void ReadoutModule_DataFormatter::shipSpyBuffer(std::vector< std::shared_ptr< daq::ftk::DFSpyBuffer > > & vecSpyBuffers, daq::ftk::BoardType boardType)
/**********************************/   
{
  // Protection for m_ftkemonDataOut pointer
  if(!m_ftkemonDataOut)
    return;

  //total number of words contained in the SpyBuffers
  uint32_t num_words = 0;

  //vector of the pairs to be shipped
  std::vector< std::pair< std::shared_ptr< daq::ftk::SpyBuffer >, uint32_t > > vec_pairs;

  uint32_t inoutBorder = (boardType == daq::ftk::BoardType::IM) 
                              ? NUMBER_OF_DF_INPUT_LANES : NUMBER_OF_DF_OUTPUT_LANES;
  // make the board numbering consistent with DF configuration
  //uint32_t boardNumber = 8*(m_shelf-1) + m_slot-3;
  uint32_t boardNumber = m_board_number;

  // Loop over spybuffers
  for(uint32_t isb = 0; isb < vecSpyBuffers.size(); isb++) {
    daq::ftk::Position inout;
    if (boardType == daq::ftk::BoardType::IM)
      inout = (isb < inoutBorder) ? daq::ftk::Position::IN : daq::ftk::Position::OUT;
    else
      inout = (isb < inoutBorder) ? daq::ftk::Position::OUT : daq::ftk::Position::IN;

    //creating the source ID for the SpyBuffer       
    uint32_t sourceid;
    sourceid = daq::ftk::encode_SourceIDSpyBuffer(boardType, 
                                                  boardNumber, 
                                                  isb, 
                                                  inout);

    num_words += vecSpyBuffers[isb]->getBuffer().size();

    //creating the pair
    std::pair< std::shared_ptr< daq::ftk::SpyBuffer >, uint32_t > pair2ship = std::make_pair( vecSpyBuffers[isb], sourceid);

    //ERS_LOG("Push back spybuffer lane = " << isb);
    vec_pairs.push_back(pair2ship);
  }

  // Make SpyBuffers available to FtkEMonDataOut
  ERS_LOG("Make SpyBuffers available to EMonDataOut ...");
  m_ftkemonDataOut->sendData(vec_pairs);
  ERS_LOG("SpyBuffers total number of words: " << num_words << " were made available to FtkEMonDataOut" );
  
}


/**********************************/   
bool ReadoutModule_DataFormatter::compareModuleIDs(std::vector<uint32_t> vec_IDs_sb, std::vector<uint32_t> vec_IDs_config)
/**********************************/ 
{
  bool is_correct = false;
  std::sort (vec_IDs_sb.begin(), vec_IDs_sb.end());
  std::sort (vec_IDs_config.begin(), vec_IDs_config.end());

  if(vec_IDs_sb.size()>vec_IDs_config.size()){
    for(uint32_t imo=0; imo<vec_IDs_config.size(); imo++)     ERS_LOG("(Spy buffer, Configuration) ModuleID = (" <<vec_IDs_sb.at(imo) << ", "  << vec_IDs_config.at(imo) << ")" );
    for(uint32_t imo=vec_IDs_config.size(); imo<vec_IDs_sb.size(); imo++)  ERS_LOG("(Spy buffer, Configuration) ModuleID = (" <<vec_IDs_sb.at(imo) << ", --)" );
  }
  else{
    for(uint32_t imo=0; imo<vec_IDs_sb.size(); imo++)     ERS_LOG("(Spy buffer, Configuration) ModuleID = (" <<vec_IDs_sb.at(imo) << ", "  << vec_IDs_config.at(imo) << ")" );
    for(uint32_t imo=vec_IDs_sb.size(); imo<vec_IDs_config.size(); imo++)  ERS_LOG("(Spy buffer, Configuration) ModuleID = (--, " << vec_IDs_config.at(imo) << ")" );
  }
  
  if(vec_IDs_sb==vec_IDs_config) is_correct = true;
  return is_correct;
}


///Function used to catch the User Command sent by the Watchdog and remove the IM BP
/**********************************/
void ReadoutModule_DataFormatter::doUser(const daq::rc::UserCmd& cmd) 
/**********************************/
{
  const std::string& cmdName = cmd.commandName();

  ERS_LOG("Received user command \"" + cmdName );

  if (cmdName == "Disable_BP" && m_configuration_done) {

    ERS_LOG("Proceding with the command \"" + cmdName + "\""); 

    for(uint32_t iFPGA = 0; iFPGA < NUMBER_OF_IM_FPGAS_PER_DF; iFPGA++) {
      m_dfApi->getIMApi()->control_xoff( iFPGA*2 + 0, 0x0 );
      m_dfApi->getIMApi()->control_xoff( iFPGA*2 + 1, 0x0 );
      // m_dfApi->getIMApi()->i2c_32b_write_register(iFPGA, adress1, 0x0, m_useBlockTransfer);
      // m_dfApi->getIMApi()->i2c_32b_write_register(iFPGA, adress2, 0x0, m_useBlockTransfer);
    }
    ERS_LOG("Disable BP Done");
  }
}

/*
////////////////
int ReadoutModule_DataFormatter::testIPBUSSpeed(FtkDataFormatterApi *df_api, const uint32_t size)
{
  int ret = 0X0;

  Timer timer;
  uint32_t tmp_register = 0X0;
  std::vector<uint32_t> buffer;
  buffer.clear();
  buffer.reserve(size);
  const uint32_t maxSize = 100000;
  uint32_t lane = 39;

  df_api->spy_freeze();
  
  // ordinary single access
  buffer.clear();
  timer.reset();
  for (uint32_t iAddr = 0; iAddr < size; iAddr++) {
    tmp_register = 0x0;
    try {
      df_api->get_ipbusApi()->single_access_read("reg.spy_readout", tmp_register);
    } catch (char *) {
      ret = 1;
    }
    buffer.push_back(tmp_register);
  }
  ERS_LOG ("Ordinary single access took " << timer.elapsed() << " seconds");
  //for (uint32_t iAddr = 0; iAddr < 22; iAddr++) ERS_LOG("Buffer word = " << hex << buffer.at(iAddr));

  // block transfer (of spy buffer)
  buffer.clear();
  timer.reset();
  df_api->get_ipbusApi()->single_access_write("reg.spy_laneselector", lane);
  uhal::ValWord< uint32_t > mems[maxSize];
  if (size > maxSize) {
    char error_msg[BUFSIZ];
    snprintf(error_msg, sizeof(error_msg), 
         "Exception: m_MaxSiz=%3d : #registers=%3d \n",
         maxSize,
         size);
    throw daq::ftk::IPBusIssue(ERS_HERE, error_msg);
  }

  for (uint32_t iAddr = 0; iAddr < size; iAddr++){
   df_api->get_ipbusApi()->get_hardware()->getNode("reg.spy_readaddr").write(iAddr);
   mems[iAddr] = df_api->get_ipbusApi()->get_hardware()->getNode("reg.spy_readout").read();
  }
  df_api->get_ipbusApi()->get_hardware()->dispatch(); // send transaction
  for (uint32_t iAddr = 0; iAddr < size; iAddr++)  buffer.push_back(mems[iAddr].value());
  ERS_LOG ("Block transfer took " << timer.elapsed() << " seconds");

  for (uint32_t iAddr = 0; iAddr < 15; iAddr++) ERS_LOG("Buffer word = " << hex << buffer.at(iAddr));

  // spy buffer readout
  buffer.clear();
  timer.reset();
  df_api->get_ipbusApi()->single_access_write("reg.spy_laneselector", lane);
  for (uint32_t iAddr = 0; iAddr < size; iAddr++) {
    df_api->get_ipbusApi()->single_access_write("reg.spy_readaddr", iAddr%NUMBER_OF_WORDS_IN_DF_SPYBUFFERS);
    tmp_register = 0x0;
    try {
      df_api->get_ipbusApi()->single_access_read("reg.spy_readout", tmp_register);
    } catch (char *) {
      ret = 1;
    }
    buffer.push_back(tmp_register);
  }
  ERS_LOG ("Spy buffer took " << timer.elapsed() << " seconds");

  df_api->spy_restart();
  
  return ret;
}
*/
