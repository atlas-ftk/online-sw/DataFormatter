#include <DataFormatter/StatusRegisterDFAccessHistos.h>
#include "ftkcommon/exceptions.h"
#include "ftkcommon/Utils.h"

using namespace daq::ftk;

StatusRegisterDFAccessHistos::StatusRegisterDFAccessHistos(std::shared_ptr<FtkDataFormatterApi> dfapi,
							   uint32_t selectorValue,
							   uint32_t selectorAddress,
							   uint32_t readerAddress,
							   srType type) 
  : m_dfapi( dfapi )
{
  // initialize parent class protected members

  m_access  = srAccess::IPBus_selector; 
  m_type    = type;
  m_write_nodes.push_back( dfapi->getRegisterNameFromAddress( selectorAddress ) );
  m_write_values.push_back( selectorValue );
  m_node    = dfapi->getRegisterNameFromAddress( readerAddress );
}

StatusRegisterDFAccessHistos::~StatusRegisterDFAccessHistos()
{
  m_write_nodes.clear();
  m_write_values.clear();
}

//============================================================
void 
StatusRegisterDFAccessHistos::readout()
{
  try {
    // Use selector to set lane to desired lane
    m_dfapi->read_using_selector( m_write_nodes, m_write_values,
				  m_node, m_value);
    // Read the bin output register
    m_dfapi->single_access_read(m_node, m_value);

    for(uint i=0; i<1024; i++){
      
      // Set the relevant bit in input_monitoring_hist_re 
      if(m_node=="reg.input_monitoring_hist_skew_hist_out"){
	m_dfapi->single_access_write("reg.input_monitoring_hist_re.value", 0x1);
      } else if(m_node=="reg.input_monitoring_ph_timeout_hist_out"){
	m_dfapi->single_access_write("reg.input_monitoring_hist_re.value", 0x2);
      } else if(m_node=="reg.input_monitoring_global_l1_skip_hist_out"){
	m_dfapi->single_access_write("reg.input_monitoring_hist_re.value", 0x4);
      } else if(m_node=="reg.input_monitoring_lanes_l1_skew_hist_out"){
	m_dfapi->single_access_write("reg.input_monitoring_hist_re.value", 0x8);
      }
      
      // Reset register to 0x0
      m_dfapi->single_access_write("reg.input_monitoring_hist_re", 0x0);
      
      m_dfapi->single_access_read(m_node, m_value);
    }
  } catch (daq::ftk::IPBusRead ex){
    m_value = srNoneValue;
    daq::ftk::IPBusIssue ex2(ERS_HERE, "failed to read out a DF status register",ex);
    ers::warning(ex2);
  }
}
  




