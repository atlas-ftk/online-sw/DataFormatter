#include "DataFormatter/DataFlowReaderDF.h"
#include "DataFormatter/DataFormatterConstants.h"
#include "ftkcommon/ReadSRFromISVectors.h"
// #include "DataFormatter/StatusRegisterDFAccessHistos.h"
#include "DataFormatter/dal/FtkDataFlowSummaryDFNamed.h"

#include "ftkcommon/Utils.h"

//OnlineServices to check if running within a partition
#include <ipc/partition.h>
#include <ipc/core.h>
#include <memory>

// ers
#include <ers/ers.h>
#include "ftkcommon/exceptions.h"



using namespace std;
using namespace daq::ftk;

/************************************************************/
DataFlowReaderDF::DataFlowReaderDF(std::shared_ptr<OHRawProvider<> > ohRawProvider, uint32_t df_fwVersion, bool forceIS ) 
   : DataFlowReader(ohRawProvider, forceIS),
     m_df_fwVersion( df_fwVersion )

/************************************************************/
{
  
}


/***************************************************************************************************************/
void DataFlowReaderDF::init(const string& deviceName, const string& partitionName, const string& isServerName)
/***************************************************************************************************************/
{
  DataFlowReader::init(deviceName, partitionName, isServerName);
  DataFlowReader::name_dataflow();
  m_theDFSummary = std::make_shared<FtkDataFlowSummaryDFNamed>(m_ipcp,name_dataflow());
  m_theSummary= std::static_pointer_cast<FtkDataFlowSummaryNamed>(m_theDFSummary);
      
  ERS_LOG("Initializing DF dataflowsummary: "<<name_dataflow()<<" :pointer");
  	
}


/************************************************************/
vector<string> DataFlowReaderDF::getISVectorNames()
/************************************************************/
{
  vector<string> srv_names;

  srv_names.push_back("DF_wcount_sr_v");
  srv_names.push_back("DF_cwcount_sr_v");
  srv_names.push_back("DF_xoffcount_sr_v");
  srv_names.push_back("DF_xoffmon_sr_v");
  srv_names.push_back("DF_gt1_sr_v");
  srv_names.push_back("DF_gt2_sr_v");
  srv_names.push_back("DF_gt3_sr_v");
  srv_names.push_back("DF_fmcenable_sr_v");
  srv_names.push_back("DF_inxoff_sr_v");
  srv_names.push_back("DF_infullfr_sr_v");
  srv_names.push_back("DF_sfos_sr_v");
  srv_names.push_back("DF_sfcs_sr_v");
  srv_names.push_back("DF_sfilos_sr_v");
  srv_names.push_back("DF_sfilis_sr_v");
  srv_names.push_back("DF_sfolos_sr_v");
  srv_names.push_back("DF_inbusy_sr_v");
  srv_names.push_back("DF_outbusy_sr_v");
  srv_names.push_back("DF_inempty_sr_v");
  srv_names.push_back("DF_outempty_sr_v");
  srv_names.push_back("DF_l1id_sr_v");
  srv_names.push_back("DF_npackets_discarded_sr_v");
  srv_names.push_back("DF_npackets_discarded_sync_sr_v");
  srv_names.push_back("DF_inputskew_sync_sr_v");
  srv_names.push_back("DF_num_packets_discarded_synch_full_sr_v"); 
  srv_names.push_back("DF_num_packets_held_synch_full_sr_v"); 
  srv_names.push_back("DF_num_packets_timedout_synch_full_sr_v"); 
  srv_names.push_back("DF_num_packets_discarded_full_sr_v"); 
  srv_names.push_back("DF_num_mods_hit_word_limit_full_sr_v"); 
  // srv_names.push_back("DF_input_monitoring_skew_sr_v");
  // srv_names.push_back("DF_input_monitoring_PH_timeout_full_sr_v");
  // srv_names.push_back("DF_input_monitoring_global_L1skip_sr_v");
  // srv_names.push_back("DF_input_monitoring_lane_L1skip_full_sr_v");
  srv_names.push_back("DF_slink_total_evts_given_up_v");
  srv_names.push_back("DF_slink_total_modules_given_up_v");
  srv_names.push_back("DF_slink_evtsorting_monitor_v");
  srv_names.push_back("DF_fmcin_fifo_sr_v");
  srv_names.push_back("DF_int_link_rx_fifo_sr_v");
  srv_names.push_back("DF_int_link_tx_fifo_sr_v");  
  srv_names.push_back("DF_slink_fifo_1_sr_v");  
  srv_names.push_back("DF_slink_fifo_2_sr_v");  
  srv_names.push_back("DF_odo_evt_sort_buff_fifo_1_sr_v");  
  srv_names.push_back("DF_odo_evt_sort_buff_fifo_2_sr_v");
  srv_names.push_back("DF_fmcin_bp_sr_v");
  srv_names.push_back("DF_ili_bp_sr_v");
  srv_names.push_back("DF_odo_bp_sr_v");
  srv_names.push_back("DF_ilo_bp_sr_v");
  srv_names.push_back("DF_input_global_freeze_sr_v");
  srv_names.push_back("DF_slink_ldc_freeze_sr_v");
  srv_names.push_back("DF_fmcout_to_im_freeze_sr_v");
  srv_names.push_back("DF_ido_clusters_per_evt_sr_v");
  srv_names.push_back("DF_ili_clusters_per_evt_sr_v");
  srv_names.push_back("DF_odo_clusters_per_evt_sr_v");
  srv_names.push_back("DF_ilo_clusters_per_evt_sr_v");
  srv_names.push_back("DF_b0f_b0f_over_b0f_e0f_sr_v");  
  return srv_names;
}

/************************************************************/
void DataFlowReaderDF::getFifoOutBusy(vector<int64_t>& srv)
/************************************************************/
{
  if ( m_df_fwVersion >= 0x19000 ) {
    srv.clear();

    for (uint32_t ilane=0;ilane< NUMBER_OF_DF_OUTPUT_LANES;ilane++) {
      srv.push_back(readRegister(ilane,0,0x4D,0x4F) & 0x1);
    }
  } else {
    srv.clear();
    srv.push_back(-1);
  }
}
/************************************************************/
void DataFlowReaderDF::getFifoInBusy(vector<int64_t>& srv)
/************************************************************/
{
  if ( m_df_fwVersion >= 0x19000 ) {
    srv.clear();

    for (uint32_t ilane=36;ilane<NUMBER_OF_DF_LANES;ilane++) {
      srv.push_back(readRegister(ilane,0,0x4D,0x4F) & 0x1);
    }
	} else {
    srv.clear();

    uint32_t input_buffer_busy = readRegister(0x0E);
    for (uint32_t ilane=0;ilane< NUMBER_OF_DF_INPUT_LANES;ilane++) {
      srv.push_back((input_buffer_busy>>ilane)&0x1);
    }
  }
}

/************************************************************/
void DataFlowReaderDF::getFifoOutEmpty(vector<int64_t>& srv)
/************************************************************/
{
  if ( m_df_fwVersion >= 0x19000 ) {
    srv.clear();

    for (uint32_t ilane=0; ilane<NUMBER_OF_DF_OUTPUT_LANES; ilane++) {
      srv.push_back(readRegister(ilane,0,0x4D,0x4E) & 0x1);
    }
  } else {
    srv.clear();
    srv.push_back(-1);
  }
}

/************************************************************/
void DataFlowReaderDF::getFifoInEmpty(vector<int64_t>& srv)
/************************************************************/
{
  if ( m_df_fwVersion >= 0x19000 ) {
    srv.clear();
    for (uint32_t ilane=36; ilane<NUMBER_OF_DF_LANES; ilane++) {
      srv.push_back(readRegister(ilane,0,0x4D,0x4E) & 0x1);
    }
  } else {
    srv.clear();
    srv.push_back(-1);
  }
}

/************************************************************/
void DataFlowReaderDF::getFifoInBusyFraction(vector<float>& srv, uint32_t)
/************************************************************/
{
  if ( m_df_fwVersion >= 0x19000 ) {
    srv.clear();

    for (uint32_t ilane=36;ilane<NUMBER_OF_DF_LANES;ilane++) {
      uint32_t temp = readRegister(ilane,0,0x4D,0x4F);
      float input_busy_fraction = (((temp>>1)&0x7FFFFFFF)/(float)CLOCK_SPEED);
      srv.push_back(input_busy_fraction);
    }
  } else {
    srv.clear();
	  srv.push_back(-1);
  }
}
/************************************************************/
void DataFlowReaderDF::getFifoInEmptyFraction(vector<float>& srv, uint32_t)
/************************************************************/
{
  if (m_df_fwVersion >= 0x19000) { 
    srv.clear();

    for (uint32_t ilane=36;ilane<NUMBER_OF_DF_LANES;ilane++) {
      int32_t temp = readRegister(ilane,0,0x4D,0x4E);
      float input_empty_fraction = (((temp>>1)&0x7FFFFFFF)/(float)CLOCK_SPEED);
      srv.push_back(input_empty_fraction);
    }
  } else {
    srv.clear();
    srv.push_back(-1);
  }
}
/************************************************************/
void DataFlowReaderDF::getFifoOutBusyFraction(vector<float>& srv, uint32_t)
/************************************************************/
{
  if (m_df_fwVersion >= 0x19000){
    srv.clear();

    for (uint32_t ilane=0;ilane<NUMBER_OF_DF_OUTPUT_LANES;ilane++) {
      uint32_t temp = readRegister(ilane,0,0x4D,0x4F);
      float output_busy_fraction = (((temp>>1)&0x7FFFFFFF)/(float)CLOCK_SPEED);
      srv.push_back(output_busy_fraction);
    }
  } else {
    srv.clear();
    srv.push_back(-1);
  }
}
/************************************************************/
void DataFlowReaderDF::getFifoOutEmptyFraction(vector<float>& srv, uint32_t)
/************************************************************/
{
  if (m_df_fwVersion >= 0x19000){
    srv.clear();

    for (uint32_t ilane=0;ilane< NUMBER_OF_DF_OUTPUT_LANES;ilane++) {
      uint32_t temp = readRegister(ilane,0,0x4D,0x4E);
      float output_empty_fraction = (((temp>>1)&0x7FFFFFFF)/(float)CLOCK_SPEED);
      srv.push_back(output_empty_fraction);
    }
  } else {
    srv.clear();
    srv.push_back(-1);
  }
}
/************************************************************/
void DataFlowReaderDF::getL1id(vector<int64_t>& srv)
/*************************************************************/
{
  if (m_df_fwVersion >= 0x19000){
    srv.clear();

    for (uint32_t ilane=0;ilane< NUMBER_OF_DF_INPUT_LANES;ilane++) {
      srv.push_back(readRegister(ilane,0,0x69,0x6A));
    }
  } else {
    srv.clear();
    srv.push_back(readRegister(0x5A));
  }
}
/******************************************************************************/
uint32_t DataFlowReaderDF::ParseInput(uint8_t temp)
/*****************************************************************************/
{
	uint32_t sum= 0;
	uint32_t array[8]={0x001,0x002,0x004,0x008,0x010,0x020,0x040,0x080};
	for (uint32_t i=0; i<8;i++){
	   
	   if(temp & array[i])
		{
		 sum += pow(2,(3+4*i));
		}
	}
	return(sum);
}

/************************************************************/
void DataFlowReaderDF::getNPacketsDiscarded(vector<int64_t>& srv)
/************************************************************/
{
  if (m_df_fwVersion >= 0x19000 && m_df_fwVersion < 0x19006){
    srv.clear();
    for (uint32_t ilane=0;ilane< NUMBER_OF_DF_INPUT_LANES;ilane++) {
      uint32_t temp=readRegister(ilane,0,0x69,0x6C);
      temp = ((temp >> 8)&0xff);
      uint32_t value = ParseInput(temp);
      srv.push_back(value);
    }
  } else if ( m_df_fwVersion >= 0x19006 ) {
    srv.clear();
    for (uint32_t ilane=0; ilane < NUMBER_OF_DF_INPUT_LANES; ilane++ ) {
      uint32_t val = readRegister(ilane, 0, 0x69, 0x46);
      srv.push_back(val);
    }
  }
  else{
    srv.clear();
    srv.push_back(-1);
  }
}
/************************************************************/
void DataFlowReaderDF::getNPacketsHeldSync(vector<int64_t>& srv)
/************************************************************/
{
  if (m_df_fwVersion >= 0x19000 && m_df_fwVersion < 0x19006){
    srv.clear();
    for (uint32_t ilane=0;ilane< NUMBER_OF_DF_INPUT_LANES;ilane++) {
      uint32_t temp= readRegister(ilane,0,0x69,0x6C);
      temp= (temp >> 24);
      uint32_t value = ParseInput(temp);
      srv.push_back(value);
    }
  } else if ( m_df_fwVersion >= 0x19006 ) {
    srv.clear();
    for (uint32_t ilane=0; ilane < NUMBER_OF_DF_INPUT_LANES; ilane++) {
      uint32_t val = readRegister(ilane, 0, 0x69, 0x44);
      srv.push_back(val);
    }
  } else {
    srv.clear();
    srv.push_back(-1);
  }
}

/************************************************************/
void DataFlowReaderDF::getNPacketsDiscardedSync(vector<int64_t>& srv)
/************************************************************/
{
  if (m_df_fwVersion >= 0x19000 && m_df_fwVersion < 0x19006){
    srv.clear();
    for (uint32_t ilane=0;ilane< NUMBER_OF_DF_INPUT_LANES;ilane++) {
      uint32_t temp= readRegister(ilane,0,0x69,0x5b);
      temp= (temp& 0xff);
      uint32_t value = ParseInput(temp);
      srv.push_back(value);
    }
  } else if ( m_df_fwVersion >= 0x19006 ) {
    srv.clear();
    for ( uint32_t ilane=0; ilane < NUMBER_OF_DF_INPUT_LANES; ilane++ ) {
      uint32_t val = readRegister(ilane, 0, 0x69, 0x42);
      srv.push_back(val);
    }
  } else {
    srv.clear();
    srv.push_back(-1);
  }
}

/************************************************************/
void DataFlowReaderDF::getNPacketsTimeoutSync(vector<int64_t>& srv)
/************************************************************/
{
  if (m_df_fwVersion >= 0x19000 && m_df_fwVersion < 0x19006){
    srv.clear();
    for (uint32_t ilane=0;ilane< NUMBER_OF_DF_INPUT_LANES;ilane++) {
      uint32_t temp= readRegister(ilane,0,0x69,0x6C);
      temp= ((temp >>16)& 0xff);
      uint32_t value = ParseInput(temp);
      srv.push_back(value);
    }
  } else if ( m_df_fwVersion >= 0x19006 ) {
    srv.clear();
    for (uint32_t ilane=0; ilane < NUMBER_OF_DF_INPUT_LANES; ilane++) {
      uint32_t val = readRegister(ilane, 0, 0x69, 0x45);
      srv.push_back(val);
    }
  } else {
    srv.clear();
    srv.push_back(-1);
  }
}

/************************************************************/
void DataFlowReaderDF::getNumModulesHitWordLimit(vector<int64_t>& srv)
/************************************************************/
{
  if (m_df_fwVersion >= 0x19000 && m_df_fwVersion < 0x19006){
    srv.clear();
    for (uint32_t ilane=0;ilane< NUMBER_OF_DF_INPUT_LANES;ilane++) {
      uint32_t temp= readRegister(ilane,0,0x69,0x6C);
      temp= (temp & 0xff);
      uint32_t value = ParseInput(temp);
      srv.push_back(value);
    }
  } else if ( m_df_fwVersion >= 0x19006 ) {
    srv.clear();
    for ( uint32_t ilane=0; ilane < NUMBER_OF_DF_INPUT_LANES; ilane++ ) {
      uint32_t val = readRegister(ilane, 0, 0x69, 0x49);
      srv.push_back(val);
    }
  } else {
    srv.clear();
    srv.push_back(-1);
  }
}
/************************************************************/
void DataFlowReaderDF::getInputSkewSync(vector<int64_t>& srv)
/*************************************************************/
{
  if (m_df_fwVersion >= 0x19000){
    srv.clear();
    srv.push_back(readRegister(0x65));
  }
  else {
    srv.clear();
    srv.push_back(-1);
  }
}

/************************************************************/
void DataFlowReaderDF::getFMCInXOff(vector<int64_t>& srv)
/************************************************************/
{
  srv.clear();

//XOFF monitor at FMCIN
  int input_xoff = readRegister(0x0F);

  for (uint32_t ilane=0;ilane< NUMBER_OF_DF_INPUT_LANES;ilane++) {
    srv.push_back((input_xoff>>ilane)&0x1);
  }
}

/************************************************************/
void DataFlowReaderDF::getSLINKOutXOff(vector<int64_t>& srv)
/************************************************************/
{
  srv.clear();

//XOFF monitor at SLINK OUT
  for (uint32_t ilane=0;ilane< NUMBER_OF_DF_OUTPUT_LANES;ilane++)
  {
    srv.push_back(readRegister(merge2x16bIn32b(3,0+ilane),0,0x74,0x70));
  }
}

/************************************************************/
void DataFlowReaderDF::getFMCInCount(vector<int64_t>& srv)
/************************************************************/
{
  srv.clear();

//FMCIN word counters (should go in a different function)
  for (uint32_t ilane=0;ilane< NUMBER_OF_DF_INPUT_LANES;ilane++)
    {
    srv.push_back(readRegister(merge2x16bIn32b(0,36+ilane),0,0x74,0x70));
  }
}



/************************************************************/
void DataFlowReaderDF::getSLINKOutCount(vector<int64_t>& srv)
/************************************************************/
{
  srv.clear();
  
  //SLINK out word counters
  for (uint32_t ilane=0;ilane< NUMBER_OF_DF_INPUT_LANES;ilane++)
    {
      srv.push_back(readRegister(merge2x16bIn32b(0,0+ilane),0,0x74,0x70));
    }
}

/************************************************************/
void DataFlowReaderDF::getSLINKOutEventsGivenUp(vector<int64_t>& srv)
/************************************************************/
{
  srv.clear();
  
  //SLINK out events given up counters
  for (uint32_t ilane=0;ilane< NUMBER_OF_DF_OUTPUT_LANES;ilane++)
    {
      srv.push_back(readRegister(merge2x16bIn32b(0,0+ilane),0,0x5e,0x55));
    }
}
/************************************************************/
void DataFlowReaderDF::getSLINKOutModsGivenUp(vector<int64_t>& srv)
/************************************************************/
{
  srv.clear();
  
  //SLINK out modules given up counters
  for (uint32_t ilane=0;ilane< NUMBER_OF_DF_OUTPUT_LANES;ilane++)
    {
      srv.push_back(readRegister(merge2x16bIn32b(0,0+ilane),0,0x5e,0x56));
    }
}
/************************************************************/
void DataFlowReaderDF::getSLINKOutEvtSortBuff(vector<int64_t>& srv)
/************************************************************/
{
  srv.clear();
  
  //SLINK out event sorting counters
  for (uint32_t ilane=0;ilane< NUMBER_OF_DF_OUTPUT_LANES;ilane++)
    {
      uint32_t tmp = readRegister(merge2x16bIn32b(0,0+ilane),0,0x5e,0x54);
      uint32_t tmp_even = (tmp & 0x1fff);
      uint32_t tmp_odd  = ((tmp >> 13) & 0x1fff);
      srv.push_back(tmp_even);
      srv.push_back(tmp_odd);
    }
}

/************************************************************/
void DataFlowReaderDF::getLinkOutStatus(vector<int64_t>& srv)
/************************************************************/
{
  srv.clear();

//SLINK xoff
  for (uint32_t ilink=0;ilink<NUMBER_OF_DF_GTX;ilink++)
    {
      //changed to align with the new definition as of ftkcommon-01-01-01 (1:ON, 0:OFF)
      srv.push_back( getBit(readRegister(0+ilink,0,0x09,0x08),6) ^ 1 );
    }
}
/************************************************************/
void DataFlowReaderDF::getFMCInFifoStatus(vector<int64_t>& srv)
/************************************************************/
{
  if(m_df_fwVersion >= 0x1900D){
    srv.clear();
    srv.push_back( (readRegister(0x33) >> 16) & 0xffff );
  } else {
    srv.clear();
    srv.push_back(-1);
  }
}
/************************************************************/
void DataFlowReaderDF::getFMCInFrontFifoStatus(vector<int64_t>& srv)
/************************************************************/
{
  if(m_df_fwVersion >= 0x1900D){
    srv.clear();
    srv.push_back( readRegister(0x33) & 0xffff );
  } else {
    srv.clear();
    srv.push_back(-1);
  }
}
/************************************************************/
void DataFlowReaderDF::getIntLinkRXFifoStatus(vector<int64_t>& srv)
/************************************************************/
{
  if(m_df_fwVersion >= 0x1900D){
    srv.clear();
    srv.push_back( readRegister(0x34) );
  } else {
    srv.clear();
    srv.push_back(-1);
  }
}

/************************************************************/
void DataFlowReaderDF::getIntLinkTXFifoStatus(vector<int64_t>& srv)
/************************************************************/
{
  if(m_df_fwVersion >= 0x1900D){
    srv.clear();
    srv.push_back( readRegister(0x35) );
  } else {
    srv.clear();
    srv.push_back(-1);
  }
}

/************************************************************/
void DataFlowReaderDF::getSlinkCh0to17FifoStatus(vector<int64_t>& srv)
/************************************************************/
{
  if(m_df_fwVersion >= 0x1900D){
    srv.clear();
    srv.push_back( readRegister(0x37) );
  } else {
    srv.clear();
    srv.push_back(-1);
  }
}

/************************************************************/
void DataFlowReaderDF::getSlinkCh18to35FifoStatus(vector<int64_t>& srv)
/************************************************************/
{
  if(m_df_fwVersion >= 0x1900D){
    srv.clear();
    srv.push_back( readRegister(0x38) );
  } else {
    srv.clear();
    srv.push_back(-1);
  }
}

/************************************************************/
void DataFlowReaderDF::getEvtSortBuffCh0to17FifoStatus(vector<int64_t>& srv)
/************************************************************/
{
  if(m_df_fwVersion >= 0x1900D){
    srv.clear();
    srv.push_back( readRegister(0x39) );
  } else {
    srv.clear();
    srv.push_back(-1);
  }
}

/************************************************************/
void DataFlowReaderDF::getEvtSortBuffCh18to35FifoStatus(vector<int64_t>& srv)
/************************************************************/
{
  if(m_df_fwVersion >= 0x1900D){
    srv.clear();
    srv.push_back( readRegister(0x3A) );
  } else {
    srv.clear();
    srv.push_back(-1);
  }
}
/************************************************************/
void DataFlowReaderDF::getFMCINBackPressure(vector<int64_t>& srv)
/************************************************************/
{
  if(m_df_fwVersion >= 0x1900D){
    srv.clear();
    for (uint32_t ilane=0;ilane<NUMBER_OF_DF_INPUT_LANES;ilane++){ 
      srv.push_back( readRegister(merge2x16bIn32b(0,0+ilane),0,0x3B,0x3C) );
    }
  } else {
    srv.clear();
    srv.push_back(-1);
  }
}

/************************************************************/
void DataFlowReaderDF::getILIBackPressure(vector<int64_t>& srv)
/************************************************************/
{
  if(m_df_fwVersion >= 0x1900D){
    srv.clear();
    for (uint32_t ilink=0;ilink<NUMBER_OF_INTERDF_LINKS; ilink++){ 
      //firstAddr is 0x200 & lastAddr is 0x218
      srv.push_back( readRegister(512+ilink,0,0x3B,0x3C) );
    }
  } else {
    srv.clear();
    srv.push_back(-1);
  }
}

/************************************************************/
void DataFlowReaderDF::getODOBackPressure(vector<int64_t>& srv)
/************************************************************/
{
  if(m_df_fwVersion >= 0x1900D){
    srv.clear();
    for (uint32_t ilane=0;ilane<32; ilane++){ 
      //firstAddr is 0x300 & lastAddr is 0x319
      srv.push_back( readRegister(768+ilane,0,0x3B,0x3C) );
    }
  } else {
    srv.clear();
    srv.push_back(-1);
  }
}
/************************************************************/
void DataFlowReaderDF::getILOBackPressure(vector<int64_t>& srv)
/************************************************************/
{
  if(m_df_fwVersion >= 0x1900D){
    srv.clear();
    for (uint32_t ilane=0;ilane<32; ilane++){ 
      //firstAddr is 0x400 & lastAddr is 0x419
      srv.push_back( readRegister(1024+ilane,0,0x3B,0x3C) );
    }
  } else {
    srv.clear();
    srv.push_back(-1);
  }
}

/************************************************************/
void DataFlowReaderDF::getInputGlobalFreezeCount(vector<int64_t>& srv)
/************************************************************/
{
  if(m_df_fwVersion >= 0x1900D){
    srv.clear();
    for (uint32_t ilane=0;ilane<NUMBER_OF_DF_INPUT_LANES; ilane++){ 
      //firstAddr is 0x1000 & lastAddr is 0x1010
      srv.push_back( readRegister(4096+ilane,0,0x3B,0x3C) );
    }
  } else {
    srv.clear();
    srv.push_back(-1);
  }
}

/************************************************************/
void DataFlowReaderDF::getSLINKLDCFreezeCount(vector<int64_t>& srv)
/************************************************************/
{
  if(m_df_fwVersion >= 0x1900D){
    srv.clear();
    for (uint32_t ilane=0;ilane<NUMBER_OF_DF_OUTPUT_LANES; ilane++){ 
      //firstAddr is 0x1300 & lastAddr is 0x1324
      srv.push_back( readRegister(4864+ilane,0,0x3B,0x3C) );
    }
  } else {
    srv.clear();
    srv.push_back(-1);
  }
}
/************************************************************/
void DataFlowReaderDF::getFMCOUTtoIMFreezeCount(vector<int64_t>& srv)
/************************************************************/
{
  if(m_df_fwVersion >= 0x1900D){
    srv.clear();
    //regAddr is 0x1500
    srv.push_back( readRegister(5376,0,0x3B,0x3C) );
  } else {
    srv.clear();
    srv.push_back(-1);
  }
}
/************************************************************/
void DataFlowReaderDF::getIDOClustersPerEvt(vector<int64_t>& srv)
/************************************************************/
{
  if(m_df_fwVersion >= 0x1900D){
    srv.clear();
    //firstAddr is 0x2100 & lastAddr is 0x2110
    for (uint32_t ilane=0; ilane<NUMBER_OF_DF_INPUT_LANES; ilane++){
      srv.push_back( readRegister(8448+ilane,0,0x3B,0x3C) );
    }
  } else {
    srv.clear();
    srv.push_back(-1);
  }
}
/************************************************************/
void DataFlowReaderDF::getILIClustersPerEvt(vector<int64_t>& srv)
/************************************************************/
{
  if(m_df_fwVersion >= 0x1900D){
    srv.clear();
    //firstAddr is 0x2200 & lastAddr is 0x2218
    for (uint32_t ilink=0; ilink<NUMBER_OF_INTERDF_LINKS; ilink++){
      srv.push_back( readRegister(8704+ilink,0,0x3B,0x3C) );
    }
  } else {
    srv.clear();
    srv.push_back(-1);
  }
}
/************************************************************/
void DataFlowReaderDF::getODOClustersPerEvt(vector<int64_t>& srv)
/************************************************************/
{
  if(m_df_fwVersion >= 0x1900D){
    srv.clear();
    //firstAddr is 0x2300 & lastAddr is 0x2324
    for (uint32_t ilane=0; ilane<NUMBER_OF_DF_OUTPUT_LANES; ilane++){
      srv.push_back( readRegister(8960+ilane,0,0x3B,0x3C) );
    }
  } else {
    srv.clear();
    srv.push_back(-1);
  }
}
/************************************************************/
void DataFlowReaderDF::getILOClustersPerEvt(vector<int64_t>& srv)
/************************************************************/
{
  if(m_df_fwVersion >= 0x1900D){
    srv.clear();
    //firstAddr is 0x2400 & lastAddr is 0x2418
    for (uint32_t ilink=0; ilink<NUMBER_OF_INTERDF_LINKS; ilink++){
      srv.push_back( readRegister(9216+ilink,0,0x3B,0x3C) );
    }
  } else {
    srv.clear();
    srv.push_back(-1);
  }
}
/************************************************************/
void DataFlowReaderDF::getB0F2B0FoverB0F2E0F(vector<int64_t>& srv)
/************************************************************/
{
  if(m_df_fwVersion >= 0x1900D){
    srv.clear();
    //firstAddr is 0x3000 & lastAddr is 0x3024
    for (uint32_t ilane=0; ilane<NUMBER_OF_DF_OUTPUT_LANES; ilane++){
      srv.push_back( readRegister(12288+ilane,0,0x3B,0x3C) );
    }
  } else {
    srv.clear();
    srv.push_back(-1);
  }
}


// /***********************************************************/
// void DataFlowReaderDF::getSkewOutHist(vector<int64_t>& srv)
// /***********************************************************/
// {
//   if (m_df_fwVersion >= 0x19008){ 
//     srv.clear();
//     srv.push_back(readRegister(0x2A));
//   } else {
//     srv.clear();
//     srv.push_back(-1);
//   }
  
// }
// /***********************************************************/
// void DataFlowReaderDF::getPHtimeoutHist(vector<int64_t>& srv)
// /***********************************************************/
// {
//   if (m_df_fwVersion >= 0x19008){
//     srv.clear();
//     for (uint32_t ilane=0;ilane< NUMBER_OF_DF_INPUT_LANES;ilane++)
//       {
// 	srv.push_back(readRegister(ilane,0,0x69,0x2B));
//       }
//   } else {
//     srv.clear();
//     srv.push_back(-1);
//   }
// }

// /***********************************************************/
// void DataFlowReaderDF::getGlobalL1SkipHist(vector<int64_t>& srv)
// /***********************************************************/
// {
//   if (m_df_fwVersion >= 0x19008){
//     srv.clear();
//     srv.push_back(readRegister(0x2C));
//   } else {
//     srv.clear();
//     srv.push_back(-1);
//   }
// }

// /***********************************************************/
// void DataFlowReaderDF::getLaneL1SkipHist(vector<int64_t>& srv)
// /***********************************************************/
// {
//   if (m_df_fwVersion >= 0x19008){
//     srv.clear();
//     for (uint32_t ilane=0;ilane< NUMBER_OF_DF_INPUT_LANES;ilane++)
//       {
// 	srv.push_back(readRegister(ilane,0,0x69,0x2D));
//       }
//   } else {
//     srv.clear();
//     srv.push_back(-1);
//   }
// }


/************************************************************/
void DataFlowReaderDF::publishExtraHistos(uint32_t option)
/************************************************************/
{
  std::vector<int64_t> srv;
    
  srv.clear();
  getFMCInXOff(srv);  
  publishSingleTH1F(srv,"FifoFMCInXOff");
  m_theDFSummary->FifoFMCInXOff = srv;
  
  srv.clear();
  getSLINKOutXOff(srv);
  publishSingleTH1F(srv,"FifoSLINKOutXOff");
  m_theDFSummary->FifoSLINKOutXOff = srv;

  srv.clear();
  getFMCInCount(srv);  
  publishSingleTH1F(srv,"FifoFMCInCount");
  m_theDFSummary->FifoFMCInCount = srv;

  srv.clear();
  getSLINKOutCount(srv);  
  publishSingleTH1F(srv,"FifoSLINKOutCount");
  m_theDFSummary->FifoSLINKOutCount = srv;

  srv.clear();
  getNumModulesHitWordLimit(srv);
  publishSingleTH1F(srv,"NumModulesHitWordLimit");
  m_theDFSummary->NumModulesHitWordLimit = srv; 
  
  // srv.clear();
  // getSkewOutHist(srv);
  // publishSingleTH1F(srv,"SkewOutHist");
  // m_theDFSummary->SkewOutHist = srv;
  
  // srv.clear();
  // getPHtimeoutHist(srv);
  // publishSingleTH1F(srv,"PHTimeout");
  // m_theDFSummary->PHTimeout = srv;

  // srv.clear();
  // getGlobalL1SkipHist(srv);
  // publishSingleTH1F(srv,"GlobalL1SkipHist");
  // m_theDFSummary->GlobalL1SkipHist = srv;

  // srv.clear();
  // getLaneL1SkipHist(srv);
  // publishSingleTH1F(srv,"LaneL1SkipHist");
  // m_theDFSummary->LaneL1SkipHist = srv;
  
  srv.clear();
  getSLINKOutEventsGivenUp(srv);
  publishSingleTH1F(srv,"SLINKOutEventsGivenUp");
  m_theDFSummary->SLINKOutEventsGivenUp = srv;
  
  srv.clear();
  getSLINKOutModsGivenUp(srv);
  publishSingleTH1F(srv,"SLINKOutModsGivenUp");
  m_theDFSummary->SLINKOutModsGivenUp = srv;

  srv.clear();
  getSLINKOutEvtSortBuff(srv);
  publishSingleTH1F(srv,"SLINKOutEvtSortBuff");
  m_theDFSummary->SLINKOutEvtSortBuff = srv;

  srv.clear();
  getFMCInFifoStatus(srv);
  publishSingleTH1F(srv,"FMCINFifoStatus");
  m_theDFSummary->FMCINFifoStatus = srv;

  srv.clear();
  getFMCInFrontFifoStatus(srv);
  publishSingleTH1F(srv,"FMCINFrontFifoStatus");
  m_theDFSummary->FMCINFrontFifoStatus = srv;
  
  srv.clear();
  getIntLinkRXFifoStatus(srv);
  publishSingleTH1F(srv,"IntLinkRXFifoStatus");
  m_theDFSummary->IntLinkRXFifoStatus = srv;

  srv.clear();
  getIntLinkTXFifoStatus(srv);
  publishSingleTH1F(srv,"IntLinkTXFifoStatus");
  m_theDFSummary->IntLinkTXFifoStatus = srv;
  
  srv.clear();
  getSlinkCh0to17FifoStatus(srv);
  publishSingleTH1F(srv,"SlinkCh0to17FifoStatus");
  m_theDFSummary->SlinkCh0to17FifoStatus = srv;

  srv.clear();
  getSlinkCh18to35FifoStatus(srv);
  publishSingleTH1F(srv,"SlinkCh18to35FifoStatus");
  m_theDFSummary->SlinkCh18to35FifoStatus = srv;
  
  srv.clear();
  getEvtSortBuffCh0to17FifoStatus(srv);
  publishSingleTH1F(srv,"EvtSortBuffCh0to17FifoStatus");
  m_theDFSummary->EvtSortBuffCh0to17FifoStatus = srv;
  
  srv.clear();
  getEvtSortBuffCh18to35FifoStatus(srv);
  publishSingleTH1F(srv,"EvtSortBuffCh18to35FifoStatus");
  m_theDFSummary->EvtSortBuffCh18to35FifoStatus = srv;

  srv.clear();
  getFMCINBackPressure(srv);
  publishSingleTH1F(srv,"FMCINBackPressure");
  m_theDFSummary->FMCINBackPressure = srv;

  srv.clear();
  getILIBackPressure(srv);
  publishSingleTH1F(srv,"ILIBackPressure");
  m_theDFSummary->ILIBackPressure = srv;

  srv.clear();
  getODOBackPressure(srv);
  publishSingleTH1F(srv,"ODOBackPressure");
  m_theDFSummary->ODOBackPressure = srv;

  srv.clear();
  getILOBackPressure(srv);
  publishSingleTH1F(srv,"ILOBackPressure");
  m_theDFSummary->ILOBackPressure = srv;

  srv.clear();
  getInputGlobalFreezeCount(srv);
  publishSingleTH1F(srv,"InputGlobalFreezeCount");
  m_theDFSummary->InputGlobalFreezeCount = srv;

  srv.clear();
  getSLINKLDCFreezeCount(srv);
  publishSingleTH1F(srv,"SLINKLDCFreezeCount");
  m_theDFSummary->SLINKLDCFreezeCount = srv;
  
  srv.clear();
  getFMCOUTtoIMFreezeCount(srv);
  publishSingleTH1F(srv,"FMCOUTtoIMFreezeCount");
  m_theDFSummary->FMCOUTtoIMFreezeCount = srv;

  srv.clear();
  getIDOClustersPerEvt(srv);
  publishSingleTH1F(srv,"IDOClustersPerEvt");
  m_theDFSummary->IDOClustersPerEvt = srv;

  srv.clear();
  getILIClustersPerEvt(srv);
  publishSingleTH1F(srv,"ILIClustersPerEvt");
  m_theDFSummary->ILIClustersPerEvt = srv;
  
  srv.clear();
  getODOClustersPerEvt(srv);
  publishSingleTH1F(srv,"ODOClustersPerEvt");
  m_theDFSummary->ODOClustersPerEvt = srv;

  srv.clear();
  getILOClustersPerEvt(srv);
  publishSingleTH1F(srv,"ILOClustersPerEvt");
  m_theDFSummary->ILOClustersPerEvt = srv;

  srv.clear();
  getB0F2B0FoverB0F2E0F(srv);
  publishSingleTH1F(srv,"B0F2B0FoverB0F2E0F");
  m_theDFSummary->B0F2B0FoverB0F2E0F = srv;

}

