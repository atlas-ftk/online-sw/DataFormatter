#include "DataFormatter/DataFormatterHistHandler.h"

using namespace daq::ftk;


// Called in initialize
DataFormatterHistHandler::DataFormatterHistHandler( const std::string& name,
                                                    std::shared_ptr<OHRootProvider> ohProvider,
                                                    std::shared_ptr<FtkDataFormatterApi> dfApi )
  : m_name( name ),
    m_ohProvider( ohProvider ),
    m_dfApi( dfApi )
{

  // initialize the histograms
   const char *lane_labels[ NUMBER_OF_DF_LANES ] = { "", "", "", "", "", "", "", "", "", "", "", "", "",
                                          "", "", "", "", "", "", "", "", "", "", "", "", "",
                                          "", "", "", "", "", "", "", "", "", "", //ch0-35
                                          "IM00 - IBL",
                                          "IM01",
                                          "IM02 - Pix",
                                          "IM03",
                                          "IM04 - Pix",
                                          "IM05 - SCT",
                                          "IM06 - Pix",
                                          "IM07 - SCT",
                                          "IM08 - IBL",
                                          "IM09",
                                          "IM10",
                                          "IM11",
                                          "IM12",
                                          "IM13 - SCT",
                                          "IM14",
                                          "IM15 - SCT" };

  ERS_LOG( "Initializing monitoring histograms." );

  // Construct the histograms
  // spy buffer histograms
  m_hlast_lvl1id = std::unique_ptr<TH1F>(new TH1F("hlast_lvl1id", "Last LVL1ID in DF spy buffer", NUMBER_OF_DF_LANES, -0.5, 51.5) );
  m_hlast_lvl1id->SetXTitle( "channel" );
  m_hlast_lvl1id->SetYTitle( "LVL1ID" );
  m_hnfragments = std::unique_ptr<TH2F>(new TH2F("hnfragments", "Fragments per spy buffer", NUMBER_OF_DF_LANES, -0.5, 51.5, 130, -0.5, 129.5) );
  m_hnfragments->SetXTitle( "channel" );
  m_hnfragments->SetYTitle( "N_{fragments}" );
  m_hnmodules = std::unique_ptr<TH2F>(new TH2F("hnmodules", "Number of Modules per fragment", NUMBER_OF_DF_LANES, -0.5, 51.5, 100, -0.5, 99.5) );
  m_hnmodules->SetXTitle( "channel" );
  m_hnmodules->SetYTitle( "N_{modules} per fragment" );
  m_hclusterpermodule = std::unique_ptr<TH2F>(new TH2F("hclusterpermodule", "Number of clusters per module", NUMBER_OF_DF_LANES, -0.5, 51.5, 100, -0.5, 99.5) );
  m_hclusterpermodule->SetXTitle( "channel" );
  m_hclusterpermodule->SetYTitle( "N_{clusters} per module" );
  m_hpixel_row_width = std::unique_ptr<TH2F>(new TH2F("hpixel_row_width", "Pixel cluster row width", NUMBER_OF_DF_LANES, -0.5, 51.5, 10, -0.5, 9.5) );
  m_hpixel_row_width->SetXTitle( "channel" );
  m_hpixel_row_width->SetYTitle( "cluster row width" );
  m_hpixel_column_width = std::unique_ptr<TH2F>(new TH2F("hpixel_column_width", "Pixel cluster column width", NUMBER_OF_DF_LANES, -0.5, 51.5, 10, -0.5, 9.5) );
  m_hpixel_column_width->SetXTitle( "channel" );
  m_hpixel_column_width->SetYTitle( "cluster column width" );
  m_hsctcluster_hit = std::unique_ptr<TH2F>(new TH2F("hsctcluster_hit", "SCT Cluster coords", NUMBER_OF_DF_LANES, -0.5, 51.5, 400, 0.0, 2000.0) );
  m_hsctcluster_hit->SetXTitle( "channel" );
  m_hsctcluster_hit->SetYTitle( "cluster coordinate" );
  m_hsct_width = std::unique_ptr<TH2F>(new TH2F("hsct_width", "SCT cluster width", NUMBER_OF_DF_LANES, -0.5, 51.5, 10, -0.5, 9.5) );
  m_hsct_width->SetXTitle( "channel" );
  m_hsct_width->SetYTitle( "cluster width" );

  // other monitoring histograms
  m_htimedout_lanes = std::unique_ptr<TH2F>(new TH2F( "htimedout_links", "IM lane status", NUMBER_OF_DF_INPUT_LANES, -0.5, 15.5, 3, -0.5, 2.5  ));
  m_htimedout_lanes->SetXTitle( "IM lane" );
  m_htimedout_lanes->GetYaxis()->SetBinLabel( 1, "not in run" );
  m_htimedout_lanes->GetYaxis()->SetBinLabel( 2, "active" );
  m_htimedout_lanes->GetYaxis()->SetBinLabel( 3, "timed out" );

  m_htimedout_lanes_flat = std::unique_ptr<TH1F>(new TH1F( "m_htimedout_lanes_flat", "IM lane status", NUMBER_OF_DF_INPUT_LANES, -0.5, 15.5 ) );
  m_htimedout_lanes_flat->SetXTitle( "IM lane" );

  m_hpacketErrorLanes = std::unique_ptr<TH1F>(new TH1F( "m_hpacketErrorLanes", "Input packet error status", NUMBER_OF_DF_INPUT_LANES, -0.5, 15.5 ) );
  m_hpacketErrorLanes->SetXTitle( "IM lane" );

  m_hpacket8thWordErrorLanes = std::unique_ptr<TH1F>(new TH1F( "m_hpacket8thWordErrorLanes", "Input packet 8th word error status", NUMBER_OF_DF_INPUT_LANES, -0.5, 15.5 ) );
  m_hpacket8thWordErrorLanes->SetXTitle( "IM lane" );

//  m_hinputLaneResyncCount = std::unique_ptr<TH1F>(new TH1F( "m_hinputLaneResyncCount", "Resync count for input lanes", NUMBER_OF_DF_INPUT_LANES, -0.5, 15.5 ) );
//  m_hinputLaneResyncCount->SetXTitle( "IM lane" );

  m_hslink_total_evts_given_up = std::unique_ptr<TH1F>(new TH1F( "hslink_total_evts_given_up", "Events timed out at output by channel", NUMBER_OF_DF_LANES, -0.5, 51.5 ) );
  m_hslink_total_evts_given_up->SetXTitle( "channel" );
  m_hslink_total_evts_given_up->SetYTitle( "events timed out at output" );


  for( uint32_t ichannel=0; ichannel<NUMBER_OF_DF_LANES; ichannel++ ) {
    m_hlast_lvl1id       ->GetXaxis()->SetBinLabel( ichannel+1, lane_labels[ichannel] );
    m_hnfragments        ->GetXaxis()->SetBinLabel( ichannel+1, lane_labels[ichannel] );
    m_hnmodules          ->GetXaxis()->SetBinLabel( ichannel+1, lane_labels[ichannel] );
    m_hclusterpermodule  ->GetXaxis()->SetBinLabel( ichannel+1, lane_labels[ichannel] );
    m_hpixel_row_width   ->GetXaxis()->SetBinLabel( ichannel+1, lane_labels[ichannel] );
    m_hpixel_column_width->GetXaxis()->SetBinLabel( ichannel+1, lane_labels[ichannel] );
    m_hsctcluster_hit    ->GetXaxis()->SetBinLabel( ichannel+1, lane_labels[ichannel] );
    m_hsct_width         ->GetXaxis()->SetBinLabel( ichannel+1, lane_labels[ichannel] );
    m_hslink_total_evts_given_up->GetXaxis()->SetBinLabel( ichannel+1, lane_labels[ichannel] );

    auto hnfragments = std::unique_ptr<TH1F>(new TH1F(Form("hnfragments_%d",ichannel), Form("Fragments per spy buffer lane = %d. %s", ichannel, lane_labels[ichannel]),30,-0.5,29.5) );
    hnfragments->SetXTitle( "N_{fragments}" );
    m_vec_hnfragments.push_back(std::move( hnfragments ));

    auto hnmodules = std::unique_ptr<TH1F>(new TH1F( Form("hnmodules_%i", ichannel), Form("Number of Modules per fragment lane = %i. %s", ichannel, lane_labels[ichannel]), 100, -0.5, 99.5) );
    hnmodules->SetXTitle( "N_{modules} per fragment" );
    m_vec_hnmodules.push_back(std::move(hnmodules));

    auto hclusterpermodule = std::unique_ptr<TH1F>(new TH1F( Form("hclusterpermodule_%i", ichannel), Form("Number of clusters per module lane = %i. %s", ichannel, lane_labels[ichannel]), 100, -0.5, 99.5) );
    hclusterpermodule->SetXTitle( "N_{clusters} per module" );
    m_vec_hclusterpermodule.push_back(std::move(hclusterpermodule));

    auto hpixelcluster_all = std::unique_ptr<TH2F>(new TH2F( Form("hpixelcluster_all_%i", ichannel), Form("Pixel clusters lane = %i. %s", ichannel, lane_labels[ichannel]), 12, -0.5, 4000.5, 12, -0.5, 4000.5 ) );
    hpixelcluster_all->SetXTitle( "cluster column" );
    hpixelcluster_all->SetYTitle( "cluster row" );
    m_vec_hpixelcluster_all.push_back(std::move(hpixelcluster_all));

    auto hpixelcluster_split = std::unique_ptr<TH2F>(new TH2F( Form("hpixelcluster_split_%i", ichannel), Form("Pixel split clusters lane = %i. %s", ichannel, lane_labels[ichannel]), 12, -0.5, 4000.5, 12, -0.5, 4000.5 ) );
    hpixelcluster_split->SetXTitle( "cluster column" );
    hpixelcluster_split->SetYTitle( "cluster row" );
    m_vec_hpixelcluster_split.push_back(std::move(hpixelcluster_split));

    auto hpixel_row_width = std::unique_ptr<TH1F>(new TH1F( Form("hpixel_row_width_%i", ichannel), Form("Pixel cluster row width lane = %i. %s", ichannel, lane_labels[ichannel]), 10, -0.5, 9.5 ) );
    hpixel_row_width->SetXTitle( "cluster row width" );
    m_vec_hpixel_row_width.push_back(std::move(hpixel_row_width));

    auto hpixel_column_width = std::unique_ptr<TH1F>(new TH1F( Form("hpixel_column_width_%i", ichannel), Form("Pixel cluster column width lane = %i. %s", ichannel, lane_labels[ichannel]), 10, -0.5, 9.5 ) );
    hpixel_column_width->SetXTitle( "cluster column width" );
    m_vec_hpixel_column_width.push_back(std::move(hpixel_column_width));

    auto hsctcluster_hit = std::unique_ptr<TH1F>(new TH1F( Form("hsctcluster_hit_%i", ichannel), Form("SCT Clusters lane = %i. %s", ichannel, lane_labels[ichannel]), 400, 0.0, 2000) );
    hsctcluster_hit->SetXTitle( "cluster coordinate" );
    m_vec_hsctcluster_hit.push_back(std::move(hsctcluster_hit));

    auto hsct_width = std::unique_ptr<TH1F>(new TH1F( Form("hsct_width_%i", ichannel), Form("SCT cluster width lane = %i. %s", ichannel, lane_labels[ichannel]), 10, -0.5, 9.5 ) );
    hsct_width->SetXTitle( "cluster width" );
    m_vec_hsct_width.push_back(std::move(hsct_width));
  } 

  ERS_LOG( "Done initializing monitoring histograms." );

}

// called by ReadoutModule destructor
DataFormatterHistHandler::~DataFormatterHistHandler() noexcept
{
  // no need to explicitly delete anything... smart points...
}

// called in configure
void DataFormatterHistHandler::configureHists()
{
  uint32_t channel_mask = 0x0;
  m_dfApi->single_access_read("reg.enable_fmc_lanes_mask", channel_mask);
  m_input_active = channel_mask;

  m_isactive.clear();
  for ( uint32_t i=0; i<NUMBER_OF_DF_INPUT_LANES; ++i ) {
    if ( channel_mask & (0x1<<i) ) m_isactive.push_back( true );
    else m_isactive.push_back( false );
  }
}

// called in prepareForRun
void DataFormatterHistHandler::resetHists()
{
  ERS_LOG( "Resetting monitoring histograms." );

  m_hlast_lvl1id->Reset();
  m_hnfragments->Reset();
  m_hnmodules->Reset();
  m_hclusterpermodule->Reset();
  m_hpixel_row_width->Reset();
  m_hpixel_column_width->Reset();
  m_hsctcluster_hit->Reset();
  m_hsct_width->Reset();
  m_hslink_total_evts_given_up->Reset();

  for(uint32_t ichannel=0; ichannel<NUMBER_OF_DF_LANES; ichannel++){
    m_vec_hnfragments.at(ichannel)->Reset();
    m_vec_hnmodules.at(ichannel)->Reset();
    m_vec_hclusterpermodule.at(ichannel)->Reset();
    m_vec_hpixelcluster_all.at(ichannel)->Reset();
    m_vec_hpixelcluster_split.at(ichannel)->Reset();
    m_vec_hpixel_row_width.at(ichannel)->Reset();
    m_vec_hpixel_column_width.at(ichannel)->Reset();
    m_vec_hsctcluster_hit.at(ichannel)->Reset();
    m_vec_hsct_width.at(ichannel)->Reset();
  }

  m_htimedout_lanes->Reset();
  m_htimedout_lanes_flat->Reset();
  m_hpacketErrorLanes->Reset();
  m_hpacket8thWordErrorLanes->Reset();
//  m_hinputLaneResyncCount->Reset();

  for (uint32_t i=0; i<NUMBER_OF_DF_INPUT_LANES; ++i) {
    if ( m_input_active & (0x1<<i) ) {
      m_htimedout_lanes->SetBinContent( i+1, 2, 1.0 );
      m_htimedout_lanes_flat->SetBinContent( i+1, 0.0 );
      m_hpacketErrorLanes->SetBinContent( i+1, 0.0 );
      m_hpacket8thWordErrorLanes->SetBinContent( i+1, 0.0 );
//      m_hinputLaneResyncCount->SetBinContent( i+1, 0.0 );
    } else {
      m_htimedout_lanes->SetBinContent( i+1, 1, 1.0 );
      m_htimedout_lanes_flat->SetBinContent( i+1, -1.0 );
      m_hpacketErrorLanes->SetBinContent( i+1, -1.0 );
      m_hpacket8thWordErrorLanes->SetBinContent( i+1, -1.0 );
//      m_hinputLaneResyncCount->SetBinContent( i+1, -1.0 );
    }
  }
}

// called in publish
void DataFormatterHistHandler::publishHists()
{
  ERS_LOG( "Update monitoring histograms." );

  uint32_t timedOut = 0x0, packetErrorLanes = 0x0, packet8thWordErrorLanes = 0x0;
//  std::vector<uint32_t> resyncCounts;

  m_dfApi->getTimedOutLinks( timedOut );
  m_dfApi->getInputPacketErrorLinks( packetErrorLanes, packet8thWordErrorLanes );
//  m_dfApi->getLaneResyncCounts( resyncCounts );

  for (uint32_t i=0; i<NUMBER_OF_DF_INPUT_LANES; ++i) {
    if ( !(m_input_active & (0x1<<i)) ) continue;

    if ( timedOut & (0x1<<i) ) {
      m_htimedout_lanes->SetBinContent( i+1, 3, 1.0 );
      m_htimedout_lanes->SetBinContent( i+1, 2, 0.0 );
      m_htimedout_lanes_flat->SetBinContent( i+1, 1.0 );
    } else {
      m_htimedout_lanes->SetBinContent( i+1, 3, 0.0 );
      m_htimedout_lanes->SetBinContent( i+1, 2, 1.0 );
      m_htimedout_lanes_flat->SetBinContent( i+1, 0.0 );
    }

    if ( packetErrorLanes & (0x1<<i) ) {
      m_hpacketErrorLanes->SetBinContent( i+1, 1.0 );
    } else {
      m_hpacketErrorLanes->SetBinContent( i+1, 0.0 );
    }
    if ( packet8thWordErrorLanes & (0x1<<i) ) {
      m_hpacket8thWordErrorLanes->SetBinContent( i+1, 1.0 );
    } else {
      m_hpacket8thWordErrorLanes->SetBinContent( i+1, 0.0 );
    }

//    if ( resyncCounts.size() < i+1 ) continue;
//    m_hinputLaneResyncCount->SetBinContent( i+1, resyncCounts[i] );
  }

  for ( uint32_t i=0; i<NUMBER_OF_DF_LANES; ++i ) {
    uint32_t nEventsGivenUp = 0;
    try {
      m_dfApi->getTotalEventsGivenUp( i, nEventsGivenUp );
      m_hslink_total_evts_given_up->SetBinContent( i+1, nEventsGivenUp );
    } catch ( std::exception ex ) {
      daq::ftk::ftkException e(ERS_HERE, m_name, ex.what());
      ers::warning( e );
    }
  }

  ERS_LOG( "Publish monitoring histograms." );

  if ( m_ohProvider ) {
    try {
      m_ohProvider->publish( *m_htimedout_lanes, m_htimedout_lanes->GetName(), true );
      m_ohProvider->publish( *m_htimedout_lanes_flat, m_htimedout_lanes_flat->GetName(), true );
      m_ohProvider->publish( *m_hpacketErrorLanes, m_hpacketErrorLanes->GetName(), true );
      m_ohProvider->publish( *m_hpacket8thWordErrorLanes, m_hpacket8thWordErrorLanes->GetName(), true );
//      m_ohProvider->publish( *m_hinputLaneResyncCount, m_hinputLaneResyncCount->GetName(), true );
      m_ohProvider->publish( *m_hslink_total_evts_given_up, m_hslink_total_evts_given_up->GetName(), true );
    } catch ( daq::oh::Exception & ex ) {
      daq::ftk::OHException e(ERS_HERE, "", ex);
      ers::warning(e);
    }
  }

  ERS_LOG( "Finished publishing monitoring histograms." );
}

// called in publishFullStats
void DataFormatterHistHandler::publishFullStatsHists( 
                          std::vector<std::vector<EventFragmentHitClusters*> >& vec_DF_fragments,
                          const std::map<std::pair<uint32_t,uint32_t>, uint32_t>& lane_idx2mod )
{
  ERS_LOG( "Update monitoring histograms (full stats)." );

  std::vector<uint32_t> vec_moduleIDs_sb;

  for ( uint32_t ichannel=0; ichannel<NUMBER_OF_DF_LANES; ++ichannel ) {
    uint32_t nfragments = vec_DF_fragments.at(ichannel).size();

    m_hnfragments->Fill(ichannel, nfragments);
    //ERS_LOG("Start Loop over " << nfragments << " fragments to fill hisograms lane = " << ichannel);
    uint32_t tmp_last_lvl1id = 0X0;

    // loop over eventfragments
    for(unsigned int ifr=0; ifr<nfragments; ifr++) {
      daq::ftk::EventFragmentHitClusters* eFragment = vec_DF_fragments.at(ichannel).at(ifr);

      // last seen LVL1ID
      if(tmp_last_lvl1id < eFragment->getL1ID())
        tmp_last_lvl1id = eFragment->getL1ID();
      m_hlast_lvl1id->SetBinContent(ichannel+1, tmp_last_lvl1id);

      uint32_t nmodules =  eFragment->getNModules();
      m_vec_hnmodules.at(ichannel)            ->Fill(            nmodules);
      m_hnmodules                    ->Fill(ichannel,     nmodules);

      vec_moduleIDs_sb.clear();
      // loop over modules
      for(unsigned int imod=0; imod<nmodules; imod++) {
        ModuleHitClusters* module = eFragment->getModule(imod);

        uint32_t ncluster = module->getNClusters();
        m_vec_hclusterpermodule.at(ichannel)        ->Fill(            ncluster);
        m_hclusterpermodule                ->Fill(ichannel,     ncluster);
        vec_moduleIDs_sb.push_back(module->getModuleNumber());
        // loop over clusters
        for(unsigned int icluster=0; icluster<ncluster; icluster++) {
          if(module->getModuleType()) {
            // SCT cluster
            SCTCluster* sctCluster = module->getSctCluster(icluster);
            // hit 
            m_vec_hsctcluster_hit.at(ichannel)->Fill(          sctCluster->hit_coord());
            m_hsctcluster_hit                 ->Fill(ichannel, sctCluster->hit_coord());
            m_vec_hsct_width.at(ichannel)     ->Fill(          sctCluster->hit_width());
            m_hsct_width                      ->Fill(ichannel, sctCluster->hit_width());
          } else {
            // Pixel cluster
            PixelCluster* pixCluster = module->getPixelCluster(icluster);
            m_vec_hpixelcluster_all.at(ichannel)    ->Fill(            pixCluster->column_coord(), pixCluster->row_coord());
            m_vec_hpixel_row_width.at(ichannel)     ->Fill(            pixCluster->row_width());
            m_hpixel_row_width                      ->Fill(ichannel,   pixCluster->row_width());
            m_vec_hpixel_column_width.at(ichannel)  ->Fill(            pixCluster->column_width());  
            m_hpixel_column_width                   ->Fill(ichannel,   pixCluster->column_width());
            // split cluster
            if ( pixCluster->split_cluster() ==1 ){
              m_vec_hpixelcluster_split.at(ichannel)->Fill(            pixCluster->column_coord(), pixCluster->row_coord());
          }
          }
        }// end loop over clusters
      }// end loop over modules

      // check module IDs of DF input spy buffer
      if(ichannel < NUMBER_OF_DF_OUTPUT_LANES || ichannel > NUMBER_OF_DF_LANES) continue;
      if(ifr==0 && m_isactive.at(ichannel%NUMBER_OF_DF_OUTPUT_LANES)){// just check one fragment not all
        // ToDo compare with IDs written to DF during configuration
        //if(!compareModuleIDs(vec_moduleIDs_sb, m_dfApi->getModuleIDs(ichannel))){
        //ERS_LOG("Module IDs in DF input spy buffer are not matching DF configuration.");
        //}
        uint32_t ret = 0;

        // check if #modules > #modules configuration
        try {
          ret = m_dfApi->checkModuleIDsSize(vec_moduleIDs_sb, ichannel%NUMBER_OF_DF_OUTPUT_LANES, lane_idx2mod);
        } catch (daq::ftk::ftkException  & ex) {
          daq::ftk::ftkException e(ERS_HERE, m_name, " Invalid number of module IDs DF input spy buffer channel = " + to_string(ichannel%NUMBER_OF_DF_OUTPUT_LANES), ex);
          ers::log(e);
        }

        // check duplicated module IDs
        try {
          ret = m_dfApi->checkModuleIDsDuplicates(vec_moduleIDs_sb, ichannel%NUMBER_OF_DF_OUTPUT_LANES);
        } catch (daq::ftk::ftkException  & ex) {
          daq::ftk::ftkException e(ERS_HERE, m_name, " Duplicated module IDs DF input spy buffer channel = " + to_string(ichannel%NUMBER_OF_DF_OUTPUT_LANES), ex);
          ers::log(e);
        }

        // check consistency of module IDs
        try {
          ret = m_dfApi->checkModuleIDsConsistency(vec_moduleIDs_sb, ichannel%NUMBER_OF_DF_OUTPUT_LANES, lane_idx2mod);
        } catch (daq::ftk::ftkException  & ex) {
          daq::ftk::ftkException e(ERS_HERE, m_name, " Inconsistent module IDs DF input spy buffer channel = " + to_string(ichannel%NUMBER_OF_DF_OUTPUT_LANES), ex);
          ers::log(e);
        }
        ERS_LOG("Return value of module ID check = 0X" << hex << ret <<" (0 is good)");
      }

      // need to delete the fragment (allocated with new in ftkcommon/EventFragmentHitClusters.h)
      delete eFragment;
    }// end loop over fragments
    // clear the fragment vector (they should have been deleted)
    vec_DF_fragments.at(ichannel).clear();
  }// end channel loop
  ERS_LOG("Done filling histograms (full stats)");

  //++++++++++++++++++++++
  // 3 Publish the histograms
  //++++++++++++++++++++++
  ERS_LOG("Publish histograms (full stats)");
  if ( m_ohProvider )
  try {
    // spy buffer histograms
    m_ohProvider->publish( *m_hlast_lvl1id,        m_hlast_lvl1id->GetName(),         true );
    m_ohProvider->publish( *m_hnfragments,        m_hnfragments->GetName(),         true );
    m_ohProvider->publish( *m_hnmodules,        m_hnmodules->GetName(),         true );
    m_ohProvider->publish( *m_hclusterpermodule,    m_hclusterpermodule->GetName(),     true );
    m_ohProvider->publish( *m_hpixel_row_width,        m_hpixel_row_width->GetName(),         true );
    m_ohProvider->publish( *m_hpixel_column_width,    m_hpixel_column_width->GetName(),     true );
    m_ohProvider->publish( *m_hsctcluster_hit,        m_hsctcluster_hit->GetName(),         true );
    m_ohProvider->publish( *m_hsct_width,        m_hsct_width->GetName(),         true );

    for(uint32_t ichannel=0; ichannel<NUMBER_OF_DF_LANES; ichannel++){
      if(ichannel < NUMBER_OF_DF_OUTPUT_LANES || ichannel > NUMBER_OF_DF_LANES) continue;
      if(m_isactive.at(ichannel%NUMBER_OF_DF_OUTPUT_LANES)) {
      m_ohProvider->publish( *m_vec_hnfragments.at(ichannel),     m_vec_hnfragments.at(ichannel)->GetName(),         true );
      m_ohProvider->publish( *m_vec_hnmodules.at(ichannel),         m_vec_hnmodules.at(ichannel)->GetName(),         true );
      m_ohProvider->publish( *m_vec_hclusterpermodule.at(ichannel),     m_vec_hclusterpermodule.at(ichannel)->GetName(),     true );
      //if(ichannel%2){// SCT
      m_ohProvider->publish( *m_vec_hsctcluster_hit.at(ichannel),     m_vec_hsctcluster_hit.at(ichannel)->GetName(),         true );
      m_ohProvider->publish( *m_vec_hsct_width.at(ichannel),         m_vec_hsct_width.at(ichannel)->GetName(),         true );
      //}
      //else{          // Pixel
      m_ohProvider->publish( *m_vec_hpixelcluster_all.at(ichannel),     m_vec_hpixelcluster_all.at(ichannel)->GetName(),     true );
      m_ohProvider->publish( *m_vec_hpixelcluster_split.at(ichannel), m_vec_hpixelcluster_split.at(ichannel)->GetName(),     true );
      m_ohProvider->publish( *m_vec_hpixel_row_width.at(ichannel),     m_vec_hpixel_row_width.at(ichannel)->GetName(),     true );
      m_ohProvider->publish( *m_vec_hpixel_column_width.at(ichannel), m_vec_hpixel_column_width.at(ichannel)->GetName(),     true );
      //}
      }
    }
  } catch ( daq::oh::Exception & ex) {
    daq::ftk::OHException e(ERS_HERE, m_name, ex);
    ers::warning(e);
  }
  ERS_LOG("Done publishing histograms (full stats)");

}


