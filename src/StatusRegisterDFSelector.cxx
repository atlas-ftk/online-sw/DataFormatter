#include <DataFormatter/StatusRegisterDFSelector.h>
#include "ftkcommon/exceptions.h"
#include "ftkcommon/Utils.h"

using namespace daq::ftk;

StatusRegisterDFSelector::StatusRegisterDFSelector(std::shared_ptr<FtkDataFormatterApi> dfapi, const srType& type)
{
  m_dfapi	  = dfapi;
  m_type    = type;
  m_access	= srAccess::IPBus_selector;
}

StatusRegisterDFSelector::~StatusRegisterDFSelector()
{
  m_write_nodes.clear();
  m_write_values.clear();
}

void 
StatusRegisterDFSelector::readout()
{
  try {
    m_dfapi->read_using_selector(	m_write_nodes, m_write_values,
					m_node, m_value);
  } catch (daq::ftk::IPBusRead ex) {
    m_value = srNoneValue;
    daq::ftk::IPBusIssue ex2(ERS_HERE, m_name, " Failed to read out a DF status register",ex);
    ers::warning(ex2);
  }
}
