// Functions used for spy buffer access using DFApi

#include "DataFormatter/DataFormatterConstants.h"
#include "DataFormatter/DFSpyBuffer.h"
// for bit parsing
#include "ftkcommon/Utils.h" 
#include "ftkcommon/exceptions.h"
#include "ftkcommon/core.h"

#include <sstream>
#include <fstream>
#include <iostream>
#include <iomanip>
#include <algorithm>



namespace daq { 
  namespace ftk {
  
    DFSpyBuffer::DFSpyBuffer(uint32_t lane_id, 
			     uint32_t spybuffer_length,
			     std::shared_ptr<FtkDataFormatterApi> df_api):
      m_lane_id(lane_id),
      m_spybuffer_length(spybuffer_length),
      m_df_api(df_api),
      m_spyOverflow(0x0),
      m_spyFreeze(0),
      m_spyPointer(0),
      m_spyDimension(NUMBER_OF_WORDS_IN_DF_SPYBUFFERS),
      m_register(1024)
    {
      //      ERS_LOG("Initialize spy buffer");
      m_buffer.clear();
      m_buffer.reserve(m_spybuffer_length);
      DFSpyBuffer::parse_spybuffer_status();
    }
    
    DFSpyBuffer::~DFSpyBuffer(){}

    void DFSpyBuffer::parse_spybuffer_status()
    {
      m_spyOverflow = 1;
      m_spyPointer = 0;
      m_spyFreeze = 1;
    }


    int DFSpyBuffer::read_spybuffer_blocktransfer(bool reset, uint32_t read_length, bool is_print )
    {
      int rc = 0;
      const uint32_t readsize = 4;
      std::vector<uint32_t> raw_data(readsize);

      if ( read_length == 0 ) read_length = m_spybuffer_length;

      if(reset){
        m_buffer.clear();
        m_buffer.reserve(m_spybuffer_length);
      }

      DFSpyBuffer::parse_spybuffer_status();

      m_df_api->single_access_write("reg.spy_laneselector", m_lane_id);
  
      for (uint32_t iAddr=0; iAddr<read_length/4; iAddr++) {
        uint32_t addr = 4*iAddr;
        //	if (iAddr%4 != 0) continue;
        m_df_api->single_access_write("reg.spy_readaddr", addr);
        try {
          m_df_api->single_access_block_read("reg.spy_readout",raw_data, readsize);
        } catch( daq::ftk::IPBusRead ex)  {
          daq::ftk::IPBusRead ex2(ERS_HERE," Error while trying to do block read", ex);
          ers::warning(ex2);  //or throw e;  
        }
 
        m_buffer.push_back( raw_data[2]);
        m_buffer.push_back( raw_data[3]);
        m_buffer.push_back( raw_data[0]);
        m_buffer.push_back( raw_data[1]);
        // if( is_print ){
        //   ERS_LOG( " raw_data " << raw_data[2] << " : " << raw_data[3] << " : " << raw_data[0] << " : " << raw_data[1] );
        // }
      }

      return rc;
    }
    
    
    int DFSpyBuffer::read_spybuffer(bool reset, uint32_t read_length)
    {
      int rc = 0;

      if ( read_length == 0 ) read_length = m_spybuffer_length;

      if(reset){
        m_buffer.clear();
        m_buffer.reserve(m_spybuffer_length);
      }

      DFSpyBuffer::parse_spybuffer_status();
      
      m_df_api->single_access_write("reg.spy_laneselector", m_lane_id);
  
      for (uint32_t iAddr=0; iAddr<read_length; iAddr++) {
        m_df_api->single_access_write("reg.spy_readaddr", iAddr);
        m_register = 0x0;
        try {
          m_df_api->single_access_read("reg.spy_readout", m_register);
        } catch (char *) {
          rc = 1;
        }
        m_buffer.push_back(m_register);
      }
      
      return rc;
    }



    
    int DFSpyBuffer::read_individual_spybuffer()
    {
      int rc = 0;
      ERS_LOG("spy_freeze()");
      m_df_api->spy_read_enable();
      m_df_api->spy_freeze();
      rc = DFSpyBuffer::read_spybuffer(true);
      ERS_LOG("spy_restart()");
      m_df_api->spy_restart();
      m_df_api->spy_read_disable();
      return rc;
    
    }

    int DFSpyBuffer::read_individual_spybuffer(const uint32_t lane_id )
    {
      int rc = 0;
      m_lane_id = lane_id;
      rc = DFSpyBuffer::read_individual_spybuffer();
      return rc;
    }
    
    int DFSpyBuffer::read_individual_lane()
    {
      int rc = 0;
      m_buffer.clear();
      rc = m_df_api->spy_dump_individual_lane(m_lane_id, 
						    m_buffer);
      return rc;
    
    }    


    // read example Spy Buffer dump from file
    int DFSpyBuffer::read_from_file(std::string filename)
    {
      m_buffer.clear();
      int length = 0;
      std::string line;
      std::ifstream myfile(filename.c_str());
      if (myfile.is_open()) {

        while( std::getline( myfile, line ) ) {
//        std::cout << line <<'\n';
          uint32_t value =std::stoul(line, nullptr, 16);
          m_buffer.push_back(value);
          length++;
        }
        myfile.close();
      }
      else {
        ERS_LOG( "Unable to open file " << filename); 
      }

      return length;
    }


  } // namespace ftk
} // namespace daq


