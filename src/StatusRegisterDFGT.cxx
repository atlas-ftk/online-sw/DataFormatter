#include <DataFormatter/StatusRegisterDFGT.h>
#include "ftkcommon/exceptions.h"
#include "ftkcommon/Utils.h"

using namespace daq::ftk;

StatusRegisterDFGT::StatusRegisterDFGT(std::shared_ptr<FtkDataFormatterApi> dfapi, 
			const srType& type, 
			const uint32_t& laneSelector,
                        const std::string& reg)
: StatusRegisterDFGT::StatusRegisterDFSelector(dfapi, type)
{
  initialize_selector(laneSelector, reg);
}

// ====================================================
StatusRegisterDFGT::StatusRegisterDFGT(std::shared_ptr<FtkDataFormatterApi> dfapi, 
			const srType& type, 
			const uint32_t& laneSelector,
                        const uint32_t& addr)
: StatusRegisterDFGT::StatusRegisterDFSelector(dfapi, type)
{
  std::string reg;

  reg = dfapi->getRegisterNameFromAddress(addr);

  initialize_selector(laneSelector, reg);
}


// ====================================================
void
StatusRegisterDFGT::initialize_selector(const uint32_t& laneSelector, const std::string& reg)
{
  m_write_nodes.push_back("reg.gt_link_monitor_configure_laneselector");

  m_write_values.push_back(laneSelector);

  m_node	  = reg;
}

