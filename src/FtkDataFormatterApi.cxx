#include <DataFormatter/FtkDataFormatterApi.h>
#include "ftkcommon/exceptions.h"
#include "ftkcommon/core.h"
#include "ftkcommon/Utils.h"

#include "CoolApplication/DatabaseSvcFactory.h"

#include "CoolKernel/IDatabaseSvc.h"
#include "CoolKernel/IDatabase.h"
#include "CoolKernel/IFolder.h"
#include "CoolKernel/IObject.h"
#include "CoolKernel/IObjectIterator.h"
#include "CoolKernel/IRecord.h"
#include "CoolKernel/IRecordIterator.h"
#include "CoolKernel/IRecordSpecification.h"
#include "CoolKernel/Record.h"
#include "CoolKernel/pointers.h"
#include "CoolKernel/Exception.h"
#include "CoolKernel/FolderSpecification.h"
#include "CoolKernel/PayloadMode.h"

#include "DataFormatter/FtkCoolSchema.h"

#include <fstream>

using namespace daq::ftk;

int msleep(unsigned long milisec)   
{   
  struct timespec req={0};   
  time_t sec=(int)(milisec/1000);   
  milisec=milisec-(sec*1000);   
  req.tv_sec=sec;   
  req.tv_nsec=milisec*1000000L;   
  while(nanosleep(&req,&req)==-1)   
    continue;   
  return 1;   
}   

// ====================================================
FtkDataFormatterApi::FtkDataFormatterApi(const std::string& connectionFile, 
                                         const std::string& deviceId,
                                         const std::string& name)
  : m_name( name ),
    m_ipbus( new FtkIPBusApi(connectionFile, deviceId ) ),
    m_im( new FtkIMApi( m_ipbus, name ) )
{
//  ERS_LOG("FtkDataFormatterApi::constructor: Entered");
}
// ====================================================
FtkDataFormatterApi::FtkDataFormatterApi(const std::string& connectionFile, 
                                         const std::string& deviceId,
                                         const std::string& name,
                                         const RegisterNodeMap& registerMap)
  : m_name( name ),
    m_ipbus( new FtkIPBusApi(connectionFile, deviceId, registerMap) ),
    m_im( new FtkIMApi( m_ipbus, name ) )
{
//  ERS_LOG("FtkDataFormatterApi::constructor: Entered");
}

// ====================================================
FtkDataFormatterApi::~FtkDataFormatterApi()
{
}


// ====================================================
/*void 
FtkDataFormatterApi::standardConfigurationWithValidation(const std::string& configurationFile,
                                                         const std::string& transceiverConfigurationFile, 
                                                         uint32_t useAutoDelaySetting)
{




  //copy then clear everything read in from the configuration file
  uint32_t this_board_mask_ido = m_this_board_mask_ido;m_this_board_mask_ido=0;
  uint32_t this_board_mask_ili = m_this_board_mask_ili;m_this_board_mask_ili=0;
  uint32_t enable_fmc_lanes_mask = m_enable_fmc_lanes_mask;m_enable_fmc_lanes_mask=0;
  map2_t fmcin2nummodules = m_fmcin2nummodules;m_fmcin2nummodules.clear();
  map2_t slinkout2nummodules = m_slinkout2nummodules;m_slinkout2nummodules.clear();
  map2_t imfpga2clkinv = m_imfpga2clkinv;m_imfpga2clkinv.clear();
  map2_t imfpga2clkdelay = m_imfpga2clkdelay;m_imfpga2clkdelay.clear();
  map3_t lane_mod2idx = m_lane_mod2idx;m_lane_mod2idx.clear();
  map3_t lane_idx2mod = m_lane_idx2mod;m_lane_idx2mod.clear();
  map2_t pixmod2dst = m_pixmod2dst;m_pixmod2dst.clear();
  map2_t sctmod2dst = m_sctmod2dst;m_sctmod2dst.clear();
  map2_t pixmod2tower = m_pixmod2tower;m_pixmod2tower.clear();
  map2_t sctmod2tower = m_sctmod2tower;m_sctmod2tower.clear();
  map2_t pixmod2ftkplane = m_pixmod2ftkplane;m_pixmod2ftkplane.clear();
  map2_t sctmod2ftkplane = m_sctmod2ftkplane;m_sctmod2ftkplane.clear();
  map2_t centralswlaneid2destinationmask = m_centralswlaneid2destinationmask;m_centralswlaneid2destinationmask.clear();
  uint32_t rxports = m_rxports;m_rxports=0;
  m_fmcin2modid.clear();
  m_module_counter_out.clear();

  m_DFBoardNumberToShelfAndSlotMap.clear();
  m_FTKPlaneTODFOutputMap.clear();
  m_DFBoardNumberToBotTowerMap.clear();
  m_DFBoardNumberToTopTowerMap.clear();
  m_ATCAFabricChToOutputBitPosMap.clear();
  m_DFFiberConnectionMap.clear();
  m_RodIdToFMCInIdMap.clear();
  m_BoardEnableMap.clear();

  //set the config parameters from txt input
  ERS_LOG("FtkDataFormatterApi::standardConfiguration: Start setting parameters");
  setParameters(configurationFile, useAutoDelaySetting);
  ERS_LOG("FtkDataFormatterApi::standardConfiguration: Done setting parameters");

  //check with the copy
  bool checksout = true;
  checksout = checksout && check(this_board_mask_ido, m_this_board_mask_ido,"this_board_mask_ido");
  checksout = checksout && check(this_board_mask_ili, m_this_board_mask_ili,"this_board_mask_ili");
  checksout = checksout && check(enable_fmc_lanes_mask, m_enable_fmc_lanes_mask,"enable_fmc_lanes_mask");
  checksout = checksout && check(fmcin2nummodules, m_fmcin2nummodules,"fmcin2nummodules");
  checksout = checksout && check(slinkout2nummodules, m_slinkout2nummodules,"slinkout2nummodules");
  checksout = checksout && check(imfpga2clkinv, m_imfpga2clkinv,"imfpga2clkinv");
  checksout = checksout && check(imfpga2clkdelay, m_imfpga2clkdelay,"imfpga2clkdelay");
  checksout = checksout && check(lane_mod2idx, m_lane_mod2idx,"lane_mod2idx");
  checksout = checksout && check(lane_idx2mod, m_lane_idx2mod,"lane_idx2mod");
  checksout = checksout && check(pixmod2dst, m_pixmod2dst,"pixmod2dst");
  checksout = checksout && check(sctmod2dst, m_sctmod2dst,"sctmod2dst");
  checksout = checksout && check(pixmod2tower, m_pixmod2tower,"pixmod2tower");
  checksout = checksout && check(sctmod2tower, m_sctmod2tower,"sctmod2tower");
  checksout = checksout && check(pixmod2ftkplane, m_pixmod2ftkplane,"pixmod2ftkplane");
  checksout = checksout && check(sctmod2ftkplane, m_sctmod2ftkplane,"sctmod2ftkplane");
  checksout = checksout && check(centralswlaneid2destinationmask, m_centralswlaneid2destinationmask,"centralswlaneid2destinationmask");
  checksout = checksout && check(rxports, m_rxports,"rxports");
  if (!checksout)
  {
    throw daq::ftk::ftkException(ERS_HERE,m_name, " Invalid configuration");
  }

  //procede with the rest of the configuration
  standardConfiguration(transceiverConfigurationFile, useAutoDelaySetting);
}*/

// public wrappers for IPBusApi functions
// TODO: add mutex locks here
std::string FtkDataFormatterApi::getRegisterNameFromAddress( const uint32_t& addr ) {
  return m_ipbus->getRegisterNameFromAddress( addr );
}
bool FtkDataFormatterApi::single_access_read( const std::string& nodename, uint32_t& read_value ) {
  return m_ipbus->single_access_read( nodename, read_value );
}
bool FtkDataFormatterApi::single_access_block_read( const std::string& nodename,
                                                    std::vector<uint32_t>& read_value,
                                                    uint32_t size ) {
  return m_ipbus->single_access_block_read( nodename, read_value, size );
}
bool FtkDataFormatterApi::read_using_selector( const std::vector<std::string>& in_node, 
                                               const std::vector<uint32_t>& din,
                                               const std::string& out_node,
                                               uint32_t& dout ) {
  return m_ipbus->read_using_selector( in_node, din, out_node, dout );
}
bool FtkDataFormatterApi::read_using_selector( const std::string& in_node, const uint32_t& din,
                                               const std::string& out_node, uint32_t& dout ) {
  return m_ipbus->read_using_selector( in_node, din, out_node, dout );
}
void FtkDataFormatterApi::single_access_write( const std::string& nodename,
                                               const uint32_t& value ) {
  m_ipbus->single_access_write( nodename, value );
}

void FtkDataFormatterApi::setDFFWVersionFromBoard() {
  m_ipbus->single_access_read("reg.firmware_version", m_df_fwVersion );
}

void FtkDataFormatterApi::getEnableFMCLanesMask( uint32_t& enable_fmc_lanes_mask ) {
  if ( m_df_fwVersion >= 0x19000 ) {
    uint32_t temp;
    m_ipbus->single_access_read("reg.input_config_a", temp);
    enable_fmc_lanes_mask = 0xffff & temp;
  } else {
    m_ipbus->single_access_read("reg.enable_fmc_lanes_mask",  enable_fmc_lanes_mask );
  }
}

void FtkDataFormatterApi::getCurrentL1IDInput( std::vector<uint32_t>& vec ) {
  vec.clear();

  for ( uint32_t iFMC=0; iFMC < NUMBER_OF_DF_INPUT_LANES; iFMC++ ) {
    if ( m_df_fwVersion >= 0x19000 ) {
      m_ipbus->single_access_write("reg.input_lane_selector", iFMC);
      uint32_t temp;
      m_ipbus->single_access_read("reg.input_lane_l1id", temp);
      vec.push_back(temp);
    } else {
      uint32_t temp;
      m_ipbus->single_access_read("reg.global_l1id", temp);
      vec.push_back(temp);
    }
  }
}

// set standard configuration registers
bool FtkDataFormatterApi::setConfigRegisters( const uint32_t& maxCyclesWaitedForData,
                                              const uint32_t& b0fTimeoutThreshold,
                                              const uint32_t& skewTimeoutThresholdMax,
                                              const uint32_t& skewTimeoutThresholdMin,
                                              const uint32_t& packetTimeoutThreshold,
                                              const uint32_t& DF_IM_freezeMode,
                                              const uint32_t& DF_enableInputResync,
                                              const uint32_t& DF_unknownModHeaderWord,
                                              const uint32_t& DF_modWordLimit,
                                              const uint32_t& DF_boardId,
                                              const uint32_t& DF_enableFMCLanesMask,
                                              const uint32_t& boardMaskIDO,
                                              const uint32_t& boardMaskILI,
                                              const uint32_t& slinkMode,
                                              const uint32_t& useBlockTransfer ) {
  if ( m_df_fwVersion >= 0x19000 ) {
    try {
      //set max cycles to wait for data from IM
      m_ipbus->single_access_write("reg.max_cycle_waited_for_data_allowed", maxCyclesWaitedForData);
  
      // set IM freeze mode
      m_ipbus->single_access_write("reg.im_freeze_mode", DF_IM_freezeMode);
  
      // form the configuration registers:
      // config_a[32:16] --> lowest 16 bits of unknown mod header word
      // config_a[15:0]  --> enable_fmc_lanes_mask
      // config_b[32:16] --> word limit per module (lower 16 bits)
      // config_b[15:0]  --> board ID
      uint32_t config_a = (( DF_unknownModHeaderWord << 16 ) & 0xffff0000) | (DF_enableFMCLanesMask & 0xffff);
      uint32_t config_b = (( DF_modWordLimit << 16 ) & 0xffff0000) | (DF_boardId & 0xffff);
      m_ipbus->single_access_write("reg.input_config_a", config_a);
      m_ipbus->single_access_write("reg.input_config_b", config_b);
  
      // form the timeout threshold registers:
      // timeout_threshold_a[19:0]  --> skew timeout thresh max
      // timeout_threshold_a[31:20] --> skew timeout thresh min bits [11:0]
      // timeout_threshold_b[7:0]   --> skew timeout thresh min bits [19:12]
      // timeout_threshold_b[27:8]  --> packet timeout thresh
      uint32_t timeout_threshold_a = (skewTimeoutThresholdMax & 0xfffff) | ((skewTimeoutThresholdMin << 20) & 0xfff00000);
      uint32_t timeout_threshold_b = ((skewTimeoutThresholdMin >> 12) & 0xff) | ((packetTimeoutThreshold << 8) & 0xfffff00);
      m_ipbus->single_access_write("reg.timeout_threshold_a", timeout_threshold_a);
      m_ipbus->single_access_write("reg.timeout_threshold_b", timeout_threshold_b);
  
      m_ipbus->single_access_write("reg.this_board_mask_ido",    boardMaskIDO);
      m_ipbus->single_access_write("reg.this_board_mask_ili",    boardMaskILI);
  
      m_im->set_slink_mode( slinkMode );
    } catch ( std::exception ex ) {
      ers::warning( daq::ftk::ftkException( ERS_HERE, m_name, " caught exception while setting configuration registers: " + std::string(ex.what()) ) );
      return false;
    }
    return true;
  }

  try { 
    //set max cycles to wait for data from IM
    m_ipbus->single_access_write("reg.max_cycle_waited_for_data_allowed", maxCyclesWaitedForData);

    // set b0f timeout threshold to drop slow IM lane
    m_ipbus->single_access_write("reg.b0f_timeout_threshold", b0fTimeoutThreshold);

    // set IM freeze mode
    m_ipbus->single_access_write("reg.im_freeze_mode", DF_IM_freezeMode);

    // set the input resync register
    m_ipbus->single_access_write("reg.input_enable_resync", DF_enableInputResync);

    // set the "unknown module" word
    m_ipbus->single_access_write("reg.input_unknown_module_header_word", DF_unknownModHeaderWord);

    // set the word limit per module
    m_ipbus->single_access_write("reg.input_word_limit_per_module", DF_modWordLimit);

    m_im->set_slink_mode( slinkMode );
  } catch ( std::exception ex ) {
    ers::warning( daq::ftk::ftkException( ERS_HERE, m_name, " caught exception while setting configuration registers: " + std::string(ex.what()) ) );
    return false;
  }
  return true;
}

void FtkDataFormatterApi::getGlobalL1ID( uint32_t& l1id ) {
  if ( m_df_fwVersion >= 0x19000 )
    m_ipbus->single_access_read( "reg.input_global_l1id", l1id );
}

void FtkDataFormatterApi::getInputSkew( uint32_t& skew ) {
  if ( m_df_fwVersion >= 0x19000 )
    m_ipbus->single_access_read( "reg.input_skew_counter", skew );
}

void FtkDataFormatterApi::getTimedOutLinks( uint32_t& skewTimedOut, uint32_t& packetTimedOut ) {
  if ( m_df_fwVersion >= 0x19000 ) {
    uint32_t temp = 0x0;
    m_ipbus->single_access_read( "reg.input_timedout_latch", temp );

    skewTimedOut = (temp >> 16) & 0xffff;
    packetTimedOut = temp & 0xffff;
  }
}

void FtkDataFormatterApi::getModHeaderErrors( uint32_t& unknownModHeaderSeen, uint32_t& duplicateModHeaderSeen ) {
  if ( m_df_fwVersion >= 0x19000 ) {
    uint32_t temp = 0x0;
    m_ipbus->single_access_read( "reg.input_mod_header_error", temp );

    unknownModHeaderSeen = (temp >> 16) & 0xffff;
    duplicateModHeaderSeen = temp & 0xffff;
  }
}

void FtkDataFormatterApi::getPacketL1IDErrors( uint32_t& packetError, uint32_t& synchDiscard ) {
  if ( m_df_fwVersion >= 0x19000 ) {
    uint32_t temp = 0x0;
    m_ipbus->single_access_read( "reg.input_packet_l1id_error", temp );

    packetError = (temp >> 16) & 0xffff;
    synchDiscard = temp & 0xffff;
  }
}

void FtkDataFormatterApi::getInputLaneStatus( const uint32_t& lane,
                                              uint32_t& l1id,
                                              uint32_t& errorWord,
                                              uint32_t& numPacketsDiscarded,
                                              uint32_t& numModsHitWordLimit ) {
  if ( m_df_fwVersion >= 0x19000 ) {

    if ( lane >= 16 ) return; // nothing to do
  
    m_ipbus->single_access_write( "reg.input_lane_selector", lane );
  
    m_ipbus->clear_read_register_buffer();
    m_ipbus->add_register_to_read( "reg.input_lane_l1id" );
    m_ipbus->add_register_to_read( "reg.input_lane_errorword" );
    m_ipbus->add_register_to_read( "reg.input_lane_counters" );
    m_ipbus->send_transaction_to_read();
  
    l1id = m_ipbus->get_readback_register_value( "reg.input_lane_l1id" );
    errorWord = m_ipbus->get_readback_register_value( "reg.input_lane_errorword" );
  
    uint32_t temp = m_ipbus->get_readback_register_value( "reg.input_lane_counters" );
    // the counters are down-sampled from 32 to 8 bits:
    // reduced bit 0 --> original bit 3
    //             1 -->              7
    //             2 -->              11 (and so on)
    // Number of packets discarded is bits 8 to 15
    numPacketsDiscarded = (temp & 0x0100) >>  5 | //  8 == 0 --> 3
                          (temp & 0x0200) >>  2 | //  9 == 1 --> 7
                          (temp & 0x0400) <<  1 | // 10 == 2 --> 11
                          (temp & 0x0800) <<  4 | // 11 == 3 --> 15
                          (temp & 0x1000) <<  7 | // 12 == 4 --> 19
                          (temp & 0x2000) << 10 | // 13 == 5 --> 23
                          (temp & 0x4000) << 13 |
                          (temp & 0x8000) << 16;
    // Number of modules hit word limit is bits 0 to 7
    numModsHitWordLimit = (temp & 0x0001) <<  3 | // 0 --> 3
                          (temp & 0x0002) <<  6 | // 1 --> 7
                          (temp & 0x0004) <<  9 | // 2 --> 11
                          (temp & 0x0008) << 12 | // 3 --> 15
                          (temp & 0x0010) << 15 | // 4 --> 19
                          (temp & 0x0020) << 18 | // 5 --> 23
                          (temp & 0x0040) << 21 |
                          (temp & 0x0080) << 24;
  }
}

// Read monitoring registers from the board and print stats to the log
void FtkDataFormatterApi::inputMonitoring( const uint32_t& activeMask, const std::map<uint32_t, uint32_t>& IMChToRobId  ) {
  if ( m_df_fwVersion >= 0x19000 ) {
    uint32_t  globalL1ID = 0x0, inputSkew = 0x0, skewTimedOut = 0x0, packetTimedOut = 0x0,
              unknownModHeaderSeen = 0x0, duplicateModHeaderSeen = 0x0, packetError = 0x0,
              synchDiscard = 0x0;

    getGlobalL1ID( globalL1ID );
    ERS_LOG( "Current global L1ID: 0x" << std::hex<<globalL1ID );

    getInputSkew( inputSkew );
    ERS_LOG( "Current input skew: 0x" << std::hex<<inputSkew );

    getTimedOutLinks( skewTimedOut, packetTimedOut );
    if ( skewTimedOut != 0)
      ERS_LOG( "Links timed out from skew: " << std::hex<<skewTimedOut );
    if ( packetTimedOut != 0)
      ERS_LOG( "Links timed out from PacketHandler: " << std::hex<<packetTimedOut );

    getModHeaderErrors( unknownModHeaderSeen, duplicateModHeaderSeen );
    if ( unknownModHeaderSeen != 0 )
      ERS_LOG( "Links seen unknown module header: " << std::hex<<unknownModHeaderSeen );
    if ( duplicateModHeaderSeen != 0 )
      ERS_LOG( "Links seen duplicate module header: " << std::hex<<duplicateModHeaderSeen );

    getPacketL1IDErrors( packetError, synchDiscard );
    if ( packetError != 0 )
      ERS_LOG( "Links with packet errors seen in PacketHandler: " << std::hex<<packetError );
    if ( synchDiscard != 0 )
      ERS_LOG( "Links with packets discarded by synchronizer: " << std::hex<<synchDiscard );

    for ( uint32_t i=0; i < NUMBER_OF_DF_INPUT_LANES; ++i ) {
      if ( !(activeMask & (0x1<<i)) ) continue;

      uint32_t robId = 0;
      if ( IMChToRobId.find( i ) != IMChToRobId.end() )
        robId = IMChToRobId.at(i);

      uint32_t  laneL1ID = 0x0, laneErrorWord = 0x0, laneNumPacketsDiscarded = 0x0,
                laneNumModsHitWordLimit = 0x0;
      getInputLaneStatus( i, laneL1ID, laneErrorWord, laneNumPacketsDiscarded, laneNumModsHitWordLimit );
      ERS_LOG( "Input lane " << i << ", ROBID 0x" << std::hex<<robId << ", current L1ID: 0x" << std::hex<<laneL1ID );
      ERS_LOG( "Input lane " << i << ", ROBID 0x" << std::hex<<robId << ", current error word: 0x" << std::hex<<laneErrorWord );
      if ( laneNumPacketsDiscarded > 0 )
        ERS_LOG( "Input lane " << i << ", ROBID 0x" << std::hex<<robId << ", number of packets discarded by PacketHandler: " << std::dec<< laneNumPacketsDiscarded );
      if ( laneNumModsHitWordLimit > 0 )
        ERS_LOG( "Input lane " << i << ", ROBID 0x" << std::hex<<robId << ", number of modules hit word limit: " << std::dec<< laneNumModsHitWordLimit );
    }
  } else { // FW version < 0x19000
    static uint32_t m_istimedout = 0x0;

    ERS_LOG( "Checking timed out lanes. Printing monitoring states per lane" );
    uint32_t b0fSkew = 0x0, timedOut = 0x0;
    try {
      getb0fSkew( b0fSkew );
      ERS_LOG( "Current b0f skew: " << b0fSkew );
  
      getTimedOutLinks( timedOut );
  
      for ( uint32_t ichannel=0; ichannel < NUMBER_OF_DF_INPUT_LANES; ++ichannel ) {
  
        if ( !( activeMask & (0x1<<ichannel) ) ) continue;
  
        uint32_t robId = 0;
        if ( IMChToRobId.find( ichannel ) != IMChToRobId.end() )
          robId = IMChToRobId.at(ichannel);

        // check for newly timed out channel
        if ( (0x1<<ichannel) & ~m_istimedout & timedOut ) {
          if ( robId )
            ERS_LOG( "IM lane number " << ichannel << ", ROB ID 0x" <<hex<< robId << ", disabled using b0f-b0f skew " <<dec<< b0fSkew );
          else
            ERS_LOG( "IM lane number " << ichannel << ", disabled using b0f-b0f skew " << b0fSkew );
        }
        // check for a re-enabled channel
        if ( (0x1<<ichannel) & m_istimedout & ~timedOut ) {
          if ( robId )
            ERS_LOG( "IM lane number " << ichannel << ", ROB ID 0x" << hex<<robId << ", re-enabled"<<dec );
          else
            ERS_LOG( "IM lane number " << ichannel << " re-enabled" );
        }
        uint32_t n_b0ftimeouts = 0x0, n_l1idtimeouts = 0x0, n_modulesHitWordLimit = 0x0,
                 n_packetStructureErrors = 0x0, n_packet8thWordErrors = 0x0;
        getInputLaneMonitoringCounts( ichannel, n_b0ftimeouts, n_l1idtimeouts,
                           n_modulesHitWordLimit, n_packetStructureErrors, n_packet8thWordErrors );
  
        ERS_LOG( "IM lane number " << ichannel << " monitoring counts:\n"
              << " b0f timeouts:             " << dec << n_b0ftimeouts << "    " << n_b0ftimeouts << "\n"
              << " l1id timeouts:            " << n_l1idtimeouts << "    " << n_l1idtimeouts << "\n"
              << " N modules hit word limit: " << n_modulesHitWordLimit << "    " << n_modulesHitWordLimit << "\n"
              << " Packet structure errors:  " << n_packetStructureErrors << "    " << n_packetStructureErrors << "\n"
              << " Packet 8th word errors:   " << n_packet8thWordErrors << "    " << n_packet8thWordErrors );
      }
      m_istimedout = timedOut;
    } catch ( const std::exception& ex ) {
      daq::ftk::ftkException e( ERS_HERE, m_name, ex.what() );
      ers::warning( e );
    }
    ERS_LOG( "Done checking timed out lanes" );

    static uint32_t m_inputPacketErrorLanes = 0x0;
    static uint32_t m_inputPacket8thWordErrorLanes = 0x0;
    static uint32_t m_unknownModuleHeader = 0x0;

    ERS_LOG( "Checking lane input packet errors (and unknown module header)." );
    uint32_t inputPacketErrorLanes = 0x0;
    uint32_t inputPacket8thWordErrorLanes = 0x0;
    uint32_t unknownModuleHeaderSeen = 0x0;
    try {
      getInputPacketErrorLinks( inputPacketErrorLanes, inputPacket8thWordErrorLanes );
      getUnknownModuleHeaderLinks( unknownModuleHeaderSeen );
  
      for ( uint32_t ichannel=0; ichannel < NUMBER_OF_DF_INPUT_LANES; ++ichannel ) {
        if ( (0x1<<ichannel) & activeMask & ~m_inputPacketErrorLanes & inputPacketErrorLanes ) {
          ERS_LOG( "Input packet error detected in IM lane number " << ichannel );
        }
        if ( (0x1<<ichannel) & activeMask & ~m_inputPacket8thWordErrorLanes & inputPacket8thWordErrorLanes ) {
          ERS_LOG( "Input packet 8th word error detected in IM lane number " << ichannel );
        }
        if ( (0x1<<ichannel) & activeMask & ~m_unknownModuleHeader & unknownModuleHeaderSeen ) {
          ERS_LOG( "Unknown module header observed in IM lane number " << ichannel );
        }
      }
      m_inputPacketErrorLanes = inputPacketErrorLanes;
      m_inputPacket8thWordErrorLanes = inputPacket8thWordErrorLanes;
      m_unknownModuleHeader = unknownModuleHeaderSeen;
    } catch ( const std::exception& ex ) {
      daq::ftk::ftkException e( ERS_HERE, m_name, ex.what() );
      ers::warning( e );
    }
    ERS_LOG( "Done checking lane input errors." );

  }
} // inputMonitoring


void FtkDataFormatterApi::stopAcceptingData( bool usePseudoData, uint32_t useBlockTransfer )
{
  ERS_LOG( "Start stopping pseudodata/disable SLINK" );

  vector<uint32_t> vec_IMChannelidx = m_im->getIMLutIdx();
  for ( uint32_t ich = 0; ich < vec_IMChannelidx.size(); ++ich ) {
    int trial = 0;
    const int NTRIALS = 3;

    // disable SLINK
    while ( !usePseudoData ) {
      try { 
        m_im->control_slink( vec_IMChannelidx.at(ich), 0x0 );
        break;
      } catch ( const std::exception& ex ) {
        if ( trial == NTRIALS-1 ) {
          ERS_LOG( "Exception caught while retrying to disable SLINK on ch = " << vec_IMChannelidx.at(ich) << ": " << ex.what() );
          ers::warning( daq::ftk::ftkException( ERS_HERE, m_name, " will skip disabling SLINK on ch = " + to_string(vec_IMChannelidx.at(ich)) + " after " + to_string(NTRIALS) + " attempts." ) );
          break;
        }
        ERS_LOG( "Exception caught while disabling SLINK on ch = " << vec_IMChannelidx.at(ich) << ": " << ex.what() );
        ++trial;
      }
    }

    while ( usePseudoData ) {
      try {
        m_im->control_pseudo_data( vec_IMChannelidx.at(ich), 0x0, 0x0 );
        break;
      } catch ( const std::exception& ex ) {
        if ( trial == NTRIALS-1 ) {
          ERS_LOG( "Exception caught while retrying to stop pseudodata on ch = " << vec_IMChannelidx.at(ich) << ": " << ex.what() );
          ers::warning( daq::ftk::ftkException( ERS_HERE, m_name, " will skip disabling pseudodata on ch = " + to_string(vec_IMChannelidx.at(ich)) + " after " + to_string(NTRIALS) + " attempts." ) );
          break;
        }
        ERS_LOG( "Exception caught while stopping pseudodata on ch = " << vec_IMChannelidx.at(ich) << ": " << ex.what() );
        ++trial;
      }
    }
  }
  ERS_LOG( "Finished stop pseudodata/disable SLINK" );
}

void FtkDataFormatterApi::clearFIFOs( std::function<void()> quickReset )
{
  quickReset();
}

void FtkDataFormatterApi::clearMonitoring()
{
  // handled in quickReset
}

// TODO: must be implemented in FW first
void FtkDataFormatterApi::checkFIFOsCleared()
{
}

// ====================================================
bool FtkDataFormatterApi::check(uint32_t first, uint32_t second, std::string name)
{
  if (first != second)
  {
    std::cout << "inconsistency in '" << name << "': " << first << " != " << second << std::endl;
    return false;
  }
  return true;
}

// ====================================================
bool FtkDataFormatterApi::check(map2_t first, map2_t second, std::string name)
{
  if (first != second)
  {
    std::cout << "inconsistency in '" << name << "'. First: " << std::endl;
    for (map2_t::iterator itr=first.begin();itr!=first.end();itr++)
    {
      std::cout << " -> (" << itr->first << ", " << itr->second << ")" << std::endl;
    }
    std::cout << "Second:" << std::endl;
    for (map2_t::iterator itr=second.begin();itr!=second.end();itr++)
    {
      std::cout << " -> (" << itr->first << ", " << itr->second << ")" << std::endl;
    }
    return false;
  }
  return true;
}

// ====================================================
bool FtkDataFormatterApi::check(map3_t first, map3_t second, std::string name)
{
  if (first != second)
  {
    std::cout << "inconsistency in '" << name << "'. First: " << std::endl;
    for (map3_t::iterator itr=first.begin();itr!=first.end();itr++)
    {
      std::cout << " -> ((" << itr->first.first << ", " << itr->first.second << ")" << ", " << itr->second << ")" << std::endl;
    }
    std::cout << "Second:" << std::endl;
    for (map3_t::iterator itr=second.begin();itr!=second.end();itr++)
    {
      std::cout << " -> ((" << itr->first.first << ", " << itr->first.second << ")" << ", " << itr->second << ")" << std::endl;
    }
    return false;
  }
  return true;
}

// ====================================================
void
FtkDataFormatterApi::gt_reset()
{  
  pll_reset();
  gtrxtx_reset();
  m_ipbus->single_access_write("reg.gt_link_controller",                 0X0);
  m_ipbus->single_access_write("reg.gt_link_controller.slink_reset",         0X1);
  m_ipbus->single_access_write("reg.gt_link_controller.slink_ureset",         0X0);
  m_ipbus->single_access_write("reg.gt_link_controller.slink_utest",         0X0);
  m_ipbus->single_access_write("reg.gt_link_controller.slink_reset",         0X1);
}

bool FtkDataFormatterApi::standardReset( bool fast, uint32_t step, 
                                         const std::map<uint32_t, uint32_t>& gtch2rxpolarity,
                                         const std::map<uint32_t, uint32_t>& gtch2txpolarity,
                                         const std::map<uint32_t, uint32_t>& gtch2force_ready_mode,
                                         const std::map<uint32_t, uint32_t>& gtch2to_altera_fpga,
                                         const std::map<uint32_t, uint32_t>& gtch_ignore_freeze,
                            const std::map<std::pair<uint32_t, uint32_t>, bool>& multishelf_enable,
                                         uint32_t rxports )
{
  if ( step == 0 ) { // initiate reset
    m_ipbus->single_access_write("reg.reset",                                 0X0);  
    m_ipbus->single_access_write("reg.reset.reset_delay",                 0X1);
    m_ipbus->single_access_write("reg.reset.disable_fmc_input",                 0X1);
    m_ipbus->single_access_write("reg.reset.reset_parity_checker",         0X1);
    m_ipbus->single_access_write("reg.reset.fmcin_logic_reset",                 0X1);
    m_ipbus->single_access_write("reg.reset.main_state_machine_reset",         0X1);
    m_ipbus->single_access_write("reg.reset.i2c_state_machine_reset",         0X1);
    if (!fast) m_ipbus->single_access_write("reg.reset.configurable_parameter_reset", 0X1);
    
    
    m_ipbus->single_access_write("reg.fmc_user_signal",                         0X0);
    m_ipbus->single_access_write("reg.fmc_user_signal.mezzanine_reset",         0X1);
    m_ipbus->single_access_write("reg.fmc_user_signal.mezzanine_trigger", 0X0);
    m_ipbus->single_access_write("reg.fmc_user_signal.mezzanine_reset",         0X1);

    m_ipbus->single_access_write("reg.spy_controller",                         0X0);
    spy_reset();

    m_ipbus->single_access_write("reg.fmc_user_signal.mezzanine_reset",    0X0);
    m_ipbus->single_access_write("reg.reset.configurable_parameter_reset", 0X0);
    m_ipbus->single_access_write("reg.reset.i2c_state_machine_reset",      0X0);
    
    // clock delay setting
    m_ipbus->single_access_write("reg.reset.reset_delay",                  0X0);

    // gt configuration with force_ready=false
    if (!fast) gt_configuration( 0, gtch2rxpolarity, gtch2txpolarity, gtch2force_ready_mode,
                                    gtch2to_altera_fpga, gtch_ignore_freeze,
                                    multishelf_enable, rxports );
  } else if ( step == 1 ) { // complete reset
    //  Reset Procedure - Enable FMC input
    m_ipbus->single_access_write("reg.reset.disable_fmc_input",                 0X0);
    //  Reset Procedure - fmc input state machine reset
    m_ipbus->single_access_write("reg.reset.fmcin_logic_reset",                 0X0);
    //ml sleep (0.5);
    //  Reset Procedure - release main state machine reset
    m_ipbus->single_access_write("reg.reset.main_state_machine_reset",         0X0);
    //ml sleep (0.5);
    //  Reset Procedure - release SLINK
    if (!fast) m_ipbus->single_access_write("reg.gt_link_controller.slink_reset",         0X0);
    //  Reset Procedure - release Sync Done
    m_ipbus->single_access_write("reg.reset.reset_parity_checker",         0X0);
    
    spy_restart();
    
    uint32_t value_spy_controller         = 0X0;
    uint32_t value_reset                  = 0X0;
    uint32_t value_fmc_user_signal        = 0X0;
    
    m_ipbus->single_access_read("reg.reset",             value_reset);
    m_ipbus->single_access_read("reg.fmc_user_signal",   value_fmc_user_signal);
    m_ipbus->single_access_read("reg.spy_controller",    value_spy_controller);
    
    m_ipbus->single_access_write("reg.reset.counter_parameter_reset",         0X1);
    m_ipbus->single_access_write("reg.reset.counter_parameter_reset",         0X0);
    

    char string1[200];
    sprintf(string1,"after reset reg.reset=0x%08x fmc_user_signal=0x%08x spy_controller=0x%08x",
            value_reset, value_fmc_user_signal, value_spy_controller);
    ERS_LOG(string1);
    if (not (value_reset                        ==0X0 and 
             value_fmc_user_signal        ==0X0 and 
             value_spy_controller                ==0X0) ) {
      ERS_LOG("[NOTE] : Some of the reset parameter are not released yet");
    }  
  }

  return true;
}

bool FtkDataFormatterApi::configureFMC( uint32_t enableFMCLanesMask, uint32_t boardMaskIDO,
                                        uint32_t boardMaskILI )
{
  if ( m_df_fwVersion < 0x19000 )
    m_ipbus->single_access_write("reg.enable_fmc_lanes_mask",  enableFMCLanesMask);
  m_ipbus->single_access_write("reg.this_board_mask_ido",    boardMaskIDO);
  m_ipbus->single_access_write("reg.this_board_mask_ili",    boardMaskILI);
  return true;
}

bool FtkDataFormatterApi::configureExpectedNumModules( const std::map<uint32_t,uint32_t>& FMCIn2NumModules,
                                                       const std::map<uint32_t,uint32_t>& SLINKOut2NumModules )
{
  for ( const auto& item : FMCIn2NumModules ) {
    expected_nummodules_information_configuration( item.first, 
                                         SW_CONF_UPDATE_MASK_FMC_INPUT_NUMBER_OF_EXPECTED_MODULES,
                                                   item.second );
  }
  for ( const auto& item : SLINKOut2NumModules ) {
    expected_nummodules_information_configuration( item.first,
                                        SW_CONF_UPDATE_MASK_SLINK_OUTPUT_NUMBER_OF_EXPECTED_MODULES,
                                                   item.second );
  }

  return true;
}


// ====================================================
void 
FtkDataFormatterApi::clock_phase_configuration(const uint32_t& im_fpga_id,
                                               const uint32_t& inv_configuration,
                                               const uint32_t& delay_configuration)
{
  const uint32_t ce_command   = (0X1 << im_fpga_id);
  const uint32_t ce_disable   = (0X0);
  const uint32_t inv_command_mask  = (0X1 << im_fpga_id);
  const uint32_t address_inv = m_ipbus->get_hardware()->getNode("reg.fmc_config_clk_inv").getAddress();
  m_ipbus->get_hardware()->getClient().write ( address_inv, inv_configuration, inv_command_mask);
  m_ipbus->get_hardware()->dispatch();
  
  for (uint32_t iDelay=0; iDelay<delay_configuration; iDelay++) {
    m_ipbus->single_access_write("reg.fmc_config_clkdelay_ce", ce_command);
    m_ipbus->single_access_write("reg.fmc_config_clkdelay_ce", ce_disable);
  }  
}

// ====================================================
void
FtkDataFormatterApi::gt_configuration(const uint32_t& gt_id,
                                      const uint32_t& gt_rxpolarity,
                                      const uint32_t& gt_txpolarity,
                                      const uint32_t& force_ready_mode,
                                      const uint32_t& to_altera_fpga,
                                      const uint32_t& ignore_freeze)
{
  m_ipbus->single_access_write        ("reg.gt_link_monitor_configure_laneselector",         gt_id);
  
  m_ipbus->clear_write_register_buffer();  
  m_ipbus->add_register_to_write("reg.gt_link_configuration.gt_rxpolarity",         gt_rxpolarity);
  m_ipbus->add_register_to_write("reg.gt_link_configuration.gt_txpolarity",         gt_txpolarity);
  m_ipbus->add_register_to_write("reg.gt_link_configuration.force_ready_mode",         force_ready_mode);
  m_ipbus->add_register_to_write("reg.gt_link_configuration.to_altera_fpga",         to_altera_fpga);
  m_ipbus->add_register_to_write("reg.gt_link_configuration.ignore_freeze",         ignore_freeze);
  m_ipbus->send_transaction_to_write();
  
  m_ipbus->single_access_write        ("reg.gt_link_configuration_wen.wen",                 0X1);
  m_ipbus->single_access_write        ("reg.gt_link_configuration_wen.wen",                 0X0);
}

// ====================================================
void 
FtkDataFormatterApi::gt_monitor(const uint32_t& gt_id,
                                uint32_t& gt_rxpolarity,
                                uint32_t& gt_txpolarity,
                                uint32_t& force_ready_mode,
                                uint32_t& to_altera_fpga,
                                uint32_t& gt_rxbyteisaligned,
                                uint32_t& gt_tx_reset_done,
                                uint32_t& gt_rx_reset_done,
                                uint32_t& gt_pll_lock,
                                uint32_t& slink_testled_n,
                                uint32_t& slink_lderrled_n,
                                uint32_t& slink_lupled_n,
                                uint32_t& slink_flowctrlled_n,
                                uint32_t& slink_activityled_n,
                                uint32_t& slink_lrl
                                )
{
  m_ipbus->single_access_write("reg.gt_link_monitor_configure_laneselector", gt_id);
  
  m_ipbus->clear_read_register_buffer();
  m_ipbus->add_register_to_read("reg.gt_link_configuration_read.gt_rxpolarity");
  m_ipbus->add_register_to_read("reg.gt_link_configuration_read.gt_txpolarity");
  m_ipbus->add_register_to_read("reg.gt_link_configuration_read.force_ready_mode");
  m_ipbus->add_register_to_read("reg.gt_link_configuration_read.to_altera_fpga");
  m_ipbus->add_register_to_read("reg.gt_link_monitor.gt_rxbyteisaligned");
  m_ipbus->add_register_to_read("reg.gt_link_monitor.gt_tx_reset_done");
  m_ipbus->add_register_to_read("reg.gt_link_monitor.gt_rx_reset_done");
  m_ipbus->add_register_to_read("reg.gt_link_monitor.gt_pll_lock");
  m_ipbus->add_register_to_read("reg.gt_link_monitor.slink_testled_n");
  m_ipbus->add_register_to_read("reg.gt_link_monitor.slink_lderrled_n");
  m_ipbus->add_register_to_read("reg.gt_link_monitor.slink_lupled_n");
  m_ipbus->add_register_to_read("reg.gt_link_monitor.slink_flowctrlled_n");
  m_ipbus->add_register_to_read("reg.gt_link_monitor.slink_activityled_n");
  m_ipbus->add_register_to_read("reg.gt_slink_lrl_monitor");
  m_ipbus->send_transaction_to_read();
  
  gt_rxpolarity      =m_ipbus->get_readback_register_value("reg.gt_link_configuration_read.gt_rxpolarity");
  gt_txpolarity      =m_ipbus->get_readback_register_value("reg.gt_link_configuration_read.gt_txpolarity");
  gt_pll_lock        =m_ipbus->get_readback_register_value("reg.gt_link_monitor.gt_pll_lock");
  force_ready_mode   =m_ipbus->get_readback_register_value("reg.gt_link_configuration_read.force_ready_mode");
  to_altera_fpga     =m_ipbus->get_readback_register_value("reg.gt_link_configuration_read.to_altera_fpga");
  gt_rxbyteisaligned =m_ipbus->get_readback_register_value("reg.gt_link_monitor.gt_rxbyteisaligned");
  gt_tx_reset_done   =m_ipbus->get_readback_register_value("reg.gt_link_monitor.gt_tx_reset_done");
  gt_rx_reset_done   =m_ipbus->get_readback_register_value("reg.gt_link_monitor.gt_rx_reset_done");
  slink_testled_n    =m_ipbus->get_readback_register_value("reg.gt_link_monitor.slink_testled_n");
  slink_lderrled_n   =m_ipbus->get_readback_register_value("reg.gt_link_monitor.slink_lderrled_n");
  slink_lupled_n     =m_ipbus->get_readback_register_value("reg.gt_link_monitor.slink_lupled_n");
  slink_flowctrlled_n=m_ipbus->get_readback_register_value("reg.gt_link_monitor.slink_flowctrlled_n");
  slink_activityled_n=m_ipbus->get_readback_register_value("reg.gt_link_monitor.slink_activityled_n");
  slink_lrl          =m_ipbus->get_readback_register_value("reg.gt_slink_lrl_monitor");
}

void FtkDataFormatterApi::getb0fSkew( uint32_t& b0fSkew )
{
  if ( m_df_fwVersion < 0x19000 )
    m_ipbus->single_access_read( "reg.input_b0f_skew", b0fSkew );
}

void FtkDataFormatterApi::getTimedOutLinks( uint32_t& timedOut )
{
  if ( m_df_fwVersion < 0x19000 )
    m_ipbus->single_access_read( "reg.input_b0f_timeout_links", timedOut );
}

void FtkDataFormatterApi::getTotalEventsGivenUp( const uint32_t& gt_id,
                                  uint32_t& nEvtsGivenUp )
{
  m_ipbus->single_access_write( "reg.slink_counter_selected", gt_id );

  m_ipbus->clear_read_register_buffer();
  m_ipbus->add_register_to_read( "reg.slink_total_evts_given_up" );
  m_ipbus->send_transaction_to_read();

  nEvtsGivenUp = m_ipbus->get_readback_register_value( "reg.slink_total_evts_given_up" );
}

void FtkDataFormatterApi::getInputPacketErrorLinks( uint32_t& inputPacketErrorLinks, uint32_t& inputPacket8thWordErrorLinks )
{
  if ( m_df_fwVersion < 0x19000 ) {
    m_ipbus->single_access_read( "reg.input_packet_error.other_error_links", inputPacketErrorLinks );
    m_ipbus->single_access_read( "reg.input_packet_error.8th_word_error_links", inputPacket8thWordErrorLinks );
  }
}

void FtkDataFormatterApi::getInputLaneMonitoringCounts( const uint32_t& lane,
                                                        uint32_t& b0f_timeout_count,
                                                        uint32_t& l1id_timeout_count,
                                                        uint32_t& word_limit_hit_count,
                                                        uint32_t& packet_structure_error_count,
                                                        uint32_t& packet_8th_word_error_count )
{
  if ( m_df_fwVersion < 0x19000 ) {
    m_ipbus->single_access_write( "reg.input_lane_monitor_selector", lane );

    m_ipbus->clear_read_register_buffer();
    m_ipbus->add_register_to_read( "reg.input_lane_monitor_b0f_timeout_count" );
    m_ipbus->add_register_to_read( "reg.input_lane_monitor_l1id_timeout_count" );
    m_ipbus->add_register_to_read( "reg.input_lane_monitor_number_modules_hit_word_limit" );
    m_ipbus->add_register_to_read( "reg.input_lane_monitor_packet_structure_error_count" );
    m_ipbus->add_register_to_read( "reg.input_lane_monitor_packet_eighth_word_error_count" );
    m_ipbus->send_transaction_to_read();

    b0f_timeout_count = m_ipbus->get_readback_register_value( "reg.input_lane_monitor_b0f_timeout_count" );
    l1id_timeout_count = m_ipbus->get_readback_register_value( "reg.input_lane_monitor_l1id_timeout_count" );
    word_limit_hit_count = m_ipbus->get_readback_register_value( "reg.input_lane_monitor_number_modules_hit_word_limit" );
    packet_structure_error_count = m_ipbus->get_readback_register_value( "reg.input_lane_monitor_packet_structure_error_count" );
    packet_8th_word_error_count = m_ipbus->get_readback_register_value( "reg.input_lane_monitor_packet_eighth_word_error_count" );
  }
}

void FtkDataFormatterApi::getUnknownModuleHeaderLinks( uint32_t& unknownModuleHeader )
{
  if ( m_df_fwVersion < 0x19000 )
    m_ipbus->single_access_read( "reg.input_unknown_module_header_seen", unknownModuleHeader );
}

// ====================================================
void
FtkDataFormatterApi::expected_nummodules_information_configuration(const uint32_t& lane_id,
                                                                   const uint32_t& update_mask,
                                                                   // type=X01 : input FMC expected number
                                                                   // type=X02 : output SLINK expected number
                                                                   const uint32_t& value)
{
  m_ipbus->single_access_write("reg.expected_number_of_module", value);
  m_ipbus->single_access_write("reg.switching_configuration_lane_selector", lane_id);
  switch (update_mask) {
  case SW_CONF_UPDATE_MASK_FMC_INPUT_NUMBER_OF_EXPECTED_MODULES:
    m_ipbus->single_access_write("reg.update_switching_configuration_enable.fmc_input_number_of_expected_modules",0X1);
    m_ipbus->single_access_write("reg.update_switching_configuration_enable.fmc_input_number_of_expected_modules",0X0);
    break;
  case SW_CONF_UPDATE_MASK_SLINK_OUTPUT_NUMBER_OF_EXPECTED_MODULES:
    m_ipbus->single_access_write("reg.update_switching_configuration_enable.slink_output_number_of_expected_modules",0X1);
    m_ipbus->single_access_write("reg.update_switching_configuration_enable.slink_output_number_of_expected_modules",0X0);
    break;
  default:
    std::stringstream message;
    message << " undefined update mask for switching configuration: update_mask=0x" << std::hex << update_mask;
    daq::ftk::ftkException ex(ERS_HERE, name_ftk(), message.str());
    ers::warning( ex );
    m_ipbus->single_access_write("reg.update_switching_configuration_enable.slink_output_number_of_expected_modules",0X0);
    break;
  }
}

// ====================================================

void
FtkDataFormatterApi::internal_link_mask_configuration(const uint32_t& central_switch_output_lane_id,
                                                      const uint32_t& mask_pattern)
{
  m_ipbus->single_access_write("reg.internallink_destination_words",                 mask_pattern);
  m_ipbus->single_access_write("reg.switching_configuration_lane_selector",         central_switch_output_lane_id);
  m_ipbus->single_access_write("reg.update_switching_configuration_enable.central_switch_output_to_destination_port",0X1);
  m_ipbus->single_access_write("reg.update_switching_configuration_enable.central_switch_output_to_destination_port",0X0);
}


// ====================================================
void
FtkDataFormatterApi::gtrxtx_reset()
{
  m_ipbus->single_access_write("reg.gt_link_controller.transceiver_reset", 0X1);
  m_ipbus->single_access_write("reg.gt_link_controller.transceiver_reset", 0X0);
}
// ====================================================
void
FtkDataFormatterApi::pll_reset()
{
  m_ipbus->single_access_write("reg.gt_link_controller.pll_reset", 0X1);
  m_ipbus->single_access_write("reg.gt_link_controller.pll_reset", 0X0);
}

// ====================================================
void
FtkDataFormatterApi::spy_reset()
{
  m_ipbus->single_access_write("reg.spy_controller.spy_reset", 0X1);
  m_ipbus->single_access_write("reg.spy_controller.spy_reset", 0X0);
}

// ====================================================
void
FtkDataFormatterApi::spy_restart()
{
  m_ipbus->single_access_write("reg.spy_controller.spy_freeze", 0X0);
}

// ====================================================
void
FtkDataFormatterApi::spy_freeze()
{
  m_ipbus->single_access_write("reg.spy_controller.spy_freeze", 0X1);
}

void
FtkDataFormatterApi::spy_read_enable()
{
  uint32_t enable(0X1);
  m_ipbus->single_access_write("reg.spy_readen", enable);

}

void
FtkDataFormatterApi::spy_read_disable()
{
  uint32_t disable(0X0);
  m_ipbus->single_access_write("reg.spy_readen", disable);

}


/***********************************/
int FtkDataFormatterApi::read_spybuffer_blocktransfer(const uint32_t& spybuffer_length, const uint32_t& lane_id, std::vector<uint32_t>& dout)
/***********************************/
{
      int rc = 0;
      const uint32_t maxSize = 10000;
    
      // block transfer (of spy buffer)
      m_ipbus->single_access_write("reg.spy_laneselector", lane_id);
      uhal::ValWord< uint32_t > mems[maxSize];
      if (spybuffer_length > maxSize) {
        char error_msg[BUFSIZ];
        snprintf(error_msg, sizeof(error_msg), 
                 "DF-%u-%u Exception: maxSize=%3d : #registers=%3d \n",
                 m_shelf,
                 m_slot,
                 maxSize,
                 spybuffer_length);
        throw daq::ftk::IPBusIssue(ERS_HERE, m_name, error_msg);
      }

      for (uint32_t iAddr = 0; iAddr < spybuffer_length; iAddr++){
        m_ipbus->get_hardware()->getNode("reg.spy_readaddr").write(iAddr);
        m_ipbus->get_hardware()->getNode("reg.spy_laneselector").write(lane_id);
        m_ipbus->get_hardware()->getNode("reg.spy_laneselector").read();
        mems[iAddr] = m_ipbus->get_hardware()->getNode("reg.spy_readout").read();
      }
      m_ipbus->get_hardware()->dispatch(); // send transaction
      for (uint32_t iAddr = 0; iAddr < spybuffer_length; iAddr++) dout.push_back(mems[iAddr].value());
      for (uint32_t iAddr = 0; iAddr < spybuffer_length; iAddr++) {
        if (!mems[iAddr].valid()) {
          throw daq::ftk::IPBusRead(ERS_HERE, m_name + " Invalid read access during spybuffer block transfer");
          rc = 1;
        }
      }
      return rc;

}


/***********************************/
int FtkDataFormatterApi::read_spybuffer_blocktransfer_test(const uint32_t& spybuffer_length, const uint32_t& lane_id, std::vector<uint32_t>& dout)
/***********************************/
{
      int rc = 0;
      const uint32_t maxSize = 10000;
    
      // block transfer (of spy buffer)
      m_ipbus->single_access_write("reg.spy_laneselector", lane_id);
      uhal::ValWord< uint32_t > mems[maxSize];
      if (spybuffer_length > maxSize) {
        char error_msg[BUFSIZ];
        snprintf(error_msg, sizeof(error_msg), 
                 "DF-%u-%u Exception: maxSize=%3d : #registers=%3d \n",
                 m_shelf,
     m_slot,
     maxSize,
                 spybuffer_length);
        throw daq::ftk::IPBusIssue(ERS_HERE, m_name, error_msg);
      }

      for (uint32_t iAddr = 0; iAddr < spybuffer_length; iAddr++){
        m_ipbus->get_hardware()->getNode("reg.spy_readaddr").write(iAddr);
        mems[iAddr] = m_ipbus->get_hardware()->getNode("reg.spy_readout").read();
      }
      m_ipbus->get_hardware()->dispatch(); // send transaction
      
      for (uint32_t iAddr = 0; iAddr < spybuffer_length; iAddr++) dout.push_back(mems[iAddr].value());
      for (uint32_t iAddr = 0; iAddr < spybuffer_length; iAddr++) {
        if (!mems[iAddr].valid()) {
          throw daq::ftk::IPBusRead(ERS_HERE, m_name + " Invalid read access during spybuffer block transfer");
          rc = 1;
        }
      }
      return rc;

}

//===============================
bool FtkDataFormatterApi::spy_dump_individual_lane_block(const uint32_t& lane_id,
                                                         std::vector<uint32_t>& dout)
{
  const uint32_t depth = 4096;
  const uint32_t readsize = 4;
  bool rc = true;
  
  spy_freeze();
  std::vector<uint32_t> dump_data(depth);
  std::vector<uint32_t> raw_data(readsize);
  msleep(10);

  spy_read_enable();

  msleep(10);

  m_ipbus->single_access_write("reg.spy_laneselector", lane_id);

  for (uint32_t iAddr=0; iAddr<depth; iAddr++) {
    if (iAddr%4 != 0) continue;
    m_ipbus->single_access_write("reg.spy_readaddr", iAddr);
    try {
      m_ipbus->single_access_block_read("reg.spy_readout", raw_data, readsize);
    } catch (daq::ftk::IPBusRead ex) {
      throw daq::ftk::IPBusIssue(ERS_HERE, m_name, " Failed to read a fifo word", ex);
      rc = false;
    }

    dump_data[iAddr] = raw_data[2];
    dump_data[iAddr+1] = raw_data[3];
    dump_data[iAddr+2] = raw_data[0];
    dump_data[iAddr+3] = raw_data[1];

  }
  
  dout = dump_data;
  spy_restart();

  uint32_t disable(0X0);
  m_ipbus->single_access_write("reg.spy_readen", disable);

  return rc;
}

// ====================================================
bool
FtkDataFormatterApi::read_32bit_counter(const uint32_t& type_id, 
                                        const uint32_t& lane_id, 
                                        uint32_t& dout)
{
  bool rc = true;
  vector<std::string> regs;
  vector<uint32_t> vals;
  regs.push_back("reg.readout_32bit_counter_lane_selector.type");
  regs.push_back("reg.readout_32bit_counter_lane_selector.lane");
  vals.push_back(type_id);
  vals.push_back(lane_id);
  try {
    rc = m_ipbus->read_using_selector(regs, vals,
                                      "reg.readout_32bit_counter", dout);
  } catch (daq::ftk::IPBusRead ex) {
    throw daq::ftk::IPBusIssue(ERS_HERE,m_name, " Failed to read a 32-bit counter using selector",ex);
  }
  return rc;  
}

// ====================================================
void
FtkDataFormatterApi::write_lut(const uint32_t& lut_type_id,
                               const uint32_t& lut_addr,
                               const uint32_t& lut_data,
                               const uint32_t& lut_lane_enable_mask)
{
  const uint32_t val_lut_config_selector     = (lut_type_id & 0X000000FF);
  const uint32_t val_lut_config_addr_confin  = (lut_addr    & 0X0000FFFF);
  const uint32_t val_lut_config_data_confin  = (lut_data    & 0XFFFFFFFF);
  const uint32_t val_lut_config_wen_distable = 0X0;
  
  m_ipbus->single_access_write        ("reg.lut_configuration_wen_confin",         val_lut_config_wen_distable);
  m_ipbus->clear_write_register_buffer();
  if( lut_type_id != 0XFFFFFFFF ) {
    // do only for a reasonable lane ID
    m_ipbus->add_register_to_write("reg.lut_configuration_selector",         val_lut_config_selector);
  }
  m_ipbus->add_register_to_write("reg.lut_configuration_addr_confin",         val_lut_config_addr_confin);
  m_ipbus->add_register_to_write("reg.lut_configuration_data_confin",         val_lut_config_data_confin);
  m_ipbus->send_transaction_to_write();
  m_ipbus->single_access_write        ("reg.lut_configuration_wen_confin",         lut_lane_enable_mask);
  m_ipbus->single_access_write        ("reg.lut_configuration_wen_confin",         val_lut_config_wen_distable);
}

// ====================================================
void
FtkDataFormatterApi::write_lut(const uint32_t& lut_addr,
                               const uint32_t& lut_data,
                               const uint32_t& lut_lane_enable_mask)
{
  write_lut(0XFFFFFFFF, lut_addr, lut_data, lut_lane_enable_mask);
}

// ====================================================
bool
FtkDataFormatterApi::read_lut(const uint32_t& lut_type_id,
                              const uint32_t& lut_addr,
                              const uint32_t& lut_lane_id,
                              uint32_t& lut_data)
{
  bool rc(true);
  
  const uint32_t val_lut_config_selector     = (lut_type_id & 0X000000FF);
  const uint32_t val_lut_config_read_lane    = (lut_lane_id & 0X000000FF);
  const uint32_t val_lut_config_addr_confin  = (lut_addr    & 0X0000FFFF);
  const uint32_t val_lut_config_wen_distable = 0X0;
  
  m_ipbus->single_access_write        ("reg.lut_configuration_wen_confin",         val_lut_config_wen_distable);
  m_ipbus->clear_write_register_buffer();
  m_ipbus->add_register_to_write("reg.lut_configuration_selector",         val_lut_config_selector);
  m_ipbus->add_register_to_write("reg.lut_configuration_read_lane",         val_lut_config_read_lane);
  m_ipbus->add_register_to_write("reg.lut_configuration_addr_confin",         val_lut_config_addr_confin);
  m_ipbus->send_transaction_to_write();
  
  try {
    m_ipbus->single_access_read("reg.lut_configuration_data_confout", lut_data);
  } catch (daq::ftk::IPBusRead ex) {
    rc = false;
    throw daq::ftk::IPBusIssue(ERS_HERE, m_name, " Failed to read reg.lut_configuration_data_confout", ex);
  }
  
  return rc; 
}

// ====================================================
void
FtkDataFormatterApi::printFwVersion( bool useBlockTransfer )
{
  uint32_t value_fw_ver                         = 0X0;

  m_ipbus->single_access_read("reg.firmware_version", value_fw_ver);
  
  daq::ftk::FTKFwVersion df_fw(ERS_HERE, m_name, "DF", (int)value_fw_ver);
  ers::info(df_fw);

  // always do this check (regardless of m_doFWCheck)
  if ( value_fw_ver < 0x19000 && m_df_fwVersion >= 0x19000 ) {
    ers::warning( daq::ftk::ftkException( ERS_HERE, m_name, " Board FW version is < 0x19000 while OKS FW version is >= 0x19000. This will almost certainly not work." ) );
  } else if ( value_fw_ver >= 0x19000 && m_df_fwVersion < 0x19000 ) {
    ers::warning( daq::ftk::ftkException( ERS_HERE, m_name, " Board FW version is >= 0x19000 while OKS FW version is < 0x19000. This will almost certainly not work." ) );
  }

  if(m_doFWCheck){
    // Data Formatter Fw Version Check
    if( value_fw_ver != m_df_fwVersion){
      ERS_LOG("DF Expected version "<<hex<<m_df_fwVersion);      
      daq::ftk::WrongFwVersion e(ERS_HERE, m_name, m_name, 0, value_fw_ver, m_df_fwVersion);
      ers::error(e);   
    }
  }

  // Input Mezzanine Fw Version Readout
  // for block transfer
  for (uint32_t ifpga=0; ifpga<8; ++ifpga) {
    m_im->print_fw_verion( ifpga, m_doFWCheck, m_im_fwVersion );
  }
  //  daq::ftk::ftkException ex(ERS_HERE, name_ftk(), "No DF FW version available yet.");
}

// ====================================================
void
FtkDataFormatterApi::gt_configuration( bool force_ready, 
                                       const std::map<uint32_t, uint32_t>& gtch2rxpolarity,
                                       const std::map<uint32_t, uint32_t>& gtch2txpolarity,
                                       const std::map<uint32_t, uint32_t>& gtch2force_ready_mode,
                                       const std::map<uint32_t, uint32_t>& gtch2to_altera_fpga,
                                       const std::map<uint32_t, uint32_t>& gtch_ignore_freeze,
                            const std::map<std::pair<uint32_t, uint32_t>, bool>& multishelf_enable,
                                       uint32_t rxports )
{
  gt_reset();
  
  for ( uint32_t i_gt=0; i_gt < NUMBER_OF_DF_GTX; ++i_gt ) {
    if ( gtch2rxpolarity.find( i_gt ) == gtch2rxpolarity.end() ) {
      std::stringstream message;
      message << " polarity not configured for transceiver number " << i_gt;
      daq::ftk::ftkException ex(ERS_HERE, name_ftk(), message.str());
      ers::warning( ex );
      continue;
    }
    if ( gtch2txpolarity.find( i_gt ) == gtch2txpolarity.end() ) {
      std::stringstream message;
      message << " polarity not configured for transceiver number " << i_gt;
      daq::ftk::ftkException ex(ERS_HERE, name_ftk(), message.str());
      ers::warning( ex );
      continue;
    }
    if ( gtch2force_ready_mode.find( i_gt ) == gtch2force_ready_mode.end() ) {
      std::stringstream message;
      message << " force ready not configured for transceiver number " << i_gt;
      daq::ftk::ftkException ex(ERS_HERE, name_ftk(), message.str());
      ers::warning( ex );
      continue;
    }
    if ( gtch2to_altera_fpga.find( i_gt ) == gtch2to_altera_fpga.end() ) {
      std::stringstream message;
      message << " Altera FPGA mode not configured for transceiver number " << i_gt;
      daq::ftk::ftkException ex(ERS_HERE, name_ftk(), message.str());
      ers::warning( ex );
      continue;
    }
    if ( gtch_ignore_freeze.find( i_gt ) == gtch_ignore_freeze.end() ) {
      std::stringstream message;
      message << " ignore freeze mode not configured for transceiver number " << i_gt;
      daq::ftk::ftkException ex(ERS_HERE, name_ftk(), message.str());
      ers::warning( ex );
      continue;
    }

    const uint32_t& gt_rxpolarity = gtch2rxpolarity.find( i_gt )->second;
    const uint32_t& gt_txpolarity = gtch2txpolarity.find( i_gt )->second;
    uint32_t gt_force_ready_mode = gtch2force_ready_mode.find( i_gt )->second;
    const uint32_t& gt_to_altera_fpga = gtch2to_altera_fpga.find( i_gt )->second;
    const uint32_t& gt_ignore_freeze = gtch_ignore_freeze.find( i_gt )->second;

    // auto-configure for multi-board
    if ( i_gt >= 36 && ( (rxports & (0x1<<(i_gt-36))) != 0 ) ) gt_force_ready_mode = 0; 

    if ( i_gt == 46 || i_gt == 58 ) {
      if ( multishelf_enable.find( std::make_pair( m_shelf, m_slot ) ) != multishelf_enable.end() ) {
        bool enable = multishelf_enable.find( std::make_pair( m_shelf, m_slot ) )->second;
        gt_force_ready_mode = !enable;
      }
    }

    gt_configuration( i_gt, gt_rxpolarity, gt_txpolarity, gt_force_ready_mode || force_ready, gt_to_altera_fpga, gt_ignore_freeze ); 

  }

  uint32_t value_gt_link_controller = 0X0;
  m_ipbus->single_access_read("reg.gt_link_controller", value_gt_link_controller);
  ERS_LOG( "After reset gt_link_controller=0x" << std::hex << value_gt_link_controller );
  if (not  value_gt_link_controller == 0X0) {
    ERS_LOG(  m_name << " [NOTE] : gt reset parameter is not released yet");
  }
}


bool FtkDataFormatterApi::internalLinkMaskConfig( 
                                        const std::map<uint32_t, uint32_t>& laneId2DestMaskMap ) {
  for ( const auto& item : laneId2DestMaskMap ) {
    internal_link_mask_configuration( item.first, item.second );
  }
  return true;
}

// ====================================================
void 
FtkDataFormatterApi::lut_config_helper(const uint32_t& lut_type,
                                              const map3_t lane)
{
  m_ipbus->single_access_write("reg.lut_configuration_selector", (0XFF & lut_type));
  map3_t::const_iterator itc = lane.begin();
  map3_t::const_iterator ite = lane.end();
  for (; itc != ite; itc++) {
    const uint32_t& lane_id  = itc->first.first;
    const uint32_t lane_mask = (0X1 << lane_id);
    write_lut(itc->first.second,
              itc->second,
              lane_mask);
  }
}

// ====================================================
void 
FtkDataFormatterApi::lut_config_helper(const uint32_t& lut_type,
                                              const map2_t lane,
                                              const uint32_t& lane_mask)
{
  m_ipbus->single_access_write("reg.lut_configuration_selector", (0XFF & lut_type));
  map2_t::const_iterator itc = lane.begin();
  map2_t::const_iterator ite = lane.end();
  //ERS_LOG("lane_mask = "<<lane_mask);
  for (; itc != ite; itc++) {
    //const uint32_t lane_mask = 0X0000FFFF;
    write_lut(itc->first,
              itc->second,
              lane_mask);
  }
}

// ====================================================
void
FtkDataFormatterApi::setTxRxLine(const uint32_t& valTx,
                                 const uint32_t& valRx)
{
  m_ipbus->clear_write_register_buffer();
  m_ipbus->add_register_to_write("reg.internal_link_tx_enable", valTx);
  m_ipbus->add_register_to_write("reg.internal_link_rx_enable", valRx);
  m_ipbus->send_transaction_to_write();
}

// ===================================================
void
FtkDataFormatterApi::setTxRxLineBert(const uint32_t& valTx,
                                     const uint32_t& valRx)
{
  m_ipbus->clear_write_register_buffer();
  m_ipbus->add_register_to_write("reg.internal_link_tx_enable_bert", valTx);
  m_ipbus->add_register_to_write("reg.internal_link_rx_enable_bert", valRx);
  m_ipbus->send_transaction_to_write();
}

bool FtkDataFormatterApi::setLinkURL( uint32_t shelf, uint32_t slot )
{
  bool status = true;

  if ( shelf < 1 || shelf > 4 ) {
    std::stringstream message;
    message << "DF shelf number out of range (should be between 1 and 4): " << shelf;
    daq::ftk::ftkException ex(ERS_HERE, name_ftk(), message.str() );
    ers::warning( ex );
    status = false;
  }
  if ( slot < 3 || slot > 10 ) {
    std::stringstream message;
    message << "DF slot number out of range (should be between 3 and 10): " << slot;
    daq::ftk::ftkException ex(ERS_HERE, name_ftk(), message.str() );
    ers::warning( ex );
    status = false;
  }
  if ( !status ) return status; // don't write to the register

  uint32_t m = shelf-1;
  uint32_t n = slot-3;
  if ( m > 1 ) m -= 2;
  uint32_t url = m*8 + n;

  m_ipbus->clear_write_register_buffer();
  m_ipbus->add_register_to_write( "reg.internallink_url", url );
  m_ipbus->send_transaction_to_write();

  return status;
}

/**********************************/ 
uint32_t FtkDataFormatterApi::checkModuleIDs( std::vector<uint32_t>& vec_IDs_sb, uint32_t channel,
                          const std::map<std::pair<uint32_t, uint32_t>, uint32_t>& lane_idx2mod )
/**********************************/
{
  uint32_t ret = 0;
  uint32_t n_modules_config = 0;
  bool is_duplicate = false;
  std::vector<uint32_t> vec_duplicates;
  
  // 1. check #modules in data is larger than #modules expected in configuration file
  map3_t::const_iterator itc = lane_idx2mod.begin();
  map3_t::const_iterator ite = lane_idx2mod.end();
  for (; itc != ite; itc++) {
    if(channel==itc->first.first){
      //uint32_t tmp_moduleID = getBitRange(itc->second, 0, 12);
      n_modules_config++;
    }
  }
  if(vec_IDs_sb.size() > n_modules_config)
    {
      ret = ret | 0x1;
      daq::ftk::ftkException ex1( ERS_HERE, name_ftk(), " Number of modules in DF input spy buffer  channel " + to_string(channel) + " is larger than expected in configuration file: (" + to_string(vec_IDs_sb.size()) + ", " + to_string(n_modules_config) + ")" );
      ers::warning(ex1);
    }
  
  // 2. check module IDs are duplicated in an event
  vec_duplicates.clear();
  for(uint32_t imo1=0; imo1<vec_IDs_sb.size(); imo1++)  {
    for(uint32_t imo2=imo1; imo2<vec_IDs_sb.size(); imo2++)  {
      if(imo1==imo2) continue;
      if(vec_IDs_sb.at(imo1)==vec_IDs_sb.at(imo2)) {
        vec_duplicates.push_back(vec_IDs_sb.at(imo1));
        is_duplicate = true;
      }
    }
  }
  if(is_duplicate)
    {
      ret = ret | 0x2;
      daq::ftk::ftkException ex2( ERS_HERE, name_ftk(), "Found " + to_string(vec_duplicates.size()) + " duplicated Module IDs in DF input spy buffer channel " + to_string(channel));
      ers::log(ex2); //downgrading from warnign to log not to scary RC shifter in data taking also remove the log statement below
      //for(uint32_t imo=0; imo<vec_duplicates.size(); imo++) ERS_LOG("Duplicated module ID " << vec_duplicates.at(imo) << " " << hex << vec_duplicates.at(imo) );
    }
  
  return ret;
}



/**********************************/ 
uint32_t FtkDataFormatterApi::checkModuleIDsSize( std::vector<uint32_t>& vec_IDs_sb, uint32_t channel,
                    const std::map<std::pair<uint32_t, uint32_t>, uint32_t>& lane_idx2mod )
/**********************************/
{
  uint32_t ret = 0;
  uint32_t n_modules_config = 0;

  // 1. check #modules in data is larger than #modules expected in configuration file
  map3_t::const_iterator itc = lane_idx2mod.begin();
  map3_t::const_iterator ite = lane_idx2mod.end();
  for (; itc != ite; itc++) {
    if(channel==itc->first.first){
      //uint32_t tmp_moduleID = getBitRange(itc->second, 0, 12);
      n_modules_config++;
    }
  }
  if(vec_IDs_sb.size() > n_modules_config)
    {
      ret = 1;
      throw daq::ftk::ftkException( ERS_HERE, m_name, " Number of modules in DF input spy buffer  channel " + to_string(channel) + " is larger than expected in configuration file: (" + to_string(vec_IDs_sb.size()) + ", " + to_string(n_modules_config) + ")" );
      //ers::warning(ex1);
    }
  
  return ret;
}

/**********************************/ 
uint32_t FtkDataFormatterApi::checkModuleIDsDuplicates(std::vector<uint32_t>& vec_IDs_sb, uint32_t channel)
/**********************************/
{
  uint32_t ret = 0;
  bool is_duplicate = false;
  std::vector<uint32_t> vec_duplicates;
  
  // 2. check module IDs are duplicated in an event
  vec_duplicates.clear();
  for(uint32_t imo1=0; imo1<vec_IDs_sb.size(); imo1++)  {
    for(uint32_t imo2=imo1; imo2<vec_IDs_sb.size(); imo2++)  {
      if(imo1==imo2) continue;
      if(vec_IDs_sb.at(imo1)==vec_IDs_sb.at(imo2)) {
        vec_duplicates.push_back(vec_IDs_sb.at(imo1));
        is_duplicate = true;
      }
    }
  }
  if(is_duplicate)
    {
      ret = 1;
      for(uint32_t imo=0; imo<vec_duplicates.size(); imo++) ERS_LOG(m_name << " DF input spybuffer channel " << channel << " duplicated module ID " << vec_duplicates.at(imo) << " " << hex << vec_duplicates.at(imo) );
      throw daq::ftk::ftkException( ERS_HERE, m_name, " Found " + to_string(vec_duplicates.size()) + " duplicated Module IDs in DF input spy buffer channel " + to_string(channel));
      //ers::warning(ex2);
    }
  
  return ret;
}

/**********************************/ 
uint32_t FtkDataFormatterApi::checkModuleIDsConsistency( std::vector<uint32_t>& vec_IDs_sb, uint32_t channel,
                            const std::map<std::pair<uint32_t, uint32_t>, uint32_t>& lane_idx2mod )
/**********************************/
{
  uint32_t ret = 0;
  bool is_inconsistent = true;
  std::vector<uint32_t> vec_inconsistent_modules;
  uint32_t tmp_moduleID = 0X0;

  // 3. check that all module IDs in spy buffer are also contained in configuration
  vec_inconsistent_modules.clear();
  // loop over module IDs in spy buffer
  for(uint32_t imo1 = 0; imo1 < vec_IDs_sb.size(); imo1++)  {
    is_inconsistent = true;
    //loop over module IDs in configuration    
    map3_t::const_iterator itc = lane_idx2mod.begin();
    map3_t::const_iterator ite = lane_idx2mod.end();
    for (; itc != ite; itc++) {
      if(channel == itc->first.first){
        tmp_moduleID = getBitRange(itc->second, 0, 12);
        if(tmp_moduleID ==  vec_IDs_sb.at(imo1)){
          is_inconsistent = false;
        }
      }
    }
    if( is_inconsistent ) vec_inconsistent_modules.push_back(vec_IDs_sb.at(imo1));
  }

  if(vec_inconsistent_modules.size()>0)
    {
      ret = 1;
      //for(uint32_t imo=0; imo<vec_inconsistent_modules.size(); imo++) 
      //        ERS_LOG("DF input spybuffer channel " << channel << " inconsistent module ID " << vec_inconsistent_modules.at(imo) << " " << hex << vec_inconsistent_modules.at(imo) );
      throw daq::ftk::ftkException( ERS_HERE, m_name, " Found " + to_string(vec_inconsistent_modules.size()) + " inconsistent Module IDs in DF input spy buffer channel " + to_string(channel));
      //ers::log(ex2);
    }

  return ret;
}

//===============================
bool
FtkDataFormatterApi::phase_scan_all_ch( bool fast, std::vector<uint32_t>& goodvalues, const uint32_t& numGoodDelayValues )
{
  goodvalues.clear();

  if (!fast)
  {
    //scan first
    for (uint32_t im_fpga_id=0; im_fpga_id<8; im_fpga_id++){
      const uint32_t ce_command   = (0X1 << im_fpga_id);
      const uint32_t ce_disable   = (0X0);
      const uint32_t nDelaySteps  = 32;
      
      m_ipbus->single_access_write("reg.data_checker_fmc_fpga", im_fpga_id);  
      
      int consec_good_vals = 0;
      int this_good_val = 0;
      
      for (uint32_t iDelay=0; iDelay<nDelaySteps; iDelay++) {
        m_ipbus->single_access_write("reg.reset.reset_parity_checker", 0X1);
        msleep(100);
        m_ipbus->single_access_write("reg.reset.reset_parity_checker", 0X0);
        msleep(100);
        //sleep(0.8);
        uint32_t value(0X0);
        m_ipbus->single_access_read ("reg.fmcin_parity_check_ok", value);
        bool results = ((value & (0X1 << im_fpga_id))==0X0) ? false : true;
        
        printf("IM_FPGA=%2d : ClkDelay=%2d Result=%4s (PARITY_OK=%08x) \n", 
               im_fpga_id, iDelay, results ? "GOOD" : "NG", value);
        
        m_ipbus->single_access_write("reg.fmc_config_clkdelay_ce", ce_command);
        m_ipbus->single_access_write("reg.fmc_config_clkdelay_ce", ce_disable);      
        if (results) consec_good_vals ++;
        else consec_good_vals = 0;
        //if (consec_good_vals >=4) {//previously was 6 -- changed by S.Sevova 13.05.19
        // if (consec_good_vals >=6) {//previously was 4 -- changed by L.Horyn 11.07.19
	if(consec_good_vals >= numGoodDelayValues){ //previously was 6 -- changed by S.Sevova 17.07.19
          this_good_val = iDelay - 2;
          break;
        }
      } // delay value
      ERS_LOG("IM_FPGA=" << im_fpga_id << " : good val=" << this_good_val);
      //if (consec_good_vals>=4){
      // if (consec_good_vals>=6){  //Was 4, changed by L.Horyn on 11.07.19
      if(consec_good_vals>=numGoodDelayValues){ // was 6, changed by S.Sevova on 17.07.19
        goodvalues.push_back(this_good_val);
      }
      else
      {
        daq::ftk::IOError e(ERS_HERE, m_name, "Please go back down to UNCONFIG! Couldn't find delay value for IM"+to_string(im_fpga_id));
        throw( e );
        goodvalues.push_back(0);
      }
      consec_good_vals=0;
    }
    m_goodvalues=goodvalues;
  }
  else
  {
    goodvalues=m_goodvalues;
  }

  return true;
}

void FtkDataFormatterApi::setClockPhaseConfiguration( const std::vector<uint32_t>& goodvalues )
{
  m_ipbus->single_access_write("reg.reset.reset_delay", 0X1);
  m_ipbus->single_access_write("reg.reset.disable_fmc_input", 0X1);
  m_ipbus->single_access_write("reg.reset.fmcin_logic_reset", 0X1);
  m_ipbus->single_access_write("reg.reset.main_state_machine_reset", 0X1);

  m_ipbus->single_access_write("reg.fmc_user_signal.mezzanine_reset", 0X1);
  m_ipbus->single_access_write("reg.fmc_user_signal.mezzanine_reset", 0X0);
  m_ipbus->single_access_write("reg.reset.reset_delay", 0X0);  

  for (uint32_t im_fpga_id=0; im_fpga_id<NUMBER_OF_IM_FPGAS_PER_DF; im_fpga_id++){
    ERS_LOG( "FPGA " << im_fpga_id << " good values for this FPGA " << goodvalues.at(im_fpga_id) );
    clock_phase_configuration( im_fpga_id, 0, goodvalues.at( im_fpga_id ) );
    msleep(100);
  }

  m_ipbus->single_access_write("reg.reset.disable_fmc_input", 0X0);
  m_ipbus->single_access_write("reg.reset.fmcin_logic_reset", 0X0);
  m_ipbus->single_access_write("reg.reset.main_state_machine_reset", 0X0);
  m_ipbus->single_access_write("reg.reset.counter_parameter_reset", 0X1);
  m_ipbus->single_access_write("reg.reset.counter_parameter_reset", 0X0);
  spy_reset();
}

bool FtkDataFormatterApi::spy_dump_individual_lane(const uint32_t& lane_id,
                                                   std::vector<uint32_t>& dout)
{
  const uint32_t depth = 4096;
  bool rc = true;
  
  spy_freeze();
  std::vector<uint32_t> dump_data(depth);
  msleep(10);
  
  //sleep(1);

  uint32_t en(0X1);
  m_ipbus->single_access_write("reg.spy_readen", en);
  msleep(10);
  //  sleep(1);

  m_ipbus->single_access_write("reg.spy_laneselector", lane_id);
  //  std::cout<<"lane_id"<<lane_id<<std::endl;
  
  for (uint32_t iAddr=0; iAddr<depth; iAddr++) {
    m_ipbus->single_access_write("reg.spy_readaddr", iAddr);
    uint32_t value(0X0);

    try {
      m_ipbus->single_access_read("reg.spy_readout", value);
    } catch (daq::ftk::IPBusRead ex) {
      throw daq::ftk::IPBusIssue(ERS_HERE, m_name, " Failed to read a spybuffer word", ex);
      rc = false;
    }
    dump_data[iAddr] = value;
  }
  
  spy_restart();
  dout = dump_data;

  uint32_t disable(0X0);
   m_ipbus->single_access_write("reg.spy_readen", disable);
   
  return rc;
}

bool FtkDataFormatterApi::doReadCheck(const std::string& nodename,uint32_t& reg){
  bool rc = true;
  try {
    m_ipbus-> single_access_read(nodename, reg);
  } catch (daq::ftk::IPBusRead ex) {
    throw daq::ftk::IPBusIssue(ERS_HERE, m_name, " Failed to read " + nodename, ex);
    rc = false;
  }
  return rc;
}

//==============================
bool FtkDataFormatterApi::read_switch_fifo_status(const uint32_t& switch_type, // so far only output switch to AUX/SSB is monitored
                                                  const uint32_t& element_row,
                                                  const uint32_t& element_column,
                                                  uint32_t& input1_input_empty,
                                                  uint32_t& input1_input_full,
                                                  uint32_t& input1_output1_rdy,
                                                  uint32_t& input1_output2_rdy,
                                                  uint32_t& input1_output1_being_sent,
                                                  uint32_t& input1_output2_being_sent,
                                                  uint32_t& input2_input_empty,
                                                  uint32_t& input2_input_full,
                                                  uint32_t& input2_output1_rdy,
                                                  uint32_t& input2_output2_rdy,
                                                  uint32_t& input2_output1_being_sent,
                                                  uint32_t& input2_output2_being_sent,
                                                  uint32_t& output1_input1_empty,
                                                  uint32_t& output1_input2_empty,
                                                  uint32_t& output1_input1_full,
                                                  uint32_t& output1_input2_full,
                                                  uint32_t& output1_input1_being_read,
                                                  uint32_t& output1_input2_being_read,
                                                  uint32_t& output1_output_rdy,
                                                  uint32_t& output2_input1_empty,
                                                  uint32_t& output2_input2_empty,
                                                  uint32_t& output2_input1_full,
                                                  uint32_t& output2_input2_full,
                                                  uint32_t& output2_input1_being_read,
                                                  uint32_t& output2_input2_being_read,
                                                  uint32_t& output2_output_rdy
                                                  )
{

  bool rc = true;
  m_ipbus->single_access_write("reg.switch_selector_for_fifo_monitoring.type", switch_type);
  m_ipbus->single_access_write("reg.switch_selector_for_fifo_monitoring.column", element_column);
  m_ipbus->single_access_write("reg.switch_selector_for_fifo_monitoring.row",    element_row);

  rc = rc &  doReadCheck("reg.switch_fifo_monitoring.input1_input_empty", input1_input_empty);
  rc = rc &  doReadCheck("reg.switch_fifo_monitoring.input1_input_full", input1_input_full);
  rc = rc &  doReadCheck("reg.switch_fifo_monitoring.input1_output1_rdy", input1_output1_rdy);
  rc = rc &  doReadCheck("reg.switch_fifo_monitoring.input1_output2_rdy", input1_output2_rdy);
  rc = rc &  doReadCheck("reg.switch_fifo_monitoring.input1_output1_being_sent", input1_output1_being_sent);
  rc = rc &  doReadCheck("reg.switch_fifo_monitoring.input1_output2_being_sent", input1_output2_being_sent);
  rc = rc &  doReadCheck("reg.switch_fifo_monitoring.input2_input_empty", input2_input_empty);
  rc = rc &  doReadCheck("reg.switch_fifo_monitoring.input2_input_full", input2_input_full);
  rc = rc &  doReadCheck("reg.switch_fifo_monitoring.input2_output1_rdy", input2_output1_rdy);
  rc = rc &  doReadCheck("reg.switch_fifo_monitoring.input2_output2_rdy", input2_output2_rdy);
  rc = rc &  doReadCheck("reg.switch_fifo_monitoring.input2_output1_being_sent", input2_output1_being_sent);
  rc = rc &  doReadCheck("reg.switch_fifo_monitoring.input2_output2_being_sent", input2_output2_being_sent);
  rc = rc &  doReadCheck("reg.switch_fifo_monitoring.output1_input1_empty", output1_input1_empty);
  rc = rc &  doReadCheck("reg.switch_fifo_monitoring.output1_input2_empty", output1_input2_empty);
  rc = rc &  doReadCheck("reg.switch_fifo_monitoring.output1_input1_full", output1_input1_full);
  rc = rc &  doReadCheck("reg.switch_fifo_monitoring.output1_input2_full", output1_input2_full);
  rc = rc &  doReadCheck("reg.switch_fifo_monitoring.output1_input1_being_read", output1_input1_being_read);
  rc = rc &  doReadCheck("reg.switch_fifo_monitoring.output1_input2_being_read", output1_input2_being_read);
  rc = rc &  doReadCheck("reg.switch_fifo_monitoring.output1_output_rdy", output1_output_rdy);
  rc = rc &  doReadCheck("reg.switch_fifo_monitoring.output2_input1_empty", output2_input1_empty);
  rc = rc &  doReadCheck("reg.switch_fifo_monitoring.output2_input2_empty", output2_input2_empty);
  rc = rc &  doReadCheck("reg.switch_fifo_monitoring.output2_input1_full", output2_input1_full);
  rc = rc &  doReadCheck("reg.switch_fifo_monitoring.output2_input2_full", output2_input2_full);
  rc = rc &  doReadCheck("reg.switch_fifo_monitoring.output2_input1_being_read", output2_input1_being_read);
  rc = rc &  doReadCheck("reg.switch_fifo_monitoring.output2_input2_being_read", output2_input2_being_read);
  rc = rc &  doReadCheck("reg.switch_fifo_monitoring.output2_output_rdy", output2_output_rdy);

  return rc;

}


//==============================
bool FtkDataFormatterApi::read_switch_fifo_fullness(const uint32_t& switch_type, // so far only output switch to AUX/SSB is monitored
                                              const uint32_t& element_row,
                                              const uint32_t& element_column,
                                              uint32_t& input1_input_full,
                                              uint32_t& input2_input_full,
                                              uint32_t& output1_input1_full,
                                              uint32_t& output1_input2_full,
                                              uint32_t& output2_input1_full,
                                              uint32_t& output2_input2_full
                                            )
{
  bool rc = true;

  m_ipbus->single_access_write("reg.switch_selector_for_fifo_monitoring.type", switch_type);
  m_ipbus->single_access_write("reg.switch_selector_for_fifo_monitoring.column", element_column);
  m_ipbus->single_access_write("reg.switch_selector_for_fifo_monitoring.row",    element_row);
  rc = rc &  doReadCheck("reg.switch_fifo_monitoring.input1_input_full", input1_input_full);
  rc = rc &  doReadCheck("reg.switch_fifo_monitoring.input2_input_full", input2_input_full);
  rc = rc &  doReadCheck("reg.switch_fifo_monitoring.output1_input1_full", output1_input1_full);
  rc = rc &  doReadCheck("reg.switch_fifo_monitoring.output1_input2_full", output1_input2_full);
  rc = rc &  doReadCheck("reg.switch_fifo_monitoring.output2_input1_full", output2_input1_full);
  rc = rc &  doReadCheck("reg.switch_fifo_monitoring.output2_input2_full", output2_input2_full);

  return rc;
}


//==============================
bool FtkDataFormatterApi::read_switch_fifo_emptiness(const uint32_t& switch_type, // so far only output switch to AUX/SSB is monitored
                                                     const uint32_t& element_row,
                                                     const uint32_t& element_column,
                                                     uint32_t& input1_input_empty,
                                                     uint32_t& input2_input_empty,
                                                     uint32_t& output1_input1_empty,
                                                     uint32_t& output1_input2_empty,
                                                     uint32_t& output2_input1_empty,
                                                     uint32_t& output2_input2_empty
                                                     )
{

  bool rc = true;

  m_ipbus->single_access_write("reg.switch_selector_for_fifo_monitoring.type", switch_type);
  m_ipbus->single_access_write("reg.switch_selector_for_fifo_monitoring.column", element_column);
  m_ipbus->single_access_write("reg.switch_selector_for_fifo_monitoring.row",    element_row);
  rc = rc &  doReadCheck("reg.switch_fifo_monitoring.input1_input_empty", input1_input_empty);
  rc = rc &  doReadCheck("reg.switch_fifo_monitoring.input2_input_empty", input2_input_empty);
  rc = rc &  doReadCheck("reg.switch_fifo_monitoring.output1_input1_empty", output1_input1_empty);
  rc = rc &  doReadCheck("reg.switch_fifo_monitoring.output1_input2_empty", output1_input2_empty);
  rc = rc &  doReadCheck("reg.switch_fifo_monitoring.output2_input1_empty", output2_input1_empty);
  rc = rc &  doReadCheck("reg.switch_fifo_monitoring.output2_input2_empty", output2_input2_empty);

  return rc;
}

//==============================
bool FtkDataFormatterApi::get_fw_version()
{

  bool rc = true;

  uint32_t fw_version(0X0);
  
  try {
    m_ipbus->single_access_read ("reg.firmware_version", fw_version);
  } catch (daq::ftk::IPBusRead ex) {
    throw daq::ftk::IPBusIssue(ERS_HERE, m_name, " Failed to read a fifo word", ex);
    rc = false;
  }

  printf("Firmware Version=%2d   (0X%08x) \n", fw_version,fw_version);

  return rc;
}


//==============================
bool FtkDataFormatterApi::read_fifo_full_counter()
{
  bool rc = true;

  uint32_t counter_ch0(0X0);
  rc &= doReadCheck("reg.fmc_fifo_full_ch0", counter_ch0);
  printf("FifoCounter_FPGA0=%2d\n", counter_ch0);
  uint32_t counter_ch1(0X0);
  rc &= doReadCheck("reg.fmc_fifo_full_ch1", counter_ch1);
  printf("FifoCounter_FPGA1=%2d\n", counter_ch1);
  uint32_t counter_ch2(0X0);
  rc &= doReadCheck("reg.fmc_fifo_full_ch2", counter_ch2);
  printf("FifoCounter_FPGA2=%2d\n", counter_ch2);
  uint32_t counter_ch3(0X0);
  rc &= doReadCheck("reg.fmc_fifo_full_ch3", counter_ch3);
  printf("FifoCounter_FPGA3=%2d\n", counter_ch3);
  uint32_t counter_ch4(0X0);
  rc &= doReadCheck("reg.fmc_fifo_full_ch4", counter_ch4);
  printf("FifoCounter_FPGA4=%2d\n", counter_ch4);
  uint32_t counter_ch5(0X0);
  rc &= doReadCheck("reg.fmc_fifo_full_ch5", counter_ch5);
  printf("FifoCounter_FPGA5=%2d\n", counter_ch5);
  uint32_t counter_ch6(0X0);
  rc &= doReadCheck("reg.fmc_fifo_full_ch6", counter_ch6);
  printf("FifoCounter_FPGA6=%2d\n", counter_ch6);
  uint32_t counter_ch7(0X0);
  rc &= doReadCheck("reg.fmc_fifo_full_ch7", counter_ch7);
  printf("FifoCounter_FPGA7=%2d\n", counter_ch7);
  
  return rc;

}

//======================================================================
// this function needs to be maintained carefully
//======================================================================
bool
FtkDataFormatterApi::usedOutputLane(const int& iOutLane, 
                                    const bool& UsedInTopTower, 
                                    const bool& UsedInBotTower, 
                                    const int& UsedDFOutputLane)
{
  if ((iOutLane%18) < 16) { // AUX channel
    //printf("dbg> %d %d %d %5s %5s [%d]\n", iOutLane, UsedDFOutputLane, ((iOutLane%17)%8), UsedInTopTower ? "TRUE" : "FALSE", UsedInBotTower ? "TRUE" : "FALSE", __LINE__);
    if ( UsedDFOutputLane!= ((iOutLane%18)%8) ) return false;
    if ( iOutLane < 16) return UsedInTopTower; // top tower
    else                return UsedInBotTower; // bottom tower
    
  } else {
    if (UsedDFOutputLane!=8) return false;
    if (iOutLane==16 or iOutLane==34) return UsedInTopTower;
    if (iOutLane==17 or iOutLane==35) return UsedInBotTower;
  }
  return true;
}
