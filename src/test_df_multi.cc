// written by yasuyuki.okumura@cern.ch 

#include <DataFormatter/df_ipbus_access_multi_board_ok.hh>
#include <uhal/log/log.hpp>

#include <fstream>
#include <string>
#include <sstream>

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <time.h>

using namespace uhal;

void standard_configuration(df_ipbus_access_multi_board_ok& df, 
			    const bool& use_default_setup_for_fermilab_test_mezzanine, 
			    const std::vector<std::string>& configuration_filename,
			    const std::vector<std::string>& transceiver_configuration_filename);

// ===================================================
void set_parameters(df_ipbus_access_multi_board_ok& df,
		    const std::string& deviceId,
		    const std::string& configuration_filename);

// ===================================================
void set_transceiver_parameters(df_ipbus_access_multi_board_ok& df,
				const std::string& deviceId,
				const std::string& transceiver_configuration_filename);

// ===================================================
void retrieve_configuration(df_ipbus_access_multi_board_ok& df, 
			    const std::string& deviceId,
			    const std::string& configuration_filename,
			    const std::string& transceiver_configuration_filename);

// ===================================================
int main()
{
  uhal::setLogLevelTo ( Warning() );
  
  std::string connection_file("file:///tbed/ftk/integrationtest2014/FTKDFIpbus/config/connections.xml");

  std::vector<std::string> device_ids;
  device_ids.push_back("DF1"); 
  device_ids.push_back("DF4"); 
  
  // if you change the file, updated here and make at cmt directory
  std::pair<std::string, std::string> confFileDF1("DF1", "/tbed/ftk/integrationtest2014/FTKDFIpbus/config/configuration_sp00.txt");
  std::pair<std::string, std::string> confFileDF4("DF4", "/tbed/ftk/integrationtest2014/FTKDFIpbus/config/configuration_sp04.txt");
  std::pair<std::string, std::string> transceiverFileDF1("DF1", "/tbed/ftk/integrationtest2014/FTKDFIpbus/config/transceiver_configuration_2DF_Test_Slot3.txt");
  std::pair<std::string, std::string> transceiverFileDF4("DF4", "/tbed/ftk/integrationtest2014/FTKDFIpbus/config/transceiver_configuration_2DF_Test_Slot4.txt");
  
  std::map<std::string, std::string> configuration_filenames;
  std::map<std::string, std::string> transceiver_configuration_filenames;
  configuration_filenames.insert(confFileDF1);
  configuration_filenames.insert(confFileDF4);
  transceiver_configuration_filenames.insert(transceiverFileDF1);
  transceiver_configuration_filenames.insert(transceiverFileDF4);
  
  df_ipbus_access_multi_board_ok df_api(connection_file,
					device_ids);
  
  for (int iDevice=0, nDevices=device_ids.size(); iDevice<nDevices; iDevice++) {
    const std::string deviceId = device_ids.at(iDevice);
    retrieve_configuration(df_api, 
			   deviceId,
			   configuration_filenames[deviceId],
			   transceiver_configuration_filenames[deviceId]);
  }
  
  df_api.standard_reset_configuration();
}


// ===================================================
void retrieve_configuration(df_ipbus_access_multi_board_ok& df, 
			    const std::string& deviceId,
			    const std::string& configuration_filename,
			    const std::string& transceiver_configuration_filename)
{
  set_parameters            (df, deviceId, configuration_filename);
  set_transceiver_parameters(df, deviceId, transceiver_configuration_filename);
}

// ===================================================
void 
set_parameters(df_ipbus_access_multi_board_ok& df,
	       const std::string& deviceId,
	       const std::string& configuration_filename
	       )
{
  uint32_t this_board_mask(0X00000001);
  std::map<uint32_t, uint32_t> imfpga2clkinv;
  std::map<uint32_t, uint32_t> imfpga2clkdelay;  
  uint32_t enable_fmc_lanes_mask(0X0001);
  uint32_t this_board_mask_internal_link_input(0XFFFF);
  bool use_different_this_board_mask_for_internal_link_input(false);
  std::map<uint32_t, uint32_t> fmcin2nummodules;    
  std::map<std::pair<uint32_t, uint32_t>, uint32_t> lane_mod2idx;
  std::map<std::pair<uint32_t, uint32_t>, uint32_t> lane_idx2mod;
  std::map<uint32_t, uint32_t> pixmod2dst;
  std::map<uint32_t, uint32_t> pixmod2ftkplane;
  std::map<uint32_t, uint32_t> pixmod2tower;
  std::map<uint32_t, uint32_t> sctmod2dst;
  std::map<uint32_t, uint32_t> sctmod2ftkplane;
  std::map<uint32_t, uint32_t> sctmod2tower;
  std::map<uint32_t, uint32_t> slinkout2nummodules;
  std::map<uint32_t, uint32_t> centralswlaneid2destinationmask;
  
  std::ifstream configuration_file(configuration_filename.c_str());
  if (not (configuration_file.is_open())) {
    fprintf(stderr, "failed in open %s\n", configuration_filename.c_str());
    exit(EXIT_FAILURE);
  }   
  
  std::string       line_str;  
  while (getline(configuration_file, line_str))  {
    std::stringstream line_ss(line_str);
    std::string       c1, c2, c3, c4;
    line_ss >> c1 >> c2 >> c3 >> c4;
    
    if (c1.find("#")!=std::string::npos) {
      continue;
    }
    
    if (c1=="this_board_mask") {
      this_board_mask = strtoul(c2.c_str(), NULL, 0);
    }

    else if (c1=="this_board_mask_internal_link_input") {
      use_different_this_board_mask_for_internal_link_input = true;
      this_board_mask_internal_link_input = strtoul(c2.c_str(), NULL, 0);
    }
    
    else if (c1=="imfpga2clkinv") {
      imfpga2clkinv.insert(std::pair<uint32_t, uint32_t>(strtoul(c2.c_str(), NULL, 0),
							 strtoul(c3.c_str(), NULL, 0)));
    }
      
    else if (c1=="imfpga2clkdelay") {
      imfpga2clkdelay.insert(std::pair<uint32_t, uint32_t>(strtoul(c2.c_str(), NULL, 0),
							   strtoul(c3.c_str(), NULL, 0)));
    }
      
    else if (c1=="enable_fmc_lanes_mask") {
      enable_fmc_lanes_mask = strtoul(c2.c_str(), NULL, 0);
    }
      
    else if (c1=="fmcin2nummodules") {
      fmcin2nummodules.insert(std::pair<uint32_t, uint32_t>(strtoul(c2.c_str(), NULL, 0),
							    strtoul(c3.c_str(), NULL, 0)));
    }
      
    else if (c1=="lane_mod2idx") {
      lane_mod2idx.insert(std::pair<std::pair<uint32_t, uint32_t>, uint32_t>
			  (std::pair<uint32_t, uint32_t>(strtoul(c2.c_str(), NULL, 0),
							 strtoul(c3.c_str(), NULL, 0)),
			   strtoul(c4.c_str(), NULL, 0)) 
			  );
    }
      
    else if (c1=="lane_idx2mod") {
      lane_idx2mod.insert(std::pair<std::pair<uint32_t, uint32_t>, uint32_t>
			  (std::pair<uint32_t, uint32_t>(strtoul(c2.c_str(), NULL, 0),
							 strtoul(c3.c_str(), NULL, 0)),
			   strtoul(c4.c_str(), NULL, 0)) 
			  );
    }
      
    else if (c1=="pixmod2dst") {
      pixmod2dst.insert(std::pair<uint32_t, uint32_t>(strtoul(c2.c_str(), NULL, 0),
						      strtoul(c3.c_str(), NULL, 0)));
    }
      
    else if (c1=="pixmod2ftkplane") {
      pixmod2ftkplane.insert(std::pair<uint32_t, uint32_t>(strtoul(c2.c_str(), NULL, 0),
							   strtoul(c3.c_str(), NULL, 0)));
    }
      
    else if (c1=="pixmod2tower") {
      pixmod2tower.insert(std::pair<uint32_t, uint32_t>(strtoul(c2.c_str(), NULL, 0),
							strtoul(c3.c_str(), NULL, 0)));
    }
      
    else if (c1=="sctmod2dst") {
      sctmod2dst.insert(std::pair<uint32_t, uint32_t>(strtoul(c2.c_str(), NULL, 0),
						      strtoul(c3.c_str(), NULL, 0)));
    }
      
    else if (c1=="sctmod2ftkplane") {
      sctmod2ftkplane.insert(std::pair<uint32_t, uint32_t>(strtoul(c2.c_str(), NULL, 0),
							   strtoul(c3.c_str(), NULL, 0)));
    }
      
    else if (c1=="sctmod2tower") {
      sctmod2tower.insert(std::pair<uint32_t, uint32_t>(strtoul(c2.c_str(), NULL, 0),
							strtoul(c3.c_str(), NULL, 0)));
    }
      
    else if (c1=="slinkout2nummodules") {
      slinkout2nummodules.insert(std::pair<uint32_t, uint32_t>(strtoul(c2.c_str(), NULL, 0),
							       strtoul(c3.c_str(), NULL, 0)));
    }
    else if (c1=="centralswlaneid2destinationmask") {
      centralswlaneid2destinationmask.insert(std::pair<uint32_t, uint32_t>(strtoul(c2.c_str(), NULL, 0),
									   strtoul(c3.c_str(), NULL, 0)));
    }
    else if (c1=="") {
    }
    
    else {
      fprintf(stderr, "undefined name = %s\n", c1.c_str());
      exit(EXIT_FAILURE);
    }
  }
  
  if (use_different_this_board_mask_for_internal_link_input) {
    // special setting for single board testing
    df.set_this_board_mask_special_for_test(deviceId, 
					    this_board_mask, 
					    this_board_mask_internal_link_input);
  }
  else  {
    // default
    df.set_this_board_mask(deviceId, this_board_mask); 
  }
  df.set_imfpga2clkinv(deviceId, imfpga2clkinv);
  df.set_imfpga2clkdelay(deviceId, imfpga2clkdelay);
  df.set_enable_fmc_lanes_mask(deviceId, enable_fmc_lanes_mask);
  df.set_fmcin2nummodules(deviceId, fmcin2nummodules);
  df.set_lane_mod2idx(deviceId, lane_mod2idx);
  df.set_lane_idx2mod(deviceId, lane_idx2mod);
  df.set_pixmod2dst(deviceId, pixmod2dst);
  df.set_pixmod2ftkplane(deviceId, pixmod2ftkplane);
  df.set_pixmod2tower(deviceId, pixmod2tower);    
  df.set_sctmod2dst(deviceId, sctmod2dst);
  df.set_sctmod2ftkplane(deviceId, sctmod2ftkplane);
  df.set_sctmod2tower(deviceId, sctmod2tower);
  df.set_slinkout2nummodules(deviceId, slinkout2nummodules);
  df.set_centralswlaneid2destinationmask(deviceId, centralswlaneid2destinationmask);
}

// ===================================================
void 
set_transceiver_parameters(df_ipbus_access_multi_board_ok& df,
			   const std::string& deviceId,
			   const std::string& transceiver_configuration_filename)
{
  std::map<uint32_t, uint32_t> gtch2rxpolarity;
  std::map<uint32_t, uint32_t> gtch2txpolarity;
  std::map<uint32_t, uint32_t> gtch2force_ready_mode;
  std::map<uint32_t, uint32_t> gtch2to_altera_fpga;
  
  std::ifstream transceiver_configuration_file(transceiver_configuration_filename.c_str());
  if (not (transceiver_configuration_file.is_open())) {
    fprintf(stderr, "failed in open %s\n", transceiver_configuration_filename.c_str());
    exit(EXIT_FAILURE);
  }   
  
  std::string       line_str;  
  while (getline(transceiver_configuration_file, line_str))  {
    std::stringstream line_ss(line_str);
    std::string       c1, c2, c3, c4;
    line_ss >> c1 >> c2 >> c3 >> c4;
    
    if (c1.find("#")!=std::string::npos) {
      continue;
    }
    
    if (c1=="gtch2rxpolarity") {
      gtch2rxpolarity.insert(std::pair<uint32_t, uint32_t>(strtoul(c2.c_str(), NULL, 0),
							   strtoul(c3.c_str(), NULL, 0)));
    } else if (c1=="gtch2txpolarity") {
      gtch2txpolarity.insert(std::pair<uint32_t, uint32_t>(strtoul(c2.c_str(), NULL, 0),
							   strtoul(c3.c_str(), NULL, 0)));
    } else if (c1=="gtch2force_ready_mode") {
      gtch2force_ready_mode.insert(std::pair<uint32_t, uint32_t>(strtoul(c2.c_str(), NULL, 0),
								 strtoul(c3.c_str(), NULL, 0)));
    } else if (c1=="gtch2to_altera_fpga") {
      gtch2to_altera_fpga.insert(std::pair<uint32_t, uint32_t>(strtoul(c2.c_str(), NULL, 0),
							       strtoul(c3.c_str(), NULL, 0)));
    } else if (c1=="") {
    } else {
      fprintf(stderr, "undefined name = %s\n", c1.c_str());
      exit(EXIT_FAILURE);
    }
  }  
  
  df.set_gtch2rxpolarity(deviceId, gtch2rxpolarity);
  df.set_gtch2txpolarity(deviceId, gtch2txpolarity);
  df.set_gtch2force_ready_mode(deviceId, gtch2force_ready_mode);
  df.set_gtch2to_altera_fpga(deviceId, gtch2to_altera_fpga);
}
