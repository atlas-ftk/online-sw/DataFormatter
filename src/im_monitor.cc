// written by mykhailo.lisovyi@cern.ch 
#include <DataFormatter/smon_components.h>
//#include <DataFormatter/StatusRegisterIS.h>
//#include <DataFormatter/dal/DataFormatterNamed.h>
#include <ftkcommon/StatusRegister/StatusRegisterISVector.h>



#include <string>
#include <fstream>
#include <sstream>

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <time.h>

#include <sys/stat.h>

#include "ftkcommon/EventFragmentCollection.h"
#include "ftkcommon/EventFragment.h"
#include "ftkcommon/EventFragmentFTKPacket.h"
#include "ftkcommon/EventFragmentHitClusters.h"

#include "ftkcommon/SourceIDSpyBuffer.h"
#include <ftkcommon/StatusRegister/StatusRegisterISVector.h>
#include <ftkcommon/Utils.h>

// ers
#include <ers/ers.h>
#include "ftkcommon/exceptions.h"
#include "ftkcommon/core.h"

// emon
#include <emon/EventIterator.h>
#include <eformat/ROBFragmentNoTemplates.h>
#include <eformat/FullEventFragmentNoTemplates.h>
#include <eformat/Issue.h>

// ipc
#include <ipc/core.h>
#include <ipc/partition.h>


using namespace std;
using namespace daq::ftk;

void sb_dump_all(const emon::Event& ee, uint32_t boardNumber);

//============================================================
void print_usage () {
  printf("-h          : produce help message \n");
  printf("-M          : mode (see below) \n");
  printf("-D <string> : device ID  \n");  
  printf("-p <string> : [optional] RunControl Partition to be monitored (FTK_DF is default)  \n");  
  printf("-I <int>    : [optional] Number of iterations  (1  is default)  \n");
  printf("-R <int>    : [optional] Shelf ID \n");
  printf("-l <int>    : [optional] Slot ID \n");
  printf("-n <string> : [optional] RCD name that publishes to emon (FTK-RCD-AtcaCrate-1 is default)  \n");
  printf("-t <int>    : [optional] Period of iterations  (30 min is default)  \n");
  printf("-A <int>    : [optional] is New FW treatment?(default is 0, Old:0, New:1) \n");
  printf("----------------------------------- \n");
  printf("MODE options \n");
  printf("  1: Readback All IM/DF SPY buffer   -R <Shelf ID (1-4)> -l <Slot ID (3-10)> \n");
  printf("  2: Readback All IM Status Monitor  -R <Shelf ID (1-4)> -l <Slot ID (3-10)> \n");
  printf("  3: Readback All IM FW Versions     -R <Shelf ID (1-4)> -l <Slot ID (3-10)> \n");
}

void sb_dump_all(const emon::Event& ee, uint32_t boardNumber){
  std::cout << setfill( '0' );
  
  const unsigned int * data;
  uint32_t ndata=0;
  
  eformat::read::FullEventFragment fe( ee.data() );
  try {
    fe.check();
  } catch (eformat::Issue ex) {
    throw ex;
  }
  ERS_LOG("Event fragment contains " + to_string(fe.nchildren()) + " children" );
  
  daq::ftk::Position position;
  
  
  std::vector< std::vector< uint32_t > > ispy_buffers;
  std::vector< std::vector< uint32_t > > ospy_buffers;
  std::vector< uint32_t > channels_ispy;
  std::vector< uint32_t > channels_ospy;
  
  
  // Input Mezzanine
  for(uint32_t inout = 0; inout < 2; inout++){
    position = ( inout == 0) ? daq::ftk::Position::IN : daq::ftk::Position::OUT;
    
    for(uint32_t ichannel = 0; ichannel < 16; ichannel++){
      for(uint32_t child_id = 0; child_id < fe.nchildren(); child_id++){
        
        eformat::read::ROBFragment rob( fe.child(child_id) );
        SourceIDSpyBuffer decoded_rob_sourceID = decode_SourceIDSpyBuffer( rob.source_id() );
        if(decoded_rob_sourceID.boardType == daq::ftk::BoardType::IM && decoded_rob_sourceID.boardNumber == boardNumber && decoded_rob_sourceID.boardInternal == 16*inout + ichannel && decoded_rob_sourceID.position == position){
          
          data  = rob.rod_data();
          ndata = rob.rod_ndata();
          
          std::vector< uint32_t > this_spy_buffer;
          for(uint32_t idata = 0; idata < ndata; idata++){
            this_spy_buffer.push_back( data[idata] );
          }
          
          if( position == daq::ftk::Position::IN ){
            channels_ispy.push_back( ichannel );
            ispy_buffers.emplace_back( std::move( this_spy_buffer ) );
          }else{
            channels_ospy.push_back( ichannel );
            ospy_buffers.emplace_back( std::move( this_spy_buffer ) );
          }


        } // board, channel, inout
      } // children loop
    } // channel loop
    
  } // inout loop
  // print

  printf("Note that : 0x2e5ec0de means Reset Code, Disabled IM Spy has 0x2e5ec0de at initial value. \n");
  printf("\n");
  printf( "%26s", "channel |" );
  for( int idx = 0, size = channels_ispy.size(); idx < size; ++idx ){
    printf("       ch%02d |", channels_ispy.at(idx) );
  }
  printf("\n");
  if( ispy_buffers.size() > 0 && ispy_buffers.at(0).size() > 0 ){
    for( int idx_ispy = 0, size_ispy = ispy_buffers.at(0).size(); idx_ispy < size_ispy; ++idx_ispy ){
      printf(" IM  inspy : addr:0x%04x |", idx_ispy );
      for( int idx = 0, size = channels_ispy.size(); idx < size; ++idx ){
        printf( " 0x%08x |", ispy_buffers.at(idx).at(idx_ispy) );
      }
      printf("\n");
    }
  } // 
  
  printf("\n");
  printf( "%26s", "channel |" );
  for( int idx = 0, size = channels_ospy.size(); idx < size; ++idx ){
    printf("       ch%02d |", channels_ospy.at(idx) );
  }
  printf("\n");
  if( ospy_buffers.size() > 0 && ospy_buffers.at(0).size() > 0 ){
    for( int idx_ospy = 0, size_ospy = ospy_buffers.at(0).size(); idx_ospy < size_ospy; ++idx_ospy ){
      printf(" IM outspy : addr:0x%04x |", idx_ospy );
      for( int idx = 0, size = channels_ospy.size(); idx < size; ++idx ){
        printf( " 0x%08x |", ospy_buffers.at(idx).at(idx_ospy) );
      }
      printf("\n");
    }
  } // 
  
  
  // Data Formatter
  uint32_t read_id = 0;
  for(uint32_t inout = 0; inout < 2; inout++){
    position = ( inout == 0 ) ? daq::ftk::Position::IN : daq::ftk::Position::OUT;
    
    for(uint32_t ichannel = 0; ichannel < 52; ichannel++){
      for(uint32_t child_id = 0; child_id < fe.nchildren(); child_id++){
        
        eformat::read::ROBFragment rob( fe.child(child_id) );
        SourceIDSpyBuffer decoded_rob_sourceID = decode_SourceIDSpyBuffer( rob.source_id() );
        
        if(decoded_rob_sourceID.boardType == daq::ftk::BoardType::DF && decoded_rob_sourceID.boardNumber == boardNumber && decoded_rob_sourceID.boardInternal == ichannel && decoded_rob_sourceID.position == position){
          
          if(decoded_rob_sourceID.position == daq::ftk::Position::IN) read_id = ichannel - 36;
          else read_id = ichannel;
          
          std::cout<< "HHHHHHHHHH DF ch"  << std::dec << read_id << " HHHHHHHHHH" << std::endl;
          
          data  = rob.rod_data();
          ndata = rob.rod_ndata();
          
          for(uint32_t idata = 0; idata < ndata; idata++)
            if(position == daq::ftk::Position::IN){
              std::cout<< "[DF inspy ch" << std::dec << read_id << "] Address: " << setw(4) << idata <<" - 0x" << setw(8) << std::hex << data[idata] << std::endl;
            }else{
              std::cout<< "[DF outspy ch" << std::dec << read_id << "] Address: " << setw(4) << idata <<" - 0x" << setw(8) << std::hex << data[idata] << std::endl;
            }
          
        }// board, channel, position
      }// children loop
    }// channel loop
  }// inout loop
}

int main(int argc, char* argv[]){
  
  // bool rc = false;
  std::string device_id("");
  // bool results = true;
  int c = 0X0;
  int mode   =0X0;
  int niter = 1; // # iterations
  int titer = 30*60; // iteration period: 30 min in sec  
  uint32_t timeout    = 900;  // emon timeout in sec
  uint32_t shelf_id   = 0x0;
  uint32_t slot_id    = 0x0;
  uint32_t option_val = 0x0;
  string partitionName = "FTK_DF";
  string serverName = "DF"; //DataFlow, will be changed later to FTK* (?)
  string rcdName	= "FTK-RCD-AtcaCrate-1";
  uint32_t is_new_fw = 0x0;
  while ((c = getopt (argc, argv, "h:D:M:F:R:l:A:s:S:N:I:T:p:s:n:t:")) != -1){
    switch (c) {
    case 'M': mode          = strtoul(optarg, NULL, 0); break;
    case 'R': shelf_id      = strtoul(optarg, NULL, 0); break;
    case 'l': slot_id       = strtoul(optarg, NULL, 0); break;
    case 'I': niter         = strtoul(optarg, NULL, 0); break;
    case 'T': timeout       = strtoul(optarg, NULL, 0); break;
    case 't': titer         = strtoul(optarg, NULL, 0); break;
    case 'V': option_val    = strtoul(optarg, NULL, 0); break;
    case 'A': is_new_fw     = strtoul(optarg, NULL, 0); break;
    case 'D': device_id     = optarg; break;
    case 'p': partitionName = optarg; break;
    case 's': serverName    = optarg; break;
    case 'n': rcdName       = optarg; break;
    case 'h': print_usage(); return EXIT_SUCCESS;
    default: print_usage();  return EXIT_FAILURE;
    }
    
  }
  
  if (device_id == "" || partitionName == "" || rcdName == "" ) {
    print_usage();
    return EXIT_FAILURE;    
  }
  
  // uint32_t boardNumber = 4*(shelf_id-1) + slot_id-3;
  uint32_t boardNumber = (shelf_id-1) + 4*(slot_id-3);

  //create a partition to run in (= same as the RC partition)
  IPCPartition partition(partitionName);

  // define IS object name
  if (device_id.back() == 'D') device_id.pop_back();
  IPCCore::init(argc, argv); //initialize communication library (= ipc)
  
  if ( mode == 1 ) { // spy 
    // if ( (boardNumber == 0) ||  ( shelf_id < 1 && 4 < shelf_id ) || (slot_id < 3 && 10 < slot_id) ){ print_usage(); return EXIT_FAILURE; }
    
    // in the case of just one channel... 
    // uint32_t sourceid = daq::ftk::encode_SourceIDSpyBuffer(daq::ftk::BoardType::IM, boardNumber, channel + 16*inspy_ospy, daq::ftk::Position::IN); 
    // emon::SelectionCriteria emonCriteria( emon::L1TriggerType(0, true), emon::SmartBitValue(), emon::SmartStreamValue(), emon::StatusWord(sourceid) );
    
    // create an EventIterator instance to access events on emon
    emon::SelectionCriteria emonCriteria( emon::L1TriggerType(0, true), emon::SmartBitValue(), emon::SmartStreamValue(), emon::StatusWord() );
    emon::SamplingAddress emonAddress( "ReadoutApplication", rcdName.c_str());
    std::unique_ptr<emon::EventIterator> eit;
    try {
      eit.reset( new emon::EventIterator( partition, emonAddress, emonCriteria ));
    } catch (emon::Exception & ex) { ers::error( ex ); return 1; }
    
    for(int k = 0; k < niter; k++) {
      // read out the next event from emon (wait for the event to appear for timeout seconds)
      emon::Event event;
      try {
        printf("Waiting %i seconds for the events on EMON to appear \n",timeout);
        event = eit -> nextEvent( timeout*1000 ); 
      } catch ( emon::NoMoreEvents & ex ) { ers::warning( ex ); 
      } catch ( emon::Exception    & ex ) { throw( ex ); }
      // dump spybuffer
      sb_dump_all(event, boardNumber);
      
      if(niter > 1) {
        ERS_LOG("Sleeping for "<<titer<<" seconds...");
        sleep(titer);
      }
    } // for iter
  } else if (mode == 2 ) { // smon 
    string isObjName  = serverName + ".FTK." + device_id;

    std::vector< uint32_t > vers;
    for( int channel = 0; channel < 8; ++channel ){
      string isAttrName = "IM_fw_ver_FPGA" + std::to_string(channel) + "_sr_v";
      daq::ftk::StatusRegisterISVector* sr = new daq::ftk::StatusRegisterISVector(partition, isObjName, isAttrName, srType::srOther);
      sr->readout();
      uint32_t tmp_fw_ver = sr->getValues().at(0);
      vers.push_back( tmp_fw_ver );
      vers.push_back( tmp_fw_ver );
      delete sr;
    }
    
    std::vector< std::vector< uint32_t > > smons;
    for( int channel = 0; channel < 16; ++channel ){
      string isAttrName = "IM" + std::to_string(channel) + "_sr_v";
      daq::ftk::StatusRegisterISVector* sr = new daq::ftk::StatusRegisterISVector(partition, isObjName, isAttrName, srType::srOther);
      
      sr->readout();
      smons.push_back( sr->getValues() );
      delete sr;
    }
    
    smon_components smon_interface;
    for( int channel = 0; channel < 16; ++channel ){
      smon_interface.read_32bitdata( smons.at(channel) );
      smon_interface.read_vector();
      bool is_mew_fw_2 = ( vers.at( channel ) & 0x3FFF ) > 0x0130;
      if( is_new_fw || is_mew_fw_2 ) smon_interface.print_one_liner( channel == 0 ? true : false, channel ); 
        
    }
    
    printf("\n");
    printf("\n"); 
    printf("\n");
   
    for( int channel = 0; channel < 16; ++channel ){
      smon_interface.read_32bitdata( smons.at(channel) );
      smon_interface.read_vector();
      bool is_mew_fw_2 = ( vers.at( channel ) & 0x3FFF ) > 0x0130;
      if( is_new_fw == 0x1 || is_mew_fw_2 ){
        smon_interface.print_full( channel );
      }else{
        smon_interface.print_full_old( channel%2, channel/2 );
      }
      smon_interface.print_raw_text();
      printf("\n");
    }
    
  } else if (mode == 3 ) {
    string isObjName  = serverName + ".FTK." + device_id;

    
    for( int channel = 0; channel < 8; ++channel ){
      string isAttrName = "IM_fw_ver_FPGA" + std::to_string(channel) + "_sr_v";
      daq::ftk::StatusRegisterISVector* sr = new daq::ftk::StatusRegisterISVector(partition, isObjName, isAttrName, srType::srOther);
      sr->readout();
      uint32_t tmp_fw_ver = sr->getValues().at(0);
      if( (tmp_fw_ver & 0x3FFF) > 0x0130 ){ // new refactored FW 
        uint32_t tmp_fw_type = sr->getValues().at(1);
        uint32_t tmp_fw_hash = sr->getValues().at(2);
        uint32_t tmp_fw_date = sr->getValues().at(3);
        printf( " IM FPGA%i FW from IS: version(0x%04x), type(0x%04x), hash(0x%08x), date(0x%08x) \n", channel, tmp_fw_ver, tmp_fw_type, tmp_fw_hash, tmp_fw_date );
      }else{
        printf( " IM FPGA%i FW from IS: version(0x%04x), Non refactored FW(old) \n", channel, tmp_fw_ver );
      }
      delete sr;
    }
  } else {
    print_usage();
    return EXIT_FAILURE;
  }
  
  
  return EXIT_SUCCESS;
}
