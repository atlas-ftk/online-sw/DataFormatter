// Functions used for spy buffer access using DFApi

#include "DataFormatter/DataFormatterConstants.h"
#include "DataFormatter/IMSpyBuffer.h"
// for bit parsing
#include "ftkcommon/Utils.h" 
#include "ftkcommon/exceptions.h"
#include "ftkcommon/core.h"

#include <sstream>
#include <fstream>
#include <iostream>
#include <iomanip>
#include <algorithm>



namespace daq { 
  namespace ftk {
  
    IMSpyBuffer::IMSpyBuffer( uint32_t lane_id,
                              bool     isInspy,
			                        std::shared_ptr<FtkIMApi> im_api,
                              uint32_t useBlockTransfer):
      m_lane_id(lane_id),
      m_isInspy( isInspy ),
      m_im_api(im_api),
      m_useBlockTransfer( useBlockTransfer )
    {
    }
    
    IMSpyBuffer::~IMSpyBuffer(){}

    int IMSpyBuffer::readSpyBuffer() {
      if ( m_useBlockTransfer ) return read_spybuffer_blocktransfer();
      else return read_spybuffer();
    }

    int IMSpyBuffer::readSpyStatusRegister( uint32_t& spyStatus ) {
      // not currently implemented for IM
      return 0;
    }

    int IMSpyBuffer::read_spybuffer_blocktransfer()
    {
      int rc = m_im_api->read_spybuffer_blocktransfer( m_lane_id, m_isInspy, m_spyStatus, m_buffer );

      // IM spy status is not yet implemented, so cannot call unwrap
//      if ( !rc )
//        unwrapBuffer(); // IM api cannot unwrap buffer before the whole thing is read

      return rc;
    }
    
    
    int IMSpyBuffer::read_spybuffer()
    {
      int rc = m_im_api->read_spybuffer( m_lane_id, m_isInspy, m_spyStatus, m_buffer );

      // IM spy status is not yet implemented, so cannot call unwrap
//      if ( !rc ) 
//        unwrapBuffer(); // IM api cannot unwrap buffer before the whole thing is read
      return rc;
    }


  } // namespace ftk
} // namespace daq


