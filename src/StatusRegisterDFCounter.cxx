#include <DataFormatter/StatusRegisterDFCounter.h>
#include "ftkcommon/exceptions.h"
#include "ftkcommon/Utils.h"

using namespace daq::ftk;

StatusRegisterDFCounter::StatusRegisterDFCounter(std::shared_ptr<FtkDataFormatterApi> dfapi, 
			const srType& type, 
			const uint32_t& valueSelector)
: StatusRegisterDFCounter::StatusRegisterDFSelector(dfapi, type)
{
  initialize_selector(getBitRange(valueSelector, 16, 18), getBitRange(valueSelector, 0,  15));
}

// ====================================================
StatusRegisterDFCounter::StatusRegisterDFCounter(std::shared_ptr<FtkDataFormatterApi> dfapi, 
			const srType& type, 
			const uint32_t& typeSelector, const uint32_t& laneSelector)
: StatusRegisterDFCounter::StatusRegisterDFSelector(dfapi, type)
{
  initialize_selector(typeSelector, laneSelector);
}

// ====================================================
void
StatusRegisterDFCounter::initialize_selector(const uint32_t& typeSelector, const uint32_t& laneSelector)
{
  m_write_nodes.push_back("reg.readout_32bit_counter_lane_selector.type");
  m_write_nodes.push_back("reg.readout_32bit_counter_lane_selector.lane");

  m_write_values.push_back(typeSelector & 0x7);
  m_write_values.push_back(laneSelector & 0xFFFF);

  m_node	  = "reg.readout_32bit_counter";
}

