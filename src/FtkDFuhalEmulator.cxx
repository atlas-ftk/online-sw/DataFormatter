
#include "DataFormatter/FtkDFuhalEmulator.h"

using namespace uhal;
using namespace uhal::tests;

void FtkDFuhalEmulator::run ()
{
  while ( true )
  {
    uint32_t lBytes = mSocket.receive_from ( boost::asio::buffer ( & ( mReceive[0] ), mReceive.size() << 2 ) , mSenderEndpoint );
    mReply.clear();

    log ( Notice(), "\n=============================== Receiving ==============================" );
    std::vector<uint32_t>::const_iterator lBegin, lEnd;
    lBegin = mReceive.begin(); lEnd = mReceive.begin() + ( lBytes>>2 );
    for ( ; lBegin != lEnd; ++lBegin ) {
      log ( Debug(), Integer( *lBegin, IntFmt<integer_base::hex,integer_format::fixed>() ) );
    }

    AnalyzeReceivedAndCreateReply ( lBytes );

    if ( mReply.size() ) {
      log ( Notice(), "Now replying. Reply size: ", Integer( mReply.size() ) );
      log ( Notice(), "\n==================================== Sending ============================" );
      //TargetToHostInspector< IPbus_major, IPbus_minor > lTargetToHostDebugger;
      lBegin = mReply.begin();
      lEnd = mReply.end();
      //lTargetToHostDebugger.analyze( lBegin, lEnd );
      for ( ; lBegin != lEnd; ++lBegin ) {
        log ( Debug(), Integer( *lBegin, IntFmt<integer_base::hex,integer_format::fixed>() ) );
      }
      mSocket.send_to ( boost::asio::buffer ( & ( mReply[0] ) , mReply.size()<<2 ) , mSenderEndpoint );
      log ( Notice(), "Finished replying." );
    }
  }
}

// see uhal/tests/DummyHardware.h (AnalyzeReceivedAndCreateReply)
void FtkDFuhalEmulator::AnalyzeReceivedAndCreateReply ( const uint32_t& aByteCount )
{
  bool is_status_request = ( *mReceive.begin() == 0xF1000020 );
  bool is_resend_request = ( ( *mReceive.begin() & 0xFF0000FF ) == 0xF2000020 );

  if ( mBigEndianHack || is_status_request || is_resend_request )
  {
    for ( std::vector<uint32_t>::iterator lIt ( mReceive.begin() ) ; lIt != mReceive.end() + ( aByteCount>>2 ) ; ++lIt )
      *lIt = ntohl ( *lIt );
  }

  std::vector<uint32_t>::const_iterator lBegin, lEnd;

  lBegin = mReceive.begin(); lEnd = mReceive.begin() + ( aByteCount>>2 );
  if ( ! analyze( lBegin, lEnd ) ) { // received bad header
    log ( Error(), "Found a bad header" );
    mReply.push_back ( IPbus < IPbus_major, IPbus_minor >::ExpectedHeader ( mType, mWordCounter, mTransactionId , 1 ) );
  }

  if ( ( mPacketType == 0 ) && ( mReply.size() != 0 ) ) {
    mReplyHistory.push_back ( std::make_pair ( mPacketCounter , mReply ) );
    mReplyHistory.pop_front();
  }

  if ( mReplyDelay ) {
    log ( Info(), "Sleeping for ", Integer ( mReplyDelay ) , "s" );
    sleep ( mReplyDelay );
    mReplyDelay = 0;
    log ( Info(), "Now replying " );
  }

  if ( mBigEndianHack || mPacketType == 1 ) {
    for ( std::vector<uint32_t>::iterator lIt (mReply.begin()) ; lIt != mReply.end() ; ++lIt ) 
      *lIt = htonl ( *lIt );
  }
}

// see uhal/TemplateDefinitions/IPbusInspector.hxx (analyze)
bool FtkDFuhalEmulator::analyze ( std::vector<uint32_t>::const_iterator& aIt , const std::vector<uint32_t>::const_iterator& aEnd, const bool& aContinueOnError )
{
//  for ( std::vector<uint32_t>::const_iterator lIt ( aIt ); lIt != aEnd; ++lIt )
//    log ( Debug, Integer ( *lIt, IntFmt<integer_base::hex,integer_format::fixed>() ) );

  uint32_t lAddress, lAddend, lAndTerm, lOrTerm ;
  std::vector<uint32_t>::const_iterator lPayloadBegin, lPayloadEnd;

  mPacketHeader = *aIt++;
  mPacketCounter = ( mPacketHeader>>8) &0xFFFF ;
  mPacketType = mPacketHeader&0x0F ;

  switch ( mPacketType )
  {
    case 0:
      if ( !control_packet_header () )
        return false;
      do {
        mHeader = *aIt++;
        if ( ! IPbus< IPbus_major, IPbus_minor >::ExtractHeader ( mHeader,
                                              mType,
                                              mWordCounter,
                                              mTransactionId,
                                              mResponseGood )
           ) {
          log ( Error(), "Unable to parse send header ", Integer ( mHeader, IntFmt<integer_base::hex,integer_format::fixed>() ) );
          if ( ! aContinueOnError ) { aIt--; return true; }
          log ( Warning(), "Attempting to see if it is because the bad header was, in fact, a packet header." );
          aIt--;
          return this->analyze( aIt, aEnd );
        }

        if ( mResponseGood != 0xF ) {
          log ( Error(), "Bad InfoCode value of ", Integer ( mResponseGood ), " detected in IPbus transaction request header ", Integer ( mHeader, IntFmt<integer_base::hex,integer_format::fixed>() ) );
          return false;
        }

        switch ( mType ) {
          case B_O_T:
            bot();
            break;
          case NI_READ:
            lAddress = *aIt++;
            ni_read ( lAddress );
            break;
          case READ:
            lAddress = *aIt++;
            read ( lAddress );
            break;
          case NI_WRITE:
            lAddress = *aIt++;
            lPayloadBegin = aIt;
            lPayloadEnd = aIt + mWordCounter;
            ni_write ( lAddress, lPayloadBegin, lPayloadEnd );
            aIt += mWordCounter;
            break;
          case WRITE:
            lAddress = *aIt++;
            lPayloadBegin = aIt;
            lPayloadEnd = aIt + mWordCounter;
            write ( lAddress, lPayloadBegin, lPayloadEnd );
            aIt += mWordCounter;
            break;
          case RMW_SUM:
            lAddress = *aIt++;
            lAddend = *aIt++;
            rmw_sum ( lAddress, lAddend );
            break;
          case RMW_BITS:
            lAddress = *aIt++;
            lAndTerm = *aIt++;
            lOrTerm = *aIt++;
            rmw_bits ( lAddress, lAndTerm, lOrTerm );
            break;
          default:
            unknown_type();
            return false;
        } // end switch (mType)
      } while ( aIt != aEnd );

      break; // end case 0
    case 1:
      aIt=aEnd;
      status_packet_header();
      break;
    case 2:
      aIt=aEnd;
      resend_packet_header();
      break;
    default:
      unkown_packet_header();
      return false;
  } // end switch ( mPacketType )

  return true;
}


void FtkDFuhalEmulator::bot()
{
  log ( Notice() , Integer (mHeader, IntFmt<integer_base::hex,integer_format::fixed>() ) , " | BOT, transaction ID ", Integer ( mTransactionId ) );

  mReceivedControlPacketHeaderHistory.push_back ( mPacketHeader );
  mReceivedControlPacketHeaderHistory.pop_front();

  uint32_t lExpected ( IPbus< IPbus_major, IPbus_minor >::ExpectedHeader ( mType, 0, mTransactionId ) );
  mReply.push_back( lExpected );
  mSentControlPacketHeaderHistory.push_back ( lExpected );
  mSentControlPacketHeaderHistory.pop_front ();

  // TODO: send to DFApi?
}

void FtkDFuhalEmulator::ni_read( const uint32_t& aAddress )
{
  log ( Notice(), Integer (mHeader, IntFmt<integer_base::hex,integer_format::fixed>() ) , " | Non-incrementing read, size " , Integer ( mWordCounter ) , ". transaction ID " , Integer ( mTransactionId ) );
  log ( Notice(), Integer ( aAddress, IntFmt<integer_base::hex,integer_format::fixed>() ), " | > Address" );

  mReceivedControlPacketHeaderHistory.push_back ( mPacketHeader );
  mReceivedControlPacketHeaderHistory.pop_front();

  uint32_t lAddress ( aAddress );
  uint32_t lExpected ( IPbus< IPbus_major, IPbus_minor >::ExpectedHeader ( mType, mWordCounter, mTransactionId ) );
  mReply.push_back( lExpected );
  mSentControlPacketHeaderHistory.push_back ( lExpected );
  mSentControlPacketHeaderHistory.pop_front ();

  // read from DFApi
  uint32_t readVal;
  std::string registerName = m_dfApi->get_ipbusApi()->getRegisterNameFromAddress ( lAddress );
  for ( ; mWordCounter!=0 ; --mWordCounter ) {
    m_dfApi->get_ipbusApi()->single_access_read( registerName, readVal );
    mReply.push_back ( readVal );
  }
}

void FtkDFuhalEmulator::read ( const uint32_t& aAddress )
{
  log ( Notice(), Integer (mHeader, IntFmt<integer_base::hex,integer_format::fixed>() ) , " | incrementing read, size " , Integer ( mWordCounter ) , ". transaction ID " , Integer ( mTransactionId ) );
  log ( Notice(), Integer ( aAddress, IntFmt<integer_base::hex,integer_format::fixed>() ), " | > Address" );

  mReceivedControlPacketHeaderHistory.push_back ( mPacketHeader );
  mReceivedControlPacketHeaderHistory.pop_front();

  uint32_t lAddress ( aAddress );
  uint32_t lExpected ( IPbus< IPbus_major, IPbus_minor >::ExpectedHeader ( mType, mWordCounter, mTransactionId ) );
  mReply.push_back( lExpected );
  mSentControlPacketHeaderHistory.push_back ( lExpected );
  mSentControlPacketHeaderHistory.pop_front ();

  // read from DFApi
  uint32_t readVal;
  for ( ; mWordCounter!=0 ; --mWordCounter ) {
    std::string registerName = m_dfApi->get_ipbusApi()->getRegisterNameFromAddress ( lAddress++ );
    m_dfApi->get_ipbusApi()->single_access_read( registerName, readVal );
    mReply.push_back ( readVal );
  }
}

void FtkDFuhalEmulator::ni_write ( const uint32_t& aAddress, std::vector<uint32_t>::const_iterator& aIt, const std::vector<uint32_t>::const_iterator& aEnd )
{
  log ( Notice() , Integer ( mHeader, IntFmt<integer_base::hex,integer_format::fixed>() ) , " | Non-incrementing write, size " , Integer ( mWordCounter ) , ", transaction ID " , Integer ( mTransactionId ) );
  log ( Notice() , Integer ( aAddress, IntFmt<integer_base::hex,integer_format::fixed>() ) , " |  > Address" );
  uint32_t lCounter ( 0 );

  while ( aIt != aEnd )
  {
    log ( Notice() , Integer ( *aIt++, IntFmt<integer_base::hex,integer_format::fixed>() ) , " |  > Data [" , Integer ( lCounter++ ) , "]" );
  }

  mReceivedControlPacketHeaderHistory.push_back ( mPacketHeader );
  mReceivedControlPacketHeaderHistory.pop_front();

  uint32_t lAddress ( aAddress );

  while ( aIt != aEnd ) {
    std::string registerName = m_dfApi->get_ipbusApi()->getRegisterNameFromAddress ( lAddress );
    m_dfApi->get_ipbusApi()->single_access_write( registerName, *aIt++ );
  }

  uint32_t lExpected = IPbus< IPbus_major, IPbus_minor >::ExpectedHeader ( mType, mWordCounter, mTransactionId );

  mReply.push_back ( lExpected );
  mSentControlPacketHeaderHistory.push_back ( lExpected );
  mSentControlPacketHeaderHistory.pop_front();
}

void FtkDFuhalEmulator::write ( const uint32_t& aAddress, std::vector<uint32_t>::const_iterator& aIt, const std::vector<uint32_t>::const_iterator& aEnd )
{
    log ( Notice() , Integer ( mHeader, IntFmt<integer_base::hex,integer_format::fixed>() ) , " | incrementing write, size " , Integer ( mWordCounter ) , ", transaction ID " , Integer ( mTransactionId ) );
    log ( Notice() , Integer ( aAddress, IntFmt<integer_base::hex,integer_format::fixed>() ) , " |  > Address" );
    uint32_t lCounter ( 0 );

    while ( aIt != aEnd )
    {
      log ( Notice() , Integer ( *aIt++, IntFmt<integer_base::hex,integer_format::fixed>() ) , " |  > Data [" , Integer ( lCounter++ ) , "]" );
    }

  mReceivedControlPacketHeaderHistory.push_back ( mPacketHeader );
  mReceivedControlPacketHeaderHistory.pop_front();

  uint32_t lAddress ( aAddress );

  while ( aIt != aEnd ) {
    std::string registerName = m_dfApi->get_ipbusApi()->getRegisterNameFromAddress ( lAddress++ );
    m_dfApi->get_ipbusApi()->single_access_write( registerName, *aIt++ );
  }
  uint32_t lExpected = IPbus< IPbus_major, IPbus_minor >::ExpectedHeader ( mType, mWordCounter, mTransactionId );

  mReply.push_back ( lExpected );
  mSentControlPacketHeaderHistory.push_back ( lExpected );
  mSentControlPacketHeaderHistory.pop_front();
}

void FtkDFuhalEmulator::rmw_sum ( const uint32_t& aAddress, const uint32_t& aAddEnd )
{
  log ( Notice(), Integer ( mHeader, IntFmt<integer_base::hex,integer_format::fixed>() ), " | Read-modify-write sum, transaction ID " , Integer (mTransactionId) );
  log ( Notice(), Integer ( aAddress, IntFmt<integer_base::hex,integer_format::fixed>() ), " | > Address" );
  log ( Notice(), Integer ( aAddEnd, IntFmt<integer_base::hex,integer_format::fixed>() ), " | > Addend" );

  mReceivedControlPacketHeaderHistory.push_back ( mPacketHeader );
  mReceivedControlPacketHeaderHistory.pop_front();
  uint32_t lAddress ( aAddress );
  uint32_t lExpected ( IPbus< IPbus_major , IPbus_minor >::ExpectedHeader ( mType , 1 , mTransactionId ) );
  mReply.push_back ( lExpected );
  mSentControlPacketHeaderHistory.push_back ( lExpected );
  mSentControlPacketHeaderHistory.pop_front();

  std::string registerName = m_dfApi->get_ipbusApi()->getRegisterNameFromAddress ( lAddress );
  uint32_t lValue;
  m_dfApi->get_ipbusApi()->single_access_read ( registerName, lValue );
  lValue += aAddEnd;
  m_dfApi->get_ipbusApi()->single_access_write ( registerName, lValue );
  // IPbus 1.x returns modified value, 2.x returns pre-modified value
  mReply.push_back ( (IPbus_major == 1) ? lValue : lValue-aAddEnd );
}

void FtkDFuhalEmulator::rmw_bits ( const uint32_t& aAddress, const uint32_t& aAndTerm, const uint32_t& aOrTerm )
{
    log ( Notice() , Integer ( mHeader, IntFmt<integer_base::hex,integer_format::fixed>() ) , " | Read-modify-write bits, transaction ID " , Integer ( mTransactionId ) );
    log ( Notice() , Integer ( aAddress, IntFmt<integer_base::hex,integer_format::fixed>() ) , " |  > Address" );
    log ( Notice() , Integer ( aAndTerm, IntFmt<integer_base::hex,integer_format::fixed>() ) , " |  > And-term" );
    log ( Notice() , Integer ( aOrTerm, IntFmt<integer_base::hex,integer_format::fixed>() ) , " |  > Or-term" );

  mReceivedControlPacketHeaderHistory.push_back ( mPacketHeader );
  mReceivedControlPacketHeaderHistory.pop_front();
  uint32_t lAddress ( aAddress );
  uint32_t lExpected ( IPbus< IPbus_major , IPbus_minor >::ExpectedHeader ( mType , 1 , mTransactionId ) );
  mReply.push_back ( lExpected );
  mSentControlPacketHeaderHistory.push_back ( lExpected );
  mSentControlPacketHeaderHistory.pop_front();

  std::string registerName = m_dfApi->get_ipbusApi()->getRegisterNameFromAddress ( lAddress );
  uint32_t lValue;
  m_dfApi->get_ipbusApi()->single_access_read ( registerName, lValue );
  uint32_t lValueStore = lValue;
  lValue &= aAndTerm;
  lValue |= aOrTerm;
  m_dfApi->get_ipbusApi()->single_access_write ( registerName, lValue );
  // IPbus 1.x returns modified value, 2.x returns pre-modified value
  mReply.push_back ( (IPbus_major == 1) ? lValue : lValueStore );
}

void FtkDFuhalEmulator::unknown_type ()
{
  log ( Error(), Integer ( mHeader, IntFmt<integer_base::hex,integer_format::fixed>() ) , " | Unknown Transaction Header. Returning error code." );

  mReceivedControlPacketHeaderHistory.push_back ( mPacketHeader );
  mReceivedControlPacketHeaderHistory.pop_front();
  uint32_t lExpected ( IPbus< IPbus_major , IPbus_minor >::ExpectedHeader ( mType , 0 , mTransactionId , 1 ) );
  mReply.push_back ( lExpected );
  mSentControlPacketHeaderHistory.push_back ( lExpected );
  mSentControlPacketHeaderHistory.pop_front();
}

bool FtkDFuhalEmulator::control_packet_header ()
{
  log ( Notice(), Integer ( mPacketHeader, IntFmt<integer_base::hex,integer_format::fixed>() ) , " | Control (Instruction Packet Header , Packet Counter " , Integer ( mPacketCounter ) );

  if ( mPacketCounter != 0 ) {
    uint16_t lTemp ( ( ( mLastPacketHeader>>8 ) & 0x0000FFFF ) + 1 );
    if ( lTemp == 0 ) lTemp = 1;

    if ( mPacketCounter != lTemp ) {
      mTrafficHistory.push_back( 5 );
      mTrafficHistory.pop_front();
      return false;
    }

    mLastPacketHeader = mPacketHeader;
  }

  mReply.push_back ( mPacketHeader );
  mTrafficHistory.push_back ( 2 );
  mTrafficHistory.pop_front();
  return true;
}

void FtkDFuhalEmulator::status_packet_header ()
{
  log ( Notice(), Integer ( mPacketHeader, IntFmt<integer_base::hex,integer_format::fixed>() ) , " | Status Request Packet Header" );

  mReply.push_back ( mPacketHeader );
  mReply.push_back ( BUFFER_SIZE * sizeof ( uint32_t ) );
  mReply.push_back ( REPLY_HISTORY_DEPTH );
  uint16_t lTemp ( ( ( mLastPacketHeader>>8 ) & 0x0000FFFF ) + 1 );

  if ( lTemp == 0 ) lTemp = 1;

  mReply.push_back ( ( mLastPacketHeader & 0xFF0000FF ) | ( ( lTemp <<8 ) & 0x00FFFF00 ) );
  std::deque< uint8_t >::const_iterator lIt ( mTrafficHistory.begin() );

  for ( uint32_t i=0; i != 4; ++i ) {
    uint32_t lTemp ( 0x00000000 );

    for ( uint32_t j = 0; j != 4; ++j ) {
      lTemp <<= 8;
      lTemp |= ( uint32_t ) ( *lIt );
      lIt++;
    }

    mReply.push_back ( lTemp );
  }

  for ( std::deque< uint32_t >::const_iterator i = mReceivedControlPacketHeaderHistory.begin(); i != mReceivedControlPacketHeaderHistory.end() ; ++i ) {
    mReply.push_back ( *i );
  }

  for ( std::deque< uint32_t >::const_iterator i = mSentControlPacketHeaderHistory.begin(); i != mSentControlPacketHeaderHistory.end() ; ++i ) {
    mReply.push_back ( *i );
  }

  mTrafficHistory.push_back ( 3 );
  mTrafficHistory.pop_front();

//  log ( Notice(), "status_packet_header() exiting." );
}

void FtkDFuhalEmulator::resend_packet_header ()
{
  log ( Notice(), Integer ( mPacketHeader, IntFmt<integer_base::hex,integer_format::fixed>() ) , " | Resend Request Packet Header" );

  std::deque< std::pair< uint32_t , std::vector< uint32_t > > >::reverse_iterator lIt = mReplyHistory.rbegin();

  for ( ; lIt!=mReplyHistory.rend() ; ++lIt )
  {
    if ( lIt->first == mPacketCounter )
    {
      mReply = lIt->second;
      break;
    }
  }

  mTrafficHistory.push_back ( 4 );
  mTrafficHistory.pop_front();
}

void FtkDFuhalEmulator::unkown_packet_header ()
{
  log ( Error(), Integer ( mPacketHeader, IntFmt<integer_base::hex,integer_format::fixed>() ), " | Unkown Packet Header" );

  mTrafficHistory.push_back ( 5 );
  mTrafficHistory.pop_front();
}
