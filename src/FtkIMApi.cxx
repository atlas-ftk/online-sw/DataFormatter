#include <DataFormatter/FtkIMApi.h>
#include "ftkcommon/exceptions.h"
#include "ftkcommon/core.h"
#include "ftkcommon/Utils.h"
#include <fstream>
#include <sstream>

//const int nretry = 10; //already declared in header

const string imFwType[4] = {"Spartan 6 test", "Spartan 6 stable", "Artix 7 test", "Artix 7 stable"};

using namespace daq::ftk;
// ====================================================
FtkIMApi::FtkIMApi(std::shared_ptr<FtkIPBusApi> ipbus, const std::string& name)
  : m_name( name ),
    m_ipbus( ipbus )
{
  // ERS_LOG("FtkIMApi::constructor: Entered");
  ERS_DEBUG(1,"FtkIMApi::constructor: Entered");
  m_current_fpga = 0xDEADBEEF;
}

// ====================================================
uint32_t FtkIMApi::get_i2c_address(const uint32_t& fpgaId){
  // fpga_id 0,1 = FMC1 = GA[0]=0 GA[1]=0
  // fpga_id 2,3 = FMC2 = GA[0]=1 GA[1]=0
  // fpga_id 4,5 = FMC3 = GA[0]=0 GA[1]=1
  // fpga_id 4,5 = FMC4 = GA[0]=1 GA[1]=1
  // Following a specification of FMC, i.e.
  // GA[0] => I2C device's A[1]
  // GA[1] => I2C device's A[0]
  // Leading to assign following address to each FPGA
  // FPGA0 = 0b0000100 = 0X4
  // FPGA1 = 0b0001000 = 0X8
  // FPGA2 = 0b0000110 = 0X6
  // FPGA3 = 0b0001010 = 0Xa

  // FPGA4 = 0b0000101 = 0X5
  // FPGA5 = 0b0001001 = 0X9
  // FPGA6 = 0b0000111 = 0X7
  // FPGA7 = 0b0001011 = 0Xb
  uint32_t fpgaOnFmcId 		=  (fpgaId%2) + 0X1;
  uint32_t leastSigFmcId 	=  (fpgaId/4);
  uint32_t mostSigFmcId 	= ((fpgaId%4)/2);
  uint32_t i2c_address 		= ((fpgaOnFmcId << 2) | (mostSigFmcId << 1) | leastSigFmcId);
  return   i2c_address;
}
// TODO: mutex lock here
void FtkIMApi::single_access_write( const std::string& nodename, const uint32_t& value ) {
  m_ipbus->single_access_write( nodename, value );
}
void FtkIMApi::set_fw_version( const uint32_t& channel ){
  if( m_current_fpga != channel/2 ) {
    m_current_fpga = channel/2;
    FtkIMApi::i2c_32b_read( FtkIMApi::get_i2c_address( channel/2 ), 0x00 | 0x80, m_fw_version ); // 0x00 | 0x80 is fw version global address
    
    // m_fw_type = 0xDEAD;
    // m_fw_hash = 0xDEADBEEF;
    // m_fw_date = 0xDEADBEEF;
  }
}
void FtkIMApi::print_fw_verion( const uint32_t& fpgaId, const uint32_t& do_check, const uint32_t& oks_fw_version, bool is_standalone ){
  std::stringstream ss;
  if( is_standalone ){
    FtkIMApi::read_register( fpgaId*2, "GA_FW_TYPE",    m_fw_type ) ;
    FtkIMApi::read_register( fpgaId*2, "GA_FW_HASH",    m_fw_hash ) ;
    FtkIMApi::read_register( fpgaId*2, "GA_FW_DATE",    m_fw_date );
    printf( " IM FPGA%i FW : version(0x%04x), type(0x%04x), hash(0x%08x), date(0x%08x) \n", fpgaId, m_fw_version, m_fw_type, m_fw_hash, m_fw_date );
  }else{
    ss << "IM" << fpgaId;
    uint32_t fw_version = ( (m_fw_type << 16) | m_fw_version );
    daq::ftk::FTKFwVersion im_fw( ERS_HERE, m_name, ss.str(), fw_version );
    ers::info( im_fw );
  }
}
void FtkIMApi::set_slink_mode( const uint32_t& slink_mode ){
  for( uint32_t channel = 0; channel < 0xf; ++channel ){
    uint32_t channel_slink_mode = ( ( 0x1 << channel ) & slink_mode ) ? 0x1 : 0x0;
    // FtkIMApi::set_fw_version(  channel );
    
    for( int i = 0; i < 10; ++i ){
      ERS_LOG( " Setting slink mode " << channel_slink_mode << " for IM channel " << channel );
      if( FtkIMApi::update_register( channel, "GA_CONTROL_REG:SLINK_MODE", channel_slink_mode ) ) {
        break;
      } // if
      if( i == 9 ) { throw daq::ftk::i2cIPBusIssue(ERS_HERE, m_name , " Failed setting IM slink mode after 10 retries."); }
    } // for i 
  } // for channel
}
bool FtkIMApi::control_global_register( const uint32_t& channel, const std::string& register_name, const uint32_t& value ){
  // FtkIMApi::set_fw_version( channel );
  for( int i = 0; i < 10; ++i ){
    if( FtkIMApi::update_register( channel, register_name, value ) ) return true;
  }
  throw daq::ftk::i2cIPBusIssue( ERS_HERE, m_name , " Failed IM global register " + register_name + " control word after 10 retries.");
  return false;
}
bool FtkIMApi::control_slink(       const uint32_t& channel, const uint32_t& slink_enable ){ return FtkIMApi::control_global_register( channel, std::string("GA_CONTROL_REG:SLINK_ENABLE"), slink_enable ); }
bool FtkIMApi::control_xoff(        const uint32_t& channel, const uint32_t& xoff_mode )   { return FtkIMApi::control_global_register( channel, std::string("GA_CONTROL_REG:XOFF_MODE"), xoff_mode ); }
bool FtkIMApi::control_pseudo_data( const uint32_t& channel, const uint32_t& pseudo_enable, const uint32_t& pseudo_mode ){
  ERS_LOG( " Setting pseudo data for ch " << channel << "  pseudo_enable / pseudo_mode  " << pseudo_enable << " / " << pseudo_mode );
  return FtkIMApi::control_global_register( channel, std::string("GA_CONTROL_REG:PSEUDO_ENABLE"), pseudo_enable ) &
         FtkIMApi::control_global_register( channel, std::string("GA_CONTROL_REG:PSEUDO_MODE"),   pseudo_mode ); }
bool FtkIMApi::control_pseudo_config( const uint32_t& channel, const int& skew_count_at_40MHz, const bool& is_pseudo_data_xoff  ){
  uint32_t this_count = (uint32_t) skew_count_at_40MHz;
  uint32_t value = 0x0;
  if( is_pseudo_data_xoff ) value |= (0x1<<31);
  value |= (this_count & 0x0FFFFFFF);
  std::string register_name = ( channel % 2 == 0 ) ? "IA_OKS_PSEUDO_DATA_CONFIG_CH0" : "IA_OKS_PSEUDO_DATA_CONFIG_CH1";
  return FtkIMApi::set_register( channel, register_name, value );
}
// void FtkIMApi::sendIMCounterReset(const uint32_t& channel ){
//   uint32_t i2c_address = get_i2c_address( channel/2 );
//   FtkIMApi::set_fw_version( channel );
//   if( FtkIMApi::is_old_fw() ){
//     FtkIMApi::i2c_32b_write( i2c_address, FtkIMApi::get_old_address( channel, "GA_OFFSET0" ),       0x00000301); // write  local config address
//     FtkIMApi::i2c_32b_write( i2c_address, FtkIMApi::get_old_address( channel, "GA_INTERNALDATA0" ), 0x00000001); // send reset signal
//     FtkIMApi::i2c_32b_write( i2c_address, FtkIMApi::get_old_address( channel, "GA_INTERNALDATA0" ), 0x00000000); // send reset signal
//   } // no need for refactored FW
// }
void FtkIMApi::set_word_counter( const uint32_t& n_word){
  m_ipbus->single_access_write("reg.fb_i2c_word_counter", 0x0);//reset
  m_ipbus->single_access_write("reg.fb_i2c_word_counter", n_word);// N word
}
void FtkIMApi::send_freeze( const uint32_t& channel, const bool& b_freeze ){
  FtkIMApi::set_word_counter( 0x1 ); // just 1 word
  string exText = m_name + " Failed to send freeze command "+to_string(b_freeze)+" to IM channel "+to_string(channel);
  
  uint32_t i(0);
  while( i < 10 ) {
    try {
      FtkIMApi::set_register(     channel, "GA_CONFIG_REG:FREEZE", b_freeze ? 0x01 : 0x0 );
      return;
    } catch (daq::ftk::i2cIPBusWrite ex) {
      daq::ftk::i2cIPBusIssue ex2(ERS_HERE, m_name, exText + ". Retrying...", ex);
      ers::log(ex2);
    }    
    i++;
  }
  daq::ftk::i2cIPBusIssue ex(ERS_HERE, m_name, exText + ". Exit!");
  ers::warning(ex);
}
void FtkIMApi::set_i2c_blockSize( const uint32_t blockSize ){
  FtkIMApi::set_word_counter( blockSize );
  // reset DF buffer address, to be fixed
  m_ipbus->single_access_write("reg.spy_controller.spy_reset_im_spy", 0X1);
  m_ipbus->single_access_write("reg.spy_controller.spy_reset_im_spy", 0X0);
}
void FtkIMApi::read_from_DFBuffer_block(const uint32_t blockSize, vector<uint32_t>& value){
  // uint32_t tmp_value;
  value.clear();
  value.reserve(blockSize);
  
  
  const uint32_t readsize = 4;
  std::vector<uint32_t> raw_data(readsize);
  m_ipbus->single_access_write("reg.spy_controller.spy_freeze", 0X1);//freeze
  m_ipbus->single_access_write("reg.spy_readen", 0x1 ); // enable
  m_ipbus->single_access_write("reg.spy_laneselector", 52); // 52 is lane_id
  
  for (uint32_t iAddr=0; iAddr<blockSize; iAddr++) {
    if (iAddr%4 != 0) continue;
    
    m_ipbus->single_access_write("reg.spy_readaddr", iAddr);
    try {
      m_ipbus->single_access_block_read("reg.spy_readout", raw_data, readsize);
    } catch( ers::Issue &ex)  {
      daq::ftk::IPBusIssue e(ERS_HERE,m_name, "Error while trying to do block readk", ex);  // NB: append the original exception (ex) to the new one 
      ers::warning(e);  //or throw e;  
    }
    value.push_back(raw_data[2]);
    value.push_back(raw_data[3]);
    value.push_back(raw_data[0]);
    value.push_back(raw_data[1]);
  }
  
  // spy restart
  m_ipbus->single_access_write("reg.spy_controller.spy_freeze", 0x0);
  m_ipbus->single_access_write("reg.spy_readen", 0x0); // disable
}

void FtkIMApi::read_from_IMBuffer_to_DFBuffer( const uint32_t& channel, const std::string& register_name, const uint32_t& ireadout, bool is_apply_freeze, const uint32_t& df_buffer_depth, bool is_standalone ){
  uint32_t internal_address    = FtkIMApi::get_internal_address( channel, register_name ) ;
  FtkIMApi::read_from_IMBuffer_to_DFBuffer( channel, internal_address, ireadout, is_apply_freeze, df_buffer_depth, is_standalone );
}
void FtkIMApi::read_from_IMBuffer_to_DFBuffer( const uint32_t& channel, const uint32_t& register_address, const uint32_t& ireadout, bool is_apply_freeze, const uint32_t& df_buffer_depth, bool is_standalone ){
  
  // depth of DF buffer for block transfer from IM to DF
  uint32_t i2c_address         = FtkIMApi::get_i2c_address( channel/2 );
  uint32_t internal_address    = register_address + df_buffer_depth * ireadout;
  uint32_t global_address_0    = FtkIMApi::get_global_address( channel, "GA_OFFSET0" )       | 0x00; // write bit 
  uint32_t global_address_1    = FtkIMApi::get_global_address( channel, "GA_INTERNALDATA0" ) | 0x80; // read bit 
  uint32_t value_to_read_dummy = 0;
  
  if( ireadout == 0 ){
    // FtkIMApi::set_fw_version( channel );
    FtkIMApi::print_fw_verion( channel%2, false, 0, is_standalone );
    // FtkIMApi::sendIMCounterReset( channel/2 );
    if( is_apply_freeze ) FtkIMApi::send_freeze( channel, true );
    
    m_ipbus->single_access_write("reg.fb_i2c_word_counter", 0x1);//reset
    m_ipbus->single_access_write("reg.fb_i2c_word_counter", 0x0);//reset
	
    m_ipbus->single_access_write("reg.spy_controller.spy_reset_im_spy", 0X1);
    m_ipbus->single_access_write("reg.spy_controller.spy_reset_im_spy", 0X0);
    FtkIMApi::set_word_counter( df_buffer_depth );// 1024/256 for spy/smon
  }
  for(int i = 0; i < 10; ++i){
    if( FtkIMApi::i2c_single_access_read_or_write_block( true,  i2c_address, global_address_0, internal_address ) ) break;
  }
  for(int i = 0; i < 10; ++i){
    if( FtkIMApi::i2c_single_access_read_or_write_block( false, i2c_address, global_address_1, value_to_read_dummy ) ) break;;
  }
  usleep(20 * 1e4); // wait to finish reading out all spy words (500ms)
  
  if( ireadout == 3 && is_apply_freeze ) FtkIMApi::send_freeze( channel, false );
  
}


bool FtkIMApi::i2c_single_access_core( const bool is_write, const uint32_t& i2c_addr, const uint32_t& value1, uint32_t& value2, uint32_t& busy, uint32_t& ackerror ){
  
  m_ipbus->clear_write_register_buffer();
  m_ipbus->clear_read_register_buffer();
  
  // write value1
  m_ipbus->single_access_write2("reg.reset.i2c_state_machine_reset", 0X0);
  m_ipbus->single_access_write2("reg.reset.i2c_state_machine_reset", 0X1);
  m_ipbus->single_access_write2("reg.reset.i2c_state_machine_reset", 0X0);
  
  const uint32_t addr_mask = m_ipbus->get_hardware()->getNode("reg.fb_i2c_address.address").getMask();
  const uint32_t addr_7bit = (i2c_addr & addr_mask);
  
  m_ipbus->single_access_write2("reg.fb_i2c_address.address", addr_7bit);
  m_ipbus->single_access_write2("reg.fb_i2c_address.is_read_access", 0X0);
  m_ipbus->single_access_write2("reg.fb_i2c_data_from_master", value1 );
  
  m_ipbus->single_access_write("reg.fb_i2c_enable.enable", 0X1);
  m_ipbus->single_access_write("reg.fb_i2c_enable.enable", 0X0);
  
  // read value2
  m_ipbus->single_access_write2("reg.reset.i2c_state_machine_reset", 0X0);
  m_ipbus->single_access_write2("reg.reset.i2c_state_machine_reset", 0X1);
  m_ipbus->single_access_write2("reg.reset.i2c_state_machine_reset", 0X0);
  
  
  if( is_write ){
    m_ipbus->single_access_write2("reg.fb_i2c_address.address", addr_7bit);
    m_ipbus->single_access_write2("reg.fb_i2c_address.is_read_access", 0X0);
    m_ipbus->single_access_write2("reg.fb_i2c_data_from_master", value2);
    
    m_ipbus->single_access_write("reg.fb_i2c_enable.enable", 0X1);
    m_ipbus->single_access_write("reg.fb_i2c_enable.enable", 0X0);
    // printf(" i2c_single_access_core : write 0x%08x / 0x%08x\n", value1, value2 );
  }else{
    m_ipbus->single_access_write2("reg.fb_i2c_address.address", addr_7bit);
    m_ipbus->single_access_write2("reg.fb_i2c_address.is_read_access", 0X1);
    
    m_ipbus->single_access_write("reg.fb_i2c_enable.enable", 0X1);
    m_ipbus->single_access_write("reg.fb_i2c_enable.enable", 0X0);
    m_ipbus->single_access_read("reg.fb_i2c_data_from_slave", value2);
    // printf(" i2c_single_access_core : read 0x%08x / 0x%08x\n", value1, value2 );
  }
  
  m_ipbus->add_register_to_read("reg.fb_i2c_status.busy");
  m_ipbus->add_register_to_read("reg.fb_i2c_status.ack_error");
  m_ipbus->send_transaction_to_read();
  
  busy     = m_ipbus->get_readback_register_value("reg.fb_i2c_status.busy");
  ackerror = m_ipbus->get_readback_register_value("reg.fb_i2c_status.ack_error");
  return true;
}
bool FtkIMApi::i2c_single_access_read_or_write(bool is_write, const uint32_t& i2c_address, const uint32_t& value1, uint32_t& value2){
  const uint32_t timeout = 1000;
  uint32_t busy, ackerror, nretry;
  
  FtkIMApi::set_word_counter( 0x1 );
  
  for(uint32_t iretry = 0; iretry < timeout + 1; iretry++ ){
    // printf( " i2c_single_access_read_or_write %d\n", iretry );
    FtkIMApi::i2c_single_access_core( is_write, i2c_address, value1, value2, busy, ackerror );
    nretry = iretry;
    if ( busy == 0x0 and ackerror == 0x0 ) break;
  }
  
  if ( nretry == timeout ) {
    if( is_write ){
      ERS_LOG("FtkIMApi::i2c_single_access_write: Invalid single-write access more than " << timeout << " retries; busy = " << busy << " ackerror = " << ackerror);
      throw daq::ftk::i2cIPBusRead(ERS_HERE, "Invalid single-write access");
    }else{
      ERS_LOG("FtkIMApi::i2c_single_access_read: Invalid single-read access more than "   << timeout << " retries; busy = " << busy << " ackerror = " << ackerror);
      throw daq::ftk::i2cIPBusRead(ERS_HERE, "Invalid single-read access");
    }
  }
  return true;
}

bool FtkIMApi::i2c_single_access_read_or_write_block( bool is_write, const uint32_t& i2c_addr, const uint32_t& value1, uint32_t& value2){
  
  const uint32_t timeout = 1000;
  uint32_t trial = 0;
  uint32_t busy, ackerror;
  FtkIMApi::i2c_single_access_core( is_write, i2c_addr, value1, value2, busy, ackerror );
  
  if          ( busy == 0x0 and ackerror == 0x0 ) { return true; }
  while ( not ( busy == 0x0 and ackerror == 0x0 ) ) {
    m_ipbus->send_transaction_to_read();
    busy     = m_ipbus->get_readback_register_value("reg.fb_i2c_status.busy");
    ackerror = m_ipbus->get_readback_register_value("reg.fb_i2c_status.ack_error");
    trial++;
    if (trial > timeout) {
      if( is_write ) {
        ERS_LOG("FtkIMApi::i2c_single_access_write_spy: Invalid single-write_spy access more than " << timeout << " retries; busy = " << busy << " ackerror = " << ackerror);
        throw daq::ftk::i2cIPBusRead( ERS_HERE, "Invalid single-write_spy access");
      }else{
        ERS_LOG("FtkIMApi::i2c_single_access_read_spy: Invalid single-read_spy access more than " << timeout << " retries; busy = " << busy << " ackerror = " << ackerror);
        throw daq::ftk::i2cIPBusRead( ERS_HERE, "Invalid single-read_spy access");
      }        
    }
  }
  return true;
}

bool FtkIMApi::i2c_32b_read(const uint32_t i2c_address, const uint32_t internal_address, uint32_t& value){
  if( internal_address == 0xDEADBEEF ) {
    ERS_LOG( "IM i2c_32b_read : invalid addresss ");
    return false;
  }
  try{
    for(uint32_t i=0; i<nretry; i++){
      if( FtkIMApi::i2c_single_access_read_or_write ( false, i2c_address, internal_address, value)) break;
    }
  } catch (daq::ftk::i2cIPBusWrite ex) { throw daq::ftk::i2cIPBusIssue(ERS_HERE, m_name , " Failed to read out a 8-bit word via i2c",ex);
  } catch (daq::ftk::i2cIPBusRead ex)  { throw daq::ftk::i2cIPBusIssue(ERS_HERE, m_name , " Failed to read out a 8-bit word via i2c",ex);
  } 
  return true;
}
bool FtkIMApi::i2c_32b_read_block(const uint32_t i2c_address, const uint32_t internal_address, uint32_t& value){
  if( internal_address == 0xDEADBEEF ) {
    ERS_LOG( "IM i2c_32b_read_block : invalid addresss ");
    return false;
  }
  ERS_LOG( "IM i2c_32b_read_block :  " << i2c_address << " : " << internal_address );
  
  try{
    for(uint32_t i=0; i<nretry; i++)
      if( FtkIMApi::i2c_single_access_read_or_write_block( false, i2c_address, internal_address, value)) break;
  } catch (daq::ftk::i2cIPBusWrite ex) { throw daq::ftk::i2cIPBusIssue(ERS_HERE, m_name , " Failed to read out a 8-bit word via i2c",ex);
  } catch (daq::ftk::i2cIPBusRead ex)  { throw daq::ftk::i2cIPBusIssue(ERS_HERE, m_name , " Failed to read out a 8-bit word via i2c",ex);
  } 
  return true;
}
bool FtkIMApi::i2c_32b_write(const uint32_t i2c_address, const uint32_t internal_address, const uint32_t value){
  if( internal_address == 0xDEADBEEF ) {
    ERS_LOG( "IM i2c_32b_write : invalid addresss ");
    return false;
  }
  uint32_t value_to_write = value;
  try {
    for(uint32_t i=0; i<nretry; i++)
      if( FtkIMApi::i2c_single_access_read_or_write( true, i2c_address, internal_address, value_to_write) ) break;
  }  catch (daq::ftk::i2cIPBusWrite ex) {
    daq::ftk::i2cIPBusIssue ex2(ERS_HERE, m_name , " Failed to write a 32-bit word via i2c", ex);
    ers::warning(ex2);
    return false;
  }
  return true;
}
 
//------------------------------- for new refactored FW  -----------------------------------//
bool FtkIMApi::read_register(    const uint32_t& channel, std::string register_name, uint32_t& value_to_read, uint32_t internal_address_offset ){
  uint32_t i2c_address       = FtkIMApi::get_i2c_address( channel/2 );
  // FtkIMApi::set_fw_version( channel );
  
  if( register_name.compare( 0, 3, "GA_" ) == 0 ) {
    uint32_t im_global_address = FtkIMApi::get_global_address( channel, register_name ) | 0x80; // read bit 
    return FtkIMApi::i2c_32b_read( i2c_address, im_global_address, value_to_read );
    
  } else if( register_name.compare( 0, 3, "IA_" ) == 0 ) {
    uint32_t im_global_address_offset0    = FtkIMApi::get_global_address( channel, "GA_OFFSET0" )       | 0x00; // write bit
    uint32_t im_global_address_intdata0   = FtkIMApi::get_global_address( channel, "GA_INTERNALDATA0") | 0x80; // read bit
    uint32_t im_internal_address_register = FtkIMApi::get_internal_address( channel, register_name ) + internal_address_offset;
    
    return FtkIMApi::i2c_32b_write( i2c_address, im_global_address_offset0,  im_internal_address_register ) & FtkIMApi::i2c_32b_read(  i2c_address, im_global_address_intdata0, value_to_read );
  }
  ERS_LOG( "IM Global Address of " <<  register_name << " is not in the table, GA_ or IA_ ... Plase check it(read_register) " );
  return false;
}
bool FtkIMApi::write_register(   const uint32_t& channel, std::string register_name, const uint32_t& value_to_write ){
  uint32_t i2c_address       = FtkIMApi::get_i2c_address( channel/2 );
  
  if( register_name.compare( 0, 3, "GA_" ) == 0 ) {
    uint32_t im_global_address = FtkIMApi::get_global_address( channel, register_name ) | 0x00; // write bit
    return FtkIMApi::i2c_32b_write( i2c_address, im_global_address, value_to_write );
    
  } else if( register_name.compare( 0, 3, "IA_" ) == 0 ) {
    uint32_t im_global_address_offset0    = FtkIMApi::get_global_address( channel, "GA_OFFSET0" )       | 0x00; // write bit
    uint32_t im_global_address_intdata0   = FtkIMApi::get_global_address( channel, "GA_INTERNALDATA0" ) | 0x00; // read bit 
    uint32_t im_internal_address_register = FtkIMApi::get_internal_address( channel, register_name );
    return FtkIMApi::i2c_32b_write( i2c_address, im_global_address_offset0,  im_internal_address_register ) & FtkIMApi::i2c_32b_write( i2c_address, im_global_address_intdata0, value_to_write );
  }
  ERS_LOG( "IM register of " <<  register_name << " is not in the table, GA_ or IA_ ... Plase check it(write_register)" );
  return false;
}
bool FtkIMApi::update_register(   const uint32_t& channel, std::string register_name, const uint32_t value_to_update ){
  unsigned long it = register_name.find(":");
  std::string reg_name = register_name.substr( 0, it );
  std::string bit_name = register_name.substr( it + 1, register_name.size() + 1 - it );
  std::transform( reg_name.begin(), reg_name.end(), reg_name.begin(), ::toupper ); 
  std::transform( bit_name.begin(), bit_name.end(), bit_name.begin(), ::toupper );
  // FtkIMApi::set_fw_version( channel );
  
  uint32_t value_to_read;
  if( FtkIMApi::read_register( channel, reg_name, value_to_read ) ) { 
    uint32_t shift_value    = FtkIMApi::get_register_shift_value( channel, reg_name, bit_name );
    uint32_t value_to_write = ( value_to_read & ~( 0x1 << shift_value ) ) | (value_to_update << shift_value); // 0x1 can be mask_value
    return FtkIMApi::write_register( channel, reg_name, value_to_write );
  }
  ERS_LOG( "IM register of " << register_name << ":" << bit_name << " is not in the table, GA_ or IA_ ... Plase check it(update_register)" );
  return false;
}
bool FtkIMApi::set_register(   const uint32_t& channel, std::string register_name, const uint32_t value_to_update ){
  // FtkIMApi::set_fw_version( channel );
  
  if( register_name.find(":") != std::string::npos ) {
    return FtkIMApi::update_register( channel, register_name, value_to_update );
  }
  // nominal, just write 
  return FtkIMApi::write_register( channel, register_name, value_to_update );
}
uint32_t FtkIMApi::get_global_address( const uint32_t& channel, string register_name ) {
  std::transform( register_name.begin(), register_name.end(), register_name.begin(), ::toupper ); 
  
  if ( register_name.find("GA_CONTROL_REG") != std::string::npos ){
    const std::string reg_name = register_name.replace( 0, 14, "GA_CONTROL_REG" + std::to_string( channel % 2 ) );
    return FtkIMApi::get_map_cont(m_map_im_global_address, reg_name );
    
  }else if ( register_name.find("GA_CONFIG_REG") != std::string::npos ){
    const std::string reg_name = register_name.replace( 0, 13, "GA_CONFIG_REG" + std::to_string( channel % 2 ) );
    return FtkIMApi::get_map_cont(m_map_im_global_address, reg_name );
  }
  if ( m_map_im_global_address.count( register_name ) > 0 ){
    return FtkIMApi::get_map_cont(m_map_im_global_address, register_name );
  }
  ERS_LOG( "IM Global Address of " <<  register_name << " is not in the table" );
  return 0xDEADBEEF;
}
uint32_t FtkIMApi::get_internal_address( const uint32_t& channel, string register_name ) {
  std::transform( register_name.begin(), register_name.end(), register_name.begin(), ::toupper );
  if( m_map_im_internal_address.find( register_name ) != m_map_im_internal_address.end() ){
    return FtkIMApi::get_map_cont(m_map_im_internal_address, register_name );
  }
  ERS_LOG( "IM Register of " <<  register_name << " is not in the table" );
  return 0xDEADBEEF;
}

uint32_t FtkIMApi::get_register_shift_value( const uint32_t& channel, string register_name, string bit_name ){
  std::transform( register_name.begin(), register_name.end(), register_name.begin(), ::toupper ); 
  std::transform( bit_name.begin(), bit_name.end(), bit_name.begin(), ::toupper );
  // FtkIMApi::set_fw_version( channel );
  
  if( m_map_im_reg_bit.count( bit_name ) > 0 ) return FtkIMApi::get_map_cont(m_map_im_reg_bit, bit_name );
  
  ERS_LOG( "IM Register of " <<  register_name << " : " << bit_name << " is not in the table" );
  return 0xDEADBEEF;
}


bool FtkIMApi::set_IM_OKS( std::vector< uint32_t > channels, const std::map< std::string, uint32_t >& IM_OKS_parameters ){

  bool im_configure_status = true;
  
  std::vector< int > enabled_fpgas;
  for( size_t idx_fpga = 0; idx_fpga < 8; ++idx_fpga ){
    if( channels.at( 2*idx_fpga + 0 ) == 1 || channels.at( 2*idx_fpga + 1 ) ) {
      // FtkIMApi::set_fw_version( 2*idx_fpga + 0 ); // using ch0 as tmporary
      
      
      for( auto pair : IM_OKS_parameters ){
        if( pair.first.compare( 0, 3, "IA_" ) == 0 ){ // not global address, just set per FPGA
          im_configure_status *= FtkIMApi::set_register( 2*idx_fpga + 0, pair.first, pair.second );
        }else{ // in the case of global address 
          if( channels.at( 2*idx_fpga + 0 ) == 1 ){ im_configure_status *= FtkIMApi::set_register( 2*idx_fpga + 0, pair.first, pair.second ); }
          if( channels.at( 2*idx_fpga + 1 ) == 1 ){ im_configure_status *= FtkIMApi::set_register( 2*idx_fpga + 1, pair.first, pair.second ); }
        }
        } // oks loops
    } // case of FPGA is enabled
  } // FPGA loop 

  return im_configure_status;
}






// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ //
void FtkIMApi::downloadIMLUT( std::vector<uint32_t>& vecLinkId, std::vector<uint32_t>& vecHashId, std::vector<uint32_t>& vecRODId, const uint32_t& channel){
  std::vector< uint32_t > vecInternalData;
  
  for (uint32_t i = 0; i < vecLinkId.size(); i++){
    uint32_t internalData = 0;
    uint32_t linkId       = vecLinkId.at(i);
    uint32_t hashId       = vecHashId.at(i);
    if(hashId == 0Xffffffff) {
      if(linkId%2 == 0) {
        internalData = ( 1 << 31 | (linkId << 16) | 0Xdead);
        hashId       = 0x5ead;
      } else if(linkId%2 == 1) {
        internalData = ( 1 << 31 | (linkId << 16) | 0Xbeef);
        hashId       = 0x3eef;
      }
    }else internalData = ( 1 << 31 | (linkId << 16) | 1 << 15 | hashId);
    vecInternalData.push_back(internalData);
  }
  
  FtkIMApi::set_word_counter( 0x1 ); 
  // FtkIMApi::sendIMCounterReset( channel/2 );
  
  // send reset signal
  uint32_t tmp_value = 0;
  std::string register_name = ( channel % 2 == 0 ) ? std::string("IA_CH0_MODULE_LOOKUP") : std::string("IA_CH1_MODULE_LOOKUP" );
  FtkIMApi::set_register(  channel, register_name, 0x40000000 ); // 0x40000000 is reset signal for lut handler
  FtkIMApi::read_register( channel, register_name, tmp_value ); // 0x40000000 is reset signal for lut handler
  
  uint32_t iter = 0;
  while( vecInternalData.size() != 0 ) { 
    iter++;
    
    // FtkIMApi::sendIMCounterReset( channel );
    
    std::vector<uint32_t>::iterator it = vecInternalData.begin();
    while( it != vecInternalData.end() ) {
      uint32_t ii = it - vecInternalData.begin();
      
      uint32_t value_of_LUT = 0xDEADBEEF;
      FtkIMApi::set_register(  channel, register_name, (*it) );
      FtkIMApi::read_register( channel, register_name, value_of_LUT );
      
      uint32_t linkId_value_of_LUT = ((value_of_LUT & 0x1fff0000) >> 16);
      uint32_t hashId_value_of_LUT = ((value_of_LUT & 0x00007fff));
      
      ERS_DEBUG(1,"Check the written value against what is read out: in = " << std::hex << std::setw(8) << (*it) << "   out = " << std::hex << std:: setw(8) << value_of_LUT );
                
      
      if ((vecLinkId.at(ii) != linkId_value_of_LUT) || (vecHashId.at(ii) != hashId_value_of_LUT)) {
        daq::ftk::IMLUTWarning ex(ERS_HERE, m_name, m_name, vecLinkId.at(ii), linkId_value_of_LUT, vecHashId.at(ii), hashId_value_of_LUT );
        ers::warning(ex);
        it++; 
      } else {
        vecInternalData.erase(it);
        vecLinkId.erase(vecLinkId.begin() + ii);
        vecHashId.erase(vecHashId.begin() + ii);
      } 
    }
    // exit from the infinite loop
    if ( iter >= 10 ) { 
      throw daq::ftk::i2cIPBusIssue(ERS_HERE,m_name , " Failed IM LUT download after 10 retries.");
    }
  }

}

void FtkIMApi::downloadIMLUT( const uint32_t& channel, const std::string& LUT_file){
  std::ifstream fin(LUT_file.c_str());
  if(fin.fail()){
    throw( daq::ftk::IOError(ERS_HERE, m_name , " can not open LUT "+LUT_file) );
    return;
  }
  
  ERS_LOG("Downloading "<<LUT_file.c_str());
  
  // prepare LUT
  std::string c1,c2,c3;
  std::vector<uint32_t> vecLinkId;
  std::vector<uint32_t> vecHashId;
  std::vector<uint32_t> vecRODId;

  do {
    fin >> c1 >> c2 >> c3;
    if(fin.eof())	continue;

    uint32_t linkId   = strtoul(c1.c_str(), NULL, 0);
    uint32_t onlineId = strtoul(c2.c_str(), NULL, 0);
    uint32_t hashId   = strtoul(c3.c_str(), NULL, 0);
    uint32_t RODId 	  = ((onlineId) & 0Xffffff);
    vecLinkId.push_back(linkId);
    vecHashId.push_back(hashId);
    vecRODId.push_back(RODId);
  } while (!fin.eof());

  if ( (vecLinkId.size() != vecHashId.size()) || (vecLinkId.size() != vecRODId.size()) ) {
    throw( daq::ftk::IOError( ERS_HERE, m_name , " cannot process LUT file " + LUT_file + ", file is malformed. Need three columns per row." ) );
    return;
  } else if ( vecLinkId.size() == 0 ) {
    ers::warning( daq::ftk::IOError( ERS_HERE, m_name , " cannot process empty LUT file " + LUT_file ) );
    return;
  }
  FtkIMApi::downloadIMLUT(vecLinkId, vecHashId, vecRODId, channel);
}
void FtkIMApi::downloadAllIMLUT( const std::string& LUT_file){
  size_t iConfPath     = LUT_file.rfind("/")+1;
  std::string confPath = LUT_file.substr( 0, iConfPath );
  
  std::ifstream fin(LUT_file.c_str());
  if(fin.fail()) { 
    throw( daq::ftk::IOError(ERS_HERE, m_name , " can not open LUT "+LUT_file) );
    return;
  }
  ERS_LOG("Done opening the file with LUT tables");
  
  std::vector<string> vec_IM_LUTs;
  string c1,c2;
  do{
    fin >> c1 >> c2;
    if(fin.eof())  continue;
    vec_IM_LUTs.push_back(c2);
  } while(!fin.eof());
  ERS_LOG("Done reading LUT and filling vec_IM_LUTs");

  if ( vec_IM_LUTs.size() < 16 ) {
    throw( daq::ftk::IOError( ERS_HERE, m_name , " LUT file " + LUT_file + " is malformed. Needs an entry for all 16 IM channels!" ) );
    return;
  }

  m_IMLUTidx.clear();
  for(uint32_t ii=0; ii<16; ii++) {
    if(vec_IM_LUTs.at(ii)!="###") {
      // download the LUT
        ERS_LOG("Dowloading "<<ii<<"-th IM LUT");
        FtkIMApi::downloadIMLUT( ii, confPath + vec_IM_LUTs.at(ii) );
        m_IMLUTidx.push_back(ii);
    } else {
        ERS_LOG(ii<<"-th IM LUT is not provided. Skipping...");
    }     
  }  
}

void FtkIMApi::downloadAllIMLUT_OTF( const std::string& LUT_prefix, const std::map<uint32_t, uint32_t>& FMCInToRodIdMap){
  m_IMLUTidx.clear();
  for(uint32_t ii = 0; ii < 16; ii++ ) {
    if ( FMCInToRodIdMap.find(ii) != FMCInToRodIdMap.end() ) {
      // download the LUT
      ERS_LOG("Dowloading "<<ii<<"-th IM LUT");
      char lutName[200];
      sprintf(lutName,"%s%i.txt",LUT_prefix.c_str(),FMCInToRodIdMap.find(ii)->second);
      FtkIMApi::downloadIMLUT(ii, lutName);
      m_IMLUTidx.push_back(ii);
    } 
  }
}
// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ //

void FtkIMApi::download_pseudo_data( const uint32_t& channel, const std::string& pseudoData_file, const int& input_rate ){
  std::ifstream fin(pseudoData_file.c_str());
  if(fin.fail()) {
    char errorMsg[300];
    snprintf( errorMsg, 300, " could not find pseudodata file %s for channel %u.", pseudoData_file.c_str(), channel );
    throw( daq::ftk::IOError( ERS_HERE, m_name, errorMsg ) );
    return;
  }
  ERS_LOG("Downloading "<<pseudoData_file.c_str());
  
  // prepare pseudo data
  std::string c1;//,c2;
  std::vector<uint32_t> vec_pseudo_data0;
  std::vector<uint32_t> vec_pseudo_data1;
  std::vector<uint32_t> vec_data_word;
  uint32_t pseudo_addr = 0X0;
  
  int tmp_n_word(0), tmp_n_event(0);
  do{
    fin >> c1;// >> c2;
    if(fin.eof())continue;
    uint32_t data_word    = strtoul((c1.substr(1,8)).c_str(), NULL, 16);
    uint32_t ctrl_bits    = strtoul((c1.substr(0,1)).c_str(), NULL, 16);
    uint32_t pseudo_data0 = ( 0X1 << 29 | (data_word & 0Xfffffff) );
    uint32_t pseudo_data1 = ( 0X1 << 30 | (pseudo_addr << 8)  | (ctrl_bits << 4) | (data_word >> 28) );
    vec_pseudo_data0.push_back(pseudo_data0);
    vec_pseudo_data1.push_back(pseudo_data1);
    vec_data_word.push_back(data_word);
    ERS_DEBUG(2,printf("0X%08X %s 0X%08X 0X%08X \n", pseudo_addr, c1.c_str(), pseudo_data1, pseudo_data0));
    pseudo_addr++;
    
    
    if( ctrl_bits > 0xB ){ // data has 0xC, b0f/e0f has D, L1ID has E
      ++tmp_n_word;
      if( ctrl_bits == 0xD ) ++tmp_n_event; // b0f or e0f
      
    }
  }while(!fin.eof());
  
  // for block transfer
  FtkIMApi::set_word_counter( 0x1 ); 
  // FtkIMApi::sendIMCounterReset( channel/2 );
  
  // calcurate and set input rate delay value
  // pseudo data clk is 40 MHz
  tmp_n_event /= 2;
  ERS_LOG("Pseud data has " << tmp_n_event << " events and " << tmp_n_word << " words " );
  int n_target_word = (40 * 1000) * tmp_n_event / input_rate ;
  int delay_value = 0;
  if( tmp_n_word >= n_target_word ){
    float maximum_rate = (40 * 1000 * 1000.0 * tmp_n_event ) / float(tmp_n_word);
    ERS_LOG("Selected Pseudo data input rate is too fast!!, set to maximum " << maximum_rate/100. << " kHz ");
  }else{
    delay_value = n_target_word - tmp_n_word;
    ERS_LOG("Pseud data input rate is OK to set, delay value is " << delay_value );
  }
  std::string reg_namme_delay_value = ( channel % 2 == 0 ) ? std::string("IA_OKS_PSEUDO_DELAY_CH0_VALUE") : std::string("IA_OKS_PSEUDO_DELAY_CH1_VALUE");
  uint32_t write_delay_value = (uint32_t) delay_value;
  FtkIMApi::set_register( channel, reg_namme_delay_value, write_delay_value );// 
  
  // send reset signal
  std::string register_name = ( channel%2 == 0 ) ? std::string("IA_CH0_PSEUDO_DATA") : std::string("IA_CH1_PSEUDO_DATA");
  FtkIMApi::set_register( channel, register_name, 0x80000000 ); // 0x80000000 is reset word
  
  // download pseudo data to IM
  std::vector<int> indices_fail;
  bool is_failed = false;
  for(uint32_t ii = 0; ii < vec_pseudo_data0.size(); ii++ ){
    
    // pseudo data 0 and data 1
    FtkIMApi::set_register( channel, register_name, vec_pseudo_data0.at(ii) );
    FtkIMApi::set_register( channel, register_name, vec_pseudo_data1.at(ii) );
    
    // confirmation
    uint32_t value_of_pseudo_data;
    FtkIMApi::read_register( channel, register_name, value_of_pseudo_data );
    
    // check the failed to download
    if(vec_data_word.at(ii) != value_of_pseudo_data){
      ERS_LOG("Failed: " << ii << " " << hex << vec_data_word.at(ii) << " " << value_of_pseudo_data);
      indices_fail.push_back(ii);
      is_failed = true;
    }
  }
  
  // redownload pseudo data when the downloading fails
  do{
    if ( !is_failed ) continue;
    // initialization
    // FtkIMApi::sendIMCounterReset( channel/2 );
    std::vector<int> tmp_indices_fail;
    is_failed = false;
    
    for(unsigned int index_fail = 0; index_fail < indices_fail.size(); index_fail++){
      // redownloading
      int ii = indices_fail.at(index_fail);
      
      // pseudo data 0 and data 1
      FtkIMApi::set_register( channel, register_name, vec_pseudo_data0.at(ii) );
      FtkIMApi::set_register( channel, register_name, vec_pseudo_data1.at(ii) );
      
      // confirmation
      uint32_t value_of_pseudo_data;
      FtkIMApi::read_register( channel, register_name, value_of_pseudo_data );
      
      // check the failed to download
      if(vec_data_word.at(ii) != value_of_pseudo_data){
        ERS_LOG("Failed: " << ii << " " << hex << vec_data_word.at(ii) << " " << value_of_pseudo_data);
        tmp_indices_fail.push_back(ii);
        is_failed = true;
      }
    }
    // replace std::vector
    indices_fail.clear();
    indices_fail = tmp_indices_fail;
  }while(is_failed);
  
  ERS_INFO( m_name << " Finished downloading pseudo data on channel = " << channel);
}

void FtkIMApi::downloadAllPseudoData( const std::string& pseudoData_file, const int& input_rate ){
  size_t iConfPath     = pseudoData_file.rfind("/")+1;
  std::string confPath = pseudoData_file.substr(0, iConfPath);
  
  std::ifstream fin( pseudoData_file.c_str() );
  if(fin.fail()) {
    throw( daq::ftk::IOError(ERS_HERE, m_name , " can not open pseudo data file "+pseudoData_file) );
    return;
  }
  ERS_LOG("Done opening the list with pseudo data files");
  
  std::vector<string> vec_IM_PseudoData;
  std::string c1,c2;
  do{
    fin >> c1 >> c2;
    if(fin.eof())  continue;
    vec_IM_PseudoData.push_back(c2);
  } while(!fin.eof());
  ERS_LOG("Done reading pseudo data and filling vec_IM_PseudoData");
  
  if ( vec_IM_PseudoData.size() < 16 ) {
    throw( daq::ftk::IOError(ERS_HERE, m_name , " pseudo data file " + pseudoData_file + " is malformed. Needs to have one line for each IM lane." ) );
    return;
  }
  
  for(uint32_t ii=0; ii<16; ii++) {
    if(vec_IM_PseudoData.at(ii)!="###") {
      ERS_LOG("Dowloading "<<ii<<"-th IM pseudo data");
      FtkIMApi::download_pseudo_data( ii, confPath + vec_IM_PseudoData.at(ii), input_rate );
    } else {
      ERS_LOG(ii<<"-th IM Pseudo data is not provided. Skipping...");
    }     
  }
}

void  FtkIMApi::downloadAllPseudoData_OTF( const std::string& pseudoData_prefix, const std::map<uint32_t, uint32_t>& FMCInToRodIdMap, const int& input_rate ){
  for( uint32_t ii = 0; ii < 16; ii++) {
    if ( FMCInToRodIdMap.find(ii) != FMCInToRodIdMap.end() ){
      // download the pseudo data
      ERS_LOG("Dowloading "<<ii<<"-th IM pseudo data");
      
      char pdName[200];
      sprintf(pdName,"%s%i.dat", pseudoData_prefix.c_str(), FMCInToRodIdMap.find(ii)->second);
      FtkIMApi::download_pseudo_data( ii, pdName, input_rate );
      //downloadPseudoData(ii/2, ii%2,  pseudoData_prefix + FMCInToRodIdMap.find(ii)->second.second + ".dat", useBlockTransfer);
    }else{
      ERS_LOG(ii<<"-th IM Pseudo data is not provided. Skipping...");
    }     
  }
}

