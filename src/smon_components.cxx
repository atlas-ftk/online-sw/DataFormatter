#include "DataFormatter/smon_components.h"

smon_components::smon_components( bool is_SCT ) :
  input_from_LDC( 70, 40 ), 
  output_from_INFIFO( 107, 80 ), 
  output_from_OUTFIFO( 144, 80 ), 
  in_fifo_monitor( 21, 40, 10, 16384 ),  // depth is 16k
  rod_fifo_monitor( 25, 80, 10, 1024 ),  // depth is 1k
  data_fifo_monitor( 29, 80, 10, 1024 ),  // depth is 1k
  out_fifo_monitor( 33, 80, 10, 16384 ),   // depth is 8k if SCT else 16k
  xoff_monitor( 37, 40, 10 ), 
  BP_monitor( 39, 40, 10 ), 
  recover_monitor( 41, 80, 10 ), 
  truncation_monitor( 43, 80, 10 ),
  SCT_input_fifo_monitor( 200, 80, 10, 512, true ), 
  SCT_decoded_fifo_monitor( 203, 80, 10, 512, true ), 
  SCT_sorted_fifo_monitor( 206, 80, 10, 512, true ), 
  fillgap_monitor( 45, 80, 10)
{
}
smon_components::~smon_components(){}

void smon_components::Put(std::string str, int num){
  for(int ii=0; ii<num; ii++)printf("%s",str.c_str());
}
void smon_components::print_table1(int num){
  for(int ii=0; ii<num-1; ii++)
    printf("-");
  printf(":|");
}

void smon_components::PutEndl(std::string str, int num){
  for(int ii=0; ii<num; ii++){
    if(ii==num-1)printf("%s\n",str.c_str());
    else printf("%s",str.c_str());
  }
}
// char* smon_components::printf_with_number_of   ( uint32_t value ){ static char buf[20]; snprintf( buf, 20, "0x%x",  value ); return buf; }
// char* smon_components::printf_with_number_of_64( uint64_t value ){ static char buf[40]; snprintf( buf, 40, "0x%lx", value ); return buf; }
// char* smon_components::printf_percent( float value ){ static char buf[20]; snprintf( buf, 40, "%3.1f%%", value ); return buf; }
std::string smon_components::printf_with_number_of   ( const uint32_t value ){
  auto size = std::snprintf( nullptr, 0, "0x%x", value );
  std::string output( size + 1, '\0');
  std::snprintf( &output[0], size + 1, "0x%x",  value );
  return output;
}
std::string smon_components::printf_with_number_of_64( const uint64_t value ){
  auto size = std::snprintf( nullptr, 0, "0x%lx", value );
  std::string output( size + 1, '\0');
  std::snprintf( &output[0], size + 1, "0x%lx",  value );
  return output;
}
std::string smon_components::printf_percent( const float value ){
  auto size = std::snprintf( nullptr, 0, "%3.1f%%", value );
  std::string output( size + 1, '\0');
  std::snprintf( &output[0], size + 1, "%3.1f%%",  value );
  return output;
}
  

void smon_components::show_title(){
  printf("================================================================================\n");
  printf("==     _____    ___    ___      _____    ___    ___    _____    ___     _     ==\n");
  printf("==    |_   _|  |   \\  /   |    /  __ \\  |   \\  /   |  / ___ \\  |   \\   | |    ==\n");
  printf("==      | |    | |\\ \\/ /| |    \\ \\  \\_\\ | |\\ \\/ /| | | |   | | | |\\ \\  | |    ==\n");
  printf("==      | |    | | \\  / | |    _ \\ \\    | | \\  / | | | |   | | | | \\ \\ | |    ==\n");
  printf("==     _| |_   | |  \\/  | |   \\ \\__\\ \\  | |  \\/  | | | |___| | | |  \\ \\| |    ==\n");
  printf("==    |_____|  |_|      |_|    \\_ ___/  |_|      |_|  \\_____/  |_|   \\___|    ==\n");
  printf("==                                                                            ==\n");
  printf("================================================================================\n");
  printf("                                                       IM Status Monitoring Tool\n");  
}
void smon_components::show_simple_title(){
  printf("__________________________________________________________________ IM Status Monitoring Tool __________________________________________________________________\n");  
}
void smon_components::show_smon_raw_title(){
  for(int i = 0; i < 12*6; ++i) printf("_");
  printf("%12s raw output %12s", "", "");
  for(int i = 0; i < 12*7; ++i) printf("_");
  printf("\n");
}
void smon_components::read_32bitdata(std::vector<uint32_t> input_vec_words){
  vec_words.clear();
  for(int i = 0, size = input_vec_words.size(); i < size; ++i){
    auto val = input_vec_words.at(i);
    vec_words.push_back( val );
  }
}
void smon_components::rd_EvInfo(){
  for(int cur_pre = 0 ; cur_pre < 2; ++cur_pre){
    // 0: RunNumber 1: L1ID 2: #words 3: word 4: time from BOF 5: #LostWords
    uint32_t init_EvInfo      = 0 + (cur_pre * 0X10) ; const int num_EvInfo=6;
    uint32_t index_RunNumber  = 0 + init_EvInfo;
    uint32_t index_L1ID       = 1 + init_EvInfo;
    uint32_t index_nWords     = 3 + init_EvInfo;
    uint32_t index_word       = 4 + init_EvInfo;
    uint32_t index_time       = 5 + init_EvInfo;
    uint32_t index_nLostWords = 6 + init_EvInfo;
    
    uint32_t evInfo[num_EvInfo];
    evInfo[0] = vec_words.at(index_RunNumber);  // RunNumber
    evInfo[1] = vec_words.at(index_L1ID);       // L1ID
    evInfo[2] = vec_words.at(index_nWords);     // nWords
    evInfo[3] = vec_words.at(index_word);       // word
    evInfo[4] = vec_words.at(index_time);       // time
    evInfo[5] = vec_words.at(index_nLostWords); // nLostWords
    
    for(int ii=0; ii<num_EvInfo; ii++){
      EvInfo[cur_pre][ii] = evInfo[ii];
    }
    // state 
    uint32_t index_state = 0X2 + (cur_pre * 0X10); const int num_state=8;
    std::string state[num_state];
    // state
    if( vec_words.at(index_state) != 0Xdeadbeef ){
      for(int ii=0; ii< num_state; ii++){
        if( ((vec_words.at(index_state) & (0Xf << (7-ii)*4)) >> (7-ii)*4) == 0X0 ) state[ii] = "---";
        if( ((vec_words.at(index_state) & (0Xf << (7-ii)*4)) >> (7-ii)*4) == 0X1 ) state[ii] = "HEADER";
        if( ((vec_words.at(index_state) & (0Xf << (7-ii)*4)) >> (7-ii)*4) == 0X2 ) state[ii] = "DATA";
        if( ((vec_words.at(index_state) & (0Xf << (7-ii)*4)) >> (7-ii)*4) == 0X4 ) state[ii] = "TRAILER";
        if( ((vec_words.at(index_state) & (0Xf << (7-ii)*4)) >> (7-ii)*4) == 0X8 ) state[ii] = "IDLE";
      }
    }else{
      for(int ii=0; ii<num_state; ii++){
        data_state[cur_pre][ii] = "0Xdeadbeef";
      }
    }
    
    int none_counter=0;
    for(int ii=0; ii<num_state; ii++) if(state[ii] == "---") none_counter++;
    for(int ii=0; ii<num_state; ii++){
      data_state[cur_pre][ii] = (ii < num_state - none_counter) ? state[none_counter+ii] : "---";
    }
  }

  // other general info
  RODID               = vec_words.at(11); // 
  is_IBL              = vec_words.at(23) >> 31 ; // 
  // nEvent_nonReset     = (vec_words.at(160) << 32) + (vec_words.at(161)); //total_number_of_b0f_early_process
  // nEvent_sent         = (vec_words.at(162) << 32) + (vec_words.at(163)); //total_number_of_b0f_early_process
  nEvent_nonReset     = vec_words.at(160);
  nEvent_sent         = vec_words.at(162) ;
  nEvent_nonReset     = nEvent_nonReset << 32;
  nEvent_sent         = nEvent_sent     << 32;
  nEvent_nonReset    += vec_words.at(161);
  nEvent_sent        += vec_words.at(163);
  
  
  nBP                 =  vec_words.at(164);                              // smon_nholddown
  nfillgap            =  vec_words.at(165);                              // smon_nfillgap
  nover_fillgap_limit =  vec_words.at(166);                              // smon_nover_fillgap_limit
  ntimeout            =  vec_words.at(167);                              // smon_ntimeout
  nblocked_readout    =  vec_words.at(168);                              // smon_nblocked_readout
  nEvent              =  vec_words.at(170);                              // smon_event_counter
  ndupli_words        =  vec_words.at(171);                              // smon_ndupli_hit
  cnt_timeout         = (vec_words.at(172) & 0x0000FFFF);                //  cnt_timeout
  cnt_long_blocked    = (vec_words.at(172) & 0xFFFF0000) >> 16;          //  cnt_long_blocked_readout
  ndupli_out          = (vec_words.at(173) & 0x0000FFFF);                //  smon_ndupli_module_in
  ndupli_in           = (vec_words.at(173) & 0xFFFF0000) >> 16;          //  smon_ndupli_module_out
  outfifo_lastL1ID    = vec_words.at(12);                                // last L1ID written in outfifo
  sent_L1ID           = vec_words.at(13);                                // L1ID sent to DF
  xoff_enable         = std::bitset<32>(vec_words.at(0xf1))[31];         // xoff enable
  xoff_auto_disable   = std::bitset<32>(vec_words.at(0xf1))[30];         // xoff auto disable
  xoff_counter        = vec_words.at(0xf1) & (0XFFFF);                   // 26-0 bit in almost full counter

}
void smon_components::rd_dupli_words(){
  // ok
  // dupli hit
  const int n_duplicheck_words = 4;
  for(int ii = 0; ii < n_duplicheck_words; ii++)
    dupli_check_word[ii] = vec_words.at(60+ii);

  // check dupli hit bit
  for(int ii = 0; ii < 32; ii++){
    error_words[ii] = std::bitset<32>(vec_words.at(14))[31-ii];
  }
  is_dupli_words = (error_words[0] == 1) ? 1 : 0;
}

void smon_components::rd_word_state(){
  for(int level = 0; level < 4; ++level ){
    // 0: pre. #words (most sig. 4bit) 1: pre. ClusterSize (least sig. 4bit) 2: pre. time (bof-bof) 3:pre. event rate 4: pre. time (bof-eof) 5: pre. latency
    uint32_t init_pre_vars    = 0X20 + ((level-1) * 0X04); const int num_pre_vars=6;
    uint32_t index_nWordsClus = 0 + init_pre_vars;
    uint32_t index_time_b_b   = 1 + init_pre_vars;
    uint32_t index_time_b_e   = 2 + init_pre_vars;
    uint32_t index_latency    = 3 + init_pre_vars;
    
    uint32_t tmp_variables[num_pre_vars];
    tmp_variables[0] = ((vec_words.at(index_nWordsClus) & (0Xffff << 16)) >> 16); // nWords
    tmp_variables[1] = (vec_words.at(index_nWordsClus) & 0Xffff); // ClusSize
    tmp_variables[2] = vec_words.at(index_time_b_b); // time_b_b
    tmp_variables[3] = uint32_t(1.e-3/(vec_words.at(index_time_b_b)*25e-9)); // event_rate
    tmp_variables[4] = vec_words.at(index_time_b_e); // time_b_e
    tmp_variables[5] = vec_words.at(index_latency); // latency
    
    for(int ii=0; ii<num_pre_vars; ii++){
      word_states[level][ii] = tmp_variables[ii];
    }
  }
}

void smon_components::rd_fifo(int fifo_seed){
  // 0: infifo status 1: sysfifo status 2: rodfifo status 3: datafifo status 
  // 4: buffifo status 5: outfifo status
  // 6: decoded fifo status 7: lvl1id fifo status
  // 8: cluster out fifo status 9: parallel fifo status
  // 10: grid system out fifo status 11: centroid calculation status
  uint32_t index_fifo = 0X30 + fifo_seed; const int num_fifo = 7;
  if(fifo_seed<0 || fifo_seed>11){
    fprintf(stderr,"Please use fifo_seed between 0 and 11.\n");
    return;
  }

  //  std::ostringstream stream_fifo_status[num_fifo];
  uint32_t fifo_status[num_fifo];
  fifo_status[0] = std::bitset<32>(vec_words.at(index_fifo))[31]; // was full
  fifo_status[1] = std::bitset<32>(vec_words.at(index_fifo))[30]; // is full
  fifo_status[2] = std::bitset<32>(vec_words.at(index_fifo))[29]; // was almost full
  fifo_status[3] = std::bitset<32>(vec_words.at(index_fifo))[28]; // is almost full
  fifo_status[4] = std::bitset<32>(vec_words.at(index_fifo))[27]; // is empty
  // fifo_status[5] = (vec_words.at(index_fifo) & (0X7FFFFF0))>>4; // 34-12 bit in almost full counter
  fifo_status[5] = vec_words.at(index_fifo) & (0X7FFFFFF); // 26-0 bit in almost full counter

  for(int ii=0; ii<num_fifo; ii++){
    if(fifo_seed ==  0) infifo_status[ii]          = fifo_status[ii];
    if(fifo_seed ==  1) sysfifo_status[ii]         = fifo_status[ii];
    if(fifo_seed ==  2) rodfifo_status[ii]         = fifo_status[ii];
    if(fifo_seed ==  3) datafifo_status[ii]        = fifo_status[ii];
    if(fifo_seed ==  4) buffifo_status[ii]         = fifo_status[ii];
    if(fifo_seed ==  5) outfifo_status[ii]         = fifo_status[ii];
    if(fifo_seed ==  6) decodedfifo_status[ii]     = fifo_status[ii];
    if(fifo_seed ==  7) lvl1idfifo_status[ii]      = fifo_status[ii];
    if(fifo_seed ==  8) clusteroutfifo_status[ii]  = fifo_status[ii];
    if(fifo_seed ==  9) parallelfifo_status[ii]    = fifo_status[ii];
    if(fifo_seed ==  0) gridsysoutfifo_status[ii]  = fifo_status[ii];
    if(fifo_seed == 11) centroidcalc_status[ii]    = fifo_status[ii];

  }
}
void smon_components::rd_fifo_each_parallel(int fifo_seed){
  uint32_t index_fifo = 0Xd3 + fifo_seed; const int num_fifo = 7;
  if(fifo_seed<0 || fifo_seed>7){
    fprintf(stderr,"Please use fifo_seed(parallel) between 0 and 7.\n");
    return;
  }

  //  std::ostringstream stream_fifo_status[num_fifo];
  uint32_t fifo_status[num_fifo];
  fifo_status[0] = std::bitset<32>(vec_words.at(index_fifo))[31]; // was full
  fifo_status[1] = std::bitset<32>(vec_words.at(index_fifo))[30]; // is full
  fifo_status[3] = std::bitset<32>(vec_words.at(index_fifo))[28]; // is almost full
  fifo_status[4] = std::bitset<32>(vec_words.at(index_fifo))[27]; // is empty
  fifo_status[5] = vec_words.at(index_fifo) & (0XFFFF); // 16-0 bit in almost full counter
  fifo_status[2] = (fifo_status[5] > 0 ) ? 1 : 0;
  fifo_status[6] = (vec_words.at(index_fifo) & (0x00ff0000)) >> 16; // 

  for(int ii=0; ii<num_fifo; ii++){
    parallel_fifo_status_each[fifo_seed][ii] =  fifo_status[ii];
  }
}
void smon_components::rd_fifo_each_clusterout(int fifo_seed){
  uint32_t index_fifo = 0Xdb + fifo_seed; const int num_fifo = 7;
  if(fifo_seed<0 || fifo_seed>7){
    fprintf(stderr,"Please use fifo_seed(clusterout) between 0 and 7.\n");
    return;
  }

  //  std::ostringstream stream_fifo_status[num_fifo];
  uint32_t fifo_status[num_fifo];
  fifo_status[0] = std::bitset<32>(vec_words.at(index_fifo))[31]; // was full
  fifo_status[1] = std::bitset<32>(vec_words.at(index_fifo))[30]; // is full
  fifo_status[3] = std::bitset<32>(vec_words.at(index_fifo))[28]; // is almost full
  fifo_status[4] = std::bitset<32>(vec_words.at(index_fifo))[27]; // is empty
  fifo_status[5] = vec_words.at(index_fifo) & (0XFFFF); // 26-0 bit in almost full counter
  fifo_status[2] = (fifo_status[5] > 0 ) ? 1 : 0;
  fifo_status[6] = (vec_words.at(index_fifo) & (0x00ff0000)) >> 16; // 

  for(int ii=0; ii<num_fifo; ii++){
    clusterout_fifo_status_each[fifo_seed][ii] =  fifo_status[ii];
  }
}

void smon_components::rd_fifo_occupancy(){
  infifo_status[6]         = (vec_words.at(67) & 0x3fff0000) >> 16 ;// infifo
  outfifo_status[6]        = (vec_words.at(67) & 0x00003fff); // outfifo
  rodfifo_status[6]        = (vec_words.at(68) & 0x3fff0000) >> 16 ;// infifo
  datafifo_status[6]       = (vec_words.at(68) & 0x00003fff); // outfifo
  gridsysoutfifo_status[6] = (vec_words.at(69) & 0x00003fff); // outfifo
}

void smon_components::rd_fifo_fraction(){
  // time fractions of FIFO status
  // 0~3: average over 1sec, 4~7: average over 60sec
  // 0,4: infifo empty  1,5: infifo busy 
  // 2,6: outfifo empty 3,7: outfifo busy 
  uint32_t index_fifo = 0X18; const int num_info = 8;

  // information of 1second average
  for(int ii=0; ii<num_info; ii++){
    fifo_fraction_information[ii] = vec_words.at(index_fifo+ii); // empty fraction
    if(ii<4) {fifo_fraction_information[ii] = (fifo_fraction_information[ii] / 40000000) * 100; // average over 1sec
    }
    else     fifo_fraction_information[ii] = (fifo_fraction_information[ii] / 2400000000) * 100; // average over 60sec
  }

}

void smon_components::rd_errors(){
  // 0: current, 1: previous, 2: previous2, 3: previous3,
  Error_Register       = vec_words.at(14);// error register
  clock_count_from_rst = vec_words.at(15);// clock cycle count from reset
  nrod_status_word0    = (vec_words.at( 107 ) & 0xFFFF0000 ) >> 16;
  nrod_status_word1    = (vec_words.at( 107 ) & 0x0000FFFF ) ;
  for(int i = 0; i < 4; ++i){
    int this_idx = 108 + i * 5;
    rod_status_word0[i]   = vec_words.at( this_idx + 0 );
    rod_status_word1[i]   = vec_words.at( this_idx + 1 );
    ID_cluster_errors[i]  = vec_words.at( this_idx + 2 );
    grid_system_errors[i] = vec_words.at( this_idx + 3 );
    l1id_error[i]         = vec_words.at( this_idx + 4 );
  }
  my_Error_Bit        = vec_words.at(64);// Error Bit
  my_Error_Bit_quick  = vec_words.at(65);// Error Bit connected simultaneously
  my_Error_Bit_quick2 = vec_words.at(66);// Error Bit2 connected simultaneously
  for(int ii = 0; ii<32; ii++ ){
    bin_my_Error_Bit[ii] = std::bitset<32>(vec_words.at(86))[31-ii];
    bin_my_Error_Bit_quick[ii]  = std::bitset<32>(vec_words.at(87))[31-ii];
    bin_my_Error_Bit_quick2[ii] = std::bitset<32>(vec_words.at(88))[31-ii];
  }
  
}

void smon_components::rd_input_rate_b0f_b0f(){
  uint32_t index_hist_b0f_b0f = 0Xc1;
  hist_input_rate_b0f_b0f[0] = vec_words.at( index_hist_b0f_b0f + 0 );
  hist_input_rate_b0f_b0f[1] = vec_words.at( index_hist_b0f_b0f + 1 );
  hist_input_rate_b0f_b0f[2] = vec_words.at( index_hist_b0f_b0f + 2 );
  hist_input_rate_b0f_b0f[3] = vec_words.at( index_hist_b0f_b0f + 3 );
  hist_input_rate_b0f_b0f[4] = vec_words.at( index_hist_b0f_b0f + 4 );
  hist_input_rate_b0f_b0f[5] = vec_words.at( index_hist_b0f_b0f + 5 );
  hist_input_rate_b0f_b0f[6] = vec_words.at( index_hist_b0f_b0f + 6 );
  hist_input_rate_b0f_b0f[7] = vec_words.at( index_hist_b0f_b0f + 7 );
  hist_input_rate_b0f_b0f[8] = vec_words.at( index_hist_b0f_b0f + 8 );
}
void smon_components::rd_input_rate_max_idle(){
  uint32_t index_hist_max_idle = 0Xca;
  hist_input_rate_max_idle[0] = vec_words.at( index_hist_max_idle + 0 );
  hist_input_rate_max_idle[1] = vec_words.at( index_hist_max_idle + 1 );
  hist_input_rate_max_idle[2] = vec_words.at( index_hist_max_idle + 2 );
  hist_input_rate_max_idle[3] = vec_words.at( index_hist_max_idle + 3 );
  hist_input_rate_max_idle[4] = vec_words.at( index_hist_max_idle + 4 );
  hist_input_rate_max_idle[5] = vec_words.at( index_hist_max_idle + 5 );
  hist_input_rate_max_idle[6] = vec_words.at( index_hist_max_idle + 6 );
  hist_input_rate_max_idle[7] = vec_words.at( index_hist_max_idle + 7 );
  hist_input_rate_max_idle[8] = vec_words.at( index_hist_max_idle + 8 );
}

void smon_components::rd_data_merger_monitor(){
  //data_merger_monitor
  data_merger_FSM  = (vec_words.at( 150 ) & 0x000f0000) >> 16 ;
  data_merger_out  =  vec_words.at( 245 );
  data_merger_L1ID =  vec_words.at( 246 );
  data_merger_end_event_tag_reg_out =  (vec_words.at( 240 ) & 0xff0000000) >> 24;
  
  data_merger_total_active_module_reg_in      =  (vec_words.at( 241 ) & 0xff000000) >> 24;
  data_merger_total_active_module_reg_out     =  (vec_words.at( 241 ) & 0x00ff0000) >> 16;
  data_merger_total_start_event_reg_in        =  (vec_words.at( 241 ) & 0x0000ff00) >>  8;
  data_merger_total_start_event_reg_out       =  (vec_words.at( 241 ) & 0x000000ff) >>  0;

  data_merger_total_start_event_encoded       =  (vec_words.at( 242 ) & 0xff000000) >> 24;
  data_merger_active_start_event              =  (vec_words.at( 242 ) & 0x00ff0000) >> 16;
  data_merger_active_end_event                =  (vec_words.at( 242 ) & 0x0000ff00) >>  8;
  data_merger_parallel_clusters_rd_int        =  (vec_words.at( 242 ) & 0x000000ff) >>  0;

  data_merger_FSM_detail_monitor              =  (vec_words.at( 243 ) & 0xf0000000) >> 28;
  data_merger_slv_active_module_number        =  (vec_words.at( 243 ) & 0x0f000000) >> 24;
  data_merger_flags                           =  (vec_words.at( 243 ) & 0x00ff0000) >> 16;
  data_merger_Parallel_clusters_valid         =  (vec_words.at( 243 ) & 0x0000ff00) >>  8;
  data_merger_Parallel_clusters_almost_empty  =  (vec_words.at( 243 ) & 0x000000ff) >>  0;

}
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++//

void smon_components::wr_globalInfo(const uint32_t& fpga_id, const uint32_t& channel_id,
                                     const uint32_t& slink_enable_value, 
                                     const uint32_t& pseudo_value, 
                                     const bool slink_is_up,
                                     const bool slink_is_slave,
                                     const bool is_bp_ignored,
                                     const bool is_test_im
                                    ){
  uint32_t serial_ch = 0;
  serial_ch = 2*fpga_id + channel_id;
  printf("%29s||","FTK_IM channel >>>>>>>>>>>>");
  printf(" %-20s is %d\n", "channel", serial_ch);
  printf("%29s||"," ");
  printf(" %-20s is 0x%08x\n", "RODID", vec_words.at(11) );
  printf("%29s||"," ");
  if ( is_IBL == 1 ) printf(" %-20s is %s\n", "IBL Mode", "Enabled" );
  else               printf(" %-20s is %s\n", "IBL Mode", "Disabled" );
  

  if( is_test_im ){
    printf("%29s||", "SLink/Pseudo >>>>>>>>>>>>");
    if(slink_is_up) printf(" %-20s is %s", "Link", "UP");
    else            printf(" %-20s is %s", "Link", "DONW");
    if(slink_is_slave) printf("%12s", "(Slave mode)\n" );
    else               printf("%12s", "(Normal mode)\n" );
    printf("%29s||", " ");
    printf(" %-20s is 0x%1x/0x%1x\n", "Slink/Psudo value", slink_enable_value, pseudo_value);
    printf("%29s||"," ");
    if(is_bp_ignored)  printf(" %-20s is %s\n", "Backpressure", "Ignored");
    else               printf(" %-20s is %s\n", "Backpressure", "Received");
  }
}
void smon_components::wr_xoff_control(){
  //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++//
  printf("%29s||", "XOFF monitor >>>>>>>>>>>>");
  if( xoff_enable == 1 ) printf(" %-20s is %s\n", "XOFF", "active");
  else                   printf(" %-20s is %s\n", "XOFF", "non-active");
  printf("%29s||", " ");
  if( xoff_auto_disable ) printf(" %-20s is %s\n", "audo-disable", "active");
  else                    printf(" %-20s is %s\n", "audo-disable", "non-active");
  printf("%29s||", " ");
  printf(" %-20s is %10s\n", "#of XOFF", printf_with_number_of( xoff_counter).c_str() );
}
void smon_components::wr_RecoverInfo(){

  //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++//
  printf("%29s||", "Recovery Info >>>>>>>>>>>>");
  printf(" %-20s is %10s\n", "#of BP", printf_with_number_of( nBP ).c_str() );
  printf("%29s||", "timeout");
  printf(" %-20s is %10s/%10s\n", "#of/count", printf_with_number_of( ntimeout).c_str(), printf_with_number_of( cnt_timeout).c_str() );
  
  printf("%29s||", "long blocked");
  printf(" %-20s is %10s/%10s\n", "#of/count", printf_with_number_of( nblocked_readout).c_str(), printf_with_number_of( cnt_long_blocked).c_str() );
  
  printf("%29s||", "fillgap");
  printf(" %-20s is %10s/%10s\n", "#of/limit", printf_with_number_of( nfillgap).c_str(), printf_with_number_of( nover_fillgap_limit).c_str() );

  printf("%29s||", "dupli module");
  printf(" %-20s is %10s/%10s\n", "#of in/out", printf_with_number_of( ndupli_in).c_str(), printf_with_number_of( ndupli_out ).c_str() );
  printf("%29s||\n", " " );

}
void smon_components::wr_GeneralInfo(){
  //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++//
  printf("%29s||", "General >>>>>>>>>>>>");
  printf("%20s|", "Recieve");
  printf("%20s|", "INFIFO");
  printf("%20s|\n", "Sent");
  printf("%29s||", "#of Evt");
  printf("%20lu|", nEvent_nonReset);
  printf("%20d|",  nEvent);
  printf("%20lu|\n",  nEvent_sent);
  printf("%29s||", "L1ID");
  printf("%12s%08x|",   "0x", sent_L1ID);
  printf("%20s|",  "N/A");
  printf("%12s%08x|\n", "0x", outfifo_lastL1ID);
  printf("%29s||\n", " " );
}
void smon_components::wr_EvtInfo(){
  //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++//
  
  printf("%29s||", "Evt Info >>>>>>>>>>>>");
  printf("%8s|", "Run");
  printf("%11s|", "L1ID");
  printf("%7s|", "#word");
  printf("%12s|", "currnet word");
  printf("%16s|", "Time");
  printf("%16s|", "#lost word");
  printf("%8s|", "1st");
  printf("%8s|", "2nd");
  printf("%8s|", "3rd");
  printf("%8s|", "4th");
  printf("%8s|\n", "5th");

  printf("%29s||", "On-going");
  printf("%8d|",           EvInfo[0][0]);// 
  printf("%3s%08x|", "0x", EvInfo[0][1]);// 
  printf("%7d|",           EvInfo[0][2]);// 
  printf("%4s%08x|", "0x", EvInfo[0][3]);// 
  printf("%16d|",          EvInfo[0][4]);// 
  printf("%16d|",          EvInfo[0][5]);// 
  printf("%8s|",             data_state[0][0].c_str());
  printf("%8s|",             data_state[0][1].c_str());
  printf("%8s|",             data_state[0][2].c_str());
  printf("%8s|",             data_state[0][3].c_str());
  printf("%8s|\n",           data_state[0][4].c_str());
  
  printf("%29s||", "Done");
  printf("%8d|",           EvInfo[1][0]);// 
  printf("%3s%08x|", "0x", EvInfo[1][1]);// 
  printf("%7d|",           EvInfo[1][2]);// 
  printf("%4s%08x|", "0x", EvInfo[1][3]);// 
  printf("%16d|",          EvInfo[1][4]);// 
  printf("%16d|",          EvInfo[1][5]);// 
  printf("%8s|",             data_state[1][0].c_str());
  printf("%8s|",             data_state[1][1].c_str());
  printf("%8s|",             data_state[1][2].c_str());
  printf("%8s|",             data_state[1][3].c_str());
  printf("%8s|\n",           data_state[1][4].c_str());
  
}
void smon_components::wr_event_rate(){
  
  // current
  printf("%29s||\n", " " );
  printf("%29s||","Evt Rate >>>>>>>>>>>>");
  printf("%8s|", "#word");
  printf("%11s|", "Rate(kHz)");
  printf("%12s|", "b0f-b0f");
  printf("%12s|", "b0f-e0f");
  printf("%6s|", "#clstr");
  printf("%11s|", "latency");
  printf("\n");
  printf("%29s||","Avg(256)");
  printf("%8d|",             word_states[0][0] ); 
  printf("%8dkHz|",          word_states[0][3] ); 
  printf("%12d|",            word_states[0][2] ); 
  printf("%12d|",            word_states[0][4] ); 
  printf("%6d|",             word_states[0][1] ); 
  printf("%3s%08x|\n", "0x", word_states[0][5] ); 
  // prev1
  printf("%29s||","Previous1");
  printf("%8d|",             word_states[1][0] ); 
  printf("%8dkHz|",          word_states[1][3] ); 
  printf("%12d|",            word_states[1][2] ); 
  printf("%12d|",            word_states[1][4] ); 
  printf("%6d|",             word_states[1][1] ); 
  printf("%3s%08x|\n", "0x", word_states[1][5] ); 
  // prev2
  printf("%29s||","Previous2");
  printf("%8d|",             word_states[2][0] ); 
  printf("%8dkHz|",          word_states[2][3] ); 
  printf("%12d|",            word_states[2][2] ); 
  printf("%12d|",            word_states[2][4] ); 
  printf("%6d|",             word_states[2][1] ); 
  printf("%3s%08x|\n", "0x", word_states[2][5] ); 
  // prev3
  printf("%29s||","Previous3");
  printf("%8d|",             word_states[3][0] ); 
  printf("%8dkHz|",          word_states[3][3] ); 
  printf("%12d|",            word_states[3][2] ); 
  printf("%12d|",            word_states[3][4] ); 
  printf("%6d|",             word_states[3][1] ); 
  printf("%3s%08x|\n", "0x", word_states[3][5] ); 
  printf("%29s||\n", " " );
}

void smon_components::wr_ErrorBit(){
  
  printf("%29s||", "IM Error >>>>>>>>>>>>");
  printf("%12s|", "err bit");
  printf("%12s|", "quick1");
  printf("%12s|", "quick2");
  printf("%12s|", "err reg");
  printf("%12s|\n", "clk fomr rst");
  printf("%29s||", "");
  printf("%4s%08x|",   "0x", my_Error_Bit);
  printf("%4s%08x|",   "0x", my_Error_Bit_quick);
  printf("%4s%08x|",   "0x", my_Error_Bit_quick2);
  printf("%4s%08x|",   "0x", Error_Register);
  printf("%4s%08x|\n", "0x", clock_count_from_rst);
  
  
  if( bin_my_Error_Bit_quick[31] == 1) printf("%29s%s", " 0 : ", " Inner Detector status element error\n");
  if( bin_my_Error_Bit_quick[30] == 1) printf("%29s%s", " 1 : ", " lost words\n");
  if( bin_my_Error_Bit_quick[29] == 1) printf("%29s%s", " 2 : ", " not continuous L1ID\n");
  if( bin_my_Error_Bit_quick[28] == 1) printf("%29s%s", " 3 : ", " skipped event\n");
  if( bin_my_Error_Bit_quick[27] == 1) printf("%29s%s", " 4 : ", " duplicated hit\n");
  if( bin_my_Error_Bit_quick[26] == 1) printf("%29s%s", " 5 : ", " invalid hit (only pixel)\n");
  if( bin_my_Error_Bit_quick[25] == 1) printf("%29s%s", " 6 : ", " rod fifo is full(for debug)\n");
  if( bin_my_Error_Bit_quick[24] == 1) printf("%29s%s", " 7 : ", " big event\n");
  
  //if( bin_my_Error_Bit_quick[23] == 1) printf("%29s%s", " \n");
  if( bin_my_Error_Bit_quick[22] == 1) printf("%29s%s", " (debug : ", " decoded fifo full (Pix))\n");
  if( bin_my_Error_Bit_quick[21] == 1) printf("%29s%s", " (debug : ", " sys fifo lostword (Pix)) \n");
  if( bin_my_Error_Bit_quick[20] == 1) printf("%29s%s", " (debug : ", " decoded fifo lostword (Pix)) \n");
  
  //if( bin_my_Error_Bit_quick[19] == 1) printf("%29s%s", "(debug : ", " ) \n");
  //if( bin_my_Error_Bit_quick[18] == 1) printf("%29s%s", "(debug : ", " ) \n");
  //if( bin_my_Error_Bit_quick[17] == 1) printf("%29s%s", "(debug : ", " ) \n");
  //if( bin_my_Error_Bit_quick[16] == 1) printf("%29s%s", "(debug : ", " ) \n");
  
  //if( bin_my_Error_Bit_quick[15] == 1) printf("%29s%s", "(debug : ", " ) \n");
  if( bin_my_Error_Bit_quick[14] == 1) printf("%29s%s", " (debug : ", " badl1id ev or lostword ev) \n");
  if( bin_my_Error_Bit_quick[13] == 1) printf("%29s%s", " (debug : ", " Recover event) \n");
  if( bin_my_Error_Bit_quick[12] == 1) printf("%29s%s", " (debug : ", " long blocked readout) \n");
   
  // if( bin_my_Error_Bit_quick[11] == 1) printf("%29s%s", "(debug : ", " ) \n");
  // if( bin_my_Error_Bit_quick[10] == 1) printf("%29s%s", "(debug : ", " ) \n");
  // if( bin_my_Error_Bit_quick[ 9] == 1) printf("%29s%s", "(debug : ", " ) \n");
  // if( bin_my_Error_Bit_quick[ 8] == 1) printf("%29s%s", "(debug : ", " ) \n");
  
  // if( bin_my_Error_Bit_quick[ 7] == 1) printf("%29s%s", "(debug : ", " ) \n");
  // if( bin_my_Error_Bit_quick[ 6] == 1) printf("%29s%s", "(debug : ", " ) \n");
  if( bin_my_Error_Bit_quick[ 5] == 1) printf("%29s%s", " (debug : ", " outfifo almost full fillgap) \n");
  if( bin_my_Error_Bit_quick[ 4] == 1) printf("%29s%s", " (debug : ", " outfifo almost full fifo) \n");

  if( bin_my_Error_Bit_quick[ 3] == 1) printf("%29s%s", " (debug : ", " HOLD DOWN) \n");
  if( bin_my_Error_Bit_quick[ 2] == 1) printf("%29s%s", " (debug : ", " timeout) \n");

  if( bin_my_Error_Bit_quick2[25] == 1) printf("%29s%s", " (debug : ", " Dupli Hit  in output) \n");
  if( bin_my_Error_Bit_quick2[26] == 1) printf("%29s%s", " (debug : ", " Dupli Mod  in input) \n");
  if( bin_my_Error_Bit_quick2[27] == 1) printf("%29s%s", " (debug : ", " Dupli Mod  in output) \n");
  if( bin_my_Error_Bit_quick2[ 3] == 1) printf("%29s%s", " (debug : ", " Packet err of e0f-b0f) \n");
  if( bin_my_Error_Bit_quick2[ 2] == 1) printf("%29s%s", " (debug : ", " Packet err of ctrl word) \n");
  if( bin_my_Error_Bit_quick2[ 1] == 1) printf("%29s%s", " (debug : ", " Packet err of header) \n");
  if( bin_my_Error_Bit_quick2[ 0] == 1) printf("%29s%s", " (debug : ", " Packet err of trailer) \n");
  //printf("%29s||\n", " " );

}
void smon_components::wr_dupli_words(){
  // const int num_info=2;
  
  printf("%29s||", "dupli words >>>>>>>>>>>>");
  if( is_dupli_words == 1 ) printf(" DUPLICATED WORDS!!!!\n");
  else                      printf(" No duplicaated words \n");
  
  printf("%29s||"," word 0");
  printf("%3s%08x\n","0x",dupli_check_word[0]);
  printf("%29s||"," word 1");
  printf("%3s%08x\n","0x",dupli_check_word[0]);
  printf("%29s||"," word 2");
  printf("%3s%08x\n","0x",dupli_check_word[0]);
  printf("%29s||"," word 3");
  printf("%3s%08x\n","0x",dupli_check_word[0]);
  
}


void smon_components::printf_fifo_status( uint32_t fifo_info[7] ){
  // current status of fifo
  if      ( fifo_info[4] == 1) { // empty
    printf("%12s|", "Empty");
  }else if( fifo_info[3] == 1) { // almost full
    printf("%12s|", "Almost Full");
  }else if( fifo_info[1] == 1) { // full 
    printf("%12s|", "Full");
  }else{
    printf("%12s|", "Not Full");
  }
}
void smon_components::printf_fifo_occupancy( uint32_t fifo_wr_data_count, int data_width ){
  
  if( data_width > 0 ){
    float v1 = float(data_width);
    float v2 = float(fifo_wr_data_count);
    float occupancy = float( v2 / v1 ) * 100.;
    printf( "%11.1f%%|" , occupancy );
  }else{
    printf( "%12s|" , "N/A" );
  }
}
void smon_components::wr_fifo( int channel){

  
  printf("%29s||", " Busy fraction >>>>>>>>>>>>" );
  printf("%25s|",  "IN Empty/Busy" );
  printf("%25s|\n", "OUT Empty/Busy" );
  printf("%29s||", "1sec avg.");
  printf("%10.1f%% / %10.1f%%|",   fifo_fraction_information[0], fifo_fraction_information[1]);
  printf("%10.1f%% / %10.1f%%|\n", fifo_fraction_information[2], fifo_fraction_information[3]);
  printf("%29s||", "1min avg.");
  printf("%10.1f%% / %10.1f%%|",   fifo_fraction_information[4], fifo_fraction_information[5]);
  printf("%10.1f%% / %10.1f%%|\n", fifo_fraction_information[6], fifo_fraction_information[7]);
  printf("%29s||\n", " " );
  
  
  // for Pix/IBL : IN - ROD - SYS - DECODED - L1ID - Para - Centroid - ClusterOut - Data - OUT
  // for SCT     : IN - ROD - SYS -                                               - Data - OUT
  if( channel == 0 ){ // Pix/IBL
    printf("%29s||", " FIFO >>>>>>>>>>>>" );
    printf("%12s|", "IN" );
    printf("%12s|", "ROD" );
    printf("%12s|", "SYS" );
    printf("%12s|", "Decoded" );
    printf("%12s|", "L1ID" );
    printf("%12s|", "Parallel" );
    printf("%12s|", "Centroid" );
    printf("%12s|", "Cluster" );
    printf("%12s|", "GridOut" );
    printf("%12s|", "Data" );
    printf("%12s|", "OUT" );
    printf("\n");
    //-- status
    printf("%29s||", "Status");
    printf_fifo_status( infifo_status );
    printf_fifo_status( rodfifo_status );
    printf_fifo_status( sysfifo_status );
    printf_fifo_status( decodedfifo_status );
    printf_fifo_status( lvl1idfifo_status );
    printf_fifo_status( parallelfifo_status );//
    printf_fifo_status( centroidcalc_status );//
    printf_fifo_status( clusteroutfifo_status ); //
    printf_fifo_status( gridsysoutfifo_status );
    printf_fifo_status( datafifo_status );
    printf_fifo_status( outfifo_status );
    printf("\n");
    // -- 
    printf("%29s||", "Occupancy");
    printf_fifo_occupancy( infifo_status[6], 0x4000 ); //16k
    printf_fifo_occupancy( rodfifo_status[6], 0x400 ); // 1k
    printf_fifo_occupancy( sysfifo_status[6], 0 ); // 1k
    printf_fifo_occupancy( decodedfifo_status[6], 0);
    printf_fifo_occupancy( lvl1idfifo_status[6], 0);
    printf_fifo_occupancy( parallelfifo_status[6], 0);
    printf_fifo_occupancy( centroidcalc_status[6], 0);
    printf_fifo_occupancy( clusteroutfifo_status[6], 0);
    printf_fifo_occupancy( gridsysoutfifo_status[6], 0x400 ); // 1k
    printf_fifo_occupancy( datafifo_status[6], 0x400 ); // 1k
    printf_fifo_occupancy( outfifo_status[6], 0x4000 ); // 16k
    printf("\n");
    // -- 
    printf("%29s||", "#of full");
    printf("%12s|", printf_with_number_of(  infifo_status[5] ).c_str() );
    printf("%12s|", printf_with_number_of(  rodfifo_status[5] ).c_str() );
    printf("%12s|", printf_with_number_of(  sysfifo_status[5] ).c_str() );
    printf("%12s|", printf_with_number_of(  decodedfifo_status[5] ).c_str() );
    printf("%12s|", printf_with_number_of(  lvl1idfifo_status[5] ).c_str() );
    printf("%12s|", printf_with_number_of(  parallelfifo_status[5] ).c_str()  );
    printf("%12s|", printf_with_number_of(  centroidcalc_status[5] & 0X00FFFFF).c_str() );
    printf("%12s|", printf_with_number_of(  clusteroutfifo_status[5] ).c_str() );
    printf("%12s|", printf_with_number_of(  gridsysoutfifo_status[5] ).c_str() );
    printf("%12s|", printf_with_number_of(  datafifo_status[5] ).c_str() );
    printf("%12s|", printf_with_number_of(  outfifo_status[5] ).c_str() );
    printf("\n");
    printf("%29s||\n", " " );
    
    // for parallel
    printf("%29s||", "Parallel >>>>>>>>>>>>" );
    printf("%12s|", "" );
    printf("%12s|", "Unit0" );
    printf("%12s|", "Unit1" );
    printf("%12s|", "Unit2" );
    printf("%12s|", "Unit3" );
    printf("%12s|", "Unit4" );
    printf("%12s|", "Unit5" );
    printf("%12s|", "Unit6" );
    printf("%12s|", "Unit7" );
    printf("\n");
    //-- status
    printf("%29s||", "Status");
    printf("%12s|", "Parallel");
    for(int i_engine = 0; i_engine < 8; ++i_engine ){
      printf_fifo_status( parallel_fifo_status_each[i_engine] );
    }
    printf("\n");
    printf("%29s||", " ");
    printf("%12s|", "Cluster");
    for(int i_engine = 0; i_engine < 8; ++i_engine ){
      printf_fifo_status( clusterout_fifo_status_each[i_engine] );
    }
    printf("\n");
    printf("%29s||", "Occupancy");
    printf("%12s|", "Parallel");
    for(int i_engine = 0; i_engine < 8; ++i_engine ){
      printf_fifo_occupancy( parallel_fifo_status_each[i_engine][6], 0x100 );
    }
    printf("\n");
    printf("%29s||", " ");
    printf("%12s|", "Cluster");
    for(int i_engine = 0; i_engine < 8; ++i_engine ){
      printf_fifo_occupancy( clusterout_fifo_status_each[i_engine][6], 0x100 );
    }
    printf("\n");
    printf("%29s||", "#of full");
    printf("%12s|", "Parallel");
    for(int i_engine = 0; i_engine < 8; ++i_engine ){
      printf("%12s|", printf_with_number_of( parallel_fifo_status_each[i_engine][5] ).c_str() );
    }
    printf("\n");
    printf("%29s||", " ");
    printf("%12s|", "Cluster");
    for(int i_engine = 0; i_engine < 8; ++i_engine ){
      printf("%12s|", printf_with_number_of( clusterout_fifo_status_each[i_engine][5] ).c_str() );
    }
    printf("\n");
  }
  else{
    printf("%29s||", "FIFO >>>>>>>>>>>>" );
    printf("%12s|", "IN" );
    printf("%12s|", "ROD" );
    printf("%12s|", "SYS" );
    printf("%12s|", "Data" );
    printf("%12s|", "OUT" );
    printf("\n");
    //-- status
    printf("%29s||", "Status");
    printf_fifo_status( infifo_status );
    printf_fifo_status( rodfifo_status );
    printf_fifo_status( sysfifo_status );
    printf_fifo_status( datafifo_status );
    printf_fifo_status( outfifo_status );
    printf("\n");
    // -- 
    printf("%29s||", "Occupancy");
    printf_fifo_occupancy( infifo_status[6], 0x4000 ); //16k
    printf_fifo_occupancy( rodfifo_status[6], 0x400 ); // 1k
    printf_fifo_occupancy( sysfifo_status[6], 0 ); // 1k
    printf_fifo_occupancy( datafifo_status[6], 0x400 ); // 1k
    printf_fifo_occupancy( outfifo_status[6], 0x4000 ); // 16k
    printf("\n");
    // -- 
    printf("%29s||", "#of full");
    printf("%12s|", printf_with_number_of( infifo_status[5] ).c_str() );
    printf("%12s|", printf_with_number_of( rodfifo_status[5] ).c_str() );
    printf("%12s|", printf_with_number_of(  sysfifo_status[5] ).c_str() );
    printf("%12s|", printf_with_number_of( datafifo_status[5] ).c_str() );
    printf("%12s|", printf_with_number_of( outfifo_status[5] ).c_str() );
    printf("\n");
  }  
  printf("%29s||\n", " " );
}

void smon_components::wr_rod_status0( int idx, std::string error_word ){
  if( ( (rod_status_word0[0] >> idx)  & 0x1) == 1  || 
      ( (rod_status_word0[1] >> idx)  & 0x1) == 1  || 
      ( (rod_status_word0[2] >> idx)  & 0x1) == 1  || 
      ( (rod_status_word0[3] >> idx)  & 0x1) == 1  ){
    printf( "%29s||",  " " );
    printf( "%s",  error_word.c_str()  );
  }
}
void smon_components::wr_errors(){

  printf("%29s||","Err Table >>>>>>>>>>>>");
  printf("%12s|","L1ID");
  printf("%12s|", "ROD err0");
  printf("%12s|", "ROD err1");
  printf("%12s|", "IM Err");
  printf("%12s|", "ID cluster");
  printf("%12s|", "IM system");
  printf("%12s|","ROL/ROC/TIM");
  printf("%12s|\n","#Err(ev frag)");
  

  // current
  printf("%29s||","Latest");
  printf("%4s%08x|", "0x", l1id_error[0] );
  printf("%4s%08x|", "0x", rod_status_word0[0] );
  printf("%4s%08x|", "0x", rod_status_word1[0] );
  printf("%4s%08x|", "0x", ID_cluster_errors[0] );
  printf("%4s%08x|", "0x", grid_system_errors[0] );
  printf("%12d|",          rod_status_word1[0] & 0x00070000);
  printf("%12d|\n",        rod_status_word1[0] & 0x0000ffff);
  
  // pre
  printf("%29s||","Previous");
  printf("%4s%08x|", "0x", l1id_error[1] );
  printf("%4s%08x|", "0x", rod_status_word0[1] );
  printf("%4s%08x|", "0x", rod_status_word1[1] );
  printf("%4s%08x|", "0x", ID_cluster_errors[1] );
  printf("%4s%08x|", "0x", grid_system_errors[1] );
  printf("%12d|",          rod_status_word1[1] & 0x00070000);
  printf("%12d|\n",        rod_status_word1[1] & 0x0000ffff);
  // pre
  printf("%29s||","Previous2");
  printf("%4s%08x|", "0x", l1id_error[2] );
  printf("%4s%08x|", "0x", rod_status_word0[2] );
  printf("%4s%08x|", "0x", rod_status_word1[2] );
  printf("%4s%08x|", "0x", ID_cluster_errors[2] );
  printf("%4s%08x|", "0x", grid_system_errors[2] );
  printf("%12d|",          rod_status_word1[2] & 0x00070000);
  printf("%12d|\n",        rod_status_word1[2] & 0x0000ffff);
  // pre
  printf("%29s||","Previous3");
  printf("%4s%08x|", "0x", l1id_error[3] );
  printf("%4s%08x|", "0x", rod_status_word0[3] );
  printf("%4s%08x|", "0x", rod_status_word1[3] );
  printf("%4s%08x|", "0x", ID_cluster_errors[3] );
  printf("%4s%08x|", "0x", grid_system_errors[3] );
  printf("%12d|",          rod_status_word1[3] & 0x00070000);
  printf("%12d|\n",        rod_status_word1[3] & 0x0000ffff);
  
  
  smon_components::wr_rod_status0(31,"  0: BC ID Error Count\n");
  smon_components::wr_rod_status0(30,"  1: L1 ID Error Count\n");
  smon_components::wr_rod_status0(29,"  2: FE Module Timeout Error Count\n");
  smon_components::wr_rod_status0(28,"  3: Data may be incorrect (see Bits 31:16)\n");
  smon_components::wr_rod_status0(27,"  4: Internal Buffer Overflow (see Bits 17:16)\n");
  smon_components::wr_rod_status0(15," 16: Almost Full Error, Data Truncated Count\n");
  smon_components::wr_rod_status0(14," 17: Data Overflow Count\n");
  smon_components::wr_rod_status0(13," 18: Header Bit Error Count\n");
  smon_components::wr_rod_status0(12," 19: Sync Error Count\n");
  smon_components::wr_rod_status0(11," 20: (SCT) Flagged Error Count / (Pixel) Invalid Row(>159) OR Column(>17)\n");
  smon_components::wr_rod_status0(10," 21: (SCT) Condensed Mode Hit Pattern Error / (Pixel) MCC Sent Empty Event\n");
  smon_components::wr_rod_status0( 9," 22: (SCT) Non-Sequential Chip Num / (Pixel) MCC Flagged Error - EOCOVERFLOW\n");
  smon_components::wr_rod_status0( 8," 23: (SCT) Invalid FE Chip / (Pixel) MCC Flagged Error - HAMMINGCODE\n");
  smon_components::wr_rod_status0( 7," 24: (SCT) Trailer Bit Error Count / (Pixel) MCC Flagged Error - REGPARITY\n");
  smon_components::wr_rod_status0( 6," 25: (SCT) Active Link Masked / (Pixel) MCC Flagged Error - HITPARITY\n");
  smon_components::wr_rod_status0( 5," 26: (Pixel) MCC Flagged Error - BITFLIP\n");
  smon_components::wr_rod_status0( 4," 27: (Pixel) MCC Flagged Error - HITOVERFLOW\n");
  smon_components::wr_rod_status0( 3," 28: (Pixel) MCC Flagged Error - EOEOVERFLOW\n");
  smon_components::wr_rod_status0( 2," 29: (Pixel) MCC Flagged Error - L1CHKFAILFE\n");
  smon_components::wr_rod_status0( 1," 30: (Pixel) MCC Flagged Error - BCIDCHKFAIL\n");
  smon_components::wr_rod_status0( 0, "31: (Pixel) MCC Flagged Error - L1CHKFAILGLOBAL\n");
  //printf("%29s||\n", " " );

  
}


void smon_components::wr_L1ID_monitor(){
  printf("%29s||",   "L1ID info >>>>>>>>>>>>");
  printf("%12s|",  "current ECR");
  printf("%12s|",  "Latest");
  printf("%12s|",  "#of jump");
  printf("%12s|",  "max gap");
  printf("%25s|",  "jump1");
  printf("%25s|",  "jump2");
  printf("%25s|",  "jump3");
  printf("%25s|",  "jump4");
  printf("\n");
  
  printf("%29s||", "Recieved");
  printf("%4s%08x|", "0x",  smon_components::vec_words.at(180) );
  printf("%4s%08x|", "0x",  smon_components::vec_words.at(174) );
  printf("%12s|",  printf_with_number_of( smon_components::vec_words.at(178) ).c_str() );
  printf("%4s%08x|", "0x",  smon_components::vec_words.at(179) );
  if( smon_components::vec_words.at(182) == 0 && smon_components::vec_words.at(181) == 0 ){
    printf("%25s|", "None");
  }else {
    printf(" %2s%08x -> %2s%08x|",  "0x", smon_components::vec_words.at(182), "0x", smon_components::vec_words.at(181) );
  }
  if( smon_components::vec_words.at(184) == 0 && smon_components::vec_words.at(183) == 0 ){
    printf("%25s|", "None");
  }else {
    printf(" %2s%08x -> %2s%08x|",  "0x", smon_components::vec_words.at(184), "0x", smon_components::vec_words.at(183) );
  }
  if( smon_components::vec_words.at(186) == 0 && smon_components::vec_words.at(185) == 0 ){
    printf("%25s|", "None");
  }else {
    printf(" %2s%08x -> %2s%08x|",  "0x", smon_components::vec_words.at(186), "0x", smon_components::vec_words.at(185) );
  }
  if( smon_components::vec_words.at(188) == 0 && smon_components::vec_words.at(187) == 0 ){
    printf("%25s|", "None");
  }else {
    printf(" %2s%08x -> %2s%08x|",  "0x", smon_components::vec_words.at(188), "0x", smon_components::vec_words.at(187) );
  }
  printf("\n");
  
  printf("%29s||", "Sent");
  printf("%4s%08x|", "0x",  smon_components::vec_words.at(195) );
  printf("%4s%08x|", "0x",  smon_components::vec_words.at(189) );
  printf("%12s|",  printf_with_number_of( smon_components::vec_words.at(193) ).c_str() );
  printf("%4s%08x|", "0x",  smon_components::vec_words.at(194) );
  if( smon_components::vec_words.at(197) == 0 && smon_components::vec_words.at(196) == 0 ){
    printf("%25s|", "None");
  }else {
    printf(" %2s%08x -> %2s%08x|",  "0x", smon_components::vec_words.at(197), "0x", smon_components::vec_words.at(196) );
  }
  if( smon_components::vec_words.at(199) == 0 && smon_components::vec_words.at(198) == 0 ){
    printf("%25s|", "None");
  }else {
    printf(" %2s%08x -> %2s%08x|",  "0x", smon_components::vec_words.at(199), "0x", smon_components::vec_words.at(198) );
  }
  if( smon_components::vec_words.at(201) == 0 && smon_components::vec_words.at(200) == 0 ){
    printf("%25s|", "None");
  }else {
    printf(" %2s%08x -> %2s%08x|",  "0x", smon_components::vec_words.at(201), "0x", smon_components::vec_words.at(200) );
  }
  if( smon_components::vec_words.at(203) == 0 && smon_components::vec_words.at(202) == 0 ){
    printf("%25s|", "None");
  }else {
    printf(" %2s%08x -> %2s%08x|", "0x", smon_components::vec_words.at(203), "0x", smon_components::vec_words.at(202) );
  }
  printf("\n");
  printf("%29s||\n", " " );
  
}

void smon_components::wr_input_rate_b0f_b0f(){
  // transform 32bit -> 34bit, see FW code
  uint64_t tmp_hist_input_rate_b0f_b0f[8];
  tmp_hist_input_rate_b0f_b0f[0] = ( (hist_input_rate_b0f_b0f[8] & 0xc0000000) <<  2 ) + ( hist_input_rate_b0f_b0f[0] & 0xffffffff );
  tmp_hist_input_rate_b0f_b0f[1] = ( (hist_input_rate_b0f_b0f[8] & 0x30000000) <<  4 ) + ( hist_input_rate_b0f_b0f[1] & 0xffffffff );
  tmp_hist_input_rate_b0f_b0f[2] = ( (hist_input_rate_b0f_b0f[8] & 0x0c000000) <<  6 ) + ( hist_input_rate_b0f_b0f[2] & 0xffffffff );
  tmp_hist_input_rate_b0f_b0f[3] = ( (hist_input_rate_b0f_b0f[8] & 0x03000000) <<  8 ) + ( hist_input_rate_b0f_b0f[3] & 0xffffffff );
  tmp_hist_input_rate_b0f_b0f[4] = ( (hist_input_rate_b0f_b0f[8] & 0x00c00000) << 10 ) + ( hist_input_rate_b0f_b0f[4] & 0xffffffff );
  tmp_hist_input_rate_b0f_b0f[5] = ( (hist_input_rate_b0f_b0f[8] & 0x00300000) << 12 ) + ( hist_input_rate_b0f_b0f[5] & 0xffffffff );
  tmp_hist_input_rate_b0f_b0f[6] = ( (hist_input_rate_b0f_b0f[8] & 0x000c0000) << 14 ) + ( hist_input_rate_b0f_b0f[6] & 0xffffffff );
  tmp_hist_input_rate_b0f_b0f[7] = ( (hist_input_rate_b0f_b0f[8] & 0x00030000) << 16 ) + ( hist_input_rate_b0f_b0f[7] & 0xffffffff );
  uint64_t tmp_sum_content = 0;
  for(int i = 0, size = 8; i < size; ++i) tmp_sum_content += tmp_hist_input_rate_b0f_b0f[i];
  if ( tmp_sum_content < 1 ) tmp_sum_content = 1;
  auto get_ratio_of_rate = [=]( uint64_t val ) -> float { return ( tmp_sum_content != 0) ? float(val * 100 / tmp_sum_content) : 0; };
  
  printf("%29s||", "input rate >>>>>>>>>>>>"); 
  printf("%12s|", "0.6 - 19kHz");
  printf("%12s|", " 19 -  39kHz");
  printf("%12s|", " 39 -  60kHz");
  printf("%12s|", " 60 -  78kHz");
  printf("%12s|", " 78 - 100kHz");
  printf("%12s|", "100 - 129kHz");
  printf("%12s|", "129 - 156kHz");
  printf("%12s|\n", "156 -       ");
  printf("%29s||", "count"); 
  printf("%12s|", printf_with_number_of_64(  tmp_hist_input_rate_b0f_b0f[0]).c_str() );
  printf("%12s|", printf_with_number_of_64(  tmp_hist_input_rate_b0f_b0f[1]).c_str() );
  printf("%12s|", printf_with_number_of_64(  tmp_hist_input_rate_b0f_b0f[2]).c_str() );
  printf("%12s|", printf_with_number_of_64(  tmp_hist_input_rate_b0f_b0f[3]).c_str() );
  printf("%12s|", printf_with_number_of_64(  tmp_hist_input_rate_b0f_b0f[4]).c_str() );
  printf("%12s|", printf_with_number_of_64(  tmp_hist_input_rate_b0f_b0f[5]).c_str() );
  printf("%12s|", printf_with_number_of_64(  tmp_hist_input_rate_b0f_b0f[6]).c_str() );
  printf("%12s|\n", printf_with_number_of_64(  tmp_hist_input_rate_b0f_b0f[7]).c_str() );
  
  printf("%29s||", "ratio"); 
  printf("%11.1lf%%|", get_ratio_of_rate( tmp_hist_input_rate_b0f_b0f[0] ) );
  printf("%11.1lf%%|", get_ratio_of_rate( tmp_hist_input_rate_b0f_b0f[1] ) );
  printf("%11.1lf%%|", get_ratio_of_rate( tmp_hist_input_rate_b0f_b0f[2] ) );
  printf("%11.1lf%%|", get_ratio_of_rate( tmp_hist_input_rate_b0f_b0f[3] ) );
  printf("%11.1lf%%|", get_ratio_of_rate( tmp_hist_input_rate_b0f_b0f[4] ) );
  printf("%11.1lf%%|", get_ratio_of_rate( tmp_hist_input_rate_b0f_b0f[5] ) );
  printf("%11.1lf%%|", get_ratio_of_rate( tmp_hist_input_rate_b0f_b0f[6] ) );
  printf("%11.1lf%%|\n", get_ratio_of_rate( tmp_hist_input_rate_b0f_b0f[7] ) );


}
void smon_components::wr_input_rate_max_idle(){
  // transform 32bit -> 34bit, see FW code
  uint64_t tmp_hist_input_rate_max_idle[8];
  tmp_hist_input_rate_max_idle[0] = ( (hist_input_rate_max_idle[8] & 0xc0000000) <<  2 ) + ( hist_input_rate_max_idle[0] & 0xffffffff );
  tmp_hist_input_rate_max_idle[1] = ( (hist_input_rate_max_idle[8] & 0x30000000) <<  4 ) + ( hist_input_rate_max_idle[1] & 0xffffffff );
  tmp_hist_input_rate_max_idle[2] = ( (hist_input_rate_max_idle[8] & 0x0c000000) <<  6 ) + ( hist_input_rate_max_idle[2] & 0xffffffff );
  tmp_hist_input_rate_max_idle[3] = ( (hist_input_rate_max_idle[8] & 0x03000000) <<  8 ) + ( hist_input_rate_max_idle[3] & 0xffffffff );
  tmp_hist_input_rate_max_idle[4] = ( (hist_input_rate_max_idle[8] & 0x00c00000) << 10 ) + ( hist_input_rate_max_idle[4] & 0xffffffff );
  tmp_hist_input_rate_max_idle[5] = ( (hist_input_rate_max_idle[8] & 0x00300000) << 12 ) + ( hist_input_rate_max_idle[5] & 0xffffffff );
  tmp_hist_input_rate_max_idle[6] = ( (hist_input_rate_max_idle[8] & 0x000c0000) << 14 ) + ( hist_input_rate_max_idle[6] & 0xffffffff );
  tmp_hist_input_rate_max_idle[7] = ( (hist_input_rate_max_idle[8] & 0x00030000) << 16 ) + ( hist_input_rate_max_idle[7] & 0xffffffff );
  uint64_t tmp_sum_content = 0;
  for(int i = 0, size = 8; i < size; ++i) tmp_sum_content += tmp_hist_input_rate_max_idle[i];
  if ( tmp_sum_content < 1 ) tmp_sum_content = 1;
  auto get_ratio_of_rate = [=]( uint64_t val ) -> float { return ( tmp_sum_content != 0) ? float(val * 100 / tmp_sum_content) : 0; };
  
  printf("%29s||", "max idle count >>>>>>>>>>>>"); 
  printf("%12s|", "   - 1.6us");
  printf("%12s|", "1.6 - 3.2us");
  printf("%12s|", "3.2 -  13us");
  printf("%12s|", " 13 -  26us");
  printf("%12s|", " 26 -  51us");
  printf("%12s|", " 51 - 102us");
  printf("%12s|", "102 - 204us");
  printf("%12s|\n","204 -     ");
  printf("%29s||", "count"); 
  printf("%12s|", printf_with_number_of_64(  tmp_hist_input_rate_max_idle[0]).c_str() );
  printf("%12s|", printf_with_number_of_64(  tmp_hist_input_rate_max_idle[1]).c_str() );
  printf("%12s|", printf_with_number_of_64(  tmp_hist_input_rate_max_idle[2]).c_str() );
  printf("%12s|", printf_with_number_of_64(  tmp_hist_input_rate_max_idle[3]).c_str() );
  printf("%12s|", printf_with_number_of_64(  tmp_hist_input_rate_max_idle[4]).c_str() );
  printf("%12s|", printf_with_number_of_64(  tmp_hist_input_rate_max_idle[5]).c_str() );
  printf("%12s|", printf_with_number_of_64(  tmp_hist_input_rate_max_idle[6]).c_str() );
  printf("%12s|\n", printf_with_number_of_64(  tmp_hist_input_rate_max_idle[7]).c_str() );
  
  printf("%29s||", "ratio"); 
  printf("%11.1lf%%|", get_ratio_of_rate( tmp_hist_input_rate_max_idle[0] ) );
  printf("%11.1lf%%|", get_ratio_of_rate( tmp_hist_input_rate_max_idle[1] ) );
  printf("%11.1lf%%|", get_ratio_of_rate( tmp_hist_input_rate_max_idle[2] ) );
  printf("%11.1lf%%|", get_ratio_of_rate( tmp_hist_input_rate_max_idle[3] ) );
  printf("%11.1lf%%|", get_ratio_of_rate( tmp_hist_input_rate_max_idle[4] ) );
  printf("%11.1lf%%|", get_ratio_of_rate( tmp_hist_input_rate_max_idle[5] ) );
  printf("%11.1lf%%|", get_ratio_of_rate( tmp_hist_input_rate_max_idle[6] ) );
  printf("%11.1lf%%|\n", get_ratio_of_rate( tmp_hist_input_rate_max_idle[7] ) );

}

void smon_components::wr_data_merger_monitor(){
  //data_merger_monitor
  printf("%29s||", "data merger Info >>>>>>>>>>>>");
  printf(" %-20s is 0x%08x/0x%08x\n", "current out/L1ID", data_merger_out, data_merger_L1ID );
  printf("%29s||", "" );
  printf(" %-20s is %s\n", "current FSM",
         ( data_merger_FSM == 0b000 ) ? "Reset" : 
         ( data_merger_FSM == 0b001 ) ? "Ready" : 
         ( data_merger_FSM == 0b010 ) ? "Choose Machine" : 
         ( data_merger_FSM == 0b011 ) ? "Choose Mod Machine" : 
         ( data_merger_FSM == 0b100 ) ? "Pop Machine" : 
         ( data_merger_FSM == 0b101 ) ? "End Event" : "???"
         );
  printf("%29s||", "" );
  printf(" %-20s is %d\n", "Detail Monitor", data_merger_FSM_detail_monitor );
  
  printf("%29s||", "" );
  printf("%6s|", "Unit0" );
  printf("%6s|", "Unit1" );
  printf("%6s|", "Unit2" );
  printf("%6s|", "Unit3" );
  printf("%6s|", "Unit4" );
  printf("%6s|", "Unit5" );
  printf("%6s|", "Unit6" );
  printf("%6s|\n", "Unit7" );

  printf("%29s||", "Current Active" );
  for(int i = 0; i < 8; ++i ) printf("%6s|", (i == int(data_merger_slv_active_module_number) ) ? "Active" : " ");
  printf("\n");

  printf("%29s||", "Active Mod In/Out" );
  for(int i = 0; i < 8; ++i ) printf("   %1x/%1x|", (data_merger_total_active_module_reg_in >> i ) & 0x1, (data_merger_total_active_module_reg_out >> i ) & 0x1 );
  printf("\n");
  
  printf("%29s||", "Start Evt Encode/In/Out" );
  for(int i = 0; i < 8; ++i ) printf(" %1x/%1x/%1x|", (data_merger_total_start_event_encoded >> i ) & 0x1, (data_merger_total_start_event_reg_in >> i ) & 0x1, (data_merger_total_start_event_reg_out >> i ) & 0x1 );
  printf("\n");
  
  printf("%29s||", "Active Start/End Evt" );
  for(int i = 0; i < 8; ++i ) printf("   %1x/%1x|", (data_merger_active_start_event >> i ) & 0x1, (data_merger_active_end_event >> i ) & 0x1 );
  printf("\n");
  
  printf("%29s||", "Cls Rd/Valid/Empty" );
  for(int i = 0; i < 8; ++i ) printf(" %1x/%1x/%1x|", (data_merger_parallel_clusters_rd_int >> i ) & 0x1, (data_merger_Parallel_clusters_valid >> i ) & 0x1, (data_merger_Parallel_clusters_almost_empty >> i ) & 0x1 );
  printf("\n");
  printf("%29s||\n", "" );
}

void smon_components::print_full_old( int channel_id, int fpga_id ){
  // =========
  // Read Data
  // =========
  // -- info
  smon_components::rd_EvInfo();
  smon_components::rd_dupli_words();
  smon_components::rd_word_state();
  smon_components::rd_errors();
  smon_components::rd_fifo(0);//infifo
  smon_components::rd_fifo(1);//sysfifo
  smon_components::rd_fifo(2);//rodfifo
  smon_components::rd_fifo(3);//datafifo
  smon_components::rd_fifo(4);//xoff
  smon_components::rd_fifo(5);//outfifo
  smon_components::rd_fifo(6);//decoded fifo
  smon_components::rd_fifo(7);//lvl1id fifo
  smon_components::rd_fifo(8);//cluster out fifo
  smon_components::rd_fifo(9);//parallel fifo
  smon_components::rd_fifo(10);//grid system out fifo
  smon_components::rd_fifo(11);//centroid calculation
  smon_components::rd_fifo_each_parallel(0);//each parallel calculation
  smon_components::rd_fifo_each_parallel(1);//each parallel calculation
  smon_components::rd_fifo_each_parallel(2);//each parallel calculation
  smon_components::rd_fifo_each_parallel(3);//each parallel calculation
  smon_components::rd_fifo_each_parallel(4);//each parallel calculation
  smon_components::rd_fifo_each_parallel(5);//each parallel calculation
  smon_components::rd_fifo_each_parallel(6);//each parallel calculation
  smon_components::rd_fifo_each_parallel(7);//each parallel calculation

  smon_components::rd_fifo_each_clusterout(0);//each clusterout calculation
  smon_components::rd_fifo_each_clusterout(1);//each clusterout calculation
  smon_components::rd_fifo_each_clusterout(2);//each clusterout calculation
  smon_components::rd_fifo_each_clusterout(3);//each clusterout calculation
  smon_components::rd_fifo_each_clusterout(4);//each clusterout calculation
  smon_components::rd_fifo_each_clusterout(5);//each clusterout calculation
  smon_components::rd_fifo_each_clusterout(6);//each clusterout calculation
  smon_components::rd_fifo_each_clusterout(7);//each clusterout calculation
  smon_components::rd_fifo_fraction();
  smon_components::rd_fifo_occupancy();
  smon_components::rd_data_merger_monitor();
  
  // -- input rate 
  smon_components::rd_input_rate_b0f_b0f();
  smon_components::rd_input_rate_max_idle();


  // ==========
  // Write Data
  // ==========
  smon_components::show_simple_title();
  smon_components::wr_globalInfo(fpga_id, channel_id, 0, 0, 0, 0, 0, 0);
  smon_components::wr_xoff_control();
  smon_components::wr_RecoverInfo();
  smon_components::wr_GeneralInfo();
  smon_components::wr_EvtInfo();
  smon_components::wr_event_rate();
  smon_components::wr_errors();
  smon_components::wr_ErrorBit();
  smon_components::wr_dupli_words();
  smon_components::wr_fifo( channel_id );
  if( channel_id  == 0 )   smon_components::wr_data_merger_monitor();
  smon_components::wr_L1ID_monitor();
  smon_components::wr_input_rate_b0f_b0f();
  smon_components::wr_input_rate_max_idle();

}

void smon_components::print_raw_text(){
  smon_components::show_smon_raw_title();
  for(uint32_t jj = 0; jj < 16; jj++ ){
    for(uint32_t ii = 0; ii < 16; ii++ ){
      int idx = ii * 16 + jj;
      printf("%02x:%08x ", idx, smon_components::vec_words.at(idx));
    }
    printf("\n");
  }
}

// --------------------------------------------- new monitor -----------------------------------------------//

void smon_components::fill( event_info& p ){
  p.run_number   =   vec_words.at( p.idx + 0 );
  p.l1id         =   vec_words.at( p.idx + 1 );
  p.cnt_b0f_e0f  = ( vec_words.at( p.idx + 2 ) & 0x0000FFFF) >>  0;
  p.cnt_b0f_b0f  = ( vec_words.at( p.idx + 2 ) & 0xFFFF0000) >> 16;
  p.bcid         = ( vec_words.at( p.idx + 3 ) & 0xFFF00000) >> 20;
  p.l1tt         = ( vec_words.at( p.idx + 3 ) & 0x000FF000) >> 12;
  p.evTyp        = ( vec_words.at( p.idx + 3 ) & 0x00000FF0) >>  4;
  p.tim          = ( vec_words.at( p.idx + 3 ) & 0x0000000F) >>  0;
  p.err_word0    =   vec_words.at( p.idx + 4 );
  p.err_word1    =   vec_words.at( p.idx + 5 );
  p.n_word       = ( vec_words.at( p.idx + 6 ) & 0xFFFF0000) >> 16;
  p.rate_b0f_b0f = 1.0 / ( float(p.cnt_b0f_b0f) / float(p.clk_freq*1000*1000) ) / 1000. ; // to kHz, clk_freq is in unit of MHz
  p.rate_b0f_e0f = 1.0 / ( float(p.cnt_b0f_e0f) / float(p.clk_freq*1000*1000) ) / 1000. ; // to kHz, clk_freq is in unit of MHz
}
void smon_components::fill( l1id_info& p ){
  p.n_l1id_jumps          = vec_words.at( p.idx + 0 );
  p.max_l1id_gap          = vec_words.at( p.idx + 1 );
  p.jumped_01_l1id_ahead  = vec_words.at( p.idx + 2 );
  p.jumped_01_l1id_behind = vec_words.at( p.idx + 3 );
  p.jumped_02_l1id_ahead  = vec_words.at( p.idx + 4 );
  p.jumped_02_l1id_behind = vec_words.at( p.idx + 5 );
  p.jumped_03_l1id_ahead  = vec_words.at( p.idx + 6 );
  p.jumped_03_l1id_behind = vec_words.at( p.idx + 7 );
}
void smon_components::fill( dataflow_info& p ){
  smon_components::fill( p.current_event );
  smon_components::fill( p.prev_01_event );
  smon_components::fill( p.prev_02_event );
  smon_components::fill( p.jump_info );
  
  uint64_t tmp_n_total_event;
  p.n_total_event = 0;
  tmp_n_total_event  = ( vec_words.at( p.idx +  6 ) & 0x0000FFFF );
  p.n_total_event    = p.n_total_event + (tmp_n_total_event << 48);
  
  tmp_n_total_event  = ( vec_words.at( p.idx + 13 ) & 0x0000FFFF );
  p.n_total_event    = p.n_total_event + (tmp_n_total_event << 32);
  
  tmp_n_total_event  = ( vec_words.at( p.idx + 20 ) & 0x0000FFFF );
  p.n_total_event    = p.n_total_event + (tmp_n_total_event << 16);
  
  tmp_n_total_event  = ( vec_words.at( p.idx + 27 ) & 0x0000FFFF );
  p.n_total_event    = p.n_total_event + (tmp_n_total_event <<  0);
}
void smon_components::fill( fifo_monitor& p ){
  p.is_full         = ( vec_words.at( p.idx ) >> 31) == 0x1;
  p.is_almost_full  = ( vec_words.at( p.idx ) >> 30) == 0x1;
  p.is_empty        = ( vec_words.at( p.idx ) >> 29) == 0x1;
  p.is_almost_empty = ( vec_words.at( p.idx ) >> 28) == 0x1;
  p.is_lost_word    = ( vec_words.at( p.idx ) >> 27) == 0x1;
  p.n_lost_word     = ( vec_words.at( p.idx ) & 0xffff);
  if( ! p.is_small ){
    p.n_full          = ( ( vec_words.at( p.idx + 1 ) & 0xffff0000 ) >> 16);
    p.cnt_full        = (   vec_words.at( p.idx + 1 ) & 0x0000ffff);
    p.n_almost_full   = ( ( vec_words.at( p.idx + 2 ) & 0xffff0000 ) >> 16);
    p.cnt_almost_full = (   vec_words.at( p.idx + 2 ) & 0x0000ffff);
    p.occupancy       = float(( vec_words.at( p.idx ) & (0xff0000) ) >> 16) / 256.0 * 100.0;
    p.busy_fraction   = float( vec_words.at( p.idx + 3 ) )/float( ( p.clk_freq* p.run_time*1000*1000) ) * 100.0;
  }else{
    p.occupancy       = float(( vec_words.at( p.idx ) & (0xff0000) ) >> 16) / 256.0 * 100.0;
    p.n_almost_full   = ( ( vec_words.at( p.idx + 1 ) & 0xffff0000 ) >> 16);
    p.cnt_almost_full = (   vec_words.at( p.idx + 1 ) & 0x0000ffff);
    p.busy_fraction   = float( vec_words.at( p.idx + 2 ) )/float( ( p.clk_freq* p.run_time*1000*1000) ) * 100.0;
  }
  if      ( p.is_full )         { p.status = "Full";       p.status_s = "FL"; }
  else if ( p.is_almost_full )  { p.status = "Almost Ful"; p.status_s = "AF"; }
  else if ( p.is_almost_empty ) { p.status = "Almost Emp"; p.status_s = "AE"; }
  else if ( p.is_empty        ) { p.status = "Empty";      p.status_s = "EM"; }
  else                          { p.status = "Not Empty";  p.status_s = "--"; }
}

void smon_components::fill( simple_counter& p ){
  p.n_full        = ( ( vec_words.at( p.idx ) & 0xffff0000 ) >> 16);
  p.cnt_full      =   ( vec_words.at( p.idx ) & 0x0000ffff );
  p.busy_fraction = float( vec_words.at( p.idx + 1 ) )/float( ( p.clk_freq* p.run_time*1000 * 1000) ) * 100.0;
}
void smon_components::print_long1( event_info& p, std::string name, uint64_t n_total_event ){
  
  if( n_total_event > 0 ) printf( "%24s evt ||", smon_components::printf_with_number_of_64( n_total_event ).c_str() );
  else                    printf( "%29s||", name.c_str() );
  printf( "%11s |",   smon_components::printf_with_number_of( p.run_number ).c_str() );
  printf( " 0x%08x |", p.l1id );
  printf( " 0x%02x |", p.l1tt );
  printf( " 0x%03x |", p.bcid );
  printf( "%9s |",    smon_components::printf_with_number_of( p.n_word ).c_str() );
  printf( " %4dkHz(%6s) |", p.rate_b0f_b0f, smon_components::printf_with_number_of( p.cnt_b0f_b0f ).c_str() );
  printf( " %4dkHz(%6s) |", p.rate_b0f_e0f, smon_components::printf_with_number_of( p.cnt_b0f_e0f ).c_str() );
  printf( " 0x%08x |",   p.err_word0 );
  printf( " 0x%08x |\n", p.err_word1 );
}
void smon_components::print_short( dataflow_info& p ){
  printf( " 0x%08x(%10s)/%5dkHz/%18s |", p.current_event.l1id, smon_components::printf_with_number_of( p.jump_info.n_l1id_jumps ).c_str(),
          p.current_event.rate_b0f_b0f, smon_components::printf_with_number_of_64( p.n_total_event ).c_str() );
}
void smon_components::print_long1( dataflow_info& p, std::string name ){
  smon_components::print_long1( p.current_event, name, 0 );
  smon_components::print_long1( p.prev_01_event, "",   p.n_total_event );
  smon_components::print_long1( p.prev_02_event, "",   0 );
}
void smon_components::print_long1( l1id_info& p, std::string name ){
  printf( "%29s||", name.c_str() );
  printf( "%11s |", smon_components::printf_with_number_of( p.n_l1id_jumps ).c_str() );
  printf( "%11s |", smon_components::printf_with_number_of( p.max_l1id_gap ).c_str() );
  printf( " 0x%08x / 0x%08x |",   p.jumped_01_l1id_ahead, p.jumped_01_l1id_behind );
  printf( " 0x%08x / 0x%08x |",   p.jumped_02_l1id_ahead, p.jumped_02_l1id_behind );
  printf( " 0x%08x / 0x%08x |\n", p.jumped_03_l1id_ahead, p.jumped_03_l1id_behind );
  
}
void smon_components::print_short( fifo_monitor& p ){
  printf( " %2s %6s/%6s |", p.status_s.c_str(), smon_components::printf_with_number_of( p.n_almost_full ).c_str(), smon_components::printf_percent( p.busy_fraction ).c_str() );
}
void smon_components::print_long1( fifo_monitor& p, std::string name ){
  printf("%29s||",     name.c_str() );
  printf( "%11s |",    p.status.c_str() );
  printf( "%6s/%6s |", smon_components::printf_with_number_of( p.n_almost_full ).c_str(), smon_components::printf_with_number_of( p.cnt_almost_full ).c_str() );
  printf( "%8s |",     smon_components::printf_percent( p.busy_fraction ).c_str() );
  printf( "%10s |",     smon_components::printf_percent( p.occupancy ).c_str() );
  printf( "%10s |\n",   smon_components::printf_with_number_of( p.n_lost_word ).c_str() );
}

void smon_components::print_short( simple_counter& p ){
  std::string status;
  if      ( p.cnt_full > 0 ) status = "X";
  else                       status = "-";
  printf( " %1s %6s/%6s |", status.c_str(), smon_components::printf_with_number_of( p.n_full ).c_str(), smon_components::printf_percent(p.busy_fraction).c_str() );
}

void smon_components::print_long1( simple_counter& p, std::string name ){
  std::string status;
  if      ( p.cnt_full > 0 ) status = "Yes";
  else                       status = "No";
  // char char_n_full[20]   = smon_components::printf_with_number_of( p.n_full );
  // char char_cnt_full[20] = smon_components::printf_with_number_of( p.cnt_full );
  
  printf("%29s||",       name.c_str() );
  printf( "%11s |",      status.c_str() );
  printf( "%6s/%6s |",   smon_components::printf_with_number_of( p.n_full ).c_str(), smon_components::printf_with_number_of( p.cnt_full ).c_str() );
  printf( "%8s |\n",     smon_components::printf_percent( p.busy_fraction ).c_str() );
}


void smon_components::read_vector(){
  
  
  smon_components::fill( input_from_LDC );
  smon_components::fill( output_from_INFIFO );
  smon_components::fill( output_from_OUTFIFO );
  
  smon_components::fill( in_fifo_monitor );
  smon_components::fill( rod_fifo_monitor );
  smon_components::fill( data_fifo_monitor );
  smon_components::fill( out_fifo_monitor );
  
  smon_components::fill( xoff_monitor );
  smon_components::fill( BP_monitor );
  smon_components::fill( recover_monitor );
  smon_components::fill( truncation_monitor );
  smon_components::fill( fillgap_monitor );

  for( int idx = 0; idx < 8; ++idx){
    smon_components::fill( parallel_input_fifo_monitor.at(idx) );
    smon_components::fill( parallel_output_fifo_monitor.at(idx) );
  } //
  
  
  smon_components::fill( SCT_input_fifo_monitor );
  smon_components::fill( SCT_decoded_fifo_monitor );
  smon_components::fill( SCT_sorted_fifo_monitor );
  
  
  fw_version = vec_words.at(0) >> 16; 
  fw_type    = vec_words.at(0) & 0xFFFF;
  fw_hash    = vec_words.at(1);
  fw_date    = vec_words.at(2);
  status_bit = vec_words.at(3);
  error_bit  = vec_words.at(4);
  RODID      = vec_words.at(60);
  first_L1ID = vec_words.at(66);
  
  fsm_encoding_pull_input_handler   = (vec_words.at(67) & 0xF0000000) >> 28;
  fsm_encoding_pull_input_splitter  = (vec_words.at(67) & 0x0F000000) >> 24;
  fsm_encoding_pull_output_merger   = (vec_words.at(67) & 0x00F00000) >> 20;
  fsm_encoding_pull_fillgap_handler = (vec_words.at(67) & 0x000F0000) >> 16;
  fsm_encoding_pull_output_builder  = (vec_words.at(67) & 0x0000F000) >> 12;
  
  fsm_encoding_push_input_handler   = (vec_words.at(68) & 0xF0000000) >> 28;
  fsm_encoding_push_input_splitter  = (vec_words.at(68) & 0x0F000000) >> 24;
  fsm_encoding_push_output_merger   = (vec_words.at(68) & 0x00F00000) >> 20;
  fsm_encoding_push_fillgap_handler = (vec_words.at(68) & 0x000F0000) >> 16;
  fsm_encoding_push_output_builder  = (vec_words.at(68) & 0x0000F000) >> 12;
  fsm_encoding_push_input_splitter_ = (vec_words.at(68) & 0x00000F00) >>  8;
  
  loophit_cnt = (vec_words.at(65) & 0x0000FFFF);
  timeout_cnt = (vec_words.at(65) & 0xFFFF0000) >> 16;
  
  float sum_input  = 0.0;
  float sum_output = 0.0;
  for(size_t idx = 0; idx < 6; ++idx) {
    m_rate_hist_input[idx]  = vec_words.at(180 + idx);
    m_rate_hist_output[idx] = vec_words.at(186 + idx);
    sum_input  += float( vec_words.at(180 + idx) );
    sum_output += float( vec_words.at(186 + idx) );
  }
  for(size_t idx = 0; idx < 6; ++idx) {
    m_rate_hist_input_frac[idx]  = float(vec_words.at(180 + idx))/sum_input  * 100.0;
    m_rate_hist_output_frac[idx] = float(vec_words.at(186 + idx))/sum_output * 100.0;
  }  
  
  
}
// void smon_components::print_one_liner_old( bool is_print_first, int channel_id ){
//   // "| ch   |    RODID |   xoff   |   BP     |   Recover|   INFIFO(#/%) |   OUTFIFO(#/%)|                   #Event/L1ID |
//   // "|-----:|---------:|---------:|---------:|---------:|--------------:|--------------:|------------------------------:|
//   // "| ch0  | 0x140000 | X 0x1234 | X 0x1234 | X 0x1234 | F 0x1234/100% | E 0x1234/100% | 0x0123456789abcdef/0x12345678 |
//   if ( is_print_first ){
    
//   }
// }
void smon_components::print_one_liner( bool is_print_first, int channel_id ){
  // "| ch   |    RODID | fw ver/type   |   xoff(#/%)   |   BP(#/%)     |   Recover(#/%)|   Trunc(#/%)  |   INFIFO(#/%) |   OUTFIFO(#/%)| #of Event/L1ID/rate(In)              | #of Event/L1ID/rate(Int)             | #of Event/L1ID/rate(Out)             |";
  // "|-----:|---------:|--------------:|--------------:|--------------:|--------------:|--------------:|--------------:|--------------:|-------------------------------------:|-------------------------------------:|-------------------------------------:|";
  // "| ch0  | 0x140000 | 0x0130/0xa8s8 | X 0x1234/100% | X 0x1234/100% | X 0x1234/100% | X 0x1234/100% | F 0x1234/100% | E 0x1234/100% | 0x0123456789abcdef/0x12345678/123kHz | 0x0123456789abcdef/0x12345678/123kHz | 0x0123456789abcdef/0x12345678/123kHz |";
  if ( is_print_first ){
    printf("\n|");
    printf( "%6s|", "ch " );
    printf( "%10s|", "RODID " );
    
    printf( "%15s|", "FW ver/type " );
    printf( "%12s|", "FW hash " );
    printf( "%12s|", "FW date " );
    
    printf( "%14s|", "Start L1ID " );
    
    
    
    printf( "%17s|", "xoff(/#/%) " );
    printf( "%17s|", "BP(/#/%) " );
    
    printf( "%17s|", "Recover(/#/%) " );
    // printf( "%15s|", "Fillgap(/#/%) " );
    printf( "%17s|", "Trunc(/#/%) " );
    printf( "%18s|", "INFIFO(/#/%) " );
    printf( "%18s|", "OUTFIFO(/#/%) " );
    
    printf( "%52s|", "Input   : L1ID(#of skip)/rate/#of Event " );
    printf( "%52s|", "Internal: L1ID(#of skip)/rate/#of Event " );
    printf( "%52s|", "Output  : L1ID(#of skip)/rate/#of Event " );
    printf("\n");
    
    printf("|");
    print_table1(6);
    print_table1(10);
    
    print_table1(15);
    print_table1(12);
    print_table1(12);
    
    print_table1(14); // 1st L1ID 
    
    print_table1(17);
    print_table1(17);
    
    print_table1(17);
    // print_table1(17);
    print_table1(17);
    print_table1(18);
    print_table1(18);
    
    print_table1(52);
    print_table1(52);
    print_table1(52);
    printf("\n");
  }
  static char char_channel_id[20];
  snprintf( char_channel_id, 20, "ch%d", channel_id );
  printf("|");
  printf( "%5s |", char_channel_id );
  printf( " 0x%06x |", RODID );
  printf( " 0x%04x/0x%04x |", fw_version, fw_type );
  printf( " 0x%08x |", fw_hash );
  printf( " 0x%08x |", fw_date );

  printf( " %1s 0x%08x |", on_off( check_bit( status_bit, 19 ), "Y", "N" ).c_str(), first_L1ID );
  
  smon_components::print_short( xoff_monitor );
  smon_components::print_short( BP_monitor );
  smon_components::print_short( recover_monitor );
  smon_components::print_short( truncation_monitor );
  smon_components::print_short( in_fifo_monitor );
  smon_components::print_short( out_fifo_monitor );
  smon_components::print_short( input_from_LDC );
  smon_components::print_short( output_from_INFIFO );
  smon_components::print_short( output_from_OUTFIFO );
  printf("\n");
  
  
  
  
}
void smon_components::print_full( int channel_id ){
  
  
  smon_components::show_simple_title();
  int n_parallel = fw_type & 0x000F;
  std::string s6_a7    = ( (fw_type & (0x1 << 15) ) >> 15 ) ? "Artix7" : "Spartan6";
  std::string valid_fw = ( (fw_type & (0x1 << 13) ) >> 13 ) ? "Valid" : "Non-Valid";
  std::string debug_fw = ( (fw_type & (0x1 << 12) ) >> 12 ) ? "Debug" : "Dataflow";
  std::string ch0_type =
    ( (fw_type & (0x1 << 11) ) >> 11 ) ? "IBL" :
    ( (fw_type & (0x1 << 10) ) >> 10 ) ? "Pix" : 
    ( (fw_type & (0x1 <<  9) ) >>  9 ) ? "SCT" : "None";
  std::string ch1_type =
    ( (fw_type & (0x1 <<  7) ) >>  7 ) ? "IBL" :
    ( (fw_type & (0x1 <<  6) ) >>  6 ) ? "Pix" : 
    ( (fw_type & (0x1 <<  5) ) >>  5 ) ? "SCT" : "None";
  
  
  std::string this_ch_type = ( channel_id%2 == 0 ) ? ch0_type : ch1_type;
  
  
  
  
  auto ch_size = std::snprintf( nullptr, 0, "ch%d : 0x%04x ", channel_id, fw_type );
  std::string ch_arg( ch_size + 1, '\0');
  std::snprintf( &ch_arg[0], ch_size + 1, "ch%d : 0x%04x ", channel_id, fw_type );
  
  printf("%29s||", ch_arg.c_str() );
  printf(" %s %s %s (ch0:%s/ch1:%s) %d Parallel FW \n", s6_a7.c_str(), valid_fw.c_str(), debug_fw.c_str(), ch0_type.c_str(), ch1_type.c_str(), n_parallel );
  printf(" %18s : 0x%04x ||","version", fw_version );
  printf(" hash : 0x%08x\n", fw_hash );
  printf("%29s||","");
  printf(" date : 0x%08x\n", fw_date );
  
  
  // General
  printf("%29s||\n","");
  printf("%29s||","General Information >>>>>> ");
  printf(" status / error : 0x%08x / 0x%08x \n", status_bit, error_bit );
  
  printf("%29s||","");
  printf("%9s | %5s | %10s | %5s | %10s | %10s | %5s | %5s | %5s | %5s | %5s |\n",
         "RODID", "slink", "hold", "start", "1st L1ID", "xoff", "LDCX", "frz", "TMX", "blk", "loop");
  printf("%29s||","");
  printf(" 0x%06x |", RODID );
  printf(" %5s |",      on_off( check_bit( status_bit, 18 ) ).c_str() ); // slink 
  printf("  %4s/%4s |", on_off( check_bit( status_bit, 17 )).c_str(), on_off(check_bit( status_bit, 16 )).c_str() ); // hold up/down
  printf(" %5s |",      on_off( check_bit( status_bit, 19 ), "Yes", "No" ).c_str()  ); // sync 
  printf(" 0x%08x |",   first_L1ID ); // firrst L1ID 
  printf(" %10s |",     on_off( check_bit( status_bit, 20 ), "Enabled", "Disabled" ).c_str() ); // xoff
  printf(" %5s |",     on_off( check_bit( status_bit, 21 ), "On", "Off" ).c_str()  ); // 
  printf(" %5s |",     on_off( check_bit( status_bit, 24 ), "On", "Off" ).c_str()  );
  printf(" %5s |",     on_off( check_bit( status_bit, 29 ), "On", "Off" ).c_str()  );
  printf(" %5s |",     on_off( check_bit( status_bit, 30 ), "On", "Off" ).c_str()  );
  printf(" %5s |\n",   on_off( check_bit( status_bit, 31 ), "On", "Off" ).c_str()  );
  
  printf("%29s||","");
  printf("%13s : 0x%04x\n",  "timeout cnt", timeout_cnt );
  printf("%29s||","");
  printf("%13s : 0x%04x\n",  "loophit cnt", loophit_cnt );
  
  
  
  
  
  printf("%29s||","Counter Information >>>>>> ");
  printf("%12s|", "status ");
  printf("%14s|", "#of/cnt ");
  printf("%9s|\n", "busy ");
  
  smon_components::print_long1( BP_monitor,         "BP " );
  smon_components::print_long1( xoff_monitor,       "xoff " );
  smon_components::print_long1( recover_monitor,    "Recover " );
  smon_components::print_long1( truncation_monitor, "Truncation " );
  // smon_components::print_long1( fillgap_monitor,    "Fillgap " );
  
  // FIFO 
  printf("%29s||",  "FIFO Status >>>>>> " );
  printf("%12s|",   "status ");
  printf("%14s|",   "#of/cnt ");
  printf("%9s|",    "busy ");
  printf("%11s|",   "occupancy ");
  printf("%11s|\n", "lost word ");
  
  smon_components::print_long1( in_fifo_monitor,   "INFIFO " );
  smon_components::print_long1( rod_fifo_monitor,  "RODFIFO " );
  smon_components::print_long1( data_fifo_monitor, "DATAFIFO " );
  smon_components::print_long1( out_fifo_monitor,  "OUTFIFO " );
  if( this_ch_type.find("SCT") != std::string::npos ){
    smon_components::print_long1( SCT_input_fifo_monitor,  "SCT IN " );
    smon_components::print_long1( SCT_decoded_fifo_monitor,  "SCT DEC " );
    smon_components::print_long1( SCT_sorted_fifo_monitor,  "SCT REDUN " );
  }
  
  
  
  // Event Info 
  printf("%29s|\n", "" );
  printf("%29s||","Event Information >>>>>> ");
  // printf( "%20s|", "#of Event " );
  printf( "%12s|", "RunNumber " );
  printf( "%12s|", "L1ID " );
  printf( "%6s|", "L1TT " );
  printf( "%7s|", "BCID " );
  printf( "%10s|", "#of word " );
  printf( "%17s|", "rate(b0f-b0f) " );
  printf( "%17s|", "rate(b0f-e0f) " );
  printf( "%12s|", "err word0 " );
  printf( "%12s|\n", "err word1 " );
  smon_components::print_long1( input_from_LDC,      "Input " );
  smon_components::print_long1( output_from_INFIFO,  "After INFIFO " );
  smon_components::print_long1( output_from_OUTFIFO, "Output " );

  // L1ID Info 
  printf( "%29s|\n", "");
  printf( "%29s||","L1ID Information >>>>>> ");
  printf( "%12s|", "#of skip " );
  printf( "%12s|", "max gap " );
  printf( "%25s|", "jump 1 (from / to) " );
  printf( "%25s|", "jump 2 (from / to) " );
  printf( "%25s|\n", "jump 3 (from / to) " );
  smon_components::print_long1( input_from_LDC.jump_info,      "Input " );
  smon_components::print_long1( output_from_INFIFO.jump_info,  "After INFIFO " );
  smon_components::print_long1( output_from_OUTFIFO.jump_info, "Output " );
  
  // Rate hist Info 
  // printf( "%29s|\n", "");
  // printf( "%29s||","In/Out Rate >>>>>> ");
  // printf( "%20s |", "-20kHz");
  // printf( "%20s |", "20-50kHz");
  // printf( "%20s |", "50-80kHz");
  // printf( "%20s |", "80-100kHz");
  // printf( "%20s |", "100-120kHz");
  // printf( "%20s |\n", "120kHz-");
  // input
  // printf( "%29s||", "input ");
  // for(size_t idx = 0; idx < 6; ++idx){
  //   printf( " %10s / %6s |", smon_components::printf_with_number_of(m_rate_hist_input[idx]).c_str(), smon_components::printf_percent(m_rate_hist_input_frac[idx]).c_str() );
  // }
  // printf("\n");
  // printf( "%29s||", "output ");
  // for(size_t idx = 0; idx < 6; ++idx){
  //   printf( " %10s / %6s |", smon_components::printf_with_number_of(m_rate_hist_output[idx]).c_str(), smon_components::printf_percent(m_rate_hist_output_frac[idx]).c_str() );
  // }
  // printf("\n");
  
  
  if( this_ch_type.find("Pix") != std::string::npos || this_ch_type.find("IBL") != std::string::npos ){
    printf( "%29s|\n", "");
    printf("%29s||",  " Parallel FIFO >>>>>> " );
    printf("%10s|",   "");
    for( int idx = 0; idx < 8; ++idx){
      printf("%6s |", std::string( std::to_string(idx) + ": in").c_str() );
      printf("%6s |", std::string( std::to_string(idx) + ":out").c_str() );
    }
    printf("\n");
    printf( "%29s||", "");
    printf("%10s|",   "status ");
    for( int idx = 0; idx < 8; ++idx){
      printf( "%6s |", parallel_input_fifo_monitor.at(idx).status_s.c_str() );
      printf( "%6s |", parallel_output_fifo_monitor.at(idx).status_s.c_str() );
    }
    printf("\n");
    printf("%29s||", "");
    printf("%10s|",   "#of afull ");
    for( int idx = 0; idx < 8; ++idx){
      printf( "%6s |", smon_components::printf_with_number_of( parallel_input_fifo_monitor.at(idx).n_almost_full).c_str());
      printf( "%6s |", smon_components::printf_with_number_of( parallel_output_fifo_monitor.at(idx).n_almost_full).c_str());
    }
    printf("\n");
    printf("%29s||", "");
    printf("%10s|",   "cnt ");
    for( int idx = 0; idx < 8; ++idx){
      printf( "%6s |", smon_components::printf_with_number_of( parallel_input_fifo_monitor.at(idx).cnt_almost_full ).c_str() );
      printf( "%6s |", smon_components::printf_with_number_of( parallel_output_fifo_monitor.at(idx).cnt_almost_full ).c_str() );
    }
    printf("\n");
    printf("%29s||", "");
    printf("%10s|",   "occupancy ");
    for( int idx = 0; idx < 8; ++idx){
      printf( "%6s |", smon_components::printf_percent( parallel_input_fifo_monitor.at(idx).occupancy ).c_str() );
      printf( "%6s |", smon_components::printf_percent( parallel_output_fifo_monitor.at(idx).occupancy ).c_str() );
    }
    printf("\n");
    printf("%29s||", "");
    printf("%10s|",   "busy frac ");
    for( int idx = 0; idx < 8; ++idx){
      printf( "%6s |", smon_components::printf_percent( parallel_input_fifo_monitor.at(idx).busy_fraction ).c_str() );
      printf( "%6s |", smon_components::printf_percent( parallel_output_fifo_monitor.at(idx).busy_fraction ).c_str() );
    }
    
    printf("\n");
    printf("%29s||", "");
    printf("%10s|", "lost word ");
    for( int idx = 0; idx < 8; ++idx){
      printf( "%6s |", smon_components::printf_with_number_of( parallel_input_fifo_monitor.at(idx).n_lost_word ).c_str() );
      printf( "%6s |", smon_components::printf_with_number_of( parallel_output_fifo_monitor.at(idx).n_lost_word ).c_str() );
    }
    
    printf("\n");
    
  }
  
}
