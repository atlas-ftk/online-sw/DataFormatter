// written by Mykhailo.Lisovyi@cern.ch 

#include <fstream>
#include <sstream>
#include <vector>
#include <string>
#include <ctime>
//#include <iomanip>
//#include <locale>
//#include <iostream>

#include <stdio.h>
#include <unistd.h>
//#include <stdlib.h>
//#include <stdint.h>

#include "DataFormatter/DataFlowReaderDF.h"
#include "ftkcommon/SourceIDSpyBuffer.h"
#include <ftkcommon/StatusRegister/StatusRegisterISVector.h>
#include <ftkcommon/Utils.h>

// ers
#include <ers/ers.h>
#include "ftkcommon/exceptions.h"
#include "ftkcommon/core.h"

// emon
#include <emon/EventIterator.h>
#include <eformat/ROBFragmentNoTemplates.h>
#include <eformat/FullEventFragmentNoTemplates.h>
#include <eformat/Issue.h>

// ipc
#include <ipc/partition.h>
#include <ipc/core.h>



using namespace std;
using namespace daq::ftk;


std::string ultostr(const uint32_t &value)
{
  char buf[BUFSIZ];
  snprintf(buf, sizeof(buf), "%u", value);
  return std::string(buf);
}

void sb_dump(const emon::Event& ee, uint32_t lane, const string& outdir);
void counter_dump(shared_ptr<DataFlowReaderDF> dfreaderDF, bool shortMonitor = false);
void gt_dump(shared_ptr<DataFlowReaderDF> dfreaderDF);

//============================================================
void print_usage ()
{
  printf("-h          : produce help message \n");
  printf("-M          : mode (see below) \n");
  printf("-D <string> : device ID  \n");  
  printf("-p <string> : [optional] RunControl Partition to be monitored (FTK_DF is default)  \n");  
  printf("-T <int>    : [optional] Timeout for emon access in seconds (500 s is default)  \n"); 
  printf("-o <string> : [optional] Output directory path (./ is default)  \n"); 
  printf("-N <int>    : [optional] Number of iterations  (1  is default)  \n");
  printf("-t <int>    : [optional] Period of iterations  (30 min is default)  \n");
  printf("-n <string> : [optional] RCD name that publishes to emon (FTK-RCD-AtcaCrate-1 is default)  \n");
  printf("-s          : [optional] short output for mode 2 (fits on a screen) \n");  
  printf("----------------------------------- \n");
  printf("MODE options \n");
  printf("2  : Word counter monitoring \n");
  printf("5  : Spy Buffer Dump (-L <uint32> to lane selection(0xff to dump whole FullEventFragment)) \n");
  printf("9  : GT CHANNEL MONITOR \n");
}

//============================================================
int main ( int argc,char* argv[] )
{
  int c;
  std::string device_id	= "";
  bool rc		= false;
  bool results		= true;
  uint32_t lane_id	= 0;
  uint32_t timeout	= 500;  // emon timeout in sec
  int mode		= 0;
  int niter		= 1; // # iterations
  int titer		= 30*60; // iteration period: 30 min in sec  
  string partitionName = "FTK_DF";
  string serverName = "DF"; //DataFlow, will be changed later to FTK* (?)
  string outdir         = "./";
  bool shortMonitor	= false;
  //TODO: update later with some smart name deduction based on the DF identifier (not possible so far)
  string rcdName	= "FTK-RCD-AtcaCrate-1";

  
  while ((c = getopt (argc, argv, "hD:M:L:p:T:o:N:t:n:s")) != -1) {
    switch (c)
      {
      case 'h':
	print_usage();
	return EXIT_SUCCESS;
      case 'M':
	mode=strtoul(optarg, NULL, 0);
	break;
      case 'D':
	rc=true;
	device_id=optarg;
	break;
      case 'L':
	lane_id=strtoul(optarg, NULL, 0);
	break;
      case 'p':
	partitionName=optarg;
	break;
      case 'T':
	timeout=strtoul(optarg, NULL, 0);
	break;
      case 'N':
	niter=strtoul(optarg, NULL, 0);
	break;
      case 't':
	titer=strtoul(optarg, NULL, 0);
	break;
      case 'n':
	rcdName=optarg;
	break;
      case 'o':
	outdir=optarg;
	break;
      case 's':
	shortMonitor = true;
	break;
      default:
	print_usage();
	return EXIT_FAILURE;
      }
  }

  if (not rc) {
    print_usage();
    return EXIT_FAILURE;    
  }

  // define IS object name
  if (device_id.back()=='D') device_id.pop_back();
  string isObjName = serverName + ".FTK." + device_id;
  //  attribute name to be defined depending on the mode chosen
  string isAttrName;

  //initialize communication library (= ipc)
  IPCCore::init(argc, argv);
  //create a partition to run in (= same as the RC partition)
  IPCPartition partition(partitionName);

  shared_ptr<DataFlowReaderDF> dfReaderDF;
  if(mode !=5) {
    dfReaderDF = shared_ptr<DataFlowReaderDF>(new DataFlowReaderDF(NULL, true));
    dfReaderDF->init(device_id,partitionName);
    dfReaderDF->readIS(1);
  }
  
  if (mode==2)   { // ===================================================
    counter_dump(dfReaderDF, shortMonitor);

    results = true;

  } else if (mode==5)   { // ===================================================
   string device_id_number = device_id;
   device_id_number.erase(0,2);

   // create an EventIterator instance to access events on emon
   emon::SelectionCriteria emonCriteria( emon::L1TriggerType(0, true), emon::SmartBitValue(), emon::SmartStreamValue(), emon::StatusWord() );
   emon::SamplingAddress emonAddress( "ReadoutApplication", rcdName.c_str());

   std::unique_ptr<emon::EventIterator> eit;
   try {
     eit.reset( new emon::EventIterator( partition, emonAddress, emonCriteria ));
   } catch (emon::Exception & ex) {
     ers::error( ex );
     return 1;
   }
   
   for(int k = 0; k < niter; k++) {
    // read out the next event from emon (wait for the event to appear for timeout seconds)
    emon::Event event;
    try {
      printf("Waiting %i seconds for the events on EMON to appear \n",timeout);
      event = eit -> nextEvent( timeout*1000 ); 
    } catch ( emon::NoMoreEvents & ex ) {
      ers::warning( ex );
    } catch ( emon::Exception & ex ) {
      throw( ex );
    }

    // print the first event
    sb_dump(event, lane_id, outdir);
    results = true;
    
    if(niter > 1)
    {
      ERS_LOG("Sleeping for "<<titer<<" seconds...");
      sleep(titer);
    }
   }

  } else if (mode==9)   { // ===================================================

    gt_dump(dfReaderDF);


    results = true;
  } else {
    print_usage();
    results = false;
  }
  
  return (results) ? EXIT_SUCCESS : EXIT_FAILURE;  
}

void sb_dump(const emon::Event& ee, uint32_t lane, const string& outdir)
{
    time_t ct = time(nullptr);
    tm* lt = localtime(&ct);

    stringstream ss;
    ss<<(lt->tm_year+1900) << setw(2) << setfill('0') << lt->tm_mon << lt->tm_mday << "_" 
      << lt->tm_hour << lt->tm_min << lt->tm_sec;

    string fout_name = outdir + "sb_dump_" + to_string(lane) + "_" + ss.str() + ".txt";
    fstream fout_stream (fout_name, fstream::out);
    fout_stream << setfill( '0' );
    cout << setfill( '0' );
    //const unsigned int * data = event.data();
    const unsigned int * data;
    uint32_t ndata=0;
    if(lane == 0xff) {
      data = ee.data();
      ndata = ee.size();
    } else {
      eformat::read::FullEventFragment fe( ee.data() );
        try {
        fe.check();
      } catch (eformat::Issue ex) {
        throw ex;
      }
      ERS_LOG("Event fragment contains " + to_string(fe.nchildren()) + " children" );

      if(lane >= fe.nchildren()) 
      {
         ERS_LOG("# of ROB fragments is smaller than the requested lane :"<<to_string(fe.nchildren())<<" < "<<to_string(lane) );
         throw 1;
      }

      //ERS_LOG("source_id = "<<fe.source_id()<<"   hex  = "<<hex<<fe.source_id());
      SourceIDSpyBuffer decoded_sourceID = decode_SourceIDSpyBuffer( fe.source_id() );
      ERS_LOG("########## FullEventFragment ##########");
      ERS_LOG("Is Spy Buffer  = " << decoded_sourceID.dataType);
      ERS_LOG("Board internal = " << decoded_sourceID.boardInternal);
      ERS_LOG("Position       = " << hex << decoded_sourceID.position);
      ERS_LOG("Detector       = " << hex << decoded_sourceID.subDetector);
      ERS_LOG("Board type     = " << hex << decoded_sourceID.boardType);
      ERS_LOG("Board number   = " << decoded_sourceID.boardNumber);
      ERS_LOG("Reserved       = " << hex << decoded_sourceID.reserved);

      // rely on the fact that ROBFragments in FullEventFragment are ordered
      eformat::read::ROBFragment rob( fe.child(lane) );

      SourceIDSpyBuffer decoded_rob_sourceID = decode_SourceIDSpyBuffer( rob.source_id() );
      ERS_LOG("########## ROBFragment ##########");
      ERS_LOG("Board internal = " << decoded_rob_sourceID.boardInternal);
      ERS_LOG("Position       = " << hex << decoded_rob_sourceID.position);
      ERS_LOG("Detector       = " << hex << decoded_rob_sourceID.subDetector);
      ERS_LOG("Board type     = " << hex << decoded_rob_sourceID.boardType);
      ERS_LOG("Board number   = " << decoded_rob_sourceID.boardNumber);
      ERS_LOG("Reserved       = " << hex << decoded_rob_sourceID.reserved);
 
      data = rob.rod_data();
      ndata = rob.rod_ndata();
    }
    
    cout << "First 20 entries from the event on emon (check "<< fout_name.c_str() <<" file in the current directory for full printout):"<<endl;
    for ( size_t i = 0; i < ndata; i++)
    {
      if( i < 20 )
        cout     << "0x" << setw(8) << std::hex << data[i] << endl;

      fout_stream<< "0x" << setw(8) << std::hex << data[i] << endl;
    }
    fout_stream.close();
}


//============================================================
void counter_dump(shared_ptr<DataFlowReaderDF> dfreaderDF, bool shortMonitor)
{
  const uint32_t n_slink_lsc_lane_offset =  0;
  const uint32_t n_slink_lsc_lanes       = 36;//34
  const uint32_t n_fmc_input_lane_offset = n_slink_lsc_lane_offset + n_slink_lsc_lanes;
  const uint32_t n_fmc_input_lanes       = 16;
  const uint32_t n_raw_input_lane_offset = n_fmc_input_lane_offset + n_fmc_input_lanes;
  const uint32_t n_raw_input_lanes       = 8;
  const uint32_t n_odo_in_lane_offset    = n_raw_input_lane_offset + n_raw_input_lanes;
  const uint32_t n_odo_in_lanes          = 32;
  const uint32_t n_ilo_in_lane_offset    = n_odo_in_lane_offset + n_odo_in_lanes;
  const uint32_t n_ilo_in_lanes          = 32;
  const uint32_t n_output_switch_in_lane_offset  = n_ilo_in_lane_offset + n_ilo_in_lanes;
  const uint32_t n_output_switch_in_lanes        = 32;
  const uint32_t n_output_switch_out_lane_offset = n_output_switch_in_lane_offset + n_output_switch_in_lanes;
  const uint32_t n_output_switch_out_lanes       = 32;
  const uint32_t n_output_event_sorter_in_lane_offset = n_output_switch_out_lane_offset + n_output_switch_out_lanes;
  const uint32_t n_output_event_sorter_in_lanes       = 36;//34
  const uint32_t n_central_switch_out_lane_offset = n_output_event_sorter_in_lane_offset + n_output_event_sorter_in_lanes;
  const uint32_t n_central_switch_out_lanes       = 32;
  const uint32_t n_internallink_output_switch_in_lane_offset  = n_central_switch_out_lane_offset + n_central_switch_out_lanes;
  const uint32_t n_internallink_output_switch_in_lanes        = 32;
  const uint32_t n_internallink_output_switch_out_lane_offset = n_internallink_output_switch_in_lane_offset + n_internallink_output_switch_in_lanes;
  const uint32_t n_internallink_output_switch_out_lanes       = 32;
  const uint32_t n_internal_link_out_lane_offset = n_internallink_output_switch_out_lane_offset + n_internallink_output_switch_out_lanes;
  const uint32_t n_internal_link_out_lanes       = 24;
  const uint32_t n_input_switch_lane_offset = n_internal_link_out_lane_offset + n_internal_link_out_lanes;
  const uint32_t n_input_switch_lanes       = 16;
  const uint32_t n_internal_event_counter_lane_offset = n_input_switch_lane_offset + n_input_switch_lanes;
  const uint32_t n_internal_event_counter_lanes       = 16;
  const uint32_t n_frame_path_lane_offset =  n_internal_event_counter_lane_offset + n_internal_event_counter_lanes;
  const uint32_t n_frame_path_lanes       =  1;
  //  const uint32_t n_counter_total = n_frame_path_lane_offset + n_frame_path_lanes;
  
  const uint32_t n_type = 4;
  const std::string type_name[n_type] = {"ctrl", "data", "xoff", "xoffmon"};
  
  //Obsolete commands:
  //uint32_t enable_fmc_lanes_mask = 0X0;
  //df.single_access_read("reg.enable_fmc_lanes_mask", enable_fmc_lanes_mask); 
  //uint32_t input_xoff = 0X0;
  //df.single_access_read("reg.input_buffer_xoff", input_xoff);


  if(shortMonitor == false)
   printf("    %1s %1s | %1s %10s %10s | %1s %10s %10s | %1s %10s | %1s %10s | %1s %10s | %1s %15s | %1s %10s %10s | %1s %10s %10s || %1s %13s | %1s %13s | %1s %13s | %1s %13s | %1s %13s\n",
	 "", "", 
	 "", "FMCIN", "",
	 "", "INPUTSW IN", "last evt",
	 "", "ODO IN",
	 "", "OUTSW IN",
	 "", "OUTSW OUT",
	 "", "PACKER IN (MOD)",
	 "", "PACKER IN", "(FRAME)",
	 "", "SLINK OUT", "",
	 "", "CENTSW IN",
	 "", "CENTSW OUT",
	 "", "INTOUTSW IN",
	 "", "INTOUTSW OUT",
	 "", "IntLink TX");
  else
   printf("    %1s %1s | %1s %10s %10s | %1s %10s %10s | %1s %10s | %1s %10s | %1s %10s | %1s %15s | %1s %10s %10s | %1s %10s %10s || \n",
	 "", "", 
	 "", "FMCIN", "",
	 "", "INPUTSW IN", "last evt",
	 "", "ODO IN",
	 "", "OUTSW IN",
	 "", "OUTSW OUT",
	 "", "PACKER IN (MOD)",
	 "", "PACKER IN", "(FRAME)",
	 "", "SLINK OUT", "");

  
  for (uint32_t ii=0; ii<36; ii++) { //34
   if(shortMonitor == false)
    printf("%2d: %1s %1s | %1s %10s %10s | %1s %10s (%8s) | %1s %10s | %1s %10s | %1s %10s | %1s %15s | %1s %10s %10s | %1s %10s %10s || %1s %13s | %1s %13s | %1s %13s | %1s %13s | %1s %13s\n",
	   ii,
	   (ii<n_fmc_input_lanes)          ? ((dfreaderDF->readRegister(0x1D) & (0X1<<ii))==0)?                    "" : "E" : "-",
	   (ii<n_fmc_input_lanes)          ? ((dfreaderDF->readRegister(0x0F) >>ii)&0X1)==0X0 ?                              "" : "X" : "-",
	   (ii<n_fmc_input_lanes)          ? (dfreaderDF->readRegister(merge2x16bIn32b(3,n_fmc_input_lane_offset+ii),0,0x74,0x70)==0X0) ?          "R" : "I" : "-", // idle or read mode
	   (ii<n_fmc_input_lanes)          ? ultostr(dfreaderDF->readRegister(merge2x16bIn32b(0,n_fmc_input_lane_offset+ ii),0,0x74,0x70)).c_str() : "-",
	   (ii<n_fmc_input_lanes)          ? ultostr(dfreaderDF->readRegister(merge2x16bIn32b(1,n_fmc_input_lane_offset+ ii),0,0x74,0x70)).c_str() : "-",
	   (ii<n_input_switch_lanes)       ? (dfreaderDF->readRegister(merge2x16bIn32b(3,n_input_switch_lane_offset+ii),0,0x74,0x70)==0X0) ?             "" : "X" : "-",
	   (ii<n_input_switch_lanes)       ? ultostr(dfreaderDF->readRegister(merge2x16bIn32b(1,n_input_switch_lane_offset+ii),0,0x74,0x70)).c_str() : "-",
	   (ii<n_internal_event_counter_lanes) ? ultostr(dfreaderDF->readRegister(merge2x16bIn32b(1,n_internal_event_counter_lane_offset+ii),0,0x74,0x70)).c_str() : "-",
	   (ii<n_odo_in_lanes)             ? (dfreaderDF->readRegister(merge2x16bIn32b(3,n_odo_in_lane_offset+ii),0,0x74,0x70)==0X0) ?             "" : "X" : "-",
	   (ii<n_odo_in_lanes)             ? ultostr(dfreaderDF->readRegister(merge2x16bIn32b(1,n_odo_in_lane_offset+ii),0,0x74,0x70)).c_str() : "-",
	   (ii<n_output_switch_in_lanes)   ? (dfreaderDF->readRegister(merge2x16bIn32b(3,n_output_switch_in_lane_offset+ii),0,0x74,0x70)==0X0) ?   "" : "X" : "-",
	   (ii<n_output_switch_in_lanes)   ? ultostr(dfreaderDF->readRegister(merge2x16bIn32b(1,n_output_switch_in_lane_offset+ii),0,0x74,0x70)).c_str() : "-",
	   (ii<n_output_switch_out_lanes)  ? (dfreaderDF->readRegister(merge2x16bIn32b(3,n_output_switch_out_lane_offset+ii),0,0x74,0x70)==0X0) ?  "" : "X" : "-",
	   (ii<n_output_switch_out_lanes)  ? ultostr(dfreaderDF->readRegister(merge2x16bIn32b(1,n_output_switch_out_lane_offset+ii),0,0x74,0x70)).c_str() : "-",
	   (ii<n_output_event_sorter_in_lanes)  ? (dfreaderDF->readRegister(merge2x16bIn32b(3,n_output_event_sorter_in_lane_offset+ii),0,0x74,0x70)==0X0) ?  "" : "X" : "-",
	   (ii<n_output_event_sorter_in_lanes)  ? ultostr(dfreaderDF->readRegister(merge2x16bIn32b(1,n_output_event_sorter_in_lane_offset+ii),0,0x74,0x70)).c_str() : "-",
	   (ii<n_frame_path_lanes)          ? (dfreaderDF->readRegister(merge2x16bIn32b(3,n_frame_path_lane_offset+ii),0,0x74,0x70)==0X0) ?          "" : "X" : "-",
	   (ii<n_frame_path_lanes)          ? ultostr(dfreaderDF->readRegister(merge2x16bIn32b(0,n_frame_path_lane_offset+ii),0,0x74,0x70)).c_str() : "-",
	   (ii<n_frame_path_lanes)          ? ultostr(dfreaderDF->readRegister(merge2x16bIn32b(1,n_frame_path_lane_offset+ii),0,0x74,0x70)).c_str() : "-",
	   (ii<n_slink_lsc_lanes)           ? (dfreaderDF->readRegister(merge2x16bIn32b(3,n_slink_lsc_lane_offset+ii),0,0x74,0x70)==0X0) ?          "" : "X" : "-",
	   (ii<n_slink_lsc_lanes)          ? ultostr(dfreaderDF->readRegister(merge2x16bIn32b(0,n_slink_lsc_lane_offset+ii),0,0x74,0x70)).c_str() : "-",
	   (ii<n_slink_lsc_lanes)          ? ultostr(dfreaderDF->readRegister(merge2x16bIn32b(1,n_slink_lsc_lane_offset+ii),0,0x74,0x70)).c_str() : "-",
	   (ii<n_ilo_in_lanes)             ? (dfreaderDF->readRegister(merge2x16bIn32b(3,n_ilo_in_lane_offset+ii),0,0x74,0x70)==0X0) ?             "" : "X" : "-",
	   (ii<n_ilo_in_lanes)             ? ultostr(dfreaderDF->readRegister(merge2x16bIn32b(1,n_ilo_in_lane_offset+ii),0,0x74,0x70)).c_str() : "-",
	   (ii<n_central_switch_out_lanes) ? (dfreaderDF->readRegister(merge2x16bIn32b(3,n_central_switch_out_lane_offset+ii),0,0x74,0x70)==0X0) ? "" : "X" : "-",
	   (ii<n_central_switch_out_lanes) ? ultostr(dfreaderDF->readRegister(merge2x16bIn32b(1,n_central_switch_out_lane_offset+ii),0,0x74,0x70)).c_str() : "-",
	   (ii<n_internallink_output_switch_in_lanes)   ? (dfreaderDF->readRegister(merge2x16bIn32b(0,n_internallink_output_switch_in_lane_offset+ii),0,0x74,0x70)==0X0) ?   "" : "X" : "-",
	   (ii<n_internallink_output_switch_in_lanes)   ? ultostr(dfreaderDF->readRegister(merge2x16bIn32b(1,n_internallink_output_switch_in_lane_offset+ii),0,0x74,0x70)).c_str() : "-",
	   (ii<n_internallink_output_switch_out_lanes)  ? (dfreaderDF->readRegister(merge2x16bIn32b(0,n_internallink_output_switch_out_lane_offset+ii),0,0x74,0x70)==0X0) ?  "" : "X" : "-",
	   (ii<n_internallink_output_switch_out_lanes)  ? ultostr(dfreaderDF->readRegister(merge2x16bIn32b(1,n_internallink_output_switch_out_lane_offset+ii),0,0x74,0x70)).c_str() : "-",
	   (ii<n_internal_link_out_lanes)  ? (dfreaderDF->readRegister(merge2x16bIn32b(0,n_internal_link_out_lane_offset+ii),0,0x74,0x70)==0X0) ?  "" : "X" : "-",
	   (ii<n_internal_link_out_lanes)  ? ultostr(dfreaderDF->readRegister(merge2x16bIn32b(1,n_internal_link_out_lane_offset+ii),0,0x74,0x70)).c_str() : "-"
	   );
   else
     printf("%2d: %1s %1s | %1s %10s %10s | %1s %10s (%8s) | %1s %10s | %1s %10s | %1s %10s | %1s %15s | %1s %10s %10s | %1s %10s %10s ||\n",
	   ii,
	   (ii<n_fmc_input_lanes)          ? ((dfreaderDF->readRegister(0x1D) & (0X1<<ii))==0)?                    "" : "E" : "-",
	   (ii<n_fmc_input_lanes)          ? ((dfreaderDF->readRegister(0x0F) >>ii)&0X1)==0X0 ?                              "" : "X" : "-",
	   (ii<n_fmc_input_lanes)          ? (dfreaderDF->readRegister(merge2x16bIn32b(3,n_fmc_input_lane_offset+ii),0,0x74,0x70)==0X0) ?          "R" : "I" : "-", // idle or read mode
	   (ii<n_fmc_input_lanes)          ? ultostr(dfreaderDF->readRegister(merge2x16bIn32b(0,n_fmc_input_lane_offset+ ii),0,0x74,0x70)).c_str() : "-",
	   (ii<n_fmc_input_lanes)          ? ultostr(dfreaderDF->readRegister(merge2x16bIn32b(1,n_fmc_input_lane_offset+ ii),0,0x74,0x70)).c_str() : "-",
	   (ii<n_input_switch_lanes)       ? (dfreaderDF->readRegister(merge2x16bIn32b(3,n_input_switch_lane_offset+ii),0,0x74,0x70)==0X0) ?             "" : "X" : "-",
	   (ii<n_input_switch_lanes)       ? ultostr(dfreaderDF->readRegister(merge2x16bIn32b(1,n_input_switch_lane_offset+ii),0,0x74,0x70)).c_str() : "-",
	   (ii<n_internal_event_counter_lanes) ? ultostr(dfreaderDF->readRegister(merge2x16bIn32b(1,n_internal_event_counter_lane_offset+ii),0,0x74,0x70)).c_str() : "-",
	   (ii<n_odo_in_lanes)             ? (dfreaderDF->readRegister(merge2x16bIn32b(3,n_odo_in_lane_offset+ii),0,0x74,0x70)==0X0) ?             "" : "X" : "-",
	   (ii<n_odo_in_lanes)             ? ultostr(dfreaderDF->readRegister(merge2x16bIn32b(1,n_odo_in_lane_offset+ii),0,0x74,0x70)).c_str() : "-",
	   (ii<n_output_switch_in_lanes)   ? (dfreaderDF->readRegister(merge2x16bIn32b(3,n_output_switch_in_lane_offset+ii),0,0x74,0x70)==0X0) ?   "" : "X" : "-",
	   (ii<n_output_switch_in_lanes)   ? ultostr(dfreaderDF->readRegister(merge2x16bIn32b(1,n_output_switch_in_lane_offset+ii),0,0x74,0x70)).c_str() : "-",
	   (ii<n_output_switch_out_lanes)  ? (dfreaderDF->readRegister(merge2x16bIn32b(3,n_output_switch_out_lane_offset+ii),0,0x74,0x70)==0X0) ?  "" : "X" : "-",
	   (ii<n_output_switch_out_lanes)  ? ultostr(dfreaderDF->readRegister(merge2x16bIn32b(1,n_output_switch_out_lane_offset+ii),0,0x74,0x70)).c_str() : "-",
	   (ii<n_output_event_sorter_in_lanes)  ? (dfreaderDF->readRegister(merge2x16bIn32b(3,n_output_event_sorter_in_lane_offset+ii),0,0x74,0x70)==0X0) ?  "" : "X" : "-",
	   (ii<n_output_event_sorter_in_lanes)  ? ultostr(dfreaderDF->readRegister(merge2x16bIn32b(1,n_output_event_sorter_in_lane_offset+ii),0,0x74,0x70)).c_str() : "-",
	   (ii<n_frame_path_lanes)          ? (dfreaderDF->readRegister(merge2x16bIn32b(3,n_frame_path_lane_offset+ii),0,0x74,0x70)==0X0) ?          "" : "X" : "-",
	   (ii<n_frame_path_lanes)          ? ultostr(dfreaderDF->readRegister(merge2x16bIn32b(0,n_frame_path_lane_offset+ii),0,0x74,0x70)).c_str() : "-",
	   (ii<n_frame_path_lanes)          ? ultostr(dfreaderDF->readRegister(merge2x16bIn32b(1,n_frame_path_lane_offset+ii),0,0x74,0x70)).c_str() : "-",
	   (ii<n_slink_lsc_lanes)           ? (dfreaderDF->readRegister(merge2x16bIn32b(3,n_slink_lsc_lane_offset+ii),0,0x74,0x70)==0X0) ?          "" : "X" : "-",
	   (ii<n_slink_lsc_lanes)          ? ultostr(dfreaderDF->readRegister(merge2x16bIn32b(0,n_slink_lsc_lane_offset+ii),0,0x74,0x70)).c_str() : "-",
	   (ii<n_slink_lsc_lanes)          ? ultostr(dfreaderDF->readRegister(merge2x16bIn32b(1,n_slink_lsc_lane_offset+ii),0,0x74,0x70)).c_str() : "-"
	   );

  }
  
}




void gt_dump(shared_ptr<DataFlowReaderDF> dfreaderDF){
    const uint32_t nGtChannel=58; // 34 + 24
    for (uint32_t iGt=0; iGt<nGtChannel; iGt++) {
      printf("GT=[%2d] :  RXFLIP=[%3s] TXFLIP=[%3s] "
	     "FORCE_READY=[%3s] TO_ALTERA=[%3s] BYTEALIGNE=[%4s] "
	     "TX_RST=[%4s] RX_RST=[%4s] PLL[%9s] "
	     "SLINK : TEST=[%4s] LDERR=[%4s] LUP=[%4s] FLOWCTRL=[%4s] ACTIVITY=[%4s] LRL=[%1x]\n",
	     iGt,
	     getBit(dfreaderDF->readRegister(iGt,0,0x09,0x0C),0)==0X0 ? "OFF" : "ON", 
	     getBit(dfreaderDF->readRegister(iGt,0,0x09,0x0C),1)==0X0 ? "OFF" : "ON",
	     getBit(dfreaderDF->readRegister(iGt,0,0x09,0x0C),2)==0X0 ? "OFF" : "ON",
	     getBit(dfreaderDF->readRegister(iGt,0,0x09,0x0C),3)==0X0 ? "OFF" : "ON", 
	     getBit(dfreaderDF->readRegister(iGt,0,0x09,0x08),0)==0X0 ? "YET" : "DONE",
	     getBit(dfreaderDF->readRegister(iGt,0,0x09,0x08),1)==0X0 ? "YET" : "DONE",
	     getBit(dfreaderDF->readRegister(iGt,0,0x09,0x08),2)==0X0 ? "YET" : "DONE",
	     getBit(dfreaderDF->readRegister(iGt,0,0x09,0x08),3)==0X0 ? "NOT LOCKED"  : "LOCKED",
	     getBit(dfreaderDF->readRegister(iGt,0,0x09,0x08),4)==0X0 ? "ON" : "OFF",
	     getBit(dfreaderDF->readRegister(iGt,0,0x09,0x08),5)==0X0 ? "BAD" : "GOOD",
	     getBit(dfreaderDF->readRegister(iGt,0,0x09,0x08),6)==0X0 ? "GOOD" : "BAD",
	     getBit(dfreaderDF->readRegister(iGt,0,0x09,0x08),7)==0X0 ? "ON" : "OFF",
	     getBit(dfreaderDF->readRegister(iGt,0,0x09,0x08),8)==0X0 ? "YES" : "NO",
	     dfreaderDF->readRegister(iGt,0,0x09,0x04));
    }

}


