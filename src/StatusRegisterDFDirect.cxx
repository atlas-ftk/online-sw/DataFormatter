#include <DataFormatter/StatusRegisterDFDirect.h>
#include "ftkcommon/exceptions.h"

using namespace daq::ftk;

StatusRegisterDFDirect::StatusRegisterDFDirect (std::shared_ptr<FtkDataFormatterApi> dfapi, std::string node, srType type)
{
  m_dfapi	= dfapi;
  m_node	= node;
  m_access	= srAccess::IPBus_direct;
  m_type	= type;
}

StatusRegisterDFDirect::StatusRegisterDFDirect (std::shared_ptr<FtkDataFormatterApi> dfapi, uint32_t addr, srType type)
{
  m_dfapi	= dfapi;
  m_node	= dfapi->getRegisterNameFromAddress(addr);
  m_access	= srAccess::IPBus_direct;
  m_type	= type;
}


// ====================================================
void 
StatusRegisterDFDirect::readout()
{
  try {
    m_dfapi->single_access_read(m_node, m_value);
  } catch (daq::ftk::IPBusRead ex) { 
    m_value = srNoneValue;
    daq::ftk::IPBusIssue ex2(ERS_HERE, m_name, " Failed to read out a DF status register",ex);
    ers::warning(ex2);
  }
}
