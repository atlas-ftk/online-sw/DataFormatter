#include <DataFormatter/FtkCoolSchema.h>

#include "CoolKernel/StorageType.h"
#include "CoolKernel/Record.h"

#include "CoolKernel/PayloadMode.h"
#include "CoolKernel/RecordSpecification.h"
#include "CoolKernel/ChannelId.h"
#include "CoolKernel/pointers.h"

namespace FtkCoolSchema {

  std::map<std::string, std::shared_ptr<Folder> > tables() {

    // Tables
    std::map<std::string, std::shared_ptr<Folder> > result;
    result["ROB"] = std::shared_ptr<Folder>(new ROB());
    result["Module"] = std::shared_ptr<Folder>(new Module());
    result["IM"] = std::shared_ptr<Folder>(new IM());
    result["DF"] = std::shared_ptr<Folder>(new DF());

    return result;
  }

  ROB::ROB() :Folder(coolFolderName("ROB")) {}
  std::shared_ptr<cool::RecordSpecification> ROB::spec() {
    if (_spec) return _spec;

    _spec = std::shared_ptr<cool::RecordSpecification>(new cool::RecordSpecification());
    _spec->extend( "ROB", cool::StorageType::String255 );
    _spec->extend( "create", cool::StorageType::Int32 );//UChar ); // 0-4 - 8 bits (uchar)
    _spec->extend( "slot", cool::StorageType::Int32 );//UChar ); //
    _spec->extend( "lane", cool::StorageType::Int32 );//UChar ); //


    return _spec;
  }

  Module::Module() :Folder(coolFolderName("Module")) {}
  std::shared_ptr<cool::RecordSpecification> Module::spec() {
    if (_spec) return _spec;

    _spec = std::shared_ptr<cool::RecordSpecification>(new cool::RecordSpecification());
    _spec->extend( "onlineId", cool::StorageType::Int32 );
    _spec->extend( "hashId", cool::StorageType::Int32 );
    _spec->extend( "hittype", cool::StorageType::Int32 );
    _spec->extend( "planeInfo", cool::StorageType::Int32 );
    //    _spec->extend( "towerVec", std::vector<cool::StorageType::Int32> );
    _spec->extend( "towerList", cool::StorageType::String255 );
    _spec->extend( "rob", cool::StorageType::String255 );

    return _spec;
  }
  
  IM::IM() :Folder(coolFolderName("IM")) {}
  std::shared_ptr<cool::RecordSpecification> IM::spec() {
    if (_spec) return _spec;
    
    _spec = std::shared_ptr<cool::RecordSpecification>(new cool::RecordSpecification());
    _spec->extend( "laneNum", cool::StorageType::Int32 );
    _spec->extend( "rodId", cool::StorageType::Int32 );
    _spec->extend( "isPixel", cool::StorageType::Bool );
    //    _spec->extend( "moduleList", std::vector<cool::StorageType::Int32> );
    _spec->extend( "DFshelf", cool::StorageType::Int16 );
    _spec->extend( "IMFPGA", cool::StorageType::Int32 );
    _spec->extend( "IMFMC", cool::StorageType::Int32 );

    return _spec;
  }
  
  DF::DF() :Folder(coolFolderName("DF")) {}
  std::shared_ptr<cool::RecordSpecification> DF::spec() {
    if (_spec) return _spec;

    _spec = std::shared_ptr<cool::RecordSpecification>(new cool::RecordSpecification());
    _spec->extend( "im", cool::StorageType::Int32 );
    _spec->extend( "planeOutputBit", cool::StorageType::Int32 );
    _spec->extend( "tower", cool::StorageType::Int32 );
    _spec->extend( "slot", cool::StorageType::Int32 );
    _spec->extend( "shelf", cool::StorageType::Int32 );

    return _spec;
  }
  

  int robHashHelper(const cool::IRecordPtr &data) 
  {
    cool::Record* row = dynamic_cast<cool::Record*>(data.get());
    return (*row)["ROB"].data<int32_t>();
  }

  int modHashHelper(const cool::IRecordPtr &data)
  {
    cool::Record* row = dynamic_cast<cool::Record*>(data.get());
    return (*row)["rob"].data<int32_t>();
  }
  
  int imHashHelper(const cool::IRecordPtr &data)
  {
    cool::Record* row = dynamic_cast<cool::Record*>(data.get());
    return (*row)["laneNum"].data<int32_t>();
  }
  
  int dfHashHelper(const cool::IRecordPtr &data) 
  {
    cool::Record* row = dynamic_cast<cool::Record*>(data.get());
    return (*row)["im"].data<int32_t>();
  }
  
  std::map<std::string, FolderHashFunction> hashes() {
    std::map<std::string, FolderHashFunction> hashMap;

    hashMap["ROB"] = robHashHelper;
    hashMap["Module"] = modHashHelper;
    hashMap["IM"] = imHashHelper;
    hashMap["DF"] = dfHashHelper;

    return hashMap;
  }

}
  
    
