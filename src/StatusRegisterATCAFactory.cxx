#include "DataFormatter/StatusRegisterATCAFactory.h"
#include "DataFormatter/StatusRegisterIMDirect.h"
#include "DataFormatter/StatusRegisterDFDirect.h"
#include "DataFormatter/StatusRegisterDFCounter.h"
#include "DataFormatter/StatusRegisterDFSwitchFifo.h"
#include "DataFormatter/StatusRegisterDFGT.h"
#include "DataFormatter/StatusRegisterDFSelectorGeneric.h"
//#include "DataFormatter/StatusRegisterDFAccessHistos.h"
#include "DataFormatter/dal/StatusRegisterDFIMStatusNamed.h"

#include "ftkcommon/Utils.h"

#include "ftkcommon/exceptions.h"

using namespace std;

namespace daq {
namespace ftk {

StatusRegisterATCAFactory::StatusRegisterATCAFactory(
	std::string boardName,
	std::string registersToRead,
	std::string isServerName,
	const IPCPartition& ipcPartition,
	std::shared_ptr<FtkDataFormatterApi> dfApi,
	uint32_t df_fwVersion,
	uint blockTransfer
	) : StatusRegisterFactory (boardName, registersToRead, isServerName)
{
  // usage of block transfer
  m_useBlockTransfer = blockTransfer;

  m_df_fwVersion = df_fwVersion;
  // Store passed variables
  m_dfApi = dfApi;

	// IS Publishing Setup
  std::unique_ptr<StatusRegisterDFIMStatusNamed> m_srATCASNamed;
  try {        
    m_srATCASNamed = std::make_unique<StatusRegisterDFIMStatusNamed>(ipcPartition, m_isProvider.c_str());
  } catch(ers::Issue &ex)  {
    ISException e(ERS_HERE, "Error while creating StatusRegisterATCAStatusNamed object", ex);  // Append the original exception (ex) to the new one 
    ers::warning(e);  //or throw e;
    return; // nothing else can be done.
	}
  m_srATCASNamed->Name = m_name;
//--------------------------------------------------------------------------------------------------------------
	setupCollection('Z',0,merge2x16bIn32b(1,0),merge2x16bIn32b(1,420),1,
				"StatusRegisterDFCounter Words","DFwc",
				&(m_srATCASNamed->DF_wcount_sr_v),&(m_srATCASNamed->DF_wcount_sr_v_info),
				0x74,0x70,
				srType::counterOther, srAccess::IPBus_selector);
	setupCollection('Z',0,merge2x16bIn32b(0,0),merge2x16bIn32b(0,420),1,
				"StatusRegisterDFCounter Control Words","DFcwc",
				&(m_srATCASNamed->DF_cwcount_sr_v),&(m_srATCASNamed->DF_cwcount_sr_v_info),
				0x74,0x70,
				srType::counterOther, srAccess::IPBus_selector);
	setupCollection('Z',0,merge2x16bIn32b(2,0),merge2x16bIn32b(2,420),1,
				"StatusRegisterDFCounter XOFF","DFxoffc",
				&(m_srATCASNamed->DF_xoffcount_sr_v),&(m_srATCASNamed->DF_xoffcount_sr_v_info),
				0x74,0x70,
				srType::counterOther, srAccess::IPBus_selector);
	setupCollection('Z',0,merge2x16bIn32b(3,0),merge2x16bIn32b(3,420),1,
				"StatusRegisterDFCounter XOFF Monitor","DFxoffmon",
				&(m_srATCASNamed->DF_xoffmon_sr_v),&(m_srATCASNamed->DF_xoffmon_sr_v_info),
				0x74,0x70,
				srType::counterOther, srAccess::IPBus_selector);
	setupCollection('Z',0,0,50,1,
			"StatusRegisterDFCounter slink total evts given up","DFslinktotalevtsgivenup",
			&(m_srATCASNamed->DF_slink_total_evts_given_up_v),&(m_srATCASNamed->DF_slink_total_evts_given_up_v_info),
			0x5e,0x55,
			srType::counterOther, srAccess::IPBus_selector);
	setupCollection('Z',0,0,50,1,
			"StatusRegisterDFCoutner slink total mods given up", "DFslinktotalmodsgivenup",
			&(m_srATCASNamed->DF_slink_total_modules_given_up_v), &(m_srATCASNamed->DF_slink_total_modules_given_up_v_info),
			0x5e,0x56,
			srType::counterOther, srAccess::IPBus_selector);
	setupCollection('Z',0,0,50,1,
			"StatusRegisterDFCoutner slink event sorting monitor", "DFslinkevtsorting",
			&(m_srATCASNamed->DF_slink_evtsorting_monitor_v), &(m_srATCASNamed->DF_slink_evtsorting_monitor_v_info),
			0x5e,0x54,
			srType::counterOther, srAccess::IPBus_selector);
//--------------------------------------------------------------------------------------------------------------
	setupCollection('U',0,merge2x16bIn32b(1,34),merge2x16bIn32b(1,50),1,
				"StatusRegisterDFCounter Words Short","DFwcs",
				&(m_srATCASNamed->DF_inwcount_sr_v),&(m_srATCASNamed->DF_inwcount_sr_v_info),
				0x74,0x70,
				srType::counterOther, srAccess::IPBus_selector);
	setupCollection('U',0,merge2x16bIn32b(0,34),merge2x16bIn32b(0,50),1,
				"StatusRegisterDFCounter Control Words Short","DFcwcs",
				&(m_srATCASNamed->DF_incwcount_sr_v),&(m_srATCASNamed->DF_incwcount_sr_v_info),
				0x74,0x70,
				srType::counterOther, srAccess::IPBus_selector);
	setupCollection('U',0,merge2x16bIn32b(2,34),merge2x16bIn32b(2,50),1,
				"StatusRegisterDFCounter XOFF Short","DFxoffcs",
				&(m_srATCASNamed->DF_inxoffcount_sr_v),&(m_srATCASNamed->DF_inxoffcount_sr_v_info),
				0x74,0x70,
				srType::counterOther, srAccess::IPBus_selector);
//--------------------------------------------------------------------------------------------------------------
	setupCollection('Y',0,0x000,0x060,1,
				"StatusRegisterDFSwitchFifo Output Switch","DFsfos",
				&(m_srATCASNamed->DF_sfos_sr_v),&(m_srATCASNamed->DF_sfos_sr_v_info),
				0x79,0x7A,
				srType::fifoOther, srAccess::IPBus_selector);
	setupCollection('Y',0,0x100,0x160,1,
				"StatusRegisterDFSwitchFifo Central Switch","DFsfcs",
				&(m_srATCASNamed->DF_sfcs_sr_v),&(m_srATCASNamed->DF_sfcs_sr_v_info),
				0x79,0x7A,
				srType::fifoOther, srAccess::IPBus_selector);
	setupCollection('Y',0,0x200,0x260,1,
				"StatusRegisterDFSwitchFifo Input Link Output Switch","DFsfilos",
				&(m_srATCASNamed->DF_sfilos_sr_v),&(m_srATCASNamed->DF_sfilos_sr_v_info),
				0x79,0x7A,
				srType::fifoOther, srAccess::IPBus_selector);
	setupCollection('Y',0,0x300,0x360,1,
				"StatusRegisterDFSwitchFifo Input Link Input Switch","DFsfilis",
				&(m_srATCASNamed->DF_sfilis_sr_v),&(m_srATCASNamed->DF_sfilis_sr_v_info),
				0x79,0x7A,
				srType::fifoOther, srAccess::IPBus_selector);
	setupCollection('Y',0,0x400,0x460,1,
				"StatusRegisterDFSwitchFifo Output Link Output Switch","DFsfolos",
				&(m_srATCASNamed->DF_sfolos_sr_v),&(m_srATCASNamed->DF_sfolos_sr_v_info),
				0x79,0x7A,
				srType::fifoOther, srAccess::IPBus_selector);
//--------------------------------------------------------------------------------------------------------------
	setupCollection('G',0,0,60,1,
				"StatusRegisterGT 1","DFgt1",
				&(m_srATCASNamed->DF_gt1_sr_v),&(m_srATCASNamed->DF_gt1_sr_v_info),
				0x09,0x0c,
				srType::gtOther, srAccess::IPBus_selector);
	setupCollection('G',0,0,60,1,
				"StatusRegisterGT 2","DFgt2",
				&(m_srATCASNamed->DF_gt2_sr_v),&(m_srATCASNamed->DF_gt2_sr_v_info),
				0x09,0x08,
				srType::gtOther, srAccess::IPBus_selector);
	setupCollection('G',0,0,60,1,
				"StatusRegisterGT 3","DFgt3",
				&(m_srATCASNamed->DF_gt3_sr_v),&(m_srATCASNamed->DF_gt3_sr_v_info),
				0x09,0x04,
				srType::gtOther, srAccess::IPBus_selector);
//--------------------------------------------------------------------------------------------------------------
/*	setupCollection('F',0,0x51,0x59,1,
				"DF input fifo full counter","DFfullc",
				&(m_srATCASNamed->DF_infullfr_sr_v),&(m_srATCASNamed->DF_infullfr_sr_v_info),
				0,0,
				srType::srOther, srAccess::IPBus_direct);
	setupCollection('F',0,0x64,0x65,1,
				"DF input fifo full threshold","DFfullt",
				&(m_srATCASNamed->DF_infullfr_sr_v),&(m_srATCASNamed->DF_infullfr_sr_v_info),
				0,0,
				srType::srOther, srAccess::IPBus_direct);*/
if ( m_df_fwVersion >= 0x19000){
  setupCollection('F',0,0x0F,0x10,1,
		  "DF input xoff","DFinxoff",
		  &(m_srATCASNamed->DF_inxoff_sr_v),&(m_srATCASNamed->DF_inxoff_sr_v_info),
		  0,0,
		  srType::srOther, srAccess::IPBus_direct);
  setupCollection('F',0,0x60,0x61,1,
		  "DF input enabled","DFinenable",
		  &(m_srATCASNamed->DF_fmcenable_sr_v),&(m_srATCASNamed->DF_fmcenable_sr_v_info),
		  0,0,
		  srType::srOther, srAccess::IPBus_direct);
  setupCollection('F',0,36,52,1,
		  "DF fifo Input busy","DFfifoinbusy",
		  &(m_srATCASNamed->DF_inbusy_sr_v),&(m_srATCASNamed->DF_inbusy_sr_v_info),
		  0x4D,0x4F,
		  srType::srOther, srAccess::IPBus_selector);
  setupCollection('F',0,0,36,1,
		  "DF fifo output busy","DFoutbusy",
		  &(m_srATCASNamed->DF_outbusy_sr_v),&(m_srATCASNamed->DF_outbusy_sr_v_info),
		  0x4D,0x4F,
		  srType::srOther, srAccess::IPBus_selector);
  
  setupCollection('F',0,36,52,1,
		  "DF fifo input empty","DFinempty",
		  &(m_srATCASNamed->DF_inempty_sr_v),&(m_srATCASNamed->DF_inempty_sr_v_info),
		  0x4D,0x4E,
		  srType::srOther, srAccess::IPBus_selector);
  
  setupCollection('F',0,0,36,1,
		  "DF fifo output empty","DFoutempty",
		  &(m_srATCASNamed->DF_outempty_sr_v),&(m_srATCASNamed->DF_outempty_sr_v_info),
		  0x4D,0x4E,
		  srType::srOther, srAccess::IPBus_selector);
  
  setupCollection('F',0,0,16,1,
		  "DF l1id","DFl1id",
		  &(m_srATCASNamed->DF_l1id_sr_v),&(m_srATCASNamed->DF_l1id_sr_v_info),
		  0x69,0x6A,
		  srType::srOther, srAccess::IPBus_selector);
  setupCollection('F',0,0,16,1,
		  "DF npackets discarded","DFnpacketsdiscarded",
		  &(m_srATCASNamed->DF_npackets_discarded_sr_v),&(m_srATCASNamed->DF_npackets_discarded_sr_v_info),
		  0x69,0x6C,
		  srType::srOther, srAccess::IPBus_selector);
  setupCollection('F',0,0,16,1,
		  "DF npackets discarded sync","DFnpacketsdiscardedsync",
		  &(m_srATCASNamed->DF_npackets_discarded_sync_sr_v),&(m_srATCASNamed->DF_npackets_discarded_sync_sr_v_info),
		  0x69,0x5b,
		  srType::srOther, srAccess::IPBus_selector);
  setupCollection('F',0,0x65,0x66,1,
		  "DF inputskew sync","DFinputskewsync",
		  &(m_srATCASNamed->DF_inputskew_sync_sr_v),&(m_srATCASNamed->DF_inputskew_sync_sr_v_info),
		  0,0,
		  srType::srOther, srAccess::IPBus_direct);
  
  if ( m_df_fwVersion >= 0x19006 ) { // added full 32-bit counters in 0x19006
    setupCollection('F', 0, 0, 16, 1,
		    "DF npackets discarded synch (full)", "DFnpacketsdiscardedsynchfull",
		    &(m_srATCASNamed->DF_num_packets_discarded_synch_full_sr_v),&(m_srATCASNamed->DF_num_packets_discarded_synch_full_sr_v_info),
		    0x69, 0x42,
		    srType::srOther, srAccess::IPBus_selector);
    setupCollection('F', 0, 0, 16, 1,
		    "DF npackets held synch (full)", "DFnpacketsheldsynchfull",
		    &(m_srATCASNamed->DF_num_packets_held_synch_full_sr_v),&(m_srATCASNamed->DF_num_packets_held_synch_full_sr_v_info),
		    0x69, 0x44,
		    srType::srOther, srAccess::IPBus_selector);
    setupCollection('F', 0, 0, 16, 1,
		    "DF npackets timedout synch (full)", "DFnpacketstimedoutsynchfull",
		    &(m_srATCASNamed->DF_num_packets_timedout_synch_full_sr_v),&(m_srATCASNamed->DF_num_packets_timedout_synch_full_sr_v_info),
		    0x69, 0x45,
		    srType::srOther, srAccess::IPBus_selector);
    setupCollection('F', 0, 0, 16, 1,
		    "DF npackets discarded  (full)", "DFnpacketsdiscardedfull",
		    &(m_srATCASNamed->DF_num_packets_discarded_full_sr_v),&(m_srATCASNamed->DF_num_packets_discarded_full_sr_v_info),
		    0x69, 0x46,
		    srType::srOther, srAccess::IPBus_selector);
    setupCollection('F', 0, 0, 16, 1,
		    "DF num mods hit word limit (full)", "DFnmodshitwordlimitfull",
		    &(m_srATCASNamed->DF_num_mods_hit_word_limit_full_sr_v),&(m_srATCASNamed->DF_num_mods_hit_word_limit_full_sr_v_info),
		    0x69, 0x49,
		    srType::srOther, srAccess::IPBus_selector);
  }

  if ( m_df_fwVersion >= 0x1900d ) { // monitoring added for Milestone T
  
    setupCollection('F', 0, 0x33, 0x34, 1,
                    "DF FMCIN FIFO", "DFfmcinfifo",
                    &(m_srATCASNamed->DF_fmcin_fifo_sr_v),&(m_srATCASNamed->DF_fmcin_fifo_sr_v_info),
                    0, 0,
                    srType::srOther, srAccess::IPBus_direct);
    setupCollection('F', 0, 0x34, 0x35, 1,
                    "DF Int Link RX FIFO", "DFintlinkrxfifo",
                    &(m_srATCASNamed->DF_int_link_rx_fifo_sr_v),&(m_srATCASNamed->DF_int_link_rx_fifo_sr_v_info),
                    0, 0,
                    srType::srOther, srAccess::IPBus_direct);
    setupCollection('F', 0, 0x35, 0x36, 1,
                    "DF Int Link TX FIFO", "DFintlinktxfifo",
                    &(m_srATCASNamed->DF_int_link_tx_fifo_sr_v),&(m_srATCASNamed->DF_int_link_tx_fifo_sr_v_info),
                    0, 0,
                    srType::srOther, srAccess::IPBus_direct);
    setupCollection('F', 0, 0x37, 0x38, 1,
                    "DF SLINK FIFO 1", "DFslinkfifo1",
                    &(m_srATCASNamed->DF_slink_fifo_1_sr_v),&(m_srATCASNamed->DF_slink_fifo_1_sr_v_info),
                    0, 0,
                    srType::srOther, srAccess::IPBus_direct);
    setupCollection('F', 0, 0x38, 0x39, 1,
                    "DF SLINK FIFO 2", "DFslinkfifo2",
                    &(m_srATCASNamed->DF_slink_fifo_2_sr_v),&(m_srATCASNamed->DF_slink_fifo_2_sr_v_info),
                    0, 0,
                    srType::srOther, srAccess::IPBus_direct);
    setupCollection('F', 0, 0x39, 0x3A, 1,
                    "DF ODO event sort buffer FIFO 1", "DFodoevtsortbufffifo1",
                    &(m_srATCASNamed->DF_odo_evt_sort_buff_fifo_1_sr_v),&(m_srATCASNamed->DF_odo_evt_sort_buff_fifo_1_sr_v_info),
                    0, 0,
                    srType::srOther, srAccess::IPBus_direct);
    setupCollection('F', 0, 0x3A, 0x3B, 1,
                    "DF ODO event sort buffer FIFO 2", "DFodoevtsortbufffifo1",
                    &(m_srATCASNamed->DF_odo_evt_sort_buff_fifo_2_sr_v),&(m_srATCASNamed->DF_odo_evt_sort_buff_fifo_2_sr_v_info),
                    0, 0,
                    srType::srOther, srAccess::IPBus_direct);
    setupCollection('F', 0, 0x00000000, 0x00000010, 1,
                    "DF FMCIN backpressure", "DFfmcinbackpressure",
                    &(m_srATCASNamed->DF_fmcin_bp_sr_v), &(m_srATCASNamed->DF_fmcin_bp_sr_v_info),
                    0x3B,0x3C,
                    srType::srOther, srAccess::IPBus_selector);
    setupCollection('F', 0, 0x00000200, 0x00000218, 1,
                    "DF ILI backpressure", "DFilibackpressure",
                    &(m_srATCASNamed->DF_ili_bp_sr_v), &(m_srATCASNamed->DF_ili_bp_sr_v_info),
                    0x3B,0x3C,
                    srType::srOther, srAccess::IPBus_selector);
    setupCollection('F', 0, 0x00000300, 0x00000320, 1,
                    "DF ODO backpressure", "DFodobackpressure",
                    &(m_srATCASNamed->DF_odo_bp_sr_v), &(m_srATCASNamed->DF_odo_bp_sr_v_info),
                    0x3B,0x3C,
                    srType::srOther, srAccess::IPBus_selector);
    setupCollection('F', 0, 0x00000400, 0x00000420, 1,
                    "DF ILO backpressure", "DFilobackpressure",
                    &(m_srATCASNamed->DF_ilo_bp_sr_v), &(m_srATCASNamed->DF_ilo_bp_sr_v_info),
                    0x3B,0x3C,
                    srType::srOther, srAccess::IPBus_selector);

    setupCollection('F',0,0x00001000,0x00001010,1,
                    "DF input global freeze counter", "DFinputglobalfreezecounter",
                    &(m_srATCASNamed->DF_input_global_freeze_sr_v), &(m_srATCASNamed->DF_input_global_freeze_sr_v_info),
                    0x3B,0x3C,
                    srType::srOther, srAccess::IPBus_selector);
    setupCollection('F',0,0x00001300,0x00001324,1,
                    "DF slink LDC freeze counter", "DFslinkldcfreezecounter",
                    &(m_srATCASNamed->DF_slink_ldc_freeze_sr_v), &(m_srATCASNamed->DF_slink_ldc_freeze_sr_v_info),
                    0x3B,0x3C,
                    srType::srOther, srAccess::IPBus_selector);
    setupCollection('F',0,0x00001500,0x00001501,1,
                    "DF FMC OUT to IM freeze counter", "DFfmcoutotimfreezecounter",
                    &(m_srATCASNamed->DF_fmcout_to_im_freeze_sr_v), &(m_srATCASNamed->DF_fmcout_to_im_freeze_sr_v_info),
                    0x3B,0x3C,
                    srType::srOther, srAccess::IPBus_selector);
    
    setupCollection('F',0,0x00002100,0x00002110,1,
                    "DF IDO clusters per event", "DFidoclustersperevent",
                    &(m_srATCASNamed->DF_ido_clusters_per_evt_sr_v),  &(m_srATCASNamed->DF_ido_clusters_per_evt_sr_v_info),
                    0x3B, 0x3C,
                    srType::srOther, srAccess::IPBus_selector);
    setupCollection('F',0,0x00002200,0x00002218,1,
                    "DF ILI clusters per event", "DFiliclustersperevent",
                    &(m_srATCASNamed->DF_ili_clusters_per_evt_sr_v),  &(m_srATCASNamed->DF_ili_clusters_per_evt_sr_v_info),
                    0x3B, 0x3C,
                    srType::srOther, srAccess::IPBus_selector);
    setupCollection('F',0,0x00002300,0x00002324,1,
                    "DF ODO clusters per event", "DFodoclustersperevent",
                    &(m_srATCASNamed->DF_odo_clusters_per_evt_sr_v),  &(m_srATCASNamed->DF_odo_clusters_per_evt_sr_v_info),
                    0x3B, 0x3C,
                    srType::srOther, srAccess::IPBus_selector);
    setupCollection('F',0,0x00002400,0x00002418,1,
                    "DF ILO clusters per event", "DFiloclustersperevent",
                    &(m_srATCASNamed->DF_ilo_clusters_per_evt_sr_v),  &(m_srATCASNamed->DF_ilo_clusters_per_evt_sr_v_info),
                    0x3B, 0x3C,
                    srType::srOther, srAccess::IPBus_selector);

    setupCollection('F',0,0x00003000,0x00003024,1,
                    "DF b0f-b0f/b0f-e0f", "DFb0f2b0foverb0f2e0f",
                    &(m_srATCASNamed->DF_b0f_b0f_over_b0f_e0f_sr_v),  &(m_srATCASNamed->DF_b0f_b0f_over_b0f_e0f_sr_v_info),
                    0x3B, 0x3C,
                    srType::srOther, srAccess::IPBus_selector);
    
    
  }

  /*
  if ( m_df_fwVersion >= 0x19008 ) { // added InputHandler monitoring histograms in 0x19008
    setupCollection('F', 0, 0x2A, 0x2B, 1,
		    "DF skew per packet", "DFskewperpacket",
		    &(m_srATCASNamed->DF_input_monitoring_skew_sr_v),&(m_srATCASNamed->DF_input_monitoring_skew_sr_v_info),
		    0x69, 0x2A,
		    srType::srOther, srAccess::IPBus_selector);
    setupCollection('F', 0, 0, 16, 1,
		    "DF PacketHandler timeout (full)", "DFpackethandlertimeoutfull",
		    &(m_srATCASNamed->DF_input_monitoring_PH_timeout_full_sr_v),&(m_srATCASNamed->DF_input_monitoring_PH_timeout_full_sr_v_info),
		    0x69, 0x2B,
		    srType::srOther, srAccess::IPBus_selector);
    setupCollection('F', 0, 0x2C, 0x2D, 1,
		    "DF global l1id skip", "DFgloball1idskip",
		    &(m_srATCASNamed->DF_input_monitoring_global_L1skip_sr_v),&(m_srATCASNamed->DF_input_monitoring_global_L1skip_sr_v_info),
		    0x69, 0x2C,
		    srType::srOther, srAccess::IPBus_selector);
    setupCollection('F', 0, 0, 16, 1,
		    "DF lane l1id skip (full)", "DFlanel1idskipfull",
		    &(m_srATCASNamed->DF_input_monitoring_lane_L1skip_full_sr_v),&(m_srATCASNamed->DF_input_monitoring_lane_L1skip_full_sr_v_info),
		    0x69, 0x2D,
		    srType::srOther, srAccess::IPBus_selector);
  }
  */
 } else {
  setupCollection('F',0,0x0F,0x10,1,
		  "DF input xoff","DFinxoff",
		  &(m_srATCASNamed->DF_inxoff_sr_v),&(m_srATCASNamed->DF_inxoff_sr_v_info),
		  0,0,
		  srType::srOther, srAccess::IPBus_direct);
  setupCollection('F',0,0x1D,0x1E,1,
		  "DF input enabled","DFinenable",
		     &(m_srATCASNamed->DF_fmcenable_sr_v),&(m_srATCASNamed->DF_fmcenable_sr_v_info),
		  0,0,
		  srType::srOther, srAccess::IPBus_direct);
  setupCollection('F',0,0x0E,0x10,1,
		  "DF fifo Input busy","DFfifoinbusy",
		  &(m_srATCASNamed->DF_inbusy_sr_v),&(m_srATCASNamed->DF_inbusy_sr_v_info),
		  0,0,
		  srType::srOther, srAccess::IPBus_direct);
  setupCollection('F',0,0x5A,0x5B,1,
		  "DF l1id","DFl1id",
		  &(m_srATCASNamed->DF_l1id_sr_v),&(m_srATCASNamed->DF_l1id_sr_v_info),
		     0,0,
		  srType::srOther, srAccess::IPBus_direct);
 }
 
/*	setupCollection('F',0,0x4F,0x10,1,
				"DF fifo output busy","DFoutbusy",
				&(m_srATCASNamed->DF_outbusy_sr_v),&(m_srATCASNamed->DF_outbusy_sr_v_info),
				0,0,
				srType::srOther, srAccess::IPBus_direct);

	setupCollection('F',0,0x0F,0x10,1,
				"DF fifo input empty","DFinempty",
				&(m_srATCASNamed->DF_inempty_sr_v),&(m_srATCASNamed->DF_inempty_sr_v_info),
				0,0,
				srType::srOther, srAccess::IPBus_direct);
	
	setupCollection('F',0,0x0F,0x10,1,
				"DF fifo output empty","DFoutempty",
				&(m_srATCASNamed->DF_outempty_sr_v),&(m_srATCASNamed->DF_outempty_sr_v_info),
				0,0,
				srType::srOther, srAccess::IPBus_direct);

  setupCollection('F',0,0x57,0x58,1,
        "DF input b0f skew","DFinb0fskew",
        &(m_srATCASNamed->DF_input_b0f_skew),&(m_srATCASNamed->DF_input_b0f_skew_info),
        0,0,
        srType::srOther, srAccess::IPBus_direct);
  setupCollection('F',0,0x58,0x59,1,
        "DF input b0f timed-out links","DFinb0ftimedout",
        &(m_srATCASNamed->DF_input_b0f_timeout_links),&(m_srATCASNamed->DF_input_b0f_timeout_links_info),
        0,0,
        srType::srOther, srAccess::IPBus_direct);
  setupCollection('F',0,0x59,0x5A,1,
				&(m_srATCASNamed->DF_inempty_sr_v),&(m_srATCASNamed->DF_inempty_sr_v_info),
				0,0,
				srType::srOther, srAccess::IPBus_direct);
	
	setupCollection('F',0,0x0F,0x10,1,
				"DF fifo output empty","DFoutempty",
				&(m_srATCASNamed->DF_outempty_sr_v),&(m_srATCASNamed->DF_outempty_sr_v_info),
				0,0,
				srType::srOther, srAccess::IPBus_direct);

  setupCollection('F',0,0x57,0x58,1,
        "DF input b0f skew","DFinb0fskew",
        &(m_srATCASNamed->DF_input_b0f_skew),&(m_srATCASNamed->DF_input_b0f_skew_info),
        0,0,
        srType::srOther, srAccess::IPBus_direct);
  setupCollection('F',0,0x58,0x59,1,
        "DF input b0f timed-out links","DFinb0ftimedout",
        &(m_srATCASNamed->DF_input_b0f_timeout_links),&(m_srATCASNamed->DF_input_b0f_timeout_links_info),
        0,0,
        srType::srOther, srAccess::IPBus_direct);
  setupCollection('F',0,0x59,0x5A,1,
				0,0,
				srType::srOther, srAccess::IPBus_direct);

	setupCollection('F',0,0x0F,0x10,1,
				"DF fifo input empty","DFinempty",
				&(m_srATCASNamed->DF_inempty_sr_v),&(m_srATCASNamed->DF_inempty_sr_v_info),
				0,0,
				srType::srOther, srAccess::IPBus_direct);
	
	setupCollection('F',0,0x0F,0x10,1,
				"DF fifo output empty","DFoutempty",
				&(m_srATCASNamed->DF_outempty_sr_v),&(m_srATCASNamed->DF_outempty_sr_v_info),
				0,0,
				srType::srOther, srAccess::IPBus_direct);

  setupCollection('F',0,0x57,0x58,1,
        "DF input b0f skew","DFinb0fskew",
        &(m_srATCASNamed->DF_input_b0f_skew),&(m_srATCASNamed->DF_input_b0f_skew_info),
        0,0,
        srType::srOther, srAccess::IPBus_direct);
  setupCollection('F',0,0x58,0x59,1,
        "DF input b0f timed-out links","DFinb0ftimedout",
        &(m_srATCASNamed->DF_input_b0f_timeout_links),&(m_srATCASNamed->DF_input_b0f_timeout_links_info),
        0,0,
        srType::srOther, srAccess::IPBus_direct);
  setupCollection('F',0,0x59,0x5A,1,
        "DF l1id out of sync","DFl1idoutofsync",
        &(m_srATCASNamed->DF_l1id_out_of_sync),&(m_srATCASNamed->DF_l1id_out_of_sync_info),
        0,0,
        srType::srOther, srAccess::IPBus_direct);*/
    //--------------------------------------------------------------------------------------------------------------
 setupCollection('P',  0, 0x00080000, 0x00080100, 1, "StatusRegisterIM link0 FPGA 0", "IM0",  &(m_srATCASNamed->IM0_sr_v),  &(m_srATCASNamed->IM0_sr_v_info),  0, 0, srType::srOther, srAccess::i2cIPBus);
 setupCollection('S',  1, 0x000c0000, 0x000c0100, 1, "StatusRegisterIM link1 FPGA 0", "IM1",  &(m_srATCASNamed->IM1_sr_v),  &(m_srATCASNamed->IM1_sr_v_info),  0, 0, srType::srOther, srAccess::i2cIPBus);
 setupCollection('P',  2, 0x00080000, 0x00080100, 1, "StatusRegisterIM link0 FPGA 1", "IM2",  &(m_srATCASNamed->IM2_sr_v),  &(m_srATCASNamed->IM2_sr_v_info),  0, 0, srType::srOther, srAccess::i2cIPBus);
 setupCollection('S',  3, 0x000c0000, 0x000c0100, 1, "StatusRegisterIM link1 FPGA 1", "IM3",  &(m_srATCASNamed->IM3_sr_v),  &(m_srATCASNamed->IM3_sr_v_info),  0, 0, srType::srOther, srAccess::i2cIPBus);
 setupCollection('P',  4, 0x00080000, 0x00080100, 1, "StatusRegisterIM link0 FPGA 2", "IM4",  &(m_srATCASNamed->IM4_sr_v),  &(m_srATCASNamed->IM4_sr_v_info),  0, 0, srType::srOther, srAccess::i2cIPBus);
 setupCollection('S',  5, 0x000c0000, 0x000c0100, 1, "StatusRegisterIM link1 FPGA 2", "IM5",  &(m_srATCASNamed->IM5_sr_v),  &(m_srATCASNamed->IM5_sr_v_info),  0, 0, srType::srOther, srAccess::i2cIPBus);
 setupCollection('P',  6, 0x00080000, 0x00080100, 1, "StatusRegisterIM link0 FPGA 3", "IM6",  &(m_srATCASNamed->IM6_sr_v),  &(m_srATCASNamed->IM6_sr_v_info),  0, 0, srType::srOther, srAccess::i2cIPBus);
 setupCollection('S',  7, 0x000c0000, 0x000c0100, 1, "StatusRegisterIM link1 FPGA 3", "IM7",  &(m_srATCASNamed->IM7_sr_v),  &(m_srATCASNamed->IM7_sr_v_info),  0, 0, srType::srOther, srAccess::i2cIPBus);
 setupCollection('P',  8, 0x00080000, 0x00080100, 1, "StatusRegisterIM link0 FPGA 4", "IM8",  &(m_srATCASNamed->IM8_sr_v),  &(m_srATCASNamed->IM8_sr_v_info),  0, 0, srType::srOther, srAccess::i2cIPBus);
 setupCollection('S',  9, 0x000c0000, 0x000c0100, 1, "StatusRegisterIM link1 FPGA 4", "IM9",  &(m_srATCASNamed->IM9_sr_v),  &(m_srATCASNamed->IM9_sr_v_info),  0, 0, srType::srOther, srAccess::i2cIPBus);
 setupCollection('P', 10, 0x00080000, 0x00080100, 1, "StatusRegisterIM link0 FPGA 5", "IM10", &(m_srATCASNamed->IM10_sr_v), &(m_srATCASNamed->IM10_sr_v_info), 0, 0, srType::srOther, srAccess::i2cIPBus);
 setupCollection('S', 11, 0x000c0000, 0x000c0100, 1, "StatusRegisterIM link1 FPGA 5", "IM11", &(m_srATCASNamed->IM11_sr_v), &(m_srATCASNamed->IM11_sr_v_info), 0, 0, srType::srOther, srAccess::i2cIPBus);
 setupCollection('P', 12, 0x00080000, 0x00080100, 1, "StatusRegisterIM link0 FPGA 6", "IM12", &(m_srATCASNamed->IM12_sr_v), &(m_srATCASNamed->IM12_sr_v_info), 0, 0, srType::srOther, srAccess::i2cIPBus);
 setupCollection('S', 13, 0x000c0000, 0x000c0100, 1, "StatusRegisterIM link1 FPGA 6", "IM13", &(m_srATCASNamed->IM13_sr_v), &(m_srATCASNamed->IM13_sr_v_info), 0, 0, srType::srOther, srAccess::i2cIPBus);
 setupCollection('P', 14, 0x00080000, 0x00080100, 1, "StatusRegisterIM link0 FPGA 7", "IM14", &(m_srATCASNamed->IM14_sr_v), &(m_srATCASNamed->IM14_sr_v_info), 0, 0, srType::srOther, srAccess::i2cIPBus);
 setupCollection('S', 15, 0x000c0000, 0x000c0100, 1, "StatusRegisterIM link1 FPGA 7", "IM15", &(m_srATCASNamed->IM15_sr_v), &(m_srATCASNamed->IM15_sr_v_info), 0, 0, srType::srOther, srAccess::i2cIPBus);
 
 setupCollection('P',  0, 0x00000000, 0x00000004, 1, "StatusRegisterIM FWVer FPGA 0", "IM_FWver_FPGA0",  &(m_srATCASNamed->IM_fw_ver_FPGA0_sr_v),  &(m_srATCASNamed->IM_fw_ver_FPGA0_sr_v_info),  0, 0, srType::srOther, srAccess::i2cIPBus );
 setupCollection('S',  1, 0x00000000, 0x00000004, 1, "StatusRegisterIM FWVer FPGA 1", "IM_FWver_FPGA1",  &(m_srATCASNamed->IM_fw_ver_FPGA1_sr_v),  &(m_srATCASNamed->IM_fw_ver_FPGA1_sr_v_info),  0, 0, srType::srOther, srAccess::i2cIPBus );
 setupCollection('P',  2, 0x00000000, 0x00000004, 1, "StatusRegisterIM FWVer FPGA 2", "IM_FWver_FPGA2",  &(m_srATCASNamed->IM_fw_ver_FPGA2_sr_v),  &(m_srATCASNamed->IM_fw_ver_FPGA2_sr_v_info),  0, 0, srType::srOther, srAccess::i2cIPBus );
 setupCollection('S',  3, 0x00000000, 0x00000004, 1, "StatusRegisterIM FWVer FPGA 3", "IM_FWver_FPGA3",  &(m_srATCASNamed->IM_fw_ver_FPGA3_sr_v),  &(m_srATCASNamed->IM_fw_ver_FPGA3_sr_v_info),  0, 0, srType::srOther, srAccess::i2cIPBus );
 setupCollection('P',  4, 0x00000000, 0x00000004, 1, "StatusRegisterIM FWVer FPGA 4", "IM_FWver_FPGA4",  &(m_srATCASNamed->IM_fw_ver_FPGA4_sr_v),  &(m_srATCASNamed->IM_fw_ver_FPGA4_sr_v_info),  0, 0, srType::srOther, srAccess::i2cIPBus );
 setupCollection('S',  5, 0x00000000, 0x00000004, 1, "StatusRegisterIM FWVer FPGA 5", "IM_FWver_FPGA5",  &(m_srATCASNamed->IM_fw_ver_FPGA5_sr_v),  &(m_srATCASNamed->IM_fw_ver_FPGA5_sr_v_info),  0, 0, srType::srOther, srAccess::i2cIPBus );
 setupCollection('P',  6, 0x00000000, 0x00000004, 1, "StatusRegisterIM FWVer FPGA 6", "IM_FWver_FPGA6",  &(m_srATCASNamed->IM_fw_ver_FPGA6_sr_v),  &(m_srATCASNamed->IM_fw_ver_FPGA6_sr_v_info),  0, 0, srType::srOther, srAccess::i2cIPBus );
 setupCollection('S',  7, 0x00000000, 0x00000004, 1, "StatusRegisterIM FWVer FPGA 7", "IM_FWver_FPGA7",  &(m_srATCASNamed->IM_fw_ver_FPGA7_sr_v),  &(m_srATCASNamed->IM_fw_ver_FPGA7_sr_v_info),  0, 0, srType::srOther, srAccess::i2cIPBus );
 
 m_srxNamed = std::move(m_srATCASNamed);
}

StatusRegisterATCAFactory::~StatusRegisterATCAFactory() {
	// delete any objects on top of the base StatusRegisterFactory
}

  void StatusRegisterATCAFactory::freeze(srAccess access, uint32_t fpgaNum, bool opt) {
	if(access == srAccess::i2cIPBus) 
	{
      m_dfApi->getIMApi()->send_freeze(fpgaNum, opt);
	}
}

// Protected Member Functions
void StatusRegisterATCAFactory::setupCollection(
                                                char IDChar,
                                                uint fpgaNum,
                                                uint firstAddress, 
                                                uint finalAddress,
                                                uint addrIncrement,
                                                std::string collectionName, 
                                                std::string collectionShortName,
                                                
                                                std::vector<uint>* ISObjectVector,
                                                std::vector<uint>* ISObjectInfoVector,
                                                uint selectorAddress,
                                                uint readerAddress,
                                                srType type,
                                                srAccess access
                                                ) 
{
  // Checks if the input string contains the identifying character for the collection
  if (std::string::npos != m_inputString.find_first_of(toupper(IDChar))) {
    std::unique_ptr<StatusRegisterCollection> src = std::make_unique<StatusRegisterCollection>(type, access, access == srAccess::i2cIPBus ? m_useBlockTransfer : 0);
    
    src->setName(collectionName);
    src->setNameShort(collectionShortName);
    for ( uint i = firstAddress; i < finalAddress; i+=addrIncrement ) {
      
      if ( type == srType::srOther && access == srAccess::IPBus_selector ) {
        //DF SelectorGeneric
        src->addStatusRegister(std::make_unique<StatusRegisterDFSelectorGeneric>(m_dfApi, i, selectorAddress, readerAddress));
        // DF AccessHistos
        // src->addStatusRegister(std::make_unique<StatusRegisterDFAccessHistos>(m_dfApi, i, selectorAddress, readerAddress, type));
      } else if(type == srType::counterOther && access == srAccess::IPBus_selector) {
        // DF Counters
        src->addStatusRegister(std::make_unique<StatusRegisterDFCounter>(m_dfApi, type, i));
      } else if(type == srType::gtOther && access == srAccess::IPBus_selector) {
        // GT Status
        src->addStatusRegister(std::make_unique<StatusRegisterDFGT>     (m_dfApi, type, i, readerAddress));
      } else if(type == srType::srOther && access == srAccess::IPBus_direct) {
        // DF Direct
        src->addStatusRegister(std::make_unique<StatusRegisterDFDirect>(m_dfApi, i, type));
      } else if(type == srType::fifoOther && access == srAccess::IPBus_selector) {
        // DF SwitchFifo
        src->addStatusRegister(std::make_unique<StatusRegisterDFSwitchFifo>(m_dfApi, type, readerAddress));
      } else if(type == srType::srOther && access == srAccess::i2cIPBus) {
        // IM Direct
        src->addStatusRegister(std::make_unique<StatusRegisterIMDirect>(m_dfApi->getIMApi(), fpgaNum, i, type ));
      }
    }//end loop over registers
    m_collections.push_back(std::move(src));
    m_isObjectVectors.push_back(ISObjectVector);
    if(ISObjectInfoVector){
      setupCollectionInfo(ISObjectInfoVector, firstAddress, finalAddress, addrIncrement, fpgaNum, selectorAddress, readerAddress);
    }
  }
}
  
} // namespace ftk
} // namespace daq
