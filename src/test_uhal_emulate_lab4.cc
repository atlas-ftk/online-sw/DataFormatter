#include "uhal/tests/UDPDummyHardware.hpp"
#include "DataFormatter/FtkDFuhalEmulator.h"
#include "uhal/log/log.hpp"
#include <iostream>

using namespace uhal;
using namespace uhal::tests;

int main ( int argc, char* argv[] )
{

//  UDPDummyHardware<2,0> lDummyHardware ( 50001, 0x0, false );
  FtkDFuhalEmulator lDummyHardware ( 50001, // port
                                     0, // replyDelay
                                     false, // bigEndianHack
                                     "file:///tbed/user/rmina/tv2017_10kHz/connections.xml", // IPbus_connection_file
                                     "DF22", // IPbus_device_id
                                     3, // shelf
                                     9, // slot
                                     false, // doFWcheck
                                     98326, // df_fwVersion
                                     16525, // imS6_fwVersion
                                     16452  // imA7_fwVersion
                                        );

  uhal::setLogLevelTo (uhal::Info());

  lDummyHardware.run();

  std::cout << "test_uhal_emulate_lab4 exiting." << std::endl;

  return 0;

}
