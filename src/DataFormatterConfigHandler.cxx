#include "DataFormatter/DataFormatterConfigHandler.h"

using namespace daq;
using namespace ftk;
using namespace dfconfig;

DataFormatterConfigHandler::DataFormatterConfigHandler( const std::string& name,
                                                        uint32_t shelf,
                                                        uint32_t slot,
                                                        uint32_t useBlockTransfer,
                                                        uint32_t useAutoDelaySetting,
                                                        bool doConfigOTF,
                                                        bool IMConfigOTF,
                                                        bool dumpConfig,
                                                        bool usePseudoData,
                                                        const std::string& standardConfigFile,
                                                        const std::string& globalCF,
                                                        const std::string& systemCF,
                                                        const std::string& boardDelayFile,
                                                        const std::string& coolConnString,
                                                        const std::string& moduleListFile,
                                                        const std::string& multishelfCF,
                                                        const std::string& transceiverConfigFile,
                                                        const std::string& configDumpFile,
                                                        const std::string& IMLUTPrefix,
                                                        const std::string& IMLUTsFile,
                                                        const std::string& IMPseudoDataPrefix,
                                                        const std::string& IMPseudoDataFile,
                                                        uint32_t IMPseudoData_InputRate,
                                                        uint32_t maxCyclesWaitedForData,
                                                        uint32_t b0fTimeoutThreshold,
                                                        uint32_t skewTimeoutThresholdMax,
                                                        uint32_t skewTimeoutThresholdMin,
                                                        uint32_t packetTimeoutThreshold,
                                                        uint32_t DF_IM_freezeMode,
                                                        uint32_t DF_enableInputResync,
                                                        uint32_t DF_unknownModHeaderWord,
                                                        uint32_t DF_modWordLimit,
                                                        uint32_t DF_boardId,
                                                        uint32_t slinkMode,
							uint32_t numGoodDelayValues,
                                                        std::shared_ptr<FtkDataFormatterApi> dfApi )
  : m_name( name ),
    m_shelf( shelf ),
    m_slot( slot ),
    m_useBlockTransfer( useBlockTransfer ),
    m_useAutoDelaySetting( useAutoDelaySetting ),
    m_doConfigOTF( doConfigOTF ),
    m_IMConfigOTF( IMConfigOTF ),
    m_dumpConfig( dumpConfig ),
    m_usePseudoData( usePseudoData ),
    m_standardConfigFile( standardConfigFile ),
    m_globalCF( globalCF ),
    m_systemCF( systemCF ),
    m_boardDelayFile( boardDelayFile ),
    m_coolConnString( coolConnString ),
    m_moduleListFile( moduleListFile ),
    m_multishelfCF( multishelfCF ),
    m_transceiverCF( transceiverConfigFile ),
    m_configDumpFile( configDumpFile ),
    m_IMLUTPrefix( IMLUTPrefix ),
    m_IMLUTsFile( IMLUTsFile ),
    m_IMPseudoDataPrefix( IMPseudoDataPrefix ),
    m_IMPseudoDataFile( IMPseudoDataFile ),
    m_IMPseudoData_InputRate( IMPseudoData_InputRate ),
    m_maxCyclesWaitedForData( maxCyclesWaitedForData ),
    m_b0fTimeoutThreshold( b0fTimeoutThreshold ),
    m_skewTimeoutThresholdMax( skewTimeoutThresholdMax ),
    m_skewTimeoutThresholdMin( skewTimeoutThresholdMin ),
    m_packetTimeoutThreshold( packetTimeoutThreshold ),
    m_DF_IM_freezeMode( DF_IM_freezeMode ),
    m_DF_enableInputResync( DF_enableInputResync ),
    m_DF_unknownModHeaderWord( DF_unknownModHeaderWord ),
    m_DF_modWordLimit( DF_modWordLimit ),
    m_DF_boardId( DF_boardId ),
    m_slinkMode( slinkMode ),
    m_numGoodDelayValues ( numGoodDelayValues ),
    m_dfApi( dfApi ),
    m_boardNumber( FtkDataFormatterApi::getBoardNumberFromShelfSlot( shelf, slot ) ),
    m_enableFMCLanesMask( 0x0 )
{
  ERS_LOG( "Initialized." );

  m_globalConfigParseBegin = 0;
  m_globalConfigParseComplete = 0;
  m_systemConfigParseBegin = 0;
  m_systemConfigParseComplete = 0;
  m_modulelistConfigParseBegin = 0;
  m_modulelistConfigParseComplete = 0;
}

// Return true if the configurations agree, false if there is a discrepancy
bool DataFormatterConfigHandler::checkRCAgainstConfig( uint32_t DF_enabled_RC, const std::string& globalConfigFile )
{
  bool discrepancy_found = false;
  uint32_t DF_enabled_file = 0x0;

  string line;
  ifstream conf_file ( globalConfigFile.c_str() );
  if ( conf_file.is_open() ) {
    while ( getline (conf_file,line ) ) { 
      if ( line.find("!") != string::npos ) continue;

      if ( line.substr(0,11) == "boardEnable" ) {
        stringstream ss(line);
        string buf, key;
        uint32_t board, en;
        while ( ss >> key >> std::setbase(0) >> board >> en ) {
          if ( en ) DF_enabled_file |= (0x1<<board);
        }
      }
    }
  } else { // couldn't open file
    IOError theIssue( ERS_HERE, " could not open global board config file ", globalConfigFile );
    throw( theIssue );
  }

  const string DF_NAMES[ NUMBER_OF_DF_BOARDS  ] = { "DF-1-03",
                                 "DF-2-03",
                                 "DF-3-03",
                                 "DF-4-03",
                                 "DF-1-04",
                                 "DF-2-04",
                                 "DF-3-04",
                                 "DF-4-04",
                                 "DF-1-05",
                                 "DF-2-05",
                                 "DF-3-05",
                                 "DF-4-05",
                                 "DF-1-06",
                                 "DF-2-06",
                                 "DF-3-06",
                                 "DF-4-06",
                                 "DF-1-07",
                                 "DF-2-07",
                                 "DF-3-07",
                                 "DF-4-07",
                                 "DF-1-08",
                                 "DF-2-08",
                                 "DF-3-08",
                                 "DF-4-08",
                                 "DF-1-09",
                                 "DF-2-09",
                                 "DF-3-09",
                                 "DF-4-09",
                                 "DF-1-10",
                                 "DF-2-10",
                                 "DF-3-10",
                                 "DF-4-10" };
  for ( uint32_t i_df = 0; i_df < NUMBER_OF_DF_BOARDS; ++i_df ) {
    if ( (0x1<<i_df) & DF_enabled_RC & ~DF_enabled_file ) {
      ers::warning( ftkException( ERS_HERE, name_ftk(), DF_NAMES[i_df] + " enabled in RC but disabled in config file " + globalConfigFile ) );
      discrepancy_found = true;
    }
    if ( (0x1<<i_df) & ~DF_enabled_RC & DF_enabled_file ) {
      ers::warning( ftkException( ERS_HERE, name_ftk(), DF_NAMES[i_df] + " disabled in RC but enabled in config file " + globalConfigFile ) );
      discrepancy_found = true;
    }
  }

  if ( discrepancy_found ) {
    ftkException theIssue( ERS_HERE, name_ftk(), "Found discrepancy between RC and config file (see above). Aborting." );
    throw( theIssue );
    return false;
  } else {
    return true;
  }
}

bool DataFormatterConfigHandler::configure() {
  bool status = true;

  ERS_INFO( m_name << " Begin IMDF configuration procedure." );

  try {
    try {
      m_dfApi->setTxRxLine( 0x0, 0x0 );
      m_dfApi->setTxRxLineBert( 0x0, 0x0 );
    } catch ( std::exception ex ) {
      ERS_LOG("Caught exception while resetting GTX. Ignoring. ex.what() = " << ex.what());
    }

    if ( m_doConfigOTF ) {
      status = status && buildConfiguration();
      status = status && standardOTFConfiguration();
    } else {
      status = status && standardConfiguration();
    }
  } catch ( uhal::exception::FileNotFound& ex ) {
    ERS_LOG( "Caught and ignored: " << ex.what() );
  }

  if ( m_dumpConfig ) {
    dumpConfiguration();
  }

  // download IM LUT
  try {
    if ( m_IMConfigOTF ) {
      status = status && downloadAllIMLUT_OTF();
    } else {
      status = status && downloadAllIMLUT();
    }
  } catch ( daq::ftk::IOError ex ){
    throw ( daq::ftk::IOError ( ERS_HERE, m_name, " download all IM LUTs failed", ex) );
  }

  // download pseudodata
  if ( m_usePseudoData ) {
    try{
      if ( m_IMConfigOTF ){
	status = status && downloadAllPseudoData_OTF();
      } else {
	status = status && downloadAllPseudoData();
      }
    } catch ( daq::ftk::IOError ex ) {
      throw ( daq::ftk::IOError ( ERS_HERE, m_name, " download all PseudoData failed", ex) );
    }
  }

  status = status && checkIMDFLanes();
  status = status && checkIMDFModules();

  if ( status ) {
    ERS_INFO( m_name << " Configuration complete." );
  } else {
    throw ( daq::ftk::ftkException( ERS_HERE, m_name, " the configure operation did not succeed." ) );
  }

  return status;
}

bool DataFormatterConfigHandler::unconfigure() {
  ERS_LOG( "Unconfiguring." );

  m_globalConfigParseBegin = 0;
  m_globalConfigParseComplete = 0;
  m_systemConfigParseBegin = 0;
  m_systemConfigParseComplete = 0;
  m_modulelistConfigParseBegin = 0;
  m_modulelistConfigParseComplete = 0;

  return true;
}

bool DataFormatterConfigHandler::quickReset() {
  ERS_LOG( "Begin quick reset." );
  bool status = true;

  status = status && standardResetConfiguration( true );
  status = status && standardResetConfiguration( true );
  status = status && standardResetConfiguration( true );
  status = status && standardResetConfiguration( true );
  status = status && clockPhaseConfiguration( true );
  status = status && m_dfApi->setLinkURL( m_shelf, m_slot );
  m_dfApi->setTxRxLine( 0x00FFFFFF, get_rxports() );

  ERS_LOG( "End quick reset." );

  return status;
}

bool DataFormatterConfigHandler::getRobIdForIMLane( const uint32_t& lane, uint32_t& robId ) {
  if ( m_FMCIn2RODId_map.find( lane ) == m_FMCIn2RODId_map.end() ) {
    ers::warning( daq::ftk::WrongParameters( ERS_HERE, m_name , " requested ROB ID for lane " + std::to_string(lane) + ", but no input is configured for that lane." ) );
    return false;
  }
  robId = m_FMCIn2RODId_map.at( lane );
  return true;
}

// -------------------------------
// ----------- Private -----------
// -------------------------------

bool DataFormatterConfigHandler::standardConfiguration() {
  ERS_INFO( m_name << " Begin standard configuration using file " << m_standardConfigFile );
  bool status = true;

  //uint32_t m_thisBoardMask = 0x0;
  //uint32_t m_thisBoardMaskILI = 0xFFFF;
  //bool useDifferentThisBoardMaskForILI = false;
  m_laneToDelayMap.clear();
  m_laneToInvMap.clear();
  m_FMCIn2NumModules.clear();
  m_pixmod2dst_map.clear();
  m_pixmod2ftkplane_map.clear();
  m_pixmod2tower_map.clear();
  m_sctmod2dst_map.clear();
  m_sctmod2ftkplane_map.clear();
  m_sctmod2tower_map.clear();
  m_SLINKOut2NumModules.clear();
  m_centralSwLaneId2DestMask.clear();
  m_lane_mod2idx.clear();
  m_lane_idx2mod.clear();
  m_rxports = 0x0;

  std::ifstream conf_file( m_standardConfigFile.c_str() );

  if ( !conf_file.is_open() ) {
    throw ( daq::ftk::IOError( ERS_HERE, m_name, " cannot open configuration file " + m_standardConfigFile ) );
    return false;
  } else {
    std::string line;
    std::pair<uint32_t, uint32_t> tmp_pair;

    while ( std::getline( conf_file, line ) ) {
      std::stringstream ss( line );
      std::string c1, c2, c3, c4;
      ss >> c1 >> c2 >> c3 >> c4;

      c1 = c1.substr(0, c1.find("#"));

      tmp_pair = std::make_pair( strtoul(c2.c_str(), NULL, 0), strtoul(c3.c_str(), NULL, 0) );

      if ( c1 == "this_board_mask" ) {
        //m_thisBoardMask = strtoul( c2.c_str(), NULL, 0 );
      } else if ( c1 == "this_board_mask_internal_link_input" ) {
        //useDifferentThisBoardMaskForInternalLinkInput = true;
        //m_thisBoardMaskILI = strtoul( c2.c_str(), NULL, 0 );
      } else if ( c1 == "imfpga2clkinv" ) {
        m_laneToInvMap.insert( tmp_pair );
      } else if ( c1 == "imfpga2clkdelay" ) {
        m_laneToDelayMap.insert( tmp_pair );
      } else if ( c1 == "fmcin2nummodules" ) {
        m_FMCIn2NumModules.insert( tmp_pair );
      } else if ( c1 == "lane_mod2idx" ) {
        m_lane_mod2idx.insert( std::make_pair( tmp_pair, strtoul( c4.c_str(), NULL, 0 ) ) );
      } else if ( c1 == "lane_idx2mod" ) {
        m_lane_idx2mod.insert( std::make_pair( tmp_pair, strtoul( c4.c_str(), NULL, 0 ) ) );
      } else if ( c1 == "pixmod2dst" ) {
        m_pixmod2dst_map.insert( tmp_pair );
      } else if ( c1 == "pixmod2ftkplane" ) {
        m_pixmod2ftkplane_map.insert( tmp_pair );
      } else if ( c1 == "pixmod2tower" ) {
        m_pixmod2tower_map.insert( tmp_pair );
      } else if ( c1 == "sctmod2dst" ) {
        m_sctmod2dst_map.insert( tmp_pair );
      } else if ( c1 == "sctmod2ftkplane" ) {
        m_sctmod2ftkplane_map.insert( tmp_pair );
      } else if ( c1 == "sctmod2tower" ) {
        m_sctmod2tower_map.insert( tmp_pair );
      } else if ( c1 == "slinkout2nummodules" ) {
        m_SLINKOut2NumModules.insert( tmp_pair );
      } else if ( c1 == "centralswlaneid2destinationmask" ) {
        m_centralSwLaneId2DestMask.insert( tmp_pair );
      } else if ( c1 == "rxports" ) {
        m_rxports = strtoul( c2.c_str(), NULL, 0 );
      }
    }
  }

  conf_file.close();

  m_clockPhaseDelayValues.clear();
  for ( uint32_t iFPGA = 0; iFPGA < NUMBER_OF_IM_FPGAS_PER_DF; ++iFPGA ) {
    if ( m_laneToDelayMap.find( iFPGA ) == m_laneToDelayMap.end() )
      m_clockPhaseDelayValues.push_back( 0 );
    else
      m_clockPhaseDelayValues.push_back( m_laneToDelayMap.at( iFPGA ) );
  }

  status = status && parseTransceiverParameters( m_gtCh2RxPolarity,
                                                 m_gtCh2TxPolarity,
                                                 m_gtCh2ForceReadyMode,
                                                 m_gtCh2ToAlteraFPGA,
                                                 m_gtCh2IgnoreFreeze );

  status = status && standardResetConfiguration( false );
  status = status && clockPhaseConfiguration( false );
  status = status && m_dfApi->setLinkURL( m_shelf, m_slot );
  m_dfApi->setTxRxLine( 0x00FFFFFF, 0x0 );

  ERS_INFO( m_name << " Finished standard configuration." );
  return status;
}

bool DataFormatterConfigHandler::standardOTFConfiguration() {
  ERS_INFO( m_name << " Begin standard on-the-fly configuration." );
  bool status = true;

  m_dfApi->setTxRxLine( 0x0, 0x0 );
  status = status && standardResetConfiguration( false );
  status = status && standardResetConfiguration( false );
  status = status && standardResetConfiguration( false );
  status = status && standardResetConfiguration( false );
  status = status && clockPhaseConfiguration( false );
  status = status && m_dfApi->setLinkURL( m_shelf, m_slot );
  m_dfApi->setTxRxLine( 0x00FFFFFF, 0x0 );

  ERS_INFO( m_name << " Finished standard configuration." );

  return status;
}

bool DataFormatterConfigHandler::dumpConfiguration() {
  bool status = true;

  ERS_LOG( "Dumping configuration to " << m_configDumpFile );

  std::ofstream conf_file( m_configDumpFile );

  // dump board mask
  char buf[BUFSIZ];
  snprintf(buf, sizeof(buf), "%-35s 0X%08x", "this_board_mask", (0X1<<m_boardNumber));
  conf_file << buf << std::endl;

  // dump phase delay values
  for ( uint32_t i_fpga=0; i_fpga < NUMBER_OF_IM_FPGAS_PER_DF; ++i_fpga ) {
    uint32_t inv = 0x0, delay = 0x0;
    if ( m_laneToInvMap.find( i_fpga ) != m_laneToInvMap.end() )
      inv = m_laneToInvMap.at( i_fpga );
    if ( m_laneToDelayMap.find( i_fpga ) != m_laneToDelayMap.end() ) 
      delay = m_laneToDelayMap.at( i_fpga );

    snprintf(buf, sizeof(buf), "%-35s %10u %10u", "imfpga2clkinv", i_fpga, inv);
    conf_file << buf << std::endl;
    snprintf(buf, sizeof(buf), "%-35s %10u %10u", "imfpga2clkdelay", i_fpga, delay);
    conf_file << buf << std::endl;
  }

  // dump number of modules, mod2idx, and idx2mod
  for ( const auto& item : m_rodIdToFMCInIdMap ) {
    const uint32_t& board = item.second.first;
    const uint32_t& robId = item.first;
    const uint32_t& lane  = item.second.second;

    if ( board != m_boardNumber ) continue; // only dump ROBs for this board

    bool isSCT = (( robId&0xF00000 ) == 0x200000 );

    if ( m_FMCIn2modid_map.find( lane ) == m_FMCIn2modid_map.end() ) continue;
    const std::vector<uint32_t>& moduleIds = m_FMCIn2modid_map.find(lane)->second;
    const uint32_t nMods = moduleIds.size();
    snprintf(buf, sizeof(buf), "%-35s %10u %10u", "fmcin2nummodules", lane, nMods);
    conf_file << buf << std::endl;

    for ( uint32_t iMod=0; iMod<nMods; ++iMod ) {
      const uint32_t& modId = (isSCT) ? ( moduleIds.at(iMod)|0x2000) : moduleIds.at(iMod);
      snprintf(buf, sizeof(buf), "%-35s %10u 0X%08x %10u", "lane_mod2idx", lane, modId, iMod);
      conf_file << buf << std::endl;
      snprintf(buf, sizeof(buf), "%-35s %10u %10u %10u", "lane_idx2mod", lane, iMod, modId);
      conf_file << buf << std::endl;
    }
  }

  // pixmod2dst
  for ( const auto& item : m_pixmod2dst_map ) {
    snprintf( buf, sizeof(buf), "%-35s 0X%04x 0X%08x", "pixmod2dst", item.first, item.second );
    conf_file << buf << std::endl;
  }
  // pixmod2ftkplane
  for ( const auto& item : m_pixmod2ftkplane_map ) {
    snprintf( buf, sizeof(buf), "%-35s 0X%04x 0X%04x", "pixmod2ftkplane", item.first, item.second );
    conf_file << buf << std::endl;
  }
  // pixmod2tower
  for ( const auto& item : m_pixmod2tower_map ) {
    snprintf( buf, sizeof(buf), "%-35s 0X%04x 0X%04x", "pixmod2tower", item.first, item.second );
    conf_file << buf << std::endl;
  }
  // sctmod2dst
  for ( const auto& item : m_sctmod2dst_map ) {
    snprintf( buf, sizeof(buf), "%-35s 0X%04x 0X%08x", "sctmod2dst", item.first, item.second );
    conf_file << buf << std::endl;
  }
  // sctmod2ftkplane
  for ( const auto& item : m_sctmod2ftkplane_map ) {
    snprintf( buf, sizeof(buf), "%-35s 0X%04x 0X%04x", "sctmod2ftkplane", item.first, item.second );
    conf_file << buf << std::endl;
  }
  // sctmod2tower
  for ( const auto& item : m_sctmod2tower_map ) {
    snprintf( buf, sizeof(buf), "%-35s 0X%04x 0X%04x", "sctmod2tower", item.first, item.second );
    conf_file << buf << std::endl;
  }

  // SLINKOut2NumModules
  for ( const auto& item : m_SLINKOut2NumModules ) {
    snprintf( buf, sizeof(buf), "%-35s %10u %10u", "slinkout2nummodules", item.first, item.second );
    conf_file << buf << std::endl;
  }

  snprintf( buf, sizeof(buf), "%-35s 0X%08x", "enable_fmc_lanes_mask", m_enableFMCLanesMask );
  conf_file << buf << std::endl;

  srand( m_boardNumber );

  for ( uint32_t i_dest=0; i_dest < NUMBER_OF_DF_BOARDS; ++i_dest ) {
    uint32_t mask_pattern = ( i_dest != m_boardNumber ) ?
                             getInternalLinkOutputBitMap( m_boardNumber, i_dest ) :
                             0x0;
    snprintf( buf, sizeof(buf), "%-35s %10u 0X%04x", "centralswlaneid2destinationamsk", i_dest, mask_pattern );
    conf_file << buf << std::endl;
  }

  conf_file << "rxports " << m_rxports << std::endl;

  conf_file.close();
  return status;
}

bool DataFormatterConfigHandler::standardResetConfiguration( bool fast ) {
  ERS_LOG( "Resetting the configuration." );
  bool status = true;

  status = status && m_dfApi->standardReset( fast, 0, 
                                              m_gtCh2RxPolarity,
                                              m_gtCh2TxPolarity,
                                              m_gtCh2ForceReadyMode,
                                              m_gtCh2ToAlteraFPGA,
                                              m_gtCh2IgnoreFreeze,
                                              m_multishelf_boardEnableMap,
                                              m_rxports );


  ERS_LOG( "Configure FMC enabled lanes << 0X" << std::hex << m_enableFMCLanesMask );
  status = status && m_dfApi->configureFMC( m_enableFMCLanesMask, (0x1<<m_boardNumber), (0x1<<m_boardNumber) );
  ERS_LOG( "Setting configuration registers." );
  status = status && m_dfApi->setConfigRegisters( m_maxCyclesWaitedForData,
                                                  m_b0fTimeoutThreshold,
                                                  m_skewTimeoutThresholdMax,
                                                  m_skewTimeoutThresholdMin,
                                                  m_packetTimeoutThreshold,
                                                  m_DF_IM_freezeMode,
                                                  m_DF_enableInputResync,
                                                  m_DF_unknownModHeaderWord,
                                                  m_DF_modWordLimit,
                                                  m_DF_boardId,
                                                  m_enableFMCLanesMask,
                                                  (0x1<<m_boardNumber),
                                                  (0x1<<m_boardNumber),
                                                  m_slinkMode,
                                                  m_useBlockTransfer );


  ERS_LOG( "Configure expected number of modules at inputs and outputs." );
  status = status && m_dfApi->configureExpectedNumModules( m_FMCIn2NumModules, m_SLINKOut2NumModules );
  ERS_LOG( "Configure DF LUTs" );
  status = status && lutConfiguration();
  ERS_LOG( "Configure internal link mask for DF central switch." );
  status = status && m_dfApi->internalLinkMaskConfig( m_centralSwLaneId2DestMask );
  status = status && m_dfApi->standardReset( fast, 1, 
                                             m_gtCh2RxPolarity,
                                             m_gtCh2TxPolarity,
                                             m_gtCh2ForceReadyMode,
                                             m_gtCh2ToAlteraFPGA,
                                             m_gtCh2IgnoreFreeze,
                                             m_multishelf_boardEnableMap,
                                             m_rxports );

  ERS_LOG( "Done resetting the configuration." );
  return status;
}

bool DataFormatterConfigHandler::clockPhaseConfiguration( bool fast  ) {
  ERS_LOG( "Setting IMDF clock delay values." );
  if ( fast ) ERS_LOG ( "...using previously computed values." );
  else if ( m_useAutoDelaySetting ) ERS_LOG( "Running the phase scan for all channels." );
  else ERS_LOG( "Using delay values loaded from the config file." );
  bool status = true;

  if ( m_useAutoDelaySetting ) return phaseScanAllChannels( fast );

  else m_dfApi->setClockPhaseConfiguration( m_clockPhaseDelayValues );

  return status;
}

bool DataFormatterConfigHandler::phaseScanAllChannels( bool fast ) {
  bool status = true;
  try{
    m_dfApi->phase_scan_all_ch( fast, m_clockPhaseDelayValues, m_numGoodDelayValues );
  } catch ( daq::ftk::IOError ex){
    throw ( daq::ftk::IOError ( ERS_HERE, m_name, " phase scan all channels failed.", ex));
  } 
  m_dfApi->setClockPhaseConfiguration( m_clockPhaseDelayValues );
  return status;
}

bool DataFormatterConfigHandler::lutConfiguration() {
  m_dfApi->lut_config_helper( m_lut_type_mod2idx,         m_lane_mod2idx );
  m_dfApi->lut_config_helper( m_lut_type_idx2mod,         m_lane_idx2mod );
  m_dfApi->lut_config_helper( m_lut_type_pixmod2dst,      m_pixmod2dst_map,      0x0000FFFF );
  m_dfApi->lut_config_helper( m_lut_type_sctmod2dst,      m_sctmod2dst_map,      0x0000FFFF );
  m_dfApi->lut_config_helper( m_lut_type_pixmod2tower,    m_pixmod2tower_map,    0xFFFFFFFF );
  m_dfApi->lut_config_helper( m_lut_type_sctmod2tower,    m_sctmod2tower_map,    0xFFFFFFFF );
  m_dfApi->lut_config_helper( m_lut_type_pixmod2ftkplane, m_pixmod2ftkplane_map, 0xFFFFFFFF );
  m_dfApi->lut_config_helper( m_lut_type_sctmod2ftkplane, m_sctmod2ftkplane_map, 0xFFFFFFFF );
  return true;
}

bool DataFormatterConfigHandler::downloadAllIMLUT() {
  ERS_LOG( "Downloading all IM LUT files." );
  bool status = true;

  if ( m_coolConnString != "" ) {
    std::vector<uint32_t> IMLutIdx;

    for ( uint32_t ii=0; ii < NUMBER_OF_DF_INPUT_LANES; ++ii ) {
      if ( m_FMCIn2RODId_map.find(ii) == m_FMCIn2RODId_map.end() ) continue;
      uint32_t rodId = m_FMCIn2RODId_map.at(ii);
      if ( m_ROB_to_hashIds.find( rodId ) == m_ROB_to_hashIds.end() ) {
        throw( daq::ftk::ftkException( ERS_HERE, m_name, " ROB for lane " + to_string(ii) + " wasn't configured properly via database." ) );
        return false;
      }

      m_dfApi->getIMApi()->downloadIMLUT( m_ROB_to_linkIds[ rodId ],
					    m_ROB_to_hashIds[ rodId ],
					    m_ROB_to_rodIds[ rodId ],
					    ii );
      
      IMLutIdx.push_back( ii );
    }

    m_dfApi->getIMApi()->setIMLutIdx( IMLutIdx );
  } else {
    m_dfApi->getIMApi()->downloadAllIMLUT( m_IMLUTsFile );
  }

  return status;
}

bool DataFormatterConfigHandler::downloadAllIMLUT_OTF() {
  ERS_LOG( "Downloading all IM LUT files OTF." );
  bool status = true;

  m_dfApi->getIMApi()->downloadAllIMLUT_OTF( m_IMLUTPrefix, m_FMCIn2RODId_map );

  return status;
}

bool DataFormatterConfigHandler::downloadAllPseudoData() {
  ERS_LOG( "Downloading all IM PseudoData." );
  bool status = true;

  m_dfApi->getIMApi()->downloadAllPseudoData( m_IMPseudoDataFile, m_IMPseudoData_InputRate );

  return status;
}

bool DataFormatterConfigHandler::downloadAllPseudoData_OTF() {
  ERS_LOG( "Downloading all IM PseudoData OTF." );
  bool status = true;

  m_dfApi->getIMApi()->downloadAllPseudoData_OTF( m_IMPseudoDataPrefix, m_FMCIn2RODId_map, m_IMPseudoData_InputRate );

  return status;
}

std::string to_hexstring( const uint32_t& theint ) {
  std::stringstream stream;
  stream << "0x" << std::hex << theint;
  return stream.str();
}

bool DataFormatterConfigHandler::checkIMDFLanes() {
  //bool status = true;
  ERS_LOG( "Checking IM LUT and DF configurations agree." );

  std::vector<uint32_t> IMIdx = m_dfApi->getIMApi()->getIMLutIdx();
  uint32_t IMMask = 0x0;
  for ( uint32_t i=0; i<IMIdx.size(); ++i ) {
    IMMask |= ( 0x1 << IMIdx.at(i) );
  }

  if ( IMMask != m_enableFMCLanesMask ) {
    ers::warning( daq::ftk::ftkException( ERS_HERE, m_name, " mis-match in IM LUT and DF enabled channels: IM = " + to_hexstring( IMMask ) + " and DF: " + to_hexstring( m_enableFMCLanesMask ) ) );
    return false;
  }
  return true;
}

// isn't implemented in FtkDataFormatterApi. Is this necessary?
bool DataFormatterConfigHandler::checkIMDFModules() {
  //bool status = true;
  return true;//status;
}

bool DataFormatterConfigHandler::buildConfiguration() {
  ERS_LOG( "Building configuration from text file input." );

  bool status = true;

  // Begin system-wide configuration ( protect shared resources from concurrent access )
  if ( !(m_globalConfigParseBegin++) ) {
    status = status && parseGlobalConfigFile( m_boardEnableMap, 
                                              m_rodIdToFMCInIdMap, 
                                              m_boardIdToFMCInIdToRODIdMap,
                                              m_boardIdToEnableFMCLanesMap,
                                              m_multishelf_boardEnableMap );
    if ( status ) m_globalConfigParseComplete = 0x1;
  }

  if ( !(m_systemConfigParseBegin++) ) {
    uint32_t numberOfFTKTowers = 0x0;
    status = status && parseSystemConfigFile( numberOfFTKTowers,
                                              m_DFBoardNumberToShelfAndSlotMap,
                                              m_FTKPlaneToDFOutputMap,
                                              m_DFBoardNumberToTopTowerMap, 
                                              m_DFBoardNumberToBotTowerMap,
                                              m_towerToDFBoardNumberMap,
                                              m_ATCAFabricChToOutputBitPosMap,
                                              m_DFFiberConnectionMap );
    if ( status ) m_systemConfigParseComplete = 0x1;
  }

  if ( !(m_modulelistConfigParseBegin++) ) {
    if ( m_coolConnString != "" ) {
      status = status && parseDatabaseFile( m_ROB_to_linkIds,
                                            m_ROB_to_hashIds,
                                            m_ROB_to_rodIds,
                                            m_ROB_to_planeInfos,
                                            m_modulelist_data );
    } else {
      status = status && parseModuleListFile( m_modulelist_data );
    }
    if ( status ) m_modulelistConfigParseComplete = 0x1;
  }
  // End system-wide configuration

  ERS_LOG( "Waiting for system-wide configuration to complete before beginning board-specific tasks." );
  // wait for system-wide config to complete
  const uint32_t N_SECONDS_TO_WAIT = 60;
  uint32_t nSecondsWaited = 0;
  while ( !(m_globalConfigParseComplete) || !(m_systemConfigParseComplete)
                                         || !(m_modulelistConfigParseComplete) ) {
    sleep(5);
    nSecondsWaited += 5;
    if ( nSecondsWaited >= N_SECONDS_TO_WAIT ) {
      throw( daq::ftk::FTKOperationTimedOut( ERS_HERE, name_ftk(), " waited too long for system-wide config to complete. Aborting." ) );
      return false;
    }
  }
  ERS_LOG( "All system-wide configuration is complete, building board-specific configuration." );

  // Begin board-specific configuration
  srand( m_boardNumber );

  m_FMCIn2RODId_map.clear();
  for ( auto& entry : m_boardIdToFMCInIdToRODIdMap ) {
    if ( entry.first == m_boardNumber ) {
      m_FMCIn2RODId_map = entry.second;
      break;
    }
  }

  if ( !m_useAutoDelaySetting ) {
    status = status && parseBoardDelayFile( m_laneToDelayMap, m_laneToInvMap  );
    m_clockPhaseDelayValues.clear();
    for ( uint32_t i_fpga=0; i_fpga < NUMBER_OF_IM_FPGAS_PER_DF; ++i_fpga ) {
      if ( m_laneToDelayMap.find( i_fpga ) == m_laneToDelayMap.end() )
        m_clockPhaseDelayValues.push_back( 0 );
      else
        m_clockPhaseDelayValues.push_back( m_laneToDelayMap.at( i_fpga ) );
    }
  }

  m_enableFMCLanesMask = 0x0;
  for ( auto& entry : m_boardIdToEnableFMCLanesMap ) {
    if ( entry.first == m_boardNumber ) {
      m_enableFMCLanesMask = entry.second;
      break;
    }
  }

  m_module_count_in = std::vector<uint32_t>(  NUMBER_OF_DF_INPUT_LANES, 0 );
  m_module_count_out = std::vector<uint32_t>( NUMBER_OF_DF_OUTPUT_LANES,    0 );
  status = status && processModuleList( m_module_count_in, m_module_count_out,
                                        m_pixmod2dst_map, m_sctmod2dst_map, m_pixmod2ftkplane_map,
                                        m_sctmod2ftkplane_map, m_pixmod2tower_map, m_sctmod2tower_map,
                                        m_FMCIn2modid_map, m_FMCIn2NumModules, m_SLINKOut2NumModules,
                                        m_lane_mod2idx, m_lane_idx2mod );

  status = status && getInternalLinkOutputDestWords( m_centralSwLaneId2DestMask );

  uint32_t ports=0;
  status = status && buildRxPorts( ports );
  m_rxports = ports;

  status = status && parseTransceiverParameters( m_gtCh2RxPolarity,
                                                 m_gtCh2TxPolarity,
                                                 m_gtCh2ForceReadyMode,
                                                 m_gtCh2ToAlteraFPGA,
                                                 m_gtCh2IgnoreFreeze  );

  ERS_LOG( "Done building configuration." );
  return status;
}

bool DataFormatterConfigHandler::parseBoardDelayFile( std::map<uint32_t, uint32_t>& delaymap,
                                                      std::map<uint32_t, uint32_t>& invmap ) 
{
  ERS_LOG( "Loading board delay values file " << m_boardDelayFile );

  bool status = true;

  delaymap.clear();
  invmap.clear();

  std::string line;
  std::ifstream conf_file( m_boardDelayFile.c_str() );
  if ( !conf_file.is_open() ) {
    throw( daq::ftk::IOError( ERS_HERE, m_name , " Cannot open board delay file " + m_boardDelayFile ) );
    return false;
  } else {
    ERS_LOG( "Loading board delay config file " << m_boardDelayFile );
    while ( std::getline( conf_file, line ) ) {
      line = line.substr(0, line.find("#"));

      if ( line.substr(0, 12) == "LaneDelayVal" ) {
        std::stringstream ss(line);
        std::string buf;
        std::string key;
        uint32_t lane, inv, delay;

        while ( ss >> key >> std::setbase(0) >> lane >> inv >> delay ) {
          delaymap.insert( std::make_pair( lane, delay ) );
          invmap.insert( std::make_pair( lane, inv ) );
        }
      }
    }
    conf_file.close();
  }

  return status;
}
bool DataFormatterConfigHandler::parseGlobalConfigFile( std::map<uint32_t, bool>& boardEnableMap,
                      std::multimap<uint32_t, std::pair<uint32_t, uint32_t> >& rodIdToFMCInIdMap,
                      std::map<uint32_t, std::map<uint32_t, uint32_t> >& boardIdToFMCInIdToRODIdMap,
                      std::map<uint32_t, uint32_t>& boardIdToEnableFMCLanesMap,
                      std::map<std::pair<uint32_t, uint32_t>, bool>& multishelf_boardEnableMap )
{
  ERS_LOG( "Loading global (boad) config file " << m_globalCF );

  boardEnableMap.clear();
  rodIdToFMCInIdMap.clear();
  boardIdToFMCInIdToRODIdMap.clear();
  boardIdToEnableFMCLanesMap.clear();
  multishelf_boardEnableMap.clear();

  std::string line;
  std::ifstream conf_file ( m_globalCF.c_str() );

  if ( !conf_file.is_open() ) {
    throw( daq::ftk::IOError( ERS_HERE, m_name , " unable to open global config file " + m_globalCF ) );
    return false;
  } else {
    ERS_LOG( "Loading board config file " << m_globalCF );

    while ( std::getline( conf_file, line ) ) {
      line = line.substr(0, line.find("#"));

      std::stringstream ss(line);
      std::string buf; std::string key;

      if ( line.substr(0, 11) == "boardEnable" ) {
        uint32_t board; uint32_t en;

        while( ss>>key>> std::setbase(0) >> board >> en ) {
          std::pair<uint32_t, bool> tmp_pair( board, en );
          boardEnableMap.insert( tmp_pair );
        }
      } // boardEnable
      else if ( line.substr(0, 10) == "rodToBoard" ) {
        uint32_t robId; uint32_t board; uint32_t fmcLane;
        while ( ss>>key>> std::setbase(0) >> robId >> board >> fmcLane ) {
          if ( boardEnableMap.find( board ) != boardEnableMap.end() && (boardEnableMap.find(board)->second != 0) ) { // skip disabled boards
            std::pair<uint32_t, std::pair<uint32_t, uint32_t> > tmp_pair( robId, std::make_pair(board, fmcLane) );
            rodIdToFMCInIdMap.insert( tmp_pair );
            if ( boardIdToFMCInIdToRODIdMap.find( board ) == boardIdToFMCInIdToRODIdMap.end() ) {
              boardIdToFMCInIdToRODIdMap[ board ] = std::map<uint32_t, uint32_t>();
            }
            boardIdToFMCInIdToRODIdMap[ board ][ fmcLane ] = robId;

            if ( boardIdToEnableFMCLanesMap.find( board ) == boardIdToEnableFMCLanesMap.end() ) {
              boardIdToEnableFMCLanesMap[ board ] = 0x0;
            }
            boardIdToEnableFMCLanesMap[ board ] |= ((0x1)<<fmcLane);
          }
        }
      } // rodToBoard
    } // end loop on lines in file
    conf_file.close();
  }

  std::ifstream multiShelf_conf_file ( m_multishelfCF.c_str() );
  if ( !multiShelf_conf_file.is_open() ) {
    throw( daq::ftk::IOError( ERS_HERE, m_name , " unable to open multishelf config file " + m_multishelfCF ) );
    return false;
  } else {
    ERS_LOG( "Loading multishelf config file " << m_multishelfCF );

    while ( std::getline( multiShelf_conf_file, line ) ) {
      line = line.substr(0, line.find("#"));

      if ( line.substr(0, 11) == "boardEnable" ) {
        std::stringstream ss( line );
        std::string key;

        uint32_t shelf, slot, enable;
        while ( ss >> key >> std::setbase(0) >> shelf >> slot >> enable ) {
          multishelf_boardEnableMap[ std::make_pair( shelf, slot ) ] = (bool) enable;
        }
      }
    }
  }

  return true;
}

bool DataFormatterConfigHandler::parseSystemConfigFile( uint32_t& numberOfFTKTowers,
                std::map<uint32_t, std::pair<uint32_t, uint32_t> >& DFBoardNumberToShelfAndSlotMap,
                std::map<uint32_t, uint32_t>& FTKPlaneToDFOutputMap,
                std::map<uint32_t, uint32_t>& DFBoardNumberToTopTowerMap,
                std::map<uint32_t, uint32_t>& DFBoardNumberToBotTowerMap,
                std::map<uint32_t, uint32_t>& towerToDFBoardNumberMap,
                std::map<uint32_t, uint32_t>& ATCAFabricChToOutputBitPosMap,
                std::map<uint32_t, uint32_t>& DFFiberConnectionMap ) {
  ERS_LOG( "Loading system config file " << m_systemCF );

  DFBoardNumberToShelfAndSlotMap.clear();
  FTKPlaneToDFOutputMap.clear();
  DFBoardNumberToTopTowerMap.clear();
  DFBoardNumberToBotTowerMap.clear();
  towerToDFBoardNumberMap.clear();
  ATCAFabricChToOutputBitPosMap.clear();
  DFFiberConnectionMap.clear();

  std::string line;
  std::ifstream conf_file ( m_systemCF.c_str() );

  if ( !conf_file.is_open() ) {
    throw( daq::ftk::IOError( ERS_HERE, m_name , " could not open system config file " + m_systemCF ) );
    return false;
  } else {
    ERS_LOG( "Loading system config file " << m_systemCF );

    while ( std::getline( conf_file, line ) ) {
      line = line.substr(0, line.find("#"));

      std::stringstream ss(line);
      std::string key;

      if ( line.substr(0,17) == "NumberOfFTKTowers" ) {
        ss>>key>>std::setbase(0)>> numberOfFTKTowers;
        ERS_LOG( "The number of FTK towers in config " << numberOfFTKTowers );
      } else if ( line.substr(0,20) == "BoardNToShelfAndSlot" ) {
        uint32_t board, shelf, slot;
        while( ss>>key>> std::setbase(0) >> board >> shelf >> slot ) {
          std::pair< uint32_t, std::pair<uint32_t, uint32_t> > tmp_pair( board,
                                                  std::pair<uint32_t, uint32_t>( shelf, slot ) );
          DFBoardNumberToShelfAndSlotMap.insert( tmp_pair );
        }
      } else if ( line.substr(0,13) == "PlaneToOutBit" ) {
        uint32_t plane, outbit;
        while( ss>>key>> std::setbase(0) >> plane >> outbit ) {
          std::pair<uint32_t, uint32_t> tmp_pair( plane, outbit );
          FTKPlaneToDFOutputMap.insert( tmp_pair );
        }
      } else if ( line.substr(0,16) == "BoardNToTopTower" ) {
        uint32_t board, tower;
        while( ss>>key>> std::setbase(0) >> board >> tower ) {
          std::pair<uint32_t, uint32_t> tmp_pair( board, tower );
          DFBoardNumberToTopTowerMap.insert( tmp_pair );
          std::pair<uint32_t, uint32_t> tmp_pair2( tower, board );
          towerToDFBoardNumberMap.insert( tmp_pair2 );
        }
      } else if ( line.substr(0,16) == "BoardNToBotTower" ) {
        uint32_t board, tower;
        while( ss>>key>> std::setbase(0) >> board >> tower ) {
          std::pair<uint32_t, uint32_t> tmp_pair( board, tower );
          DFBoardNumberToBotTowerMap.insert( tmp_pair );
          std::pair<uint32_t, uint32_t> tmp_pair2( tower, board );
          towerToDFBoardNumberMap.insert( tmp_pair2 );
        }
      } else if ( line.substr(0,12) == "ChanToOutBit" ) {
        uint32_t chan, obit;
        while ( ss>>key>> std::setbase(0) >> chan >> obit ) {
          std::pair<uint32_t, uint32_t> tmp_pair( chan, obit );
          ATCAFabricChToOutputBitPosMap.insert( tmp_pair );
        }
      } else if ( line.substr(0,17) == "DFFiberConnection" ) {
        uint32_t DF1, DF2;
        while ( ss>>key>> std::setbase(0) >> DF1 >> DF2 ) {
          std::pair<uint32_t, uint32_t> tmp_pair1( DF1, DF2 );
          std::pair<uint32_t, uint32_t> tmp_pair2( DF2, DF1 );
          DFFiberConnectionMap.insert( tmp_pair1 );
          DFFiberConnectionMap.insert( tmp_pair2 );
        }
      }
    } // end loop on lines 
    conf_file.close();
  }

  for( uint32_t iBoard=0; iBoard < NUMBER_OF_DF_BOARDS; iBoard++ ){
    if( m_DFBoardNumberToShelfAndSlotMap.find( iBoard ) == m_DFBoardNumberToShelfAndSlotMap.end() ){
      char errMsg[300];
      snprintf( errMsg, 300, "DF board number %u missing from shelf and slot map. Check the system config file %s", iBoard, m_systemCF.c_str() );
      throw daq::ftk::ftkException( ERS_HERE, name_ftk(), errMsg );
    }
  }

  return true;
}

bool DataFormatterConfigHandler::parseDatabaseFile( 
                                      std::map<uint32_t, std::vector<uint32_t> >& ROB_to_linkIds,
                                      std::map<uint32_t, std::vector<uint32_t> >& ROB_to_hashIds,
                                      std::map<uint32_t, std::vector<uint32_t> >& ROB_to_rodIds,
                                      std::map<uint32_t, std::vector<uint32_t> >& ROB_to_planeInfos,
                                      std::vector<std::vector<uint32_t> >& modulelist_data ) {
  ERS_LOG( "Load database from COOL connection string " << m_coolConnString );

  ROB_to_linkIds.clear();
  ROB_to_hashIds.clear();
  ROB_to_rodIds.clear();
  ROB_to_planeInfos.clear();
  modulelist_data.clear();

  cool::IDatabaseSvc& dbSvc = cool::DatabaseSvcFactory::databaseService();

  ERS_LOG( "Try opening the database." );
  cool::IDatabasePtr db;
  db = dbSvc.openDatabase( m_coolConnString, true );

  cool::IFolderPtr m_mod = db->getFolder( FtkCoolSchema::tables()["Module"]->name);

  cool::IObjectIteratorPtr objs = m_mod->browseObjects(0, cool::ValidityKeyMax, cool::ChannelSelection::all());

  while ( objs->goToNext() ) {
    const cool::IObject& obj = objs->currentRef();

    cool::IRecordIterator& recItr = obj.payloadIterator();
    while ( recItr.goToNext() ) {
      const cool::IRecord& p = recItr.currentRef();

      int onlineId = p["onlineId"].data<int>();
      int hashId = p["hashId"].data<int>();
      int planeInfo = p["planeInfo"].data<int>();
      std::string towerList = p["towerList"].data<std::string>();
      std::string rob = p["ROB"].data<std::string>();

      int rodId  = (onlineId & 0x0000FFFFFF);
      int linkId = (onlineId & 0x00FF000000);
      linkId = linkId >> 24;

      if ( (rodId & 0xF00000) == 0x200000 ) {
        int barrelEC = p["BarrelEC"].data<int>();
        int layer = p["layer"].data<int>();
        int eta = p["layer"].data<int>();
        int side = p["side"].data<int>();

       int layer_disk;
        // this conversion refer to https://cds.cern.ch/record/2002615/files/ATL-COM-DAQ-2015-027.pdf                                                    
        if(barrelEC == 0){ // Barrel                                                                                                                     
          layer_disk = layer*2 + (int)!(layer%2 ^ side);
        }else{ // Endcap                                                                                                                                 

          int layer_disk_add(0x0);
          if(layer == 8) layer_disk_add = 1 - side;
          else if(eta == 0) layer_disk_add = side;
          else if(eta == side + 1) layer_disk_add = 3;
          else layer_disk_add = 2;
          layer_disk = layer*4 + layer_disk_add;
        }

        bool isEndcapA = ( barrelEC == 2 );
        bool swapped = isEndcapA ^ ((layer_disk%4 == 0) || (layer_disk%4 == 3));
        if (swapped) hashId += 0x2000; // add phi swap bit for SCT

        linkId = 16*(linkId-linkId%12)/12 + linkId%12;
      }

      ROB_to_linkIds[ rodId ].push_back( linkId );
      ROB_to_hashIds[ rodId ].push_back( hashId );
      ROB_to_rodIds[  rodId ].push_back( rodId );

      uint32_t towermap_31_00 = 0x0;
      uint32_t towermap_63_32 = 0x0;
      std::istringstream str( towerList );
      while (str) {
        std::string s;
        str >> s;
        if (s == "") continue;
        s = s.substr(0, s.size()-1); // remove the trailing '.'
        long int value = strtoul( s.c_str(), NULL, 0);

        if (value<32) towermap_31_00 |= ( (0x1)<<value );
        else towermap_63_32 |= ( (0x1<<(value-32)) );
      }

      std::vector<uint32_t> modulelist_line;
      modulelist_line.push_back( rodId );
      modulelist_line.push_back( hashId );
      modulelist_line.push_back( towermap_31_00 );
      modulelist_line.push_back( towermap_63_32 );
      modulelist_line.push_back( planeInfo );
      modulelist_data.push_back( modulelist_line );
    } // loop on recItr
  } // loop on objs

  return true;
}

bool DataFormatterConfigHandler::parseModuleListFile( 
                                      std::vector<std::vector<uint32_t> >& modulelist_data ) {
  ERS_LOG( "Loading module list file " << m_moduleListFile );

  modulelist_data.clear();

  std::ifstream infile( m_moduleListFile.c_str() );

  if ( infile.fail() || infile.bad() ) {
    throw( daq::ftk::IOError( ERS_HERE, m_name , " could not open modulelist file " + m_moduleListFile ) );
    return false;
  }

  std::string line;
  while ( std::getline( infile, line ) ) {
    line = line.substr(0, line.find("#"));
    std::stringstream ss(line);

    std::string c1, c2, c3, c4, c5;
    ss >> c1 >> c2 >> c3 >> c4 >> c5;
    const uint32_t robId          = strtoul(c1.c_str(), NULL, 0);
    const uint32_t moduleId       = strtoul(c2.c_str(), NULL, 0);
    const uint32_t towermap_00_31 = strtoul(c3.c_str(), NULL, 0);
    const uint32_t towermap_32_63 = strtoul(c4.c_str(), NULL, 0);
    const uint32_t planeId        = strtoul(c5.c_str(), NULL, 0);

    std::vector<uint32_t> modulelist_line;
    modulelist_line.push_back( robId );
    modulelist_line.push_back( moduleId );
    modulelist_line.push_back( towermap_00_31 );
    modulelist_line.push_back( towermap_32_63 );
    modulelist_line.push_back( planeId );
    modulelist_data.push_back( modulelist_line );
  }

  return true;
}

bool DataFormatterConfigHandler::getInternalLinkOutputDestWords
                                    ( std::map<uint32_t, uint32_t>& centralSwLaneId2DestMask ) {
  ERS_LOG( "Loading DF central switch destination mask patterns." );
  for ( uint32_t iDest=0; iDest < NUMBER_OF_DF_BOARDS; ++iDest ) {
    uint32_t mask_pattern = ( iDest!=m_boardNumber ) ? 
                                    getInternalLinkOutputBitMap( m_boardNumber, iDest ) :
                                    0x0;
    centralSwLaneId2DestMask[ iDest ] = mask_pattern;
  }
  return true;
}

uint32_t DataFormatterConfigHandler::getInternalLinkOutputBitMap( uint32_t source, uint32_t dest ) {
  if ( source == dest ) return 0x0;

  uint32_t source_shelf = m_DFBoardNumberToShelfAndSlotMap.at( source ).first;
  uint32_t source_slot = m_DFBoardNumberToShelfAndSlotMap.at( source ).second;
  uint32_t dest_shelf = m_DFBoardNumberToShelfAndSlotMap.at( dest ).first;
  uint32_t dest_slot = m_DFBoardNumberToShelfAndSlotMap.at( dest ).second;

  uint32_t bitPos = getFabricBitPosFromShelfAndSlot( source_shelf, source_slot,
                                                     dest_shelf, dest_slot,
                                                     source, dest );
  return ((0x1)<<bitPos);
}

uint32_t DataFormatterConfigHandler::getFabricBitPosFromShelfAndSlot( uint32_t source_shelf,
                                                                      uint32_t source_slot,
                                                                      uint32_t dest_shelf,
                                                                      uint32_t dest_slot,
                                                                      uint32_t source_num,
                                                                      uint32_t dest_num )
{
  if ( source_num == dest_num ) return 0x0;

  if ( source_shelf == dest_shelf ) {
    uint32_t ATCAFabricChannelId = ( source_slot > dest_slot ) ? dest_slot : dest_slot-1;
    if ( m_ATCAFabricChToOutputBitPosMap.find( ATCAFabricChannelId ) == m_ATCAFabricChToOutputBitPosMap.end() ) {
      char errMsg[300];
      snprintf( errMsg, 300, "No entry in ATCAFabricChToOutputBitPosMap for channel ID %u. Check the system config file.", ATCAFabricChannelId );
      throw daq::ftk::ftkException( ERS_HERE, name_ftk(), errMsg );
    }
    return m_ATCAFabricChToOutputBitPosMap.at( ATCAFabricChannelId );
  }

  // case of FIBER connections
  // We have in total 4 cases
  // case 1: DF 1 - DF 2 has direct connection (go through fiber directly)  
  // case 2: DF 1 is connected to the crate that DF 2 is in (go through fiber directly)
  // case 3: DF 1 is not connected to the crate that DF 2 is in, and DF 3 is in crate of DF 1 and directly connected DF 2 (go through fabric)
  // case 4: DF 1 is not connected to the crate that DF 2 is in, and there isn't a DF is in crate of DF 1 and directly connected DF 2 (go through fabric)

  if ( m_DFFiberConnectionMap.find( source_num ) == m_DFFiberConnectionMap.end() ) {
    char errMsg[300];
    snprintf( errMsg, 300, "DF number %u not found in the fiber connection map! Check the fiber connections in the system config file.", source_num );
    throw daq::ftk::ftkException( ERS_HERE, name_ftk(), errMsg );
  } else if ( m_DFBoardNumberToShelfAndSlotMap.find( m_DFFiberConnectionMap.at(source_num) ) == m_DFBoardNumberToShelfAndSlotMap.end() ) {
    char errMsg[300];
    snprintf( errMsg, 300, "DF fiber connection for %u is %u, but the latter is not a recognized board.. Check the fiber connections in the system config file.", source_num, m_DFFiberConnectionMap.at(source_num) );
    throw daq::ftk::ftkException( ERS_HERE, name_ftk(), errMsg );
  }

  // case 1
  if ( m_DFFiberConnectionMap.at(source_num) == dest_num ){
    return 14;
  }

  // case 2 
  else if ( m_DFBoardNumberToShelfAndSlotMap.at( m_DFFiberConnectionMap.at(source_num) ).first == dest_shelf ){
    return 14;
  }

  else{
    // case 3 and 4
    uint32_t midBoard_slot;
    std::vector<int> DFsConnectedtoNextCrate;

    // case 3
    for(uint32_t iBoard=0; iBoard < NUMBER_OF_DF_BOARDS; iBoard ++){
      if( m_DFBoardNumberToShelfAndSlotMap.at(iBoard).first == source_shelf){
        if( m_DFFiberConnectionMap.at(iBoard) == dest_num ){
          midBoard_slot = m_DFBoardNumberToShelfAndSlotMap.at(iBoard).second;

          const uint32_t ATCAFabricChannelId =  // in terms of channel number of fabric interface
            (source_slot>midBoard_slot) ? midBoard_slot : midBoard_slot-1;

            return m_ATCAFabricChToOutputBitPosMap.at(ATCAFabricChannelId);
        }
      }
    }

    // case 4
    for(uint32_t iBoard=0; iBoard < NUMBER_OF_DF_BOARDS; iBoard ++){
      if( m_DFBoardNumberToShelfAndSlotMap.at(iBoard).first == source_shelf){

        uint32_t destination_board = m_DFFiberConnectionMap.at(iBoard);
  if ( m_DFBoardNumberToShelfAndSlotMap.find( destination_board ) == m_DFBoardNumberToShelfAndSlotMap.end() ) continue;
        if (m_DFBoardNumberToShelfAndSlotMap.at(destination_board).first == m_DFBoardNumberToShelfAndSlotMap.at(dest_num).first){
          DFsConnectedtoNextCrate.push_back(iBoard);
        }
      }
    }

    // case 4
    int random_board = (rand()%DFsConnectedtoNextCrate.size());
    midBoard_slot = m_DFBoardNumberToShelfAndSlotMap.at(  DFsConnectedtoNextCrate.at(random_board) ).second;
    const uint32_t ATCAFabricChannelId =  // in terms of channel number of fabric interface
      (source_slot>midBoard_slot) ? midBoard_slot : midBoard_slot-1;
    return m_ATCAFabricChToOutputBitPosMap.at(ATCAFabricChannelId);
  }

  return 0;
}

bool DataFormatterConfigHandler::processModuleList( 
                        std::vector<uint32_t>& module_count_in,
                        std::vector<uint32_t>& module_count_out,
                        std::map<uint32_t, uint32_t>& pixmod2dst_map,
                        std::map<uint32_t, uint32_t>& sctmod2dst_map,
                        std::map<uint32_t, uint32_t>& pixmod2ftkplane_map,
                        std::map<uint32_t, uint32_t>& sctmod2ftkplane_map,
                        std::map<uint32_t, uint32_t>& pixmod2tower_map,
                        std::map<uint32_t, uint32_t>& sctmod2tower_map,
                        std::map<uint32_t, std::vector<uint32_t> >& FMCIn2modid_map,
                        std::map<uint32_t, uint32_t>& FMCIn2NumModules,
                        std::map<uint32_t, uint32_t>& SLINKOut2NumModules,
                        std::map<std::pair<uint32_t, uint32_t>, uint32_t>& lane_mod2idx,
                        std::map<std::pair<uint32_t, uint32_t>, uint32_t>& lane_idx2mod )
{
  ERS_LOG( "Making routing maps and module counts from module list" );

  pixmod2dst_map.clear();
  sctmod2dst_map.clear();
  pixmod2ftkplane_map.clear();
  sctmod2ftkplane_map.clear();
  pixmod2tower_map.clear();
  sctmod2tower_map.clear();
  lane_mod2idx.clear();
  lane_idx2mod.clear();

  uint32_t top_tower = m_DFBoardNumberToTopTowerMap.at( m_boardNumber );
  uint32_t bot_tower = m_DFBoardNumberToBotTowerMap.at( m_boardNumber );

  uint32_t top_tower_map_mask_00_31 = (top_tower<32) ? (0x1<<top_tower) : 0x0;
  uint32_t top_tower_map_mask_32_63 = (top_tower<32) ? 0x0              : (0x1<<(top_tower-32));
  uint32_t bot_tower_map_mask_00_31 = (bot_tower<32) ? (0x1<<bot_tower) : 0x0;
  uint32_t bot_tower_map_mask_32_63 = (bot_tower<32) ? 0x0              : (0x1<<(bot_tower-32));
  if ( m_32TowerConfig ) {
    bot_tower_map_mask_00_31 = 0x0;
    bot_tower_map_mask_32_63 = 0x0;
  }

  for ( uint32_t iline=0; iline<m_modulelist_data.size(); ++iline ) {
    uint32_t ROBId          = m_modulelist_data[iline][0];
    uint32_t moduleId       = m_modulelist_data[iline][1];
    uint32_t towerMap_00_31 = m_modulelist_data[iline][2];
    uint32_t towerMap_32_63 = m_modulelist_data[iline][3];
    uint32_t planeId        = m_modulelist_data[iline][4];

    uint32_t nLinks = m_rodIdToFMCInIdMap.count( ROBId );
    if ( nLinks == 0 ) continue; // ROB is not part of the configuration

    // calculate destination bit mask
    uint32_t dest_mask = 0x0;
    for ( uint32_t iTower=0; iTower < NUMBER_OF_FTK_TOWERS; ++iTower ) {
      if ( m_towerToDFBoardNumberMap.find( iTower ) == m_towerToDFBoardNumberMap.end() ) {
        ers::warning( daq::ftk::IOError( ERS_HERE, m_name , " tower " + std::to_string(iTower) + " missing from board-to-tower mapping in " + m_systemCF ) );
        continue;
      }

      if ( iTower<32 and (((0x1<<iTower)&towerMap_00_31)!=0x0) ) {
        uint32_t dfBoardNumber = m_towerToDFBoardNumberMap.at( iTower );
        dest_mask |= (0x1<<dfBoardNumber);
      } else if ( iTower>=32 and (((0x1<<iTower)&towerMap_32_63)!=0x0) ) {
        uint32_t dfBoardNumber = m_towerToDFBoardNumberMap.at( iTower );
        dest_mask |= (0x1<<dfBoardNumber);
      }
    }
    bool thisBoardIsInDestList = ((0x1<<m_boardNumber) & dest_mask ) != 0x0;

    uint32_t tower_mask = 0x0;
    bool matchedToTopTower = ( ( (top_tower_map_mask_00_31 & towerMap_00_31) != 0x0 )
          or ( (top_tower_map_mask_32_63 & towerMap_32_63) != 0x0 ) );
    bool matchedToBotTower = ( ( (bot_tower_map_mask_00_31 & towerMap_00_31) != 0x0 )
          or ( (bot_tower_map_mask_32_63 & towerMap_32_63) != 0x0 ) );
    if ( matchedToTopTower ) {
      tower_mask |= 0x1;
    }
    if ( matchedToBotTower ) {
      tower_mask |= 0x2;
    }

    auto itr = m_rodIdToFMCInIdMap.find( ROBId );
    for ( uint32_t ilink=0; ilink < nLinks; ++ilink ) {
      const uint32_t& boardId_for_the_rod = itr->second.first;

      bool thisBoardIsTheFirstForTheModule = ( boardId_for_the_rod == m_boardNumber );

      // no need to consider this module
      if ( !(thisBoardIsInDestList or thisBoardIsTheFirstForTheModule ) ) continue;

      const uint32_t& FMCLaneId = itr->second.second;
      const uint32_t& DFOutputLane = m_FTKPlaneToDFOutputMap[planeId];

      bool isSCT = ( (ROBId & 0xF00000) == 0x200000 );

      if ( thisBoardIsTheFirstForTheModule ) { // add to input count/LUT
        module_count_in[ FMCLaneId ]++;
        if ( FMCIn2modid_map.find(FMCLaneId) == FMCIn2modid_map.end() )
          FMCIn2modid_map.insert( std::pair<uint32_t, std::vector<uint32_t> >( FMCLaneId,
                                                                  std::vector<uint32_t>() ) );

        if ( isSCT )
          sctmod2dst_map.insert( std::pair<uint32_t, uint32_t>( moduleId, dest_mask ) );
        else
          pixmod2dst_map.insert( std::pair<uint32_t, uint32_t>( moduleId, dest_mask ) );

        FMCIn2modid_map.at( FMCLaneId ).push_back( moduleId );
      }

      if ( thisBoardIsInDestList ) { // add to output count/plane&tower maps
        for ( uint32_t iOut=0; iOut < NUMBER_OF_DF_OUTPUT_LANES; ++iOut ) {
          if ( m_dfApi->usedOutputLane( iOut, matchedToTopTower, matchedToBotTower, DFOutputLane ) )
            module_count_out[ iOut ]++;
        }

        if ( isSCT ) {
          sctmod2ftkplane_map.insert( std::pair<uint32_t, uint32_t>( moduleId, (0x1<<DFOutputLane) ) );
          sctmod2tower_map.insert   ( std::pair<uint32_t, uint32_t>( moduleId, tower_mask ) );
        } else {
          pixmod2ftkplane_map.insert( std::pair<uint32_t, uint32_t>( moduleId, (0x1<<DFOutputLane) ) );
          pixmod2tower_map.insert   ( std::pair<uint32_t, uint32_t>( moduleId, tower_mask ) );
        }
      }
      ++itr;
    } // end loop on links for this ROBID
  } // end loop on modules

  for ( auto& item : m_rodIdToFMCInIdMap ) {
    uint32_t boardId = item.second.first;
    uint32_t ROBId = item.first;
    uint32_t lane = item.second.second;

    if ( boardId != m_boardNumber ) continue;

    bool isSCT = ( (ROBId & 0xF00000) == 0x200000 );

    bool isMapped = false;
    std::vector<uint32_t> moduleIds;
    for ( auto& entry : FMCIn2modid_map ) {
      if ( entry.first == lane ) {
        moduleIds = entry.second;
        isMapped = true;
        break;
      }
    }
    if ( !isMapped ) {
      throw( daq::ftk::ftkException( ERS_HERE, m_name, " Err> lane="+to_string(lane)+" is not mapped for the board = "+to_string(m_boardNumber)+"\n" ) );
      return false;
    }
    FMCIn2NumModules[ lane ] = moduleIds.size();

    for ( uint32_t iMod=0; iMod<moduleIds.size(); ++iMod ) {
      uint32_t modId = (isSCT) ? ( moduleIds.at(iMod)|0x2000 ) : moduleIds.at(iMod);

      lane_mod2idx[ std::make_pair( lane, modId ) ] = iMod;
      lane_idx2mod[ std::make_pair( lane, iMod ) ] = modId;
    }
  }

  SLINKOut2NumModules.clear();
  for ( uint32_t iSLINK=0; iSLINK<module_count_out.size(); ++iSLINK ) {
    SLINKOut2NumModules[ iSLINK ] = module_count_out.at( iSLINK );
  }

  ERS_LOG( "Finished making routing configuration and module lists." );

  return true;
}

bool DataFormatterConfigHandler::buildRxPorts( uint32_t& ports ) {
  ERS_LOG( "Building RX ports." );

  for ( const auto& item : m_boardEnableMap ) {
    if ( item.second == 0 ) continue;

    std::pair<uint32_t, uint32_t> shelfAndSlot = m_DFBoardNumberToShelfAndSlotMap.find(item.first)->second;
    uint32_t shelf = shelfAndSlot.first;
    uint32_t slot = shelfAndSlot.second;
    if ( shelf == m_shelf ) {
      if ( slot == m_slot ) continue;

      int offset = 3;
      if ( slot > m_slot ) offset=4;
      ports |= (0x1<<(slot-offset));
      ports |= (0x1<<(slot-offset+12));
    } else {
      // check for inter-shelf QSFP connection, need to open additional rx ports
      uint32_t shelf_mask = 0x400400;
      if (( m_DFFiberConnectionMap.find( m_boardNumber ) != m_DFFiberConnectionMap.end()
              && m_DFFiberConnectionMap.find( m_boardNumber )->second == item.first ) ||
          ( m_DFFiberConnectionMap.find( item.first ) != m_DFFiberConnectionMap.end()
              && m_DFFiberConnectionMap.find( item.first )->second == m_boardNumber ))
        ports |= shelf_mask;
    }
  }

  return true;
}

bool DataFormatterConfigHandler::parseTransceiverParameters( 
                                               std::map<uint32_t, uint32_t>& gtCh2RxPolarity,
                                               std::map<uint32_t, uint32_t>& gtCh2TxPolarity,
                                               std::map<uint32_t, uint32_t>& gtCh2ForceReadyMode,
                                               std::map<uint32_t, uint32_t>& gtCh2ToAlteraFPGA,
                                               std::map<uint32_t, uint32_t>& gtCh2IgnoreFreeze)
{
  ERS_LOG( "Loading transceiver configuration file " << m_transceiverCF );

  bool status = true;

  std::ifstream infile( m_transceiverCF );

  if ( not infile.is_open() ) {
    throw( daq::ftk::IOError( ERS_HERE, m_name , " could not open transceiver file " + m_transceiverCF ) );
    status = false;
  } else {
    std::string line;
    std::pair<uint32_t, uint32_t> tmp_pair;

    while ( std::getline( infile, line ) ) {
      std::stringstream ss( line );
      std::string c1, c2, c3, c4;
      ss >> c1 >> c2 >> c3 >> c4;

      c1 = c1.substr(0, c1.find("#"));

      tmp_pair = std::make_pair( strtoul( c2.c_str(), NULL, 0 ),
                                 strtoul( c3.c_str(), NULL, 0 ) );

      if ( c1 == "gtch2rxpolarity" ) gtCh2RxPolarity.insert( tmp_pair );
      else if ( c1 == "gtch2txpolarity" ) gtCh2TxPolarity.insert( tmp_pair );
      else if ( c1 == "gtch2force_ready_mode" ) gtCh2ForceReadyMode.insert( tmp_pair );
      else if ( c1 == "gtch2to_altera_fpga" ) gtCh2ToAlteraFPGA.insert( tmp_pair );
      else if ( c1 == "gtch_ignore_freeze" ) gtCh2IgnoreFreeze.insert( tmp_pair );
    }
  }

  return status;
}







