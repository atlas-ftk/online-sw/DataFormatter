# emulate the behavior of the DF input handler.
# The current implementation only outputs frame words.

# Input file name: required cmd line argument
# Input file format: N 34-bit words per line for N input lanes, in hex (eg. 0x?????????)
# # bits 0-31: input data word
# # bit 32: control word bit
# # bit 33: data valid bit
# Words on each line should be separated by whitespace.
# The number of input lanes is the number of words on the first line, unless the optional cmd line argument is supplied.
# If, on any line, there are m < N words, then the first m lanes will process that line (and the rest treat the line as an idle word).

# Frame output file name: cmd line argument or default -> input_file + "_frame.out"
# Frame output file format: 1 34-bit word per line, in hex (eg. 0x?????????)
# # bits 0-31: output frame word
# # bit 32: control word bit
# # bit 33: data valid bit (will always be 1 for this SW emulator)

# Module data output currently not implemented.

# Disabling lanes:
# Any lane that has one or more of the following packet errors is "disabled" for that packet:
# # Incorrect number of control words, or any control word is malformed
# # A second valid b0f word appears before the e0f (the second b0f is assumed to belong to the following packet)
# # Valid word between e0f and b0f (disabled for the following packet)
# # Any of the following words are module header words (begin with 0x8000): 
# # # ( 2nd after b0f, 4th after b0f, 5th after b0f, 6th after b0f )
# # 7th valid word after b0f is not either a module header (begins with 0x8000) or the e0da word
# # 1st valid word after b0f is not 0xff1234ff
# # 5th valid word after e0da is not the e0f word
# # L1ID in trailer (1st valid word after e0da) is not the same as in header (3rd valid word after b0f)
# If on any given packet, all lanes are disabled, processing continues to the next packet and no output is produced.

# Majority logic:
# These frame words are chosen by majority logic over the (enabled) input lanes:
# # Run number
# # BCID
# # L1TT
# # evTyp ROD and TIM
# # checksum
# Each bit is chosen independently by majority logic over the (enabled) input lanes
# Ties for a single bit result in 0

# bitwise majority logic: each of the 32 bits are chosen as the most common of '0' or '1' among all inputs
def maj_logic_word( words ):
  N = len(words)
  if N == 0: return 0
  if N == 1: return words[0]

  retval = 0x0

  for i in xrange(32):
    bits = [ (word & (0x1<<i))>>i for word in words ]
    bit = maj_logic_bit( bits )
    retval |= bit<<i

  return retval
def maj_logic_bit( bits ):
  N = len(bits)
  if N == 0: return 0
  if N == 1: return bits[0]

  num_ones = sum(bits)
  if num_ones > len(bits)/2: return 0x1 # ties give 0
  else: return 0x0

# Error flags word is a bitwise or over the (enabled) input lanes

# The global L1ID is chosen according to the following logic:
# # For the first event, the L1ID is chosen by majority logic over the enabled input lanes
# # For subsequent events:
# # # if any lane sees a curr_l1id == (previous_l1id + 1), that value is used
# # # else if the majority of enabled lanes see ECR, the global L1ID is the majority logic of the ECR bits
# # # else the L1ID is the majority logic over the enabled lanes
# If any enabled lane sees a L1ID that is greater than the global L1ID, that lane is held
# A held lane does not process it's input, but participates in future L1ID decisions (so it may resume processing at a later time)
# The L1ID output in the header and trailer are the same, since the L1ID decision logic only uses enabled lanes

# takes a list of l1id words, the previous l1id value, and whether or not this is the first packet
def l1id_logic( l1ids, previous_l1id, first_packet ):
  if first_packet:
    return maj_logic_word( l1ids ) # truncates to 32 bits

  nConsL1ID = 0
  nECR = 0

  previous_l1id &= 0xFFFFFFFF # truncate to 32 bits
  for l1id in l1ids:
    l1id &= 0xFFFFFFFF # truncate to 32 bits
    if l1id == previous_l1id + 1: nConsL1ID += 1
    elif (l1id & 0xFFFFFF) == 0: nECR += 1

  if (nConsL1ID > 0): return previous_l1id + 1
  # as it happens, if majority of L1IDs are ECR, then the majority logic over ECR bits is just
  # the majority logic over all the bits
  #elif (nECR > len(l1ids)/2): return maj_logic_word( l1ids )
  else: return maj_logic_word( l1ids )

# Timing out:
# This emulator allows lanes to be timed out on a given packet through two timeout schemes:
# b0f skew timeout --
# # Let x be the line number of the first b0f word across all input lanes.
# # For all other lanes, let y be the line number of the next b0f word in that lane.
# # If y - x > B0F_SKEW_THRESHOLD, the lane is timed out and does not process it's input for this particular packet.
# # The lane will resume processing when the skew (y - x) becomes smaller than the threshold
# # B0F_SKEW_THRESHOLD is set here:
B0F_SKEW_THRESHOLD = 0xffff # corresponds to ~330 usec at 200MHz
# evt_counter timeout --
# # Let y be the line number of the b0f word in a particular input lane for a particular packet
# # Let z be the line number of the next e0f or b0f word in that lane, whichever is first
# # If z - y > EVT_COUNTER_THRESHOLD, the lane is timed out 
# # In particular, the lane does not process any input line after y + EVT_COUNTER_THRESHOLD
# # The lane will resume processing from the next b0f.
# # EVT_COUNTER_THRESHOLD is set here:
EVT_COUNTER_THRESHOLD = 0xffff # corresponds to ~330 usec at 200MHz

# a Packet class, represents every word after e0f until the following e0f
import copy

class Packet:

  # in this case, words is a list of tuples: ( linenum, word )
  def __init__(self, words):
    assert len(words) > 0, "Cannot create Packet with no input words"
    self.words = copy.deepcopy(words)
    self.b0f_linenum = self.words[0][0]
    self.parsed = False
    self.run_num = -1
    self.l1id = -1
    self.bcidword = -1
    self.l1ttword = -1
    self.evTypTIMword = -1
    self.errword = -1
    self.checksum = -1

    self.parse_words()


  # most of the work happens here
  # parse the packet and detect the packet errors described at the top of this file
  def parse_words(self):
    self.evt_counter_timeout = False
    self.packet_err_too_short = False
    self.packet_err_words_before_b0f = False
    self.packet_err_header1 = False
    self.packet_err_early_module_header = False
    self.packet_err_8th_word = False
    self.packet_err_ctrl_word_malformed = False
    self.packet_err_l1id_mismatch = False
    self.packet_err_ctrl_words_count = False

    self.packet_err_too_short = len(self.words) < 13

    if ( self.packet_err_too_short ): # don't need to do any more processing
      self.packet_err = True
      return

    valid_words_count = -1 # N valid words since b0f
    ctrl_words_count = 0 # N control words
    valid_words_since_e0da = -1 # N valid words since e0da
    self.e0f_seen = False
    self.b0f_seen = False

    for (linenum, word) in self.words:
      if self.b0f_seen and (linenum - self.b0f_linenum > EVT_COUNTER_THRESHOLD): 
        self.evt_counter_timeout = True
        break

      if not ( word & 0x200000000 ): continue # check data valid bit

      if ( valid_words_count >= 0 ): valid_words_count += 1
      elif ( word & 0x1FFFF0000 ) != 0x1b0f00000: # did we see a word before b0f?
        self.packet_err_words_before_b0f = True

      if ( valid_words_since_e0da >= 0 ): valid_words_since_e0da += 1

      # check 1st word after b0f
      if valid_words_count == 1 and (( word & 0x1FFFFFFFF ) != 0x0FF1234FF):
        self.packet_err_header1 = True

      elif valid_words_count == 7 and (( word & 0x1FFFF0000 ) != 0x080000000) and (( word & 0x1FFFF0000 ) != 0x1e0da0000 ):
        self.packet_err_8th_word = True

      elif valid_words_count == 3:
        self.l1id = word & 0xFFFFFFFF

      elif valid_words_count in [ 2, 4, 5, 6 ] and (( word & 0x1FFFF0000 ) == 0x08000 ):
        self.packet_err_early_mod_header = True

      if valid_words_count == 2:
        self.run_num = word & 0xFFFFFFFF
      elif valid_words_count == 4:
        self.bcidword = word & 0xFFFFFFFF
      elif valid_words_count == 5:
        self.l1ttword = word & 0xFFFFFFFF
      elif valid_words_count == 6:
        self.evTypTIMword = word & 0xFFFFFFFF
      elif valid_words_since_e0da == 2:
        self.errword = word & 0xFFFFFFFF

      elif valid_words_since_e0da == 4:
        self.checksum = word & 0xFFFFFFFF

      if ( word & 0x100000000): # ctrl word
        ctrl_words_count += 1
        if ( word & 0xFFFF0000 ) == 0xe0da0000:
          valid_words_since_e0da = 0
        elif ( word & 0xFFFF0000 ) == 0xb0f00000:
          valid_words_count = 0
          self.b0f_linenum = linenum
          self.b0f_seen = True
        elif ( word & 0xFFFF0000 ) == 0xe0f00000:
          self.e0f_linenum = linenum
          self.e0f_seen = True
        else:
          self.packet_err_ctrl_word_malformed = True

      if valid_words_since_e0da == 1: # check trailer L1ID
        self.packet_err_l1id_mismatch = ( ( word & 0xFFFFFFFF ) != self.l1id )

    if ctrl_words_count != 3:
      self.packet_err_ctrl_words_count = True

    if valid_words_count < 13:
      self.packet_err_too_short = True

    self.packet_err = self.packet_err_too_short \
                      or self.packet_err_words_before_b0f \
                      or self.packet_err_header1 \
                      or self.packet_err_early_mod_header \
                      or self.packet_err_8th_word \
                      or self.packet_err_ctrl_word_malformed \
                      or self.packet_err_l1id_mismatch \
                      or self.packet_err_ctrl_words_count

    self.parsed = True
    return

# lane_words is a list of list of (int) input words
def parse_lane_inputs( lane_words ):
  retval = [] # a list of packet objects

  # a "packet" is every word after the previous e0f up to and including the next e0f
  # this is to catch the case where we see extra words between e0f and b0f
  # There are two exceptions:
  # # the first packet starts on the first b0f in the file
  # # we don't allow multiple b0f words in a packet, so a packet truncates with the word just before the next b0f in case of multiple b0f words showing up between two e0f words
  first_packet = True
  curr_packet_words = []
  curr_packet_b0f_seen = False
  i = 0
  while (i<len(lane_words)):

    word = lane_words[i]

    if not word & 0x200000000: # can just skip non-valid input
      i += 1
      continue

    if ( word & 0x3FFFF0000 ) == 0x3b0f00000:
      if curr_packet_b0f_seen: # 2nd b0f, need to truncate the packet
        retval.append( Packet( curr_packet_words ) )
        curr_packet_words = []
      curr_packet_b0f_seen = True
      first_packet = False
    elif ( word & 0x3FFFF0000 ) == 0x3e0f00000: # close the packet
      curr_packet_words.append( (i, word) )
      retval.append( Packet( curr_packet_words ) )
      curr_packet_words = []
      curr_packet_b0f_seen = False
      i += 1
      continue

    if not first_packet:
      curr_packet_words.append( (i, word) )

    i += 1

  return retval



# now the actual processing


import argparse

parser = argparse.ArgumentParser(description='Emulate the DF input handler module')
parser.add_argument('input_filename', help='Path to the input file.')
parser.add_argument('--out', dest='output_filename', default='', help='Path to the output file.')
parser.add_argument('--nInput', dest='N', type=int, default=-1, help='Number of input lanes.')

args = parser.parse_args()
infile = args.input_filename
outfile = args.output_filename
if len(outfile)==0: outfile = infile+'_frame.out'
N = args.N

from os import access, R_OK
from os.path import isfile

assert isfile(infile) and access(infile, R_OK), \
  "File {} doesn't exist or isn't readable".format(infile)


# process input file
lane_words = [] # a list of lists of words

with open(infile, 'r+') as fp:
  firstline = True

  for line in fp:
    words = []
    for token in line.strip().split():
      try:
        words.append( int(token, 16) )
      except ValueError:
        pass

    if firstline:
      if N < 0:
        N = len(words)
      lane_words = [ [] for i in xrange(N) ]
      firstline = False


    while len(words) < N: words.append(0) # add idle for missing words

    [ lane_words[i].append( word ) for i,word in enumerate(words) ]
# done reading input file

# parse the packets in each lane
lane_packets = [ parse_lane_inputs( lane_words[i] ) for i in xrange(N) ] # a list of lists of packets

# now we've parsed all the input packets. It remains to loop over them and produce the output

# Useful constants for making output
# full 34-bit
B0F_WORD  = 0x3b0f00000
E0DA_WORD = 0x3e0da0000
E0F_WORD  = 0x3e0f00000
HEADER1_WORD = 0x2FF1234FF

frame_out = []

packet_i = [ 0 for lane in lane_packets ] # initialize the packet index for each lane
lane_enabled = dict()
for i in xrange(len(lane_packets)):
  lane_enabled[i] = True
first_packet = True
previous_l1id = -1
internal_event_counter = 0
while True:

  # see if there are any more packets in this lane
  nEnabled = 0
  for i_lane, lane in enumerate(lane_packets):
    if packet_i[i_lane] >= len(lane): lane_enabled[i_lane] = False
    else: nEnabled += 1
  # break condition -> there are no more lanes with packets
  if nEnabled == 0: break 

  print "------------------------------------------------------------------------------"
  print "Packet 0x%03x. N enabled=%2d. Packet indices: "%(internal_event_counter, nEnabled), packet_i

  # decide on b0f timeouts
  b0f_linenums = dict()
  min_linenum = 1e9
  for i_lane,lane in enumerate(lane_packets):
    if not lane_enabled[i_lane]: continue
    b0f_linenums[ i_lane ] = lane[ packet_i[i_lane] ].b0f_linenum
    if b0f_linenums[i_lane] < min_linenum:
      min_linenum = b0f_linenums[i_lane]
  # second loop to disable timed out lanes
  for i_lane, lane in enumerate(lane_packets):
    if not lane_enabled[i_lane]: continue
    if (b0f_linenums[i_lane] - min_linenum) > B0F_SKEW_THRESHOLD:
      # time out without going to the next packet
      lane_enabled[i_lane] = False

  # now disable lanes with errors in the header (and advance to the next packet)
  for i_lane, lane in enumerate(lane_packets):
    if not lane_enabled[i_lane]: continue
    packet = lane[ packet_i[i_lane] ]
    if packet.packet_err_words_before_b0f \
        or packet.packet_err_header1 \
        or packet.packet_err_early_module_header \
        or packet.packet_err_8th_word:
      lane_enabled[i_lane] = False
      packet_i[ i_lane ] += 1
  enabled_packets = [ lane[ packet_i[i_lane] ] for i_lane, lane in enumerate(lane_packets) if lane_enabled[ i_lane ] ]
  post_header_nEnabled = len(enabled_packets)
  print "Packet 0x%03x. post header N disabled lanes= %2d"%(internal_event_counter,nEnabled-post_header_nEnabled)

  # if no enabled packets, skip this packet
  if len(enabled_packets)==0: continue 

  # now compute and send the header words
  # random 5bit counter = 0x0
  internal_frame_header = 0x3c0000000 | (0xFFFF & internal_event_counter)
  frame_out.append( internal_frame_header )
  frame_out.append( B0F_WORD )
  frame_out.append( HEADER1_WORD )
  frame_out.append( 0x200000000 | maj_logic_word( [ packet.run_num for packet in enabled_packets ] ) )
  curr_l1id =  l1id_logic( [ packet.l1id for packet in enabled_packets ], previous_l1id, first_packet )
  frame_out.append( 0x200000000 | curr_l1id )

  # now apply l1id timeout and holding
  for i_lane, lane in enumerate(lane_packets):
    if not lane_enabled[i_lane]: continue
    packet = lane[ packet_i[i_lane] ]
    if packet.l1id > curr_l1id: # hold the lane
      lane_enabled[i_lane] = False
      continue # don't advance the packet index
    elif packet.l1id != curr_l1id: # disable the lane
      lane_enabled[i_lane] = False
      packet_i[i_lane] += 1 # advance the packet index
  enabled_packets = [ lane[ packet_i[i_lane] ] for i_lane, lane in enumerate(lane_packets) if lane_enabled[i_lane] ]
  print "Packet 0x%03x. Global L1ID= %08x"%(internal_event_counter,curr_l1id)
  l1id_check_nEnabled = len(enabled_packets)
  print "Packet 0x%03x. L1ID check N disabled lanes=  %2d"%(internal_event_counter,post_header_nEnabled-l1id_check_nEnabled)

  frame_out.append( 0x200000000 | maj_logic_word( [ packet.bcidword for packet in enabled_packets ] ) )
  frame_out.append( 0x200000000 | maj_logic_word( [ packet.l1ttword for packet in enabled_packets ] ) )
  frame_out.append( 0x200000000 | maj_logic_word( [ packet.evTypTIMword for packet in enabled_packets ] ) )
  frame_out.append( E0DA_WORD )
  frame_out.append( 0x200000000 | curr_l1id )

  # now additionally disable lanes with errors affecting the trailer
  for i_lane, lane in enumerate(lane_packets):
    if not lane_enabled[i_lane]: continue
    packet = lane[ packet_i[i_lane] ]
    if packet.packet_err_ctrl_word_malformed \
        or packet.packet_err_l1id_mismatch \
        or packet.packet_err_ctrl_words_count:
      lane_enabled[i_lane] = False
      packet_i[ i_lane ] += 1
  enabled_packets = [ lane[ packet_i[i_lane] ] for i_lane, lane in enumerate(lane_packets) if lane_enabled[ i_lane ] ]
  post_trailer_nEnabled = len(enabled_packets)
  print "Packet 0x%03x. post trailer N disabled lanes=%2d"%(internal_event_counter,l1id_check_nEnabled-post_trailer_nEnabled)

  # error flag is or over all enabled lanes
  err_flag = 0
  for packet in enabled_packets:
    err_flag |= packet.errword
  frame_out.append( 0x200000000 | err_flag )
  # just use 0 for the reserved word
  frame_out.append( 0x200000000 )
  # also 0 for the checksum
  frame_out.append( 0x200000000 )
  frame_out.append( E0F_WORD )

  first_packet = False
  previous_l1id = curr_l1id
  internal_event_counter += 1
  # increment packet counter for enabled lanes
  for i_lane, lane in enumerate(lane_packets):
    if not lane_enabled[i_lane]:
      lane_enabled[i_lane] = True
    else:
      packet_i[i_lane] += 1

# time to write output
with open(outfile, "w+") as fp:
  for word in frame_out:
    fp.write("%09x\n"%word)


