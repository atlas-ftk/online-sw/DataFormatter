#!/bin/sh

DIR="USA15_FiberTest_Results/"
LOG="USA15_log.txt"
TIME=`date "+%H%M%S"`

echo "HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH"
echo "H  Fiber test script in USA 15  H"
echo "H  only use 2nd sfp from left   H"
echo "HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH"

if [ $# != 1 ];  then
    echo "usage : USA15_FiberTest [ROD_ID]"
    echo "ERROR  1 argument required"
    exit 1
fi

echo -n " Start test for RODID= " 
echo -n $1 
echo "."

mkdir -p $DIR

DF="DF1D"

# Reset  enable ch 1 3 5 7 9 11 13 15. don't care other detail config. we ignore xoff
echo -n "Reset and config......"
test_df -M 1 -D DF1D -F config/configuration_mod_delay_v2.txt -X config/USA15_transceier_configuration_orig.txt

echo " done"

# enable SLINK 0; slink, 1: pseudo
echo "Opening odd SLINK channels......"
test_im -D ${DF} -M 9 -F 0 -N 0 -V 1
test_im -D ${DF} -M 9 -F 0 -N 1 -V 0
test_im -D ${DF} -M 9 -F 1 -N 0 -V 1
test_im -D ${DF} -M 9 -F 1 -N 1 -V 0
test_im -D ${DF} -M 9 -F 2 -N 0 -V 1
test_im -D ${DF} -M 9 -F 2 -N 1 -V 0
test_im -D ${DF} -M 9 -F 3 -N 0 -V 1
test_im -D ${DF} -M 9 -F 3 -N 1 -V 0
echo " done"

# enable xoff from DOWNSTRAM (e.g. df) 0;on, 1:off
echo "hold_down ignore setting....."
test_im -D ${DF} -M 8 -F 0 -N 0 -V 1
test_im -D ${DF} -M 8 -F 0 -N 1 -V 1
test_im -D ${DF} -M 8 -F 1 -N 0 -V 1
test_im -D ${DF} -M 8 -F 1 -N 1 -V 1
test_im -D ${DF} -M 8 -F 2 -N 0 -V 1
test_im -D ${DF} -M 8 -F 2 -N 1 -V 1
test_im -D ${DF} -M 8 -F 3 -N 0 -V 1
test_im -D ${DF} -M 8 -F 3 -N 1 -V 1
echo " done"

# enable xoff to upstream (e.g. quest, ROD)
# dont touch that. Default off! 


# Check the leds
echo -n "Is LEDs ok? (error, ~50k Hz, ~10k Hz, ~1k Hz) [y:SMon and DFInSpy/n:exit] y/n >"
while read answer; do
    case $answer in
	'y' ) 	    break;;

	'n' ) echo "Exit the scirpt">> $DIR/$LOG
	    exit 0;;
        *) echo -n "[y/n]! >"
    esac
done

# log
echo -n  RODID$1 >> $DIR/$LOG
echo -n  "  led:OK  " >> $DIR/$LOG

# get SMon
echo "SMon reading...."
test_im -D ${DF} -M 4 -F 2 -N 1 | tee $DIR/SMon_RODID${1}_${TIME}.dat

head -n 43 $DIR/SMon_RODID${1}_${TIME}.dat
# Check the leds
echo -n "Is there some event?[n:exit]  y/n >"
while read answer; do
    case $answer in
	'y' ) 	    break;;

	'n' ) echo "Exit the scirpt">> $DIR/$LOG
	    exit 0;;
        *) echo -n "[y/n]! >"
    esac
done

# log
echo -n  "  SMon:OK  " >> $DIR/$LOG

# get DF inspy
echo "DF inspy reading...."
test_df -D ${DF} -M 5 -L 39 | tee $DIR/DFinspy_RODID${1}_${TIME}.dat


clear
head -n 35 $DIR/DFinspy_RODID${1}_${TIME}.dat
# Check the leds
echo -n "Is ther rirst 2 events?[n:exit]  y/n >"
while read answer; do
    case $answer in
	'y' ) 	    break;;

	'n' ) echo "Exit the scirpt">> $DIR/$LOG
	    exit 0;;
        *) echo -n "[y/n]! >"
    esac
done

# log
echo -n  "  DF inSpy:OK  " >> $DIR/$LOG
echo -n  " All ok  " >> $DIR/$LOG
date >> $DIR/$LOG


echo "HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH"
echo -n "H  Fibber test done for RODID : " 
echo $1
echo "H All ok          Detail :"
echo $DIR"/"$LOG
echo $DIR"/SMon_RODID"${1}"_"${TIME}".dat"
echo $DIR"/DFinspy_RODID"${1}"_"${TIME}".dat"
echo "HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH"