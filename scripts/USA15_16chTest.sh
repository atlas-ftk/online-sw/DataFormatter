
# download LUT
echo "ch0 LUT Downloading..... "
test_im -D DF1D -M 5 -F 0 -N 0 -T FakeLUTModuleList/FakeLUT_ch00.txt > /dev/null
echo "ch1 LUT Downloading..... "
test_im -D DF1D -M 5 -F 0 -N 1 -T FakeLUTModuleList/FakeLUT_ch01.txt > /dev/null
echo "ch2 LUT Downloading..... "
test_im -D DF1D -M 5 -F 1 -N 0 -T FakeLUTModuleList/FakeLUT_ch02.txt > /dev/null
echo "ch3 LUT Downloading..... "
test_im -D DF1D -M 5 -F 1 -N 1 -T FakeLUTModuleList/FakeLUT_ch03.txt > /dev/null
echo "ch4 LUT Downloading..... "
test_im -D DF1D -M 5 -F 2 -N 0 -T FakeLUTModuleList/FakeLUT_ch04.txt > /dev/null
echo "ch5 LUT Downloading..... "
test_im -D DF1D -M 5 -F 2 -N 1 -T FakeLUTModuleList/FakeLUT_ch05.txt > /dev/null
echo "ch6 LUT Downloading..... "
test_im -D DF1D -M 5 -F 3 -N 0 -T FakeLUTModuleList/FakeLUT_ch06.txt > /dev/null
echo "ch7 LUT Downloading..... "
test_im -D DF1D -M 5 -F 3 -N 1 -T FakeLUTModuleList/FakeLUT_ch07.txt > /dev/null
echo "ch8 LUT Downloading..... "
test_im -D DF1D -M 5 -F 4 -N 0 -T FakeLUTModuleList/FakeLUT_ch08.txt > /dev/null
echo "ch9 LUT Downloading..... "
test_im -D DF1D -M 5 -F 4 -N 1 -T FakeLUTModuleList/FakeLUT_ch09.txt > /dev/null
echo "ch10 LUT Downloading..... "
test_im -D DF1D -M 5 -F 5 -N 0 -T FakeLUTModuleList/FakeLUT_ch10.txt > /dev/null
echo "ch11 LUT Downloading..... "
test_im -D DF1D -M 5 -F 5 -N 1 -T FakeLUTModuleList/FakeLUT_ch11.txt > /dev/null
echo "ch12 LUT Downloading..... "
test_im -D DF1D -M 5 -F 6 -N 0 -T FakeLUTModuleList/FakeLUT_ch12.txt > /dev/null
echo "ch13 LUT Downloading..... "
test_im -D DF1D -M 5 -F 6 -N 1 -T FakeLUTModuleList/FakeLUT_ch13.txt > /dev/null
echo "ch14 LUT Downloading..... "
test_im -D DF1D -M 5 -F 7 -N 0 -T FakeLUTModuleList/FakeLUT_ch14.txt > /dev/null
echo "ch15 LUT Downloading..... "
test_im -D DF1D -M 5 -F 7 -N 1 -T FakeLUTModuleList/FakeLUT_ch15.txt > /dev/null 


# full ch oen config file
#test_df -M 1 -D DF1D -F conf_script/USA15_configuration.txt -X config_files/USA15_transceier_configuration_orig.txt
test_df -M 1 -D DF1D -F config/configuration_mod_delay_v2.txt -X config/USA15_transceier_configuration_orig.txt
#test_df -M 1 -D DF1D -F conf_script/configuration_B0_20150506.txt -X config_files/USA15_transceier_configuration_orig.txt

# enable xoff from DOWNSTRAM (e.g. df)
test_im -D DF1D -M 8 -F 0 -N 0 -V 0
test_im -D DF1D -M 8 -F 0 -N 1 -V 0
test_im -D DF1D -M 8 -F 1 -N 0 -V 0
test_im -D DF1D -M 8 -F 1 -N 1 -V 0
test_im -D DF1D -M 8 -F 2 -N 0 -V 0
test_im -D DF1D -M 8 -F 2 -N 1 -V 0
test_im -D DF1D -M 8 -F 3 -N 0 -V 0
test_im -D DF1D -M 8 -F 3 -N 1 -V 0
test_im -D DF1D -M 8 -F 4 -N 0 -V 0
test_im -D DF1D -M 8 -F 4 -N 1 -V 0
test_im -D DF1D -M 8 -F 5 -N 0 -V 0
test_im -D DF1D -M 8 -F 5 -N 1 -V 0
test_im -D DF1D -M 8 -F 6 -N 0 -V 0
test_im -D DF1D -M 8 -F 6 -N 1 -V 0
test_im -D DF1D -M 8 -F 7 -N 0 -V 0
test_im -D DF1D -M 8 -F 7 -N 1 -V 0


# enable SLINK 0; slink, 1: pseudo
test_im -D DF1D -M 9 -F 0 -N 0 -V 0
test_im -D DF1D -M 9 -F 0 -N 1 -V 0
test_im -D DF1D -M 9 -F 1 -N 0 -V 0
test_im -D DF1D -M 9 -F 1 -N 1 -V 0
test_im -D DF1D -M 9 -F 2 -N 0 -V 0
test_im -D DF1D -M 9 -F 2 -N 1 -V 0
test_im -D DF1D -M 9 -F 3 -N 0 -V 0
test_im -D DF1D -M 9 -F 3 -N 1 -V 0
test_im -D DF1D -M 9 -F 4 -N 0 -V 0
test_im -D DF1D -M 9 -F 4 -N 1 -V 0
test_im -D DF1D -M 9 -F 5 -N 0 -V 0
test_im -D DF1D -M 9 -F 5 -N 1 -V 0
test_im -D DF1D -M 9 -F 6 -N 0 -V 0
test_im -D DF1D -M 9 -F 6 -N 1 -V 0
test_im -D DF1D -M 9 -F 7 -N 0 -V 0
test_im -D DF1D -M 9 -F 7 -N 1 -V 0

# check df
test_df -D DF1D -M 2
echo "check the results by "
echo "test_df -D DF1D -M 2"
