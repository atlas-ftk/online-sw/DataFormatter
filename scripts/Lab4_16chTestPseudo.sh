# download LUT
echo "ch0 LUT Downloading..... "
test_im.exe -D DF4 -M 5 -F 0 -N 0 -T input_LUT/FakeLUT/FakeLUT_ch00.txt > /dev/null
echo "ch1 LUT Downloading..... "
test_im.exe -D DF4 -M 5 -F 0 -N 1 -T input_LUT/FakeLUT/FakeLUT_ch01.txt > /dev/null
echo "ch2 LUT Downloading..... "
test_im.exe -D DF4 -M 5 -F 1 -N 0 -T input_LUT/FakeLUT/FakeLUT_ch02.txt > /dev/null
echo "ch3 LUT Downloading..... "
test_im.exe -D DF4 -M 5 -F 1 -N 1 -T input_LUT/FakeLUT/FakeLUT_ch03.txt > /dev/null
echo "ch4 LUT Downloading..... "
test_im.exe -D DF4 -M 5 -F 2 -N 0 -T input_LUT/FakeLUT/FakeLUT_ch04.txt > /dev/null
echo "ch5 LUT Downloading..... "
test_im.exe -D DF4 -M 5 -F 2 -N 1 -T input_LUT/FakeLUT/FakeLUT_ch05.txt > /dev/null
echo "ch6 LUT Downloading..... "
test_im.exe -D DF4 -M 5 -F 3 -N 0 -T input_LUT/FakeLUT/FakeLUT_ch06.txt > /dev/null
echo "ch7 LUT Downloading..... "
test_im.exe -D DF4 -M 5 -F 3 -N 1 -T input_LUT/FakeLUT/FakeLUT_ch07.txt > /dev/null
echo "ch8 LUT Downloading..... "
test_im.exe -D DF4 -M 5 -F 4 -N 0 -T input_LUT/FakeLUT/FakeLUT_ch08.txt > /dev/null
echo "ch9 LUT Downloading..... "
test_im.exe -D DF4 -M 5 -F 4 -N 1 -T input_LUT/FakeLUT/FakeLUT_ch09.txt > /dev/null
echo "ch10 LUT Downloading..... "
test_im.exe -D DF4 -M 5 -F 5 -N 0 -T input_LUT/FakeLUT/FakeLUT_ch10.txt > /dev/null
echo "ch11 LUT Downloading..... "
test_im.exe -D DF4 -M 5 -F 5 -N 1 -T input_LUT/FakeLUT/FakeLUT_ch11.txt > /dev/null
echo "ch12 LUT Downloading..... "
test_im.exe -D DF4 -M 5 -F 6 -N 0 -T input_LUT/FakeLUT/FakeLUT_ch12.txt > /dev/null
echo "ch13 LUT Downloading..... "
test_im.exe -D DF4 -M 5 -F 6 -N 1 -T input_LUT/FakeLUT/FakeLUT_ch13.txt > /dev/null
echo "ch14 LUT Downloading..... "
test_im.exe -D DF4 -M 5 -F 7 -N 0 -T input_LUT/FakeLUT/FakeLUT_ch14.txt > /dev/null
echo "ch15 LUT Downloading..... "
test_im.exe -D DF4 -M 5 -F 7 -N 1 -T input_LUT/FakeLUT/FakeLUT_ch15.txt > /dev/null 

# full open
test_df.exe -M 1 -D DF4 -F conf_script/configuration.txt -X config_files/transceiver_configuration.txt
# enable xoff from DOWNSTRAM (e.g. df) 0: normal, 1: ignore
test_im.exe -D DF4 -M 8 -F 0 -N 0 -V 0
test_im.exe -D DF4 -M 8 -F 0 -N 1 -V 0 
test_im.exe -D DF4 -M 8 -F 1 -N 0 -V 0
test_im.exe -D DF4 -M 8 -F 1 -N 1 -V 0
test_im.exe -D DF4 -M 8 -F 2 -N 0 -V 0
test_im.exe -D DF4 -M 8 -F 2 -N 1 -V 0
test_im.exe -D DF4 -M 8 -F 3 -N 0 -V 0
test_im.exe -D DF4 -M 8 -F 3 -N 1 -V 0



# enable SLINK 0; slink, 1: pseudo
test_im.exe -D DF4 -M 9 -F 0 -N 0 -V 1
test_im.exe -D DF4 -M 9 -F 0 -N 1 -V 1 
test_im.exe -D DF4 -M 9 -F 1 -N 0 -V 1
test_im.exe -D DF4 -M 9 -F 1 -N 1 -V 1
test_im.exe -D DF4 -M 9 -F 2 -N 0 -V 1
test_im.exe -D DF4 -M 9 -F 2 -N 1 -V 1
test_im.exe -D DF4 -M 9 -F 3 -N 0 -V 1
test_im.exe -D DF4 -M 9 -F 3 -N 1 -V 1






# send pseudo data
 test_im_bt -D DF20 -M 6 -F 0 -N 0 -P 3
 test_im.exe -D DF20 -M 6 -F 0 -N 1 -P 3
 test_im.exe -D DF20 -M 6 -F 1 -N 0 -P 3
 test_im.exe -D DF20 -M 6 -F 1 -N 1 -P 3
 test_im.exe -D DF20 -M 6 -F 2 -N 0 -P 3
 test_im.exe -D DF20 -M 6 -F 2 -N 1 -P 3
 test_im.exe -D DF20 -M 6 -F 3 -N 0 -P 3
 test_im.exe -D DF20 -M 6 -F 3 -N 1 -P 3
 test_im.exe -D DF20 -M 6 -F 4 -N 0 -P 3
 test_im.exe -D DF20 -M 6 -F 4 -N 1 -P 3
 test_im.exe -D DF20 -M 6 -F 5 -N 0 -P 3
 test_im.exe -D DF20 -M 6 -F 5 -N 1 -P 3
 test_im.exe -D DF20 -M 6 -F 6 -N 0 -P 3
 test_im.exe -D DF20 -M 6 -F 6 -N 1 -P 3
 test_im.exe -D DF20 -M 6 -F 7 -N 0 -P 3
 test_im.exe -D DF20 -M 6 -F 7 -N 1 -P 3


#check df
test_df.exe -D DF4 -M 2
echo "check the results by "
echo "test_df.exe -D DF4 -M 2"
