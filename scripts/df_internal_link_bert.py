import os

LaneNumbersInFW = [401, 402, 403, 404, 405, 406, 407, 408, 409, 410, 411, 412]
LaneName = ["F3", "F4", "F5", "F6", "F7", "F8", "F9", "F10", "F11", "F12", "I0", "I1"]

Map_LaneName_Number = {}

for i in range(len(LaneNumbersInFW)):
    Map_LaneName_Number[ LaneName[i] ] = LaneNumbersInFW[i]


def StartTest( board1, board2, board3 =None, board4 =None, board5 =None, board6 = None, board7 = None, board8 = None):

    cmd = "test_df -M 1 -D {!s} -F dummuy_DF_Config.txt -X dummuy_DF_GTX_config.txt".format(board1)
    os.system(cmd)
    os.system(cmd)
    os.system(cmd)
    cmd = "test_df -M 1 -D {!s} -F dummuy_DF_Config.txt -X dummuy_DF_GTX_config.txt".format(board2)
    os.system(cmd)
    os.system(cmd)
    os.system(cmd)
    if board3 != None:
        cmd = "test_df -M 1 -D {!s} -F dummuy_DF_Config.txt -X dummuy_DF_GTX_config.txt".format(board3)
        os.system(cmd)
        os.system(cmd)
        os.system(cmd)
    if board4 != None:
        cmd = "test_df -M 1 -D {!s} -F dummuy_DF_Config.txt -X dummuy_DF_GTX_config.txt".format(board4)
        os.system(cmd)
        os.system(cmd)
        os.system(cmd)
    if board5 != None:
        cmd = "test_df -M 1 -D {!s} -F dummuy_DF_Config.txt -X dummuy_DF_GTX_config.txt".format(board5)
        os.system(cmd)
        os.system(cmd)
        os.system(cmd)
    if board6 != None:
        cmd = "test_df -M 1 -D {!s} -F dummuy_DF_Config.txt -X dummuy_DF_GTX_config.txt".format(board6)
        os.system(cmd)
        os.system(cmd)
        os.system(cmd)
    if board7 != None:
        cmd = "test_df -M 1 -D {!s} -F dummuy_DF_Config.txt -X dummuy_DF_GTX_config.txt".format(board7)
        os.system(cmd)
        os.system(cmd)
        os.system(cmd)
    if board8 != None:
        cmd = "test_df -M 1 -D {!s} -F dummuy_DF_Config.txt -X dummuy_DF_GTX_config.txt".format(board8)
        os.system(cmd)
        os.system(cmd)        
        os.system(cmd)
        
    os.system("sleep 1")
    
    ## open
    cmd = "test_df -D {!s} -M 100 -N reg.internal_link_tx_enable -w -V 0X00FFFFFF".format(board1)
    os.system(cmd)
    cmd = "test_df -D {!s} -M 100 -N reg.internal_link_tx_enable -w -V 0X00FFFFFF".format(board2)
    os.system(cmd)
    if board3 != None:
        cmd = "test_df -D {!s} -M 100 -N reg.internal_link_tx_enable -w -V 0X00FFFFFF".format(board3)
        os.system(cmd)
    if board4 != None:
        cmd = "test_df -D {!s} -M 100 -N reg.internal_link_tx_enable -w -V 0X00FFFFFF".format(board4)
        os.system(cmd)
    if board5 != None:
        cmd = "test_df -D {!s} -M 100 -N reg.internal_link_tx_enable -w -V 0X00FFFFFF".format(board5)
        os.system(cmd)
    if board6 != None:
        cmd = "test_df -D {!s} -M 100 -N reg.internal_link_tx_enable -w -V 0X00FFFFFF".format(board6)
        os.system(cmd)
    if board7 != None:
        cmd = "test_df -D {!s} -M 100 -N reg.internal_link_tx_enable -w -V 0X00FFFFFF".format(board7)
        os.system(cmd)
    if board8 != None:
        cmd = "test_df -D {!s} -M 100 -N reg.internal_link_tx_enable -w -V 0X00FFFFFF".format(board8)
        os.system(cmd)
        


    ## open
    cmd = "test_df -D {!s} -M 100 -N reg.internal_link_rx_enable -w -V 0X00FFFFFF".format(board1)
    os.system(cmd)
    cmd = "test_df -D {!s} -M 100 -N reg.internal_link_rx_enable -w -V 0X00FFFFFF".format(board2)
    os.system(cmd)
    if board3 != None:
        cmd = "test_df -D {!s} -M 100 -N reg.internal_link_rx_enable -w -V 0X00FFFFFF".format(board3)
        os.system(cmd)
    if board4 != None:
        cmd = "test_df -D {!s} -M 100 -N reg.internal_link_rx_enable -w -V 0X00FFFFFF".format(board4)
        os.system(cmd)
    if board5 != None:
        cmd = "test_df -D {!s} -M 100 -N reg.internal_link_rx_enable -w -V 0X00FFFFFF".format(board5)
        os.system(cmd)
    if board6 != None:
        cmd = "test_df -D {!s} -M 100 -N reg.internal_link_rx_enable -w -V 0X00FFFFFF".format(board6)
        os.system(cmd)
    if board7 != None:
        cmd = "test_df -D {!s} -M 100 -N reg.internal_link_rx_enable -w -V 0X00FFFFFF".format(board7)
        os.system(cmd)
    if board8 != None:
        cmd = "test_df -D {!s} -M 100 -N reg.internal_link_rx_enable -w -V 0X00FFFFFF".format(board8)
        os.system(cmd)


    ## reset counter for BERT at rx side
    cmd = "test_df -D {!s} -M 100 -N reg.reset.internal_link_bert_counter_reset -w -V 0X1".format(board1)
    os.system(cmd)
    cmd = "test_df -D {!s} -M 100 -N reg.reset.internal_link_bert_counter_reset -w -V 0X1".format(board2)
    os.system(cmd)
    if board3 != None:
        cmd = "test_df -D {!s} -M 100 -N reg.reset.internal_link_bert_counter_reset -w -V 0X1".format(board3)
        os.system(cmd)
    if board4 != None:
        cmd = "test_df -D {!s} -M 100 -N reg.reset.internal_link_bert_counter_reset -w -V 0X1".format(board4)
        os.system(cmd)
    if board5 != None:
        cmd = "test_df -D {!s} -M 100 -N reg.reset.internal_link_bert_counter_reset -w -V 0X1".format(board5)
        os.system(cmd)
    if board6 != None:
        cmd = "test_df -D {!s} -M 100 -N reg.reset.internal_link_bert_counter_reset -w -V 0X1".format(board6)
        os.system(cmd)
    if board7 != None:
        cmd = "test_df -D {!s} -M 100 -N reg.reset.internal_link_bert_counter_reset -w -V 0X1".format(board7)
        os.system(cmd)
    if board8 != None:
        cmd = "test_df -D {!s} -M 100 -N reg.reset.internal_link_bert_counter_reset -w -V 0X1".format(board8)
        os.system(cmd)


    ## gurantee there is no pseudo data being sent yet
    cmd = "test_df -D {!s} -M 100 -N reg.internal_link_tx_enable_bert -w -V 0X00000000".format(board1)
    os.system(cmd)
    cmd = "test_df -D {!s} -M 100 -N reg.internal_link_tx_enable_bert -w -V 0X00000000".format(board2)
    os.system(cmd)
    if board3 != None:
        cmd = "test_df -D {!s} -M 100 -N reg.internal_link_tx_enable_bert -w -V 0X00000000".format(board3)
        os.system(cmd)
    if board4 != None:
        cmd = "test_df -D {!s} -M 100 -N reg.internal_link_tx_enable_bert -w -V 0X00000000".format(board4)
        os.system(cmd)
    if board5 != None:
        cmd = "test_df -D {!s} -M 100 -N reg.internal_link_tx_enable_bert -w -V 0X00000000".format(board5)
        os.system(cmd)
    if board6 != None:
        cmd = "test_df -D {!s} -M 100 -N reg.internal_link_tx_enable_bert -w -V 0X00000000".format(board6)
        os.system(cmd)
    if board7 != None:
        cmd = "test_df -D {!s} -M 100 -N reg.internal_link_tx_enable_bert -w -V 0X00000000".format(board7)
        os.system(cmd)
    if board8 != None:
        cmd = "test_df -D {!s} -M 100 -N reg.internal_link_tx_enable_bert -w -V 0X00000000".format(board8)
        os.system(cmd)


    ## stop data transfer to main logic at receiver side
    cmd = "test_df -D {!s} -M 100 -N reg.internal_link_rx_enable_bert -w -V 0X00FFFFFF".format(board1)
    os.system(cmd)
    cmd = "test_df -D {!s} -M 100 -N reg.internal_link_rx_enable_bert -w -V 0X00FFFFFF".format(board2)
    os.system(cmd)
    if board3 != None:
        cmd = "test_df -D {!s} -M 100 -N reg.internal_link_rx_enable_bert -w -V 0X00FFFFFF".format(board3)
        os.system(cmd)
    if board4 != None:
        cmd = "test_df -D {!s} -M 100 -N reg.internal_link_rx_enable_bert -w -V 0X00FFFFFF".format(board4)
        os.system(cmd)
    if board5 != None:
        cmd = "test_df -D {!s} -M 100 -N reg.internal_link_rx_enable_bert -w -V 0X00FFFFFF".format(board5)
        os.system(cmd)
    if board6 != None:
        cmd = "test_df -D {!s} -M 100 -N reg.internal_link_rx_enable_bert -w -V 0X00FFFFFF".format(board6)
        os.system(cmd)
    if board7 != None:
        cmd = "test_df -D {!s} -M 100 -N reg.internal_link_rx_enable_bert -w -V 0X00FFFFFF".format(board7)
        os.system(cmd)
    if board8 != None:
        cmd = "test_df -D {!s} -M 100 -N reg.internal_link_rx_enable_bert -w -V 0X00FFFFFF".format(board8)
        os.system(cmd)

    

    ## start pseudo data being sent
    cmd = "test_df -D {!s} -M 100 -N reg.internal_link_tx_enable_bert -w -V 0X00FFFFFF".format(board1)
    os.system(cmd)
    cmd = "test_df -D {!s} -M 100 -N reg.internal_link_tx_enable_bert -w -V 0X00FFFFFF".format(board2)
    os.system(cmd)
    os.system("sleep 1")
    if board3 != None:
        cmd = "test_df -D {!s} -M 100 -N reg.internal_link_tx_enable_bert -w -V 0X00FFFFFF".format(board3)
        os.system(cmd)
    if board4 != None:
        cmd = "test_df -D {!s} -M 100 -N reg.internal_link_tx_enable_bert -w -V 0X00FFFFFF".format(board4)
        os.system(cmd)
    if board5 != None:
        cmd = "test_df -D {!s} -M 100 -N reg.internal_link_tx_enable_bert -w -V 0X00FFFFFF".format(board5)
        os.system(cmd)
    if board6 != None:
        cmd = "test_df -D {!s} -M 100 -N reg.internal_link_tx_enable_bert -w -V 0X00FFFFFF".format(board6)
        os.system(cmd)
    if board7 != None:
        cmd = "test_df -D {!s} -M 100 -N reg.internal_link_tx_enable_bert -w -V 0X00FFFFFF".format(board7)
        os.system(cmd)
    if board8 != None:
        cmd = "test_df -D {!s} -M 100 -N reg.internal_link_tx_enable_bert -w -V 0X00FFFFFF".format(board8)
        os.system(cmd)


    ## start BERT counter
    cmd = "test_df -D {!s} -M 100 -N reg.reset.internal_link_bert_counter_reset -w -V 0X0".format(board1)
    os.system(cmd)
    cmd = "test_df -D {!s} -M 100 -N reg.reset.internal_link_bert_counter_reset -w -V 0X0".format(board2)
    os.system(cmd)
    if board3 != None:
        cmd = "test_df -D {!s} -M 100 -N reg.reset.internal_link_bert_counter_reset -w -V 0X0".format(board3)
        os.system(cmd)
    if board4 != None:
        cmd = "test_df -D {!s} -M 100 -N reg.reset.internal_link_bert_counter_reset -w -V 0X0".format(board4)
        os.system(cmd)
    if board5 != None:
        cmd = "test_df -D {!s} -M 100 -N reg.reset.internal_link_bert_counter_reset -w -V 0X0".format(board5)
        os.system(cmd)
    if board6 != None:
        cmd = "test_df -D {!s} -M 100 -N reg.reset.internal_link_bert_counter_reset -w -V 0X0".format(board6)
        os.system(cmd)
    if board7 != None:
        cmd = "test_df -D {!s} -M 100 -N reg.reset.internal_link_bert_counter_reset -w -V 0X0".format(board7)
        os.system(cmd)
    if board8 != None:
        cmd = "test_df -D {!s} -M 100 -N reg.reset.internal_link_bert_counter_reset -w -V 0X0".format(board8)
        os.system(cmd)


def CheckResults(board, rx_ch_s):
    
    rx_ch_num = Map_LaneName_Number[ rx_ch_s]

    os.system( "echo -------------------------------------")
    os.system( "echo Board "+board)
    os.system( "echo rx channel name "+ rx_ch_s)
    os.system( "echo -------------------------------------")

    rx_ch0 = str(rx_ch_num)
    rx_ch1 = str(rx_ch_num+12)

    os.system("echo Total Bits Received Ch0")
    cmd = "test_df -D {!s} -M 6 -T 1 -L {!s}".format(board, rx_ch0)
    os.system(cmd)

    os.system("echo Error Bits Received Ch0")
    cmd = "test_df -D {!s} -M 6 -T 0 -L {!s}".format(board, rx_ch0)
    os.system(cmd)

    os.system("echo Total Bits Received Ch1")
    cmd = "test_df -D {!s} -M 6 -T 1 -L {!s}".format(board, rx_ch1)
    os.system(cmd)

    os.system("echo Error Bits Received Ch1")
    cmd = "test_df -D {!s} -M 6 -T 0 -L {!s}".format(board, rx_ch1)
    os.system(cmd)

#Boards = ["DF-01-03", "DF-01-04", "DF-01-05", "DF-01-06", "DF-01-07", "DF-01-08", "DF-01-09", "DF-01-10"]
Boards = ["DF-02-03", "DF-02-04", "DF-02-05", "DF-02-06", "DF-02-07", "DF-02-08", "DF-02-09", "DF-02-10"]
Lanes = ["F3", "F4", "F5", "F6", "F7", "F8", "F9"]

StartTest("DF-02-03", "DF-02-04", "DF-02-05", "DF-02-06", "DF-02-07", "DF-02-08", "DF-02-09", "DF-02-10")

os.system("sleep 20")

for board in Boards:
    for lane in Lanes:
        CheckResults(board, lane) 
