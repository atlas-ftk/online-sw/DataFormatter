#!/bin/bash

if [ $# != 2 ];  then
    echo "usage :nohup bash  USA15_PeriodicMonitoring.sh  [description] [how long?(hours)]"
    echo "e.g. \nohup bash $USA15_PeriodicMonitoring.sh testrun_0 6 "
    echo "ERROR  2 argument required"
    exit 1
fi


DIR="Monitoring_outputs"

DF="DF1D"

mkdir -p $DIR

i="0"
while [ $i  -lt ${2} ]
do
TIME=`date "+%Y%m%d%H%M"`
echo "ch00 processing......." ${TIME}
echo "--- ch00 ---" > ${DIR}/imsmonall_${1}_h${i}_${TIME}.dat
test_im -D ${DF} -M 1 -F 0
test_im -D ${DF} -M 4 -F 0 -N 0 >> ${DIR}/imsmonall_${1}_h${i}_${TIME}.dat
echo "ch01 processing......." ${TIME}
echo "--- ch01 ---" >> ${DIR}/imsmonall_${1}_h${i}_${TIME}.dat
test_im -D ${DF} -M 1 -F 0
test_im -D ${DF} -M 4 -F 0 -N 1 >> ${DIR}/imsmonall_${1}_h${i}_${TIME}.dat
echo "ch02 processing......." ${TIME}
echo "--- ch02 ---" >> ${DIR}/imsmonall_${1}_h${i}_${TIME}.dat
test_im -D ${DF} -M 1 -F 1
test_im -D ${DF} -M 4 -F 1 -N 0 >> ${DIR}/imsmonall_${1}_h${i}_${TIME}.dat
echo "ch03 processing......." ${TIME}
echo "--- ch03 ---" >> ${DIR}/imsmonall_${1}_h${i}_${TIME}.dat
test_im -D ${DF} -M 1 -F 1
test_im -D ${DF} -M 4 -F 1 -N 1 >> ${DIR}/imsmonall_${1}_h${i}_${TIME}.dat
echo "ch04 processing......." ${TIME}
echo "--- ch04 ---" >> ${DIR}/imsmonall_${1}_h${i}_${TIME}.dat
test_im -D ${DF} -M 1 -F 2
test_im -D ${DF} -M 4 -F 2 -N 0 >> ${DIR}/imsmonall_${1}_h${i}_${TIME}.dat
echo "ch05 processing......." ${TIME}
echo "--- ch05 ---" >> ${DIR}/imsmonall_${1}_h${i}_${TIME}.dat
test_im -D ${DF} -M 1 -F 2
test_im -D ${DF} -M 4 -F 2 -N 1 >> ${DIR}/imsmonall_${1}_h${i}_${TIME}.dat
echo "ch06 processing......." ${TIME}
echo "--- ch06 ---" >> ${DIR}/imsmonall_${1}_h${i}_${TIME}.dat
test_im -D ${DF} -M 1 -F 3
test_im -D ${DF} -M 4 -F 3 -N 0 >> ${DIR}/imsmonall_${1}_h${i}_${TIME}.dat
echo "ch07 processing......." ${TIME}
echo "--- ch07 ---" >> ${DIR}/imsmonall_${1}_h${i}_${TIME}.dat
test_im -D ${DF} -M 1 -F 3
test_im -D ${DF} -M 4 -F 3 -N 1 >> ${DIR}/imsmonall_${1}_h${i}_${TIME}.dat
echo "ch08 processing......." ${TIME}
echo "--- ch08 ---" >> ${DIR}/imsmonall_${1}_h${i}_${TIME}.dat
test_im -D ${DF} -M 1 -F 4
test_im -D ${DF} -M 4 -F 4 -N 0 >> ${DIR}/imsmonall_${1}_h${i}_${TIME}.dat
echo "ch09 processing......." ${TIME}
echo "--- ch09 ---" >> ${DIR}/imsmonall_${1}_h${i}_${TIME}.dat
test_im -D ${DF} -M 1 -F 4
test_im -D ${DF} -M 4 -F 4 -N 1 >> ${DIR}/imsmonall_${1}_h${i}_${TIME}.dat
echo "ch10 processing......." ${TIME}
echo "--- ch10 ---" >> ${DIR}/imsmonall_${1}_h${i}_${TIME}.dat
test_im -D ${DF} -M 1 -F 5
test_im -D ${DF} -M 4 -F 5 -N 0 >> ${DIR}/imsmonall_${1}_h${i}_${TIME}.dat
echo "ch11 processing......." ${TIME}
echo "--- ch11 ---" >> ${DIR}/imsmonall_${1}_h${i}_${TIME}.dat
test_im -D ${DF} -M 1 -F 5
test_im -D ${DF} -M 4 -F 5 -N 1 >> ${DIR}/imsmonall_${1}_h${i}_${TIME}.dat
echo "ch12 processing......." ${TIME}
echo "--- ch12 ---" >> ${DIR}/imsmonall_${1}_h${i}_${TIME}.dat
test_im -D ${DF} -M 1 -F 6
test_im -D ${DF} -M 4 -F 6 -N 0 >> ${DIR}/imsmonall_${1}_h${i}_${TIME}.dat
echo "ch13 processing......." ${TIME}
echo "--- ch13 ---" >> ${DIR}/imsmonall_${1}_h${i}_${TIME}.dat
test_im -D ${DF} -M 1 -F 6
test_im -D ${DF} -M 4 -F 6 -N 1 >> ${DIR}/imsmonall_${1}_h${i}_${TIME}.dat
echo "ch14 processing......." ${TIME}
echo "--- ch14 ---" >> ${DIR}/imsmonall_${1}_h${i}_${TIME}.dat
test_im -D ${DF} -M 1 -F 7
test_im -D ${DF} -M 4 -F 7 -N 0 >> ${DIR}/imsmonall_${1}_h${i}_${TIME}.dat
echo "ch15 processing......." ${TIME}
echo "--- ch15 ---" >> ${DIR}/imsmonall_${1}_h${i}_${TIME}.dat
test_im -D ${DF} -M 1 -F 7
test_im -D ${DF} -M 4 -F 7 -N 1 >> ${DIR}/imsmonall_${1}_h${i}_${TIME}.dat
sleep 1800 # 30 min

TIME=`date "+%Y%m%d%H%M"`

echo "ch00 processing......." ${TIME}
echo "--- ch00 ---" > ${DIR}/imsmonall_${1}_h${i}_${TIME}.dat
test_im -D ${DF} -M 1 -F 0
test_im -D ${DF} -M 4 -F 0 -N 0 >> ${DIR}/imsmonall_${1}_h${i}_${TIME}.dat
echo "ch01 processing......." ${TIME}
echo "--- ch01 ---" >> ${DIR}/imsmonall_${1}_h${i}_${TIME}.dat
test_im -D ${DF} -M 1 -F 0
test_im -D ${DF} -M 4 -F 0 -N 1 >> ${DIR}/imsmonall_${1}_h${i}_${TIME}.dat
echo "ch02 processing......." ${TIME}
echo "--- ch02 ---" >> ${DIR}/imsmonall_${1}_h${i}_${TIME}.dat
test_im -D ${DF} -M 1 -F 1
test_im -D ${DF} -M 4 -F 1 -N 0 >> ${DIR}/imsmonall_${1}_h${i}_${TIME}.dat
echo "ch03 processing......." ${TIME}
echo "--- ch03 ---" >> ${DIR}/imsmonall_${1}_h${i}_${TIME}.dat
test_im -D ${DF} -M 1 -F 1
test_im -D ${DF} -M 4 -F 1 -N 1 >> ${DIR}/imsmonall_${1}_h${i}_${TIME}.dat
echo "ch04 processing......." ${TIME}
echo "--- ch04 ---" >> ${DIR}/imsmonall_${1}_h${i}_${TIME}.dat
test_im -D ${DF} -M 1 -F 2
test_im -D ${DF} -M 4 -F 2 -N 0 >> ${DIR}/imsmonall_${1}_h${i}_${TIME}.dat
echo "ch05 processing......." ${TIME}
echo "--- ch05 ---" >> ${DIR}/imsmonall_${1}_h${i}_${TIME}.dat
test_im -D ${DF} -M 1 -F 2
test_im -D ${DF} -M 4 -F 2 -N 1 >> ${DIR}/imsmonall_${1}_h${i}_${TIME}.dat
echo "ch06 processing......." ${TIME}
echo "--- ch06 ---" >> ${DIR}/imsmonall_${1}_h${i}_${TIME}.dat
test_im -D ${DF} -M 1 -F 3
test_im -D ${DF} -M 4 -F 3 -N 0 >> ${DIR}/imsmonall_${1}_h${i}_${TIME}.dat
echo "ch07 processing......." ${TIME}
echo "--- ch07 ---" >> ${DIR}/imsmonall_${1}_h${i}_${TIME}.dat
test_im -D ${DF} -M 1 -F 3
test_im -D ${DF} -M 4 -F 3 -N 1 >> ${DIR}/imsmonall_${1}_h${i}_${TIME}.dat
echo "ch08 processing......." ${TIME}
echo "--- ch08 ---" >> ${DIR}/imsmonall_${1}_h${i}_${TIME}.dat
test_im -D ${DF} -M 1 -F 4
test_im -D ${DF} -M 4 -F 4 -N 0 >> ${DIR}/imsmonall_${1}_h${i}_${TIME}.dat
echo "ch09 processing......." ${TIME}
echo "--- ch09 ---" >> ${DIR}/imsmonall_${1}_h${i}_${TIME}.dat
test_im -D ${DF} -M 1 -F 4
test_im -D ${DF} -M 4 -F 4 -N 1 >> ${DIR}/imsmonall_${1}_h${i}_${TIME}.dat
echo "ch10 processing......." ${TIME}
echo "--- ch10 ---" >> ${DIR}/imsmonall_${1}_h${i}_${TIME}.dat
test_im -D ${DF} -M 1 -F 5
test_im -D ${DF} -M 4 -F 5 -N 0 >> ${DIR}/imsmonall_${1}_h${i}_${TIME}.dat
echo "ch11 processing......." ${TIME}
echo "--- ch11 ---" >> ${DIR}/imsmonall_${1}_h${i}_${TIME}.dat
test_im -D ${DF} -M 1 -F 5
test_im -D ${DF} -M 4 -F 5 -N 1 >> ${DIR}/imsmonall_${1}_h${i}_${TIME}.dat
echo "ch12 processing......." ${TIME}
echo "--- ch12 ---" >> ${DIR}/imsmonall_${1}_h${i}_${TIME}.dat
test_im -D ${DF} -M 1 -F 6
test_im -D ${DF} -M 4 -F 6 -N 0 >> ${DIR}/imsmonall_${1}_h${i}_${TIME}.dat
echo "ch13 processing......." ${TIME}
echo "--- ch13 ---" >> ${DIR}/imsmonall_${1}_h${i}_${TIME}.dat
test_im -D ${DF} -M 1 -F 6
test_im -D ${DF} -M 4 -F 6 -N 1 >> ${DIR}/imsmonall_${1}_h${i}_${TIME}.dat
echo "ch14 processing......." ${TIME}
echo "--- ch14 ---" >> ${DIR}/imsmonall_${1}_h${i}_${TIME}.dat
test_im -D ${DF} -M 1 -F 7
test_im -D ${DF} -M 4 -F 7 -N 0 >> ${DIR}/imsmonall_${1}_h${i}_${TIME}.dat
echo "ch15 processing......." ${TIME}
echo "--- ch15 ---" >> ${DIR}/imsmonall_${1}_h${i}_${TIME}.dat
test_im -D ${DF} -M 1 -F 7
test_im -D ${DF} -M 4 -F 7 -N 1 >> ${DIR}/imsmonall_${1}_h${i}_${TIME}.dat
sleep 1800 # 30 min

i=$[$i+1]
done