d=$1

fw=$(python -c "import os,sys; print os.popen('test_df -D $d -M 21').read().split(' ')[-2][-6:-1];")

test_df -D $d -M 100 -r -N reg.b0f_timeout_threshold
test_df -D $d -M 100 -r -N reg.input_b0f_skew
test_df -D $d -M 100 -r -N reg.global_l1id
test_df -D $d -M 100 -w -N reg.slink_counter_selected -V 0Xf
test_df -D $d -M 100 -r -N reg.slink_total_evts_given_up
test_df -D $d -M 100 -r -N reg.slink_evtsorting_counter
if [ $fw == "1803b" ] || [ $fw == "1803c" ]; then
  test_df -D $d -M 100 -r -N reg.input_word_limit_per_module
  test_df -D $d -M 100 -r -N reg.input_unknown_module_header_word
fi

echo '--------------------------Error Registers-------------------------------------------------'

if [ $fw != "1803a" ]; then
  test_df -D $d -M 100 -r -N reg.l1id_out_of_sync
fi
test_df -D $d -M 100 -r -N reg.input_b0f_timeout_links
if [ $fw != "18038" ]; then
  test_df -D $d -M 100 -r -N reg.input_packet_error.8th_word_error_links
  test_df -D $d -M 100 -r -N reg.input_packet_error.other_error_links
  test_df -D $d -M 100 -r -N reg.input_l1id_ecr_jump
else
  test_df -D $d -M 100 -r -N reg.input_packet_error
  test_df -D $d -M 100 -r -N reg.input_packet_eighth_word_error
fi
if [ $fw == "1803a" ] || [ $fw == "1803b" ] || [ $fw == "1803c" ]; then
  test_df -D $d -M 100 -r -N reg.input_unknown_module_header_seen
fi

if [ $fw == "18039" ]; then
  echo '--------------------------Lane Reenabled counts--------------------------------------'
  test_df -D $d -M 100 -r -N reg.input_channel_reenabled
  test_df -D $d -M 100 -r -N reg.input_link_reenabled_count_0_2.ch0
  test_df -D $d -M 100 -r -N reg.input_link_reenabled_count_0_2.ch1
  test_df -D $d -M 100 -r -N reg.input_link_reenabled_count_0_2.ch2
  test_df -D $d -M 100 -r -N reg.input_link_reenabled_count_3_5.ch3
  test_df -D $d -M 100 -r -N reg.input_link_reenabled_count_3_5.ch4
  test_df -D $d -M 100 -r -N reg.input_link_reenabled_count_3_5.ch5
  test_df -D $d -M 100 -r -N reg.input_link_reenabled_count_6_8.ch6
  test_df -D $d -M 100 -r -N reg.input_link_reenabled_count_6_8.ch7
  test_df -D $d -M 100 -r -N reg.input_link_reenabled_count_6_8.ch8
  test_df -D $d -M 100 -r -N reg.input_link_reenabled_count_9_11.ch9
  test_df -D $d -M 100 -r -N reg.input_link_reenabled_count_9_11.ch10
  test_df -D $d -M 100 -r -N reg.input_link_reenabled_count_9_11.ch11
  test_df -D $d -M 100 -r -N reg.input_link_reenabled_count_12_14.ch12
  test_df -D $d -M 100 -r -N reg.input_link_reenabled_count_12_14.ch13
  test_df -D $d -M 100 -r -N reg.input_link_reenabled_count_12_14.ch14
  test_df -D $d -M 100 -r -N reg.input_link_reenabled_count_15.ch15
fi

if [ $fw == "1803a" ] || [ $fw == "1803b" ] || [ $fw == "1803c" ]; then
  if [[ $# -gt 1 ]]; then
  echo '-----------------------------Lane counts--------------------------------------------------'
  for lane in $(seq 0 15); do
    echo "---Lane ${lane}---"
    test_df -D $d -M 100 -w -N reg.input_lane_monitor_selector -V $lane
    test_df -D $d -M 100 -r -N reg.input_lane_monitor_b0f_timeout_count
    test_df -D $d -M 100 -r -N reg.input_lane_monitor_l1id_timeout_count
    test_df -D $d -M 100 -r -N reg.input_lane_monitor_number_modules_hit_word_limit
    if [ $fw == "1803c" ]; then
      test_df -D $d -M 100 -r -N reg.input_lane_monitor_packet_structure_error_count
      test_df -D $d -M 100 -r -N reg.input_lane_monitor_packet_eighth_word_error_count
    fi
  done
  fi
fi

