


# full ch oen config file
#test_df -M 1 -D DF1D -F conf_script/USA15_configuration.txt -X config_files/USA15_transceier_configuration_orig.txt
test_df -M 1 -D DF1D -F conf_script/configuration_B0_20150506.txt -X config_files/USA15_transceier_configuration_orig.txt

# enable xoff from DOWNSTRAM (e.g. df)
test_im -D DF1D -M 8 -F 0 -N 0 -V 0
test_im -D DF1D -M 8 -F 0 -N 1 -V 0
test_im -D DF1D -M 8 -F 1 -N 0 -V 0
test_im -D DF1D -M 8 -F 1 -N 1 -V 0
test_im -D DF1D -M 8 -F 2 -N 0 -V 0
test_im -D DF1D -M 8 -F 2 -N 1 -V 0
test_im -D DF1D -M 8 -F 3 -N 0 -V 0
test_im -D DF1D -M 8 -F 3 -N 1 -V 0


# enable SLINK 0; slink, 1: pseudo
test_im -D DF1D -M 9 -F 0 -N 0 -V 0
test_im -D DF1D -M 9 -F 0 -N 1 -V 0
test_im -D DF1D -M 9 -F 1 -N 0 -V 0
test_im -D DF1D -M 9 -F 1 -N 1 -V 0
test_im -D DF1D -M 9 -F 2 -N 0 -V 0
test_im -D DF1D -M 9 -F 2 -N 1 -V 0
test_im -D DF1D -M 9 -F 3 -N 0 -V 0
test_im -D DF1D -M 9 -F 3 -N 1 -V 0

# check df
test_df -D DF1D -M 2
echo "check the results by "
echo "test_df -D DF1D -M 2"
