import os
from time import time

ISPSEUDODATA = True
#ISPSEUDODATA = False

DFSYSTEMCONFIG= "config/code/config_txt_files/df_board_crate1_config_07_14_2016.txt"
DFLOGICNUMBER = {}

##check if temporary file storage fold is there
if not os.path.exists("config/GTXConfigs_tmp"):
    cmd = "mkdir config/GTXConfigs_tmp"
    os.system(cmd)
    
if not os.path.exists("config/Configs_tmp"):
    cmd = "mkdir config/Configs_tmp"
    os.system(cmd)

## Lab 4 boards emulating shelf 1 ins USA 15
DFLOGICNUMBER["DF24"] = "8"
DFLOGICNUMBER["DF25"] = "24"
DFLOGICNUMBER["DF26"] = "28"

### USA 15 boards
DFLOGICNUMBER["DF-01-03"] = "0"
DFLOGICNUMBER["DF-01-04"] = "4"
DFLOGICNUMBER["DF-01-05"] = "8"
DFLOGICNUMBER["DF-01-06"] = "12"
DFLOGICNUMBER["DF-01-07"] = "16"
DFLOGICNUMBER["DF-01-08"] = "20"
DFLOGICNUMBER["DF-01-09"] = "24"
DFLOGICNUMBER["DF-01-10"] = "28"

DFLOGICNUMBER["DF-02-03"] = "1"
DFLOGICNUMBER["DF-02-04"] = "5"
DFLOGICNUMBER["DF-02-05"] = "9"
DFLOGICNUMBER["DF-02-06"] = "13"
DFLOGICNUMBER["DF-02-07"] = "17"
DFLOGICNUMBER["DF-02-08"] = "21"
DFLOGICNUMBER["DF-02-09"] = "25"
DFLOGICNUMBER["DF-02-10"] = "29"


BOARDS = ["DF-01-03","DF-01-04","DF-01-05","DF-01-06","DF-01-07","DF-01-08","DF-01-09","DF-01-10",
          "DF-02-03","DF-02-04","DF-02-05","DF-02-06","DF-02-07","DF-02-08","DF-02-09","DF-02-10",
          "DF3D",
          "DF24", "DF25", "DF26"]

ONEDFCONFIG={}
for board in BOARDS:
    ONEDFCONFIG[board] = "config/Configs_tmp/config_{!s}_1df_32towers.txt".format(board)

TESTBOARDRXCONFIG = {}
TESTBOARDGTXCONFIG = {}
TESTBOARDCONFIG = {}

## shelf 2 6 df
#TESTBOARDS = ["DF-02-05","DF-02-06","DF-02-07","DF-02-08","DF-02-09","DF-02-10"]
#TESTBOARDSRX = ["0X07c07c", "0X07c07c", "0X07c07c", "0X07c07c", "0X07c07c", "0X07c07c" ]
#TESTBOARDSURL = ["0Xa","0Xb","0Xc","0Xd","0Xe","0Xf"]

#TESTBOARDS = ["DF-01-05","DF-01-06","DF-01-07","DF-01-08"]
#TESTBOARDSRX = ["0X01c01c", "0X01c01c", "0X01c01c", "0X01c01c"]
#TESTBOARDSURL = ["0X2","0X3","0X4","0X5"]
#
#TESTBOARDS = ["DF-01-05","DF-01-06"]
#TESTBOARDSRX = ["0X004004", "0X004004"]
#TESTBOARDSURL = ["0X2","0X3"]

#TESTBOARDS = ["DF-02-04","DF-02-05","DF-02-06","DF-02-07","DF-02-08","DF-02-09","DF-02-10"]
#TESTBOARDSRX = ["0X07e07e", "0X07e07e", "0X07e07e", "0X07e07e", "0X07e07e", "0X07e07e", "0X07e07e" ]
#TESTBOARDSURL = ["0X9","0Xa","0Xb","0Xc","0Xd","0Xe","0Xf"]

#TESTBOARDS = ["DF-01-03","DF-01-04"]
#TESTBOARDSRX = ["0X001001", "0X001001"]
#TESTBOARDSURL = ["0X1","0X2"]

#TESTBOARDS = ["DF-01-03","DF-01-04","DF-01-05","DF-01-06"]
#TESTBOARDSRX = ["0X007007", "0X007007", "0X007007", "0X007007"]
#TESTBOARDSURL = ["0X1","0X2","0X3","0X4"]

#TESTBOARDSRX = ["0X070070", "0X070070", "0X070070", "0X070070"]
#TESTBOARDS = ["DF-01-07","DF-01-08","DF-01-09","DF-01-10"]
#TESTBOARDSURL = ["0X5","0X6","0X7","0X8"]

#TESTBOARDS = ["DF-01-03","DF-01-04","DF-01-05","DF-01-06","DF-01-07","DF-01-08","DF-01-09","DF-01-10"]
#TESTBOARDSRX = ["0X07f07f", "0X07f07f", "0X07f07f", "0X07e07e", "0X07f07f", "0X07f07f", "0X07f07f", "0X07f07f" ]
#TESTBOARDSURL = ["0X1","0X2","0X3","0X4","0X5","0X6","0X7","0X8"]

#TESTBOARDS = ["DF-01-03","DF-01-04","DF-01-05","DF-01-06","DF-01-07","DF-01-08","DF-01-09","DF-01-10",
#              "DF-02-03","DF-02-04","DF-02-05","DF-02-06","DF-02-07"]
#TESTBOARDSRX = ["0X07f07f", "0X07f07f", "0X07f07f", "0X07f07f", "0X47f47f", "0X47f47f", "0X47f47f", "0X47f47f",
#                "0X40f40f", "0X40f40f", "0X40f40f", "0X40f40f", "0X00f00f"]
#TESTBOARDSURL = ["0X0","0X1","0X2","0X3","0X4","0X5","0X6","0X7",
#                 "0X8","0X9","0Xa","0Xb","0Xc"]

### shelf-1 5-10
#TESTBOARDS = ["DF-01-05","DF-01-06","DF-01-07","DF-01-08","DF-01-09","DF-01-10"]
#TESTBOARDSRX = ["0X07c07c", "0X07c07c", "0X07c07c", "0X07c07c", "0X07c07c", "0X07c07c" ]
#TESTBOARDSURL = ["0X2","0X3","0X4","0X5","0X6","0X7"]

### shelf-1 4-9
#TESTBOARDS = ["DF-01-04","DF-01-05","DF-01-06","DF-01-07","DF-01-08","DF-01-09"]
#TESTBOARDSRX = ["0X03e03e","0X03e03e", "0X03e03e", "0X03e03e", "0X03e03e", "0X03e03e"]
#TESTBOARDSURL = ["0X1","0X2","0X3","0X4","0X5","0X6","0X7"]

### shelf-2 3 5 7 8 9
#TESTBOARDS = ["DF-02-03","DF-02-05","DF-02-07","DF-02-08","DF-02-09"]
#TESTBOARDSRX = ["0X03a03a", "0X039039", "0X035035", "0X035035", "0X035035"]
#TESTBOARDSURL = ["0X8","0Xa","0Xc","0Xd","0Xe"]

### shelf-2 3 5 7 9
#TESTBOARDS = ["DF-02-03","DF-02-05","DF-02-07","DF-02-09"]
#TESTBOARDSRX = ["0X02a02a", "0X029029", "0X025025", "0X025025"]
#TESTBOARDSURL = ["0X8","0Xa","0Xc","0Xe"]

#TESTBOARDS = ["DF-01-05","DF-01-06","DF-01-07","DF-01-08","DF-01-09","DF-01-10",
#              "DF-02-05","DF-02-06","DF-02-07","DF-02-08","DF-02-09","DF-02-10"]
#TESTBOARDSRX = ["0X07c07c", "0X07c07c", "0X07c07c", "0X07c07c", "0Xc7cc7c", "0Xc7cc7c", 
#                "0Xc7cc7c", "0Xc7cc7c", "0X07c07c", "0X07c07c", "0X07c07c", "0X07c07c" ]
#TESTBOARDSURL = ["0X3","0X4","0X5","0X6","0X7","0X8",
#                 "0Xa","0Xb","0Xc","0Xd","0Xe","0Xf"]

#TESTBOARDS    = ["DF-01-05","DF-01-06","DF-01-07","DF-01-08","DF-01-09","DF-01-10",
#                 "DF-02-05","DF-02-06"]
#TESTBOARDSRX  = ["0X07c07c", "0X07c07c", "0X07c07c", "0X07c07c", "0Xc7cc7c", "0Xc7cc7c", 
#                 "0Xc04c04", "0Xc04c04"]
#TESTBOARDSURL = ["0X3","0X4","0X5","0X6","0X7","0X8",
#                 "0Xa","0Xb"]

## shelf-1 9-10 shelf-2 5-10
#TESTBOARDS    = ["DF-01-09","DF-01-10",
#                 "DF-02-05","DF-02-06", "DF-02-07","DF-02-08", "DF-02-09","DF-02-10" ]
#TESTBOARDSRX  = ["0Xc40c40", "0Xc40c40",
#                 "0Xc7cc7c", "0Xc7cc7c","0X07c07c", "0X07c07c", "0X07c07c", "0X07c07c"]
#TESTBOARDSURL = ["0X6","0X7",
#                 "0Xa","0Xb", "0Xc","0Xd", "0Xe","0Xf"]

## shelf-1 9-10 shelf-2 5-8
#TESTBOARDS    = ["DF-01-09","DF-01-10",
#                 "DF-02-05","DF-02-06", "DF-02-07","DF-02-08"]
#TESTBOARDSRX  = ["0X440440", "0X440440",
#                 "0X41c41c", "0X41c41c","0X01c01c", "0X01c01c"]
#TESTBOARDSURL = ["0X6","0X7",
#                 "0Xa","0Xb", "0Xc","0Xd"]

### shelf-1 9-10 shelf-2 5-6
#TESTBOARDS    = ["DF-01-09","DF-01-10",
#                 "DF-02-05","DF-02-06"]
#TESTBOARDSRX  = ["0X440440", "0X440440",
#                 "0X404404", "0X404404"]
#TESTBOARDSURL = ["0X6","0X7",
#                 "0Xa","0Xb"]

## shelf-1 9-10 shelf-2 5-10
#TESTBOARDS    = ["DF-01-05","DF-01-06","DF-01-07","DF-01-08","DF-01-09","DF-01-10",
#                 "DF-02-05","DF-02-06","DF-02-07"]
#TESTBOARDSRX  = ["0X07c07c","0X07c07c","0X07c07c","0X07c07c","0X47c47c","0X47c47c",
#                 "0X40c40c","0X40c40c","0X00c00c"]
#TESTBOARDSURL = ["0X2","0X3","0X4","0X5","0X6","0X7",
#                 "0Xa","0Xb","0Xc"]


#TESTBOARDS    = ["DF-01-09","DF-01-10",
#                 "DF-02-03","DF-02-04","DF-02-05","DF-02-06", "DF-02-07","DF-02-08", "DF-02-09","DF-02-10" ]
#TESTBOARDSRX  = ["0Xc40c40", "0Xc40c40",
#                 "0X07f07f", "0X07f07f","0Xc7cc7c", "0Xc7cc7c","0X07c07c", "0X07c07c", "0X07c07c", "0X07c07c"]
#TESTBOARDSURL = ["0X6","0X7",
#                 "0x8","0x9","0Xa","0Xb", "0Xc","0Xd", "0Xe","0Xf"]

#TESTBOARDS = ["DF-01-06","DF-01-09","DF-01-10",
#              "DF-02-05","DF-02-06"]
#TESTBOARDSRX = ["0X060060", "0Xc48c48", "0Xc48c48", 
#                "0Xc04c04", "0Xc04c04"]
#TESTBOARDSURL = ["0X4","0X7","0X8",
#                 "0Xa","0Xb"]

#TESTBOARDS = ["DF-01-09","DF-02-05"]
#TESTBOARDSRX = ["0Xc00c00", "0Xc00c00"]
#TESTBOARDSURL = ["0X7","0Xa"]
#
#TESTBOARDS = ["DF-01-10","DF-02-06"]
#TESTBOARDSRX = ["0Xc00c00", "0Xc00c00"]
#TESTBOARDSURL = ["0X8","0Xb"]

#TESTBOARDS = ["DF-01-09","DF-01-10","DF-02-05","DF-02-06"]
#TESTBOARDSRX = ["0Xc40c40", "0Xc40c40", "0Xc04c04","0Xc04c04"]
#TESTBOARDSURL = ["0X6","0X7","0Xa","0Xb"]

#TESTBOARDS = ["DF-01-05","DF-01-06","DF-01-07","DF-01-09","DF-02-03","DF-02-05"]
#TESTBOARDSRX = ["0X02c02c", "0X02c02c", "0Xc2cc2c","0Xc1cc1c","0Xc02c02","0Xc01c01"]
#TESTBOARDSURL = ["0X3","0X4","0X5","0X7","0Xa","0Xc"]


## shelf-1 5-9 shelf-2 3
#TESTBOARDS = ["DF-01-05","DF-01-06","DF-01-07","DF-01-08", "DF-01-09", "DF-02-03"]
#TESTBOARDSRX = ["0X03c03c", "0X03c03c", "0Xc3cc3c", "0X03c03c", "0X03c03c", "0Xc00c00"]
#TESTBOARDSURL = ["0X2","0X3","0X4","0X5","0X6","0X8"]

## shelf-1 5-9 shelf-2 3 5 7
#TESTBOARDS = ["DF-01-05","DF-01-06","DF-01-07","DF-01-08", "DF-01-09", "DF-02-03", "DF-02-05", "DF-02-07"]
#TESTBOARDSRX = ["0X03c03c", "0X03c03c", "0Xc3cc3c", "0X03c03c", "0Xc3cc3c", "0Xc0ac0a", "0Xc09c09", "0X005005"]
#TESTBOARDSURL = ["0X2","0X3","0X4","0X5","0X6","0X8","0Xa","0Xc"]

## shelf-1 5-9 
#TESTBOARDS = ["DF-01-05","DF-01-06","DF-01-07","DF-01-08", "DF-01-09"]
#TESTBOARDSRX = ["0X03c03c", "0X03c03c", "0X03c03c", "0X03c03c", "0X03c03c"]
#TESTBOARDSURL = ["0X2","0X3","0X4","0X5","0X6"]

# shelf-2 3-10
#TESTBOARDS = ["DF-02-03","DF-02-04","DF-02-05","DF-02-06","DF-02-07","DF-02-08", "DF-02-09", "DF-02-10"]
#TESTBOARDSRX = ["0X07f07f", "0X07f07f", "0X07f07f", "0X07f07f", "0X07f07f", "0X07f07f", "0X07f07f", "0X07f07f"]
#TESTBOARDSURL = ["0x8","0x9","0Xa","0Xb", "0Xc","0Xd", "0Xe","0Xf"]

# shelf-1 5 6 7 9 10 shelf-2 3
#TESTBOARDS = ["DF-01-05","DF-01-06","DF-01-07","DF-01-09","DF-01-10","DF-02-03"]
#TESTBOARDSRX = ["0X06c06c", "0X06c06c", "0Xc6cc6c", "0X05c05c","0X05c05c","0Xc00c00"]
#TESTBOARDSURL = ["0x2","0x3","0x4","0x6","0x7","0x8"]

# shelf-1 9-10 shelf-2 3-10
#TESTBOARDS = ["DF-01-09","DF-01-10","DF-02-03", "DF-02-04", "DF-02-05", "DF-02-06", "DF-02-07","DF-02-08", "DF-02-09", "DF-02-10"]
#TESTBOARDSRX = ["0Xc00c00","0Xc00c00","0Xc7fc7f","0Xc7fc7f","0X07f07f","0X07f07f","0X07f07f","0X07f07f","0X07f07f","0X07f07f"]
#TESTBOARDSURL = ["0x6","0x7","0x8","0x9","0xa","0xb","0xc","0xd","0xe","0xf"]

# shelf-1 9-10 shelf-2 3-10
#TESTBOARDS = ["DF-02-03", "DF-02-04"]
#TESTBOARDSRX = ["0X001001","0X001001"]
#TESTBOARDSURL = ["0x8","0x9"]

## shelf-1 5-10 shelf-2 5-9
#TESTBOARDS    = ["DF-01-05","DF-01-06","DF-01-07","DF-01-08","DF-01-09","DF-01-10",
#                 "DF-02-05","DF-02-06","DF-02-07","DF-02-08","DF-02-09"]
#TESTBOARDSRX  = ["0X07c07c", "0X07c07c","0X07c07c","0X07c07c","0X47c47c","0X47c47c",
#                 "0X43c43c", "0X43c43c","0X03c03c","0X03c03c","0X03c03c"]
#TESTBOARDSURL = ["0X2","0X3","0X4","0X5","0X6","0X7",
#                 "0Xa","0Xb","0Xc","0Xd","0Xe"]


## shelf-1 6-10 shelf-2 5-9
#TESTBOARDS    = ["DF-01-06","DF-01-07","DF-01-08","DF-01-09","DF-01-10",
#                 "DF-02-05","DF-02-06","DF-02-07","DF-02-08","DF-02-09"]
#TESTBOARDSRX  = ["0X078078", "0X078078","0X078078","0X478478","0X478478",
#                 "0X43c43c", "0X43c43c","0X03c03c","0X03c03c","0X03c03c"]
#TESTBOARDSURL = ["0X3","0X4","0X5","0X6","0X7",
#                 "0Xa","0Xb","0Xc","0Xd","0Xe"]

## shelf-1 8-10 shelf-2 5-9
#TESTBOARDS    = ["DF-01-08","DF-01-09","DF-01-10",
#                 "DF-02-05","DF-02-06","DF-02-07","DF-02-08","DF-02-09"]
#TESTBOARDSRX  = ["0X060060","0X460460","0X460460",
#                 "0X43c43c", "0X43c43c","0X03c03c","0X03c03c","0X03c03c"]
#TESTBOARDSURL = ["0X5","0X6","0X7",
#                 "0Xa","0Xb","0Xc","0Xd","0Xe"]

## shelf-1 9-10 shelf-2 5-9
#TESTBOARDS    = ["DF-01-09","DF-01-10",
#                 "DF-02-05","DF-02-06","DF-02-07","DF-02-08","DF-02-09"]
#TESTBOARDSRX  = ["0X440440","0X440440",
#                 "0X43c43c","0X43c43c","0X03c03c","0X03c03c","0X03c03c"]
#TESTBOARDSURL = ["0X6","0X7",
#                 "0Xa","0Xb","0Xc","0Xd","0Xe"]

TESTBOARDS = ["DF24", "DF25", "DF26"]
TESTBOARDSRX = ["0X060060", "0X044044", "0X044044"]
TESTBOARDSURL = ["0X1","0X2","0X3"]


for iboard in range(len(TESTBOARDS)):

    TESTBOARDRXCONFIG[ TESTBOARDS[iboard] ] = [ TESTBOARDSRX[iboard], TESTBOARDSURL[iboard] ]

    ### shelf-2 6 DFs
    #TESTBOARDCONFIG[ TESTBOARDS[iboard] ] = "P1Test/config_{!s}_6df_shelf2_32towers.txt".format(  TESTBOARDS[iboard] )
    
    ### shelf-2 7 DFs
    #TESTBOARDCONFIG[ TESTBOARDS[iboard] ] = "P1Test/config_{!s}_7df_32towers.txt".format(  TESTBOARDS[iboard] )
    
    ### shelf-2 7 DFs sct only 
    #TESTBOARDCONFIG[ TESTBOARDS[iboard] ] = "P1Test/config_{!s}_7df_sctonly_32towers.txt".format(  TESTBOARDS[iboard] )

    ### shelf-1 6 DFs 5-10
    #TESTBOARDCONFIG[ TESTBOARDS[iboard] ] = "P1Test/config_{!s}_6df_32towers.txt".format(  TESTBOARDS[iboard] )

    ### shelf-1 6 DFs 4-9
    #TESTBOARDCONFIG[ TESTBOARDS[iboard] ] = "P1Test/config_{!s}_6df-4-9_32towers.txt".format(  TESTBOARDS[iboard] )

    ### shelf-2 5 DFs
    #TESTBOARDCONFIG[ TESTBOARDS[iboard] ] = "P1Test/config_{!s}_5df_shelf2_32towers.txt".format(  TESTBOARDS[iboard] )

    ### shelf-2 4 DFs
    #TESTBOARDCONFIG[ TESTBOARDS[iboard] ] = "P1Test/config_{!s}_4df_shelf2_32towers.txt".format(  TESTBOARDS[iboard] )

    ## shelf-1 8 DFs
    #TESTBOARDCONFIG[ TESTBOARDS[iboard] ] = "P1Test/config_{!s}_8df_shelf1_32towers.txt".format(  TESTBOARDS[iboard] )

    ## shelf-1 8 DFs and shelf-2 5 DFs
    #TESTBOARDCONFIG[ TESTBOARDS[iboard] ] = "P1Test/config_{!s}_13df_32towers.txt".format(  TESTBOARDS[iboard] )

    ## shelf-1 4 DFs 3-6
    #TESTBOARDCONFIG[ TESTBOARDS[iboard] ] = "P1Test/config_{!s}_4df_32towers.txt".format(  TESTBOARDS[iboard] )

    ## shelf-1 4 DFs 7-10
    #TESTBOARDCONFIG[ TESTBOARDS[iboard] ] = "P1Test/config_{!s}_4df_32towers.txt".format(  TESTBOARDS[iboard] )

    ## shelf-1 2 DFs 3-4
    #TESTBOARDCONFIG[ TESTBOARDS[iboard] ] = "P1Test/config_{!s}_2df_3-4_32towers.txt".format(  TESTBOARDS[iboard] )

    ## shelf-1-2 12 DFs 5-10
    #TESTBOARDCONFIG[ TESTBOARDS[iboard] ] = "P1Test/config_{!s}_12df_32towers.txt".format(  TESTBOARDS[iboard] )

    ## shelf-1 9-10 shelf-2 5-6
    #TESTBOARDCONFIG[ TESTBOARDS[iboard] ] = "P1Test/config_{!s}_4df_IT_32towers.txt".format(  TESTBOARDS[iboard] )

    ## shelf-1 5-10 shelf-2 5-7
    #TESTBOARDCONFIG[ TESTBOARDS[iboard] ] = "P1Test/config_{!s}_9df_shelf1-2_4sct_32towers.txt".format(  TESTBOARDS[iboard] )

    # shelf-1 9-10 shelf-2 3-10
    #TESTBOARDCONFIG[ TESTBOARDS[iboard] ] = "P1Test/config_{!s}_10df_shelf1-2_32towers.txt".format(  TESTBOARDS[iboard] )


    ## shelf-1 5-10 shelf-2 5-6
    #TESTBOARDCONFIG[ TESTBOARDS[iboard] ] = "P1Test/config_{!s}_8dfIT_32towers.txt".format(  TESTBOARDS[iboard] )

    ## shelf-1 5-6 shelf-2 5-10
    #TESTBOARDCONFIG[ TESTBOARDS[iboard] ] = "P1Test/config_{!s}_8dfIT_32towers.txt".format(  TESTBOARDS[iboard] )

    ## shelf-1 6 9 10 shelf-2 5-6
    #TESTBOARDCONFIG[ TESTBOARDS[iboard] ] = "P1Test/config_{!s}_5dfIT_32towers.txt".format(  TESTBOARDS[iboard] )

    #TESTBOARDCONFIG[ TESTBOARDS[iboard] ] = "P1Test/config_{!s}_6df_shelf1-2_32towers.txt".format(  TESTBOARDS[iboard] ) 

    ## shelf-1 5-9 shelf-2 3
    #TESTBOARDCONFIG[ TESTBOARDS[iboard] ] = "P1Test/config_{!s}_6df_shelf1-2_32towers.txt".format(  TESTBOARDS[iboard] )

    ## shelf-1 5-9 shelf-2 3 5 7
    #TESTBOARDCONFIG[ TESTBOARDS[iboard] ] = "P1Test/config_{!s}_8df_shelf1-2_32towers.txt".format(  TESTBOARDS[iboard] )

    ## shelf-1 5-9 
    #TESTBOARDCONFIG[ TESTBOARDS[iboard] ] = "P1Test/config_{!s}_5df_32towers.txt".format(  TESTBOARDS[iboard] )

    ## shelf-1 5-9 
    #TESTBOARDCONFIG[ TESTBOARDS[iboard] ] = "P1Test/config_{!s}_8df_shelf2_32towers.txt".format(  TESTBOARDS[iboard] )

    ## shelf-1 5 6 7 9 10 shelf-2 3
    #TESTBOARDCONFIG[ TESTBOARDS[iboard] ] = "P1Test/config_{!s}_6df_shelf1-2_32towers.txt".format( TESTBOARDS[iboard] )

    ## shelf-1 9-10 shelf-2 3-10
    #TESTBOARDCONFIG[ TESTBOARDS[iboard] ] = "P1Test/config_{!s}_10df_shelf1-2_32towers.txt".format( TESTBOARDS[iboard] )

    ## shelf-2 3 4
    #TESTBOARDCONFIG[ TESTBOARDS[iboard] ] = "P1Test/config_{!s}_2df_4sct_32towers.txt".format( TESTBOARDS[iboard] )

    ## shelf-2 3-10
    #TESTBOARDCONFIG[ TESTBOARDS[iboard] ] = "P1Test/config_{!s}_8df_shelf2_32towers.txt".format( TESTBOARDS[iboard] )

    ## shelf-1 9-10 shelf-2 5-10
    #TESTBOARDCONFIG[ TESTBOARDS[iboard] ] = "P1Test/config_{!s}_8df_shelf1-2_32towers.txt".format( TESTBOARDS[iboard] )
    #TESTBOARDCONFIG[ TESTBOARDS[iboard] ] = "P1Test/config_{!s}_8df_shelf1-2_3sct_32towers.txt".format( TESTBOARDS[iboard] )

    ## shelf-1 9-10 shelf-2 5-8
    #TESTBOARDCONFIG[ TESTBOARDS[iboard] ] = "P1Test/config_{!s}_6df_shelf1-2_4sct_32towers.txt".format( TESTBOARDS[iboard] )
    #TESTBOARDCONFIG[ TESTBOARDS[iboard] ] = "P1Test/config_{!s}_6df_shelf1-2_3sct_32towers.txt".format( TESTBOARDS[iboard] )


    ## shelf-1 9-10 shelf-2 5-6
    #TESTBOARDCONFIG[ TESTBOARDS[iboard] ] = "P1Test/config_{!s}_4df_shelf1-2_4sct_32towers.txt".format( TESTBOARDS[iboard] )
    #TESTBOARDCONFIG[ TESTBOARDS[iboard] ] = "P1Test/config_{!s}_4df_shelf1-2_3sct_32towers.txt".format( TESTBOARDS[iboard] )

    ## shelf-2 5-8
    #TESTBOARDCONFIG[ TESTBOARDS[iboard] ] = "P1Test/config_{!s}_4df_4sct_32towers.txt".format( TESTBOARDS[iboard] )
    #TESTBOARDCONFIG[ TESTBOARDS[iboard] ] = "P1Test/config_{!s}_2df_4sct_32towers.txt".format( TESTBOARDS[iboard] )


    ## shelf-1 5-10 shelf-2 5-9
    #TESTBOARDCONFIG[ TESTBOARDS[iboard] ] = "P1Test/config_{!s}_11df_shelf1-2_4sct_32towers.txt".format( TESTBOARDS[iboard] )

    ## shelf-1 6-10 shelf-2 5-9
    #TESTBOARDCONFIG[ TESTBOARDS[iboard] ] = "P1Test/config_{!s}_10df_shelf1-2_4sct_32towers.txt".format( TESTBOARDS[iboard] )

    ## shelf-1 8-10 shelf-2 5-9
    #TESTBOARDCONFIG[ TESTBOARDS[iboard] ] = "P1Test/config_{!s}_8df_shelf1-2_4sct_32towers.txt".format( TESTBOARDS[iboard] )

    ## shelf-1 9-10 shelf-2 5-9
    #TESTBOARDCONFIG[ TESTBOARDS[iboard] ] = "P1Test/config_{!s}_7df_shelf1-2_4sct_32towers.txt".format( TESTBOARDS[iboard] )

    
    ## Lab4 3 board test
    TESTBOARDCONFIG[ TESTBOARDS[iboard] ] = "config/Configs_tmp/config_{!s}_3df_32towers.txt".format(  TESTBOARDS[iboard] )

    

def convertBittoInt(bin):
    length = len(bin)
    indicators = []

    
    for i in range(length-2):
        #print i, bin, length-i
        if bin[length-i-1] == '1':
            indicators.append(i)
    return indicators
        

def GenerateTransceiverFile(board, rxconfig):

    print rxconfig
    rxconfig = rxconfig[board][0]
    rxconfig = rxconfig[0:5]
    thisbin = bin(int(rxconfig, 16))
    indicators =  convertBittoInt(thisbin)

    channels = []
    for i in indicators:
        channels.append(i+36)
        channels.append(i+48)

    print channels

    txtfile = open("config/dummuy_DF_GTX_config.txt")
    lines = txtfile.readlines()

    filename = "config/GTXConfigs_tmp/GTX_board-"+board+"_"+str(int(time()))+".txt"
    newfile = open(filename, 'w')

    for iline in range(len(lines)):
        if ("gtch2force_ready_mode" in lines[iline]) and ("#" not in lines[iline]):
            
            for c in channels:
                if str(c) in lines[iline]:
                    stringlist = list(lines[iline])
                    #print str(c), stringlist, stringlist[32]
                    stringlist[32]='0'
                    #print lines[iline]
                    lines[iline] = "".join(stringlist)
                    #print lines[iline]

    for line in lines:
        newfile.write(line)


    TESTBOARDGTXCONFIG[board] = filename

    print board, "is using this gtx xver file", filename
    newfile.close()


def StopSendingPseudoData( board):
    #for iFPGA in range(8):
    for iFPGA in [2,3,6,7]:
        for iChannel in range(2):
            cmd = "test_im_bt -D {!s} -M 6 -F {!s} -N {!s} -P 0".format(board, str(iFPGA), str(iChannel))
            os.system(cmd)

def SendingPseudoData(board):
    #for iFPGA in range(8):
    print board
    for iFPGA in [2,3,6,7]:
        for iChannel in range(2):
            cmd = "test_im_bt -D {!s} -M 6 -F {!s} -N {!s} -P 3".format(board, str(iFPGA), str(iChannel))
            os.system(cmd)

def EnableIMChannel(board):
    #for iFPGA in range(8):
    for iFPGA in [2,3,6,7]:
        for iChannel in [1]:
            cmd = "test_im_bt -D {!s} -M 9 -F {!s} -N {!s} -V 0".format(board, str(iFPGA), str(iChannel))
            os.system(cmd)

def SetIMStartTime(board):
    for iFPGA in [2,3,6,7]:
        for iChannel in [1]:
            cmd = "test_im_bt -D {!s} -M 101 -F {!s} -N {!s} -R 0X00000132 -W 0X00000001".format(board, str(iFPGA), str(iChannel))
            os.system(cmd)


def DisableIMChannel(board):
    #for iFPGA in range(8):
    for iFPGA in [2,3,5,7]:
        for iChannel in range(2):
            cmd = "test_im_bt -D {!s} -M 9 -F {!s} -N {!s} -V 1".format(board, str(iFPGA), str(iChannel))
            os.system(cmd)


def SetTimeOutTreshold(board):
    cmd = "test_df -D {!s} -N reg.max_cycle_waited_for_data_allowed -M 100 -w -V 0X4FF".format(board)
    os.system(cmd)
    
    
def ResetDF(board, config, gtxconfig):
    cmd = "test_df -M 1 -D {!s} -F {!s} -X {!s}".format(board, config, gtxconfig)
    os.system(cmd)

def AutoDelayValueScan(board):
    cmd = "test_df -M 30 -D {!s}".format(board)
    os.system(cmd)

def DisableGTX(board):
    cmd = "test_df -D {!s} -M 100 -N reg.internal_link_rx_enable -w -V 0X00000000".format(board)
    os.system(cmd)
    cmd = "test_df -D {!s} -M 100 -N reg.internal_link_tx_enable -w -V 0X00000000".format(board)
    os.system(cmd)

    cmd = "test_df -D {!s} -M 100 -N reg.internal_link_rx_enable_bert -w -V 0X00000000".format(board)
    os.system(cmd)
    cmd = "test_df -D {!s} -M 100 -N reg.internal_link_tx_enable_bert -w -V 0X00000000".format(board)
    os.system(cmd)


def EnableAllTX(board):
    cmd = "test_df -D {!s} -M 100 -N reg.internal_link_tx_enable -w -V 0X00FFFFFF".format(board)
    os.system(cmd)

def ConfigGTXTX(board, gtxconfigs):
    cmd = "test_df -D {!s} -M 100 -N reg.internal_link_tx_enable -w -V {!s}".format(board, gtxconfigs[board][0])
    os.system(cmd)

def ConfigGTXRX(board, gtxconfigs):

    cmd = "test_df -D {!s} -M 100 -N reg.internal_link_rx_enable -w -V {!s}".format(board, gtxconfigs[board][0])
    os.system(cmd)
    cmd = "test_df -D {!s} -M 100 -N reg.internallink_url -w -V {!s}".format(board, gtxconfigs[board][1])
    os.system(cmd)


def Sleep(length):
    cmd = "sleep {!s}".format(str(length))
    os.system(cmd)


def DownloadIMLUTPseudoData(board, config):
    txtfile = open(config)
    lines = txtfile.readlines()
    startParsing = False

    SCTRobs = []
    PIXRobs = []
    SCTChannel = []
    PIXChannel = []

    for iline in range(len(lines)):
        if "RobID" in lines[iline]:
            startParsing = True

        if not startParsing:
            continue
        else:
            line = lines[iline].split()
            if ("#" not in line[0]) and (line[2]==DFLOGICNUMBER[board]):
                if "0x2" in line[1]:
                    SCTRobs.append(line[1].lstrip("0x"))
                    SCTChannel.append(int(line[3]))
                if "0x1" in line[1]:
                    PIXRobs.append(line[1].lstrip("0x"))
                    PIXChannel.append(int(line[3]))


    print SCTRobs
    print PIXRobs

    for irob in range(len(SCTRobs)):


        cmd = "test_im_bt -D {!s} -M 5 -F {!s} -N {!s} -T config/IM_LUT_09_14_2016/IM_LUT_0x{!s}.txt".format(board, str(SCTChannel[irob]/2), str(SCTChannel[irob]%2), SCTRobs[irob])
        os.system(cmd)
        cmd = "test_im_bt -D {!s} -M 7 -F {!s} -N {!s} -I config/IM_PseudoData_09_14_2016/IM_SimpleRAMDATA_0x00{!s}.dat".format(board, str(SCTChannel[irob]/2), str(SCTChannel[irob]%2), SCTRobs[irob])
        os.system(cmd)
        cmd = "test_im_bt -D {!s} -M 101 -F {!s} -N {!s} -R 0X0000011b -W 0X00000000".format(board, str(SCTChannel[irob]/2), str(SCTChannel[irob]%2))
        os.system(cmd)
        

    for irob in range(len(PIXRobs)):

        cmd = "test_im_bt -D {!s} -M 5 -F {!s} -N {!s} -T /atlas-home/0/tomoya/DataFormatter/config/IMDF_LUT_Run2/IM_LUT_0x{!s}.txt".format(board, str(PIXChannel[irob]/2), str(PIXChannel[irob]%2), PIXRobs[irob])
        os.system(cmd)
        cmd = "test_im_bt -D {!s} -M 7 -F {!s} -N {!s} -I /atlas-home/0/tomoya/DataFormatter/config/IM_TestData_Run2/IM_SimpleRAMDATA_0x00{!s}.dat".format(board, str(PIXChannel[irob]/2), str(PIXChannel[irob]%2), PIXRobs[irob])
        os.system(cmd)
        cmd = "test_im_bt -D {!s} -M 101 -F {!s} -N {!s} -R 0X0000001b -W 0X00000000".format(board, str(PIXChannel[irob]/2), str(PIXChannel[irob]%2))
        os.system(cmd)


def OneDFTest(board):
    DisableGTX(board)

    if ISPSEUDODATA:
        StopSendingPseudoData(board)
    else:
        DisableIMChannel(board)
    
    SetTimeOutTreshold(board)


    Sleep(0.5)
    ResetDF(board, ONEDFCONFIG[board], "config/dummuy_DF_GTX_config.txt")

    SetTimeOutTreshold(board)
    #AutoDelayValueScan(board)

    Sleep(1)
    #SetIMStartTime(board)   

    if ISPSEUDODATA:
        SendingPseudoData(board)
    else:
        EnableIMChannel(board)


def MultiDFTest(boards, configs, gtxconfigs, rxconfigs):

    for board in boards:
        print "generating trasceiver file"
        GenerateTransceiverFile(board, rxconfigs)

        if ISPSEUDODATA:
            StopSendingPseudoData(board)
        else:
            DisableIMChannel(board)

        SetTimeOutTreshold(board)

        
    for board in boards:
        DisableGTX(board)

    for board in boards:
        
        ResetDF(board, configs[board], gtxconfigs[board])
        Sleep(0.5)

    #for board in boards:
    #    AutoDelayValueScan(board)

    Sleep(0.5)
    for board in boards:
        ConfigGTXTX(board, rxconfigs)

    Sleep(0.5)
    for board in boards:
        ConfigGTXRX(board, rxconfigs)
        
        SetIMStartTime(board)

    Sleep(0.5)
    for board in boards:
        if ISPSEUDODATA:
            SendingPseudoData(board)
        else:
            EnableIMChannel(board)
            

#DownloadIMLUTPseudoData("DF3D", DFSYSTEMCONFIG)
#DownloadIMLUTPseudoData("DF-01-06", DFSYSTEMCONFIG)
#DownloadIMLUTPseudoData("DF-01-07", DFSYSTEMCONFIG)
#DownloadIMLUTPseudoData("DF-01-08", DFSYSTEMCONFIG)
#DownloadIMLUTPseudoData("DF-02-05", DFSYSTEMCONFIG)
#DownloadIMLUTPseudoData("DF-02-06", DFSYSTEMCONFIG)
#DownloadIMLUTPseudoData("DF-02-07", DFSYSTEMCONFIG)
#DownloadIMLUTPseudoData("DF-02-08", DFSYSTEMCONFIG)
#DownloadIMLUTPseudoData("DF-02-09", DFSYSTEMCONFIG)
#DownloadIMLUTPseudoData("DF-02-10", DFSYSTEMCONFIG)
#DownloadIMLUTPseudoData("DF24", DFSYSTEMCONFIG)
#DownloadIMLUTPseudoData("DF25", DFSYSTEMCONFIG)
#DownloadIMLUTPseudoData("DF26", DFSYSTEMCONFIG)

#OneDFTest("DF3D")
#OneDFTest("DF-01-03")
#OneDFTest("DF-01-04")
#OneDFTest("DF-01-05")
#OneDFTest("DF-01-06")
#OneDFTest("DF-01-07")
#OneDFTest("DF-01-08")
#OneDFTest("DF-01-09")
#OneDFTest("DF-01-10")
#OneDFTest("DF-02-03")
#OneDFTest("DF-02-04")
#OneDFTest("DF-02-05")
#OneDFTest("DF-02-06")
#OneDFTest("DF-02-07")
#OneDFTest("DF-02-08")
#OneDFTest("DF-02-09")
#OneDFTest("DF-02-10")
#OneDFTest("DF24")
#OneDFTest("DF25")
#OneDFTest("DF26")

#for board in TESTBOARDS:
#     DownloadIMLUTPseudoData(board, DFSYSTEMCONFIG)

MultiDFTest(TESTBOARDS, TESTBOARDCONFIG, TESTBOARDGTXCONFIG, TESTBOARDRXCONFIG)

