# (!): To be done SA
# (+): implemented in RC
# (-): not in RC and not mandatory

#! download pseudo data
echo "step 1: download pseudo data"
test_im -D DF1 -M 7 -F 2 -N 1 -I config/mod_sct_pseudo_0x240103.txt

#+ download LUT
echo "step2: download LUT"
test_im -D DF1 -M 5 -F 2 -N 1 -T config/input_LUT/LUT_0x240103.txt


#! stop pseuda data in case it is going
echo "step 3: stop pseudo data"
test_im -D DF1D -M 6 -F 2 -N 1 -P 0


echo "configuring DF"
#+close TX and RX before configuration
echo "step 4: close TX side"
test_df -D DF1D -M 100 -N reg.internal_link_tx_enable -w -V 0X00000000
echo "step 4: close RX side"
test_df -D DF1D -M 100 -N reg.internal_link_rx_enable -w -V 0X00000000

#+configure for first pix channel on FPGA0
echo "step 5: configure DF"
test_df -D DF1D -M 1 -X ./config/transceiver_configuration.txt -F ./config/configuration_b0_f2c1.txt

#- stop IBERT mode.. ask yasu if this is built into trancievers, we want to remove this later
echo "step 6: stop IBERT mode at TX side, additional setup"
test_df -D DF1D -M 100 -N reg.internal_link_tx_enable_bert -w -V 0X00000000
echo "step 7: stop IBERT mode at RX side, additional setup"
test_df -D DF1D -M 100 -N reg.internal_link_rx_enable_bert -w -V 0X00000000

#+open TX side
echo "Step 8: Open TX side on DF, additional setup"
test_df -D DF1D -M 100 -N reg.internal_link_tx_enable -w -V 0X00FFFFFF

#+disable all internal links
echo "step 9 : close internal DF links , additional setup"
test_df -D DF1D -M 100 -N reg.internal_link_rx_enable -w -V 0X00000000

#-set url, not necessary for now, not needed for now
echo "step 10 : setup url, additional setup"
test_df -D DF1D -M 100 -N reg.internallink_url -w -V 0X1

#! check tables to make sure all is clear
echo "step 11: check table"
test_df -D DF1D -M 2


#!start pseudo data
echo "step 12 : start pseudo data"
test_im -D DF1D -M 6 -F 2 -N 1 -P 3

#!check table to make sure we have data
echo "step 13: check table"
test_df -D DF1D -M 2

#! check IM output spybuffer for channel 0
#echo "check IM input spy buffers in spy_buffer_check_temp/IM_out_ch0.txt"
#test_im -D DF1 -M 3 -F 0 -N 0 -S 1 -L 4096  >spy_buffer_check_temp/IM_out_ch0.txt # L is max number of lines


#! check DF input spybuffer (FMCIN)
#echo "check DF input spy buffers in spy_buffer_check_temp/DF_in_ch0.txt"
#test_df -M 5 -D DF1D -L 34 >spy_buffer_check_temp/DF_in_ch0.txt # L 34 means ch 0


#! check DF output spybuffer (SLINK OUT)
#echo "check DF output spy buffers in spy_buffer_check_temp/DF_out_ch0.txt"
#test_df -M 5 -D DF1 -L 0 >spy_buffer_check_temp/DF_out_ch0.txt # L 0 means ch 0 
