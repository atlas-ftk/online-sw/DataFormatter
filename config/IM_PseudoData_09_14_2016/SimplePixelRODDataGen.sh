#! /bin/sh -f                                                                   

echo "HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH"
echo "H  Simple ROD data genarator  H"
echo "H ROD: "${1}
echo "H IM LUT: "${2}
echo "H outputs : IM_SimpleRAMDATA_0x"${1}".dat and IM_SimpleQUESTDATA_0x"${1}".dat"
echo "HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH"

if [ $# -le 1 ];  then
    echo "usage : $bash SimpleRODDataGen.sh [RODID] [IM_LUT file] "
    echo "e.g. bash SimplePixelRODDataGen.sh 00111816 ../IMDF_LUT_MC12/IM_LUT_0x111816.txt "
    echo "ERROR  2 argument required"
    exit 1
fi



#if [ ${3} = 1 ]; then
echo "800000000" > IM_SimpleRAMDATA_0x${1}.dat
echo "Db0f00000" >> IM_SimpleRAMDATA_0x${1}.dat
echo "Cee1234ee" >> IM_SimpleRAMDATA_0x${1}.dat
echo "C00000009" >> IM_SimpleRAMDATA_0x${1}.dat
echo "C03010000" >> IM_SimpleRAMDATA_0x${1}.dat
echo "C"${1}     >> IM_SimpleRAMDATA_0x${1}.dat
echo "C0000face" >> IM_SimpleRAMDATA_0x${1}.dat
echo "E01000000" >> IM_SimpleRAMDATA_0x${1}.dat
echo "C00000337" >> IM_SimpleRAMDATA_0x${1}.dat
echo "C00000088" >> IM_SimpleRAMDATA_0x${1}.dat
echo "C00000000" >> IM_SimpleRAMDATA_0x${1}.dat
#else
#echo "0Xb0f00000" > IM_DATA_0x${1}.dat
echo "0Xee1234ee" > IM_SimpleQUESTDATA_0x${1}.dat
echo "0X00000009" >> IM_SimpleQUESTDATA_0x${1}.dat
echo "0X03010000" >> IM_SimpleQUESTDATA_0x${1}.dat
echo "0X"${1}     >> IM_SimpleQUESTDATA_0x${1}.dat
echo "0X0000face" >> IM_SimpleQUESTDATA_0x${1}.dat
echo "0X01000000" >> IM_SimpleQUESTDATA_0x${1}.dat
echo "0X00000337" >> IM_SimpleQUESTDATA_0x${1}.dat
echo "0X00000088" >> IM_SimpleQUESTDATA_0x${1}.dat
echo "0X00000000" >> IM_SimpleQUESTDATA_0x${1}.dat
#fi




a="`wc ${2} | awk '{print $1}'`";

for ((i=1;i<a+1;i++)) ; 
do  

#if [ ${3} = 1 ]; then
echo -n "C20" >> IM_SimpleRAMDATA_0x${1}.dat
cat ${2} | awk -v nm=${i} 'NR==nm {printf $1}'  | sed -e s/0X//g >> IM_SimpleRAMDATA_0x${1}.dat
echo "0000" >> IM_SimpleRAMDATA_0x${1}.dat
echo "C810f0101" >> IM_SimpleRAMDATA_0x${1}.dat
echo "C40000000" >> IM_SimpleRAMDATA_0x${1}.dat
#else
echo -n "0x20" >> IM_SimpleQUESTDATA_0x${1}.dat
cat ${2} | awk -v nm=${i} 'NR==nm {printf $1}'  | sed -e s/0X//g >> IM_SimpleQUESTDATA_0x${1}.dat
echo "0000" >> IM_SimpleQUESTDATA_0x${1}.dat
echo "0x810f0101" >> IM_SimpleQUESTDATA_0x${1}.dat
echo "0x40000000" >> IM_SimpleQUESTDATA_0x${1}.dat

#fi;

done ;

#if [ ${3} = 1 ]; then
echo "C00000000" >> IM_SimpleRAMDATA_0x${1}.dat
echo "C00000000" >> IM_SimpleRAMDATA_0x${1}.dat
echo "C00000002" >> IM_SimpleRAMDATA_0x${1}.dat
echo "C0000000c" >> IM_SimpleRAMDATA_0x${1}.dat
echo "C00000001" >> IM_SimpleRAMDATA_0x${1}.dat
echo "De0f00000" >> IM_SimpleRAMDATA_0x${1}.dat
echo "800000000" >> IM_SimpleRAMDATA_0x${1}.dat
echo "800000000" >> IM_SimpleRAMDATA_0x${1}.dat
echo "800000000" >> IM_SimpleRAMDATA_0x${1}.dat
echo "000000000" >> IM_SimpleRAMDATA_0x${1}.dat
echo "000000000" >> IM_SimpleRAMDATA_0x${1}.dat
#else
echo "0X00000000" >> IM_SimpleQUESTDATA_0x${1}.dat
echo "0X00000000" >> IM_SimpleQUESTDATA_0x${1}.dat
echo "0X00000002" >> IM_SimpleQUESTDATA_0x${1}.dat
echo "0X0000000c" >> IM_SimpleQUESTDATA_0x${1}.dat
echo "0X00000001" >> IM_SimpleQUESTDATA_0x${1}.dat
#echo "0Xe0f00000" >> IM_DATA_0x${1}.dat
#done ;