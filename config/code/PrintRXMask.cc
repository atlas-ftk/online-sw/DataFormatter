#include <vector>
#include <string>
#include <sstream>
#include <fstream>
#include <iostream>
#include <iomanip>
#include <map>
#include <utility>

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>

#include <unistd.h>

using namespace std;

void readBoardMapping(string fileName, map<int, pair<int,int> >& BoardToShelfAndSlot);
bool boardReceives(string fileName, int board);
vector<string> parseString(string str, string sep);
int main(int argc, char* argv[])
{
  int result;
  string board_numbers="";
  string system_config_file="";
  string output_config_file="";
  while((result=getopt(argc,argv,"B:S:O:"))!=-1){
    switch(result){
    case 'B':
      board_numbers = optarg;//strtoul(optarg, NULL, 0);
      cout<<"Making board configuration file for DF internal board No  "<<optarg << endl;;
      break;
    case 'S':
      system_config_file = optarg;
      std::cout<<"Using System Configuration File " <<
	system_config_file<<std::endl;
      break;
    case 'O':
      output_config_file = optarg;
      std::cout<<"Creating output configuration file  " <<
	output_config_file<<std::endl;
      break;
    default:
      printf("%s -B <comma-separated board ids (0-31)> -S <sysconfigfile.txt> -O <output_config_name>\n", argv[0]);
      exit(EXIT_FAILURE);
    }
  }

  vector<string> boardsStr = parseString(board_numbers,",");
  vector<int> boards;
  for (int i=0;i<(int)boardsStr.size();i++)
  {
    int b;
    stringstream s;
    s << boardsStr[i];
    s >> b;
    cout << "Adding board: " << b << endl;
    boards.push_back(b);
  }

  map<int, pair<int,int> > BoardToShelfAndSlot;
  readBoardMapping(system_config_file, BoardToShelfAndSlot);

  ofstream outFile(output_config_file.c_str());
  int nrBoards = boards.size();
  for (int i=0;i<nrBoards;i++)
  {
    int rx_board = boards[i];
    int rx_shelf = BoardToShelfAndSlot[rx_board].first;
    int rx_slot = BoardToShelfAndSlot[rx_board].second;
    uint32_t mask = 0x0;
    for (int j=0;j<nrBoards;j++)
    {
      int tx_board = boards[j];
      int tx_slot = BoardToShelfAndSlot[tx_board].second;
      stringstream fileName;
      fileName << "configuration_8board_B" << tx_board << ".txt";
      bool receives = boardReceives(fileName.str(), rx_board);
      if (!receives)
      {
	cout << "Slot " << rx_slot << " gets no data from slot " << tx_slot << endl;

	int offset=3;
	if (tx_slot > rx_slot) offset=4;
	mask |= (0x1<<(tx_slot-offset));
	mask |= (0x1<<(tx_slot-offset+12));
      }
    }
    mask = (~mask) & 0xfffffff;
    cout << "board: " << rx_board << ", mask: " << std::hex << mask << std::dec << endl;
    outFile << "shelfAndSlotMask " << rx_shelf << " " << rx_slot << " 0X" << std::hex << mask << std::dec << endl;
  }
  outFile.close();
  return 0;
}

bool boardReceives(string fileName, int rx_board)
{
  int board_mask = 0x1 << rx_board;
  string line;
  ifstream inFile(fileName.c_str());
  while (getline(inFile, line))
  {
    stringstream line_ss(line);
    string       c1, c2, c3, c4;
    line_ss >> c1 >> c2 >> c3 >> c4;
    //cout << c1 << " " << c2 << " " << c3 << " " << c4 << endl;

    if (c1.find("#") != string::npos) {
      continue;
    }

    if (c1 == "sctmod2dst" || c1 == "pixmod2dst")
    {
      bool receives = ((int)strtol(c3.c_str(), NULL, 0) & board_mask);
      if (receives) return true;
    }
  }
  return false;
}
void readBoardMapping(string fileName, map<int, pair<int,int> >& BoardToShelfAndSlot)
{
  string line;
  ifstream inFile(fileName.c_str());
  while (getline(inFile, line))
  {
    stringstream line_ss(line);
    string       c1;
    int c2, c3, c4;
    line_ss >> c1 >> c2 >> c3 >> c4;
    //cout << c1 << " " << c2 << " " << c3 << " " << c4 << endl;
    
    if (c1.find("#") != string::npos) {
      continue;
    }

    if (c1 == "BoardNToShelfAndSlot")
    {
      BoardToShelfAndSlot[c2] = make_pair(c3,c4);
    }
  }
}

vector<string> parseString(string str, string sep)
{
  vector<string> parsed;
  int pos = 0;
  bool first = true;
  if (str.size() == 0) return parsed;
  if (str.find(sep) == string::npos)
  {
    parsed.push_back(str);
    return parsed;
  }
  while (true)
  {
    int newPos = str.find(sep, pos);
    if (str.find(sep, pos) == string::npos)
    {
      if (!first) parsed.push_back(str.substr(pos, newPos-pos));
      break;
    }
    string sub = str.substr(pos, newPos-pos);
    parsed.push_back(sub);
    pos = newPos+1;
    first = false;
  }
  return parsed;
}


