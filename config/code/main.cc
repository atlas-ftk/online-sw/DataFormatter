// temporal script to extract the DF configuration file
// for one DF configuration setup :

#include <fstream>
#include <string>
#include <sstream>
#include <vector>
#include <map>
#include <iostream>
#include <iomanip>

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>

#include <algorithm>
#include <unistd.h>

bool dumpSummary = false;

using namespace std;

// ======================================================================
// MODIFY from HERE (1/2)
// ======================================================================
// port configuration
// ROD-ID                                    Board ID (0-31) Input lane (0-15)


//// USA15 test with real configuration 20150813

// ======================================================================
// MODIFY until HERE (1/2)
// ======================================================================


// nWordsPerModule configuration
// from DF spec : SCT Barrel   10*(80/30) / clustering factor ~ 2 * 100% (increase #hits dicrease by change of BX req) *  = 5
// from DF spec : Pixel BLayer 23*(80/30) / clustering factor ~ 3 * 145% (8/50ns->14/25ns) = 30 & IBL
// from DF spec : Pixel L1     11*(80/30) / clustering factor ~ 3 * 145% (8/50ns->14/25ns) = 14
// from DF spec : Pixel L2      7*(80/30) / clustering factor ~ 3 * 145% (8/50ns->14/25ns) = 9

//======================================================================
// https://box.cern.ch/public.php?service=files&t=323c839f9407229887e21d0965f8ae5a
// see Switching Bit Assignment sheet
// (this is related to firmware design of which gth connnecting to which fabric channels)
//======================================================================

//static const int NumberOfFTKTower = 64;
bool SkipBotTower = true;
int NumberOfFTKTower = 32;
static const int NumberOfDFBoards = 32;
static const int NumberOfFMCInputLanesPerBoard = 16;
static const int NumberOfOutputLanesPerBoard = 36;

//======================================================================
void print_param(const std::string& name, const uint32_t& value, const int& numbits);
void print_param_int(const std::string& name, const uint32_t& value);

//======================================================================
void dump_configuration_timeline_to_file
(std::ofstream& process_time_line,
 const int& youngest_input_id,
 const uint32_t& this_board_number,
 const std::vector<int>& module_counter_in, //[16]
 const std::vector<int>& module_counter_out, //[16]
 const uint32_t& enable_fmc_lanes_mask, //[bit mask for 16bit]
 const std::map<uint32_t, uint32_t>& pixmod2dst_map,
 const std::map<uint32_t, uint32_t>& sctmod2dst_map,
 const std::map<uint32_t, uint32_t>& pixmod2ftkplane_map,
 const std::map<uint32_t, uint32_t>& sctmod2ftkplane_map,
 const std::map<uint32_t, uint32_t>& pixmod2tower_map,
 const std::map<uint32_t, uint32_t>& sctmod2tower_map,
 const std::map<uint32_t, std::pair<uint32_t, uint32_t> >& DFBoardNumberToShelfAndSlotMap,
 const std::map<uint32_t, uint32_t>& DFFiberConnectionMap,
 const std::map<uint32_t, uint32_t>& ATCAFabricChToOutputBitPosMap 
 );

//======================================================================
void dump_configuration(const int& youngest_input_id,
			const uint32_t& this_board_number,
			const std::vector<int>& module_counter_in, //[16]
			const std::vector<int>& module_counter_out, //[34]
			const uint32_t& enable_fmc_lanes_mask, //[bit mask for 16bit]
			std::map<uint32_t, uint32_t> pixmod2dst_map,
			std::map<uint32_t, uint32_t> sctmod2dst_map,
			std::map<uint32_t, uint32_t> pixmod2ftkplane_map,
			std::map<uint32_t, uint32_t> sctmod2ftkplane_map,
			std::map<uint32_t, uint32_t> pixmod2tower_map,
			std::map<uint32_t, uint32_t> sctmod2tower_map
			);

//======================================================================
void dump_configuration_file(std::ofstream& configuration_file,
			     const uint32_t& this_board_number,
			     const std::vector<int>& module_counter_in, //[16]
			     const std::vector<int>& module_counter_out, //[34]
			     const uint32_t& enable_fmc_lanes_mask, //[bit mask for 16bit]
			     const std::map<int, std::vector<uint32_t> >& fmcin2modid_map,
			     const std::map<uint32_t, uint32_t>& pixmod2dst_map,
			     const std::map<uint32_t, uint32_t>& sctmod2dst_map,
			     const std::map<uint32_t, uint32_t>& pixmod2ftkplane_map,
			     const std::map<uint32_t, uint32_t>& sctmod2ftkplane_map,
			     const std::map<uint32_t, uint32_t>& pixmod2tower_map,
			     const std::map<uint32_t, uint32_t>& sctmod2tower_map,
			     const std::map<uint32_t, std::pair<uint32_t, uint32_t> >& DFBoardNumberToShelfAndSlotMap,
			     const std::map<uint32_t, uint32_t>& DFFiberConnectionMap,
			     const std::map<uint32_t, uint32_t>& ATCAFabricChToOutputBitPosMap,
			     const std::multimap<uint32_t, std::pair<uint32_t, uint32_t> >& RodIdToFMCInIdMap,
			     const std::map<uint32_t, std::pair<uint32_t,uint32_t> >& LaneToInvDelayMap,
			     const std::map<uint32_t, bool>& BoardEnableMap
);

//======================================================================
void dump_rx_ports_to_file
(std::ofstream& configuration_file,
 int this_board_number,
 const std::map<uint32_t, std::pair<uint32_t, uint32_t> >& DFBoardNumberToShelfAndSlotMap,
 const std::map<uint32_t, bool>& BoardEnableMap,
 const std::map<uint32_t, uint32_t> DFFiberConnectionMap);


//======================================================================
void dump_internallink_output_destination_words
(std::ofstream& configuration_file, 
 const uint32_t& this_board_number,
 const std::map<uint32_t, std::pair<uint32_t, uint32_t> >& DFBoardNumberToShelfAndSlotMap,
 const std::map<uint32_t, uint32_t>& DFFiberConnectionMap, 
 const std::map<uint32_t, uint32_t>& ATCAFabricChToOutputBitPosMap);

//======================================================================
void
dump_mod2tower_configuration(std::ofstream& configuration_file,
			     bool isSct,
			     const uint32_t& modid,
			     const uint32_t& destination);

//======================================================================
void
dump_mod2ftkplane_configuration(std::ofstream& configuration_file,
				bool isSct,
				const uint32_t& modid,
				const uint32_t& destination);

//======================================================================
void
dump_mod2dst_configuration(std::ofstream& configuration_file,
			   bool isSct,
			   const uint32_t& modid,
			   const uint32_t& destination);

//======================================================================
void
dump_lut_lanebylane_configuration(std::ofstream& configuration_file,
				  std::string lutname, 
				  const int& lane,
				  const int& address,
				  const int& value);

//======================================================================
void
dump_clkphase_configuration(std::ofstream& configuration_file,
			    const int& lane,
			    const int& inv,
			    const int& delay);

//======================================================================
void
dump_slinkout2nummodules_configuration(std::ofstream& configuration_file,
				       const int& lane, 
				       const int& num_expectedmodules);

//======================================================================
void
dump_fmcin2nummodules_configuration(std::ofstream& configuration_file,
				    const int& lane, 
				    const int& num_expectedmodules);

//======================================================================
void
dump_enable_fmc_lanes_mask_configuration(std::ofstream& configuration_file,
					 const uint32_t& enable_fmc_lanes_mask);

//======================================================================
void
dump_this_board_mask_configuration(std::ofstream& configuration_file,
				   const int& this_board_number);

//======================================================================
void set_wait_ns(std::ofstream& process_time_line, 
		 const int& time_in_ns);

//======================================================================
void set_wait_us(std::ofstream& process_time_line, 
		 const int& time_in_us);

//======================================================================
void set_trigger(std::ofstream& process_time_line);

//======================================================================
void set_lut_pixmod2dst(std::ofstream& process_time_line,
			const uint32_t& address, 
			const uint32_t& data);

//======================================================================
void set_lut_sctmod2dst(std::ofstream& process_time_line,
			const uint32_t& address, 
			const uint32_t& data);

//======================================================================
void set_lut_pixmod2ftkplane(std::ofstream& process_time_line,
			     const uint32_t& address, 
			     const uint32_t& data);

//======================================================================
void set_lut_sctmod2ftkplane(std::ofstream& process_time_line,
			     const uint32_t& address, 
			     const uint32_t& data);

//======================================================================
void set_lut_pixmod2tower(std::ofstream& process_time_line,
			  const uint32_t& address, 
			  const uint32_t& data);

//======================================================================
void set_lut_sctmod2tower(std::ofstream& process_time_line,
			  const uint32_t& address, 
			  const uint32_t& data);

//======================================================================
std::string 
getBin(const int num, uint32_t input);

//======================================================================
bool
UsedOutputLane(const int& iOutLane, 
	       const bool& UsedInTopTower, 
	       const bool& UsedInBotTower, 
	       const int& UsedDFOutputLane);

//======================================================================
void
outputlineTestVector(std::ofstream& coefile,
		     std::ofstream& datfile,
		     const std::string control_word,
		     const uint32_t data,
		     bool eof_coe = false);

//======================================================================
void
CreateTestVector(std::ofstream& datfile,
		 std::ofstream& coefile,
		 std::vector<uint32_t> module_id_list,
		 const int nWordsPerModule,
		 const int nEvents,
		 bool isSctLink
		 );

//======================================================================
uint32_t
ReturnInternalLinkOutputBitMap
(const std::map<uint32_t, uint32_t>& ATCAFabricChToOutputBitPosMap,
 const std::map<uint32_t, uint32_t>& DFFiberConnectionMap, 
 std::map<uint32_t, std::pair<uint32_t, uint32_t> > DFBoardNumberToShelfAndSlotMap, // copy on purpose
 const uint32_t& this_board_number,
 const uint32_t& destination_board_number);

//======================================================================
uint32_t
ReturnFabricBitPositionFromSourceAndDestination
(const uint32_t& source_shelf,
 const uint32_t& source_slot,
 const uint32_t& destination_shelf,
 const uint32_t& destination_slot,
 const uint32_t& DF1, 
 const uint32_t& DF2, 
 std::map<uint32_t, uint32_t> ATCAFabricChToOutputBitPosMap,
 std::map<uint32_t, uint32_t> DFFiberConnectionMap, 
 std::map<uint32_t, std::pair<uint32_t, uint32_t> > DFBoardNumberToShelfAndSlotMap);

//======================================================================
void
set_internallink_output_destination_words
(std::ofstream& process_time_line, 
 const uint32_t& this_board_number,
 const std::map<uint32_t, std::pair<uint32_t, uint32_t> >& DFBoardNumberToShelfAndSlotMap,
 const std::map<uint32_t, uint32_t>& DFFiberConnectionMap, 
 const std::map<uint32_t, uint32_t>& ATCAFabricChToOutputBitPosMap 
 );

//=====================================================================
void ParseBoardConfigFile(std::string board_config_file, 
			  std::multimap<uint32_t, std::pair<uint32_t, uint32_t> >  &rodmap,
  			  std::map<uint32_t, bool>& BoardEnableMap);



//=====================================================================
void ParseSystemConfigFile(std::string system_config_file,
			   std::map<uint32_t, std::pair<uint32_t, uint32_t> > &slotshelf,
			   std::map<uint32_t, uint32_t> & planetooutput,
			   std::map<uint32_t, uint32_t> & TopTowerMap,
			   std::map<uint32_t, uint32_t> & BotTowerMap,
			   std::map<uint32_t, uint32_t> & chantooutbit,
			   std::map<uint32_t, uint32_t> & dfdfconnection,
			   int & NumberOfFTKTowers);

//=====================================================================
void ParseBoardDelayFile(std::string board_delay_file, 
			 std::map<uint32_t, std::pair<uint32_t, uint32_t> >  &delaymap);

//=====================================================================
int GetBoardNumberFromShelfSlot(int this_shelf, int this_slot,
				std::map<uint32_t, std::pair<uint32_t, uint32_t> > DFBoardNumberToShelfAndSlotMap);


//======================================================================
int main(int argc, char* argv[])
{
  
  int result;
  
  int this_board_number = -1;
  int this_shelf = 0;
  int this_slot = 0;
  std::string board_config_file, system_config_file, output_config_file;
  std::string board_delay_file, modulelist_file;

    while((result=getopt(argc,argv,"B:H:L:C:S:O:D:M:X:"))!=-1){
    switch(result){
    case 'B':
      this_board_number = strtoul(optarg, NULL, 0);
      cout<<"Making board configuration file for DF internal board No  "<<optarg;
      break;
    case 'H':
      this_shelf = strtoul(optarg, NULL, 0);
      cout<<"Making board configuration file for shelf  "<<optarg;
      break;
    case 'L':
      this_slot = strtoul(optarg, NULL, 0);
      cout<<" slot  "<<optarg<<endl;
      break;
    case 'C':
      board_config_file = optarg;
      std::cout<<"Using Board Configuration File " <<
	board_config_file<<std::endl;
      break;
    case 'S':
      system_config_file = optarg;
      std::cout<<"Using System Configuration File " <<
	system_config_file<<std::endl;
      break;
    case 'D':
      board_delay_file = optarg;
      std::cout<<"Using Board Delay File " <<
	board_delay_file<<std::endl;
      break;
    case 'O':
      output_config_file = optarg;
      std::cout<<"Creating output configuration file  " <<
	output_config_file<<std::endl;
      break;
    case 'M':
      modulelist_file = optarg;
      std::cout<<"Using modulelist  " <<
	modulelist_file<<std::endl;
      break;
    case 'X':
      modulelist_file = optarg;
      std::cout<<"Using modulelist  " <<
	modulelist_file<<std::endl;
      break;
    default:
      printf("%s -B <board id (0-31)> -C <boardconfigfile.txt> -S <sysconfigfile.txt> -D <delay value file> -M <module list file>  -O <output_config_name>\n", argv[0]);
      exit(EXIT_FAILURE);
    }
  }
  

   
  std::ofstream process_time_line("process_time_line.vhd");
  std::ofstream configuration_file(output_config_file.c_str());
  char BUF[BUFSIZ];
  
  std::ifstream module_list_file(modulelist_file.c_str());
    
  if (module_list_file.fail() || module_list_file.bad()){
    printf("Failed to open modulelist! Exiting.\n");
    exit(EXIT_FAILURE);
    
  }


  std::string       line_str;
  
  // ==========================================================
  // -------------- CONFIGURATION PARAMETERS ------------------
  // --------- BOARD CONFIGURATION IS HARD CODED HERE ---------
  // Start
  std::multimap<uint32_t, std::pair<uint32_t, uint32_t> > RodIdToFMCInIdMap;
  std::map<uint32_t, bool> BoardEnableMap;
  ParseBoardConfigFile(board_config_file,RodIdToFMCInIdMap,BoardEnableMap);

  // ======================================================================
  // MODIFY from HERE (2/2)
  // ======================================================================
  
  std::map<uint32_t, std::pair<uint32_t, uint32_t> >
    DFBoardNumberToShelfAndSlotMap;
  std::map<uint32_t, uint32_t> FTKPlaneTODFOutputMap ;
  std::map<uint32_t, uint32_t> DFBoardNumberToBotTowerMap;
  std::map<uint32_t, uint32_t> DFBoardNumberToTopTowerMap;
  std::map<uint32_t, uint32_t> ATCAFabricChToOutputBitPosMap;
  std::map<uint32_t, uint32_t> DFFiberConnectionMap;
  std::map<uint32_t, std::pair<uint32_t,uint32_t> > LaneToInvDelayMap;

  ParseBoardDelayFile(board_delay_file,
		      LaneToInvDelayMap);

  ParseSystemConfigFile(system_config_file,
			DFBoardNumberToShelfAndSlotMap,
			FTKPlaneTODFOutputMap,
			DFBoardNumberToTopTowerMap,
			DFBoardNumberToBotTowerMap,
			ATCAFabricChToOutputBitPosMap,
			DFFiberConnectionMap,
			NumberOfFTKTower);
    
  if (DFBoardNumberToShelfAndSlotMap.size()==0){
    printf("Could not fill Shelf and Slot Map!  Exiting!");
    exit(EXIT_FAILURE);
  }

  //get internal board number from shelf and slot
  if (this_board_number==-1 && ((this_slot <1) ||  (this_shelf < 1)) ) {
    printf("%s -B <board id (0-31)> [or -H <board shelf> -L <board slot> ] -C <boardconfigfile.txt> -S <sysconfigfile.txt> -O <output_config_name>\n", argv[0]);
    exit(EXIT_FAILURE);    
  }else if(this_board_number > 0 && (this_slot > 0 || this_shelf > 0)){
    printf("Should not specify both internal DF number and shelf & slot number!!  Exiting!");
    exit(EXIT_FAILURE);
  }else if (this_board_number==-1 && (this_slot > 0) && (this_shelf > 0) ) {
    this_board_number=GetBoardNumberFromShelfSlot(this_shelf,this_slot,DFBoardNumberToShelfAndSlotMap);
    printf("Converted to board number %d ", this_board_number);
  }
  srand(this_board_number);

  
  if (FTKPlaneTODFOutputMap.size()==0){
    printf("Could not fill Output Plane Map!  Exiting!");
    exit(EXIT_FAILURE);
  }
  if (DFBoardNumberToTopTowerMap.size()==0){
    printf("Could not fill Top Tower Map!  Exiting!");
    exit(EXIT_FAILURE);
  }
  if (DFBoardNumberToBotTowerMap.size()==0){
    printf("Warning: could not fill Bottom Tower Map!  OK for 32 tower configuration \n");
    //exit(EXIT_FAILURE);
  }

  printf("Using system with %d towers", NumberOfFTKTower);


 
  // ===========================================================
  // Set Mapping
  // ===========================================================
  
     
  // create map (inversed table - Tower to Board Number)
  std::map<uint32_t, uint32_t> TowerToDFBoardNumberMap;
  for (int iTower=0; iTower<NumberOfFTKTower; iTower++) {
    bool current_tower_found = false;
    std::map<uint32_t, uint32_t>::const_iterator top_it_c = DFBoardNumberToTopTowerMap.begin();
    const std::map<uint32_t, uint32_t>::const_iterator top_it_e = DFBoardNumberToTopTowerMap.end();
    std::map<uint32_t, uint32_t>::const_iterator bot_it_c = DFBoardNumberToBotTowerMap.begin();
    const std::map<uint32_t, uint32_t>::const_iterator bot_it_e = DFBoardNumberToBotTowerMap.end();
    
    for (; top_it_c!=top_it_e; top_it_c++) {

    
      if (top_it_c->second == iTower) {
	TowerToDFBoardNumberMap.insert(std::pair<uint32_t, uint32_t>(top_it_c->second, top_it_c->first));
 
	current_tower_found = true;
	break;
      }
    }
    
    if (current_tower_found) continue;
    
    for (; bot_it_c!=bot_it_e; bot_it_c++) {
      if (bot_it_c->second == iTower) {
	TowerToDFBoardNumberMap.insert(std::pair<uint32_t, uint32_t>(bot_it_c->second, bot_it_c->first));
	current_tower_found = true;
	break;
      }
    }
  }
  
  int youngest_input_id = NumberOfFMCInputLanesPerBoard; // initial vealue
  // this time, it is equal to the RODs coming into this board
  // generally it should be all the RODs contributing to the test or operation
  
  uint32_t enable_fmc_lanes_mask = 0X0;
  std::multimap<uint32_t, std::pair<uint32_t, uint32_t> >::const_iterator rods_itc = RodIdToFMCInIdMap.begin();
  const std::multimap<uint32_t, std::pair<uint32_t, uint32_t> >::const_iterator rods_ite = RodIdToFMCInIdMap.end();
  for (; rods_itc!=rods_ite; rods_itc++) {
    const uint32_t& boardId_for_the_rod = rods_itc->second.first;
    if (this_board_number != boardId_for_the_rod) continue;
    
    const uint32_t& fmc_id = rods_itc->second.second;
    //printf("%s %5d dbg> %4d (0X%04x) \n", __PRETTY_FUNCTION__, __LINE__, fmc_id, enable_fmc_lanes_mask);
    if (fmc_id<youngest_input_id) {youngest_input_id=fmc_id;}
    enable_fmc_lanes_mask = (enable_fmc_lanes_mask | (0X1<<fmc_id));
  }
  
  std::map<uint32_t, uint32_t> pixmod2dst_map;
  std::map<uint32_t, uint32_t> sctmod2dst_map;
  std::map<uint32_t, uint32_t> pixmod2ftkplane_map;
  std::map<uint32_t, uint32_t> sctmod2ftkplane_map;
  std::map<uint32_t, uint32_t> pixmod2tower_map;
  std::map<uint32_t, uint32_t> sctmod2tower_map;
  std::map<int, std::vector<uint32_t> > fmcin2modid_map;
  
  // make tower mask
  const int& top_tower = DFBoardNumberToTopTowerMap[this_board_number];
  const int& bot_tower = DFBoardNumberToBotTowerMap[this_board_number];
  //printf("dbg> top tower=%d bot tower=%d \n", top_tower, bot_tower);
//  const uint32_t top_tower_map_mask_00_31 = (top_tower<32) ? (0X1<<top_tower) : 0X0;
//  const uint32_t top_tower_map_mask_32_63 = (top_tower<32) ? 0X0              : (0X1<<(top_tower-32));
//  const uint32_t bot_tower_map_mask_00_31 = (bot_tower<32) ? (0X1<<bot_tower) : 0X0;
//  const uint32_t bot_tower_map_mask_32_63 = (bot_tower<32) ? 0X0              : (0X1<<(bot_tower-32));

  uint32_t top_tower_map_mask_00_31 = (top_tower<32) ? (0X1<<top_tower) : 0X0;
  uint32_t top_tower_map_mask_32_63 = (top_tower<32) ? 0X0              : (0X1<<(top_tower-32));
  uint32_t bot_tower_map_mask_00_31 = (bot_tower<32) ? (0X1<<bot_tower) : 0X0;
  uint32_t bot_tower_map_mask_32_63 = (bot_tower<32) ? 0X0              : (0X1<<(bot_tower-32));

  if (SkipBotTower){
    bot_tower_map_mask_00_31 = 0X0;
    bot_tower_map_mask_32_63 = 0X0;

  }
  
  // input module counters per FMC 
  std::vector<int> module_counter_in(NumberOfFMCInputLanesPerBoard);
  for (int ii=0; ii<module_counter_in.size(); ii++) {
    module_counter_in[ii] = 0;
  }
  
  // output module counter per output lanes  
  std::vector<int> module_counter_out(NumberOfOutputLanesPerBoard);
  for (int ii=0; ii<module_counter_out.size(); ii++) {
    module_counter_out[ii] = 0;
  }
  
  // module loop
  while (getline(module_list_file, line_str))  {
    std::stringstream line_ss(line_str);
    std::string       c1, c2, c3, c4, c5;
    line_ss >> c1 >> c2 >> c3 >> c4 >> c5;
    
    const uint32_t ROBID          = strtoul(c1.c_str(), NULL,  0);
    
    // skip modules in RODs which do not contribute to this run / operation / test 
    const int nLinks = (int) RodIdToFMCInIdMap.count(ROBID);
    
    //    std::cout<<nLinks<<std::endl;
    if (nLinks==0){ continue;} // usually 0 or 1, if we allow the same input in several links it could be >1.
    
    std::multimap<uint32_t, std::pair<uint32_t, uint32_t> >::const_iterator itr = RodIdToFMCInIdMap.find(ROBID);    
    for (int iLink=0; iLink<nLinks; iLink++) {      
      const uint32_t MODULEID       = strtoul(c2.c_str(), NULL,  0);
      const uint32_t TowerMap_00_31 = strtoul(c3.c_str(), NULL,  0);
      const uint32_t TowerMap_32_63 = strtoul(c4.c_str(), NULL,  0);
      const uint32_t PLANEID        = strtoul(c5.c_str(), NULL,  0);
    
      bool thisBoardIsTheFirstDFBoardForTheModule = false;
    
      const uint32_t& boardId_for_the_rod = itr->second.first;


      //printf("dbg> %d\n", boardId_for_the_rod);
      if (boardId_for_the_rod==this_board_number) {
	thisBoardIsTheFirstDFBoardForTheModule=true;
      }
    
      const uint32_t& FMCLaneID     = itr->second.second;
      const uint32_t& DFOutputLane  = FTKPlaneTODFOutputMap[PLANEID];

      //printf("dbg> B=%d FMC=%d 0x%0x \n", boardId_for_the_rod, FMCLaneID, ROBID);

      
      //printf("dbg> %d %d [%d]\n", FTKPlaneTODFOutputMap[PLANEID], PLANEID, __LINE__);    
    
      bool isSct(false);
      if ((ROBID&0XF00000) == 0X200000){ isSct=true; }
    
      // Calculate Destination Bit Mask
      uint32_t destination_bit = 0X0;
      for (int iTower=0; iTower<NumberOfFTKTower; iTower++) {      
	if (iTower<32) {
	  const int iBitPos = iTower;
	  if (((0X1<<iBitPos)&TowerMap_00_31) != 0X0) {
	    const int dfBoardNumber = TowerToDFBoardNumberMap[iTower];
	    destination_bit = (destination_bit | (0X1<<dfBoardNumber));
	  }
	} else {
	  const int iBitPos = iTower-32;
	  if (((0X1<<iBitPos)&TowerMap_32_63) != 0X0) {
	    const int dfBoardNumber = TowerToDFBoardNumberMap[iTower];
	    destination_bit = (destination_bit | (0X1<<dfBoardNumber));
	  }
	}
      }
    
      // check top / bot tower
      const bool MatchToTopTower = 
	( ((top_tower_map_mask_00_31&TowerMap_00_31) != 0X0) or
	  ((top_tower_map_mask_32_63&TowerMap_32_63) != 0X0) );
      const bool MatchToBotTower = 
	( ((bot_tower_map_mask_00_31&TowerMap_00_31) != 0X0) or
	  ((bot_tower_map_mask_32_63&TowerMap_32_63) != 0X0) );
    
      bool thisBoardIsIncludedInDestinationList = (MatchToTopTower or MatchToBotTower);
    
      //printf("dbg> 0x%08x 0x%08x 0x%08x 0x%08x [%d] \n", top_tower_map_mask_00_31, TowerMap_00_31, top_tower_map_mask_32_63, TowerMap_32_63, __LINE__);
    
      //printf("dbg> 0x%08x 0x%08x 0x%08x 0x%08x [%d] \n", bot_tower_map_mask_00_31, TowerMap_00_31, bot_tower_map_mask_32_63, TowerMap_32_63, __LINE__);
    
      uint32_t tower_mask = 0X0;
      if (MatchToTopTower) { tower_mask = (tower_mask | 0X1); }
      if (MatchToBotTower) { tower_mask = (tower_mask | 0X2); }
      
    
      // If The Module is included in direct "INPUT" for the board from FMC
      if (thisBoardIsTheFirstDFBoardForTheModule) {
	module_counter_in[FMCLaneID]++;
	if (fmcin2modid_map.find(FMCLaneID) == fmcin2modid_map.end()) {
	  std::vector<uint32_t> dummy;
	  fmcin2modid_map.insert(std::pair<int, std::vector<uint32_t> >(FMCLaneID, dummy) );
	}

      
	if (isSct) { // is SCT
	  sctmod2dst_map.insert     (std::pair<uint32_t, uint32_t>(MODULEID, destination_bit)); // common for all boards
	} else { // is Pixel
	  pixmod2dst_map.insert     (std::pair<uint32_t, uint32_t>(MODULEID, destination_bit)); // common for all boards
	}
      
	fmcin2modid_map[FMCLaneID].push_back(MODULEID);
      }
    
      if (thisBoardIsIncludedInDestinationList) { // VERY temporal situation !!
	for (int iOutLane=0; iOutLane<NumberOfOutputLanesPerBoard; iOutLane++) {
	  if (UsedOutputLane(iOutLane, MatchToTopTower, MatchToBotTower, DFOutputLane)) {
	    //printf("dbg> : %d [%d]\n", iOutLane, __LINE__);
	    module_counter_out[iOutLane]++;
	  }
	}
      
	if (isSct) { // is SCT
	  sctmod2ftkplane_map.insert(std::pair<uint32_t, uint32_t>(MODULEID, (0X1<<DFOutputLane) )); // common for all boards
	  sctmod2tower_map.insert   (std::pair<uint32_t, uint32_t>(MODULEID, tower_mask)); // board by board different
	} else { // is Pixel
	  pixmod2ftkplane_map.insert(std::pair<uint32_t, uint32_t>(MODULEID, (0X1<<DFOutputLane) )); // common for all boards
	  pixmod2tower_map.insert   (std::pair<uint32_t, uint32_t>(MODULEID, tower_mask)); // board by board different
	}
      }
      itr++;
    }
  }
  
  dump_configuration_file(configuration_file,
			  this_board_number,
			  module_counter_in,
			  module_counter_out, 
			  enable_fmc_lanes_mask,
			  fmcin2modid_map,
			  pixmod2dst_map,
			  sctmod2dst_map,
			  pixmod2ftkplane_map,
			  sctmod2ftkplane_map,
			  pixmod2tower_map,
			  sctmod2tower_map,
			  DFBoardNumberToShelfAndSlotMap,
			  DFFiberConnectionMap,
			  ATCAFabricChToOutputBitPosMap,
			  RodIdToFMCInIdMap,
			  LaneToInvDelayMap,
			  BoardEnableMap
			  );
  
  dump_configuration_timeline_to_file(process_time_line,
				      youngest_input_id,
				      this_board_number,
				      module_counter_in,
				      module_counter_out, 
				      enable_fmc_lanes_mask,
				      pixmod2dst_map,
				      sctmod2dst_map,
				      pixmod2ftkplane_map,
				      sctmod2ftkplane_map,
				      pixmod2tower_map,
				      sctmod2tower_map,
				      DFBoardNumberToShelfAndSlotMap,
				      DFFiberConnectionMap,
				      ATCAFabricChToOutputBitPosMap 
				      );
  
  if (dumpSummary) {
    printf("======== SUMMARY ========\n");
    dump_configuration(youngest_input_id,
		       this_board_number,
		       module_counter_in,
		       module_counter_out, 
		       enable_fmc_lanes_mask,
		       pixmod2dst_map,
		       sctmod2dst_map,
		       pixmod2ftkplane_map,
		       sctmod2ftkplane_map,
		       pixmod2tower_map,
		       sctmod2tower_map
		       );
  }
  
}

//======================================================================
void
dump_rx_ports_to_file(std::ofstream& configuration_file,
		      int this_board_number,
		      const std::map<uint32_t, std::pair<uint32_t, uint32_t> >& DFBoardNumberToShelfAndSlotMap,
		      const std::map<uint32_t, bool>& BoardEnableMap,
		      const std::map<uint32_t, uint32_t> DFFiberConnectionMap)
{
  uint32_t ports=0;
  std::pair<uint32_t, uint32_t> shelfAndSlot = DFBoardNumberToShelfAndSlotMap.find(this_board_number)->second;
  int shelf = shelfAndSlot.first;
  int slot = shelfAndSlot.second;
  for (std::map<uint32_t, bool>::const_iterator itr=BoardEnableMap.begin();itr!=BoardEnableMap.end();itr++)
  {
    //bool en = itr->second;                                                                                                                                                               
    //int board = itr->first;                                                                                                                                                              
    //std::cout << "board: " << board << ", en=" << en << std::endl;                                                                                                                       
    if (itr->second == 0) continue;
    std::pair<uint32_t, uint32_t> this_shelfAndSlot = DFBoardNumberToShelfAndSlotMap.find(itr->first)->second;
    int this_shelf = this_shelfAndSlot.first;
    int this_slot = this_shelfAndSlot.second;
    if (shelf == this_shelf)
    {
      if (slot == this_slot) continue;
      int offset=3;
      if (this_slot > slot) offset=4;
      ports |= (0x1<<(this_slot-offset));
      ports |= (0x1<<(this_slot-offset+12));
      //cout << "port is now " << std::hex << ports << std::dec << endl;                                                                                                                   
    }
    else
    {
      //if the two boards are connected via inter-shelf QSFP, specify the additional rx ports to open                                                                                      
      uint32_t shelf_mask = 0x400400;
      if ((DFFiberConnectionMap.find(this_board_number) != DFFiberConnectionMap.end() && DFFiberConnectionMap.find(this_board_number)->second == itr->first) ||
          (DFFiberConnectionMap.find(itr->first) != DFFiberConnectionMap.end() && DFFiberConnectionMap.find(itr->first)->second == this_board_number))
      {
        ports |= shelf_mask;
      }
    }
  }


/*
  uint32_t ports=0;
  std::pair<uint32_t, uint32_t> shelfAndSlot = DFBoardNumberToShelfAndSlotMap.find(this_board_number)->second;
  int slot = shelfAndSlot.second;
  for (std::map<uint32_t, bool>::const_iterator itr=BoardEnableMap.begin();itr!=BoardEnableMap.end();itr++)
  {
    bool en = itr->second;
    int board = itr->first;
    //std::cout << "board: " << board << ", en=" << en << std::endl;
    if (itr->second == 0) continue;
    std::pair<uint32_t, uint32_t> this_shelfAndSlot = DFBoardNumberToShelfAndSlotMap.find(itr->first)->second;
    int this_slot = this_shelfAndSlot.second;
    if (slot == this_slot) continue;

    int offset=3;
    if (this_slot > slot) offset=4;
    ports |= (0x1<<(this_slot-offset));
    ports |= (0x1<<(this_slot-offset+12));
    //cout << "port is now " << std::hex << ports << std::dec << endl;
    }*/
  configuration_file << "rxports " << ports << std::endl;
}

//======================================================================
void
dump_this_board_mask_configuration(std::ofstream& configuration_file,
				   const int& this_board_number) {
  char buf[BUFSIZ];
  snprintf(buf, sizeof(buf), "%-35s 0X%08x", "this_board_mask", (0X1<<this_board_number));
  configuration_file << buf << std::endl;
}

//======================================================================
void
dump_enable_fmc_lanes_mask_configuration(std::ofstream& configuration_file,
					 const uint32_t& enable_fmc_lanes_mask) {
  char buf[BUFSIZ];
  snprintf(buf, sizeof(buf), "%-35s 0X%08x", "enable_fmc_lanes_mask", enable_fmc_lanes_mask);
  configuration_file << buf << std::endl;
}

//======================================================================
void
dump_fmcin2nummodules_configuration(std::ofstream& configuration_file,
				    const int& lane, 
				    const int& num_expectedmodules) {
  char buf[BUFSIZ];
  snprintf(buf, sizeof(buf), "%-35s %10d %10d", "fmcin2nummodules", lane, num_expectedmodules);
  configuration_file << buf << std::endl;
}

//======================================================================
void
dump_slinkout2nummodules_configuration(std::ofstream& configuration_file,
				       const int& lane, 
				       const int& num_expectedmodules) {
  char buf[BUFSIZ];
  snprintf(buf, sizeof(buf), "%-35s %10d %10d", "slinkout2nummodules", lane, num_expectedmodules);
  configuration_file << buf << std::endl;
}

//======================================================================
void
dump_clkphase_configuration(std::ofstream& configuration_file,
			    const int& lane,
			    const int& inv,
			    const int& delay) {
  char buf[BUFSIZ];
  snprintf(buf, sizeof(buf), "%-35s %10d %10d", "imfpga2clkinv", lane, inv);
  configuration_file << buf << std::endl;
  snprintf(buf, sizeof(buf), "%-35s %10d %10d", "imfpga2clkdelay", lane, delay);
  configuration_file << buf << std::endl;
}

//======================================================================
void
dump_lut_lanebylane_configuration(std::ofstream& configuration_file,
				  std::string lutname, 
				  const int& lane,
				  const int& address,
				  const int& value) {
  char address_char[BUFSIZ];
  snprintf(address_char, sizeof(address_char), "0X%04x", address);
  char command[BUFSIZ];
  snprintf(command, sizeof(command), "%-35s %10d %10s %10d", lutname.c_str(), lane, address_char, value);
  configuration_file << command << std::endl;
}

//======================================================================
void
dump_mod2dst_configuration(std::ofstream& configuration_file,
			   bool isSct,
			   const uint32_t& modid,
			   const uint32_t& destination) {
  char address_char[BUFSIZ];
  snprintf(address_char, sizeof(address_char), "0X%04x", modid);
  char value_char[BUFSIZ];
  snprintf(value_char, sizeof(value_char), "0X%08x", destination);
  
  char command[BUFSIZ];
  snprintf(command, sizeof(command), "%-35s %10s %10s", (isSct) ? "sctmod2dst" : "pixmod2dst", address_char, value_char);
  configuration_file << command << std::endl;
}

//======================================================================
void
dump_mod2ftkplane_configuration(std::ofstream& configuration_file,
				bool isSct,
				const uint32_t& modid,
				const uint32_t& destination) {
//  uint32_t dest;
//  if (destination ==1){
//    dest = 0;
//  }
//  if (destination ==2){
//    dest = 1;
//  }
//  if (destination ==4){
//    dest = 2;
//  }
//  if (destination ==8){
//    dest = 3;
//  }
//  if (destination ==16){
//    dest = 4;
//  }
//  if (destination ==32){
//    dest = 5;
//  }
//  if (destination ==64){
//    dest = 6;
//  }
//  if (destination ==128){
//    dest = 7;
//  }
//  if (destination ==256){
//    dest = 8;
//  }
//  if (destination ==512){
//    dest = 9;
//  }
//  if (destination ==1024){
//    dest = 10;
//  }
//  if (destination ==2048){
//    dest = 11;
//  }

  
  char address_char[BUFSIZ];
  snprintf(address_char, sizeof(address_char), "0X%04x", modid);
  char value_char[BUFSIZ];
  snprintf(value_char, sizeof(value_char), "0X%04x", destination);
  
  char command[BUFSIZ];
  snprintf(command, sizeof(command), "%-35s %10s %10s", (isSct) ? "sctmod2ftkplane" : "pixmod2ftkplane", address_char, value_char);
  configuration_file << command << std::endl;
}

//======================================================================
void
dump_mod2tower_configuration(std::ofstream& configuration_file,
			     bool isSct,
			     const uint32_t& modid,
			     const uint32_t& destination) {
  char address_char[BUFSIZ];
  snprintf(address_char, sizeof(address_char), "0X%04x", modid);
  char value_char[BUFSIZ];
  snprintf(value_char, sizeof(value_char), "0X%1x", destination);
  
  char command[BUFSIZ];
  snprintf(command, sizeof(command), "%-35s %10s %10s", (isSct) ? "sctmod2tower" : "pixmod2tower", address_char, value_char);
  configuration_file << command << std::endl;
}

//======================================================================
void dump_configuration_file(std::ofstream& configuration_file,
			     const uint32_t& this_board_number,
			     const std::vector<int>& module_counter_in, //[NumberOfFMCInputLanesPerBoard]
			     const std::vector<int>& module_counter_out, //[NumberOfOutputLanesPerBoard]
			     const uint32_t& enable_fmc_lanes_mask, //[bit mask for 16bit]
			     const std::map<int, std::vector<uint32_t> >& fmcin2modid_map,
			     const std::map<uint32_t, uint32_t>& pixmod2dst_map,
			     const std::map<uint32_t, uint32_t>& sctmod2dst_map,
			     const std::map<uint32_t, uint32_t>& pixmod2ftkplane_map,
			     const std::map<uint32_t, uint32_t>& sctmod2ftkplane_map,
			     const std::map<uint32_t, uint32_t>& pixmod2tower_map,
			     const std::map<uint32_t, uint32_t>& sctmod2tower_map,
			     const std::map<uint32_t, std::pair<uint32_t, uint32_t> >& DFBoardNumberToShelfAndSlotMap,
			     const std::map<uint32_t, uint32_t>& DFFiberConnectionMap,
			     const std::map<uint32_t, uint32_t>& ATCAFabricChToOutputBitPosMap,
			     const std::multimap<uint32_t, std::pair<uint32_t, uint32_t> >& RodIdToFMCInIdMap,
			     const std::map<uint32_t, std::pair<uint32_t,uint32_t> >&  LaneToInvDelayMap,
			     const std::map<uint32_t, bool>& BoardEnableMap
)
{
  dump_this_board_mask_configuration(configuration_file, this_board_number);
  
  // 
  std::map<uint32_t, std::pair<uint32_t, uint32_t> >::const_iterator dl_itc = LaneToInvDelayMap.begin();
  const std::map<uint32_t, std::pair<uint32_t, uint32_t> >::const_iterator dl_ite = LaneToInvDelayMap.end();
  for (; dl_itc!=dl_ite; dl_itc++) 
    dump_clkphase_configuration(configuration_file, dl_itc->first,(dl_itc->second).first, (dl_itc->second).second); // lane, inv, delay


  //
  std::multimap<uint32_t, std::pair<uint32_t, uint32_t> >::const_iterator rods_itc = RodIdToFMCInIdMap.begin();
  const std::map<uint32_t, std::pair<uint32_t, uint32_t> >::const_iterator rods_ite = RodIdToFMCInIdMap.end();
  for (; rods_itc!=rods_ite; rods_itc++) {
    // std::map<int, std::vector<uint32_t> >::const_iterator fmcin2modid_map_ite_cur = fmcin2modid_map.begin();
    // const std::map<int, std::vector<uint32_t> >::const_iterator fmcin2modid_map_ite_end = fmcin2modid_map.end();
    // for (; fmcin2modid_map_ite_cur!=fmcin2modid_map_ite_end; fmcin2modid_map_ite_cur++) { // lane loop
    const uint32_t& boardId  = rods_itc->second.first;
    const uint32_t& ROBID    = rods_itc->first;
    const uint32_t& lane     = rods_itc->second.second;
    
    if (this_board_number!=boardId) { continue; } // skip input RODs arriving at other boards
    
    bool isSct(false);
    if ((ROBID&0XF00000) == 0X200000){ isSct=true; }
    
    std::map<int, std::vector<uint32_t> >::const_iterator fmcin2modid_map_ite_cur = fmcin2modid_map.begin();
    const std::map<int, std::vector<uint32_t> >::const_iterator fmcin2modid_map_ite_end = fmcin2modid_map.end();
    
    if (fmcin2modid_map.find(lane)==fmcin2modid_map.end()) {
      fprintf(stderr, "ERR> lane=%d is not mapped for the board = %d\n", lane, this_board_number);
      exit(EXIT_FAILURE);
    }
    const std::vector<uint32_t>& moduleIds = fmcin2modid_map.find(lane)->second;
    
    const int nMods=moduleIds.size();
    dump_fmcin2nummodules_configuration(configuration_file, lane, nMods);
    
    for (int iMod=0; iMod<nMods; iMod++) {
      const uint32_t& moduleId = (isSct) ? (moduleIds.at(iMod)|0x2000) : moduleIds.at(iMod);
      dump_lut_lanebylane_configuration(configuration_file, "lane_mod2idx", lane, moduleId, iMod);
      dump_lut_lanebylane_configuration(configuration_file, "lane_idx2mod", lane, iMod, moduleId);
    }
  }
  
  std::map<uint32_t, uint32_t>::const_iterator pixmod2dst_map_ite_cur = pixmod2dst_map.begin();
  std::map<uint32_t, uint32_t>::const_iterator sctmod2dst_map_ite_cur = sctmod2dst_map.begin();
  std::map<uint32_t, uint32_t>::const_iterator pixmod2ftkplane_map_ite_cur = pixmod2ftkplane_map.begin();
  std::map<uint32_t, uint32_t>::const_iterator sctmod2ftkplane_map_ite_cur = sctmod2ftkplane_map.begin();
  std::map<uint32_t, uint32_t>::const_iterator pixmod2tower_map_ite_cur = pixmod2tower_map.begin();
  std::map<uint32_t, uint32_t>::const_iterator sctmod2tower_map_ite_cur = sctmod2tower_map.begin();
  
  const std::map<uint32_t, uint32_t>::const_iterator pixmod2dst_map_ite_end = pixmod2dst_map.end();
  const std::map<uint32_t, uint32_t>::const_iterator sctmod2dst_map_ite_end = sctmod2dst_map.end();
  const std::map<uint32_t, uint32_t>::const_iterator pixmod2ftkplane_map_ite_end = pixmod2ftkplane_map.end();
  const std::map<uint32_t, uint32_t>::const_iterator sctmod2ftkplane_map_ite_end = sctmod2ftkplane_map.end();
  const std::map<uint32_t, uint32_t>::const_iterator pixmod2tower_map_ite_end = pixmod2tower_map.end();
  const std::map<uint32_t, uint32_t>::const_iterator sctmod2tower_map_ite_end = sctmod2tower_map.end();
  
  //======================================================================
  for (; pixmod2dst_map_ite_cur!=pixmod2dst_map_ite_end; pixmod2dst_map_ite_cur++) {
    const bool isSct = false;
    const uint32_t& modid = pixmod2dst_map_ite_cur->first;
    const uint32_t& destination = pixmod2dst_map_ite_cur->second;
    dump_mod2dst_configuration(configuration_file, isSct, modid, destination);
  }
  
  for (; pixmod2ftkplane_map_ite_cur!=pixmod2ftkplane_map_ite_end; pixmod2ftkplane_map_ite_cur++) {
    const bool isSct = false;
    const uint32_t& modid = pixmod2ftkplane_map_ite_cur->first;
    const uint32_t& ftkplane = pixmod2ftkplane_map_ite_cur->second;
    dump_mod2ftkplane_configuration(configuration_file, isSct, modid, ftkplane);
  }
  
  for (; pixmod2tower_map_ite_cur!=pixmod2tower_map_ite_end; pixmod2tower_map_ite_cur++) {
    const bool isSct = false;
    const uint32_t& modid = pixmod2tower_map_ite_cur->first;
    const uint32_t& tower = pixmod2tower_map_ite_cur->second;
    dump_mod2tower_configuration(configuration_file, isSct, modid, tower);
  }
  
  //======================================================================
  for (; sctmod2dst_map_ite_cur!=sctmod2dst_map_ite_end; sctmod2dst_map_ite_cur++) {
    const bool isSct = true;
    const uint32_t& modid = sctmod2dst_map_ite_cur->first;
    const uint32_t& destination = sctmod2dst_map_ite_cur->second;
    dump_mod2dst_configuration(configuration_file, isSct, modid, destination);
  }
  
  for (; sctmod2ftkplane_map_ite_cur!=sctmod2ftkplane_map_ite_end; sctmod2ftkplane_map_ite_cur++) {
    const bool isSct = true;
    const uint32_t& modid = sctmod2ftkplane_map_ite_cur->first;
    const uint32_t& ftkplane = sctmod2ftkplane_map_ite_cur->second;
    dump_mod2ftkplane_configuration(configuration_file, isSct, modid, ftkplane);
  }
  
  for (; sctmod2tower_map_ite_cur!=sctmod2tower_map_ite_end; sctmod2tower_map_ite_cur++) {
    const bool isSct = true;
    const uint32_t& modid = sctmod2tower_map_ite_cur->first;
    const uint32_t& tower = sctmod2tower_map_ite_cur->second;
    dump_mod2tower_configuration(configuration_file, isSct, modid, tower);
  }
  
  for (int iSlinkOut=0, nSlinkOuts=module_counter_out.size(); iSlinkOut<nSlinkOuts; iSlinkOut++) {
    dump_slinkout2nummodules_configuration(configuration_file, iSlinkOut, module_counter_out.at(iSlinkOut));
  }
  
  dump_enable_fmc_lanes_mask_configuration(configuration_file, enable_fmc_lanes_mask);
  
  dump_internallink_output_destination_words
    (configuration_file, this_board_number, DFBoardNumberToShelfAndSlotMap, DFFiberConnectionMap, ATCAFabricChToOutputBitPosMap);

  dump_rx_ports_to_file(configuration_file, this_board_number, DFBoardNumberToShelfAndSlotMap, BoardEnableMap, DFFiberConnectionMap);

}

//======================================================================
void dump_configuration_timeline_to_file(std::ofstream& process_time_line,
					 const int& youngest_input_id,
					 const uint32_t& this_board_number,
					 const std::vector<int>& module_counter_in, //[NumberOfFMCInputLanesPerBoard]
					 const std::vector<int>& module_counter_out, //[NumberOfOutputLanesPerBoard]
					 const uint32_t& enable_fmc_lanes_mask, //[bit mask for 16bit]
					 const std::map<uint32_t, uint32_t>& pixmod2dst_map,
					 const std::map<uint32_t, uint32_t>& sctmod2dst_map,
					 const std::map<uint32_t, uint32_t>& pixmod2ftkplane_map,
					 const std::map<uint32_t, uint32_t>& sctmod2ftkplane_map,
					 const std::map<uint32_t, uint32_t>& pixmod2tower_map,
					 const std::map<uint32_t, uint32_t>& sctmod2tower_map,
					 const std::map<uint32_t, std::pair<uint32_t, uint32_t> >& DFBoardNumberToShelfAndSlotMap,
					 const std::map<uint32_t, uint32_t>& DFFiberConnectionMap,
					 const std::map<uint32_t, uint32_t>& ATCAFabricChToOutputBitPosMap 
					 )
{
  const uint32_t this_board_mask = (0X1<<this_board_number);
  
  process_time_line << "SWITCHINGMODE : if (NO_SWITCH_MODE = 0) generate begin " 
		    << "output_slink_packer_number_of_expected_modules_i <= ("
		    << module_counter_out[0] << ","
		    << module_counter_out[1] << ","
		    << module_counter_out[2] << ","
		    << module_counter_out[3] << ","
		    << module_counter_out[4] << ","
		    << module_counter_out[5] << ","
		    << module_counter_out[6] << ","
		    << module_counter_out[7] << ","
		    << module_counter_out[8] << ","
		    << module_counter_out[9] << ","
		    << module_counter_out[10] << ","
		    << module_counter_out[11] << ","
		    << module_counter_out[12] << ","
		    << module_counter_out[13] << ","
		    << module_counter_out[14] << ","
		    << module_counter_out[15] << ","
		    << module_counter_out[16] << ","
		    << module_counter_out[17] << ","
		    << module_counter_out[18] << ","
		    << module_counter_out[19] << ","
		    << module_counter_out[20] << ","
		    << module_counter_out[21] << ","
		    << module_counter_out[22] << ","
		    << module_counter_out[23] << ","
		    << module_counter_out[24] << ","
		    << module_counter_out[25] << ","
		    << module_counter_out[26] << ","
		    << module_counter_out[27] << ","
		    << module_counter_out[28] << ","
		    << module_counter_out[29] << ","
		    << module_counter_out[30] << ","
		    << module_counter_out[31] << ","
		    << module_counter_out[32] << ","
		    << module_counter_out[33]
		    << "); end generate; " << std::endl;
  process_time_line << "NOSWITCHINGMODE : if (NO_SWITCH_MODE = 1) generate begin " 
		    << "output_slink_packer_number_of_expected_modules_i <= ("
		    << (module_counter_in[0] + module_counter_in[1]) << ",0,0,0,"
		    << (module_counter_in[2] + module_counter_in[3]) << ",0,0,0,"
		    << (module_counter_in[4] + module_counter_in[5]) << ",0,0,0,"
		    << (module_counter_in[6] + module_counter_in[7]) << ",0,0,0,"
		    << (module_counter_in[8] + module_counter_in[9]) << ",0,0,0,"
		    << (module_counter_in[10] + module_counter_in[11]) << ",0,0,0,"
		    << (module_counter_in[12] + module_counter_in[13]) << ",0,0,0,"
		    << (module_counter_in[14] + module_counter_in[15]) << ",0,0,0)"
		    << "; end generate;" << std::endl;

  process_time_line << "process" << std::endl;
  process_time_line << "begin" << std::endl;
  
  process_time_line << "-- <configuration>" << std::endl;
  process_time_line << "output_force_ready_i <= \"0000000000000000000000000000000000\";" << std::endl;
  process_time_line << "internallink_output_force_ready_i <= \"000000000000000000000000\";" << std::endl;
  process_time_line << "counter_parameter_reset <= '0';" << std::endl;
  process_time_line << "input_lut_frame_dout_laneid_i <= " << youngest_input_id << ";" << std::endl;
  process_time_line << "input_lut_expected_num_of_module_i <= ("
		    << module_counter_in[0] << ","
		    << module_counter_in[1] << ","
		    << module_counter_in[2] << ","
		    << module_counter_in[3] << ","
		    << module_counter_in[4] << ","
		    << module_counter_in[5] << ","
		    << module_counter_in[6] << ","
		    << module_counter_in[7] << ","
		    << module_counter_in[8] << ","
		    << module_counter_in[9] << ","
		    << module_counter_in[10] << ","
		    << module_counter_in[11] << ","
		    << module_counter_in[12] << ","
		    << module_counter_in[13] << ","
		    << module_counter_in[14] << ","
		    << module_counter_in[15] 
		    << ");" << std::endl;
  
  
  char buf[BUFSIZ];
  snprintf(buf, sizeof(buf), "enable_fmc_lanes_mask_i <= X\"%04x\";", (enable_fmc_lanes_mask&0XFFFF));
  process_time_line << buf << std::endl;
  snprintf(buf, sizeof(buf), "input_single_switch_this_board_mask_ido_i <= X\"%08x\";", this_board_mask);
  process_time_line << buf << std::endl;
  snprintf(buf, sizeof(buf), "input_single_switch_this_board_mask_ili_i <= X\"%08x\";", this_board_mask);
  process_time_line << buf << std::endl;
  process_time_line << "-- should be 0X1 or 0x4 => see the local destination table definition" << std::endl;
  set_internallink_output_destination_words(process_time_line, 
					    this_board_number,
					    DFBoardNumberToShelfAndSlotMap,
					    DFFiberConnectionMap, 
					    ATCAFabricChToOutputBitPosMap);
  
  process_time_line << "fmcin_config_channeldelay_dir_i <= (others => (others => \'0\'));" << std::endl;
  process_time_line << "fmcin_config_channeldelay_ce_i <= (others => (others => \'0\'));" << std::endl;
  
  process_time_line << "fmcin_config_clk_inv_i <= (others => \'0\');" << std::endl;
  process_time_line << "fmcin_config_clkdelay_dir_i <= (others => \'0\');" << std::endl;
  process_time_line << "fmcin_config_clkdelay_ce_i <= (others => \'0\');" << std::endl;
  process_time_line << "fmcout_config_spy_freeze_to_im_i  <= \'0\';" << std::endl;
    
  process_time_line << "-- <trigger>" << std::endl;
  process_time_line << "fmc_config_l1a_trigger_i <= \'0\';" << std::endl;
  
  process_time_line << "-- <reset procedure>" << std::endl;
  process_time_line << "fmc_config_mezzanine_reset_i    <= \'1\';" << std::endl;
  process_time_line << "fmcin_config_delay_reset_i      <= \'1\';" << std::endl;
  process_time_line << "fmcin_config_fifo_reset_i       <= \'1\';" << std::endl;
  process_time_line << "fmcin_config_frame_reset_i      <= \'1\';" << std::endl;
  process_time_line << "fmcin_config_clock_sync_reset_i  <= \'1\';" << std::endl;
  process_time_line << "fmcin_buffer_config_reset_i      <= \'1\';" << std::endl;
  process_time_line << "internallink_input_buffer_reset_i<= \'1\';" << std::endl;
  process_time_line << "input_lut_reset_i               <= \'1\';" << std::endl;
  process_time_line << "input_single_switch_reset_i     <= \'1\';" << std::endl;
  process_time_line << "output_ftkplane_word_adder_reset_i <= \'1\';" << std::endl;
  process_time_line << "output_switch_merger_reset_i       <= \'1\';" << std::endl;
  process_time_line << "output_switch_reset_i              <= \'1\';" << std::endl;
  process_time_line << "output_duplicator_reset_i          <= \'1\';" << std::endl;
  process_time_line << "output_slink_packer_reset_i        <= \'1\';" << std::endl;
  process_time_line << "internallink_output_central_switch_reset_i <= \'1\';" << std::endl;
  process_time_line << "internallink_output_destination_word_adder_reset_i <= \'1\';" << std::endl;
  process_time_line << "internallink_output_destination_word_remover_reset_i <= \'1\';" << std::endl;
  
  process_time_line << "transceiver_reset_i   <= \'1\';" << std::endl;
  process_time_line << "slink_reset_i         <= \'1\';" << std::endl;
  
  process_time_line << "-- need to wait to establish the clock" << std::endl;
  set_wait_us(process_time_line, 2);
  
  std::map<uint32_t, uint32_t>::const_iterator pixmod2dst_map_itc = pixmod2dst_map.begin();
  const std::map<uint32_t, uint32_t>::const_iterator pixmod2dst_map_ite = pixmod2dst_map.end();
  for (; pixmod2dst_map_itc!=pixmod2dst_map_ite; pixmod2dst_map_itc++) {
    set_lut_pixmod2dst(process_time_line, 
		       pixmod2dst_map_itc->first, 
		       pixmod2dst_map_itc->second);
  }
  
  std::map<uint32_t, uint32_t>::const_iterator sctmod2dst_map_itc = sctmod2dst_map.begin();
  const std::map<uint32_t, uint32_t>::const_iterator sctmod2dst_map_ite = sctmod2dst_map.end();
  for (; sctmod2dst_map_itc!=sctmod2dst_map_ite; sctmod2dst_map_itc++) {
    set_lut_sctmod2dst(process_time_line, 
		       sctmod2dst_map_itc->first, 
		       sctmod2dst_map_itc->second);
  }
  
  std::map<uint32_t, uint32_t>::const_iterator pixmod2ftkplane_map_itc = pixmod2ftkplane_map.begin();
  const std::map<uint32_t, uint32_t>::const_iterator pixmod2ftkplane_map_ite = pixmod2ftkplane_map.end();
  for (; pixmod2ftkplane_map_itc!=pixmod2ftkplane_map_ite; pixmod2ftkplane_map_itc++) {
    set_lut_pixmod2ftkplane(process_time_line, 
			    pixmod2ftkplane_map_itc->first, 
			    pixmod2ftkplane_map_itc->second);
  }
  
  std::map<uint32_t, uint32_t>::const_iterator sctmod2ftkplane_map_itc = sctmod2ftkplane_map.begin();
  const std::map<uint32_t, uint32_t>::const_iterator sctmod2ftkplane_map_ite = sctmod2ftkplane_map.end();
  for (; sctmod2ftkplane_map_itc!=sctmod2ftkplane_map_ite; sctmod2ftkplane_map_itc++) {
    set_lut_sctmod2ftkplane(process_time_line, 
			    sctmod2ftkplane_map_itc->first, 
			    sctmod2ftkplane_map_itc->second);
  }
  
  std::map<uint32_t, uint32_t>::const_iterator pixmod2tower_map_itc = pixmod2tower_map.begin();
  const std::map<uint32_t, uint32_t>::const_iterator pixmod2tower_map_ite = pixmod2tower_map.end();
  for (; pixmod2tower_map_itc!=pixmod2tower_map_ite; pixmod2tower_map_itc++) {
    set_lut_pixmod2tower(process_time_line, 
			 pixmod2tower_map_itc->first, 
			 pixmod2tower_map_itc->second);
  }
  
  std::map<uint32_t, uint32_t>::const_iterator sctmod2tower_map_itc = sctmod2tower_map.begin();
  const std::map<uint32_t, uint32_t>::const_iterator sctmod2tower_map_ite = sctmod2tower_map.end();
  for (; sctmod2tower_map_itc!=sctmod2tower_map_ite; sctmod2tower_map_itc++) {
    set_lut_sctmod2tower(process_time_line, 
			 sctmod2tower_map_itc->first, 
			 sctmod2tower_map_itc->second);
  }
  
  // reset  release
  set_wait_us(process_time_line, 1);
  process_time_line << "transceiver_reset_i   <= \'0\';" << std::endl;
  set_wait_us(process_time_line, 10);
  process_time_line << "slink_reset_i         <= \'0\';" << std::endl;
  set_wait_us(process_time_line, 5);
  process_time_line << "fmc_config_mezzanine_reset_i    <= \'0\';" << std::endl;
  set_wait_us(process_time_line, 1);
  process_time_line << "fmcin_config_delay_reset_i      <= \'0\';" << std::endl;
  set_wait_us(process_time_line, 1);
  process_time_line << "fmcin_config_fifo_reset_i       <= \'0\';" << std::endl;
  set_wait_us(process_time_line, 1);
  process_time_line << "fmcin_config_frame_reset_i      <= \'0\';" << std::endl;
  set_wait_us(process_time_line, 1);
  process_time_line << "fmcin_config_clock_sync_reset_i <= \'0\';" << std::endl;
  set_wait_us(process_time_line, 1);
  process_time_line << "fmcin_buffer_config_reset_i       <= \'0\';" << std::endl;
  process_time_line << "internallink_input_buffer_reset_i <= \'0\';" << std::endl;
  set_wait_us(process_time_line, 1);
  process_time_line << "input_lut_reset_i               <= \'0\';" << std::endl;
  set_wait_us(process_time_line, 1);
  process_time_line << "input_single_switch_reset_i     <= \'0\';" << std::endl;
  process_time_line << "internallink_output_central_switch_reset_i <= \'0\';" << std::endl;
  process_time_line << "internallink_output_destination_word_adder_reset_i <= \'0\';" << std::endl;
  process_time_line << "internallink_output_destination_word_remover_reset_i <= \'0\';" << std::endl;
  set_wait_us(process_time_line, 1);
  process_time_line << "output_ftkplane_word_adder_reset_i <= \'0\';" << std::endl;
  set_wait_us(process_time_line, 1);
  process_time_line << "output_switch_merger_reset_i       <= \'0\';" << std::endl;
  set_wait_us(process_time_line, 1);
  process_time_line << "output_switch_reset_i              <= \'0\';" << std::endl;
  set_wait_us(process_time_line, 1);
  process_time_line << "output_duplicator_reset_i          <= \'0\';" << std::endl;
  process_time_line << "output_slink_packer_reset_i        <= \'0\';" << std::endl;
  set_wait_us(process_time_line, 1);
  process_time_line << "counter_parameter_reset <= \'1\';" << std::endl;
  set_wait_us(process_time_line, 1);
  process_time_line << "counter_parameter_reset <= \'0\';" << std::endl;
  
  set_wait_us(process_time_line, 1);
  set_trigger(process_time_line); 
  set_wait_us(process_time_line, 30);
  set_trigger(process_time_line); 
  
  process_time_line << "wait;" << std::endl;
  process_time_line << "end process;" << std::endl;
  
}

//======================================================================
void set_wait_ns(std::ofstream& process_time_line, 
		 const int& time_in_ns)
{
  char buf[BUFSIZ];
  snprintf(buf, sizeof(buf), "wait for %d ns;", time_in_ns);
  process_time_line << buf << std::endl;
}

//======================================================================
void set_wait_us(std::ofstream& process_time_line, 
		 const int& time_in_us)
{
  char buf[BUFSIZ];
  snprintf(buf, sizeof(buf), "wait for %d us;", time_in_us);
  process_time_line << buf << std::endl;
}

//======================================================================
void set_trigger(std::ofstream& process_time_line)
{
  process_time_line << "fmc_config_l1a_trigger_i <= \'1\';" << std::endl;
  set_wait_ns(process_time_line, 50);
  process_time_line << "fmc_config_l1a_trigger_i <= \'0\';" << std::endl;
}

//======================================================================
void set_lut_pixmod2dst(std::ofstream& process_time_line,
			const uint32_t& address, 
			const uint32_t& data) 
{
  const int address_width = 11;
  const int data_width = 36;
  char buf[BUFSIZ];
  process_time_line << "input_lut_pixmod2dst_wen_confin_i  <= X\"0000\";" << std::endl;
  set_wait_ns(process_time_line, 50);
  snprintf(buf, sizeof(buf), "input_lut_pixmod2dst_addr_confin_i <= (others=>\"%s\");", 
	   getBin(address_width, address).c_str());
  process_time_line << buf << std::endl;
  snprintf(buf, sizeof(buf), "input_lut_pixmod2dst_data_confin_i <= (others =>\"%s\");",
	   getBin(data_width, data).c_str());
  process_time_line << buf << std::endl;
  process_time_line << "input_lut_pixmod2dst_wen_confin_i  <= X\"FFFF\";" << std::endl;
  set_wait_ns(process_time_line, 50);
  process_time_line << "input_lut_pixmod2dst_wen_confin_i  <= X\"0000\";" << std::endl;
}

//======================================================================
void set_lut_sctmod2dst(std::ofstream& process_time_line,
			const uint32_t& address, 
			const uint32_t& data) 
{
  const int address_width = 13;
  const int data_width = 36;
  char buf[BUFSIZ];
  process_time_line << "input_lut_sctmod2dst_wen_confin_i  <= X\"0000\";" << std::endl;
  set_wait_ns(process_time_line, 50);
  snprintf(buf, sizeof(buf), "input_lut_sctmod2dst_addr_confin_i <= (others=>\"%s\");", 
	   getBin(address_width, address).c_str());
  process_time_line << buf << std::endl;
  snprintf(buf, sizeof(buf), "input_lut_sctmod2dst_data_confin_i <= (others =>\"%s\");",
	   getBin(data_width, data).c_str());
  process_time_line << buf << std::endl;
  process_time_line << "input_lut_sctmod2dst_wen_confin_i  <= X\"FFFF\";" << std::endl;
  set_wait_ns(process_time_line, 50);
  process_time_line << "input_lut_sctmod2dst_wen_confin_i  <= X\"0000\";" << std::endl;
}

//======================================================================
void set_lut_pixmod2ftkplane(std::ofstream& process_time_line,
			const uint32_t& address, 
			const uint32_t& data) 
{
  const int address_width = 11;
  const int data_width = 12;
  char buf[BUFSIZ];
  process_time_line << "output_pixmod2ftkplane_wen_confin_i  <= X\"00000000\";" << std::endl;
  set_wait_ns(process_time_line, 50);
  snprintf(buf, sizeof(buf), "output_pixmod2ftkplane_addr_confin_i <= (others=>\"%s\");", 
	   getBin(address_width, address).c_str());
  process_time_line << buf << std::endl;
  snprintf(buf, sizeof(buf), "output_pixmod2ftkplane_data_confin_i <= (others =>\"%s\");",
	   getBin(data_width, data).c_str());
  process_time_line << buf << std::endl;
  process_time_line << "output_pixmod2ftkplane_wen_confin_i  <= X\"FFFFFFFF\";" << std::endl;
  set_wait_ns(process_time_line, 50);
  process_time_line << "output_pixmod2ftkplane_wen_confin_i  <= X\"00000000\";" << std::endl;
}

//======================================================================
void set_lut_sctmod2ftkplane(std::ofstream& process_time_line,
			     const uint32_t& address, 
			     const uint32_t& data) 
{
  const int address_width = 13;
  const int data_width = 12;
  char buf[BUFSIZ];
  process_time_line << "output_sctmod2ftkplane_wen_confin_i  <= X\"00000000\";" << std::endl;
  set_wait_ns(process_time_line, 50);
  snprintf(buf, sizeof(buf), "output_sctmod2ftkplane_addr_confin_i <= (others=>\"%s\");", 
	   getBin(address_width, address).c_str());
  process_time_line << buf << std::endl;
  snprintf(buf, sizeof(buf), "output_sctmod2ftkplane_data_confin_i <= (others =>\"%s\");",
	   getBin(data_width, data).c_str());
  process_time_line << buf << std::endl;
  process_time_line << "output_sctmod2ftkplane_wen_confin_i  <= X\"FFFFFFFF\";" << std::endl;
  set_wait_ns(process_time_line, 50);
  process_time_line << "output_sctmod2ftkplane_wen_confin_i  <= X\"00000000\";" << std::endl;
}

//======================================================================
void set_lut_pixmod2tower(std::ofstream& process_time_line,
			  const uint32_t& address, 
			  const uint32_t& data) 
{
  const int address_width = 11;
  const int data_width = 2;
  char buf[BUFSIZ];
  process_time_line << "output_pixmod2tower_wen_confin_i  <= X\"00000000\";" << std::endl;
  set_wait_ns(process_time_line, 50);
  snprintf(buf, sizeof(buf), "output_pixmod2tower_addr_confin_i <= (others=>\"%s\");", 
	   getBin(address_width, address).c_str());
  process_time_line << buf << std::endl;
  snprintf(buf, sizeof(buf), "output_pixmod2tower_data_confin_i <= (others =>\"%s\");",
	   getBin(data_width, data).c_str());
  process_time_line << buf << std::endl;
  process_time_line << "output_pixmod2tower_wen_confin_i  <= X\"FFFFFFFF\";" << std::endl;
  set_wait_ns(process_time_line, 50);
  process_time_line << "output_pixmod2tower_wen_confin_i  <= X\"00000000\";" << std::endl;
}

//======================================================================
void set_lut_sctmod2tower(std::ofstream& process_time_line,
			  const uint32_t& address, 
			  const uint32_t& data) 
{
  const int address_width = 13;
  const int data_width = 2;
  char buf[BUFSIZ];
  process_time_line << "output_sctmod2tower_wen_confin_i  <= X\"00000000\";" << std::endl;
  set_wait_ns(process_time_line, 50);
  snprintf(buf, sizeof(buf), "output_sctmod2tower_addr_confin_i <= (others=>\"%s\");", 
	   getBin(address_width, address).c_str());
  process_time_line << buf << std::endl;
  snprintf(buf, sizeof(buf), "output_sctmod2tower_data_confin_i <= (others =>\"%s\");",
	   getBin(data_width, data).c_str());
  process_time_line << buf << std::endl;
  process_time_line << "output_sctmod2tower_wen_confin_i  <= X\"FFFFFFFF\";" << std::endl;
  set_wait_ns(process_time_line, 50);
  process_time_line << "output_sctmod2tower_wen_confin_i  <= X\"00000000\";" << std::endl;
}

// =================================
std::string 
getBin(const int num, uint32_t input)
{
  std::vector<std::string> output_vector(num);
  
  for (int ii=0; ii<num; ii++) {
    if (input%2==1) output_vector[ii]="1";
    else            output_vector[ii]="0";
    
    input=input-input%2;
    input=input/2;
  }
  
  std::string output("");
  for (int ii=0; ii<num; ii++) {
    int idx = (num-1)-ii;
    output+=output_vector.at(idx);
  }
  
  return output;
}


//======================================================================
// this function needs to be maintained carefully
//======================================================================
bool
UsedOutputLane(const int& iOutLane, 
	       const bool& UsedInTopTower, 
	       const bool& UsedInBotTower, 
	       const int& UsedDFOutputLane)
{
  if ((iOutLane%18) < 16) { // AUX channel
    //printf("dbg> %d %d %d %5s %5s [%d]\n", iOutLane, UsedDFOutputLane, ((iOutLane%17)%8), UsedInTopTower ? "TRUE" : "FALSE", UsedInBotTower ? "TRUE" : "FALSE", __LINE__);
    if ( UsedDFOutputLane!= ((iOutLane%18)%8) ) return false;
    if ( iOutLane < 16) return UsedInTopTower; // top tower
    else                return UsedInBotTower; // bottom tower
    
  } else {
    if (UsedDFOutputLane!=8) return false;
    if (iOutLane==16 or iOutLane==17) return UsedInTopTower;
    if (iOutLane==34 or iOutLane==35) return UsedInBotTower;
  }
  return false;
}

//======================================================================
void print_param(const std::string& name, const uint32_t& value, const int& numbits)
{
  printf("%-35s 0x%08x : %s\n", name.c_str(), value, getBin(numbits, value).c_str());
}

//======================================================================
void print_param_int(const std::string& name, const uint32_t& value)
{
  printf("%-35s %-8d \n", name.c_str(), value);
}

//======================================================================
void dump_configuration(const int& youngest_input_id,
			const uint32_t& this_board_number,
			const std::vector<int>& module_counter_in, //[16]
			const std::vector<int>& module_counter_out, //[16]
			const uint32_t& enable_fmc_lanes_mask, //[bit mask for 16bit]
			std::map<uint32_t, uint32_t> pixmod2dst_map,
			std::map<uint32_t, uint32_t> sctmod2dst_map,
			std::map<uint32_t, uint32_t> pixmod2ftkplane_map,
			std::map<uint32_t, uint32_t> sctmod2ftkplane_map,
			std::map<uint32_t, uint32_t> pixmod2tower_map,
			std::map<uint32_t, uint32_t> sctmod2tower_map
			)
{
  const uint32_t this_board_mask = (0X1<<this_board_number);
  
  char buf[BUFSIZ];
  print_param("FMC Input Enable Mask", enable_fmc_lanes_mask, 16);
  print_param_int("Header/Trailer source input lane", youngest_input_id);
  print_param("This board mask", this_board_mask, 32);
  printf("\n");
  printf("-- input lane number of module list --\n");
  
  for (int iInputLane=0; iInputLane<NumberOfFMCInputLanesPerBoard; iInputLane++) {
    snprintf(buf, sizeof(buf), "Number of module @ FMC input - CH%d", iInputLane);
    print_param_int(buf, module_counter_in[iInputLane]);
  }

  printf("\n");
  printf("-- output lane number of module list --\n");
  for (int iOutputLane=0; iOutputLane<NumberOfOutputLanesPerBoard; iOutputLane++) {
    snprintf(buf, sizeof(buf), "Number of module @ output - CH%d", iOutputLane);
    print_param_int(buf, module_counter_out[iOutputLane]);    
  }
  
  printf("\n");
  printf("-- input pixel modules (total=%4d modules are picked up) --\n", pixmod2dst_map.size());
  std::map<uint32_t, uint32_t>::const_iterator pixin_itc = pixmod2dst_map.begin();
  const std::map<uint32_t, uint32_t>::const_iterator pixin_ite = pixmod2dst_map.end();
  for (; pixin_itc!=pixin_ite; pixin_itc++) {
    printf("Modules ID %4d : ", pixin_itc->first);
    print_param("Destination Bits", pixin_itc->second, 32);
    printf("\n");
  }
  
  printf("\n");
  printf("-- input sct modules (total=%4d modules are picked up) --\n", sctmod2dst_map.size());
  std::map<uint32_t, uint32_t>::const_iterator sctin_itc = sctmod2dst_map.begin();
  const std::map<uint32_t, uint32_t>::const_iterator sctin_ite = sctmod2dst_map.end();
  for (; sctin_itc!=sctin_ite; sctin_itc++) {
    printf("Modules ID %4d :", sctin_itc->first);
    print_param("Destination Bits", sctin_itc->second, 32);
  }
  
  printf("\n");
  printf("-- output pixel modules (total=%4d modules are picked up) --\n", pixmod2tower_map.size());
  std::map<uint32_t, uint32_t>::const_iterator pixout_itc = pixmod2tower_map.begin();
  const std::map<uint32_t, uint32_t>::const_iterator pixout_ite = pixmod2tower_map.end();
  for (; pixout_itc!=pixout_ite; pixout_itc++) {
    printf("Modules ID %4d \n", pixout_itc->first);
    print_param("Output Lane map", pixmod2ftkplane_map[pixout_itc->first], 12);
    print_param("Output Tower map", pixout_itc->second, 2);
    printf("\n");
  }
  
  printf("\n");
  printf("-- output sct modules (total=%4d modules are picked up) --\n", sctmod2tower_map.size());
  std::map<uint32_t, uint32_t>::const_iterator sctout_itc = sctmod2tower_map.begin();
  const std::map<uint32_t, uint32_t>::const_iterator sctout_ite = sctmod2tower_map.end();
  for (; sctout_itc!=sctout_ite; sctout_itc++) {
    printf("Modules ID %4d \n", sctout_itc->first);
    print_param("Output Lane map", sctmod2ftkplane_map[sctout_itc->first], 12);
    print_param("Output Tower map", sctout_itc->second, 2);
  }
}

//======================================================================
void
CreateTestVector(std::ofstream& datfile,
		 std::ofstream& coefile,
		 std::vector<uint32_t> module_id_list,
		 const int nWordsPerModule,
		 const int nEvents,
		 bool isSctLink
		 )
{
  unsigned int word_bof = 0XB0F0;
  unsigned int word_eof = 0XE0F0;
  const int nModule = module_id_list.size();
  
  std::string delayWord       = "00001";
  std::string eventendWord    = "01000";
  std::string terminationWord = "11000";
  std::string eventstartword  = "00100"; // Newly defined
  std::string controlWord     = "00100"; // Newly defined
  std::string dataWord        = "00000";
  
  coefile << "memory_initialization_radix=2;" << std::endl;
  coefile << "memory_initialization_vector=" << std::endl;
  
  for (int jj=0; jj<nEvents; jj++) {
    outputlineTestVector(coefile, datfile, delayWord, 0);
    // Second Word is sent as Control Word (Begin of Fragment word)
    outputlineTestVector(coefile, datfile, eventstartword, word_bof<<16); // NEW (Word-0)
    outputlineTestVector(coefile, datfile, dataWord, 0XFF1234FF); // Start of header (Word-1)
    outputlineTestVector(coefile, datfile, dataWord, 0X00000001); // Run Number (Word-5)
    outputlineTestVector(coefile, datfile, dataWord, jj)        ; // Event Number (Word-6)
    outputlineTestVector(coefile, datfile, dataWord, 0X000000FF); // Bunch Counter ID (Word-7)
    outputlineTestVector(coefile, datfile, dataWord, 0X00000000); // Trigger Type (Word-8)
    outputlineTestVector(coefile, datfile, dataWord, 0X00000000); // Detector Event Type (Word-9)
    
    for (int ii=0; ii<nModule; ii++) {
      const uint32_t headerword = (isSctLink) ? (0X80000000 | 0X00008000 | (module_id_list[ii]&0X1FFF) ) : (0X80000000 | (module_id_list[ii]&0X1FFF) );
      outputlineTestVector(coefile, datfile, dataWord, headerword); // internal df header
      
      for (int iWord=0; iWord<nWordsPerModule; iWord++) {
	outputlineTestVector(coefile, datfile, dataWord, ( ((iWord<<24)|(0XCAFE)) & 0X7FFFFFFF)); //
      }
    }
    
    outputlineTestVector(coefile, datfile, controlWord, 0XE0DA0000); // End of Data Control Word
    outputlineTestVector(coefile, datfile, dataWord, jj)        ; // L1 ID (Word-1)
    outputlineTestVector(coefile, datfile, dataWord, 0x0)       ; // Error Flag (Word-2)
    outputlineTestVector(coefile, datfile, dataWord, 0x0)       ; // Reserved (Word-3)
    outputlineTestVector(coefile, datfile, dataWord, 0x0)       ; // CRC (Word-4)
    
    // The last Word is sent as Control Word (End of Fragment word)
    
    if (jj+1!=nEvents) { outputlineTestVector(coefile, datfile, eventendWord , word_eof<<16); }
    else { outputlineTestVector(coefile, datfile, terminationWord , word_eof<<16, true); }
  }
}

// =================================
void
outputlineTestVector(std::ofstream& coefile,
		     std::ofstream& datfile,
		     const std::string control_word,
		     const uint32_t data,
		     bool eof_coe) {
  const uint32_t control_word_ui = strtoul(control_word.c_str(), NULL, 2);
  if (not eof_coe) { coefile << control_word << getBin(32, data) << "," << std::endl; }
  else { coefile << control_word << getBin(32, data) << ";" << std::endl; } 
  char buf[BUFSIZ];
  snprintf(buf, sizeof(buf), "%02x%08x", control_word_ui, data);
  datfile << buf << std::endl;
}

// =================================
uint32_t
ReturnFabricBitPositionFromSourceAndDestination
(
 const uint32_t& source_shelf,
 const uint32_t& source_slot,
 const uint32_t& destination_shelf,
 const uint32_t& destination_slot,
 const uint32_t& DF1, 
 const uint32_t& DF2, 
 std::map<uint32_t, uint32_t> ATCAFabricChToOutputBitPosMap,
 std::map<uint32_t, uint32_t> DFFiberConnectionMap, 
 std::map<uint32_t, std::pair<uint32_t, uint32_t> > DFBoardNumberToShelfAndSlotMap)
                                                             // 
{

  std::cout<<"DF1 "<<DF1<<" DF2 "<<DF2<<std::endl;
  
  if (source_shelf==destination_shelf and 
      source_slot==destination_slot) {
    fprintf(stderr, "ERR> the same channels are input s:[%2d,%2d], d:[%2d,%2d]\n",
	    source_shelf, source_slot, destination_shelf, destination_slot);
  }
  
  // Firmware design dependency
  // see Switching Bit Assignment sheet of
  // https://box.cern.ch/public.php?service=files&t=323c839f9407229887e21d0965f8ae5a
  
  // case of FABRIC interface
  // ATCA standard fabric channel counting way
  else if (source_shelf == destination_shelf){
    std::cout<<"same shelf"<<std::endl;
    const uint32_t ATCA_FABRIC_CHANNEL_ID =  // in terms of channel number of fabric interface
      (source_slot>destination_slot) ? destination_slot : destination_slot-1;
    
    return ATCAFabricChToOutputBitPosMap[ATCA_FABRIC_CHANNEL_ID];
  }

  // case of FIBER connections
  // We have in total 4 cases
  // case 1: DF 1 - DF 2 has direct connection (go through fiber directly)  
  // case 2: DF 1 is connected to the crate that DF 2 is in (go through fiber directly)
  // case 3: DF 1 is not connected to the crate that DF 2 is in, and DF 3 is in crate of DF 1 and directly connected DF 2 (go through fabric)
  // case 4: DF 1 is not connected to the crate that DF 2 is in, and there isn't a DF is in crate of DF 1 and directly connected DF 2 (go through fabric)

    
  else{
    // case 1
    if ( DFFiberConnectionMap[DF1] == DF2){
      //std::cout<<"Case I: DF1 and DF2 has direct connection"<<std::endl;
      return 14;
    }

    // case 2 
    else if (   DFBoardNumberToShelfAndSlotMap[ DFFiberConnectionMap[DF1] ].first == destination_shelf){
      //std::cout<<"Case II"<<std::endl;
      return 14;
    }
    
    else{
      // case 3 and 4
      
      std::cout<<"Case III and IV"<<std::endl;
      int midBoard_slot;      
      std::vector<int> DFsConnectedtoNextCrate;

      // case 3
      for(int iBoard=0; iBoard < NumberOfDFBoards; iBoard ++){
	if( DFBoardNumberToShelfAndSlotMap[iBoard].first == source_shelf){
	  if( DFFiberConnectionMap[iBoard] == DF2 ){
	    midBoard_slot = DFBoardNumberToShelfAndSlotMap[iBoard].second;

	    const uint32_t ATCA_FABRIC_CHANNEL_ID =  // in terms of channel number of fabric interface
	      (source_slot>midBoard_slot) ? midBoard_slot : midBoard_slot-1;
	    
	    std::cout<<"find good board "<<iBoard<<std::endl;
	    //std::cout<<" ATCA ID "<<ATCA_FABRIC_CHANNEL_ID<<std::endl;
	    //std::cout<<" bit "<<ATCAFabricChToOutputBitPosMap[ATCA_FABRIC_CHANNEL_ID]<<std::endl;

  	    return ATCAFabricChToOutputBitPosMap[ATCA_FABRIC_CHANNEL_ID];
	  }
	}
      }

      // case 4
      for(int iBoard=0; iBoard < NumberOfDFBoards; iBoard ++){
	if( DFBoardNumberToShelfAndSlotMap[iBoard].first == source_shelf){

	  int destination_board = DFFiberConnectionMap[iBoard];
	  if (DFBoardNumberToShelfAndSlotMap[destination_board].first == DFBoardNumberToShelfAndSlotMap[DF2].first){
	    DFsConnectedtoNextCrate.push_back(iBoard);
	    std::cout<<"source board "<<DF1<<" final board "<<DF2<<" checking board "<<iBoard<<" destination board "<<destination_board<<std::endl;
	  }
	}
      }

      // case 4
      //std::cout << "getting random board!" << endl;
      //exit(1);
      int random_board = (rand()%DFsConnectedtoNextCrate.size());
      std::cout<<"intermediate board "<<DFsConnectedtoNextCrate[random_board]<<std::endl;
      midBoard_slot = DFBoardNumberToShelfAndSlotMap[  DFsConnectedtoNextCrate [random_board]   ].second;
      const uint32_t ATCA_FABRIC_CHANNEL_ID =  // in terms of channel number of fabric interface
	(source_slot>midBoard_slot) ? midBoard_slot : midBoard_slot-1;
      return ATCAFabricChToOutputBitPosMap[ATCA_FABRIC_CHANNEL_ID];
    }
  }
  return 0;
}

// =================================
uint32_t ReturnInternalLinkOutputBitMap
(const std::map<uint32_t, uint32_t>& ATCAFabricChToOutputBitPosMap,
 const std::map<uint32_t, uint32_t>& DFFiberConnectionMap, 
 std::map<uint32_t, std::pair<uint32_t, uint32_t> > DFBoardNumberToShelfAndSlotMap, // copy on purpose
 const uint32_t& this_board_number,
 const uint32_t& destination_board_number) 
{
  if (this_board_number == destination_board_number) {
    fprintf(stderr, "ERR> this_board_number (%2d)==destination_board_number (%2d)\n",
	    this_board_number, destination_board_number);
    return 0X0;
  }
  
  const uint32_t source_shelf      = DFBoardNumberToShelfAndSlotMap[this_board_number].first;
  const uint32_t source_slot       = DFBoardNumberToShelfAndSlotMap[this_board_number].second;
  const uint32_t destination_shelf = DFBoardNumberToShelfAndSlotMap[destination_board_number].first;
  const uint32_t destination_slot  = DFBoardNumberToShelfAndSlotMap[destination_board_number].second;
  
  const uint32_t bitPos = ReturnFabricBitPositionFromSourceAndDestination
    (source_shelf, source_slot, destination_shelf, destination_slot, this_board_number, destination_board_number,
     ATCAFabricChToOutputBitPosMap, DFFiberConnectionMap, DFBoardNumberToShelfAndSlotMap );
  return ((0X1)<<bitPos);
}

// =================================
void
set_internallink_output_destination_words
(std::ofstream& process_time_line, 
 const uint32_t& this_board_number,
 const std::map<uint32_t, std::pair<uint32_t, uint32_t> >& DFBoardNumberToShelfAndSlotMap,
 const std::map<uint32_t, uint32_t>& DFFiberConnectionMap, 
 const std::map<uint32_t, uint32_t>& ATCAFabricChToOutputBitPosMap 
 ) {
  char buf[BUFSIZ];
  process_time_line << "internallink_output_destination_words_i <=( " << std::endl;
  for (int iDestinationBoard=0; iDestinationBoard<NumberOfDFBoards; iDestinationBoard++) {
    const uint32_t mask_pattern 
      = (iDestinationBoard!=this_board_number) ? ReturnInternalLinkOutputBitMap(ATCAFabricChToOutputBitPosMap, 
										DFFiberConnectionMap, 
										DFBoardNumberToShelfAndSlotMap,
										this_board_number,
										iDestinationBoard) : 0X0;
    if ( (iDestinationBoard+1)<NumberOfDFBoards) snprintf(buf, sizeof(buf), "X\"%04x\",", mask_pattern);  
    else snprintf(buf, sizeof(buf), "X\"%04x\");", mask_pattern); 
    process_time_line << buf << std::endl;
  }
}


// =================================
void
dump_internallink_output_destination_words
(std::ofstream& configuration_file, 
 const uint32_t& this_board_number,
 const std::map<uint32_t, std::pair<uint32_t, uint32_t> >& DFBoardNumberToShelfAndSlotMap,
 const std::map<uint32_t, uint32_t>& DFFiberConnectionMap, 
 const std::map<uint32_t, uint32_t>& ATCAFabricChToOutputBitPosMap 
 ) {
  for (int iDestinationBoard=0; iDestinationBoard<NumberOfDFBoards; iDestinationBoard++) {
    const uint32_t mask_pattern 
      = (iDestinationBoard!=this_board_number) ? ReturnInternalLinkOutputBitMap(ATCAFabricChToOutputBitPosMap, 
										DFFiberConnectionMap, 
										DFBoardNumberToShelfAndSlotMap,
										this_board_number,
										iDestinationBoard) : 0X0;
    char buf[BUFSIZ];
    char value[BUFSIZ];
    
    snprintf(value, sizeof(value), "0X%04x", mask_pattern);
    snprintf(buf, sizeof(buf), "%-35s %10d %10s", "centralswlaneid2destinationmask", iDestinationBoard, value);
    configuration_file << buf << std::endl;
  }
}


void ParseSystemConfigFile(std::string system_config_file,
			   std::map<uint32_t, std::pair<uint32_t, uint32_t> > &slotshelf,
			   std::map<uint32_t, uint32_t> & planetooutput,
			   std::map<uint32_t, uint32_t> & TopTowerMap,
			   std::map<uint32_t, uint32_t> & BotTowerMap,
			   std::map<uint32_t, uint32_t> & chantooutbit,
			   std::map<uint32_t, uint32_t> & dfdfconnection,
			   int & numberOfFTKTowers){
  

  string line;
  ifstream conf_file (system_config_file.c_str());
  if (conf_file.is_open())
  {
    cout << "Loading system config file \n";
    while ( getline (conf_file,line) )
    {
      if (line.find("#")!= string::npos)
	continue;

      if (line.substr(0,17) == "NumberOfFTKTowers"){

	stringstream ss(line);
	string key;
	uint32_t nt;
	ss>>key>>std::setbase(0)>>nt;
	numberOfFTKTowers = nt;
      }

      //now trying to find correct key
      if (line.substr(0, 20) == "BoardNToShelfAndSlot"){
	//if we got it, find the values...so painful in cpp
	stringstream ss(line);
	string key;
	uint32_t board, shelf, slot;
	
	while(ss>>key>> std::setbase(0) >> board >> shelf >> slot){
	  //build object
	  std::pair<uint32_t, std::pair<uint32_t, uint32_t> >
	    tmp_pair(board, std::pair<uint32_t, uint32_t>(shelf,slot)); 

	  slotshelf.insert(tmp_pair);
	}  
      }else if (line.substr(0,13) == "PlaneToOutBit"){
	stringstream ss(line);
	string key;
	uint32_t plane, outbit;
	
	while(ss>>key>> std::setbase(0) >> plane >> outbit){
	  //build object
	  std::pair<uint32_t, uint32_t >
	    tmp_pair(plane , outbit); 

	  planetooutput.insert(tmp_pair);

	}
      }else if (line.substr(0,16) == "BoardNToTopTower"){
	stringstream ss(line);
	string key;
	uint32_t board, tower;
	while(ss>>key>> std::setbase(0) >> board >> tower){
	  
	  //build object
	  std::pair<uint32_t, uint32_t >
	    tmp_pair(board , tower); 
	  TopTowerMap.insert(tmp_pair);

	}

      }else if (line.substr(0,16) == "BoardNToBotTower"){
	stringstream ss(line);
	string key;
	uint32_t board, tower;
	
	while(ss>>key>> std::setbase(0) >> board >> tower){

	  //build object
	  std::pair<uint32_t, uint32_t >
	    tmp_pair(board , tower); 
	  
	  BotTowerMap.insert(tmp_pair);

	}

      }
      else if (line.substr(0,12) == "ChanToOutBit"){
	stringstream ss(line);
	string key;
	uint32_t chan, obit;
	
	while(ss>>key>> std::setbase(0) >> chan >> obit){

	  //build object
	  std::pair<uint32_t, uint32_t >
	    tmp_pair(chan , obit); 
	  
	  chantooutbit.insert(tmp_pair);

	}

      }
      else if (line.substr(0,17) =="DFFiberConnection"){
	stringstream ss(line);
	string key;
	uint32_t DF1, DF2;
	
	while(ss>>key>> std::setbase(0) >> DF1 >> DF2){

	  //build object
	  std::pair<uint32_t, uint32_t >
	    tmp_pair_1(DF1 , DF2); 
	  std::pair<uint32_t, uint32_t >
	    tmp_pair_2(DF2 , DF1); 

	  dfdfconnection.insert(tmp_pair_1);
	  dfdfconnection.insert(tmp_pair_2);

	}
      }

      else{ continue;}
      
    }
    conf_file.close();
  }
  else{
    cout << "Unable to open file \n"; 
    return;
  }

  return;
}

//======================================================================
/** This function makes a RodIdToFMCInIdMap object from an input 
 * configuration file name
 */
//======================================================================
void ParseBoardConfigFile(std::string board_config_file, 
			  std::multimap<uint32_t, std::pair<uint32_t, uint32_t> >  &rodmap,
			  std::map<uint32_t, bool>& BoardEnableMap){



 
  //First open the file
  string line;
  ifstream conf_file (board_config_file.c_str());
  if (conf_file.is_open())
  {
    cout << "Loading board config file \n";
    while ( getline (conf_file,line) )
    {
      if (line.find("#")!= string::npos)
	continue;

      //now trying to find correct key
      if (line.substr(0, 11) == "boardEnable"){

	//if we got it, find the values...so painful in cpp
	stringstream ss(line);
	string buf;
	string key;
	uint32_t board;
	uint32_t en;
	
	while(ss>>key>> std::setbase(0) >>  board >> en){
	  //build object
	  std::pair<uint32_t, bool >
	    tmp_pair(board,bool(en));
	  cout<<board<<" "<<en<<endl;
	  BoardEnableMap.insert(tmp_pair);
	}  
      }else if (line.substr(0, 10) == "rodToBoard"){
	
	//if we got it, find the values...so painful in cpp
	stringstream ss(line);
	string buf;
	string key;
	uint32_t robid;
	uint32_t board;
	uint32_t fmclane;
	
	while(ss>>key>> std::setbase(0) >>robid>> board >> fmclane){
	  //build object
	  //first check if the board is enabled:
	  
	  if(BoardEnableMap.find(board) != BoardEnableMap.end()){
	    if (BoardEnableMap.find(board)->second){
	      std::pair<uint32_t, std::pair<uint32_t, uint32_t> >
		tmp_pair(robid, std::pair<uint32_t, uint32_t>(board,fmclane));
	      rodmap.insert(tmp_pair);
	      cout<<"Filling "<<std::hex<<robid<<std::dec<<" "<<board<<" "<<fmclane<<endl;
	    }
	  }
	} 
      }else{ continue;}
      
    }
    conf_file.close();
  }
  else{
    cout << "Unable to open file \n"; 
  }
  return;  
}
void ParseBoardDelayFile(std::string board_delay_file, 
		      std::map<uint32_t, std::pair<uint32_t, uint32_t> >  &delaymap){


 
  //First open the file
  string line;
  ifstream conf_file (board_delay_file.c_str());
  if (conf_file.is_open())
  {
    cout << "Loading board config file \n";
    while ( getline (conf_file,line) )
    {
      if (line.find("#")!= string::npos)
	continue;

      //now trying to find correct key
      if (line.substr(0, 12) == "LaneDelayVal"){
	
	//if we got it, find the values...so painful in cpp
	stringstream ss(line);
	string buf;
	string key;
	uint32_t lane;
	uint32_t inv;
	uint32_t delay;
	
	while(ss>>key>> std::setbase(0) >>lane>> inv >> delay){
	  //build object
	  std::pair<uint32_t, std::pair<uint32_t, uint32_t> >
	    tmp_pair(lane, std::pair<uint32_t, uint32_t>(inv,delay));
	      delaymap.insert(tmp_pair);
	      cout<<"Filling delay values"<<" "<<lane<<" "<<inv<<" "<<delay<<endl;
	}
      }else{ continue;}
      
    }
    conf_file.close();
  }
  else{
    cout << "Unable to open file \n"; 
  }
  return;  
}
 
int GetBoardNumberFromShelfSlot(int this_shelf, int this_slot,
				std::map<uint32_t, std::pair<uint32_t, uint32_t> > DFBoardNumberToShelfAndSlotMap){

  std::map<uint32_t, std::pair<uint32_t, uint32_t> >::iterator  ent1 = 
    DFBoardNumberToShelfAndSlotMap.begin();

  for (;ent1 != DFBoardNumberToShelfAndSlotMap.end(); ++ent1){
    std::pair<uint32_t, uint32_t> ss_pair = ent1->second;
    if ((ss_pair.first == this_shelf) && (ss_pair.second == this_slot))
      return ent1->first;
  }
  return -1;
}
