#For P1:
#8-board configs
#for b in 0 4 8 12 16 20 24 28 1 5 9 13 17 21 25 29; do ./DFConfigurationMaker -B ${b} -C config_txt_files/df_shelf1_config.txt -S config_txt_files/df_system_config_32towers.txt -D config_txt_files/df_board_delay_values_B0.txt -O configuration_8board_B${b}.txt -M modulelist_naoki.txt; done
#for b in 0; do ./DFConfigurationMaker -B ${b} -C config_txt_files/df_shelf1_config.txt -S config_txt_files/df_system_config_32towers.txt -D config_txt_files/df_board_delay_values_B0.txt -O configuration_8board_B${b}.txt -M modulelist_naoki.txt; done

#./PrintRXMask -B 0,4 -O rx_configuration.txt -S config_txt_files/df_system_config_32towers.txt 

#./DFConfigurationMaker -B 0 -C config_txt_files/df_board_crate1_config_tomoya.txt -S config_txt_files/df_system_config_32towers.txt -D config_txt_files/df_board_delay_values_B0.txt -O configuration_tomoya.txt -M modulelist_naoki.txt


#1-5 
#./DFConfigurationMaker -H 1 -L 5 -C config_txt_files/df_board_crate1_config_03_21_2016.txt -S config_txt_files/df_system_config_32towers.txt -D config_txt_files/df_board_delay_values_B0.txt -O configuration_8board_1-05.txt -M modulelist.txt


#DF.18 (Tower 22, B26)
#./DFConfigurationMaker -B 26 -C config_txt_files/df_board_crate1_config_26.txt -S config_txt_files/df_system_config_32towers.txt -D config_txt_files/df_board_delay_values_B0.txt -O configuration_B26.txt -M modulelist_naoki.txt

#DF.32, SCT-only
#./DFConfigurationMaker -B 14 -C config_txt_files/df_board_crate1_config_14.txt -S config_txt_files/df_system_config_32towers.txt -D config_txt_files/df_board_delay_values_B0.txt -O configuration_B14.txt -M modulelist.txt

#For Lab4:
#DF20
#./DFConfigurationMaker -B 20 -C config_txt_files/df_board_crate1_config_20.txt -S config_txt_files/df_system_config_32towers.txt -D config_txt_files/df_board_delay_values_B0.txt -O configuration.txt -M modulelist.txt

#DF22
#./DFConfigurationMaker -B 22 -C config_txt_files/df_board_crate1_config_22.txt -S config_txt_files/df_system_config_32towers.txt -D config_txt_files/df_board_delay_values_B0.txt -O configuration.txt -M modulelist.txt
#./DFConfigurationMaker -H 3 -L 8 -C config_txt_files/df_board_crate1_config_22.txt -S config_txt_files/df_system_config_32towers.txt -D config_txt_files/df_board_delay_values_B0.txt -O configuration.txt -M modulelist.txt


# DF 2-08
./DFConfigurationMaker -B 21 -C config_txt_files/df_board_crate1_config_masahiro.txt -S config_txt_files/df_system_config_32towers.txt -D config_txt_files/df_board_delay_values_B0.txt -O configuration_DF-2-08.txt -M modulelist.txt



#Example files:

#-C config_txt_files/df_board_crate1_config.txt   # enable boards via the board enable key
#-S config_txt_files/df_system_config.txt  # don't change
#-D config_txt_files/df_board_delay_values_B0.txt  #example delay values
#-M modulelist.txt #module list txt file.  current version is for Summer 2016 cabling
#-O configuration.txt #output configuration file
