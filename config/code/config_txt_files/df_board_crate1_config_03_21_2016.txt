################################
##Key BoardNumber Enabled?(0 = no, 1 = yes)
################################
################ crate 1 slot 3
boardEnable 0 1
################ crate 1 slot 4
boardEnable 4 1
################ crate 1 slot 5 DF.48
boardEnable 8 0
################ crate 1 slot 6
boardEnable 12 0
################ crate 1 slot 7
boardEnable 16 0
################ crate 1 slot 8 DF.51
boardEnable 20 0
################ crate 1 slot 9 DF.52
boardEnable 24 0
################ crate 1 slot 10 DF.53
boardEnable 28 0
################################
##KEY RobID DFNumber IMLane
################################
#rodToBoard 0x120220 0 2
#rodToBoard 0x130307 0 4
#rodToBoard 0x111706 0 6
#rodToBoard 0x140062 0 0
#rodToBoard 0x130310 0 12
#rodToBoard 0x121616 0 10
#rodToBoard 0x140072 0 8
#rodToBoard 0x111746 0 14
rodToBoard 0x240100 0 5
rodToBoard 0x240105 0 7
rodToBoard 0x240101 0 13
#rodToBoard 0x240103 0 15
########################
#rodToBoard 0x112512 4  2	  ???  (see note, cable with matching ID not found) 
#rodToBoard 0x111806 4  4
#rodToBoard 0x140063 4  0
#rodToBoard 0x112552 4  6
#rodToBoard 0x112515 4  10
#rodToBoard 0x111808 4  12
#rodToBoard 0x140073 4  8
#rodToBoard 0x112555 4  14
rodToBoard 0x220009 4  5 
rodToBoard 0x220008 4  7 
rodToBoard 0x22000a 4  13
rodToBoard 0x22000b 4  15
########################
#rodToBoard 0x112511 8 2
#rodToBoard 0x111805 8 4
#rodToBoard 0x140060 8 0
#rodToBoard 0x112551 8 6
#rodToBoard 0x112514 8 10
#rodToBoard 0x111807 8 12
#rodToBoard 0x140070 8 8
#rodToBoard 0x112554 8 14
rodToBoard 0x210009 8 5
rodToBoard 0x210008 8 7
rodToBoard 0x21000a 8 13
rodToBoard 0x21000b 8 15
########################
#rodToBoard 0x120221 12 2
#rodToBoard 0x130308 12 4
#rodToBoard 0x111705 12 6
#rodToBoard 0x140061 12 0
#rodToBoard 0x130309 12 12
#rodToBoard 0x121617 12 10
#rodToBoard 0x140071 12 8
#rodToBoard 0x111745 12 14
rodToBoard 0x230009 12 7
rodToBoard 0x23000e 12 5
rodToBoard 0x230008 12 15
rodToBoard 0x23000f 12 13
########################
#rodToBoard 0x120218 16 2
#rodToBoard 0x130311 16 4
#rodToBoard 0x111810 16 6
#rodToBoard 0x140082 16 0
#rodToBoard 0x130316 16 12
#rodToBoard 0x140092 16 8
#rodToBoard 0x111748 16 10
#rodToBoard 0x111850 16 14
rodToBoard 0x240102 16 5
rodToBoard 0x240106 16 7
rodToBoard 0x240104 16 13
rodToBoard 0x240107 16 15
########################
#rodToBoard 0x130315 20 4
#rodToBoard 0x112517 20 2
#rodToBoard 0x140083 20 0
#rodToBoard 0x112557 20 6
#rodToBoard 0x112519 20 10
#rodToBoard 0x111708 20 12
#rodToBoard 0x140093 20 8
#rodToBoard 0x112559 20 14
rodToBoard 0x22000c 20 7
rodToBoard 0x22000d 20 5
rodToBoard 0x22000e 20 13
#rodToBoard 0x22000f 20 15
########################
#rodToBoard 0x112516 24 2
#rodToBoard 0x111809 24 4
#rodToBoard 0x140080 24 0
#rodToBoard 0x112556 24 6
#rodToBoard 0x130314 24 12
#rodToBoard 0x112518 24 10
#rodToBoard 0x140090 24 8
#rodToBoard 0x112558 24 14
rodToBoard 0x21000d 24 5
rodToBoard 0x21000c 24 7
#rodToBoard 0x21000c 24 7
rodToBoard 0x21000e 24 13
rodToBoard 0x21000f 24 15
########################
#rodToBoard 0x120219  28 2
#rodToBoard 0x130312  28 4
#rodToBoard 0x111707  28 6
#rodToBoard 0x140081  28 0
#rodToBoard 0x130317  28 12
#rodToBoard 0x140091  28 8
#rodToBoard 0x111747 28 10
#rodToBoard 0x111849 28 14
rodToBoard 0x23000a  28 5
rodToBoard 0x23000c  28 7
rodToBoard 0x23000b  28 15
rodToBoard 0x23000d  28 13
