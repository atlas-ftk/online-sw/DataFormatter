#needed in order to set the configuration of the connections
export arch_comp=x86_64-slc6-gcc49-opt

export DF_IPUBUS_CONFIG=`pwd`/config
export PATH=`pwd`/../installed/${arch_comp}/bin:$PATH
export LD_LIBRARY_PATH=`pwd`/../installed/${arch_comp}/lib:$LD_LIBRARY_PATH

#source cmt/setup.sh
